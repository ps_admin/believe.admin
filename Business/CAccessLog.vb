Imports System.IO.FileInfo
Namespace Internal.Church

    <Serializable()>
    <CLSCompliant(True)>
    Public Class CAccessLog

        Private mbChanged As Boolean

        Private miAccessLogID As Integer
        Private miContactID As Integer
        Private msModuleName As String
        Private mdLogTime As DateTime
        Private msScreen As String
        Private msURLLink As String
        Private msItemCategory As String
        Private msRecordAccessed As String
        Private msIPaddress As String
        Private msUserBrowser As String


        Public Sub Save()

            If mbChanged Then

                Dim oParameters As New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", miContactID))
                oParameters.Add(New SqlClient.SqlParameter("@Module", msModuleName))
                oParameters.Add(New SqlClient.SqlParameter("@LogTime", SQLDateTime(mdLogTime)))
                oParameters.Add(New SqlClient.SqlParameter("@Screen", msScreen))
                oParameters.Add(New SqlClient.SqlParameter("@URLLink", msURLLink))
                oParameters.Add(New SqlClient.SqlParameter("@ItemCategory", msItemCategory))
                oParameters.Add(New SqlClient.SqlParameter("@RecordAccessed", msRecordAccessed))
                DataAccess.ExecuteCommand("sp_Church_AccessLogInsert", oParameters)

                mbChanged = False

            End If

        End Sub

        Public Sub SaveWithBrowserIP()

            If mbChanged Then

                Dim oParameters As New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", miContactID))
                oParameters.Add(New SqlClient.SqlParameter("@Module", msModuleName))
                oParameters.Add(New SqlClient.SqlParameter("@LogTime", SQLDateTime(mdLogTime)))
                oParameters.Add(New SqlClient.SqlParameter("@Screen", msScreen))
                oParameters.Add(New SqlClient.SqlParameter("@URLLink", msURLLink))
                oParameters.Add(New SqlClient.SqlParameter("@ItemCategory", msItemCategory))
                oParameters.Add(New SqlClient.SqlParameter("@RecordAccessed", msRecordAccessed))
                oParameters.Add(New SqlClient.SqlParameter("@IPAddress", msIPaddress))
                oParameters.Add(New SqlClient.SqlParameter("@UserBrowser", msUserBrowser))

                DataAccess.ExecuteCommand("sp_Church_AccessLogInsertBrowserIP", oParameters)

                mbChanged = False

            End If

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miAccessLogID
            End Get
        End Property
        Public Property ModuleName() As String
            Get
                Return msModuleName
            End Get
            Set(ByVal value As String)
                If msModuleName <> value Then mbChanged = True
                msModuleName = value
            End Set
        End Property
        Public Property ScreenName() As String
            Get
                Return msScreen
            End Get
            Set(ByVal value As String)
                If msScreen <> value Then mbChanged = True
                Dim oFileInfo As System.IO.FileInfo = New System.IO.FileInfo(value)
                msScreen = oFileInfo.Name
            End Set
        End Property
        Public Property ContactID() As Integer
            Get
                Return miContactID
            End Get
            Set(ByVal value As Integer)
                If miContactID <> value Then mbChanged = True
                miContactID = value
            End Set
        End Property
        Public Property LogTime() As Date
            Get
                Return mdLogTime
            End Get
            Set(ByVal value As Date)
                If mdLogTime <> value Then mbChanged = True
                mdLogTime = value
            End Set
        End Property
        Public Property URlLink() As String
            Get
                Return msURLLink
            End Get
            Set(ByVal value As String)
                If msURLLink <> value Then mbChanged = True
                msURLLink = value
            End Set
        End Property
        Public Property ItemCategory() As String
            Get
                Return msItemCategory
            End Get
            Set(ByVal value As String)
                If msItemCategory <> value Then mbChanged = True
                msItemCategory = value
            End Set
        End Property
        Public Property RecordAccessed() As String
            Get
                Return msRecordAccessed
            End Get
            Set(ByVal value As String)
                If msRecordAccessed <> value Then mbChanged = True
                msRecordAccessed = value
            End Set
        End Property
        Public Property IPaddress() As String
            Get
                Return msIPaddress
            End Get
            Set(ByVal value As String)
                If msIPaddress <> value Then mbChanged = True
                msIPaddress = value
            End Set
        End Property
        Public Property UserBrowser() As String
            Get
                Return msUserBrowser
            End Get
            Set(ByVal value As String)
                If msUserBrowser <> value Then mbChanged = True
                msUserBrowser = value
            End Set
        End Property

        Public ReadOnly Property Changed() As Boolean
            Get
                Return mbChanged
            End Get
        End Property
        Public Sub ResetChanges() 
            mbChanged = False
        End Sub

    End Class

End Namespace