
<Serializable()> _
<CLSCompliant(True)> _
Public Class CArrayList(Of itemType)
    Inherits List(Of itemType)
    Implements ICloneable

    Private mbChanged As Boolean

    Public mcAdd As ClonableList(Of itemType)
    Public mcRemove As ClonableList(Of itemType)

    Public Sub New()

        MyBase.New()

        mcAdd = New ClonableList(Of itemType)
        mcRemove = New ClonableList(Of itemType)

    End Sub

    Public Sub New(ByVal c As System.Collections.ICollection)

        For Each Item As itemType In c
            MyBase.Add(Item)
        Next

        mcAdd = New ClonableList(Of itemType)
        mcRemove = New ClonableList(Of itemType)

    End Sub

    Public Shadows Function Add(ByVal objAdd As itemType) As Integer

        MyBase.Add(objAdd)

        If mcRemove.Contains(objAdd) Then
            mcRemove.Remove(objAdd)
        Else
            mcAdd.Add(objAdd)
        End If

        Return False
    End Function

    Public Shadows Function Remove(ByVal objRemove As itemType) As Integer

        If objRemove IsNot Nothing Then

            MyBase.Remove(objRemove)

            If mcAdd.Contains(objRemove) Then
                mcAdd.Remove(objRemove)
            Else
                mcRemove.Add(objRemove)
            End If

        End If
        Return True

    End Function

    Public Shadows Function RemoveAt(ByVal index As Integer) As Integer

        Dim objRemove As Object = MyBase.Item(index)

        If mcAdd.Contains(objRemove) Then
            mcAdd.Remove(objRemove)
        Else
            mcRemove.Add(objRemove)
        End If

        MyBase.RemoveAt(index)
        Return True

    End Function

    Public Shadows Sub Clear()
        For Each objRemove As itemType In Me.ToArray
            Remove(objRemove)
        Next
    End Sub

    Public Function GetItemByGUID(ByVal sGUID As String) As itemType
        Dim oItem As Object
        For Each oItem In Me.ToArray
            If oItem.GUID.ToString.ToLower = sGUID.ToLower Then Return oItem
        Next
        Return Nothing
    End Function
    Public Function GetItemByID(ByVal iID As Integer) As itemType
        Dim oItem As Object
        For Each oItem In Me.ToArray
            If oItem.id = iID Then Return oItem
        Next
        Return Nothing
    End Function

    Public Sub RemoveItemByGUID(ByVal sGUID As String)
        Me.Remove(GetItemByGUID(sGUID))
    End Sub
    Public Sub RemoveItemByID(ByVal iID As Integer)
        Me.Remove(GetItemByID(iID))
    End Sub

    Public Sub UpdateItemByGUID(ByVal oNewItem As itemType)
        Dim oItem As Object
        For Each oItem In Me.ToArray
            If oItem.GUID.ToString.ToLower = CType(oNewItem, Object).GUID.ToString.ToLower Then
                MyBase.Item(MyBase.IndexOf(oItem)) = oNewItem
            End If
        Next
    End Sub

    Public Sub UpdateItemByID(ByVal oNewItem As itemType)
        Dim oItem As Object
        For Each oItem In Me.ToArray
            If oItem.ID = CType(oNewItem, Object).ID Then
                MyBase.Item(MyBase.IndexOf(oItem)) = oNewItem
            End If
        Next
    End Sub


    Public ReadOnly Property Added() As ClonableList(Of itemType)
        Get
            Return mcAdd
        End Get
    End Property
    Public ReadOnly Property Removed() As ClonableList(Of itemType)
        Get
            Return mcRemove
        End Get
    End Property

    Public ReadOnly Property Changed() As Boolean
        Get
            Return mcAdd.Count > 0 Or mcRemove.Count > 0
        End Get
    End Property

    Public Sub ResetChanges()
        mcAdd.Clear()
        mcRemove.Clear()
    End Sub

    Public ReadOnly Property BaseArray() As ArrayList
        Get

            Dim oArrayList As New ArrayList
            Dim oObject As itemType
            For Each oObject In Me.ToArray
                oArrayList.Add(oObject)
            Next
            Return oArrayList

        End Get
    End Property
    Protected WriteOnly Property CloneRemoved() As List(Of itemType)
        Set(ByVal value As List(Of itemType))

        End Set
    End Property
    Function Clone() As Object Implements ICloneable.Clone

        Dim oArrayList As New CArrayList(Of itemType)
        Dim oObject As itemType
        For Each oObject In Me.ToArray
            oArrayList.Add(oObject)
        Next
        oArrayList.ResetChanges()
        Return oArrayList

    End Function

End Class

<Serializable()> _
<CLSCompliant(True)> _
Public Class ClonableList(Of itemType)
    Inherits List(Of itemType)
    Implements ICloneable

    Function Clone() As Object Implements ICloneable.Clone

        Dim oClonableList As New ClonableList(Of itemType)
        Dim oObject As itemType
        For Each oObject In Me.ToArray
            oClonableList.Add(oObject)
        Next
        Return oClonableList

    End Function

End Class