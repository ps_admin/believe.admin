
<Serializable()> _
<CLSCompliant(True)> _
Public Class CContactFamily

    Private miFamilyMemberID As Integer         'ContactID of the family member
    Private moFamilyMember As CContact
    Private miFamilyMemberTypeID As Integer
    Private miFamilyID As Integer               'ContactID used to group this family together
    Private msName As String
    Private msChurchCampus As String
    Private msChurchStatus As String
    Private msStreetAddress As String
    Private msPostalAddress As String
    Private msAge As String
    Private msPhone As String
    Private msMobile As String
    Private msEmail As String
    Private msInternalExternal As String
    Private mbUpdateStreetAddress As Boolean
    Private mbUpdatePostalAddress As Boolean
    Private mbUpdateHomePhone As Boolean
    Private mbUpdateMobilePhone As Boolean
    Private msGUID As String

    Public Sub New()

        miFamilyMemberID = 0
        miFamilyMemberTypeID = 0
        msName = String.Empty
        msChurchCampus = String.Empty
        msChurchStatus = String.Empty
        msStreetAddress = String.Empty
        msPostalAddress = String.Empty
        msPhone = String.Empty
        msMobile = String.Empty
        msEmail = String.Empty
        msInternalExternal = String.Empty
        mbUpdateStreetAddress = False
        mbUpdatePostalAddress = False
        mbUpdateHomePhone = False
        mbUpdateMobilePhone = False
        msGUID = System.Guid.NewGuid.ToString

    End Sub

    Public Sub New(ByRef oDataRow As DataRow)

        Me.New()

        miFamilyMemberID = oDataRow("FamilyMember_ID")
        miFamilyMemberTypeID = IsNull(oDataRow("FamilyMemberType_ID"), 0)
        miFamilyID = IsNull(oDataRow("Family_ID"), 0)
        msName = oDataRow("Name")
        msChurchCampus = oDataRow("ChurchCampus")
        msChurchStatus = oDataRow("ChurchStatus")
        msStreetAddress = oDataRow("StreetAddress")
        msPostalAddress = oDataRow("PostalAddress")
        msAge = oDataRow("Age")
        msPhone = oDataRow("Phone")
        msMobile = oDataRow("Mobile")
        msEmail = oDataRow("Email")
        msInternalExternal = oDataRow("InternalExternal")

    End Sub

    Public Sub Save(ByVal oContact As CContact)

        Dim oParameters As New ArrayList
        oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", miFamilyMemberID))
        oParameters.Add(New SqlClient.SqlParameter("@Family_ID", SQLNull(miFamilyID)))
        oParameters.Add(New SqlClient.SqlParameter("@FamilyMemberType_ID", SQLNull(miFamilyMemberTypeID)))
        oParameters.Add(New SqlClient.SqlParameter("@ModifiedBy_ID", SQLNull(oContact.ModifiedByID)))

        DataAccess.ExecuteCommand("sp_Common_ContactUpdateFamily2", oParameters)

    End Sub

    Public Sub SaveHomePhone(ByVal oContact As CContact)

        Dim oParameters As New ArrayList
        oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", miFamilyMemberID))
        oParameters.Add(New SqlClient.SqlParameter("@HomePhone", oContact.PhoneHome))
        oParameters.Add(New SqlClient.SqlParameter("@ModifiedBy_ID", SQLNull(oContact.ModifiedByID)))

        DataAccess.ExecuteCommand("sp_Common_ContactUpdatePhone", oParameters)

        'Update this object with the new Phone Number
        msPhone = oContact.PhoneHome

    End Sub
    Public Sub SaveMobilePhone(ByVal oContact As CContact)

        Dim oParameters As New ArrayList
        oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", miFamilyMemberID))
        oParameters.Add(New SqlClient.SqlParameter("@MobilePhone", oContact.PhoneMobile))
        oParameters.Add(New SqlClient.SqlParameter("@ModifiedBy_ID", SQLNull(oContact.ModifiedByID)))

        DataAccess.ExecuteCommand("sp_Common_ContactUpdatePhone", oParameters)

        'Update this object with the new Phone Number
        msMobile = oContact.PhoneMobile

    End Sub

    Public Sub SaveStreetAddress(ByVal oContact As CContact)

        Dim oParameters As New ArrayList
        oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", miFamilyMemberID))
        oParameters.Add(New SqlClient.SqlParameter("@Address1", oContact.Address1))
        oParameters.Add(New SqlClient.SqlParameter("@Address2", oContact.Address2))
        oParameters.Add(New SqlClient.SqlParameter("@Suburb", oContact.Suburb))
        oParameters.Add(New SqlClient.SqlParameter("@State_ID", SQLNull(oContact.StateID)))
        oParameters.Add(New SqlClient.SqlParameter("@StateOther", oContact.StateOther))
        oParameters.Add(New SqlClient.SqlParameter("@Postcode", oContact.Postcode))
        oParameters.Add(New SqlClient.SqlParameter("@Country_ID", SQLNull(oContact.CountryID)))
        oParameters.Add(New SqlClient.SqlParameter("@ModifiedBy_ID", SQLNull(oContact.ModifiedByID)))

        DataAccess.ExecuteCommand("sp_Common_ContactUpdateStreetAddress", oParameters)

        'Update this object with the new Address
        msStreetAddress = Replace(oContact.ContactAddress, vbCrLf, ", ")

    End Sub

    Public Sub SavePostalAddress(ByVal oContact As CContact)

        Dim oParameters As New ArrayList
        oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", miFamilyMemberID))
        oParameters.Add(New SqlClient.SqlParameter("@PostalAddress1", oContact.Address1))
        oParameters.Add(New SqlClient.SqlParameter("@PostalAddress2", oContact.Address2))
        oParameters.Add(New SqlClient.SqlParameter("@PostalSuburb", oContact.Suburb))
        oParameters.Add(New SqlClient.SqlParameter("@PostalState_ID", SQLNull(oContact.StateID)))
        oParameters.Add(New SqlClient.SqlParameter("@PostalStateOther", oContact.StateOther))
        oParameters.Add(New SqlClient.SqlParameter("@PostalPostcode", oContact.Postcode))
        oParameters.Add(New SqlClient.SqlParameter("@PostalCountry_ID", SQLNull(oContact.CountryID)))
        oParameters.Add(New SqlClient.SqlParameter("@ModifiedBy_ID", SQLNull(oContact.ModifiedByID)))

        DataAccess.ExecuteCommand("sp_Common_ContactInternalUpdatePostalAddress", oParameters)

        'Update this object with the new Address
        msPostalAddress = Replace(oContact.Internal.ContactPostalAddress, vbCrLf, ", ")

    End Sub

    Public Property ID() As Integer
        Get
            Return miFamilyMemberID
        End Get
        Set(ByVal Value As Integer)
            miFamilyMemberID = Value
        End Set
    End Property
    Public Function GetFamilyMember() As CContact

        If moFamilyMember Is Nothing Then
            moFamilyMember = New CContact
            moFamilyMember.LoadByID(miFamilyMemberID)
        End If
        Return moFamilyMember

    End Function
    Public Sub UpdateFamilyMember(ByVal oContact As CContact)

        moFamilyMember = oContact

    End Sub
    Public Property FamilyMemberTypeID() As Integer
        Get
            Return miFamilyMemberTypeID
        End Get
        Set(ByVal Value As Integer)
            miFamilyMemberTypeID = Value
        End Set
    End Property
    Public Property FamilyID() As Integer
        Get
            Return miFamilyID
        End Get
        Set(ByVal Value As Integer)
            miFamilyID = Value
        End Set
    End Property
    Public ReadOnly Property FamilyMemberType() As CFamilyMemberType
        Get
            Return Lists.GetFamilyMemberTypeByID(miFamilyMemberTypeID)
        End Get
    End Property

    Public ReadOnly Property Name() As String
        Get
            Return msName
        End Get
    End Property
    Public ReadOnly Property ChurchCampus() As String
        Get
            Return msChurchCampus
        End Get
    End Property
    Public ReadOnly Property ChurchStatus() As String
        Get
            Return msChurchStatus
        End Get
    End Property
    Public ReadOnly Property StreetAddress() As String
        Get
            Return msStreetAddress
        End Get
    End Property
    Public ReadOnly Property PostalAddress() As String
        Get
            Return msPostalAddress
        End Get
    End Property
    Public ReadOnly Property Age() As String
        Get
            Return msAge
        End Get
    End Property
    Public ReadOnly Property Phone() As String
        Get
            Return msPhone
        End Get
    End Property
    Public ReadOnly Property Mobile() As String
        Get
            Return msMobile
        End Get
    End Property
    Public ReadOnly Property Email() As String
        Get
            Return msEmail
        End Get
    End Property
    Public ReadOnly Property InternalExternal() As String
        Get
            Return msInternalExternal
        End Get
    End Property
    Public Property UpdateStreetAddress() As Boolean
        Get
            Return mbUpdateStreetAddress
        End Get
        Set(ByVal value As Boolean)
            mbUpdateStreetAddress = value
        End Set
    End Property
    Public Property UpdatePostalAddress() As Boolean
        Get
            Return mbUpdatePostalAddress
        End Get
        Set(ByVal value As Boolean)
            mbUpdatePostalAddress = value
        End Set
    End Property
    Public Property UpdateHomePhone() As Boolean
        Get
            Return mbUpdateHomePhone
        End Get
        Set(ByVal value As Boolean)
            mbUpdateHomePhone = value
        End Set
    End Property
    Public Property UpdateMobilePhone() As Boolean
        Get
            Return mbUpdateMobilePhone
        End Get
        Set(ByVal value As Boolean)
            mbUpdateMobilePhone = value
        End Set
    End Property
    Public ReadOnly Property GUID() As String
        Get
            Return msGUID
        End Get
    End Property

End Class