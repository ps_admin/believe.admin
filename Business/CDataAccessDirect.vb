Imports System
Imports System.Net
Imports System.Net.Mail
Imports System.Configuration
Imports Planetshakers.Business.Planetshakers.Business.Helpers

<CLSCompliant(True)>
Public Class CDataAccessDirect
    Inherits CDataAccess

    Private moDataAccess As New DataAccess.CPlanetshakersDataAccess

    Public Sub New()

        moDataAccess = New DataAccess.CPlanetshakersDataAccess

    End Sub

    Public Overrides Function GetDataSet(ByVal sSQL As String, ByRef oTableMappings As ArrayList) As DataSet

        Dim dNow As Date = System.DateTime.Now
        Dim oDataSet As DataSet = Nothing

        oDataSet = moDataAccess.GetDataSet(sSQL, oTableMappings)

        Debug.Print("DataSet - Duration: " & Now.Subtract(dNow).TotalSeconds & ", SQL: " & sSQL)
        Return oDataSet

    End Function

    Public Overrides Function GetDataSet(ByVal sSQL As String, ByRef oTableMappings As ArrayList, ByRef oParameters As ArrayList) As DataSet

        Dim dNow As Date = System.DateTime.Now
        Dim oDataSet As DataSet = Nothing

        oDataSet = moDataAccess.GetDataSet(sSQL, oTableMappings, oParameters)

        Debug.Print("DataSet - Duration: " & Now.Subtract(dNow).TotalSeconds & ", SQL: " & sSQL & " " & GetParameters(oParameters))
        Return oDataSet

    End Function

    Public Overrides Function GetDataTable(ByVal sSQL As String) As DataTable

        Dim dNow As Date = System.DateTime.Now
        Dim oDataTable As DataTable = Nothing

        oDataTable = moDataAccess.GetDataTable(sSQL)

        Debug.Print("DataTable - Duration: " & Now.Subtract(dNow).TotalSeconds & ", SQL: " & sSQL)
        Return oDataTable

    End Function

    Public Overrides Function GetDataTable(ByVal sSQL As String, ByRef oParameters As ArrayList) As DataTable

        Dim dNow As Date = System.DateTime.Now
        Dim oDataTable As DataTable = Nothing

        oDataTable = moDataAccess.GetDataTable(sSQL, oParameters)

        Debug.Print("DataTable - Duration: " & Now.Subtract(dNow).TotalSeconds & ", SQL: " & sSQL & " " & GetParameters(oParameters))
        Return oDataTable

    End Function

    Public Overrides Function GetValue(ByVal sSQL As String) As Object

        Dim dNow As Date = System.DateTime.Now
        Dim oValue As Object = Nothing

        oValue = moDataAccess.GetValue(sSQL)

        Debug.Print("GetValue - Duration: " & Now.Subtract(dNow).TotalSeconds & ", SQL: " & sSQL)
        Return oValue

    End Function
    Public Overrides Function GetValue(ByVal sSQL As String, ByRef oParameters As ArrayList) As Object

        Dim dNow As Date = System.DateTime.Now
        Dim oValue As Object = Nothing

        oValue = moDataAccess.GetValue(sSQL, oParameters)

        Debug.Print("GetValue - Duration: " & Now.Subtract(dNow).TotalSeconds & ", SQL: " & sSQL & " " & GetParameters(oParameters))
        Return oValue

    End Function

    Public Overrides Sub ExecuteCommand(ByVal sSQL As String)

        Dim dNow As Date = System.DateTime.Now

        moDataAccess.ExecuteCommand(sSQL)

        Debug.Print("ExecuteCommand - Duration: " & Now.Subtract(dNow).TotalSeconds & ", SQL: " & sSQL)

    End Sub

    Public Overrides Sub ExecuteCommand(ByVal sSQL As String, ByRef oParameters As ArrayList)

        Dim dNow As Date = System.DateTime.Now

        moDataAccess.ExecuteCommand(sSQL, oParameters)

        Debug.Print("ExecuteCommand - Duration: " & Now.Subtract(dNow).TotalSeconds & ", SQL: " & sSQL & " " & GetParameters(oParameters))

    End Sub

    Public Overrides Function SendEmail(ByVal sToAddress As String, ByVal sToName As String, ByVal sSubject As String, ByVal sBody As String, ByVal oAttachments As ArrayList, ByRef sError As String) As Boolean

        Dim sgApi As String = AzureServices.KeyVault_RetreiveSecret("believe-apps", "SendGridAPIKey")
        Dim oSmtpClient As SmtpClient = New SmtpClient
        Dim basicCredential As NetworkCredential = New NetworkCredential("apikey", sgApi)
        Dim oMessage As MailMessage = New MailMessage()
        Dim oFromAddress As MailAddress = New MailAddress("no-reply@planetshakers.com", "Planetshakers Team")


        If String.IsNullOrEmpty(ConfigurationManager.AppSettings("smtpHost")) Then
            oSmtpClient.EnableSsl = True
            oSmtpClient.Port = 587

            oSmtpClient.Host = "smtp.sendgrid.net"
            oSmtpClient.UseDefaultCredentials = False
        Else
            oSmtpClient.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings("smtpEnableSSL"))
            oSmtpClient.Port = Convert.ToInt32(ConfigurationManager.AppSettings("smtpPort"))

            oSmtpClient.Host = ConfigurationManager.AppSettings("smtpHost")
            oSmtpClient.UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings("smtpUseDefaultCredentials"))
        End If

        oSmtpClient.Credentials = basicCredential

        oMessage.From = oFromAddress
        oMessage.Subject = sSubject

        oMessage.IsBodyHtml = True
        oMessage.Body = sBody
        oMessage.To.Add(New MailAddress(sToAddress, sToName))

        For Each sAttachment As String() In oAttachments
            Dim fs As New System.IO.FileStream(sAttachment(0), IO.FileMode.Open, IO.FileAccess.Read)
            Dim oAttachment As New Attachment(fs, sAttachment(1))
            oAttachment.ContentId = sAttachment(1)
            oMessage.Attachments.Add(oAttachment)
        Next

        Try
            oSmtpClient.Send(oMessage)
            Return True
        Catch ex As Exception
            sError = ex.Message
            Return False
        Finally
            oMessage.Dispose()
        End Try

    End Function

    Public Overrides Function ProcessCreditCard(ByVal sCard As String, ByVal sExpiry As String, ByVal sCVC2 As String, ByVal sName As String, ByVal dAmount As Decimal, ByRef sComment As String, ByRef sBankAccountClientID As String, ByRef sBankAccountCertificateName As String, ByRef sBankAccountCertificatePassPhrase As String, ByRef sTxRef As String, ByRef sResponse As String) As Boolean
        Return CreditCardProcessing.ProcessCreditCard(sCard, sExpiry, sCVC2, sName, dAmount, sComment, sBankAccountClientID, sBankAccountCertificateName, sBankAccountCertificatePassPhrase, sTxRef, sResponse)
    End Function

    Public Overrides Function ProcessRefund(ByVal sName As String, ByVal sCCTransactionRef As String, ByVal dAmount As Decimal, ByRef sComment As String, ByRef sBankAccountClientID As String, ByRef sBankAccountCertificateName As String, ByRef sBankAccountCertificatePassPhrase As String, ByRef sTxRef As String, ByRef sResponse As String) As Boolean
        Return CreditCardProcessing.ProcessRefund(sName, sCCTransactionRef, dAmount, sComment, sBankAccountCertificateName, sBankAccountCertificateName, sBankAccountCertificatePassPhrase, sTxRef, sResponse)
    End Function

    Public Overrides Property ConnectionString() As String
        Get
            Return moDataAccess.ConnectionString
        End Get
        Set(ByVal value As String)
            moDataAccess.ConnectionString = value
        End Set
    End Property

    Public Overrides WriteOnly Property UserID() As Integer
        Set(ByVal value As Integer)
            moDataAccess.UserID = value
        End Set
    End Property

    Private Function GetParameters(ByVal oParameters As ArrayList) As String

        Dim oParameter As SqlClient.SqlParameter
        Dim sOutput As String = String.Empty

        For Each oParameter In oParameters
            If oParameter.DbType = DbType.Binary Then
                sOutput += oParameter.ParameterName & "= (binary), "
            Else
                'commented to fix issue:I0273
                'sOutput += oParameter.ParameterName & "= " & oParameter.Value.ToString() & ", "
                sOutput += oParameter.ParameterName & "= " & oParameter.Value & ", "
            End If
        Next

        Return sOutput

    End Function
End Class