Imports System.Text.RegularExpressions

<CLSCompliant(True)> _
Public Class CDataAccessWeb
    Inherits CDataAccess

    Private Function WebDataAccess() As Planetshakers.Events.WebService.PlanetshakersWebService
        Dim oDataAccess As New Planetshakers.Events.WebService.PlanetshakersWebService

        Dim oAuth As New Planetshakers.Events.WebService.WebServiceAuthHeader
        oAuth.UserName = "{2BA6A61F-D3F8-4186-A565-B0106B521EB5}"
        oAuth.Password = "{A7BF2F55-8E50-4d25-B059-F6E33671A8FF}"

        oDataAccess.WebServiceAuthHeaderValue = oAuth

        Return oDataAccess

    End Function

    Public Overrides Function GetDataSet(ByVal sSQL As String, ByRef oTableMappings As ArrayList) As DataSet

        Dim dNow As Date = System.DateTime.Now
        Dim oDataSet As DataSet = Nothing
        Dim sString As String = String.Empty

        'Web Services don't like passing array lists. We'll convert the Table Mapping ArrayList to an Array
        sString = WebDataAccess.GetDataSet(sSQL, ConvertTableMappingsToStringArray(oTableMappings))
        oDataSet = ConvertStringToDataSet(sString)

        Debug.Print("DataSet - Duration: " & Now.Subtract(dNow).TotalSeconds & ", SQL: " & sSQL & ", Len: " & sString.Length)
        Return oDataSet

    End Function

    Public Overrides Function GetDataSet(ByVal sSQL As String, ByRef oTableMappings As ArrayList, ByRef oParameters As ArrayList) As DataSet

        Dim dNow As Date = System.DateTime.Now
        Dim oDataSet As DataSet = Nothing
        Dim sString As String = String.Empty

        'Web Services don't like passing array lists. We'll convert the Table Mapping ArrayList to an Array
        oDataSet = ConvertStringToDataSet(WebDataAccess.GetDataSetParameters(sSQL, _
            ConvertTableMappingsToStringArray(oTableMappings), _
            ConvertSQLParametersToStringArray(oParameters)))

        Debug.Print("DataSet - Duration: " & Now.Subtract(dNow).TotalSeconds & ", SQL: " & sSQL & ", Len: " & sString.Length)
        Return oDataSet

    End Function

    Public Overrides Function GetDataTable(ByVal sSQL As String) As DataTable

        Dim dNow As Date = System.DateTime.Now
        Dim oDataTable As DataTable = Nothing
        Dim sString As String = String.Empty

        oDataTable = ConvertStringToDataTable(WebDataAccess.GetDataTable(sSQL))

        Debug.Print("DataTable - Duration: " & Now.Subtract(dNow).TotalSeconds & ", SQL: " & sSQL & ", Len: " & sString.Length)
        Return oDataTable

    End Function

    Public Overrides Function GetDataTable(ByVal sSQL As String, ByRef oParameters As ArrayList) As DataTable

        Dim dNow As Date = System.DateTime.Now
        Dim oDataTable As DataTable = Nothing
        Dim sString As String = String.Empty

        oDataTable = ConvertStringToDataTable(WebDataAccess.GetDataTableParameters(sSQL, ConvertSQLParametersToStringArray(oParameters)))

        Debug.Print("DataTable - Duration: " & Now.Subtract(dNow).TotalSeconds & ", SQL: " & sSQL & ", Len: " & sString.Length)
        Return oDataTable

    End Function

    Public Overrides Function GetValue(ByVal sSQL As String) As Object

        Dim dNow As Date = System.DateTime.Now
        Dim oValue As Object = Nothing

        oValue = WebDataAccess.GetValue(sSQL)

        Debug.Print("GetValue - Duration: " & Now.Subtract(dNow).TotalSeconds & ", SQL: " & sSQL)
        Return oValue

    End Function

    Public Overrides Function GetValue(ByVal sSQL As String, ByRef oParameters As ArrayList) As Object

        Dim dNow As Date = System.DateTime.Now
        Dim oValue As Object = Nothing

        oValue = WebDataAccess.GetValueParameters(sSQL, ConvertSQLParametersToStringArray(oParameters))

        Debug.Print("GetValue - Duration: " & Now.Subtract(dNow).TotalSeconds & ", SQL: " & sSQL)
        Return oValue

    End Function

    Public Overrides Sub ExecuteCommand(ByVal sSQL As String)

        Dim dNow As Date = System.DateTime.Now

        WebDataAccess.ExecuteCommand(sSQL)

        Debug.Print("ExecuteCommand - Duration: " & Now.Subtract(dNow).TotalSeconds & ", SQL: " & sSQL)

    End Sub

    Public Overrides Sub ExecuteCommand(ByVal sSQL As String, ByRef oParameters As ArrayList)

        Dim dNow As Date = System.DateTime.Now

        WebDataAccess.ExecuteCommandParameters(sSQL, ConvertSQLParametersToStringArray(oParameters))

        Debug.Print("ExecuteCommand - Duration: " & Now.Subtract(dNow).TotalSeconds & ", SQL: " & sSQL)

    End Sub

    Public Overrides Function SendEmail(ByVal sToAddress As String, ByVal sToName As String, ByVal sSubject As String, ByVal sBody As String, ByVal oAttachments As ArrayList, ByRef sError As String) As Boolean

        Return WebDataAccess.SendEmailNew(sToAddress, sToName, sSubject, sBody, sError)

    End Function

    Public Overrides Function ProcessCreditCard(ByVal sCard As String, ByVal sExpiry As String, ByVal sCVC2 As String, ByVal sName As String, ByVal dAmount As Decimal, ByRef sComment As String, ByRef sBankAccountClientID As String, ByRef sBankAccountCertificateName As String, ByRef sBankAccountCertificatePassPhrase As String, ByRef sTxRef As String, ByRef sResponse As String) As Boolean

        Return WebDataAccess.ProcessCreditCard(sCard, sExpiry, sCVC2, sName, dAmount, sComment, sBankAccountClientID, sBankAccountCertificateName, sBankAccountCertificatePassPhrase, sTxRef, sResponse)

    End Function

    Public Overrides Function ProcessRefund(ByVal sName As String, ByVal sCCTransactionRef As String, ByVal dAmount As Decimal, ByRef sComment As String, ByRef sBankAccountClientID As String, ByRef sBankAccountCertificateName As String, ByRef sBankAccountCertificatePassPhrase As String, ByRef sTxRef As String, ByRef sResponse As String) As Boolean

        Return WebDataAccess.ProcessRefund(sName, sCCTransactionRef, dAmount, sComment, sBankAccountClientID, sBankAccountCertificateName, sBankAccountCertificatePassPhrase, sTxRef, sResponse)

    End Function

    Public Overrides Property ConnectionString() As String
        Get
            Return String.Empty
        End Get
        Set(ByVal value As String)
        End Set
    End Property

    Public Overrides WriteOnly Property UserID() As Integer
        Set(ByVal value As Integer)
        End Set
    End Property

    Private Function ConvertStringToDataTable(ByVal xmlString As String) As DataTable

        If xmlString Is Nothing Then Return Nothing

        xmlString = AdjustDateTimeValues(xmlString)

        Dim oDataTable As New DataTable
        Dim oStringReader As System.IO.StringReader = New System.IO.StringReader(xmlString)
        oDataTable.ReadXml(oStringReader)
        Return oDataTable

    End Function

    Private Function ConvertStringToDataSet(ByVal xmlString As String) As DataSet

        If xmlString Is Nothing Then Return Nothing

        xmlString = AdjustDateTimeValues(xmlString)

        Dim oDataSet As New DataSet
        Dim oStringReader As System.IO.StringReader = New System.IO.StringReader(xmlString)
        oDataSet.ReadXml(oStringReader)
        Return oDataSet

    End Function

    Private Function ConvertTableMappingsToStringArray(ByVal oTableMappings As ArrayList) As String()

        Dim sString(0) As String

        If oTableMappings IsNot Nothing AndAlso oTableMappings.Count > 0 Then
            ReDim sString(oTableMappings.Count * 2)
            Dim oTableMapping As System.Data.Common.DataTableMapping
            Dim iCount As Integer
            For Each oTableMapping In oTableMappings
                iCount += 1
                sString(iCount) = oTableMapping.SourceTable
                iCount += 1
                sString(iCount) = oTableMapping.DataSetTable
            Next
        End If
        Return sString

    End Function

    Private Function ConvertSQLParametersToStringArray(ByVal oSQLParameters As ArrayList) As String()

        Dim sString(0) As String

        If oSQLParameters IsNot Nothing AndAlso oSQLParameters.Count > 0 Then
            ReDim sString(oSQLParameters.Count * 3)
            Dim oSQLParameter As SqlClient.SqlParameter
            Dim iCount As Integer
            For Each oSQLParameter In oSQLParameters
                iCount += 1
                sString(iCount) = oSQLParameter.ParameterName
                iCount += 1
                'We need to especially encode the DBNull value, because we can't store it in a string
                If oSQLParameter.Value Is DBNull.Value Then
                    sString(iCount) = "System.DBNull.Value"
                Else
                    sString(iCount) = """" & oSQLParameter.Value & """"
                End If
                iCount += 1
                sString(iCount) = oSQLParameter.DbType
            Next
        End If
        Return sString

    End Function

    Private Function ConvertStringArrayToSQLParameters(ByVal sString() As String) As ArrayList

        Dim oArrayList As ArrayList = New ArrayList

        If sString.Length > 1 Then
            Dim iCount As Integer
            For iCount = 1 To sString.Length - 1 Step 3
                Dim oSQLParameter As New SqlClient.SqlParameter
                oSQLParameter.ParameterName = sString(iCount)
                If sString(iCount + 1) = "System.DBNull.Value" Then
                    oSQLParameter.Value = System.DBNull.Value
                Else
                    oSQLParameter.Value = sString(iCount + 1).Substring(1, Len(sString(iCount + 1)) - 2)
                End If
                oSQLParameter.DbType = sString(iCount + 2)
                oArrayList.Add(oSQLParameter)
            Next
        End If

        Return oArrayList

    End Function

    Private Function AdjustDateTimeValues(ByVal xmlData As String) As String
        ' Search for DateTime values of formats like the following
        ' --> 2006-08-22T00:00:00.0000000-05:00
        ' --> 2006-08-22T00:00:00.0000000-00:00
        ' --> 2006-08-22T00:00:00.0000000+05:00
        Dim rp As String = "(?<DATE>\d{4}-\d{2}-\d{2})(?<TIME>T\d{2}:\d{2}:\d{2}(.\d{2,7})?)(?<HOUR>[+-]\d{2})(?<LAST>:\d{2})"

        ' Replace UTC offset value
        Dim fixedString As String = Regex.Replace(xmlData, rp, _
            New MatchEvaluator(AddressOf GetHourOffset))

        Return fixedString
    End Function

    Private Function GetHourOffset(ByVal m As Match) As String
        ' Need to also account for Daylights Savings
        ' Time when calculating UTC offset value
        Dim dtLocal As DateTime = DateTime.Parse(m.Result("${DATE}"))
        Dim dtUTC As DateTime = dtLocal.ToUniversalTime
        Dim hourLocalOffset As Integer = CInt(DateTime.op_Subtraction(dtLocal, dtUTC).TotalHours)

        Dim hourServer As Integer = Integer.Parse(m.Result("${HOUR}"))
        'Dim newHour As String = (hourServer + _
        '      (hourLocalOffset - hourServer)).ToString("+0#;-0#;-0#")
        Dim newHour As String = hourLocalOffset.ToString("+0#;-0#;-0#")
        Dim retString As String = m.Result(("${DATE}" + ("${TIME}" _
                                + (newHour + "${LAST}"))))
        Return retString

    End Function
End Class