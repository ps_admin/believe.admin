<Serializable()> _
<CLSCompliant(True)> _
Public Class CFamilyMemberType

    Private miFamilyMemberTypeID As Integer
    Private msName As String
    Private miSortOrder As Integer

    Public Sub New()

        miFamilyMemberTypeID = 0
        msName = String.Empty
        miSortOrder = 0

    End Sub

    Public Sub New(ByRef oDataRow As DataRow)

        Me.New()

        miFamilyMemberTypeID = oDataRow("FamilyMemberType_ID")
        msName = oDataRow("Name")
        miSortOrder = oDataRow("SortOrder")

    End Sub

    Public ReadOnly Property ID() As Integer
        Get
            Return miFamilyMemberTypeID
        End Get
    End Property
    Public Property Name() As String
        Get
            Return msName
        End Get
        Set(ByVal Value As String)
            msName = Value
        End Set
    End Property
    Public Property SortOrder() As Integer
        Get
            Return miSortOrder
        End Get
        Set(ByVal Value As Integer)
            miSortOrder = Value
        End Set
    End Property

End Class