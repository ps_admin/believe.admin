
<Serializable()> _
<CLSCompliant(True)> _
Public Class CMailingListType

    Private miMailingListTypeID As Integer
    Private msName As String

    Public Sub New()

        miMailingListTypeID = 0
        msName = String.Empty

    End Sub

    Public Sub New(ByRef oDataRow As DataRow)

        Me.New()

        miMailingListTypeID = oDataRow("MailingListType_ID")
        msName = oDataRow("Name")

    End Sub

    Public ReadOnly Property ID() As Integer
        Get
            Return miMailingListTypeID
        End Get
    End Property
    Public Property Name() As String
        Get
            Return msName
        End Get
        Set(ByVal Value As String)
            msName = Value
        End Set
    End Property

    Public Overrides Function toString() As String
        Return msName
    End Function

End Class