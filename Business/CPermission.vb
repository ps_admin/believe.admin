﻿<Serializable()> _
<CLSCompliant(True)>
Public Class CPermission

    Protected miDatabaseObjectID As Integer
    Protected mbRead As Boolean
    Protected mbWrite As Boolean
    Protected mbChanged As Boolean

    Public Sub New()
        miDatabaseObjectID = 0
        mbRead = False
        mbWrite = False
        mbChanged = False
    End Sub

    Public Property DatabaseObjectID() As Integer
        Get
            Return miDatabaseObjectID
        End Get
        Set(value As Integer)
            If value <> miDatabaseObjectID Then mbChanged = True
            miDatabaseObjectID = value
        End Set
    End Property

    Public ReadOnly Property DatabaseObject() As CDatabaseObject
        Get
            Return Lists.GetDatabaseObjectByID(DatabaseObjectID)
        End Get
    End Property

    Public Property Read() As Boolean
        Get
            Return mbRead
        End Get
        Set(value As Boolean)
            If value <> mbRead Then mbChanged = True
            mbRead = value
        End Set
    End Property

    Public Property Write() As Boolean
        Get
            Return mbWrite
        End Get
        Set(value As Boolean)
            If value <> mbWrite Then mbChanged = True
            mbWrite = value
        End Set
    End Property

End Class
