
<Serializable()> _
<CLSCompliant(True)> _
Public Class CSalutation

    Private miSalutationID As Integer
    Private msName As String
    Private miSortOrder As Integer

    Public Sub New()

        miSalutationID = 0
        msName = ""
        miSortOrder = 0

    End Sub

    Public Sub New(ByRef oDataRow As DataRow)

        Me.New()

        miSalutationID = oDataRow("Salutation_ID")
        msName = oDataRow("Salutation")
        miSortOrder = oDataRow("SortOrder")

    End Sub

    Public ReadOnly Property ID() As Integer
        Get
            Return miSalutationID
        End Get
    End Property
    Public Property Name() As String
        Get
            Return msName
        End Get
        Set(ByVal Value As String)
            msName = Value
        End Set
    End Property
    Public Property SortOrder() As Integer
        Get
            Return miSortOrder
        End Get
        Set(ByVal Value As Integer)
            miSortOrder = Value
        End Set
    End Property

    Public Overrides Function toString() As String
        Return msName
    End Function

End Class