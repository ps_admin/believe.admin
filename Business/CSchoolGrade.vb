
<Serializable()> _
<CLSCompliant(True)> _
Public Class CSchoolGrade

    Private miSchoolGradeID As Integer
    Private msName As String
    Private msShortName As String
    Private miSortOrder As Integer

    Public Sub New()

        miSchoolGradeID = 0
        msName = String.Empty
        msShortName = String.Empty
        miSortOrder = 0

    End Sub

    Public Sub New(ByRef oDataRow As DataRow)

        Me.New()

        miSchoolGradeID = oDataRow("SchoolGrade_ID")
        msName = oDataRow("Grade")
        msShortName = oDataRow("ShortGrade")
        miSortOrder = oDataRow("SortOrder")

    End Sub

    Property ID() As Integer
        Get
            Return miSchoolGradeID
        End Get
        Set(ByVal Value As Integer)
            miSchoolGradeID = Value
        End Set
    End Property

    Property Name() As String
        Get
            Return msName
        End Get
        Set(ByVal Value As String)
            msName = Value
        End Set
    End Property

    Property ShortName() As String
        Get
            Return msShortName
        End Get
        Set(ByVal Value As String)
            msShortName = Value
        End Set
    End Property

    Property SortOrder() As Integer
        Get
            Return miSortOrder
        End Get
        Set(ByVal value As Integer)
            miSortOrder = value
        End Set
    End Property

End Class
