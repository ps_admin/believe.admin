
<Serializable()> _
<CLSCompliant(True)> _
Public Class CUser

    Private miUserID As Integer
    Private moContact As CContact
    Private msFirstName As String
    Private msLastName As String
    Private msEmail As String
    Private msPassword As String
    Private miStateID As Integer
    Private miCountryID As Integer
    Private miCampusID As Integer
    Private mdLastPasswordChangeDate As Date
    Private miInvalidPasswordAttempts As Integer
    Private mbPasswordResetRequired As Boolean
    Private mbInactive As Boolean
    Private mcDatabaseRoles As CArrayList(Of CContactDatabaseRole)
    Private mcContactPermissions As CArrayList(Of CContactRolePermission)

    Private moMixedPermissions As Hashtable

    Private mbChanged As Boolean

    Public Sub New()

        miUserID = 0
        msFirstName = String.Empty
        msLastName = String.Empty
        msEmail = String.Empty
        msPassword = String.Empty
        miStateID = 0
        miCountryID = 0
        miCampusID = 0
        mdLastPasswordChangeDate = Nothing
        miInvalidPasswordAttempts = 0
        mbPasswordResetRequired = True
        mbInactive = False
        mcDatabaseRoles = New CArrayList(Of CContactDatabaseRole)
        mcContactPermissions = New CArrayList(Of CContactRolePermission)

        mbChanged = False

    End Sub

    Public Sub New(ByRef oDataRow As DataRow)
        Me.New()
        LoadDataRow(oDataRow)
    End Sub

    Public Sub LoadDataRowSocial(ByRef oDataRow As DataRow)

        miUserID = oDataRow("Contact_ID")
        msFirstName = oDataRow("FirstName")
        msLastName = oDataRow("LastName")
        msEmail = oDataRow("Email")
        msPassword = oDataRow("Password")
        miStateID = IsNull(oDataRow("State_ID"), 0)
        miCountryID = IsNull(oDataRow("Country_ID"), 0)

    End Sub
    Public Sub LoadDataRow(ByRef oDataRow As DataRow)

        miUserID = oDataRow("Contact_ID")
        msFirstName = oDataRow("FirstName")
        msLastName = oDataRow("LastName")
        msEmail = oDataRow("Email")
        msPassword = oDataRow("Password")
        miStateID = IsNull(oDataRow("State_ID"), 0)
        miCountryID = IsNull(oDataRow("Country_ID"), 0)
        miCampusID = IsNull(oDataRow("Campus_ID"), 0)
        mdLastPasswordChangeDate = IsNull(oDataRow("LastPasswordChangeDate"), Nothing)
        miInvalidPasswordAttempts = oDataRow("InvalidPasswordAttempts")
        mbPasswordResetRequired = oDataRow("PasswordResetRequired")
        mbInactive = oDataRow("Inactive")

        For Each oDatabaseRoleRow As DataRow In oDataRow.GetChildRows("DatabaseRole")
            mcDatabaseRoles.Add(New CContactDatabaseRole(oDatabaseRoleRow))
        Next

        For Each oContactRolePermissionRow As DataRow In oDataRow.GetChildRows("ContactRolePermission")
            mcContactPermissions.Add(New CContactRolePermission(oContactRolePermissionRow))
        Next

        mcDatabaseRoles.ResetChanges()

        mbChanged = False
    End Sub

    Public Function Load(ByVal sEmail As String) As Boolean

        Dim oDataSet As DataSet
        Dim dataset As DataSet
        Dim TableMappings As New ArrayList

        Dim oTableMappings As New ArrayList
        oTableMappings.Add(New Common.DataTableMapping("Table", "User"))
        oTableMappings.Add(New Common.DataTableMapping("Table1", "DatabaseRole"))
        oTableMappings.Add(New Common.DataTableMapping("Table2", "ContactRolePermission"))

        oDataSet = DataAccess.GetDataSet("EXEC sp_Common_UserReadByEmail '" & sEmail & "'", oTableMappings)

        oDataSet.Relations.Add("DatabaseRole", oDataSet.Tables("User").Columns("Contact_ID"), _
                oDataSet.Tables("DatabaseRole").Columns("Contact_ID"))

        oDataSet.Relations.Add("ContactRolePermission", oDataSet.Tables("User").Columns("Contact_ID"),
                oDataSet.Tables("ContactRolePermission").Columns("Contact_ID"))

        TableMappings.Add(New Common.DataTableMapping("Table", "User"))
        TableMappings.Add(New Common.DataTableMapping("Table1", "DatabaseRole"))
        TableMappings.Add(New Common.DataTableMapping("Table2", "ContactRolePermission"))
        If oDataSet.Tables("User").Rows.Count = 0 Then
            dataset = DataAccess.GetDataSet("EXEC sp_Common_ContactByEmail '" & sEmail & "'", TableMappings)
            If dataset.Tables("User").Rows.Count > 0 Then
                Dim oDataRow As DataRow = dataset.Tables("User").Rows(0)
                LoadDataRowSocial(oDataRow)
                Return False
            End If
        Else
            Dim oDataRow As DataRow = oDataSet.Tables("User").Rows(0)
            LoadDataRow(oDataRow)
            Return True
        End If
        Return False

    End Function

    Public Function Load(ByVal iContactID As Integer) As Boolean

        Dim oDataSet As DataSet

        Dim oTableMappings As New ArrayList
        oTableMappings.Add(New Common.DataTableMapping("Table", "User"))
        oTableMappings.Add(New Common.DataTableMapping("Table1", "DatabaseRole"))
        oTableMappings.Add(New Common.DataTableMapping("Table2", "ContactRolePermission"))

        oDataSet = DataAccess.GetDataSet("EXEC sp_Common_UserReadByID " & iContactID.ToString, oTableMappings)

        oDataSet.Relations.Add("DatabaseRole", oDataSet.Tables("User").Columns("Contact_ID"), _
                oDataSet.Tables("DatabaseRole").Columns("Contact_ID"))

        oDataSet.Relations.Add("ContactRolePermission", oDataSet.Tables("User").Columns("Contact_ID"), _
                oDataSet.Tables("ContactRolePermission").Columns("Contact_ID"))

        If oDataSet.Tables("User").Rows.Count = 0 Then
            Return False
        Else
            Dim oDataRow As DataRow = oDataSet.Tables("User").Rows(0)
            LoadDataRow(oDataRow)
            Return True
        End If

    End Function


    Public Sub Save(ByVal oUser As CUser)

        If mbChanged Then

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", miUserID))
            oParameters.Add(New SqlClient.SqlParameter("@LastPasswordChangeDate", SQLDateTime(mdLastPasswordChangeDate)))
            oParameters.Add(New SqlClient.SqlParameter("@InvalidPasswordAttempts", miInvalidPasswordAttempts))
            oParameters.Add(New SqlClient.SqlParameter("@PasswordResetRequired", mbPasswordResetRequired))
            oParameters.Add(New SqlClient.SqlParameter("@Inactive", mbInactive))

            DataAccess.ExecuteCommand("sp_Common_UserDetailUpdate", oParameters)

            mbChanged = False

        End If

        SaveDatabaseRoles(oUser)
        SaveContactPermissions(oUser)
        mcDatabaseRoles.ResetChanges()

    End Sub

    Private Sub SaveDatabaseRoles(ByVal oUser As CUser)

        Dim oParameters As ArrayList
        Dim oContactDatabaseRole As CContactDatabaseRole

        'Remove the Deleted Database Roles
        For Each oContactDatabaseRole In mcDatabaseRoles.Removed
            oParameters = New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@ContactDatabaseRole_ID", oContactDatabaseRole.ID))
            oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))
            DataAccess.ExecuteCommand("sp_Common_ContactDatabaseRoleDelete", oParameters)
        Next

        For Each oContactDatabaseRole In mcDatabaseRoles
            oContactDatabaseRole.Save(Me.ID, oUser)
        Next

    End Sub

    Private Sub SaveContactPermissions(ByRef oUser As CUser)

        Dim oParameters As ArrayList
        Dim oContactRolePermission As CContactRolePermission

        'Remove the Deleted Contact Permissions
        For Each oContactRolePermission In mcContactPermissions.Removed
            oParameters = New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@ContactPermission_ID", oContactRolePermission.ID))
            oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))
            DataAccess.ExecuteCommand("sp_Common_ContactPermissions_Delete", oParameters)
        Next

        For Each oContactRolePermission In mcContactPermissions
            If oContactRolePermission.DatabaseObjectID = 104 Then
                Dim i As Integer = 1
            End If
            oContactRolePermission.Save(Me.ID, oUser)
        Next

        moMixedPermissions = PopulateMixedPermissions()

    End Sub

    Public ReadOnly Property DatabaseRoles() As CArrayList(Of CContactDatabaseRole)
        Get
            mcDatabaseRoles.Sort()
            Return mcDatabaseRoles
        End Get
    End Property

    Public ReadOnly Property ContactPermissions() As CArrayList(Of CContactRolePermission)
        Get
            mcContactPermissions.Sort()
            Return mcContactPermissions
        End Get
    End Property

    Public Property ID() As Integer
        Get
            Return miUserID
        End Get
        Set(ByVal Value As Integer)
            If miUserID <> Value Then mbChanged = True
            miUserID = Value
        End Set
    End Property
    Public Property FirstName() As String
        Get
            Return msFirstName
        End Get
        Set(ByVal Value As String)
            msFirstName = Value
        End Set
    End Property
    Public Property LastName() As String
        Get
            Return msLastName
        End Get
        Set(ByVal Value As String)
            msLastName = Value
        End Set
    End Property
    Public Property Email() As String
        Get
            Return msEmail
        End Get
        Set(ByVal Value As String)
            msEmail = Value
        End Set
    End Property
    Public Property Password() As String
        Get
            Return msPassword
        End Get
        Set(ByVal Value As String)
            msPassword = Value
        End Set
    End Property
    Public ReadOnly Property StateID() As Integer
        Get
            Return miStateID
        End Get
    End Property
    Public ReadOnly Property State() As CState
        Get
            Return Lists.GetStateByID(miStateID)
        End Get
    End Property
    Public ReadOnly Property CountryID() As Integer
        Get
            Return miCountryID
        End Get
    End Property
    Public ReadOnly Property Country() As CCountry
        Get
            Return Lists.GetCountryByID(miCountryID)
        End Get
    End Property
    Public ReadOnly Property CampusID() As Integer
        Get
            Return miCampusID
        End Get
    End Property
    Public ReadOnly Property Campus() As Internal.Church.CCampus
        Get
            Return Lists.Internal.Church.GetCampusByID(miCampusID)
        End Get
    End Property
    Public Property LastPasswordChangeDate() As Date
        Get
            Return mdLastPasswordChangeDate
        End Get
        Set(ByVal value As Date)
            If mdLastPasswordChangeDate <> value Then mbChanged = True
            mdLastPasswordChangeDate = value
        End Set
    End Property
    Public Property InvalidPasswordAttempts() As Integer
        Get
            Return miInvalidPasswordAttempts
        End Get
        Set(ByVal value As Integer)
            If miInvalidPasswordAttempts <> value Then mbChanged = True
            miInvalidPasswordAttempts = value
        End Set
    End Property
    Public Property PasswordResetRequired() As Boolean
        Get
            Return mbPasswordResetRequired
        End Get
        Set(ByVal value As Boolean)
            If mbPasswordResetRequired <> value Then mbChanged = True
            mbPasswordResetRequired = value
        End Set
    End Property
    Public Property Inactive() As Boolean
        Get
            Return mbInactive
        End Get
        Set(ByVal value As Boolean)
            If mbInactive <> value Then mbChanged = True
            mbInactive = value
        End Set
    End Property

    Public ReadOnly Property AccountLockedOut() As Boolean
        Get
            Return miInvalidPasswordAttempts >= 4
        End Get
    End Property

    Public Function GetContact() As CContact

        If miUserID > 0 And moContact Is Nothing Then
            moContact = New CContact
            moContact.LoadByID(miUserID)
        End If
        Return moContact

    End Function

    Public Sub ResetLoginAccount()

        If miUserID > 0 Then
            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("Contact_ID", miUserID))
            DataAccess.ExecuteCommand("sp_Common_UserAccountReset", oParameters)
            miInvalidPasswordAttempts = 0
        End If

    End Sub

    ReadOnly Property HasRoleInModule(ByVal iModule As EnumDatabaseModule) As Boolean
        Get
            Dim oRole As CContactDatabaseRole
            For Each oRole In mcDatabaseRoles
                If oRole.DatabaseRole.ModuleName = iModule.ToString Then Return True
            Next
            Return False
        End Get
    End Property

    ReadOnly Property IsMemberOfRole(ByVal oDatabaseRole As CDatabaseRole) As Boolean
        Get
            Dim oRole As CContactDatabaseRole
            For Each oRole In mcDatabaseRoles
                If oRole.DatabaseRole.ID = oDatabaseRole.ID Then Return True
            Next
            Return False
        End Get
    End Property

    Public Function AggregateRolePermissions(Optional ByVal iModule As EnumDatabaseModule = 0) As ArrayList

        Dim oAggregatePermissions As New ArrayList

        Dim oContactDatabaseRole As CContactDatabaseRole
        Dim oPermission As CDatabaseRolePermission
        Dim oAggregatePermission As CDatabaseRolePermission = Nothing

        Dim oDatabaseObject As CDatabaseObject
        For Each oDatabaseObject In Lists.DatabaseObjectsByModule(iModule).Values
            oAggregatePermission = New CDatabaseRolePermission
            oAggregatePermission.DatabaseObjectID = oDatabaseObject.ID
            oAggregatePermissions.Add(oAggregatePermission)
        Next

        For Each oContactDatabaseRole In mcDatabaseRoles
            If oContactDatabaseRole.DatabaseRole.ModuleName = iModule.ToString Or iModule = 0 Then
                For Each oPermission In oContactDatabaseRole.DatabaseRole.Permissions

                    For Each oAggregatePermission In oAggregatePermissions
                        If oAggregatePermission.DatabaseObject.DatabaseObjectName = oPermission.DatabaseObject.DatabaseObjectName Then
                            Exit For
                        End If
                    Next

                    If oPermission.Read Then oAggregatePermission.Read = True
                    If oPermission.Write Then oAggregatePermission.Write = True
                    If oPermission.ViewState Then oAggregatePermission.ViewState = True
                    If oPermission.ViewCountry Then oAggregatePermission.ViewCountry = True
                Next
            End If
        Next

        Return oAggregatePermissions

    End Function

    Public ReadOnly Property MixedPermissions As Hashtable
        Get
            If moMixedPermissions Is Nothing Then moMixedPermissions = PopulateMixedPermissions()
            Return moMixedPermissions
        End Get
    End Property

    Private Function PopulateMixedPermissions() As Hashtable
        Dim oPermissions As New Hashtable

        Dim oParams As New ArrayList
        oParams.Add(New SqlClient.SqlParameter("@Contact_ID", miUserID))
        Dim oDataTable As DataTable = Business.DataAccess.GetDataTable("sp_AggregatePermissions_Read", oParams)

        For Each oDataRow As DataRow In oDataTable.Rows
            Dim oPermission As New CPermission()
            oPermission.DatabaseObjectID = CType(oDataRow("DatabaseObject_ID"), Integer)

            If oDataRow("RoleRead") Or oDataRow("CustomRead") Then
                oPermission.Read = True
            End If

            If oDataRow("RoleWrite") Or oDataRow("CustomWrite") Then
                oPermission.Write = True
            End If

            If oPermission IsNot Nothing Then oPermissions.Add(oPermission.DatabaseObjectID, oPermission)
        Next

        Return oPermissions
    End Function

    Public Function GetPermission(ByVal sDatabaseObjectName As String) As CPermission

        If MixedPermissions Is Nothing Then Return New CPermission()

        Dim oPermission As CPermission

        Try
            For Each oPermission In MixedPermissions.Values
                If oPermission.DatabaseObject.DatabaseObjectName = sDatabaseObjectName Then
                    Return oPermission
                End If
            Next
        Catch ex As Exception
        End Try

        Return New CPermission()

    End Function

    Public Function GetContactPermission(ByVal iDatabaseObjectID As Integer) As CContactRolePermission
        Dim oParameters As New ArrayList
        oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", Me.ID))
        oParameters.Add(New SqlClient.SqlParameter("@DatabaseObject_ID", iDatabaseObjectID))

        Dim oDataTable As DataTable = DataAccess.GetDataTable("sp_Common_ContactPermissions_Read", oParameters)

        If oDataTable.Rows.Count > 0 Then
            Return New CContactRolePermission(oDataTable.Rows(0))
        End If

        Return Nothing
    End Function

    Public Function GetRolePermission(ByVal iDatabaseObjectID As Integer) As CDatabaseRolePermission
        Dim oParameters As New ArrayList
        oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", Me.ID))
        oParameters.Add(New SqlClient.SqlParameter("@DatabaseObject_ID", iDatabaseObjectID))

        Dim oDataTable As DataTable = DataAccess.GetDataTable("sp_Common_ContactDatabaseRole_Permissions_Read", oParameters)

        If oDataTable.Rows.Count > 0 Then
            Return New CDatabaseRolePermission(oDataTable.Rows(0))
        End If

        Return Nothing
    End Function

    ReadOnly Property Name() As String
        Get
            Return msFirstName & " " & msLastName
        End Get
    End Property

    Public Overrides Function toString() As String
        Return msFirstName & " " & msLastName
    End Function

    Public ReadOnly Property Changed() As Boolean
        Get

            If mbChanged Then Return True

            Dim oContactDatabaseRole As CContactDatabaseRole
            For Each oContactDatabaseRole In mcDatabaseRoles
                If oContactDatabaseRole.Changed Then Return True
            Next

            Return False

        End Get
    End Property

    Public Sub ResetChanges()
        mbChanged = False
        mcDatabaseRoles.ResetChanges()
    End Sub

End Class