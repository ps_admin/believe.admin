Namespace External.Events

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CConferencePreOrder

        Private miCPID As Integer
        Private miConferenceID As Integer
        Private miProductID As Integer
        Private mbShowPreOrderOnWeb As Boolean

        Public Sub New()

            miCPID = 0
            miConferenceID = 0
            miProductID = 0
            mbShowPreOrderOnWeb = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miCPID = oDataRow("CP_ID")
            miConferenceID = oDataRow("Conference_ID")
            miProductID = oDataRow("Product_ID")
            mbShowPreOrderOnWeb = oDataRow("ShowPreOrderOnWeb")

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miCPID
            End Get
        End Property
        Public Property ConferenceID() As Integer
            Get
                Return miConferenceID
            End Get
            Set(ByVal Value As Integer)
                miConferenceID = Value
            End Set
        End Property
        Public ReadOnly Property Conference() As CConference
            Get
                Return Lists.External.Events.GetConferenceByID(miConferenceID)
            End Get
        End Property
        Public Property ProductID() As Integer
            Get
                Return miProductID
            End Get
            Set(ByVal Value As Integer)
                miProductID = Value
            End Set
        End Property
        Public ReadOnly Property Product() As CProduct
            Get
                Return Lists.External.Events.GetProductByID(miProductID)
            End Get
        End Property
        Property ShowPreOrderOnWeb() As Boolean
            Get
                Return mbShowPreOrderOnWeb
            End Get
            Set(ByVal Value As Boolean)
                mbShowPreOrderOnWeb = Value
            End Set
        End Property

    End Class

End Namespace