Namespace External.Events

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CContactExternalEvents

        Private miContactID As Integer

        Private msEmergencyContactName As String
        Private msEmergencyContactPhone As String
        Private msEmergencyContactRelationship As String
        Private msMedicalInformation As String
        Private msMedicalAllergies As String
        Private msAccessibilityInformation As String
        Private msEventComments As String

        Private mcPayment As CArrayList(Of CContactPayment)
        Private mcRegistration As CArrayList(Of CRegistration)
        Private mcWaitingList As CArrayList(Of CWaitingList)

        Public mbChanged As Boolean

        Public Sub New()

            msEmergencyContactName = String.Empty
            msEmergencyContactPhone = String.Empty
            msEmergencyContactRelationship = String.Empty
            msMedicalInformation = String.Empty
            msMedicalAllergies = String.Empty
            msAccessibilityInformation = String.Empty
            msEventComments = String.Empty

            mcPayment = New CArrayList(Of CContactPayment)
            mcRegistration = New CArrayList(Of CRegistration)
            mcWaitingList = New CArrayList(Of CWaitingList)

            mbChanged = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)
            Me.New()
            LoadDataRow(oDataRow)
        End Sub

        Public Sub LoadDataRow(ByRef oDataRow As DataRow)

            Dim oChildRow As DataRow

            miContactID = oDataRow("Contact_ID")
            msEmergencyContactName = oDataRow("EmergencyContactName")
            msEmergencyContactPhone = oDataRow("EmergencyContactPhone")
            msEmergencyContactRelationship = oDataRow("EmergencyContactRelationship")
            msMedicalInformation = oDataRow("MedicalInformation")
            msMedicalAllergies = oDataRow("MedicalAllergies")
            msAccessibilityInformation = oDataRow("AccessibilityInformation")
            msEventComments = oDataRow("EventComments")

            For Each oChildRow In oDataRow.GetChildRows("ContactPayment")
                mcPayment.Add(New CContactPayment(oChildRow))
            Next

            For Each oChildRow In oDataRow.GetChildRows("Registration")
                mcRegistration.Add(New CRegistration(oChildRow))
            Next

            For Each oChildRow In oDataRow.GetChildRows("WaitingList")
                mcWaitingList.Add(New CWaitingList(oChildRow))
            Next

            mcPayment.ResetChanges()
            mcRegistration.ResetChanges()
            mcWaitingList.ResetChanges()

            mbChanged = False

        End Sub

        Public Sub LoadID(ByVal iContactID As Integer)

            Dim oDataSet As DataSet

            Dim oTableMappings As New ArrayList
            oTableMappings.Add(New Common.DataTableMapping("Table", "ContactExternalEvents"))
            oTableMappings.Add(New Common.DataTableMapping("Table1", "ContactPayment"))
            oTableMappings.Add(New Common.DataTableMapping("Table2", "Registration"))
            oTableMappings.Add(New Common.DataTableMapping("Table3", "RegistrationChildInformation"))
            oTableMappings.Add(New Common.DataTableMapping("Table4", "PreOrder"))
            oTableMappings.Add(New Common.DataTableMapping("Table5", "RegistrationOption"))
            oTableMappings.Add(New Common.DataTableMapping("Table6", "WaitingList"))

            oDataSet = DataAccess.GetDataSet("sp_Common_ContactExternalEventsRead " & iContactID.ToString, oTableMappings)

            oDataSet.Relations.Add("ContactPayment", oDataSet.Tables("ContactExternalEvents").Columns("Contact_ID"), _
                    oDataSet.Tables("ContactPayment").Columns("Contact_ID"))

            oDataSet.Relations.Add("Registration", oDataSet.Tables("ContactExternalEvents").Columns("Contact_ID"), _
                    oDataSet.Tables("Registration").Columns("Contact_ID"))

            oDataSet.Relations.Add("RegistrationChildInformation", oDataSet.Tables("Registration").Columns("Registration_ID"), _
                    oDataSet.Tables("RegistrationChildInformation").Columns("Registration_ID"))

            oDataSet.Relations.Add("PreOrder", oDataSet.Tables("Registration").Columns("Registration_ID"), _
                    oDataSet.Tables("PreOrder").Columns("Registration_ID"))

            oDataSet.Relations.Add("RegistrationOption", oDataSet.Tables("Registration").Columns("Registration_ID"), _
                    oDataSet.Tables("RegistrationOption").Columns("Registration_ID"))

            oDataSet.Relations.Add("WaitingList", oDataSet.Tables("ContactExternalEvents").Columns("Contact_ID"), _
                    oDataSet.Tables("WaitingList").Columns("Contact_ID"))

            If oDataSet.Tables("ContactExternalEvents").Rows.Count = 1 Then
                LoadDataRow(oDataSet.Tables("ContactExternalEvents").Rows(0))
            Else
                'If we can't find the ContactExternalEvents records, set the contact ID of the object, 
                'so it knows that it should save a new one.
                miContactID = iContactID
                mbChanged = True
            End If

        End Sub

        Public Sub Save(ByVal oUser As CUser)

            Dim oParameters As New ArrayList

            If mbChanged Then

                oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", miContactID))
                oParameters.Add(New SqlClient.SqlParameter("@EmergencyContactName", msEmergencyContactName))
                oParameters.Add(New SqlClient.SqlParameter("@EmergencyContactPhone", msEmergencyContactPhone))
                oParameters.Add(New SqlClient.SqlParameter("@EmergencyContactRelationship", msEmergencyContactRelationship))
                oParameters.Add(New SqlClient.SqlParameter("@MedicalInformation", msMedicalInformation))
                oParameters.Add(New SqlClient.SqlParameter("@MedicalAllergies", msMedicalAllergies))
                oParameters.Add(New SqlClient.SqlParameter("@AccessibilityInformation", msAccessibilityInformation))
                oParameters.Add(New SqlClient.SqlParameter("@EventComments", msEventComments))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))

                DataAccess.ExecuteCommand("sp_Common_ContactExternalEventsUpdate", oParameters)

                mbChanged = False

            End If

            SavePayments(oUser)
            SaveRegistrations(oUser)
            SaveWaitingLists(oUser)

            mcPayment.ResetChanges()
            mcRegistration.ResetChanges()
            mcWaitingList.ResetChanges()

        End Sub

        Private Sub SavePayments(ByVal oUser As CUser)

            Dim oParameters As ArrayList
            Dim oContactPayment As CContactPayment

            'Remove the Deleted payments
            For Each oContactPayment In mcPayment.Removed
                oParameters = New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@ContactPayment_ID", oContactPayment.ID))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))
                DataAccess.ExecuteCommand("sp_Events_ContactPaymentDelete", oParameters)
            Next

            'Add/Update all the others
            For Each oContactPayment In mcPayment
                oContactPayment.ContactID = miContactID
                oContactPayment.Save(oUser)
            Next

        End Sub

        Private Sub SaveRegistrations(ByVal oUser As CUser)

            Dim oParameters As ArrayList
            Dim oRegistration As CRegistration
            Dim oComboRegistration As CRegistration

            'Remove the Deleted Registrations
            For Each oRegistration In mcRegistration.Removed
                oParameters = New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@Registration_ID", oRegistration.ID))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))
                DataAccess.ExecuteCommand("sp_Events_RegistrationDelete", oParameters)
            Next

            'If there is a combo rego, find the two rego's and link them together...
            For Each oRegistration In mcRegistration
                If oRegistration.ID = 0 Then
                    If oRegistration.RegistrationType.IsComboRegistration Then
                        For Each oComboRegistration In mcRegistration
                            If oComboRegistration.ID = 0 Then
                                If oRegistration.RegistrationType.IsComboRegistration(oComboRegistration.RegistrationType) Then

                                    'We have found the two rego's. Save them so we can get their ID's
                                    oComboRegistration.ContactID = miContactID
                                    oComboRegistration.Save(oUser)
                                    oRegistration.ContactID = miContactID
                                    oRegistration.Save(oUser)

                                    'Link them together and Save again
                                    oRegistration.ComboRegistrationID = oComboRegistration.ID
                                    oRegistration.Save(oUser)
                                    oComboRegistration.ComboRegistrationID = oRegistration.ID
                                    oComboRegistration.Save(oUser)

                                End If
                            End If
                        Next
                    End If
                End If
            Next

            'Add/Update all the others
            For Each oRegistration In mcRegistration
                oRegistration.ContactID = miContactID
                oRegistration.Save(oUser)
            Next

        End Sub

        Private Sub SaveWaitingLists(ByVal oUser As CUser)

            Dim oParameters As ArrayList
            Dim oWaitingList As CWaitingList

            'Remove the Deleted Waiting Lists
            For Each oWaitingList In mcWaitingList.Removed
                oParameters = New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@WaitingList_ID", oWaitingList.ID))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))
                DataAccess.ExecuteCommand("sp_Events_WaitingListDelete", oParameters)
            Next

            'Add/Update all the others
            For Each oWaitingList In mcWaitingList
                oWaitingList.ContactID = miContactID
                oWaitingList.Save(oUser)
            Next

        End Sub

        Public ReadOnly Property Payment() As CArrayList(Of CContactPayment)
            Get
                Return mcPayment
            End Get
        End Property
        Public ReadOnly Property Registration() As CArrayList(Of CRegistration)
            Get
                Return mcRegistration
            End Get
        End Property
        Public ReadOnly Property WaitingList() As CArrayList(Of CWaitingList)
            Get
                Return mcWaitingList
            End Get
        End Property

        Public Function SessionOrderRead() As DataTable

            Return DataAccess.GetDataTable("EXEC sp_Events_ContactSessionOrderRead " & miContactID.ToString)

        End Function

        Public ReadOnly Property ID() As Integer
            Get
                Return miContactID
            End Get
        End Property

        Public Property ContactID() As Integer
            Get
                Return miContactID
            End Get
            Set(ByVal value As Integer)
                If miContactID <> value Then mbChanged = True
                miContactID = value
            End Set
        End Property

        Property EmergencyContactName() As String
            Get
                Return msEmergencyContactName
            End Get
            Set(ByVal Value As String)
                If msEmergencyContactName <> Value Then mbChanged = True
                msEmergencyContactName = Value
            End Set
        End Property
        Property EmergencyContactPhone() As String
            Get
                Return msEmergencyContactPhone
            End Get
            Set(ByVal Value As String)
                If msEmergencyContactPhone <> Value Then mbChanged = True
                msEmergencyContactPhone = Value
            End Set
        End Property
        Property EmergencyContactRelationship() As String
            Get
                Return msEmergencyContactRelationship
            End Get
            Set(ByVal Value As String)
                If msEmergencyContactRelationship <> Value Then mbChanged = True
                msEmergencyContactRelationship = Value
            End Set
        End Property
        Property MedicalInformation() As String
            Get
                Return msMedicalInformation
            End Get
            Set(ByVal Value As String)
                If msMedicalInformation <> Value Then mbChanged = True
                msMedicalInformation = Value
            End Set
        End Property
        Property MedicalAllergies() As String
            Get
                Return msMedicalAllergies
            End Get
            Set(ByVal Value As String)
                If msMedicalAllergies <> Value Then mbChanged = True
                msMedicalAllergies = Value
            End Set
        End Property
        Property AccessibilityInformation() As String
            Get
                Return msAccessibilityInformation
            End Get
            Set(ByVal Value As String)
                If msAccessibilityInformation <> Value Then mbChanged = True
                msAccessibilityInformation = Value
            End Set
        End Property
        Property EventComments() As String
            Get
                Return msEventComments
            End Get
            Set(ByVal Value As String)
                If msEventComments <> Value Then mbChanged = True
                msEventComments = Value
            End Set
        End Property

        ReadOnly Property AmountTotal(ByVal oConference As CConference, Optional ByVal iGroupLeaderID As Integer = 0) As Decimal
            Get
                Dim oRegistration As CRegistration
                Dim dTotal As Decimal
                For Each oRegistration In mcRegistration
                    If oConference Is Nothing OrElse oRegistration.Venue.Conference.ID = oConference.ID Then
                        If oRegistration.GroupLeaderID = iGroupLeaderID Then
                            dTotal += oRegistration.TotalCost
                        End If
                    End If
                Next
                Return IIf(dTotal < 0, 0, dTotal)
            End Get
        End Property

        ReadOnly Property AmountPaid(Optional ByVal oConference As CConference = Nothing) As Decimal
            Get
                Dim dTotal As Decimal
                Dim oPayment As CContactPayment
                For Each oPayment In mcPayment
                    If oConference Is Nothing OrElse oPayment.Conference.ID = oConference.ID Then
                        dTotal = dTotal + oPayment.PaymentAmount
                    End If
                Next
                Return dTotal
            End Get
        End Property

        ReadOnly Property AmountOwing(Optional ByVal oConference As CConference = Nothing) As Decimal
            Get
                Return AmountTotal(oConference) - AmountPaid(oConference)
            End Get
        End Property

        ReadOnly Property Registrations(ByVal oConference As CConference, Optional ByVal iGroupLeaderID As Integer = -1) As CArrayList(Of CRegistration)
            Get
                Dim oArraylist As New CArrayList(Of CRegistration)
                Dim oRegistration As CRegistration
                For Each oRegistration In mcRegistration
                    If iGroupLeaderID = -1 Or oRegistration.GroupLeaderID = iGroupLeaderID Then
                        If oConference Is Nothing OrElse oRegistration.Venue.Conference.ID = oConference.ID Then
                            oArraylist.Add(oRegistration)
                        End If
                    End If
                Next
                Return oArraylist
            End Get
        End Property

        ReadOnly Property HasCurrentRegistration(Optional ByVal iGroupLeaderID As Integer = -1, Optional ByVal oConference As CConference = Nothing, Optional ByVal bIncludeComboRegistrations As Boolean = True) As Boolean
            Get
                Dim oRegistration As CRegistration
                For Each oRegistration In Registration
                    If iGroupLeaderID = -1 Or oRegistration.GroupLeaderID = iGroupLeaderID Then
                        If oConference Is Nothing OrElse oRegistration.Venue.Conference.ID = oConference.ID Then
                            If Not oRegistration.Venue.HasEnded Then
                                If bIncludeComboRegistrations Or (oRegistration.RegistrationType.IsComboRegistration AndAlso oRegistration.RegistrationType.IsComboRegistrationPrimary) Then
                                    Return True
                                End If
                            End If
                        End If
                    End If
                Next
                Return False
            End Get
        End Property

        ReadOnly Property GetLatestRegistration() As CRegistration
            Get

                Dim oRegistration As CRegistration
                Dim oLatestRegistration As CRegistration

                'We need to return the latest registration. IF there are none, return nothing
                'Else, set the lastest to the first rego, so we have something to compare against.
                If Registration.Count = 0 Then
                    Return Nothing
                Else
                    oLatestRegistration = Registration.Item(0)
                End If
                For Each oRegistration In Registration
                    If oRegistration.Venue.ConferenceStartDate > oLatestRegistration.Venue.ConferenceStartDate Then
                        oLatestRegistration = oRegistration
                    End If
                Next

                Return oLatestRegistration

            End Get
        End Property

        Public Function GetFriendRegistrations(ByVal oConference As CConference) As CArrayList(Of CContact)

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("Contact_ID", miContactID))
            oParameters.Add(New SqlClient.SqlParameter("Conference_ID", oConference.ID))

            Dim oArrayList As New CArrayList(Of CContact)
            Dim oDataTable As DataTable = DataAccess.GetDataTable("sp_Events_RegistrationFriendList", oParameters)
            For Each oDataRow As DataRow In oDataTable.Rows
                oArrayList.Add(New CContact(oDataRow("Contact_ID")))
            Next
            Return oArrayList

        End Function

        Public Property Changed() As Boolean
            Get

                If mbChanged Then Return True

                If mcPayment.Changed Then Return True
                If mcRegistration.Changed Then Return True
                If mcWaitingList.Changed Then Return True

                Dim oPayment As CContactPayment
                For Each oPayment In mcPayment
                    If oPayment.Changed Then Return True
                Next

                Dim oRegistration As CRegistration
                For Each oRegistration In mcRegistration
                    If oRegistration.Changed Then Return True
                Next

                Dim oWaitingList As CWaitingList
                For Each oWaitingList In mcWaitingList
                    If oWaitingList.Changed Then Return True
                Next

                Return False

            End Get
            Set(ByVal value As Boolean)
                mbChanged = value
            End Set
        End Property

        Public Sub ResetChanges()

            mbChanged = False

            mcPayment.ResetChanges()
            mcRegistration.ResetChanges()
            mcWaitingList.ResetChanges()

            Dim oPayment As CContactPayment
            For Each oPayment In mcPayment
                oPayment.ResetChanges()
            Next

            Dim oRegistration As CRegistration
            For Each oRegistration In mcRegistration
                oRegistration.ResetChanges()
            Next

            Dim oWaitingList As CWaitingList
            For Each oWaitingList In mcWaitingList
                oWaitingList.ResetChanges()
            Next

        End Sub

    End Class

End Namespace