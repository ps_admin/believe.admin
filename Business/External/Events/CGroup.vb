Namespace External.Events

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CGroup

        Private miGroupLeaderID As Integer
        Private moGroupLeader As CContact
        Private msGroupName As String
        Private msGroupColeaders As String
        Private msYouthLeaderName As String
        Private miGroupSizeID As Integer
        Private miGroupCreditStatusID As Integer
        Private miGroupTypeID As Integer
        Private msComments As String
        Private mbDetailsVerified As Boolean
        Private mbDeleted As Boolean

        Private mdCreationDate As Date
        Private miCreatedByID As Integer
        Private mdModificationDate As Date
        Private miModifiedByID As Integer

        Private msGUID As String

        Private mcNotes As CArrayList(Of CGroupNotes)
        Private mcPayments As CArrayList(Of CGroupPayment)
        Private mcGroupPromoRequest As CArrayList(Of CGroupPromoRequest)
        Private mcBulkRegistrations As CArrayList(Of CGroupBulkRegistration)
        Private mcBulkPurchase As CArrayList(Of CGroupBulkPurchase)
        Private mcContacts As CArrayList(Of CContact)

        Private mbNewGroup As Boolean

        Private mbChanged As Boolean

        Public Sub New()

            miGroupLeaderID = 0
            msGroupName = String.Empty
            msGroupColeaders = String.Empty
            msYouthLeaderName = String.Empty
            miGroupSizeID = 0
            miGroupCreditStatusID = EnumGroupCreditStatus.NonCredit
            miGroupTypeID = EnumGroupType.YouthGroup
            msComments = String.Empty
            mbDetailsVerified = False
            mbDeleted = False

            mdCreationDate = Nothing
            mdModificationDate = Nothing
            miCreatedByID = 0
            miModifiedByID = 0

            msGUID = System.Guid.NewGuid.ToString

            mcNotes = New CArrayList(Of CGroupNotes)
            mcPayments = New CArrayList(Of CGroupPayment)
            mcGroupPromoRequest = New CArrayList(Of CGroupPromoRequest)
            mcBulkRegistrations = New CArrayList(Of CGroupBulkRegistration)
            mcBulkPurchase = New CArrayList(Of CGroupBulkPurchase)
            mcContacts = New CArrayList(Of CContact)

            mbNewGroup = True

            mbChanged = False

        End Sub

        Public Function LoadByEmail(ByVal sEmail As String) As Boolean

            Dim oParameter As New ArrayList
            oParameter.Add(New SqlClient.SqlParameter("@Email", sEmail))

            Return Load(DataAccess.GetValue("sp_Events_GroupReadByEmail", oParameter))

        End Function

        Public Function Load(ByVal iGroupLeaderID As Integer) As Boolean

            Dim oDataSet As DataSet
            Dim oDataRow As DataRow
            Dim oChildRow As DataRow

            Dim oContact As CContact
            Dim oTableMappings As New ArrayList

            oTableMappings.Add(New Common.DataTableMapping("Table", "Group"))
            oTableMappings.Add(New Common.DataTableMapping("Table1", "GroupNotes"))
            oTableMappings.Add(New Common.DataTableMapping("Table2", "GroupPayment"))
            oTableMappings.Add(New Common.DataTableMapping("Table3", "GroupPromoRequest"))
            oTableMappings.Add(New Common.DataTableMapping("Table4", "GroupBulkRegistration"))
            oTableMappings.Add(New Common.DataTableMapping("Table5", "GroupBulkPurchase"))
            oTableMappings.Add(New Common.DataTableMapping("Table6", "Contact"))
            oTableMappings.Add(New Common.DataTableMapping("Table7", "ContactMailingList"))
            oTableMappings.Add(New Common.DataTableMapping("Table8", "ContactMailingListType"))
            oTableMappings.Add(New Common.DataTableMapping("Table9", "ContactDatabaseRole"))
            oTableMappings.Add(New Common.DataTableMapping("Table10", "ContactExternal"))
            oTableMappings.Add(New Common.DataTableMapping("Table11", "ContactExternalEvents"))
            oTableMappings.Add(New Common.DataTableMapping("Table12", "ContactPayment"))
            oTableMappings.Add(New Common.DataTableMapping("Table13", "Registration"))
            oTableMappings.Add(New Common.DataTableMapping("Table14", "CrecheChild"))
            oTableMappings.Add(New Common.DataTableMapping("Table15", "PreOrder"))
            oTableMappings.Add(New Common.DataTableMapping("Table16", "RegistrationOption"))
            oTableMappings.Add(New Common.DataTableMapping("Table17", "ContactVenueWaiting"))

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@GroupLeader_ID", iGroupLeaderID))

            oDataSet = DataAccess.GetDataSet("sp_Events_GroupRead", oTableMappings, oParameters)


            If oDataSet.Tables(0).Rows.Count > 0 Then

                oDataSet.Relations.Add("GroupNotes", oDataSet.Tables("Group").Columns("GroupLeader_ID"), _
                    oDataSet.Tables("GroupNotes").Columns("GroupLeader_ID"))

                oDataSet.Relations.Add("GroupPayment", oDataSet.Tables("Group").Columns("GroupLeader_ID"), _
                    oDataSet.Tables("GroupPayment").Columns("GroupLeader_ID"))

                oDataSet.Relations.Add("GroupPromoRequest", oDataSet.Tables("Group").Columns("GroupLeader_ID"), _
                    oDataSet.Tables("GroupPromoRequest").Columns("GroupLeader_ID"))

                oDataSet.Relations.Add("GroupBulkRegistration", oDataSet.Tables("Group").Columns("GroupLeader_ID"), _
                    oDataSet.Tables("GroupBulkRegistration").Columns("GroupLeader_ID"))

                oDataSet.Relations.Add("GroupBulkPurchase", oDataSet.Tables("Group").Columns("GroupLeader_ID"), _
                    oDataSet.Tables("GroupBulkPurchase").Columns("GroupLeader_ID"))

                oDataSet.Relations.Add("Contact", oDataSet.Tables("Group").Columns("GroupLeader_ID"), _
                    oDataSet.Tables("Contact").Columns("GroupLeader_ID"))

                oDataSet.Relations.Add("ContactMailingList", oDataSet.Tables("Contact").Columns("Contact_ID"), _
                    oDataSet.Tables("ContactMailingList").Columns("Contact_ID"))

                oDataSet.Relations.Add("ContactMailingListType", oDataSet.Tables("Contact").Columns("Contact_ID"), _
                    oDataSet.Tables("ContactMailingListType").Columns("Contact_ID"))

                oDataSet.Relations.Add("ContactDatabaseRole", oDataSet.Tables("Contact").Columns("Contact_ID"), _
                    oDataSet.Tables("ContactDatabaseRole").Columns("Contact_ID"))

                oDataSet.Relations.Add("ContactExternal", oDataSet.Tables("Contact").Columns("Contact_ID"), _
                    oDataSet.Tables("ContactExternal").Columns("Contact_ID"))

                oDataSet.Relations.Add("ContactExternalEvents", oDataSet.Tables("Contact").Columns("Contact_ID"), _
                    oDataSet.Tables("ContactExternalEvents").Columns("Contact_ID"))

                oDataSet.Relations.Add("ContactPayment", oDataSet.Tables("ContactExternalEvents").Columns("Contact_ID"), _
                  oDataSet.Tables("ContactPayment").Columns("Contact_ID"))

                oDataSet.Relations.Add("Registration", oDataSet.Tables("ContactExternalEvents").Columns("Contact_ID"), _
                    oDataSet.Tables("Registration").Columns("Contact_ID"))

                oDataSet.Relations.Add("CrecheChild", oDataSet.Tables("Registration").Columns("Registration_ID"), _
                    oDataSet.Tables("CrecheChild").Columns("Registration_ID"))

                oDataSet.Relations.Add("PreOrder", oDataSet.Tables("Registration").Columns("Registration_ID"), _
                    oDataSet.Tables("PreOrder").Columns("Registration_ID"))

                oDataSet.Relations.Add("RegistrationOption", oDataSet.Tables("Registration").Columns("Registration_ID"), _
                    oDataSet.Tables("RegistrationOption").Columns("Registration_ID"))

                oDataSet.Relations.Add("ContactVenueWaiting", oDataSet.Tables("ContactExternalEvents").Columns("Contact_ID"), _
                    oDataSet.Tables("ContactVenueWaiting").Columns("Contact_ID"))

                oDataRow = oDataSet.Tables("Group").Rows(0)
                miGroupLeaderID = oDataRow("GroupLeader_ID")
                msGroupName = oDataRow("GroupName")
                msGroupColeaders = oDataRow("GroupColeaders")
                msYouthLeaderName = oDataRow("YouthLeaderName")
                miGroupSizeID = IsNull(oDataRow("GroupSize_ID"), 0)
                miGroupCreditStatusID = oDataRow("GroupCreditStatus_ID")
                miGroupTypeID = oDataRow("GroupType_ID")
                msComments = oDataRow("Comments")
                mbDetailsVerified = oDataRow("DetailsVerified")
                mbDeleted = oDataRow("Deleted")
                mdCreationDate = IsNull(oDataRow("CreationDate"), Nothing)
                mdModificationDate = IsNull(oDataRow("ModificationDate"), Nothing)
                miCreatedByID = oDataRow("CreatedBy_ID")
                miModifiedByID = IsNull(oDataRow("ModifiedBy_ID"), 0)
                msGUID = oDataRow("GUID").ToString

                mcNotes = New CArrayList(Of CGroupNotes)
                For Each oChildRow In oDataRow.GetChildRows(oDataSet.Relations("GroupNotes"))
                    mcNotes.Add(New CGroupNotes(oChildRow))
                Next

                mcPayments = New CArrayList(Of CGroupPayment)
                For Each oChildRow In oDataRow.GetChildRows(oDataSet.Relations("GroupPayment"))
                    mcPayments.Add(New CGroupPayment(oChildRow))
                Next

                mcGroupPromoRequest = New CArrayList(Of CGroupPromoRequest)
                For Each oChildRow In oDataRow.GetChildRows(oDataSet.Relations("GroupPromoRequest"))
                    mcGroupPromoRequest.Add(New CGroupPromoRequest(oChildRow))
                Next

                mcBulkRegistrations = New CArrayList(Of CGroupBulkRegistration)
                For Each oChildRow In oDataRow.GetChildRows(oDataSet.Relations("GroupBulkRegistration"))
                    mcBulkRegistrations.Add(New CGroupBulkRegistration(oChildRow, Me))
                Next

                mcBulkPurchase = New CArrayList(Of CGroupBulkPurchase)
                For Each oChildRow In oDataRow.GetChildRows(oDataSet.Relations("GroupBulkPurchase"))
                    mcBulkPurchase.Add(New CGroupBulkPurchase(oChildRow))
                Next

                mcContacts = New CArrayList(Of CContact)
                For Each oChildRow In oDataRow.GetChildRows("Contact")
                    oContact = New CContact
                    oContact.LoadDataRow(oChildRow)
                    mcContacts.Add(oContact)
                Next
                mcContacts.Sort(New CSorterContacts)

                mcNotes.ResetChanges()
                mcPayments.ResetChanges()
                mcGroupPromoRequest.ResetChanges()
                mcBulkRegistrations.ResetChanges()
                mcBulkPurchase.ResetChanges()
                mcContacts.ResetChanges()

                mbNewGroup = False
                mbChanged = False

                Return True

            Else
                Return False
            End If

        End Function

        Public Function Save(ByRef oUser As CUser) As Boolean

            If mbChanged Then

                Dim oParameters As New ArrayList

                If oUser Is Nothing Then
                    oUser = Lists.GetUserByID(EnumSystemUserNames.Administrator)
                End If

                'Update the Group Details
                If mbNewGroup Then
                    mdCreationDate = GetDateTime
                    miCreatedByID = oUser.ID
                    mbNewGroup = False
                Else
                    mdModificationDate = GetDateTime
                    miModifiedByID = oUser.ID
                End If

                oParameters.Add(New SqlClient.SqlParameter("@GroupLeader_ID", miGroupLeaderID))
                oParameters.Add(New SqlClient.SqlParameter("@GroupName", GroupName))
                oParameters.Add(New SqlClient.SqlParameter("@GroupColeaders", msGroupColeaders))
                oParameters.Add(New SqlClient.SqlParameter("@YouthLeaderName", YouthLeaderName))
                oParameters.Add(New SqlClient.SqlParameter("@GroupSize_ID", SQLNull(miGroupSizeID)))
                oParameters.Add(New SqlClient.SqlParameter("@GroupCreditStatus_ID", miGroupCreditStatusID))
                oParameters.Add(New SqlClient.SqlParameter("@GroupType_ID", miGroupTypeID))
                oParameters.Add(New SqlClient.SqlParameter("@Comments", Comments))
                oParameters.Add(New SqlClient.SqlParameter("@DetailsVerified", mbDetailsVerified))
                oParameters.Add(New SqlClient.SqlParameter("@Deleted", Deleted))
                oParameters.Add(New SqlClient.SqlParameter("@CreationDate", SQLDateTime(mdCreationDate)))
                oParameters.Add(New SqlClient.SqlParameter("@ModificationDate", SQLDateTime(mdModificationDate)))
                oParameters.Add(New SqlClient.SqlParameter("@CreatedBy_ID", miCreatedByID))
                oParameters.Add(New SqlClient.SqlParameter("@ModifiedBy_ID", SQLNull(miModifiedByID)))
                oParameters.Add(New SqlClient.SqlParameter("@GUID", msGUID))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))

                DataAccess.ExecuteCommand("sp_Events_GroupUpdate", oParameters)

                mbChanged = False

            End If

            If moGroupLeader IsNot Nothing Then
                moGroupLeader.Save(oUser)
            End If

            SaveNotes()
            SavePayments(oUser)
            SaveGroupPromoRequests()
            SaveBulkRegistrations(oUser)
            SaveBulkPurchase(oUser)
            SaveContacts(oUser)

            mcNotes.ResetChanges()
            mcPayments.ResetChanges()
            mcGroupPromoRequest.ResetChanges()
            mcBulkRegistrations.ResetChanges()
            mcBulkPurchase.ResetChanges()
            mcContacts.ResetChanges()

            Return True
        End Function

        Private Sub SaveNotes()

            Dim oParameters As ArrayList
            Dim oNotes As CGroupNotes

            'Remove the Deleted Notes
            For Each oNotes In mcNotes.Removed
                oParameters = New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@GroupNotes_ID", oNotes.ID))
                DataAccess.ExecuteCommand("sp_Events_GroupNotesDelete", oParameters)
            Next

            'Add/Update all the others
            For Each oNotes In mcNotes
                oNotes.GroupLeaderID = Me.ID
                oNotes.Save()
            Next

        End Sub

        Private Sub SavePayments(ByVal oUser As CUser)

            Dim oParameters As ArrayList
            Dim oPayment As CGroupPayment

            'Remove the Deleted payments
            For Each oPayment In mcPayments.Removed
                oParameters = New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@GroupPayment_ID", oPayment.ID))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))
                DataAccess.ExecuteCommand("sp_Events_GroupPaymentDelete", oParameters)
            Next

            'Add/Update all the others
            For Each oPayment In mcPayments
                oPayment.GroupLeaderID = Me.ID
                oPayment.Save(ouser)
            Next

        End Sub

        Private Sub SaveGroupPromoRequests()

            Dim oParameters As ArrayList
            Dim oGroupPromoRequest As CGroupPromoRequest

            'Remove the Deleted payments
            For Each oGroupPromoRequest In mcGroupPromoRequest.Removed
                oParameters = New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@GroupPromoRequest_ID", oGroupPromoRequest.ID))
                DataAccess.ExecuteCommand("sp_Events_GroupPromoRequestDelete", oParameters)
            Next

            'Add/Update all the others
            For Each oGroupPromoRequest In mcGroupPromoRequest
                oGroupPromoRequest.GroupLeaderID = Me.ID
                oGroupPromoRequest.Save()
            Next

        End Sub

        Private Sub SaveBulkRegistrations(ByVal oUser As CUser)

            Dim oParameters As ArrayList
            Dim oBulkRegistration As CGroupBulkRegistration

            'Remove the Deleted Bulk Registrations
            For Each oBulkRegistration In mcBulkRegistrations.Removed
                oParameters = New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@GroupBulkRegistration_ID", oBulkRegistration.ID))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))
                DataAccess.ExecuteCommand("sp_Events_GroupBulkRegistrationDelete", oParameters)
            Next

            'Add/Update all the others
            For Each oBulkRegistration In mcBulkRegistrations
                oBulkRegistration.GroupLeaderID = miGroupLeaderID
                oBulkRegistration.Save(oUser)
            Next

        End Sub

        Private Sub SaveBulkPurchase(ByVal oUser As CUser)

            Dim oParameters As ArrayList
            Dim oBulkPurchase As CGroupBulkPurchase

            'Remove the Deleted Bulk Registration Options
            For Each oBulkPurchase In mcBulkPurchase.Removed
                oParameters = New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@GroupBulkPurchase_ID", oBulkPurchase.ID))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))
                DataAccess.ExecuteCommand("sp_Events_GroupBulkPurchaseDelete", oParameters)
            Next

            'Add/Update all the others
            For Each oBulkPurchase In mcBulkPurchase
                oBulkPurchase.GroupLeaderID = miGroupLeaderID
                oBulkPurchase.Save(oUser)
            Next

        End Sub

        Public Sub SaveContacts(ByRef oUser As CUser)

            Dim oContact As CContact
            Dim sContactList As String = ""

            For Each oContact In mcContacts.Clone

                'If it's a new contact: we need to mark external->events as changed, so it saves.
                If oContact.ID = 0 Then
                    oContact.External.Events.Changed = True
                End If

                oContact.Save(oUser)
                If oContact.External.Deleted Then
                    mcContacts.Remove(oContact)
                End If
                sContactList += oContact.ID & ", "
            Next

            'Update the Group<->Contact relationships, by passing a string of Reg ID's to the update Stored Proc
            If sContactList <> "" Then
                sContactList = Left(sContactList, sContactList.Length - 2)
                DataAccess.ExecuteCommand("sp_Events_GroupUpdateContactList " & MSQLUtilities.SQL(ID) & ", " & MSQLUtilities.SQL(sContactList))
            End If

        End Sub

        Public Function HistoryRead() As DataTable

            Return DataAccess.GetDataTable("EXEC sp_Events_GroupHistoryRead " & miGroupLeaderID.ToString)

        End Function

        Public Sub HistoryAdd(ByVal oVenue As CVenue, ByVal oUser As CUser, ByVal sAction As String, ByVal sItem As String, ByVal sOldValue As String, ByVal sNewValue As String)

            Dim oConference As CConference = Nothing
            If oVenue IsNot Nothing Then oConference = oVenue.Conference

            Dim oParameters As New ArrayList

            oParameters.Add(New SqlClient.SqlParameter("@GroupLeader_ID", miGroupLeaderID))
            oParameters.Add(New SqlClient.SqlParameter("@Conference_ID", SQLObject(oConference)))
            oParameters.Add(New SqlClient.SqlParameter("@Venue_ID", SQLObject(oVenue)))
            oParameters.Add(New SqlClient.SqlParameter("@DateChanged", SQLDateTime(GetDateTime)))
            oParameters.Add(New SqlClient.SqlParameter("@User_ID", SQLObject(oUser)))
            oParameters.Add(New SqlClient.SqlParameter("@Action", sAction))
            oParameters.Add(New SqlClient.SqlParameter("@Item", sItem))
            oParameters.Add(New SqlClient.SqlParameter("@Table", String.Empty))
            oParameters.Add(New SqlClient.SqlParameter("@FieldName", String.Empty))
            oParameters.Add(New SqlClient.SqlParameter("@OldValue", String.Empty))
            oParameters.Add(New SqlClient.SqlParameter("@OldText", sOldValue))
            oParameters.Add(New SqlClient.SqlParameter("@NewValue", String.Empty))
            oParameters.Add(New SqlClient.SqlParameter("@NewText", sNewValue))

            DataAccess.ExecuteCommand("sp_Events_GroupHistoryInsert", oParameters)

        End Sub

        Public Function Notes() As CArrayList(Of CGroupNotes)
            Return mcNotes
        End Function
        Public Function Payments() As CArrayList(Of CGroupPayment)
            Return mcPayments
        End Function
        Public Function GroupPromo() As CArrayList(Of CGroupPromoRequest)
            Return mcGroupPromoRequest
        End Function
        Public Function GroupPromo(ByVal oConference As CConference) As CArrayList(Of CGroupPromoRequest)

            Dim oArrayList As New CArrayList(Of CGroupPromoRequest)
            Dim oGroupPromo As CGroupPromoRequest
            For Each oGroupPromo In mcGroupPromoRequest
                If oGroupPromo.ConferenceID = oConference.ID Then oArrayList.Add(oGroupPromo)
            Next
            Return oArrayList

        End Function
        Public Function BulkRegistrations() As CArrayList(Of CGroupBulkRegistration)
            Return mcBulkRegistrations
        End Function

        Public Function BulkRegistrationTypes(ByRef oConference As CConference) As Hashtable
            Dim oRegistrationTypes As New Hashtable
            Dim oVenues As Hashtable = Lists.External.Events.ConferenceVenues(oConference)

            For Each oBulkRegistration As CGroupBulkRegistration In BulkRegistrations()
                If oVenues.ContainsKey(oBulkRegistration.VenueID) Then
                    If Not oRegistrationTypes.ContainsKey(oBulkRegistration.RegistrationTypeID) Then
                        oRegistrationTypes(oBulkRegistration.RegistrationTypeID) = oBulkRegistration.RegistrationType
                    End If
                End If
            Next

            Return oRegistrationTypes
        End Function

        Public Function BulkPurchase() As CArrayList(Of CGroupBulkPurchase)
            Return mcBulkPurchase
        End Function

        Public Function BulkRegistrationsAvailable(ByVal oRegistrationType As CRegistrationType, Optional ByVal iVenueID As Integer = 0) As Integer

            Dim iTotal As Integer = 0
            Dim oBulkRegistration As CGroupBulkRegistration
            Dim oContact As CContact
            Dim oRegistration As CRegistration

            For Each oBulkRegistration In mcBulkRegistrations
                If oBulkRegistration.RegistrationTypeID = oRegistrationType.ID And (iVenueID = 0 Or oBulkRegistration.VenueID = iVenueID) Then
                    iTotal += oBulkRegistration.Quantity
                End If
            Next

            For Each oContact In mcContacts
                For Each oRegistration In oContact.External.Events.Registration
                    If oRegistration.RegistrationTypeID = oRegistrationType.ID And oRegistration.GroupLeaderID = Me.GroupLeader.ID And (iVenueID = 0 Or oRegistration.VenueID = iVenueID) Then
                        iTotal -= 1
                    End If
                Next
            Next

            Return iTotal

        End Function

        Public Function BulkRegistrationsAllocated(ByVal oRegistrationType As CRegistrationType, Optional ByVal iVenueID As Integer = 0) As Integer

            Dim iTotal As Integer = 0
            Dim oContact As CContact
            Dim oRegistration As CRegistration

            For Each oContact In mcContacts
                For Each oRegistration In oContact.External.Events.Registration
                    If oRegistration.RegistrationTypeID = oRegistrationType.ID And oRegistration.GroupLeaderID = Me.GroupLeader.ID And (iVenueID = 0 Or oRegistration.VenueID = iVenueID) Then
                        iTotal += 1
                    End If
                Next
            Next

            Return iTotal

        End Function

        Public Function BulkRegistrationsAvailableSummary(ByVal oVenue As CVenue, ByVal oSelectedRegistration As CRegistration) As ArrayList

            Dim oArrayList As New ArrayList
            Dim oBulkRegistration As CGroupBulkRegistration
            Dim oContact As CContact
            Dim oRegistration As CRegistration
            Dim oBulkRegistrationSummary As CGroupBulkRegistrationSummary = Nothing

            For Each oBulkRegistration In mcBulkRegistrations
                If oBulkRegistration.Venue.ID = oVenue.ID Then
                    Dim bFound As Boolean = False
                    For Each oBulkRegistrationSummary In oArrayList
                        If oBulkRegistrationSummary.VenueID = oBulkRegistration.VenueID And oBulkRegistrationSummary.RegistrationTypeID = oBulkRegistration.RegistrationTypeID Then
                            bFound = True
                            Exit For
                        End If
                    Next
                    If Not bFound Then
                        oBulkRegistrationSummary = New CGroupBulkRegistrationSummary
                        oBulkRegistrationSummary.VenueID = oBulkRegistration.VenueID
                        oBulkRegistrationSummary.RegistrationTypeID = oBulkRegistration.RegistrationTypeID
                        oArrayList.Add(oBulkRegistrationSummary)
                    End If
                    oBulkRegistrationSummary.Quantity += oBulkRegistration.Quantity
                End If
            Next

            For Each oContact In mcContacts
                For Each oRegistration In oContact.External.Events.Registration
                    If oSelectedRegistration Is Nothing OrElse oRegistration.ID <> oSelectedRegistration.ID Then
                        For Each oBulkRegistrationSummary In oArrayList
                            If oBulkRegistrationSummary.RegistrationTypeID = oRegistration.RegistrationType.ID Then
                                If oBulkRegistrationSummary.VenueID = oRegistration.Venue.ID Then
                                    If oRegistration.GroupLeaderID = miGroupLeaderID Then
                                        If oBulkRegistrationSummary.Quantity > 0 Then
                                            oBulkRegistrationSummary.Quantity -= 1
                                        End If
                                    End If
                                End If
                            End If
                        Next
                    End If
                Next
            Next

            'Remove items with zero available
            For Each oBulkRegistrationSummary In oArrayList.Clone
                If oBulkRegistrationSummary.Quantity = 0 Then oArrayList.Remove(oBulkRegistrationSummary)
            Next

            Return oArrayList

        End Function

        Public Function BulkRegistrationsTotal(ByRef oRegistrationType As CRegistrationType) As Integer

            Dim iTotal As Integer = 0
            Dim oBulkRegistration As CGroupBulkRegistration

            For Each oBulkRegistration In mcBulkRegistrations
                If oBulkRegistration.RegistrationTypeID = oRegistrationType.ID Then
                    iTotal += oBulkRegistration.Quantity
                End If
            Next

            Return iTotal

        End Function

        Public Function BulkPurchaseAvailable(ByVal oConference As CConference, ByVal iType As EnumBulkPurchaseType) As Integer

            Dim dTotal As Integer = 0
            Dim oBulkPurchase As CGroupBulkPurchase
            Dim oContact As CContact
            Dim oRegistration As CRegistration

            For Each oBulkPurchase In mcBulkPurchase
                If oBulkPurchase.Venue.Conference.ID = oConference.ID Then
                    Select Case iType
                        Case EnumBulkPurchaseType.Accommodation
                            dTotal += oBulkPurchase.AccommodationQuantity
                        Case EnumBulkPurchaseType.Catering
                            dTotal += oBulkPurchase.CateringQuantity
                        Case EnumBulkPurchaseType.LeadershipBreakfast
                            dTotal += oBulkPurchase.LeadershipBreakfastQuantity
                    End Select
                End If
            Next

            For Each oContact In mcContacts
                For Each oRegistration In oContact.External.Events.Registration
                    If oRegistration.Venue.Conference.ID = oConference.ID Then
                        If oRegistration.GroupLeaderID = Me.ID Then
                            Select Case iType
                                Case EnumBulkPurchaseType.Accommodation
                                    dTotal -= IIf(oRegistration.Accommodation, 1, 0)
                                Case EnumBulkPurchaseType.Catering
                                    dTotal -= IIf(oRegistration.Catering, 1, 0)
                                Case EnumBulkPurchaseType.LeadershipBreakfast
                                    dTotal -= IIf(oRegistration.LeadershipBreakfast, 1, 0)
                            End Select
                        End If
                    End If
                Next
            Next

            Return dTotal

        End Function

        Public Function Contacts() As CArrayList(Of CContact)
            Return mcContacts
        End Function

        Public Property ID() As Integer
            Get
                Return miGroupLeaderID
            End Get
            Set(ByVal value As Integer)
                miGroupLeaderID = value
            End Set
        End Property
        Public Property GroupLeader() As CContact
            Get
                If miGroupLeaderID > 0 And moGroupLeader Is Nothing Then
                    moGroupLeader = New CContact
                    moGroupLeader.LoadByID(miGroupLeaderID)
                End If
                Return moGroupLeader
            End Get
            Set(ByVal value As CContact)
                moGroupLeader = value
            End Set
        End Property
        Public Property GroupColeaders() As String
            Get
                Return msGroupColeaders
            End Get
            Set(ByVal Value As String)
                If msGroupColeaders <> Value Then mbChanged = True
                msGroupColeaders = Value
            End Set
        End Property
        Public Property GroupName() As String
            Get
                Return msGroupName
            End Get
            Set(ByVal Value As String)
                If msGroupName <> Value Then mbChanged = True
                msGroupName = Value
            End Set
        End Property
        Public Property YouthLeaderName() As String
            Get
                Return msYouthLeaderName
            End Get
            Set(ByVal Value As String)
                If msYouthLeaderName <> Value Then mbChanged = True
                msYouthLeaderName = Value
            End Set
        End Property
        Public Property GroupSizeID() As Integer
            Get
                Return miGroupSizeID
            End Get
            Set(ByVal Value As Integer)
                If miGroupSizeID <> Value Then mbChanged = True
                miGroupSizeID = Value
            End Set
        End Property
        Public ReadOnly Property GroupSize() As CGroupSize
            Get
                Return Lists.External.Events.GetGroupSizeByID(miGroupSizeID)
            End Get
        End Property
        Public Property GroupCreditStatusID() As Integer
            Get
                Return miGroupCreditStatusID
            End Get
            Set(ByVal Value As Integer)
                If miGroupCreditStatusID <> Value Then mbChanged = True
                miGroupCreditStatusID = Value
            End Set
        End Property
        Public ReadOnly Property GroupCreditStatus() As CGroupCreditStatus
            Get
                Return Lists.External.Events.GetGroupCreditStatusByID(miGroupCreditStatusID)
            End Get
        End Property
        Public Property GroupTypeID() As Integer
            Get
                Return miGroupTypeID
            End Get
            Set(ByVal Value As Integer)
                If miGroupTypeID <> Value Then mbChanged = True
                miGroupTypeID = Value
            End Set
        End Property
        Public ReadOnly Property GroupType() As CGroupType
            Get
                Return Lists.External.Events.GetGroupTypeByID(miGroupTypeID)
            End Get
        End Property
        Public Property Comments() As String
            Get
                Return msComments
            End Get
            Set(ByVal Value As String)
                If msComments <> Value Then mbChanged = True
                msComments = Value
            End Set
        End Property
        Public Property DetailsVerified() As Boolean
            Get
                Return mbDetailsVerified
            End Get
            Set(ByVal value As Boolean)
                mbDetailsVerified = value
            End Set
        End Property
        Public Property Deleted() As Boolean
            Get
                Return mbDeleted
            End Get
            Set(ByVal Value As Boolean)
                If mbDeleted <> Value Then mbChanged = True
                mbDeleted = Value
            End Set
        End Property
        Public Property CreationDate() As Date
            Get
                Return mdCreationDate
            End Get
            Set(ByVal Value As Date)
                If mdCreationDate <> Value Then mbChanged = True
                mdCreationDate = Value
            End Set
        End Property
        Public Property ModificationDate() As Date
            Get
                Return mdModificationDate
            End Get
            Set(ByVal Value As Date)
                If mdModificationDate <> Value Then mbChanged = True
                mdModificationDate = Value
            End Set
        End Property
        Public Property CreatedByID() As Integer
            Get
                Return miCreatedByID
            End Get
            Set(ByVal Value As Integer)
                If miCreatedByID <> Value Then mbChanged = True
                miCreatedByID = Value
            End Set
        End Property
        Public ReadOnly Property CreatedBy() As CUser
            Get
                Return Lists.GetUserByID(miCreatedByID)
            End Get
        End Property
        Public Property ModifiedByID() As Integer
            Get
                Return miModifiedByID
            End Get
            Set(ByVal Value As Integer)
                If miModifiedByID <> Value Then mbChanged = True
                miModifiedByID = Value
            End Set
        End Property
        Public ReadOnly Property ModifiedBy() As CUser
            Get
                Return Lists.GetUserByID(miModifiedByID)
            End Get
        End Property
        ReadOnly Property GUID() As String
            Get
                Return msGUID
            End Get
        End Property

        Public ReadOnly Property Changed() As Boolean
            Get
                Return ChangedSansNotes Or mcNotes.Changed
            End Get
        End Property

        Public ReadOnly Property ChangedSansNotes() As Boolean
            Get
                If mbChanged Then Return True

                If moGroupLeader IsNot Nothing Then
                    If moGroupLeader.Changed Then Return True
                End If

                If mcPayments.Changed Then Return True
                If mcGroupPromoRequest.Changed Then Return True
                If mcBulkRegistrations.Changed Then Return True
                If mcBulkPurchase.Changed Then Return True
                If mcContacts.Changed Then Return True

                Dim oPayment As CGroupPayment
                For Each oPayment In mcPayments
                    If oPayment.Changed Then Return True
                Next
                Dim oPromoRequest As CGroupPromoRequest
                For Each oPromoRequest In mcGroupPromoRequest
                    If oPromoRequest.Changed Then Return True
                Next
                Dim oBulkRegistration As CGroupBulkRegistration
                For Each oBulkRegistration In mcBulkRegistrations
                    If oBulkRegistration.Changed Then Return True
                Next
                Dim oBulkPurchase As CGroupBulkPurchase
                For Each oBulkPurchase In mcBulkPurchase
                    If oBulkPurchase.Changed Then Return True
                Next
                Dim oContact As CContact
                For Each oContact In mcContacts
                    If oContact.Changed Then Return True
                Next

                Return False

            End Get
        End Property

        Public ReadOnly Property HasActiveGroupRegistration() As Boolean
            Get
                Dim oConference As CConference
                For Each oConference In Lists.External.Events.ConferencesCurrent.Values
                    If HasActiveGroupRegistration(oConference) Then Return True
                Next
                Return False
            End Get
        End Property

        Public ReadOnly Property HasActiveGroupRegistration(ByVal oConference As CConference, Optional ByVal bIncludeComboRegistrations As Boolean = True) As Boolean
            Get
                For Each oContact As CContact In mcContacts
                    If oContact.External.Events.HasCurrentRegistration(miGroupLeaderID, oConference) Then Return True
                Next
                For Each oBulkRegistration As CGroupBulkRegistration In mcBulkRegistrations
                    If bIncludeComboRegistrations Then
                        If oBulkRegistration.Venue.Conference.ID = oConference.ID Then Return True
                    Else
                        If oBulkRegistration.Venue.Conference.ID = oConference.ID And Not oBulkRegistration.RegistrationType.IsComboRegistrationSecondary(oConference) Then Return True
                    End If
                Next
                For Each oBulkPurchase As CGroupBulkPurchase In mcBulkPurchase
                    If oBulkPurchase.Venue.Conference.ID = oConference.ID Then Return True
                Next
                Return False
            End Get
        End Property

        Public Function AmountTotal(ByRef oConference As CConference, Optional ByVal bIncludeComboRegistrations As Boolean = False) As Decimal

            Dim oContact As CContact
            Dim oRegistration As CRegistration
            Dim dTotal As Decimal = 0
            Dim dDiscount As Decimal = 0
            Dim bFound As Boolean = False

            Dim oBulkRegistration As CGroupBulkRegistration = Nothing
            Dim oBulkRegistrationSummary As CGroupBulkRegistrationSummary = Nothing
            Dim cBulkRegistration As New CArrayList(Of CGroupBulkRegistrationSummary)

            Dim oBulkPurchase As CGroupBulkPurchase = Nothing
            Dim oBulkPurchaseSummary As CGroupBulkPurchaseSummary = Nothing
            Dim cBulkPurchase As New CArrayList(Of CGroupBulkPurchaseSummary)

            'First, create a summary array of all the Bulk registrations
            For Each oBulkRegistration In mcBulkRegistrations
                If oBulkRegistration.Venue.Conference.ID = oConference.ID Or (bIncludeComboRegistrations And oBulkRegistration.RegistrationType.IsComboRegistrationSecondary(oConference)) Then
                    bFound = False
                    For Each oBulkRegistrationSummary In cBulkRegistration
                        If oBulkRegistrationSummary.VenueID = oBulkRegistration.VenueID And oBulkRegistrationSummary.RegistrationTypeID = oBulkRegistration.RegistrationTypeID Then
                            bFound = True
                            Exit For
                        End If
                    Next
                    If Not bFound Then
                        oBulkRegistrationSummary = New CGroupBulkRegistrationSummary
                        oBulkRegistrationSummary.VenueID = oBulkRegistration.VenueID
                        oBulkRegistrationSummary.RegistrationTypeID = oBulkRegistration.RegistrationTypeID
                        cBulkRegistration.Add(oBulkRegistrationSummary)
                    End If
                    oBulkRegistrationSummary.Quantity += oBulkRegistration.Quantity
                End If
            Next

            'Create a summary array of all the Bulk Purchases
            For Each oBulkPurchase In mcBulkPurchase
                If oBulkPurchase.Venue.Conference.ID = oConference.ID Then
                    bFound = False
                    For Each oBulkPurchaseSummary In cBulkPurchase
                        If oBulkPurchase.VenueID = oBulkPurchaseSummary.VenueID Then
                            bFound = True
                            Exit For
                        End If
                    Next
                    If Not bFound Then
                        oBulkPurchaseSummary = New CGroupBulkPurchaseSummary
                        oBulkPurchaseSummary.VenueID = oBulkPurchase.VenueID
                        cBulkPurchase.Add(oBulkPurchaseSummary)
                    End If
                    oBulkPurchaseSummary.AccommodationQuantity += oBulkPurchase.AccommodationQuantity
                    oBulkPurchaseSummary.CateringQuantity += oBulkPurchase.CateringQuantity
                    oBulkPurchaseSummary.LeadershipBreakfastQuantity += oBulkPurchase.LeadershipBreakfastQuantity
                End If
            Next


            'Loop through each contact, and see if we can cover the contact's registration with a 
            'bulk registration item. If not, add it to the total cost
            For Each oContact In Contacts()
                For Each oRegistration In oContact.External.Events.Registration
                    If oRegistration.GroupLeaderID = Me.ID Then
                        If oRegistration.Venue.Conference.ID = oConference.ID Or (bIncludeComboRegistrations And oRegistration.RegistrationType.IsComboRegistrationSecondary(oConference)) Then

                            bFound = False
                            For Each oBulkRegistrationSummary In cBulkRegistration
                                If oBulkRegistrationSummary.VenueID = oRegistration.Venue.ID And oBulkRegistrationSummary.RegistrationTypeID = oRegistration.RegistrationType.ID Then
                                    bFound = True
                                    If oBulkRegistrationSummary.Quantity > 0 Then
                                        oBulkRegistrationSummary.Quantity -= 1
                                    Else
                                        dTotal += oRegistration.RegistrationPrice
                                    End If
                                End If
                            Next
                            If Not bFound Then
                                dTotal += oRegistration.RegistrationPrice
                            End If

                            bFound = False
                            For Each oBulkPurchaseSummary In cBulkPurchase
                                If oBulkPurchaseSummary.VenueID = oRegistration.Venue.ID Then
                                    bFound = True

                                    If oRegistration.Accommodation Then
                                        If oBulkPurchaseSummary.AccommodationQuantity > 0 Then
                                            oBulkPurchaseSummary.AccommodationQuantity -= 1
                                        Else
                                            dTotal += oRegistration.Venue.Conference.AccommodationCost
                                        End If
                                    End If

                                    If oRegistration.Catering Then
                                        If oBulkPurchaseSummary.CateringQuantity > 0 Then
                                            oBulkPurchaseSummary.CateringQuantity -= 1
                                        Else
                                            dTotal += oRegistration.Venue.Conference.CateringCost
                                        End If
                                    End If

                                    If oRegistration.LeadershipBreakfast Then
                                        If oBulkPurchaseSummary.LeadershipBreakfastQuantity > 0 Then
                                            oBulkPurchaseSummary.LeadershipBreakfastQuantity -= 1
                                        Else
                                            dTotal += oRegistration.Venue.Conference.LeadershipBreakfastCost
                                        End If
                                    End If

                                End If
                            Next

                            dDiscount += oRegistration.RegistrationDiscount / 100 * oRegistration.RegistrationPrice
                        End If
                    End If
                Next
            Next

            'Add in the costs of the bulk registration items
            For Each oBulkRegistration In BulkRegistrations()
                If oBulkRegistration.Venue.Conference.ID = oConference.ID Or (bIncludeComboRegistrations And oBulkRegistration.RegistrationType.IsComboRegistrationSecondary(oConference)) Then
                    dTotal += oBulkRegistration.TotalCost
                End If
            Next
            For Each oBulkPurchase In BulkPurchase()
                If oBulkPurchase.Venue.Conference.ID = oConference.ID Then
                    dTotal += oBulkPurchase.TotalCost
                End If
            Next

            'Add cost of Preorders from each of the contacts
            Dim oPreOrder As CPreOrder
            For Each oContact In Contacts()
                For Each oRegistration In oContact.External.Events.Registration
                    If oRegistration.Venue.Conference.ID = oConference.ID Then
                        If oRegistration.GroupLeaderID = Me.ID Then
                            For Each oPreOrder In oRegistration.PreOrder
                                dTotal += oPreOrder.Price(oRegistration.Venue.Conference.Country)
                            Next
                        End If
                    End If
                Next
            Next

            Return dTotal - dDiscount

        End Function

        Public Function AmountTotalCurrent() As Decimal

            Dim oConference As CConference
            Dim dOwing As Decimal
            For Each oConference In Lists.External.Events.Conferences.Values
                If oConference.ConferenceActive Then
                    dOwing += AmountTotal(oConference)
                End If
            Next
            Return dOwing

        End Function

        Public Function AmountOwing(ByRef oConference As CConference, Optional ByVal bIncludeComboRegistrations As Boolean = False) As Decimal
            Return AmountTotal(oConference, bIncludeComboRegistrations) - AmountPaid(oConference)
        End Function
        Public Function AmountOwingCurrent() As Decimal
            Return AmountTotalCurrent() - AmountPaidCurrent()
        End Function

        Public Function AmountPaid(ByRef oConference As CConference) As Decimal
            If oConference Is Nothing Then Return False
            Dim oPayment As CGroupPayment
            For Each oPayment In mcPayments
                If oConference Is Nothing OrElse oPayment.Conference.ID = oConference.ID Then
                    AmountPaid += oPayment.PaymentAmount
                End If
            Next
            Return AmountPaid
        End Function
        Public Function AmountPaidCurrent() As Decimal

            Dim oConference As CConference
            Dim dOwing As Decimal
            For Each oConference In Lists.External.Events.Conferences.Values
                If oConference.ConferenceActive Then
                    dOwing += AmountPaid(oConference)
                End If
            Next
            Return dOwing

        End Function

        Public Function PaymentsMade(ByRef oConference As CConference) As Integer

            If oConference Is Nothing Then Return False
            Dim oPayment As CGroupPayment
            For Each oPayment In mcPayments
                If oPayment.Conference.ID = oConference.ID Then
                    PaymentsMade += 1
                End If
            Next
            Return True

        End Function

        Public Function BulkRegistrationsTotal(ByRef oVenue As CVenue) As Integer

            Dim oBulkRegistration As CGroupBulkRegistration
            Dim iTotal As Integer

            'Add up the total BulkRegistrations of a particular Registration Type
            For Each oBulkRegistration In BulkRegistrations()
                If oBulkRegistration.Venue.ID = oVenue.ID Then
                    iTotal += oBulkRegistration.Quantity
                End If
            Next

            Return iTotal

        End Function

        Public Function RegistrationCount(ByVal oConference As CConference) As Integer

            Dim oContact As CContact
            Dim oRegistration As CRegistration
            Dim iTotal As Integer
            For Each oContact In Contacts()
                For Each oRegistration In oContact.External.Events.Registration
                    If oRegistration.GroupLeaderID = ID Then
                        If oRegistration.RegistrationType.Conference.ID = oConference.ID Then
                            iTotal += 1
                        End If
                    End If
                Next
            Next

            Return iTotal

        End Function

        Private Class CGroupBulkRegistrationSummary
            Public VenueID As Integer
            Public RegistrationTypeID As Integer
            Public Quantity As Integer
            Public ReadOnly Property Summary() As String
                Get
                    Return Lists.External.Events.GetVenueByID(VenueID).Name & " - " & _
                           Lists.External.Events.GetRegistrationTypeByID(RegistrationTypeID).Name & " - $" & _
                           Format(Lists.External.Events.GetRegistrationTypeByID(RegistrationTypeID).RegistrationCost, "#0") & " - (" & _
                           Quantity.ToString + " Available)"
                End Get
            End Property
            Public ReadOnly Property ID() As Integer
                Get
                    Return RegistrationTypeID
                End Get
            End Property
        End Class

        Private Class CGroupBulkPurchaseSummary
            Public VenueID As Integer
            Public AccommodationQuantity As Integer
            Public CateringQuantity As Integer
            Public LeadershipBreakfastQuantity As Integer
        End Class

    End Class

End Namespace