Namespace External.Events

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CGroupBulkRegistration

        Private miGroupBulkRegistrationID As Integer
        Private miGroupLeaderID As Integer
        Private moGroup As CGroup
        Private miVenueID As Integer
        Private miRegistrationTypeID As Integer
        Private miQuantity As Integer
        Private mdDateAdded As Date
        Private msGUID As String

        Private mbChanged As Boolean

        Public Sub New()

            miGroupBulkRegistrationID = 0
            miGroupLeaderID = 0
            miVenueID = 0
            miRegistrationTypeID = 0
            miQuantity = 0
            mdDateAdded = Nothing
            msGUID = System.Guid.NewGuid.ToString
            mbChanged = False

        End Sub

        Public Sub New(ByVal oDataRow As DataRow, ByVal oGroup As CGroup)

            Me.New()

            miGroupBulkRegistrationID = oDataRow("GroupBulkRegistration_ID")
            miGroupLeaderID = oDataRow("GroupLeader_ID")
            miVenueID = oDataRow("Venue_ID")
            miRegistrationTypeID = oDataRow("RegistrationType_ID")
            miQuantity = oDataRow("Quantity")
            mdDateAdded = oDataRow("DateAdded")
            mbChanged = False

            moGroup = oGroup

        End Sub

        Public Sub Save(ByVal oUser As CUser)

            If mbChanged Then

                Dim oParameters As New ArrayList

                oParameters.Add(New SqlClient.SqlParameter("@GroupLeader_ID", miGroupLeaderID))
                oParameters.Add(New SqlClient.SqlParameter("@Venue_ID", miVenueID))
                oParameters.Add(New SqlClient.SqlParameter("@RegistrationType_ID", miRegistrationTypeID))
                oParameters.Add(New SqlClient.SqlParameter("@Quantity", miQuantity))
                oParameters.Add(New SqlClient.SqlParameter("@DateAdded", SQLDateTime(mdDateAdded)))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))

                If Me.ID = 0 Then
                    miGroupBulkRegistrationID = DataAccess.GetValue("sp_Events_GroupBulkRegistrationInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@GroupBulkRegistration_ID", miGroupBulkRegistrationID))
                    DataAccess.ExecuteCommand("sp_Events_GroupBulkRegistrationUpdate", oParameters)
                End If

                mbChanged = False

            End If

        End Sub

        ReadOnly Property ID() As Integer
            Get
                Return miGroupBulkRegistrationID
            End Get
        End Property
        Property GroupLeaderID() As Integer
            Get
                Return miGroupLeaderID
            End Get
            Set(ByVal value As Integer)
                If miGroupLeaderID <> value Then mbChanged = True
                miGroupLeaderID = value
            End Set
        End Property
        Property VenueID() As Integer
            Get
                Return miVenueID
            End Get
            Set(ByVal value As Integer)
                If miVenueID <> value Then mbChanged = True
                miVenueID = value
            End Set
        End Property
        Public ReadOnly Property Venue() As CVenue
            Get
                Return Lists.External.Events.GetVenueByID(miVenueID)
            End Get
        End Property
        Property RegistrationTypeID() As Integer
            Get
                Return miRegistrationTypeID
            End Get
            Set(ByVal value As Integer)
                If miRegistrationTypeID <> value Then mbChanged = True
                miRegistrationTypeID = value
            End Set
        End Property
        Public ReadOnly Property RegistrationType() As CRegistrationType
            Get
                Return Lists.External.Events.GetRegistrationTypeByID(miRegistrationTypeID)
            End Get
        End Property
        Property Quantity() As Integer
            Get
                Return miQuantity
            End Get
            Set(ByVal Value As Integer)
                If miQuantity <> Value Then mbChanged = True
                miQuantity = Value
            End Set
        End Property
        Property DateAdded() As Date
            Get
                Return mdDateAdded
            End Get
            Set(ByVal Value As Date)
                If mdDateAdded <> Value Then mbChanged = True
                mdDateAdded = Value
            End Set
        End Property
        ReadOnly Property TotalCost() As Decimal
            Get
                Return RegistrationType.RegistrationCost * miQuantity
            End Get
        End Property
        ReadOnly Property GUID() As String
            Get
                GUID = msGUID
            End Get
        End Property

        Public ReadOnly Property Changed() As Boolean
            Get
                Return mbChanged
            End Get
        End Property

    End Class

End Namespace