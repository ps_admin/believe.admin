Namespace External.Events

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CGroupNotes

        Private miGroupNotesID As Integer
        Private miGroupLeaderID As Integer
        Private mdDateAdded As Date
        Private miUserID As Integer
        Private msNotes As String

        Private mbChanged As Boolean

        Public Sub New()

            miGroupNotesID = 0
            miGroupLeaderID = 0
            mdDateAdded = Nothing
            miUserID = 0
            msNotes = String.Empty

            mbChanged = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miGroupNotesID = oDataRow("GroupNotes_ID")
            miGroupLeaderID = oDataRow("GroupLeader_ID")
            mdDateAdded = oDataRow("DateAdded")
            miUserID = oDataRow("User_ID")
            msNotes = oDataRow("Notes")

            mbChanged = False

        End Sub

        Public Sub Save()

            If mbChanged Then

                Dim oParameters As New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@GroupLeader_ID", miGroupLeaderID))
                oParameters.Add(New SqlClient.SqlParameter("@DateAdded", SQLDateTime(mdDateAdded)))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", miUserID))
                oParameters.Add(New SqlClient.SqlParameter("@Notes", msNotes))

                If Me.ID = 0 Then
                    miGroupNotesID = DataAccess.GetValue("sp_Events_GroupNotesInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@GroupNotes_ID", miGroupNotesID))
                    DataAccess.ExecuteCommand("sp_Events_GroupNotesUpdate", oParameters)
                End If

                mbChanged = False

            End If

        End Sub
        Public ReadOnly Property ID() As Integer
            Get
                Return miGroupNotesID
            End Get
        End Property
        Public Property GroupLeaderID() As Integer
            Get
                Return miGroupLeaderID
            End Get
            Set(ByVal value As Integer)
                miGroupLeaderID = value
            End Set
        End Property
        Public Property DateAdded() As Date
            Get
                Return mdDateAdded
            End Get
            Set(ByVal value As Date)
                If mdDateAdded <> value Then mbChanged = True
                mdDateAdded = value
            End Set
        End Property
        Public Property UserID() As Integer
            Get
                Return miUserID
            End Get
            Set(ByVal value As Integer)
                miUserID = value
            End Set
        End Property
        Public ReadOnly Property User() As CUser
            Get
                Return Lists.GetUserByID(miUserID)
            End Get
        End Property
        Public Property Notes() As String
            Get
                Return msNotes
            End Get
            Set(ByVal value As String)
                If msNotes <> value Then mbChanged = True
                msNotes = value
            End Set
        End Property

    End Class

End Namespace