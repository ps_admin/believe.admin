Namespace External.Events

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CLists
        Private moAccommodationVenue As Hashtable
        Private moAccessLevel As Hashtable
        Private moChurchList As Hashtable
        Private moConference As Hashtable
        Private moConferencePreOrder As Hashtable
        Private moCurrentConference As External.Events.CConference           'Holds the most current Conference
        Private moConferenceVenue As Hashtable
        Private moElective As Hashtable
        Private moGroupList As Hashtable
        Private moGroupCreditStatus As Hashtable
        Private moGroupPromo As Hashtable
        Private moGroupSize As Hashtable
        Private moGroupType As Hashtable
        Private moGroupStatus As Hashtable
        Private moPOSRegister As Hashtable
        Private moProduct As Hashtable
        Private moProductPreOrder As Hashtable
        Private moReferredBy As Hashtable
        Private moRegistrationOptionName As Hashtable
        Private moRegistrationOptionValue As Hashtable
        Private moRegistrationType As Hashtable
        Private moRegistrationTypeCombo As Hashtable
        Private moRegistrationTypeComboDetail As Hashtable
        Private moRegistrationTypeSaleGroup As Hashtable
        Private moRegistrationTypeReportGroup As Hashtable
        Private moRegistrationULG As Hashtable
        Private moVolunteer As Hashtable
        Private moVolunteerDepartment As Hashtable

        Private gdVenueLastRefresh As Date

        Public ReadOnly Property AccessLevels(Optional ByVal oConferenceID As Integer = 0) As Hashtable
            Get
                If moAccessLevel Is Nothing Then
                    Dim sSQL As String = "SELECT * FROM Events_AccessLevel"
                    If oConferenceID > 0 Then sSQL &= " WHERE AccessLevel_ID IN (SELECT DISTINCT AccessLevel_ID FROM Events_ConferenceDepartmentMapping WHERE Conference_ID = " & oConferenceID & ")"

                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable(sSQL)

                    moAccessLevel = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oAccessLevel As New CAccessLevel(oDataRow)
                        moAccessLevel.Add(oAccessLevel.ID, oAccessLevel)
                    Next
                End If
                Return moAccessLevel
            End Get
        End Property

        Public ReadOnly Property AccommodationVenues() As Hashtable
            Get
                If moAccommodationVenue Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Events_Accommodation")

                    moAccommodationVenue = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oAccommodation As New External.Events.CAccommodationVenue(oDataRow)
                        moAccommodationVenue.Add(oAccommodation.ID, oAccommodation)
                    Next
                End If
                Return moAccommodationVenue
            End Get
        End Property
        

        Public ReadOnly Property ChurchList() As Hashtable
            Get
                If moChurchList Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT DISTINCT ChurchName FROM Common_ContactExternal WHERE Deleted = 0 and ChurchName <>'' ORDER BY ChurchName")

                    moChurchList = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        moChurchList.Add(oDataRow("ChurchName"), oDataRow("ChurchName"))
                    Next
                End If
                Return moChurchList
            End Get
        End Property

        Public ReadOnly Property Conferences() As Hashtable
            Get
                If moConference Is Nothing Then
                    LoadConferences()
                ElseIf moConference.Count < 1 Then
                    LoadConferences()
                End If
                Return moConference
            End Get
        End Property

        Private Sub LoadConferences()
            Dim oDataRow As DataRow
            Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Events_Conference ORDER BY SortOrder DESC")

            moConference = New Hashtable
            For Each oDataRow In oDataTable.Rows
                Dim oConference As New External.Events.CConference(oDataRow)
                moConference.Add(oConference.ID, oConference)
            Next
        End Sub

        Public ReadOnly Property ConferencesCurrent() As Hashtable
            Get
                Dim oEvent As External.Events.CConference
                Dim oConferencesCurrent As New Hashtable
                For Each oEvent In Conferences.Values
                    If oEvent.ConferenceActive Then
                        oConferencesCurrent.Add(oEvent.ID, oEvent)
                    End If
                Next
                Return oConferencesCurrent
            End Get
        End Property

        Public ReadOnly Property ConferenceCurrent(Optional ByVal oCountry As CCountry = Nothing) As External.Events.CConference
            Get
                Dim oConference As External.Events.CConference
                Dim dStartDate As Date = Nothing

                'First check for the most current Conference that is active.
                For Each oConference In Conferences.Values
                    If oCountry Is Nothing OrElse oCountry.ID = oConference.Country.ID Then         'Only conferences where the country matches the parameter given
                        If oConference.ConferenceActive Then
                            If dStartDate = Nothing Then
                                dStartDate = oConference.ConferenceFirstStartDate
                                moCurrentConference = oConference
                            End If
                            If dStartDate <> Nothing And oConference.ConferenceFirstStartDate <> Nothing Then
                                If oConference.ConferenceFirstStartDate < dStartDate Then
                                    dStartDate = oConference.ConferenceFirstStartDate
                                    moCurrentConference = oConference
                                End If
                            End If
                        End If
                    End If
                Next

                'If we can't find an active one, select the last active Conference
                If moCurrentConference Is Nothing Then
                    dStartDate = Nothing
                    For Each oConference In Conferences.Values
                        If oCountry Is Nothing OrElse oCountry.ID = oConference.Country.ID Then     'Only conferences where the country matches the parameter given
                            If dStartDate = Nothing Then
                                dStartDate = oConference.ConferenceFirstStartDate
                                moCurrentConference = oConference
                            End If
                            If dStartDate <> Nothing And oConference.ConferenceFirstStartDate <> Nothing Then
                                dStartDate = oConference.ConferenceFirstStartDate
                                moCurrentConference = oConference
                            End If
                        End If
                    Next
                End If
                Return moCurrentConference

            End Get
        End Property

        Public ReadOnly Property ConferenceVenues() As Hashtable
            Get

                Dim oDataTable As DataTable
                Dim oDataRow As DataRow

                If moConferenceVenue Is Nothing Then

                    'If we haven't initialised venues yet, do this now.
                    oDataTable = DataAccess.GetDataTable("EXEC sp_Events_VenueSelectAll")

                    moConferenceVenue = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oVenue As New External.Events.CVenue(oDataRow)
                        moConferenceVenue.Add(oVenue.ID, oVenue)
                    Next
                    gdVenueLastRefresh = GetDateTime

                ElseIf DateAdd(DateInterval.Minute, 10, gdVenueLastRefresh) < GetDateTime Then

                    'Just refresh each venue (to update current contacts), don't create new.
                    oDataTable = DataAccess.GetDataTable("EXEC sp_Events_VenueSelectAll")

                    Dim oVenue As External.Events.CVenue
                    For Each oDataRow In oDataTable.Rows
                        If moConferenceVenue.ContainsKey(oDataRow("Venue_ID")) Then
                            oVenue = moConferenceVenue(oDataRow("Venue_ID"))
                            oVenue.LoadDataRow(oDataRow)
                        Else
                            oVenue = New External.Events.CVenue(oDataRow)
                            moConferenceVenue.Add(oVenue.ID, oVenue)
                        End If
                    Next
                    gdVenueLastRefresh = GetDateTime

                End If

                Return moConferenceVenue

            End Get
        End Property

        Public ReadOnly Property ConferenceVenues(ByVal oConference As External.Events.CConference) As Hashtable
            Get
                Dim oVenue As External.Events.CVenue
                Dim oConferenceVenues As New Hashtable
                For Each oVenue In ConferenceVenues().Values
                    If oConference Is Nothing OrElse oVenue.Conference.ID = oConference.ID Then
                        oConferenceVenues.Add(oVenue.ID, oVenue)
                    End If
                Next
                Return oConferenceVenues
            End Get
        End Property
        Public ReadOnly Property ConferenceVenuesCurrent(ByVal oConference As External.Events.CConference) As Hashtable
            Get
                Dim oVenue As External.Events.CVenue
                Dim oConferenceVenuesCurrent As New Hashtable
                For Each oVenue In ConferenceVenues(oConference).Values
                    If oVenue.Conference.ConferenceActive Then
                        oConferenceVenuesCurrent.Add(oVenue.ID, oVenue)
                    End If
                Next
                Return oConferenceVenuesCurrent
            End Get
        End Property

        Public ReadOnly Property Elective() As Hashtable
            Get
                If moElective Is Nothing Then

                    Dim oDataRow As DataRow
                    Dim oDataSet As New DataSet

                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Events_Elective")
                    oDataTable.TableName = "Elective"
                    Dim oDataTableVenue As DataTable = DataAccess.GetDataTable("SELECT * FROM Events_RegistrationTypeElective")
                    oDataTableVenue.TableName = "RegistrationTypeElective"

                    oDataSet.Tables.Add(oDataTable)
                    oDataSet.Tables.Add(oDataTableVenue)

                    oDataSet.Relations.Add("RegistrationType", oDataSet.Tables("Elective").Columns("Elective_ID"), oDataSet.Tables("RegistrationTypeElective").Columns("Elective_ID"))

                    moElective = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oElective As New CElective(oDataRow)
                        moElective.Add(oElective.ID, oElective)
                    Next
                End If
                Return moElective
            End Get
        End Property

        Public ReadOnly Property ElectiveByRegistrationType(ByVal oRegistrationType As External.Events.CRegistrationType) As ArrayList
            Get
                Dim oArrayList As New ArrayList
                Dim oElective As External.Events.CElective
                Dim oRegistrationTypeElective As External.Events.CRegistrationType

                For Each oElective In Elective.Values
                    For Each oRegistrationTypeElective In oElective.RegistrationType
                        If oRegistrationTypeElective.ID = oRegistrationType.ID Then
                            oArrayList.Add(oElective)
                        End If
                    Next
                Next
                oArrayList.Sort(New CSorterBySortOrder)
                Return oArrayList
            End Get
        End Property
        Public ReadOnly Property GroupConcise() As Hashtable
            Get
                If moGroupList Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT GroupLeader_ID, (c.FirstName + ' ' + c.LastName) AS GroupName FROM Events_Group g LEFT JOIN Common_Contact c ON g.GroupLeader_ID = c.Contact_ID WHERE Deleted=0 ORDER BY GroupName")

                    moGroupList = New Hashtable
                    moGroupList.Add(0, New External.Events.CGroupConcise(0, ""))
                    For Each oDataRow In oDataTable.Rows
                        Dim oGroup As New External.Events.CGroupConcise
                        oGroup.ID = oDataRow("GroupLeader_ID")
                        If IsDBNull(oDataRow("GroupName")) = True Then
                            oGroup.GroupName = ""
                        Else
                            oGroup.GroupName = oDataRow("GroupName")
                        End If

                        moGroupList.Add(oGroup.ID, oGroup)
                    Next
                End If
                Return moGroupList
            End Get
        End Property

        Public ReadOnly Property GroupCreditStatus() As Hashtable
            Get
                If moGroupCreditStatus Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Events_GroupCreditStatus")

                    moGroupCreditStatus = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oGroupCreditStatus As New External.Events.CGroupCreditStatus(oDataRow)
                        moGroupCreditStatus.Add(oGroupCreditStatus.ID, oGroupCreditStatus)
                    Next
                End If
                Return moGroupCreditStatus
            End Get
        End Property

        Public ReadOnly Property GroupPromo() As Hashtable
            Get
                If moGroupPromo Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Events_GroupPromo")

                    moGroupPromo = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oGroupPromo As New CGroupPromo(oDataRow)
                        moGroupPromo.Add(oGroupPromo.ID, oGroupPromo)
                    Next
                End If
                Return moGroupPromo
            End Get
        End Property
        Public ReadOnly Property GroupSize() As Hashtable
            Get
                If moGroupSize Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Events_GroupSize ORDER BY SortOrder")

                    moGroupSize = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oGroupSize As New External.Events.CGroupSize(oDataRow)
                        moGroupSize.Add(oGroupSize.ID, oGroupSize)
                    Next
                End If
                Return moGroupSize
            End Get
        End Property

        Public ReadOnly Property GroupType() As Hashtable
            Get
                If moGroupType Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Events_GroupType ORDER BY Name")

                    moGroupType = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oGroupType As New External.Events.CGroupType(oDataRow)
                        moGroupType.Add(oGroupType.ID, oGroupType)
                    Next
                End If
                Return moGroupType
            End Get
        End Property

        Public ReadOnly Property Products() As Hashtable
            Get
                If moProduct Is Nothing Then

                    Dim oDataRow As DataRow
                    Dim oTableMappings As New ArrayList
                    oTableMappings.Add(New System.Data.Common.DataTableMapping("Table", "Product"))
                    oTableMappings.Add(New System.Data.Common.DataTableMapping("Table1", "ProductPrice"))

                    Dim oDataSet As DataSet = DataAccess.GetDataSet("sp_Events_ProductSelectAll", oTableMappings)
                    oDataSet.Relations.Add("ProductPrice", oDataSet.Tables("Product").Columns("Product_ID"), oDataSet.Tables("ProductPrice").Columns("Product_ID"))

                    moProduct = New Hashtable
                    For Each oDataRow In oDataSet.Tables(0).Rows
                        Dim oProduct As New CProduct(oDataRow, oDataRow.GetChildRows("ProductPrice"))
                        moProduct.Add(oProduct.ID, oProduct)
                    Next
                End If
                Return moProduct
            End Get
        End Property
        Public ReadOnly Property ProductsCurrent() As Hashtable
            Get
                Dim oProduct As CProduct
                Dim oProductList As New Hashtable

                For Each oProduct In Products.Values
                    If Not oProduct.Deleted Then
                        oProductList.Add(oProduct.ID, oProduct)
                    End If
                Next
                Return oProductList
            End Get
        End Property
        Public ReadOnly Property ProductsPreOrder() As Hashtable
            Get
                If moConferencePreOrder Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Events_ConferencePreOrder")

                    moConferencePreOrder = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oConferencePreOrder As New External.Events.CConferencePreOrder(oDataRow)
                        moConferencePreOrder.Add(oConferencePreOrder.ID, oConferencePreOrder)
                    Next
                End If
                Return moConferencePreOrder
            End Get
        End Property

        Public ReadOnly Property POSRegisters() As Hashtable
            Get
                If moPOSRegister Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Events_POSRegister")

                    moPOSRegister = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oPOSRegister As New External.Events.CPOSRegister(oDataRow)
                        moPOSRegister.Add(oPOSRegister.ID, oPOSRegister)
                    Next
                End If
                Return moPOSRegister
            End Get
        End Property

        Public ReadOnly Property RegistrationOptionNames() As Hashtable
            Get
                If moRegistrationOptionName Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Events_RegistrationOptionName")

                    moRegistrationOptionName = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oRegistrationOptionName As New External.Events.CRegistrationOptionName(oDataRow)
                        moRegistrationOptionName.Add(oRegistrationOptionName.ID, oRegistrationOptionName)
                    Next
                End If
                Return moRegistrationOptionName
            End Get
        End Property

        Public ReadOnly Property RegistrationOptionValues() As Hashtable
            Get
                If moRegistrationOptionValue Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Events_RegistrationOptionValue")

                    moRegistrationOptionValue = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oRegistrationOptionValue As New External.Events.CRegistrationOptionValue(oDataRow)
                        moRegistrationOptionValue.Add(oRegistrationOptionValue.ID, oRegistrationOptionValue)
                    Next
                End If
                Return moRegistrationOptionValue
            End Get
        End Property

        Public ReadOnly Property RegistrationTypes() As Hashtable
            Get

                If moRegistrationType Is Nothing Then

                    Dim oDataRow As DataRow
                    Dim oDataSet As New DataSet

                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Events_RegistrationType")
                    oDataTable.TableName = "RegistrationType"
                    Dim oDataTableVenue As DataTable = DataAccess.GetDataTable("SELECT * FROM Events_RegistrationTypeVenue")
                    oDataTableVenue.TableName = "RegistrationTypeVenue"
                    Dim oRegistrationTypeQuantityGroup As DataTable = DataAccess.GetDataTable("SELECT * FROM Events_RegistrationTypeQuantityGroup")
                    oRegistrationTypeQuantityGroup.TableName = "RegistrationTypeQuantityGroup"

                    oDataSet.Tables.Add(oDataTable)
                    oDataSet.Tables.Add(oDataTableVenue)
                    oDataSet.Tables.Add(oRegistrationTypeQuantityGroup)

                    oDataSet.Relations.Add("RegistrationType", oDataSet.Tables("RegistrationType").Columns("RegistrationType_ID"), oDataSet.Tables("RegistrationTypeVenue").Columns("RegistrationType_ID"))
                    oDataSet.Relations.Add("RegistrationTypeQuantity", oDataSet.Tables("RegistrationTypeQuantityGroup").Columns("RegistrationTypeQuantityGroup_ID"), oDataSet.Tables("RegistrationType").Columns("RegistrationTypeQuantityGroup_ID"))

                    moRegistrationType = New Hashtable
                    For Each oDataRow In oDataSet.Tables(0).Rows
                        Dim oRegistrationType As New CRegistrationType(oDataRow)
                        moRegistrationType.Add(oRegistrationType.ID, oRegistrationType)
                    Next
                End If
                Return moRegistrationType

            End Get
        End Property

        Public ReadOnly Property RegistrationTypeReportGroups() As Hashtable
            Get

                If moRegistrationTypeReportGroup Is Nothing Then

                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Events_RegistrationTypeReportGroup ORDER BY SortOrder")

                    moRegistrationTypeReportGroup = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oRegistrationTypeReportGroup As New CRegistrationTypeReportGroup(oDataRow)
                        moRegistrationTypeReportGroup.Add(oRegistrationTypeReportGroup.ID, oRegistrationTypeReportGroup)
                    Next
                End If
                Return moRegistrationTypeReportGroup

            End Get
        End Property
        Public ReadOnly Property RegistrationTypeCombo() As Hashtable
            Get

                If moRegistrationTypeCombo Is Nothing Then

                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Events_RegistrationTypeCombo")

                    moRegistrationTypeCombo = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oRegistrationTypeCombo As New CRegistrationTypeCombo(oDataRow)
                        moRegistrationTypeCombo.Add(oRegistrationTypeCombo.ID, oRegistrationTypeCombo)
                    Next
                End If
                Return moRegistrationTypeCombo

            End Get
        End Property
        Public ReadOnly Property RegistrationTypeComboDetail() As Hashtable
            Get

                If moRegistrationTypeComboDetail Is Nothing Then

                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Events_RegistrationTypeComboDetail")

                    moRegistrationTypeComboDetail = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oRegistrationTypeComboDetail As New CRegistrationTypeComboDetail(oDataRow)
                        moRegistrationTypeComboDetail.Add(oRegistrationTypeComboDetail.ID, oRegistrationTypeComboDetail)
                    Next
                End If
                Return moRegistrationTypeComboDetail

            End Get
        End Property
        Public ReadOnly Property RegistrationTypeSaleGroups() As Hashtable
            Get

                If moRegistrationTypeSaleGroup Is Nothing Then

                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Events_RegistrationTypeSaleGroup ORDER BY SortOrder")

                    moRegistrationTypeSaleGroup = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oRegistrationTypeSaleGroup As New CRegistrationTypeSaleGroup(oDataRow)
                        moRegistrationTypeSaleGroup.Add(oRegistrationTypeSaleGroup.ID, oRegistrationTypeSaleGroup)
                    Next
                End If
                Return moRegistrationTypeSaleGroup

            End Get
        End Property
        Public ReadOnly Property RegistrationTypesByVenue(ByVal oVenue As External.Events.CVenue) As ArrayList
            Get
                Dim oArrayList As New ArrayList
                Dim oRegistrationType As External.Events.CRegistrationType
                Dim oRegistrationTypeVenue As External.Events.CVenue

                For Each oRegistrationType In RegistrationTypes.Values
                    For Each oRegistrationTypeVenue In oRegistrationType.Venue
                        If oRegistrationTypeVenue.ID = oVenue.ID Then
                            oArrayList.Add(oRegistrationType)
                        End If
                    Next
                Next
                oArrayList.Sort(New CSorterBySortOrder)
                Return oArrayList
            End Get
        End Property
        Public ReadOnly Property RegistrationULG() As Hashtable
            Get

                If moRegistrationULG Is Nothing Then

                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM vw_Events_RegistrationULG ORDER BY SortOrder")

                    moRegistrationULG = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oRegistrationULG As New CRegistrationULG(oDataRow)
                        moRegistrationULG.Add(oRegistrationULG.ID, oRegistrationULG)
                    Next
                End If
                Return moRegistrationULG

            End Get
        End Property
        Public ReadOnly Property VenueCurrent() As CVenue
            Get

                Dim oConference As CConference = ConferenceCurrent
                Dim oVenue As CVenue
                Dim oSelectedVenue As CVenue = Nothing

                For Each oVenue In ConferenceVenues.Values
                    If oVenue.Conference.ID = oConference.ID Then
                        If oSelectedVenue Is Nothing OrElse (oVenue.ConferenceEndDate < oSelectedVenue.ConferenceEndDate And oVenue.ConferenceEndDate >= GetDate) Then
                            oSelectedVenue = oVenue
                        End If
                    End If
                Next

                Return oSelectedVenue

            End Get
        End Property
        Public ReadOnly Property VolunteerDepartments() As Hashtable
            Get
                If moVolunteerDepartment Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Events_VolunteerDepartment")

                    moVolunteerDepartment = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oVolunteerDepartment As New CVolunteerDepartment(oDataRow)
                        moVolunteerDepartment.Add(oVolunteerDepartment.ID, oVolunteerDepartment)
                    Next
                End If
                Return moVolunteerDepartment
            End Get
        End Property

        Public Function GetAccommodationVenueByID(ByVal iID As Integer) As External.Events.CAccommodationVenue
            Return AccommodationVenues(iID)
        End Function
        Public Function GetConferenceByID(ByVal iID As Integer) As External.Events.CConference
            Return Conferences(iID)
        End Function
        Public Function GetElectiveByID(ByVal iID As Integer) As External.Events.CElective
            Return Elective(iID)
        End Function
        Public Function GetGroupConciseByID(ByVal iID As Integer) As External.Events.CGroupConcise
            Return GroupConcise(iID)
        End Function
        Public Function GetGroupCreditStatusByID(ByVal iID As Integer) As External.Events.CGroupCreditStatus
            Return GroupCreditStatus(iID)
        End Function
        Public Function GetGroupPromoByID(ByVal iID As Integer) As External.Events.CGroupPromo
            Return GroupPromo(iID)
        End Function
        Public Function GetGroupSizeByID(ByVal iID As Integer) As External.Events.CGroupSize
            Return GroupSize(iID)
        End Function
        Public Function GetGroupTypeByID(ByVal iID As Integer) As External.Events.CGroupType
            Return GroupType(iID)
        End Function
        Public Function GetPOSRegisterByID(ByVal iID As Integer) As External.Events.CPOSRegister
            Return POSRegisters(iID)
        End Function
        Public Function GetProductByID(ByVal iID As Integer) As CProduct
            Return Products(iID)
        End Function
        Public Function GetRegistrationOptionNameByID(ByVal iID As Integer) As External.Events.CRegistrationOptionName
            Return RegistrationOptionNames(iID)
        End Function
        Public Function GetRegistrationOptionValueByID(ByVal iID As Integer) As External.Events.CRegistrationOptionValue
            Return RegistrationOptionValues(iID)
        End Function
        Public Function GetRegistrationTypeByID(ByVal iID As Integer) As External.Events.CRegistrationType
            Return RegistrationTypes(iID)
        End Function
        Public Function GetRegistrationTypeComboByID(ByVal iID As Integer) As External.Events.CRegistrationTypeCombo
            Return RegistrationTypeCombo(iID)
        End Function
        Public Function GetRegistrationTypeComboDetailByID(ByVal iID As Integer) As External.Events.CRegistrationTypeComboDetail
            Return RegistrationTypeComboDetail(iID)
        End Function
        Public Function GetRegistrationTypeReportGroupByID(ByVal iID As Integer) As External.Events.CRegistrationTypeReportGroup
            Return RegistrationTypeReportGroups(iID)
        End Function
        Public Function GetRegistrationTypeSaleGroupByID(ByVal iID As Integer) As External.Events.CRegistrationTypeSaleGroup
            Return RegistrationTypeSaleGroups(iID)
        End Function
        Public Function GetRegistrationULGByID(ByVal iID As Integer) As CRegistrationULG
            Return RegistrationULG(iID)
        End Function
        Public Function GetVenueByID(ByVal iID As Integer) As External.Events.CVenue
            Return ConferenceVenues()(iID)
        End Function
        Public Function GetVolunteerDepartmentByID(ByVal iID As Integer) As CVolunteerDepartment
            Return VolunteerDepartments(iID)
        End Function

        Public Function GetConferenceWithVolunteerDepartments() As ArrayList
            Dim oArrayList As New ArrayList
            Dim sSQL As String
            Dim oDataTable As DataTable

            sSQL = "SELECT DISTINCT cdm.Conference_ID, (SELECT TOP 1 v.[ConferenceStartDate] FROM Events_Venue v WHERE cdm.[Conference_ID] = v.[Conference_ID] ORDER BY v.[ConferenceStartDate]) AS [StartDate] FROM Events_ConferenceDepartmentMapping cdm ORDER BY [StartDate] DESC"
            oDataTable = DataAccess.GetDataTable(sSQL)

            For Each oDataRow As DataRow In oDataTable.Rows
                oArrayList.Add(Lists.External.Events.GetConferenceByID(CType(oDataRow("Conference_ID"), Integer)))
            Next

            Return oArrayList
        End Function

        Public Function GetConferenceVolunteerDepartments(ByVal oConference As CConference) As Hashtable
            Dim oHashtable As New Hashtable
            Dim sSQL As String
            Dim oDataTable As DataTable

            sSQL = "SELECT DISTINCT VolunteerDepartment_ID FROM Events_ConferenceDepartmentMapping WHERE Conference_ID = " & oConference.ID
            oDataTable = DataAccess.GetDataTable(sSQL)

            For Each oDataRow As DataRow In oDataTable.Rows
                Dim oVolunteerDepartment = Lists.External.Events.GetVolunteerDepartmentByID(CType(oDataRow("VolunteerDepartment_ID"), Integer))
                oHashtable.Add(oVolunteerDepartment.ID, oVolunteerDepartment)
            Next

            Return oHashtable
        End Function

        Public Function GetConferenceAccessLevels(ByVal oConference As CConference) As ArrayList
            Dim oArrayList As New ArrayList
            Dim sSQL As String
            Dim oDataTable As DataTable

            sSQL = "SELECT DISTINCT AccessLevel_ID FROM Events_ConferenceDepartmentMapping WHERE Conference_ID = " & oConference.ID
            oDataTable = DataAccess.GetDataTable(sSQL)

            For Each oDataRow As DataRow In oDataTable.Rows
                oArrayList.Add(Lists.External.Events.AccessLevels(CType(oDataRow("AccessLevel_ID"), Integer)))
            Next

            Return oArrayList
        End Function

        Public Function GetConferenceAccessLevels(ByVal oConference As CConference, ByVal oVolunteerDepartment As CVolunteerDepartment) As ArrayList
            Dim oArrayList As New ArrayList
            Dim sSQL As String
            Dim oDataTable As DataTable

            sSQL = "SELECT DISTINCT AccessLevel_ID FROM Events_ConferenceDepartmentMapping WHERE Conference_ID = " & oConference.ID & " AND VolunteerDepartment_ID = " & oVolunteerDepartment.ID
            oDataTable = DataAccess.GetDataTable(sSQL)

            For Each oDataRow As DataRow In oDataTable.Rows
                oArrayList.Add(Lists.External.Events.AccessLevels(oConference.ID)(CType(oDataRow("AccessLevel_ID"), Integer)))
            Next

            Return oArrayList
        End Function

        Public Function GetTagLoans(ByVal iContactID As Integer) As ArrayList
            Dim oTagLoans As New ArrayList

            Dim oArrayList As New ArrayList
            oArrayList.Add(New SqlClient.SqlParameter("@Contact_ID", iContactID))

            Dim oDataTable As DataTable = DataAccess.GetDataTable("sp_Events_TagLoanRead", oArrayList)
            For Each oDataRow As DataRow In oDataTable.Rows
                If CType(oDataRow("Type"), String) = "Volunteer" Then
                    Dim oVolunteerTagLoan As New CVolunteerTagLoan(oDataRow("Tag_ID"), Lists.External.Events.Conferences(CType(oDataRow("Conference_ID"), Integer)))
                    oTagLoans.Add(oVolunteerTagLoan)
                ElseIf CType(oDataRow("Type"), String) = "Priority" Then
                    Dim oPriorityTagLoan As New CPriorityTagLoan(oDataRow("Tag_ID"), Lists.External.Events.Conferences(CType(oDataRow("Conference_ID"), Integer)))
                    oTagLoans.Add(oPriorityTagLoan)
                End If
            Next

           Return oTagLoans
        End Function

        Public Function GetRegistrationByID(ByVal iRegistrationID As Integer) As CRegistration
            Dim oRegistration As New CRegistration

            Dim oParams As New ArrayList
            oParams.Add(New SqlClient.SqlParameter("@Registration_ID", iRegistrationID))

            Dim oDataTable As DataTable = DataAccess.GetDataTable("sp_Events_RegistrationRead", oParams)
            If oDataTable.Rows.Count > 0 Then
                oRegistration = New CRegistration(oDataTable.Rows(0))
            Else
                oRegistration = Nothing
            End If

            If oRegistration IsNot Nothing Then
                Dim oContact As New CContact
                oContact.LoadByID(oRegistration.ContactID)

                For Each oContactRegistration As CRegistration In oContact.External.Events.Registration
                    If oContactRegistration.ID = oRegistration.ID Then
                        oContactRegistration.VenueID = oRegistration.VenueID
                        Return oContactRegistration
                    End If
                Next
            End If

            Return Nothing
        End Function
    End Class

End Namespace