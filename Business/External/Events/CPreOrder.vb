Namespace External.Events

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CPreOrder

        Private miPreOrderID As Integer
        Private miRegistrationID As Integer
        Private miProductID As Integer
        Private mdDiscount As Decimal
        Private msGUID As String

        Private mbChanged As Boolean

        Public Sub New()

            miPreOrderID = 0
            miRegistrationID = 0
            miProductID = 0
            mdDiscount = 0
            msGUID = System.Guid.NewGuid.ToString
            mbChanged = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miPreOrderID = oDataRow("PreOrder_ID")
            miRegistrationID = oDataRow("Registration_ID")
            miProductID = oDataRow("Product_ID")
            Discount = oDataRow("Discount")
            mbChanged = False

        End Sub

        Public Sub Save(ByVal oUser As CUser)

            If mbChanged Then

                Dim oParameters As New ArrayList

                oParameters.Add(New SqlClient.SqlParameter("@Registration_ID", miRegistrationID))
                oParameters.Add(New SqlClient.SqlParameter("@Product_ID", miProductID))
                oParameters.Add(New SqlClient.SqlParameter("@Discount", mdDiscount))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))

                If Me.ID = 0 Then
                    miPreOrderID = DataAccess.GetValue("sp_Events_PreOrderInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@PreOrder_ID", ID))
                    DataAccess.ExecuteCommand("sp_Events_PreOrderUpdate", oParameters)
                End If

            End If

        End Sub

        Property ID() As Integer
            Get
                Return miPreOrderID
            End Get
            Set(ByVal value As Integer)
                miPreOrderID = value
            End Set
        End Property
        Property RegistrationID() As Integer
            Get
                Return miRegistrationID
            End Get
            Set(ByVal value As Integer)
                If miRegistrationID <> value Then mbChanged = True
                miRegistrationID = value
            End Set
        End Property
        Property ProductID() As Integer
            Get
                Return miProductID
            End Get
            Set(ByVal Value As Integer)
                If miProductID <> Value Then mbChanged = True
                miProductID = Value
            End Set
        End Property
        ReadOnly Property Product() As CProduct
            Get
                Return Lists.External.Events.GetProductByID(miProductID)
            End Get
        End Property
        Property Discount() As Decimal
            Get
                Return mdDiscount
            End Get
            Set(ByVal Value As Decimal)
                If mdDiscount <> Value Then mbChanged = True
                mdDiscount = Value
            End Set
        End Property
        ReadOnly Property Price(ByVal oCountry As CCountry) As Decimal
            Get
                Return Decimal.Round(Product.Price(oCountry) * ((100 - Discount) / 100), 2)
            End Get
        End Property
        Property GUID() As String
            Get
                Return msGUID
            End Get
            Set(ByVal value As String)
                msGUID = value
            End Set
        End Property

        Property Changed() As Boolean
            Get
                Return mbChanged
            End Get
            Set(ByVal value As Boolean)
                mbChanged = value
            End Set
        End Property

        Public Sub ResetChanges()
            mbChanged = False
        End Sub

    End Class

End Namespace