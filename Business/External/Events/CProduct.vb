Namespace External.Events

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CProduct

        Private miProductID As Integer
        Private msCode As String
        Private msCode2 As String
        Private msTitle As String
        Private msAuthorArtist As String
        Private mbDeleted As Boolean

        Private moProductPrice As CArrayList(Of CProductPrice)

        Public Sub New()

            miProductID = 0
            msCode = ""
            msCode2 = ""
            msTitle = ""
            msAuthorArtist = ""
            mbDeleted = False
            moProductPrice = New CArrayList(Of CProductPrice)

        End Sub

        Public Sub New(ByRef oProduct As DataRow, ByVal oProdPrice As DataRow())

            Me.New()

            miProductID = oProduct("Product_ID")
            msCode = oProduct("Code")
            msCode2 = oProduct("Code2")
            msTitle = oProduct("Title")
            msAuthorArtist = oProduct("AuthorArtist")
            mbDeleted = oProduct("Deleted")

            Dim oDataRow As DataRow
            For Each oDataRow In oProdPrice
                moProductPrice.Add(New CProductPrice(oDataRow))
            Next

        End Sub

        Public Function Save() As Boolean

            'Update the product

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Code", Code))
            oParameters.Add(New SqlClient.SqlParameter("@Code2", Code2))
            oParameters.Add(New SqlClient.SqlParameter("@Title", Title))
            oParameters.Add(New SqlClient.SqlParameter("@AuthorArtist", AuthorArtist))
            oParameters.Add(New SqlClient.SqlParameter("@Deleted", Deleted))

            If ID = 0 Then
                miProductID = DataAccess.GetValue("sp_Events_ProductInsert", oParameters)
            Else
                oParameters.Add(New SqlClient.SqlParameter("@Product_ID", ID))
                DataAccess.ExecuteCommand("sp_Events_ProductUpdate", oParameters)
            End If


            'Update this product's prices as well
            Dim oProductPrice As CProductPrice
            Dim sDeleteProducts As String = String.Empty

            'Remove the deleted prices from the database
            For Each oProductPrice In Me.ProductPrice
                sDeleteProducts += " AND ProductPrice_ID <> " & oProductPrice.ID
            Next
            DataAccess.ExecuteCommand("DELETE FROM Events_ProductPrice WHERE Product_ID = " & ID & sDeleteProducts)

            'Update/Insert other products
            For Each oProductPrice In Me.ProductPrice

                Dim oPriceParameters As New ArrayList
                oPriceParameters.Add(New SqlClient.SqlParameter("@Product_ID", ID))
                oPriceParameters.Add(New SqlClient.SqlParameter("@Country_ID", SQLObject(oProductPrice.Country)))
                oPriceParameters.Add(New SqlClient.SqlParameter("@Price", oProductPrice.Price))

                If oProductPrice.ID = 0 Then
                    oProductPrice.ID = DataAccess.GetValue("sp_Events_ProductPriceInsert", oPriceParameters)
                Else
                    oPriceParameters.Add(New SqlClient.SqlParameter("@ProductPrice_ID", oProductPrice.ID))
                    DataAccess.ExecuteCommand("sp_Events_ProductPriceUpdate", oPriceParameters)
                End If
            Next

            Return True
        End Function

        Public Function CurrentStockLevel() As Integer

            Dim oVenue As CVenue = Lists.External.Events.VenueCurrent
            Return DataAccess.GetValue("SELECT ISNULL(SUM(Quantity),0) FROM Events_StockLevel WHERE AdjustmentType = 1 AND Product_ID = " & Me.ID & " AND Venue_ID = " & oVenue.ID)

        End Function

        ReadOnly Property ProductPrice() As CArrayList(Of CProductPrice)
            Get
                Return moProductPrice
            End Get
        End Property
        ReadOnly Property ID() As Integer
            Get
                Return miProductID
            End Get
        End Property
        Property Code() As String
            Get
                Return msCode
            End Get
            Set(ByVal Value As String)
                msCode = Value
            End Set
        End Property
        Property Code2() As String
            Get
                Return msCode2
            End Get
            Set(ByVal Value As String)
                msCode2 = Value
            End Set
        End Property
        Property Title() As String
            Get
                Return msTitle
            End Get
            Set(ByVal Value As String)
                msTitle = Value
            End Set
        End Property
        Property AuthorArtist() As String
            Get
                Return msAuthorArtist
            End Get
            Set(ByVal Value As String)
                msAuthorArtist = Value
            End Set
        End Property
        ReadOnly Property Price(ByVal oCountry As CCountry) As Decimal
            Get
                Dim oProductPrice As CProductPrice
                Dim dPrice As Decimal = 0
                For Each oProductPrice In moProductPrice
                    If oProductPrice.Country.ID = oCountry.ID Then
                        dPrice = oProductPrice.Price
                    End If
                Next
                Return dPrice
            End Get
        End Property
        Property Deleted() As Boolean
            Get
                Return mbDeleted
            End Get
            Set(ByVal Value As Boolean)
                mbDeleted = Value
            End Set
        End Property

        Public Overrides Function toString() As String
            Return Title & " - " & Code & " - " & AuthorArtist
        End Function
        Public Function LongDescription() As String
            Return Me.Title & " - " & Me.AuthorArtist
        End Function

        Public ReadOnly Property ListDescriptionTitle() As String
            Get
                Return Title & " - " & Code & " - " & AuthorArtist
            End Get
        End Property

        Public ReadOnly Property ListDescriptionCode() As String
            Get
                Return Code & " - " & Title & " - " & AuthorArtist
            End Get
        End Property

    End Class

End Namespace