Namespace External.Events

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CRegistrationChildInformation

        Private miRegistrationID As Integer

        Private miSchoolGradeID As Integer
        Private msChildsFriend As String
        Private msDropOffPickUpName As String
        Private msDropOffPickUpRelationship As String
        Private msDropOffPickUpContactNumber As String
        Private msChildrensChurchPastor As String
        Private mbConsentFormSent As Boolean
        Private mdConsentFormSentDate As Date
        Private mbConsentFormReturned As Boolean
        Private mdConsentFormReturnedDate As Date

        Public mbChanged As Boolean

        Public Sub New()

            miSchoolGradeID = 0
            msChildsFriend = String.Empty
            msDropOffPickUpName = String.Empty
            msDropOffPickUpRelationship = String.Empty
            msDropOffPickUpContactNumber = String.Empty
            msChildrensChurchPastor = String.Empty
            mbConsentFormSent = False
            mdConsentFormSentDate = Nothing
            mbConsentFormReturned = False
            mdConsentFormReturnedDate = Nothing

            mbChanged = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)
            Me.New()
            LoadDataRow(oDataRow)
        End Sub

        Public Sub LoadDataRow(ByRef oDataRow As DataRow)

            miRegistrationID = oDataRow("Registration_ID")
            miSchoolGradeID = IsNull(oDataRow("CrecheChildSchoolGrade_ID"), 0)
            msChildsFriend = oDataRow("ChildsFriend")
            msDropOffPickUpName = oDataRow("DropOffPickUpName")
            msDropOffPickUpRelationship = oDataRow("DropOffPickUpRelationship")
            msDropOffPickUpContactNumber = oDataRow("DropOffPickUpContactNumber")
            msChildrensChurchPastor = oDataRow("ChildrensChurchPastor")
            mbConsentFormSent = oDataRow("ConsentFormSent")
            mdConsentFormSentDate = IsNull(oDataRow("ConsentFormSentDate"), Nothing)
            mbConsentFormReturned = oDataRow("ConsentFormReturned")
            mdConsentFormReturnedDate = IsNull(oDataRow("ConsentFormReturnedDate"), Nothing)

            mbChanged = False

        End Sub

        Public Sub Save(ByVal oUser As CUser)

            Dim oParameters As New ArrayList

            If mbChanged Then

                oParameters.Add(New SqlClient.SqlParameter("@Registration_ID", miRegistrationID))
                oParameters.Add(New SqlClient.SqlParameter("@CrecheChildSchoolGrade_ID", SQLNull(miSchoolGradeID)))
                oParameters.Add(New SqlClient.SqlParameter("@ChildsFriend", msChildsFriend))
                oParameters.Add(New SqlClient.SqlParameter("@DropOffPickUpName", msDropOffPickUpName))
                oParameters.Add(New SqlClient.SqlParameter("@DropOffPickUpRelationship", msDropOffPickUpRelationship))
                oParameters.Add(New SqlClient.SqlParameter("@DropOffPickUpContactNumber", msDropOffPickUpContactNumber))
                oParameters.Add(New SqlClient.SqlParameter("@ChildrensChurchPastor", msChildrensChurchPastor))
                oParameters.Add(New SqlClient.SqlParameter("@ConsentFormSent", mbConsentFormSent))
                oParameters.Add(New SqlClient.SqlParameter("@ConsentFormSentDate", SQLDate(mdConsentFormSentDate)))
                oParameters.Add(New SqlClient.SqlParameter("@ConsentFormReturned", mbConsentFormReturned))
                oParameters.Add(New SqlClient.SqlParameter("@ConsentFormReturnedDate", SQLDate(mdConsentFormReturnedDate)))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))

                DataAccess.ExecuteCommand("sp_Events_RegistrationChildInformationUpdate", oParameters)

                mbChanged = False

            End If

        End Sub

        Public Property ID() As Integer
            Get
                Return miRegistrationID
            End Get
            Set(ByVal value As Integer)
                If miRegistrationID <> value Then mbChanged = True
                miRegistrationID = value
            End Set
        End Property

        Public Property SchoolGradeID() As Integer
            Get
                Return miSchoolGradeID
            End Get
            Set(ByVal value As Integer)
                If miSchoolGradeID <> value Then mbChanged = True
                miSchoolGradeID = value
            End Set
        End Property

        Public ReadOnly Property SchoolGrade() As CSchoolGrade
            Get
                Return Lists.GetSchoolGradeByID(miSchoolGradeID)
            End Get
        End Property
        Property ChildsFriend() As String
            Get
                Return msChildsFriend
            End Get
            Set(ByVal Value As String)
                If msChildsFriend <> Value Then mbChanged = True
                msChildsFriend = Value
            End Set
        End Property
        Property DropOffPickUpName() As String
            Get
                Return msDropOffPickUpName
            End Get
            Set(ByVal Value As String)
                If msDropOffPickUpName <> Value Then mbChanged = True
                msDropOffPickUpName = Value
            End Set
        End Property
        Property DropOffPickUpRelationship() As String
            Get
                Return msDropOffPickUpRelationship
            End Get
            Set(ByVal Value As String)
                If msDropOffPickUpRelationship <> Value Then mbChanged = True
                msDropOffPickUpRelationship = Value
            End Set
        End Property
        Property DropOffPickUpContactNumber() As String
            Get
                Return msDropOffPickUpContactNumber
            End Get
            Set(ByVal Value As String)
                If msDropOffPickUpContactNumber <> Value Then mbChanged = True
                msDropOffPickUpContactNumber = Value
            End Set
        End Property
        Property ChildrensChurchPastor() As String
            Get
                Return msChildrensChurchPastor
            End Get
            Set(ByVal Value As String)
                If msChildrensChurchPastor <> Value Then mbChanged = True
                msChildrensChurchPastor = Value
            End Set
        End Property
        Property ConsentFormSent() As Boolean
            Get
                Return mbConsentFormSent
            End Get
            Set(ByVal Value As Boolean)
                If mbConsentFormSent <> Value Then mbChanged = True
                mbConsentFormSent = Value
            End Set
        End Property
        Property ConsentFormSentDate() As Date
            Get
                Return mdConsentFormSentDate
            End Get
            Set(ByVal Value As Date)
                If mdConsentFormSentDate <> Value Then mbChanged = True
                mdConsentFormSentDate = Value
            End Set
        End Property
        Property ConsentFormReturned() As Boolean
            Get
                Return mbConsentFormReturned
            End Get
            Set(ByVal Value As Boolean)
                If mbConsentFormReturned <> Value Then mbChanged = True
                mbConsentFormReturned = Value
            End Set
        End Property
        Property ConsentFormReturnedDate() As Date
            Get
                Return mdConsentFormReturnedDate
            End Get
            Set(ByVal Value As Date)
                If mdConsentFormReturnedDate <> Value Then mbChanged = True
                mdConsentFormReturnedDate = Value
            End Set
        End Property

        Public Property Changed() As Boolean
            Get
                Return mbChanged
            End Get
            Set(ByVal value As Boolean)
                mbChanged = value
            End Set
        End Property

        Public Sub ResetChanges()

            mbChanged = False

        End Sub

    End Class

End Namespace