Namespace External.Events

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CRegistrationOption

        Private miRegistrationOptionID As Integer
        Private miRegistrationID As Integer
        Private miRegistrationOptionNameID As Integer
        Private miRegistrationOptionValueID As Integer
        Private msRegistrationOptionText As String
        Private msGUID As String

        Private mbChanged As Boolean

        Public Sub New()

            miRegistrationOptionID = 0
            miRegistrationID = 0
            miRegistrationOptionNameID = 0
            miRegistrationOptionValueID = 0
            msRegistrationOptionText = ""
            msGUID = System.Guid.NewGuid.ToString
            mbChanged = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miRegistrationOptionID = oDataRow("RegistrationOption_ID")
            miRegistrationID = oDataRow("Registration_ID")
            miRegistrationOptionNameID = oDataRow("RegistrationOptionName_ID")
            miRegistrationOptionValueID = IsNull(oDataRow("RegistrationOptionValue_ID"), 0)
            msRegistrationOptionText = oDataRow("RegistrationOptionText")
            mbChanged = False

        End Sub

        Public Sub Save(ByVal oUser As CUser)

            If mbChanged Then

                Dim oParameters As New ArrayList

                oParameters.Add(New SqlClient.SqlParameter("@Registration_ID", miRegistrationID))
                oParameters.Add(New SqlClient.SqlParameter("@RegistrationOptionName_ID", SQLNull(miRegistrationOptionNameID)))
                oParameters.Add(New SqlClient.SqlParameter("@RegistrationOptionValue_ID", SQLNull(miRegistrationOptionValueID)))
                oParameters.Add(New SqlClient.SqlParameter("@RegistrationOptionText", msRegistrationOptionText))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))

                If Me.ID = 0 Then
                    miRegistrationOptionID = DataAccess.GetValue("sp_Events_RegistrationOptionInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@RegistrationOption_ID", ID))
                    DataAccess.ExecuteCommand("sp_Events_RegistrationOptionUpdate", oParameters)
                End If

                mbChanged = False

            End If

        End Sub

        Public Property ID() As Integer
            Get
                Return miRegistrationOptionID
            End Get
            Set(ByVal value As Integer)
                miRegistrationOptionID = value
            End Set
        End Property

        Property RegistrationID() As Integer
            Get
                Return miRegistrationID
            End Get
            Set(ByVal value As Integer)
                If miRegistrationID <> value Then mbChanged = True
                miRegistrationID = value
            End Set
        End Property

        Public Property RegistrationOptionNameID() As Integer
            Get
                Return miRegistrationOptionNameID
            End Get
            Set(ByVal Value As Integer)
                If miRegistrationOptionNameID <> Value Then mbChanged = True
                miRegistrationOptionNameID = Value
            End Set
        End Property
        Public ReadOnly Property RegistrationOptionName() As CRegistrationOptionName
            Get
                Return Lists.External.Events.GetRegistrationOptionNameByID(miRegistrationOptionNameID)
            End Get
        End Property
        Public Property RegistrationOptionValueID() As Integer
            Get
                Return miRegistrationOptionValueID
            End Get
            Set(ByVal Value As Integer)
                If miRegistrationOptionValueID <> Value Then mbChanged = True
                miRegistrationOptionValueID = Value
            End Set
        End Property
        Public ReadOnly Property RegistrationOptionValue() As CRegistrationOptionValue
            Get
                Return Lists.External.Events.GetRegistrationOptionValueByID(miRegistrationOptionValueID)
            End Get
        End Property
        Public Property RegistrationOptionText() As String
            Get
                Return msRegistrationOptionText
            End Get
            Set(ByVal Value As String)
                If msRegistrationOptionText <> Value Then mbChanged = True
                msRegistrationOptionText = Value
            End Set
        End Property

        Property GUID() As String
            Get
                Return msGUID
            End Get
            Set(ByVal value As String)
                msGUID = value
            End Set
        End Property
        Public Overrides Function toString() As String
            Select Case RegistrationOptionName.FieldType
                Case "TXT"
                    Return msRegistrationOptionText
                Case "DDL"
                    Return RegistrationOptionValue.Name
                Case "BIT"
                    Return IIf(msRegistrationOptionText = "True", "Yes", "No")
                Case Else
                    Return msRegistrationOptionText
            End Select
        End Function

        Public Property Changed() As Boolean
            Get
                Return mbChanged
            End Get
            Set(ByVal value As Boolean)
                mbChanged = value
            End Set
        End Property

        Public Sub ResetChanges()
            mbChanged = False
        End Sub

    End Class

End Namespace