Namespace External.Events

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CReportParameter

        Private miReportParameterID As Integer
        Private msParameterName As String
        Private msParameterSQL As String
        Private moParameterList As ArrayList

        Public Sub New()

            miReportParameterID = 0
            msParameterName = String.Empty
            msParameterSQL = String.Empty
            moParameterList = New ArrayList

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miReportParameterID = oDataRow("Report_ID")
            msParameterName = oDataRow("ParameterName")
            msParameterSQL = oDataRow("ParameterSQL")

            Dim oDataTable As DataTable = DataAccess.GetDataTable(oDataRow("ParameterSQLList"))
            Dim oParameterDataRow As DataRow
            For Each oParameterDataRow In oDataTable.Rows
                Dim oParameterValue As New CReportParameterValue
                oParameterValue.ID = oDataRow("ID")
                oParameterValue.Text = oDataRow("Text")
                moParameterList.Add(oParameterValue)
            Next

        End Sub

        Property ID() As Integer
            Get
                Return miReportParameterID
            End Get
            Set(ByVal Value As Integer)
                miReportParameterID = Value
            End Set
        End Property
        Property ParameterName() As String
            Get
                Return msParameterName
            End Get
            Set(ByVal Value As String)
                msParameterName = Value
            End Set
        End Property
        Property ParameterSQL() As String
            Get
                Return msParameterSQL
            End Get
            Set(ByVal Value As String)
                msParameterSQL = Value
            End Set
        End Property
        Property ParameterList() As String
            Get
                Return msParameterSQL
            End Get
            Set(ByVal Value As String)
                msParameterSQL = Value
            End Set
        End Property

    End Class

End Namespace
