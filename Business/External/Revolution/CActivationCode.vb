Namespace External.Revolution

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CActivationCode

        Private miActivationCodeID As Integer
        Private miSerialNumber As Integer
        Private msActivationCode As String
        Private mbCardActivated As Boolean
        Private mbSubscriptionActivated As Boolean
        Private miSubscriptionID As Integer
        Private miContactID As Integer
        Private mdActivationDate As Date
        Private miActivatedByID As Integer

        Private mbChanged As Boolean

        Public Sub New()

            miActivationCodeID = 0
            miSerialNumber = 0
            msActivationCode = String.Empty
            mbCardActivated = False
            mbSubscriptionActivated = False
            miSubscriptionID = 0
            miContactID = 0
            mdActivationDate = Nothing
            miActivatedByID = 0

            mbChanged = False

        End Sub

        Public Function LoadBySerialNumber(ByVal iSerialNumber As Integer) As Boolean

            Dim oDataTable As DataTable

            Dim oParameter As New ArrayList
            oParameter.Add(New SqlClient.SqlParameter("@SerialNumber", iSerialNumber))
            oDataTable = DataAccess.GetDataTable("sp_Revolution_ActivationCodeReadBySerialNumber", oParameter)

            If oDataTable.Rows.Count <> 1 Then
                Return False
            Else
                Return LoadDataRow(oDataTable.Rows(0))
            End If

        End Function

        Public Function LoadByActivationCode(ByVal sActivationCode As String) As Boolean

            Dim oDataTable As DataTable

            Dim oParameter As New ArrayList
            oParameter.Add(New SqlClient.SqlParameter("@ActivationCode", sActivationCode))
            oDataTable = DataAccess.GetDataTable("sp_Revolution_ActivationCodeReadByActivationCode", oParameter)

            If oDataTable.Rows.Count <> 1 Then
                Return False
            Else
                Return LoadDataRow(oDataTable.Rows(0))
            End If

        End Function

        Public Function LoadDataRow(ByRef oDataRow As DataRow) As Boolean

            miActivationCodeID = oDataRow("ActivationCode_ID")
            miSerialNumber = oDataRow("SerialNumber")
            msActivationCode = oDataRow("ActivationCode").ToString
            mbCardActivated = oDataRow("CardActivated")
            mbSubscriptionActivated = oDataRow("SubscriptionActivated")
            miSubscriptionID = oDataRow("Subscription_ID")
            miContactID = IsNull(oDataRow("Contact_ID"), 0)
            mdActivationDate = IsNull(oDataRow("ActivationDate"), Nothing)
            miActivatedByID = IsNull(oDataRow("ActivatedBy_ID"), 0)

            mbChanged = False

            Return True

        End Function

        Public Sub Save()

            If mbChanged Then

                Dim oParameters As New ArrayList

                oParameters.Add(New SqlClient.SqlParameter("@SerialNumber", miSerialNumber))
                oParameters.Add(New SqlClient.SqlParameter("@ActivationCode", msActivationCode))
                oParameters.Add(New SqlClient.SqlParameter("@CardActivated", mbCardActivated))
                oParameters.Add(New SqlClient.SqlParameter("@SubscriptionActivated", mbSubscriptionActivated))
                oParameters.Add(New SqlClient.SqlParameter("@Subscription_ID", miSubscriptionID))
                oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", SQLNull(miContactID)))
                oParameters.Add(New SqlClient.SqlParameter("@ActivationDate", SQLDateTime(mdActivationDate)))
                oParameters.Add(New SqlClient.SqlParameter("@ActivatedBy_ID", SQLNull(miActivatedByID)))

                If Me.ID > 0 Then
                    oParameters.Add(New SqlClient.SqlParameter("@ActivationCode_ID", miActivationCodeID))
                    DataAccess.ExecuteCommand("sp_Revolution_ActivationCodeUpdate", oParameters)
                End If

                mbChanged = False

            End If

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miActivationCodeID
            End Get
        End Property
        Public Property SerialNumber() As Integer
            Get
                Return miSerialNumber
            End Get
            Set(ByVal value As Integer)
                If miSerialNumber <> value Then mbChanged = True
                miSerialNumber = value
            End Set
        End Property
        Public Property ActivationCode() As String
            Get
                Return msActivationCode
            End Get
            Set(ByVal value As String)
                If msActivationCode <> value Then mbChanged = True
                msActivationCode = value
            End Set
        End Property
        Public Property CardActivated() As Boolean
            Get
                Return mbCardActivated
            End Get
            Set(ByVal value As Boolean)
                If mbCardActivated <> value Then mbChanged = True
                mbCardActivated = value
            End Set
        End Property
        Public Property SubscriptionActivated() As Boolean
            Get
                Return mbSubscriptionActivated
            End Get
            Set(ByVal value As Boolean)
                If mbSubscriptionActivated <> value Then mbChanged = True
                mbSubscriptionActivated = value
            End Set
        End Property
        Public Property SubscriptionID() As Integer
            Get
                Return miSubscriptionID
            End Get
            Set(ByVal value As Integer)
                If miSubscriptionID <> value Then mbChanged = True
                miSubscriptionID = value
            End Set
        End Property
        Public ReadOnly Property Subscription() As CSubscription
            Get
                Return Lists.External.Revolution.GetSubscriptionByID(miSubscriptionID)
            End Get
        End Property
        Public Property ContactID() As Integer
            Get
                Return miContactID
            End Get
            Set(ByVal value As Integer)
                If miContactID <> value Then mbChanged = True
                miContactID = value
            End Set
        End Property
        Public Property ActivationDate() As Date
            Get
                Return mdActivationDate
            End Get
            Set(ByVal value As Date)
                If mdActivationDate <> value Then mbChanged = True
                mdActivationDate = value
            End Set
        End Property
        Public Property ActivatedByID() As Integer
            Get
                Return miActivatedByID
            End Get
            Set(ByVal value As Integer)
                If miActivatedByID <> value Then mbChanged = True
                miActivatedByID = value
            End Set
        End Property
        Public ReadOnly Property ActivatedBy() As CUser
            Get
                Return Lists.GetUserByID(miActivatedByID)
            End Get
        End Property

    End Class

End Namespace