Namespace External.Revolution

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CContent

        Private miContentID As Integer
        Private miContentPackID As Integer
        Private msName As String
        Private mbAuthenticationRequired As Boolean
        Private mbRestrictedDownload As Boolean
        Private msMediaPlayerBackgroundImage As Byte()
        Private msThumbnailImage As Byte()
        Private msDescription As String
        Private msMoreDetailsLink As String
        Private mbVisible As Boolean
        Private miSortOrder As Integer
        Private msGUID As String

        Private mcContentFiles As CArrayList(Of CContentFile)
        Private mcContentCategories As CArrayList(Of CContentContentCategory)

        Private mbChanged As Boolean

        Public Sub New()

            miContentID = 0
            miContentPackID = 0
            msName = ""
            mbAuthenticationRequired = False
            mbRestrictedDownload = False
            msMediaPlayerBackgroundImage = Nothing
            msThumbnailImage = Nothing
            msDescription = String.Empty
            msMoreDetailsLink = String.Empty
            mbVisible = False
            miSortOrder = 0
            msGUID = System.Guid.NewGuid.ToString

            mcContentFiles = New CArrayList(Of CContentFile)
            mcContentCategories = New CArrayList(Of CContentContentCategory)
            mbChanged = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miContentID = oDataRow("Content_ID")
            miContentPackID = oDataRow("ContentPack_ID")
            msName = oDataRow("Name")
            mbAuthenticationRequired = oDataRow("AuthenticationRequired")
            mbRestrictedDownload = oDataRow("RestrictedDownload")

            'Don't load the images until they're requested - can be very slow in dev.
            'msMediaPlayerBackgroundImage = IsNull(oDataRow("MediaPlayerBackgroundImage"), Nothing)
            'msThumbnailImage = IsNull(oDataRow("ThumbnailImage"), Nothing)

            msDescription = oDataRow("Description")
            msMoreDetailsLink = oDataRow("MoreDetailsLink")
            mbVisible = oDataRow("Visible")
            miSortOrder = oDataRow("SortOrder")
            msGUID = oDataRow("GUID").ToString


            Dim oContentFile As CContentFile
            For Each oContentFile In Lists.External.Revolution.ContentFile.Values
                If oContentFile.ContentID = miContentID Then
                    mcContentFiles.Add(oContentFile)
                End If
            Next
            mcContentFiles.Sort(New CSorterContentFile)

            Dim oContentSubCategoryRow As DataRow
            For Each oContentSubCategoryRow In oDataRow.GetChildRows("ContentContentCategory")
                mcContentCategories.Add(New CContentContentCategory(oContentSubCategoryRow))
            Next

            mcContentFiles.ResetChanges()
            mcContentCategories.ResetChanges()

            mbChanged = False

        End Sub

        Public Sub Save()
            Save(miContentPackID)
        End Sub

        Public Sub Save(ByVal iContentPackID As Integer)

            If mbChanged Then

                Dim oParameters As New ArrayList

                oParameters.Add(New SqlClient.SqlParameter("@ContentPack_ID", iContentPackID))
                oParameters.Add(New SqlClient.SqlParameter("@Name", msName))
                oParameters.Add(New SqlClient.SqlParameter("@AuthenticationRequired", mbAuthenticationRequired))
                oParameters.Add(New SqlClient.SqlParameter("@RestrictedDownload", mbRestrictedDownload))
                If msMediaPlayerBackgroundImage IsNot Nothing Then oParameters.Add(New SqlClient.SqlParameter("@MediaPlayerBackgroundImage", msMediaPlayerBackgroundImage))
                If msThumbnailImage IsNot Nothing Then oParameters.Add(New SqlClient.SqlParameter("@ThumbnailImage", msThumbnailImage))
                oParameters.Add(New SqlClient.SqlParameter("@Description", msDescription))
                oParameters.Add(New SqlClient.SqlParameter("@MoreDetailsLink", msMoreDetailsLink))
                oParameters.Add(New SqlClient.SqlParameter("@Visible", mbVisible))
                oParameters.Add(New SqlClient.SqlParameter("@SortOrder", miSortOrder))

                If Me.ID = 0 Then
                    miContentID = DataAccess.GetValue("sp_Revolution_ContentInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@Content_ID", ID))
                    DataAccess.ExecuteCommand("sp_Revolution_ContentUpdate", oParameters)
                End If

                mbChanged = False

            End If

            SaveContentFiles()
            SaveContentContentSubCategories()

            mcContentFiles.ResetChanges()
            mcContentCategories.ResetChanges()

        End Sub

        Private Sub SaveContentFiles()

            Dim oParameters As ArrayList
            Dim oContentFile As CContentFile

            'Remove the Deleted Content
            For Each oContentFile In mcContentFiles.Removed
                oParameters = New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@ContentFile_ID", oContentFile.ID))
                DataAccess.ExecuteCommand("sp_Revolution_ContentFileDelete", oParameters)

                Dim sFileLocation As String = Options.ContentFolder & miContentID & "\" & oContentFile.FileName
                If System.IO.File.Exists(sFileLocation) Then
                    System.IO.File.Delete(sFileLocation)
                End If

            Next

            'Add/Update all the others
            For Each oContentFile In mcContentFiles
                oContentFile.Save(miContentID)
            Next

        End Sub

        Private Sub SaveContentContentSubCategories()

            Dim oParameters As ArrayList
            Dim oContentContentCategory As CContentContentCategory

            'Remove the Deleted Sub-Categories
            For Each oContentContentCategory In mcContentCategories.Removed
                oParameters = New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@ContentContentCategory_ID", oContentContentCategory.ID))
                DataAccess.ExecuteCommand("sp_Revolution_ContentContentCategoryDelete", oParameters)
            Next

            'Add/Update all the others
            For Each oContentContentCategory In mcContentCategories
                oContentContentCategory.Save(miContentID)
            Next

            mcContentCategories.Sort(New CSorterContentContentCategory)

        End Sub

        Public ReadOnly Property ContentFiles() As CArrayList(Of CContentFile)
            Get
                Return mcContentFiles
            End Get
        End Property

        Public ReadOnly Property ContentCategories() As CArrayList(Of CContentContentCategory)
            Get
                Return mcContentCategories
            End Get
        End Property

        Public Function ContainsContentCategory(ByVal oContentSubCategory As CContentSubCategory) As Boolean

            Dim oContentContentCategory As CContentContentCategory
            For Each oContentContentCategory In mcContentCategories
                If oContentContentCategory.ContentSubCategory.ID = oContentSubCategory.ID Then Return True
            Next
            Return False

        End Function

        Public ReadOnly Property ID() As Integer
            Get
                Return miContentID
            End Get
        End Property
        Public Property ContentPackID() As Integer
            Get
                Return miContentPackID
            End Get
            Set(ByVal value As Integer)
                miContentPackID = value
            End Set
        End Property
        Public ReadOnly Property ContentPack() As CContentPack
            Get
                Return Lists.External.Revolution.GetContentPackByID(miContentPackID)
            End Get
        End Property
        Public ReadOnly Property FileCount() As Integer
            Get
                Return mcContentFiles.Count
            End Get
        End Property
        Public Property Name() As String
            Get
                Return msName
            End Get
            Set(ByVal value As String)
                If msName <> value Then mbChanged = True
                msName = value
            End Set
        End Property
        Public Property AuthenticationRequired() As Boolean
            Get
                Return mbAuthenticationRequired
            End Get
            Set(ByVal value As Boolean)
                If mbAuthenticationRequired <> value Then mbChanged = True
                mbAuthenticationRequired = value
            End Set
        End Property
        Public Property RestrictedDownload() As Boolean
            Get
                Return mbRestrictedDownload
            End Get
            Set(ByVal value As Boolean)
                If mbRestrictedDownload <> value Then mbChanged = True
                mbRestrictedDownload = value
            End Set
        End Property
        Property MediaPlayerBackgroundImage() As Byte()
            Get
                'Load images only if we're on the production server - they are too slow to load in dev
                If CurrentWebDomain.Contains("planetshakers.com") Or CurrentWebDomain.Contains("localhost") Then
                    If msMediaPlayerBackgroundImage Is Nothing And miContentID > 0 Then
                        Dim oDataRow As DataRow = DataAccess.GetDataRow("SELECT MediaPlayerBackgroundImage FROM Revolution_Content WHERE Content_ID = " & miContentID)
                        If oDataRow IsNot Nothing Then msMediaPlayerBackgroundImage = IsNull(oDataRow("MediaPlayerBackgroundImage"), Nothing)
                    End If
                End If
                Return msMediaPlayerBackgroundImage
            End Get
            Set(ByVal Value As Byte())

                Dim sOriginal As String = String.Empty
                Dim sNew As String = String.Empty

                If Value IsNot Nothing Then sOriginal = System.Convert.ToBase64String(Value)
                If msMediaPlayerBackgroundImage IsNot Nothing Then sNew = System.Convert.ToBase64String(msMediaPlayerBackgroundImage)
                If sOriginal <> sNew Then mbChanged = True

                msMediaPlayerBackgroundImage = Value

            End Set
        End Property
        Property ThumbnailImage() As Byte()
            Get
                'Load images only if we're on the production server - they are too slow to load in dev
                If CurrentWebDomain.Contains("planetshakers.com") Or CurrentWebDomain.Contains("localhost") Then
                    If msThumbnailImage Is Nothing And miContentID > 0 Then
                        Dim oDataRow As DataRow = DataAccess.GetDataRow("SELECT ThumbnailImage FROM Revolution_Content WHERE Content_ID = " & miContentID)
                        If oDataRow IsNot Nothing Then msThumbnailImage = IsNull(oDataRow("ThumbnailImage"), Nothing)
                    End If
                End If
                Return msThumbnailImage
            End Get
            Set(ByVal Value As Byte())

                Dim sOriginal As String = String.Empty
                Dim sNew As String = String.Empty
                
                If Value IsNot Nothing Then sOriginal = System.Convert.ToBase64String(Value)
                If msThumbnailImage IsNot Nothing Then sNew = System.Convert.ToBase64String(msThumbnailImage)
                If sOriginal <> sNew Then mbChanged = True

                msThumbnailImage = Value

            End Set
        End Property
        Property Description() As String
            Get
                Return msDescription
            End Get
            Set(ByVal value As String)
                If msDescription <> value Then mbChanged = True
                msDescription = value
            End Set
        End Property
        Property MoreDetailsLink() As String
            Get
                Return msMoreDetailsLink
            End Get
            Set(ByVal value As String)
                If msMoreDetailsLink <> value Then mbChanged = True
                msMoreDetailsLink = value
            End Set
        End Property
        Property Visible() As Boolean
            Get
                Return mbVisible
            End Get
            Set(ByVal value As Boolean)
                If mbVisible <> value Then mbChanged = True
                mbVisible = value
            End Set
        End Property
        Property SortOrder() As Integer
            Get
                Return miSortOrder
            End Get
            Set(ByVal value As Integer)
                If miSortOrder <> value Then mbChanged = True
                miSortOrder = value
            End Set
        End Property
        Public ReadOnly Property GUID() As String
            Get
                Return msGUID
            End Get
        End Property

    End Class

End Namespace
