Namespace External.Revolution

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CContentCategory

        Private miContentCategoryID As Integer
        Private msName As String
        Private mcContentSubCategories As CArrayList(Of CContentSubCategory)

        Public Sub New()

            miContentCategoryID = 0
            msName = String.Empty
            mcContentSubCategories = New CArrayList(Of CContentSubCategory)

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miContentCategoryID = oDataRow("ContentCategory_ID")
            msName = oDataRow("Name")

            Dim oContentSubCategory As CContentSubCategory
            For Each oContentSubCategory In Lists.External.Revolution.ContentSubCategory.Values
                If oContentSubCategory.ContentCategoryID = miContentCategoryID Then
                    mcContentSubCategories.Add(oContentSubCategory)
                End If
            Next

        End Sub

        Public ReadOnly Property SubCategories() As CArrayList(Of CContentSubCategory)
            Get
                Return mcContentSubCategories
            End Get
        End Property

        Public ReadOnly Property ID() As Integer
            Get
                Return miContentCategoryID
            End Get
        End Property
        Public Property Name() As String
            Get
                Return msName
            End Get
            Set(ByVal value As String)
                msName = value
            End Set
        End Property

        Public ReadOnly Property HasSubCategories() As Boolean
            Get
                'Subcategories will only be recognised only if a Content Category has more than 1 sub category
                Return mcContentSubCategories.Count > 1
            End Get
        End Property

    End Class

End Namespace
