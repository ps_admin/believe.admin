Namespace External.Revolution

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CContentContentCategory

        Private miContentContentCategoryID As Integer
        Private miContentSubCategoryID As Integer
        Private miSortOrder As Integer

        Private mbChanged As Boolean

        Public Sub New()

            miContentContentCategoryID = 0
            miContentSubCategoryID = 0
            miSortOrder = 0
            mbChanged = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miContentContentCategoryID = oDataRow("ContentContentCategory_ID")
            miContentSubCategoryID = oDataRow("ContentSubCategory_ID")
            miSortOrder = oDataRow("SortOrder")
            mbChanged = False

        End Sub

        Public Sub Save(ByVal iContentID As Integer)

            If mbChanged Then

                Dim oParameters As New ArrayList

                oParameters.Add(New SqlClient.SqlParameter("@Content_ID", iContentID))
                oParameters.Add(New SqlClient.SqlParameter("@ContentSubCategory_ID", miContentSubCategoryID))
                oParameters.Add(New SqlClient.SqlParameter("@SortOrder", miSortOrder))

                If Me.ID = 0 Then
                    miContentContentCategoryID = DataAccess.GetValue("sp_Revolution_ContentContentCategoryInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@ContentContentCategory_ID", ID))
                    DataAccess.ExecuteCommand("sp_Revolution_ContentContentCategoryUpdate", oParameters)
                End If

            End If

        End Sub
        Public ReadOnly Property ID() As Integer
            Get
                Return miContentContentCategoryID
            End Get
        End Property
        Public ReadOnly Property ContentCategory() As CContentCategory
            Get
                Return Lists.External.Revolution.GetContentCategoryByID(ContentSubCategory.ContentCategory.ID)
            End Get
        End Property
        Public Property ContentSubCategoryID() As Integer
            Get
                Return miContentSubCategoryID
            End Get
            Set(ByVal value As Integer)
                If miContentSubCategoryID <> value Then mbChanged = True
                miContentSubCategoryID = value
            End Set
        End Property
        Public ReadOnly Property ContentSubCategory() As CContentSubCategory
            Get
                Return Lists.External.Revolution.GetContentSubCategoryByID(miContentSubCategoryID)
            End Get
        End Property

        Public Property SortOrder() As Integer
            Get
                Return miSortOrder
            End Get
            Set(ByVal value As Integer)
                If miSortOrder <> value Then mbChanged = True
                miSortOrder = value
            End Set
        End Property

    End Class

End Namespace