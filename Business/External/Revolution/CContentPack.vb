Namespace External.Revolution

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CContentPack

        Private miContentPackID As Integer
        Private msName As String
        Private mdStartDate As Date
        Private mdEndDate As Date
        Private mbBonusContentPack As Boolean

        Private mcContent As CArrayList(Of CContent)
        Private mcSubscription As CArrayList(Of CSubscription)

        Private mbChanged As Boolean

        Public Sub New()

            miContentPackID = 0
            msName = String.Empty
            mdStartDate = Nothing
            mdEndDate = Nothing
            mbBonusContentPack = False
            mcContent = New CArrayList(Of CContent)
            mcSubscription = New CArrayList(Of CSubscription)
            mbChanged = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miContentPackID = oDataRow("ContentPack_ID")
            msName = oDataRow("Name")
            mdStartDate = oDataRow("StartDate")
            mdEndDate = IsNull(oDataRow("EndDate"), Nothing)
            mbBonusContentPack = oDataRow("BonusContentPack")

            Dim oContent As New CContent
            For Each oContent In Lists.External.Revolution.Content.Values
                If oContent.ContentPackID = miContentPackID Then
                    mcContent.Add(oContent)
                End If
            Next

            Dim oSubscription As New CSubscription
            Dim oSubscriptionRow As DataRow
            For Each oSubscriptionRow In oDataRow.GetChildRows("SubscriptionContentPack")
                mcSubscription.Add(Lists.External.Revolution.GetSubscriptionByID(oSubscriptionRow("Subscription_ID")))
            Next

            mcContent.ResetChanges()
            mcSubscription.ResetChanges()

            mbChanged = False

        End Sub

        Public Sub Save()

            If mbChanged Then

                Dim oParameters As New ArrayList

                oParameters.Add(New SqlClient.SqlParameter("@Name", msName))
                oParameters.Add(New SqlClient.SqlParameter("@StartDate", SQLDate(mdStartDate)))
                oParameters.Add(New SqlClient.SqlParameter("@EndDate", SQLDate(mdEndDate)))
                oParameters.Add(New SqlClient.SqlParameter("@BonusContentPack", mbBonusContentPack))

                If Me.ID = 0 Then
                    miContentPackID = DataAccess.GetValue("sp_Revolution_ContentPackInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@ContentPack_ID", ID))
                    DataAccess.ExecuteCommand("sp_Revolution_ContentPackUpdate", oParameters)
                End If

            End If

            SaveContent()
            SaveSubscriptions()

            mcContent.ResetChanges()
            mcSubscription.ResetChanges()

            mbChanged = False

        End Sub

        Private Sub SaveContent()

            Dim oParameters As ArrayList
            Dim oContent As CContent

            'Remove the Deleted Content
            For Each oContent In mcContent.Removed
                oParameters = New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@Content_ID", oContent.ID))
                DataAccess.ExecuteCommand("sp_Revolution_ContentDelete", oParameters)
            Next

            'Add/Update all the others
            For Each oContent In mcContent
                oContent.Save()
            Next

        End Sub

        Private Sub SaveSubscriptions()

            Dim oParameters As ArrayList
            Dim oSubscription As CSubscription

            'Remove the Deleted Subscription-ContentPacks
            For Each oSubscription In mcSubscription.Removed
                oParameters = New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@Subscription_ID", oSubscription.ID))
                oParameters.Add(New SqlClient.SqlParameter("@ContentPack_ID", miContentPackID))
                DataAccess.ExecuteCommand("sp_Revolution_SubscriptionContentPackDelete", oParameters)
            Next

            'Add the new Subscription-Contentpacks
            For Each oSubscription In mcSubscription.Added
                oParameters = New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@Subscription_ID", oSubscription.ID))
                oParameters.Add(New SqlClient.SqlParameter("@ContentPack_ID", miContentPackID))
                DataAccess.ExecuteCommand("sp_Revolution_SubscriptionContentPackInsert", oParameters)
            Next

        End Sub

        Public ReadOnly Property Content() As CArrayList(Of CContent)
            Get
                Return mcContent
            End Get
        End Property
        Public ReadOnly Property ContentCount() As Integer
            Get
                Return mcContent.Count
            End Get
        End Property
        Public ReadOnly Property Subscription() As CArrayList(Of CSubscription)
            Get
                Return mcSubscription
            End Get
        End Property

        Public ReadOnly Property ID() As Integer
            Get
                Return miContentPackID
            End Get
        End Property
        Public Property Name() As String
            Get
                Return msName
            End Get
            Set(ByVal value As String)
                If msName <> value Then mbChanged = True
                msName = value
            End Set
        End Property
        Public Property StartDate() As Date
            Get
                Return mdStartDate
            End Get
            Set(ByVal value As Date)
                If mdStartDate <> value Then mbChanged = True
                mdStartDate = value
            End Set
        End Property
        Public Property EndDate() As Date
            Get
                Return mdEndDate
            End Get
            Set(ByVal value As Date)
                If mdEndDate <> value Then mbChanged = True
                mdEndDate = value
            End Set
        End Property
        Public Property BonusContentPack() As Boolean
            Get
                Return mbBonusContentPack
            End Get
            Set(ByVal value As Boolean)
                If mbBonusContentPack <> value Then mbChanged = True
                mbBonusContentPack = value
            End Set
        End Property

        Public Function ContainsSubscription(ByVal oSubscription As CSubscription) As Boolean

            Return mcSubscription.GetItemByID(oSubscription.ID) IsNot Nothing

        End Function

    End Class

End Namespace