Namespace External.Revolution

    <CLSCompliant(True)> _
    Public Class CLists

        Private moContentCategory As Hashtable
        Private moContent As Hashtable
        Private moContentFile As Hashtable
        Private moContentFileCategory As Hashtable
        Private moContentFileType As Hashtable
        Private moContentPack As Hashtable
        Private moContentSubCategory As Hashtable
        Private moReferredBy As Hashtable
        Private moSubscription As Hashtable

        Private mdContentRefresh As Date
        Private mdContentFileRefresh As Date
        Private mdContentPackRefresh As Date
        Private mdSubscriptionRefresh As Date

        Private miRefreshInterval As Integer = 10

        Public ReadOnly Property Content() As Hashtable
            Get
                If moContent Is Nothing Or DateDiff(DateInterval.Minute, mdContentRefresh, GetDateTime) > miRefreshInterval Then

                    Dim oDataRow As DataRow

                    Dim oTableMappings As New ArrayList
                    oTableMappings.Add(New System.Data.Common.DataTableMapping("Table", "Content"))
                    oTableMappings.Add(New System.Data.Common.DataTableMapping("Table1", "ContentContentCategory"))

                    Dim oDataset As DataSet = DataAccess.GetDataSet("sp_Revolution_ContentSelectAll", oTableMappings)

                    oDataset.Relations.Add("ContentContentCategory", oDataset.Tables("Content").Columns("Content_ID"), _
                            oDataset.Tables("ContentContentCategory").Columns("Content_ID"))

                    moContent = New Hashtable
                    For Each oDataRow In oDataset.Tables("Content").Rows
                        Dim oContent As New CContent(oDataRow)
                        moContent.Add(oContent.ID, oContent)
                    Next

                    mdContentRefresh = GetDateTime

                End If
                Return moContent
            End Get
        End Property

        Public ReadOnly Property ContentCategory() As Hashtable
            Get
                If moContentCategory Is Nothing Then
                    Dim oDataRow As DataRow

                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Revolution_ContentCategory")

                    moContentCategory = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oContentCategory As New CContentCategory(oDataRow)
                        moContentCategory.Add(oContentCategory.ID, oContentCategory)
                    Next
                End If
                Return moContentCategory
            End Get
        End Property

        Public ReadOnly Property ContentFile() As Hashtable
            Get
                If moContentFile Is Nothing Or DateDiff(DateInterval.Minute, mdContentFileRefresh, GetDateTime) > miRefreshInterval Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Revolution_ContentFile")

                    moContentFile = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oContentFile As New CContentFile(oDataRow)
                        moContentFile.Add(oContentFile.ID, oContentFile)
                    Next
                    mdContentFileRefresh = GetDateTime
                End If
                Return moContentFile
            End Get
        End Property

        Public ReadOnly Property ContentFileCategory() As Hashtable
            Get
                If moContentFileCategory Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Revolution_ContentFileCategory")

                    moContentFileCategory = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oContentFileCategory As New CContentFileCategory(oDataRow)
                        moContentFileCategory.Add(oContentFileCategory.ID, oContentFileCategory)
                    Next
                End If
                Return moContentFileCategory
            End Get
        End Property

        Public ReadOnly Property ContentFileType() As Hashtable
            Get
                If moContentFileType Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Revolution_ContentFileType")

                    moContentFileType = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oContentFileType As New CContentFileType(oDataRow)
                        moContentFileType.Add(oContentFileType.ID, oContentFileType)
                    Next
                End If
                Return moContentFileType
            End Get
        End Property


        Public ReadOnly Property ContentPack() As Hashtable
            Get
                If moContentPack Is Nothing Or DateDiff(DateInterval.Minute, mdContentPackRefresh, GetDateTime) > miRefreshInterval Then

                    Dim oDataRow As DataRow

                    Dim oTableMappings As New ArrayList
                    oTableMappings.Add(New System.Data.Common.DataTableMapping("Table", "ContentPack"))
                    oTableMappings.Add(New System.Data.Common.DataTableMapping("Table1", "SubscriptionContentPack"))

                    Dim oDataset As DataSet = DataAccess.GetDataSet("sp_Revolution_ContentPackSelectAll", oTableMappings)

                    oDataset.Relations.Add("SubscriptionContentPack", oDataset.Tables("ContentPack").Columns("ContentPack_ID"), _
                            oDataset.Tables("SubscriptionContentPack").Columns("ContentPack_ID"))

                    moContentPack = New Hashtable
                    For Each oDataRow In oDataset.Tables("ContentPack").Rows
                        Dim oContentPack As New CContentPack(oDataRow)
                        moContentPack.Add(oContentPack.ID, oContentPack)
                    Next

                    mdContentPackRefresh = GetDateTime

                End If
                Return moContentPack
            End Get
        End Property

        Public ReadOnly Property ContentSubCategory() As Hashtable
            Get
                If moContentSubCategory Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Revolution_ContentSubCategory")

                    moContentSubCategory = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oContentSubCategory As New CContentSubCategory(oDataRow)
                        moContentSubCategory.Add(oContentSubCategory.ID, oContentSubCategory)
                    Next
                End If
                Return moContentSubCategory
            End Get
        End Property

        Public ReadOnly Property ReferredBy() As Hashtable
            Get
                If moReferredBy Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Revolution_ReferredBy")

                    moReferredBy = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oReferredBy As New CReferredBy(oDataRow)
                        moReferredBy.Add(oReferredBy.ID, oReferredBy)
                    Next
                End If
                Return moReferredBy
            End Get
        End Property


        Public ReadOnly Property Subscription() As Hashtable
            Get
                If moSubscription Is Nothing Or DateDiff(DateInterval.Minute, mdSubscriptionRefresh, GetDateTime) > miRefreshInterval Then

                    Dim oDataRow As DataRow

                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Revolution_Subscription")

                    moSubscription = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oSubscription As New CSubscription(oDataRow)
                        moSubscription.Add(oSubscription.ID, oSubscription)
                    Next
                    mdSubscriptionRefresh = GetDateTime

                End If
                Return moSubscription
            End Get
        End Property

        Public Function GetContentByID(ByVal iID As Integer) As CContent
            Return Content(iID)
        End Function
        Public Function GetContentByGUID(ByVal sGUID As String) As CContent
            Dim oContent As CContent
            For Each oContent In Content.Values
                If oContent.GUID = sGUID Then Return oContent
            Next
            Return Nothing
        End Function
        Public Function GetContentCategoryByID(ByVal iID As Integer) As CContentCategory
            Return ContentCategory(iID)
        End Function
        Public Function GetContentFileByID(ByVal iID As Integer) As CContentFile
            Return ContentFile(iID)
        End Function
        Public Function GetContentFileByGUID(ByVal sGUID As String) As CContentFile
            Dim oContentFile As CContentFile
            For Each oContentFile In ContentFile.Values
                If oContentFile.GUID = sGUID Then Return oContentFile
            Next
            Return Nothing
        End Function
        Public Function GetContentFileTypeByID(ByVal iID As Integer) As CContentFileType
            Return ContentFileType(iID)
        End Function
        Public Function GetContentFileCategoryByID(ByVal iID As Integer) As CContentFileCategory
            Return ContentFileCategory(iID)
        End Function
        Public Function GetContentPackByID(ByVal iID As Integer) As CContentPack
            Return ContentPack(iID)
        End Function
        Public Function GetContentSubCategoryByID(ByVal iID As Integer) As CContentSubCategory
            Return ContentSubCategory(iID)
        End Function
        Public Function GetReferredByByID(ByVal iID As Integer) As CReferredBy
            Return ReferredBy(iID)
        End Function
        Public Function GetSubscriptionByID(ByVal iID As Integer) As CSubscription
            Return Subscription(iID)
        End Function

        Public Sub RefreshContentPack()
            mdSubscriptionRefresh = DateAdd(DateInterval.Year, -1, mdContentPackRefresh)
        End Sub
        Public Sub RefreshContent()
            mdSubscriptionRefresh = DateAdd(DateInterval.Year, -1, mdContentRefresh)
        End Sub
        Public Sub RefreshContentFile()
            mdSubscriptionRefresh = DateAdd(DateInterval.Year, -1, mdContentFileRefresh)
        End Sub
        Public Sub RefreshSubscription()
            mdSubscriptionRefresh = DateAdd(DateInterval.Year, -1, mdSubscriptionRefresh)
        End Sub

    End Class

End Namespace