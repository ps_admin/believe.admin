Namespace External.Revolution

    Public Module MOptions

        Dim moOptions As COptions

        Public ReadOnly Property Options() As COptions
            Get

                If moOptions Is Nothing Then
                    moOptions = New COptions
                End If
                Return moOptions

            End Get
        End Property

    End Module

End Namespace
