﻿Namespace Internal.Church


    <Serializable()> _
       <CLSCompliant(True)> _
    Public Class CAnotherLanguage
        Private miAnotherLanguageID As Integer
        Private msAnotherLanguage As String

        Public Sub New()
            miAnotherLanguageID = 0
            msAnotherLanguage = ""
        End Sub

        Public Sub New(ByRef oDataRow As DataRow)
            Me.New()

            miAnotherLanguageID = oDataRow("AnotherLanguage_ID")
            msAnotherLanguage = oDataRow("Language")

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miAnotherLanguageID
            End Get
        End Property

        Public Property Name() As String
            Get
                Return msAnotherLanguage
            End Get
            Set(ByVal value As String)
                msAnotherLanguage = value
            End Set
        End Property

    End Class
End Namespace
