Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CCampusMinistryRegion

        Private miCampusMinistryRegionID As Integer
        Private miCampusID As Integer
        Private miMinistryID As Integer
        Private miRegionID As Integer

        Public Sub New()

            miCampusMinistryRegionID = 0
            miCampusID = 0
            miMinistryID = 0
            miRegionID = 0

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miCampusMinistryRegionID = oDataRow("CampusMinistryRegion_ID")
            miCampusID = oDataRow("Campus_ID")
            miMinistryID = oDataRow("Ministry_ID")
            miRegionID = oDataRow("Region_ID")

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miCampusMinistryRegionID
            End Get
        End Property
        Public Property CampusID() As Integer
            Get
                Return miCampusID
            End Get
            Set(ByVal value As Integer)
                miCampusID = value
            End Set
        End Property
        Public ReadOnly Property Campus() As CCampus
            Get
                Return Lists.Internal.Church.GetCampusByID(miCampusID)
            End Get
        End Property
        Public Property MinistryID() As Integer
            Get
                Return miMinistryID
            End Get
            Set(ByVal value As Integer)
                miMinistryID = value
            End Set
        End Property
        Public ReadOnly Property Ministry() As CMinistry
            Get
                Return Lists.Internal.Church.GetMinistryByID(miMinistryID)
            End Get
        End Property
        Public Property RegionID() As Integer
            Get
                Return miRegionID
            End Get
            Set(ByVal value As Integer)
                miRegionID = value
            End Set
        End Property
        Public ReadOnly Property Region() As CRegion
            Get
                Return Lists.Internal.Church.GetRegionByID(miRegionID)
            End Get
        End Property

    End Class

End Namespace