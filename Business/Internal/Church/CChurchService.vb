Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CChurchService

        Private miChurchServiceID As Integer
        Private miCampusID As Integer
        Private msName As String
        Private mbDisplayInPCRReport As Boolean
        Private mbDisplayInKidsMinistryModule As Boolean
        Private miSortOrder As Integer

        Public Sub New()

            miChurchServiceID = 0
            miCampusID = 0
            msName = String.Empty
            mbDisplayInPCRReport = False
            mbDisplayInKidsMinistryModule = False
            miSortOrder = 0

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miChurchServiceID = oDataRow("ChurchService_ID")
            miCampusID = oDataRow("Campus_ID")
            msName = oDataRow("Name")
            mbDisplayInPCRReport = oDataRow("DisplayInPCRReport")
            mbDisplayInKidsMinistryModule = oDataRow("DisplayInKidsMinistryModule")
            miSortOrder = oDataRow("SortOrder")

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miChurchServiceID
            End Get
        End Property
        Public Property CampusID() As Integer
            Get
                Return miCampusID
            End Get
            Set(ByVal value As Integer)
                miCampusID = value
            End Set
        End Property
        Public ReadOnly Property Campus() As CCampus
            Get
                Return Lists.Internal.Church.GetCampusByID(miCampusID)
            End Get
        End Property
        Public Property Name() As String
            Get
                Return msName
            End Get
            Set(ByVal value As String)
                msName = value
            End Set
        End Property
        Public Property DisplayInPCRReport() As Boolean
            Get
                Return mbDisplayInPCRReport
            End Get
            Set(ByVal value As Boolean)
                mbDisplayInPCRReport = value
            End Set
        End Property
        Public Property DisplayInKidsMinistryModule() As Boolean
            Get
                Return mbDisplayInKidsMinistryModule
            End Get
            Set(ByVal value As Boolean)
                mbDisplayInKidsMinistryModule = value
            End Set
        End Property

        Public Property SortOrder() As Integer
            Get
                Return miSortOrder
            End Get
            Set(ByVal value As Integer)
                miSortOrder = value
            End Set
        End Property

    End Class

End Namespace