Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CContactKidsMinistryCode

        Private miContactKidsMinistryCodeID As Integer
        Private miContactID As Integer
        Private miKidsMinistryCodeID As Integer
        Private mdDateAdded As Date
        Private msGUID As String

        Private mbChanged As Boolean

        Public Sub New()

            miContactKidsMinistryCodeID = 0
            miContactID = 0
            miKidsMinistryCodeID = 0
            mdDateAdded = Nothing
            msGUID = System.Guid.NewGuid.ToString

            mbChanged = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miContactKidsMinistryCodeID = oDataRow("ContactKidsMinistryCode_ID")
            miContactID = oDataRow("Contact_ID")
            miKidsMinistryCodeID = oDataRow("KidsMinistryCode_ID")
            mdDateAdded = oDataRow("DateAdded")

            mbChanged = False

        End Sub

        Public Sub Save(ByVal oUser As CUser)

            If mbChanged Then

                Dim oParameters As New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", miContactID))
                oParameters.Add(New SqlClient.SqlParameter("@KidsMinistryCode_ID", miKidsMinistryCodeID))
                oParameters.Add(New SqlClient.SqlParameter("@DateAdded", SQLDateTime(mdDateAdded)))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))

                If Me.ID = 0 Then
                    miContactKidsMinistryCodeID = DataAccess.GetValue("sp_Church_ContactKidsMinistryCodeInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@ContactKidsMinistryCode_ID", miContactKidsMinistryCodeID))
                    DataAccess.ExecuteCommand("sp_Church_ContactKidsMinistryCodeUpdate", oParameters)
                End If

                mbChanged = False

            End If

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miContactKidsMinistryCodeID
            End Get
        End Property
        Public Property ContactID() As Integer
            Get
                Return miContactID
            End Get
            Set(ByVal value As Integer)
                If miContactID <> value Then mbChanged = True
                miContactID = value
            End Set
        End Property
        Public Property KidsMinistryCodeID() As Integer
            Get
                Return miKidsMinistryCodeID
            End Get
            Set(ByVal value As Integer)
                If miKidsMinistryCodeID <> value Then mbChanged = True
                miKidsMinistryCodeID = value
            End Set
        End Property
        Public ReadOnly Property KidsMinistryCode() As CKidsMinistryCode
            Get
                Return Lists.Internal.Church.GetKidsMinistryCodeByID(miKidsMinistryCodeID)
            End Get
        End Property
        Public Property DateAdded() As Date
            Get
                Return mdDateAdded
            End Get
            Set(ByVal value As Date)
                If mdDateAdded <> value Then mbChanged = True
                mdDateAdded = value
            End Set
        End Property
        Public ReadOnly Property Changed() As Boolean
            Get
                Return mbChanged
            End Get
        End Property
        Public Sub ResetChanges()
            mbChanged = False
        End Sub
        Public ReadOnly Property GUID() As String
            Get
                Return msGUID
            End Get
        End Property

    End Class

End Namespace