Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CContactRole
        Implements IComparable(Of CContactRole)

        Private miContactRoleID As Integer
        Private miContactID As Integer
        Private miRoleID As Integer
        Private miCampusID As Integer
        Private mdDateAdded As Date
        Private mbInactive As Boolean
        Private msGUID As String

        Private mcRestrictedRole As CArrayList(Of CRestrictedAccessRole)
        Private mcRestrictedULG As CArrayList(Of CRestrictedAccessULG)

        Private mbChanged As Boolean

        Public Sub New()

            miContactRoleID = 0
            miContactID = 0
            miRoleID = 0
            miCampusID = 0
            mdDateAdded = Nothing
            mbInactive = False
            msGUID = System.Guid.NewGuid.ToString

            mcRestrictedRole = New CArrayList(Of CRestrictedAccessRole)
            mcRestrictedULG = New CArrayList(Of CRestrictedAccessULG)

            mbChanged = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miContactRoleID = oDataRow("ContactRole_ID")
            miContactID = oDataRow("Contact_ID")
            miRoleID = oDataRow("Role_ID")
            miCampusID = oDataRow("Campus_ID")
            mdDateAdded = oDataRow("DateAdded")
            mbInactive = oDataRow("Inactive")

            Dim oRestrictedAccessRoleRow As DataRow
            For Each oRestrictedAccessRoleRow In oDataRow.GetChildRows("RestrictedAccessRole")
                mcRestrictedRole.Add(New CRestrictedAccessRole(oRestrictedAccessRoleRow))
            Next

            Dim oRestrictedAccessULGRow As DataRow
            For Each oRestrictedAccessULGRow In oDataRow.GetChildRows("RestrictedAccessULG")
                mcRestrictedULG.Add(New CRestrictedAccessULG(oRestrictedAccessULGRow))
            Next

            mcRestrictedRole.ResetChanges()
            mcRestrictedULG.ResetChanges()

            mbChanged = False

        End Sub

        Public Sub Save(ByVal oUser As CUser)

            If mbChanged Then

                Dim oParameters As New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", miContactID))
                oParameters.Add(New SqlClient.SqlParameter("@Role_ID", miRoleID))
                oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", miCampusID))
                oParameters.Add(New SqlClient.SqlParameter("@DateAdded", SQLDateTime(mdDateAdded)))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))

                If Me.ID = 0 Then
                    miContactRoleID = DataAccess.GetValue("sp_Church_ContactRoleInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@ContactRole_ID", miContactRoleID))
                    DataAccess.ExecuteCommand("sp_Church_ContactRoleUpdate", oParameters)
                End If

                mbChanged = False

            End If

            SaveRestrictedRoles(oUser)
            SaveRestrictedULG(oUser)

            mcRestrictedRole.ResetChanges()
            mcRestrictedULG.ResetChanges()

        End Sub

        Private Sub SaveRestrictedRoles(ByVal oUser As CUser)

            Dim oParameters As ArrayList
            Dim oRestrictedAccessRole As CRestrictedAccessRole

            'Remove the deleted Restricted Roles
            For Each oRestrictedAccessRole In mcRestrictedRole.Removed
                oParameters = New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@RestrictedAccessRole_ID", oRestrictedAccessRole.ID))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))
                DataAccess.ExecuteCommand("sp_Church_RestrictedAccessRoleDelete", oParameters)
            Next

            'Add/Update all others
            For Each oRestrictedAccessRole In mcRestrictedRole
                oRestrictedAccessRole.Save(miContactRoleID, oUser)
            Next

        End Sub

        Private Sub SaveRestrictedULG(ByVal oUser As CUser)

            Dim oParameters As ArrayList
            Dim oRestrictedAccessULG As CRestrictedAccessULG

            'Remove the Deleted Restricted ULG
            For Each oRestrictedAccessULG In mcRestrictedULG.Removed
                oParameters = New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@RestrictedAccessULG_ID", oRestrictedAccessULG.ID))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))
                DataAccess.ExecuteCommand("sp_Church_RestrictedAccessULGDelete", oParameters)
            Next

            'Add/Update all others
            For Each oRestrictedAccessULG In mcRestrictedULG
                oRestrictedAccessULG.Save(miContactRoleID, oUser)
            Next

        End Sub

        Public ReadOnly Property RestrictedAccessRoles() As CArrayList(Of CRestrictedAccessRole)
            Get
                Return mcRestrictedRole
            End Get
        End Property
        Public ReadOnly Property RestrictedAccessULG() As CArrayList(Of CRestrictedAccessULG)
            Get
                Return mcRestrictedULG
            End Get
        End Property

        Public ReadOnly Property ID() As Integer
            Get
                Return miContactRoleID
            End Get
        End Property
        Public Property ContactID() As Integer
            Get
                Return miContactID
            End Get
            Set(ByVal value As Integer)
                If miContactID <> value Then mbChanged = True
                miContactID = value
            End Set
        End Property
        Public Property RoleID() As Integer
            Get
                Return miRoleID
            End Get
            Set(ByVal value As Integer)
                If miRoleID <> value Then mbChanged = True
                miRoleID = value
            End Set
        End Property
        Public ReadOnly Property Role() As CRole
            Get
                Return Lists.Internal.Church.GetRoleByID(miRoleID)
            End Get
        End Property
        Public Property CampusID() As Integer
            Get
                Return miCampusID
            End Get
            Set(ByVal value As Integer)
                If miCampusID <> value Then mbChanged = True
                miCampusID = value
            End Set
        End Property
        Public ReadOnly Property Campus() As CCampus
            Get
                Return Lists.Internal.Church.GetCampusByID(miCampusID)
            End Get
        End Property
        Public Property DateAdded() As Date
            Get
                Return mdDateAdded
            End Get
            Set(ByVal value As Date)
                If mdDateAdded <> value Then mbChanged = True
                mdDateAdded = value
            End Set
        End Property
        Public ReadOnly Property Changed() As Boolean
            Get
                If mbChanged Then Return True

                If mcRestrictedRole.Changed Then Return True
                If mcRestrictedULG.Changed Then Return True

                Dim oRestrictedRole As CRestrictedAccessRole
                For Each oRestrictedRole In mcRestrictedRole
                    If oRestrictedRole.Changed Then Return True
                Next
                Dim oRestrictedULG As CRestrictedAccessULG
                For Each oRestrictedULG In mcRestrictedULG
                    If oRestrictedULG.Changed Then Return True
                Next

                Return False

            End Get
        End Property

        Public Sub ResetChanges()

            mbChanged = False

            mcRestrictedRole.ResetChanges()
            mcRestrictedULG.ResetChanges()

            Dim oRestrictedRole As CRestrictedAccessRole
            For Each oRestrictedRole In mcRestrictedRole
                oRestrictedRole.ResetChanges()
            Next
            Dim oRestrictedULG As CRestrictedAccessULG
            For Each oRestrictedULG In mcRestrictedULG
                oRestrictedULG.ResetChanges()
            Next

        End Sub

        Public ReadOnly Property GUID() As String
            Get
                Return msGUID
            End Get
        End Property

        Public ReadOnly Property Inactive() As Boolean
            Get
                Return mbInactive
            End Get
        End Property

        Public Function CompareTo(ByVal other As CContactRole) As Integer Implements System.IComparable(Of CContactRole).CompareTo
            'Return String.Compare(Me.Role.Name, other.Role.Name)
            'Code change by BIT for object not set error
            Return String.Compare(FormatObjectForTable(Me.Role), FormatObjectForTable(other.Role))
        End Function

    End Class

End Namespace