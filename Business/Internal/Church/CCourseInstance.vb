Namespace Internal.Church

    <Serializable()>
    <CLSCompliant(True)>
    Public Class CCourseInstance
        Implements IComparable(Of CCourseInstance)

        Private miCourseInstanceID As Integer
        Private miCourseID As Integer
        Private msName As String
        Private mdStartDate As Date

        Private mdEndDate As Date
        Private mcCost As Decimal

        Private mbDeleted As Boolean
        Private msGUID As String
        Private mcCourseInstanceCampus As CArrayList(Of CCourseInstanceCampus)

        Private mbChanged As Boolean

        Public Sub New()

            miCourseInstanceID = 0
            miCourseID = 0
            msName = String.Empty
            mdStartDate = Nothing
            mbDeleted = False
            msGUID = System.Guid.NewGuid.ToString
            mcCourseInstanceCampus = New CArrayList(Of CCourseInstanceCampus)

            mbChanged = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miCourseInstanceID = oDataRow("CourseInstance_ID")
            miCourseID = oDataRow("Course_ID")
            msName = oDataRow("Name")
            mdStartDate = IsNull(oDataRow("StartDate"), Nothing)

            mdEndDate = IsNull(oDataRow("EndDate"), Nothing)
            mcCost = IsNull(oDataRow("Cost"), Nothing)
            mbDeleted = oDataRow("Deleted")


            For Each oCourseInstanceCampusRow As DataRow In oDataRow.GetChildRows("CourseInstanceCampus")
                mcCourseInstanceCampus.Add(New CCourseInstanceCampus(oCourseInstanceCampusRow))
            Next
            mcCourseInstanceCampus.ResetChanges()

            mbChanged = False

        End Sub

        Public Sub Save()

            If mbChanged Then

                Dim oParameters As New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@Course_ID", miCourseID))
                oParameters.Add(New SqlClient.SqlParameter("@Name", msName))
                oParameters.Add(New SqlClient.SqlParameter("@StartDate", SQLDate(mdStartDate)))
                oParameters.Add(New SqlClient.SqlParameter("@EndDate", mdEndDate))
                oParameters.Add(New SqlClient.SqlParameter("@Cost", mcCost))
                oParameters.Add(New SqlClient.SqlParameter("@Deleted", mbDeleted))

                If Me.ID = 0 Then
                    miCourseInstanceID = DataAccess.GetValue("sp_Church_CourseInstanceCostInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@CourseInstance_ID", miCourseInstanceID))
                    DataAccess.ExecuteCommand("sp_Church_CourseInstanceCostUpdate", oParameters)
                End If

                mbChanged = False

            End If

            SaveCourseInstanceCampuss()
            mcCourseInstanceCampus.ResetChanges()

            'Update course instance in the Global List
            'Need to update the CourseSession in the List here, not when the SP is called above (as is done in CourseTopic), 
            'because CourseInstanceCampuss only may have changed, which means update would not be called...
            Lists.Internal.Church.CourseInstance(Me.ID) = Me

        End Sub

        Private Sub SaveCourseInstanceCampuss()

            Dim oParameters As ArrayList
            Dim oCourseInstanceCampus As CCourseInstanceCampus

            'Remove the Deleted payments
            For Each oCourseInstanceCampus In mcCourseInstanceCampus.Removed
                oParameters = New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@CourseInstanceCampus_ID", oCourseInstanceCampus.ID))
                DataAccess.ExecuteCommand("sp_Church_CourseInstanceCampusDelete", oParameters)
            Next

            For Each oCourseInstanceCampus In mcCourseInstanceCampus
                oCourseInstanceCampus.CourseInstanceID = miCourseInstanceID
                oCourseInstanceCampus.Save()
            Next

        End Sub

        Public ReadOnly Property CourseInstanceCampus() As CArrayList(Of CCourseInstanceCampus)
            Get
                mcCourseInstanceCampus.Sort()
                Return mcCourseInstanceCampus
            End Get
        End Property

        Public ReadOnly Property CourseSession() As CArrayList(Of CCourseSession)
            Get
                Dim oArrayList As New CArrayList(Of CCourseSession)

                For Each oCourseSession As CCourseSession In Lists.Internal.Church.CourseSession.Values
                    If oCourseSession.CourseInstanceID = miCourseInstanceID Then
                        oArrayList.Add(oCourseSession)
                    End If
                Next

                oArrayList.Sort()
                Return oArrayList

            End Get
        End Property
        Public ReadOnly Property CourseSessionCurrent() As CArrayList(Of CCourseSession)
            Get
                Dim oArraylist As New CArrayList(Of CCourseSession)
                Dim oCourseSession As CCourseSession
                For Each oCourseSession In CourseSession
                    If Not oCourseSession.Deleted Then oArraylist.Add(oCourseSession)
                Next

                oArraylist.Sort()
                Return oArraylist

            End Get
        End Property

        Public ReadOnly Property ID() As Integer
            Get
                Return miCourseInstanceID
            End Get
        End Property
        Public Property CourseID() As Integer
            Get
                Return miCourseID
            End Get
            Set(ByVal value As Integer)
                If miCourseID <> value Then mbChanged = True
                miCourseID = value
            End Set
        End Property
        Public ReadOnly Property Course() As CCourse
            Get
                Return Lists.Internal.Church.GetCourseByID(miCourseID)
            End Get
        End Property
        Public Property Name() As String
            Get
                Return msName
            End Get
            Set(ByVal value As String)
                If msName <> value Then mbChanged = True
                msName = value
            End Set
        End Property

        Public Property Cost() As Decimal
            Get
                Return mcCost
            End Get
            Set(ByVal value As Decimal)
                If mcCost <> value Then mbChanged = True
                mcCost = value
            End Set
        End Property
        Public ReadOnly Property CampusList() As String
            Get
                Dim sReturn As String = String.Empty
                For Each oCourseInstanceCampus As CCourseInstanceCampus In mcCourseInstanceCampus
                    sReturn += oCourseInstanceCampus.Campus.ShortName & ", "
                Next
                If sReturn.Length > 0 Then sReturn = sReturn.Substring(0, sReturn.Length - 2)
                Return sReturn
            End Get
        End Property
        Public ReadOnly Property CampusListAndName() As String
            Get
                Dim sReturn As String = CampusList
                If sReturn.Length > 0 Then sReturn += " - "
                Return sReturn & Me.Name
            End Get
        End Property
        'Public ReadOnly Property StartDate() As Date
        '    Get
        '        'We are no longer using StartDate (CourseInstance) to keep track of the date of the Course
        '        'We are using the first date from the Sessions inside CourseInstance now....
        '        If mdStartDate = Nothing Then
        '            Dim dStartDate As Date = Nothing
        '            For Each oCourseSession As CCourseSession In CourseSession
        '                If Not oCourseSession.Deleted Then
        '                    If dStartDate = Nothing OrElse oCourseSession.Date < dStartDate Then dStartDate = oCourseSession.Date
        '                End If
        '            Next
        '            Return dStartDate
        '        Else
        '            Return mdStartDate
        '        End If
        '    End Get
        'End Property

        Public Property StartDate() As Date
            Get
                Return mdStartDate
            End Get
            Set(ByVal value As Date)
                If mdStartDate <> value Then mbChanged = True
                mdStartDate = value
            End Set
        End Property

        Public Property EndDate() As Date
            Get
                Return mdEndDate
            End Get
            Set(ByVal value As Date)
                If mdEndDate <> value Then mbChanged = True
                mdEndDate = value
            End Set
        End Property
        Public Property Deleted() As Boolean
            Get
                Return mbDeleted
            End Get
            Set(ByVal value As Boolean)
                If mbDeleted <> value Then mbChanged = True
                mbDeleted = value
            End Set
        End Property
        Public ReadOnly Property GUID() As String
            Get
                Return msGUID
            End Get
        End Property

        Public ReadOnly Property HasCampus(ByVal oCampus As CCampus) As Boolean
            Get
                For Each oSearchCampus As CCourseInstanceCampus In CourseInstanceCampus
                    If oSearchCampus.CampusID = oCampus.ID Then
                        Return True
                    End If
                Next
                Return False
            End Get
        End Property
        Public Function CompareTo(ByVal other As CCourseInstance) As Integer Implements System.IComparable(Of CCourseInstance).CompareTo

            Return Date.Compare(other.StartDate, StartDate)

        End Function

    End Class

End Namespace