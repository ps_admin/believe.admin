Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CCourseSession
        Implements IComparable(Of CCourseSession)

        Private miCourseSessionID As Integer
        Private miCourseInstanceID As Integer
        Private mdDate As Date
        Private mbDeleted As Boolean
        Private msGUID As String
        Private mcCourseSessionTopic As CArrayList(Of CCourseSessionTopic)

        Private mbChanged As Boolean

        Public Sub New()

            miCourseSessionID = 0
            miCourseInstanceID = 0
            mdDate = Nothing
            mbDeleted = False
            msGUID = System.Guid.NewGuid.ToString
            mcCourseSessionTopic = New CArrayList(Of CCourseSessionTopic)

            mbChanged = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miCourseSessionID = oDataRow("CourseSession_ID")
            miCourseInstanceID = oDataRow("CourseInstance_ID")
            mdDate = oDataRow("Date")
            mbDeleted = oDataRow("Deleted")

            For Each oCourseSessionTopicRow As DataRow In oDataRow.GetChildRows("CourseSessionTopic")
                mcCourseSessionTopic.Add(New CCourseSessionTopic(oCourseSessionTopicRow))
            Next
            mcCourseSessionTopic.ResetChanges()

            mbChanged = False

        End Sub

        Public Sub Save()

            If mbChanged Then

                Dim oParameters As New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@CourseInstance_ID", miCourseInstanceID))
                oParameters.Add(New SqlClient.SqlParameter("@Date", SQLDate(mdDate)))
                oParameters.Add(New SqlClient.SqlParameter("@Deleted", mbDeleted))

                If Me.ID = 0 Then
                    miCourseSessionID = DataAccess.GetValue("sp_Church_CourseSessionInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@CourseSession_ID", miCourseSessionID))
                    DataAccess.ExecuteCommand("sp_Church_CourseSessionUpdate", oParameters)
                End If

                mbChanged = False

            End If

            SaveCourseSessionTopics()
            mcCourseSessionTopic.ResetChanges()

            'Update course session in the Global List
            'Need to update the CourseSession in the List here, not when the SP is called above (as is done in CourseTopic), 
            'because CourseSessionTopics only may have changed, which means update would not be called...
            Lists.Internal.Church.CourseSession(Me.ID) = Me

        End Sub

        Private Sub SaveCourseSessionTopics()

            Dim oParameters As ArrayList
            Dim oCourseSessionTopic As CCourseSessionTopic

            'Remove the Deleted payments
            For Each oCourseSessionTopic In mcCourseSessionTopic.Removed
                oParameters = New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@CourseSessionTopic_ID", oCourseSessionTopic.ID))
                DataAccess.ExecuteCommand("sp_Church_CourseSessionTopicDelete", oParameters)
            Next

            For Each oCourseSessionTopic In mcCourseSessionTopic
                oCourseSessionTopic.CourseSessionID = miCourseSessionID
                oCourseSessionTopic.Save()
            Next

        End Sub
        Public ReadOnly Property CourseSessionTopic() As CArrayList(Of CCourseSessionTopic)
            Get
                mcCourseSessionTopic.Sort()
                Return mcCourseSessionTopic
            End Get
        End Property

        Public ReadOnly Property ID() As Integer
            Get
                Return miCourseSessionID
            End Get
        End Property
        Public Property CourseInstanceID() As Integer
            Get
                Return miCourseInstanceID
            End Get
            Set(ByVal value As Integer)
                If miCourseInstanceID <> value Then mbChanged = True
                miCourseInstanceID = value
            End Set
        End Property
        Public ReadOnly Property CourseInstance() As CCourseInstance
            Get
                Return Lists.Internal.Church.GetCourseInstanceByID(miCourseInstanceID)
            End Get
        End Property
        Public Property [Date]() As Date
            Get
                Return mdDate
            End Get
            Set(ByVal value As Date)
                If mdDate <> value Then mbChanged = True
                mdDate = value
            End Set
        End Property
        Public Property Deleted() As Boolean
            Get
                Return mbDeleted
            End Get
            Set(ByVal value As Boolean)
                If mbDeleted <> value Then mbChanged = True
                mbDeleted = value
            End Set
        End Property
        Public ReadOnly Property GUID() As String
            Get
                Return msGUID
            End Get
        End Property

        Public Function CompareTo(ByVal other As CCourseSession) As Integer Implements System.IComparable(Of CCourseSession).CompareTo

            Return Date.Compare(CDate(Me.Date), CDate(other.Date))

        End Function

    End Class

End Namespace