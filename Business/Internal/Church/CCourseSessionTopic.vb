Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CCourseSessionTopic
        Implements IComparable(Of CCourseSessionTopic)

        Private miCourseSessionTopicID As Integer
        Private miCourseSessionID As Integer
        Private miCourseTopicID As String

        Private mbChanged As Boolean

        Public Sub New()

            miCourseSessionTopicID = 0
            miCourseSessionID = 0
            miCourseTopicID = 0

            mbChanged = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miCourseSessionTopicID = oDataRow("CourseSessionTopic_ID")
            miCourseSessionID = oDataRow("CourseSession_ID")
            miCourseTopicID = oDataRow("CourseTopic_ID")
            mbChanged = False

        End Sub

        Public Sub Save()

            If mbChanged Then

                Dim oParameters As New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@CourseSession_ID", miCourseSessionID))
                oParameters.Add(New SqlClient.SqlParameter("@CourseTopic_ID", miCourseTopicID))

                If Me.ID = 0 Then
                    miCourseSessionTopicID = DataAccess.GetValue("sp_Church_CourseSessionTopicInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@CourseSessionTopic_ID", miCourseSessionTopicID))
                    DataAccess.ExecuteCommand("sp_Church_CourseSessionTopicUpdate", oParameters)
                End If

                mbChanged = False

            End If

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miCourseSessionTopicID
            End Get
        End Property
        Public Property CourseSessionID() As Integer
            Get
                Return miCourseSessionID
            End Get
            Set(ByVal value As Integer)
                If miCourseSessionID <> value Then mbChanged = True
                miCourseSessionID = value
            End Set
        End Property
        Public Property CourseTopicID() As String
            Get
                Return miCourseTopicID
            End Get
            Set(ByVal value As String)
                If miCourseTopicID <> value Then mbChanged = True
                miCourseTopicID = value
            End Set
        End Property
        Public ReadOnly Property CourseTopic() As CCourseTopic
            Get
                Return Lists.Internal.Church.GetCourseTopicByID(miCourseTopicID)
            End Get
        End Property

        Public Function CompareTo(ByVal other As CCourseSessionTopic) As Integer Implements System.IComparable(Of CCourseSessionTopic).CompareTo

            Return String.Compare(Me.CourseTopic.Name, other.CourseTopic.Name)

        End Function

    End Class

End Namespace