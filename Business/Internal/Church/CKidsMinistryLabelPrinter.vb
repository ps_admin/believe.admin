Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CKidsMinistryLabelPrinter

        Private miKidsMinistryLabelPrinterID As Integer
        Private msName As String
        Private msPrinterName As String
        Private miSortOrder As Integer

        Public Sub New()

            miKidsMinistryLabelPrinterID = 0
            msName = String.Empty
            msPrinterName = String.Empty
            miSortOrder = 0

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miKidsMinistryLabelPrinterID = oDataRow("KidsMinistryLabelPrinter_ID")
            msName = oDataRow("Name")
            msPrinterName = oDataRow("PrinterName")
            miSortOrder = oDataRow("SortOrder")

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miKidsMinistryLabelPrinterID
            End Get
        End Property
        Public Property Name() As String
            Get
                Return msName
            End Get
            Set(ByVal value As String)
                msName = value
            End Set
        End Property
        Public Property PrinterName() As String
            Get
                Return msPrinterName
            End Get
            Set(ByVal value As String)
                msPrinterName = value
            End Set
        End Property
        Public Property SortOrder() As Integer
            Get
                Return miSortOrder
            End Get
            Set(ByVal value As Integer)
                miSortOrder = value
            End Set
        End Property

    End Class

End Namespace