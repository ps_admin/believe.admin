Namespace Internal.Church

    <CLSCompliant(True)> _
    Public Class CLists
        Private moSuperAdmin As Hashtable
        Private mo247PrayerContactMethod As Hashtable
        Private mo247PrayerDay As Hashtable
        Private mo247PrayerDuration As Hashtable
        Private mo247PrayerTime As Hashtable
        Private moCampus As Hashtable
        Private moCampusMinistryRegion As Hashtable
        Private moCarParkUsed As Hashtable
        Private moChurchStatus As Hashtable
        Private moChurchStatusNewRecord As Hashtable
        Private moCommentSource As Hashtable
        Private moCommentType As Hashtable
        Private moCourse As Hashtable
        Private moCourseInstanceName As Hashtable
        Private moCourseInstance As Hashtable
        Private moCourseTopic As Hashtable
        Private moCourseSession As Hashtable
        Private moDeleteReason As Hashtable
        Private moMatchID As Hashtable
        Private moKidsMinistryCode As Hashtable
        Private moKidsMinistryLabelPrinter As Hashtable
        Private moMinistry As Hashtable
        Private moEducation As Hashtable
        Private moEmploymentIndustry As Hashtable
        Private moEmploymentPosition As Hashtable
        Private moEmploymentStatus As Hashtable
        Private moEntryPoint As Hashtable
        Private moModeOfTransport As Hashtable
        Private moNCSummaryStatsMinistry As Hashtable
        Private moNPNCInitialStatus As Hashtable
        Private moNPNCOutcomeInactive As Hashtable
        Private moNPNCType As Hashtable
        Private moNCDecisionType As Hashtable
        Private moRegion As Hashtable
        Private moRegionalPastor As Hashtable
        Private moRoleType As Hashtable
        Private moRoleTypeUL As Hashtable
        Private moRoom As Hashtable
        Private moPCRDate As Hashtable
        Private moRole As Hashtable
        Private moCategory As Hashtable
        Private moChurchService As Hashtable
        Private moULG As Hashtable
        Private moULGDateDay As Hashtable
        Private moULGDateTime As Hashtable
        Private moPKRoom As Hashtable
        Private moAnotherLanguage As Hashtable
        Private moNationality As Hashtable
        Private moPCR As Hashtable
        Private moPaidCourses As Hashtable
        Private gdULGLastRefresh As Date

        '╔══════════════════════ Volunteer START ═══════════════════════════════════════════════╗
        Private moVolunteerArea As Hashtable
        Private moVolunteerApplication As Hashtable
        Private moVolunteerApproval As Hashtable
        Private moVolunteerApprovalType As Hashtable
        Private moVolunteerApprover As Hashtable
        Private moVolunteerAnswer As Hashtable
        Private moVolunteerQuestion As Hashtable
        '╚══════════════════════ Volunteer END ═════════════════════════════════════════════════╝

        Public ReadOnly Property SuperAdmins() As Hashtable
            Get
                If moSuperAdmin Is Nothing Then
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("select email from common_contact cc join common_contactdatabaserole ccr on ccr.contact_ID=cc.contact_ID and ccr.databaserole_id=62")

                    moSuperAdmin = New Hashtable
                End If
                Return moSuperAdmin
            End Get
        End Property

        Public ReadOnly Property Prayer247ContactMethod() As Hashtable
            Get
                If mo247PrayerContactMethod Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Church_247PrayerContactMethod")

                    mo247PrayerContactMethod = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim o247PrayerContactMethod As New C247PrayerContactMethod(oDataRow)
                        mo247PrayerContactMethod.Add(o247PrayerContactMethod.ID, o247PrayerContactMethod)
                    Next
                End If
                Return mo247PrayerContactMethod
            End Get
        End Property

        Public ReadOnly Property Prayer247Day() As Hashtable
            Get
                If mo247PrayerDay Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Church_247PrayerDay")

                    mo247PrayerDay = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim o247PrayerDay As New C247PrayerDay(oDataRow)
                        mo247PrayerDay.Add(o247PrayerDay.ID, o247PrayerDay)
                    Next
                End If
                Return mo247PrayerDay
            End Get
        End Property

        Public ReadOnly Property Prayer247Duration() As Hashtable
            Get
                If mo247PrayerDuration Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Church_247PrayerDuration")

                    mo247PrayerDuration = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim o247PrayerDuration As New C247PrayerDuration(oDataRow)
                        mo247PrayerDuration.Add(o247PrayerDuration.ID, o247PrayerDuration)
                    Next
                End If
                Return mo247PrayerDuration
            End Get
        End Property
        Public ReadOnly Property Prayer247Time() As Hashtable
            Get
                If mo247PrayerTime Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Church_247PrayerTime")

                    mo247PrayerTime = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim o247PrayerTime As New C247PrayerTime(oDataRow)
                        mo247PrayerTime.Add(o247PrayerTime.ID, o247PrayerTime)
                    Next
                End If
                Return mo247PrayerTime
            End Get
        End Property

        Public ReadOnly Property CourseInstanceName() As Hashtable
            Get
                If moCourseInstanceName Is Nothing Then

                    Dim oTableMappings As New ArrayList
                    oTableMappings.Add(New System.Data.Common.DataTableMapping("Table", "CourseAndInstanceName"))

                    Dim oDataSet As DataSet = DataAccess.GetDataSet("sp_Church_GetCourseAndInstanceName", oTableMappings)

                    moCourseInstanceName = New Hashtable
                    For Each oDataRow As DataRow In oDataSet.Tables(0).Rows
                        Dim oCourseInstance As New CCourseInstance(oDataRow)
                        moCourseInstanceName.Add(oCourseInstance.ID, oCourseInstance)
                    Next

                End If
                Return moCourseInstanceName
            End Get
        End Property

        Public ReadOnly Property Campus() As Hashtable
            Get
                If moCampus Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Church_Campus ORDER BY SortOrder")

                    moCampus = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oCampus As New CCampus(oDataRow)
                        moCampus.Add(oCampus.ID, oCampus)
                    Next
                End If
                Return moCampus
            End Get
        End Property

        Public ReadOnly Property CampusPlusAll() As Hashtable
            Get
                Dim oHashTable As Hashtable = Campus.Clone
                Dim oCampus As New CCampus(Integer.MaxValue, "All")
                oHashTable.Add(oCampus.ID, oCampus)
                Return oHashTable
            End Get
        End Property

        Public ReadOnly Property CampusMinistryRegion() As Hashtable
            Get
                If moCampusMinistryRegion Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("sp_Church_CampusMinistryRegionSelectAll")

                    moCampusMinistryRegion = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oCampusMinistryRegion As New CCampusMinistryRegion(oDataRow)
                        moCampusMinistryRegion.Add(oCampusMinistryRegion.ID, oCampusMinistryRegion)
                    Next
                End If
                Return moCampusMinistryRegion
            End Get
        End Property

        Public ReadOnly Property CarParkUsed() As Hashtable
            Get
                If moCarParkUsed Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Church_CarParkUsed ORDER BY SortOrder")

                    moCarParkUsed = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oCarParkUsed As New CCarParkUsed(oDataRow)
                        moCarParkUsed.Add(oCarParkUsed.ID, oCarParkUsed)
                    Next
                End If
                Return moCarParkUsed
            End Get
        End Property

        Public ReadOnly Property ChurchStatus() As Hashtable
            Get
                If moChurchStatus Is Nothing Then
                    Dim oDataRow As DataRow

                    Dim oTableMappings As New ArrayList
                    oTableMappings.Add(New System.Data.Common.DataTableMapping("Table", "ChurchStatus"))
                    oTableMappings.Add(New System.Data.Common.DataTableMapping("Table1", "ChurchStatusChange"))

                    Dim oDataSet As DataSet = DataAccess.GetDataSet("sp_Church_ChurchStatusSelectAll", oTableMappings)

                    oDataSet.Relations.Add("ChurchStatusChange", oDataSet.Tables("ChurchStatus").Columns("ChurchStatus_ID"), oDataSet.Tables("ChurchStatusChange").Columns("ChurchStatusFrom_ID"))

                    'We need to populate the ChurchStatus list before we populate the 'allowed changes' lists in each ChurchStatus
                    moChurchStatus = New Hashtable
                    For Each oDataRow In oDataSet.Tables(0).Rows
                        Dim oNewChurchStatus As New CChurchStatus(oDataRow)
                        moChurchStatus.Add(oNewChurchStatus.ID, oNewChurchStatus)
                    Next

                    Dim oChurchStatus As CChurchStatus
                    For Each oChurchStatus In moChurchStatus.Values
                        oChurchStatus.LoadChangeRow(oDataSet.Tables(0).Select("ChurchStatus_ID = " & oChurchStatus.ID)(0))
                    Next

                End If
                Return moChurchStatus
            End Get
        End Property

        'Public ReadOnly Property ChurchStatus(ByVal bIncludeDeleted As Boolean) As Hashtable
        '    Get
        '        Dim oChurchStatus As CChurchStatus
        '        Dim oHashTable As New Hashtable
        '        For Each oChurchStatus In ChurchStatus().Values
        '            If Not oChurchStatus.Deleted Then oHashTable.Add(oChurchStatus.ID, oChurchStatus)
        '        Next
        '        Return oHashTable
        '    End Get
        'End Property

        'This is a list of the church statuses allowed for new records.
        Public ReadOnly Property ChurchStatusNewRecord() As Hashtable
            Get
                If moChurchStatusNewRecord Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Church_ChurchStatusAllowedChanges WHERE ChurchStatusFrom_ID IS NULL")

                    moChurchStatusNewRecord = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oChurchStatus As CChurchStatus = GetChurchStatusByID(oDataRow("ChurchStatusTo_ID"))
                        moChurchStatusNewRecord.Add(oChurchStatus.ID, oChurchStatus)
                    Next
                End If
                Return moChurchStatusNewRecord
            End Get
        End Property

        Public ReadOnly Property CommentSource() As Hashtable
            Get
                If moCommentSource Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Church_CommentSource")

                    moCommentSource = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oCommentSource As New CCommentSource(oDataRow)
                        moCommentSource.Add(oCommentSource.ID, oCommentSource)
                    Next
                End If
                Return moCommentSource
            End Get
        End Property
        Public ReadOnly Property CommentSource(ByVal bShowInNPNC As Boolean) As Hashtable
            Get
                Dim oCommentSource As CCommentSource
                Dim oHashTable As New Hashtable
                For Each oCommentSource In CommentSource().Values
                    If oCommentSource.ShowInNPNC = bShowInNPNC Then
                        oHashTable.Add(oCommentSource.ID, oCommentSource)
                    End If
                Next
                Return oHashTable
            End Get
        End Property

        Public ReadOnly Property CommentType() As Hashtable
            Get
                If moCommentType Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Church_CommentType")

                    moCommentType = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oCommentType As New CCommentType(oDataRow)
                        moCommentType.Add(oCommentType.ID, oCommentType)
                    Next
                End If
                Return moCommentType
            End Get
        End Property
        Public ReadOnly Property Course() As Hashtable
            Get
                If moCourse Is Nothing Then

                    Dim oTableMappings As New ArrayList
                    oTableMappings.Add(New System.Data.Common.DataTableMapping("Table", "Course"))
                    oTableMappings.Add(New System.Data.Common.DataTableMapping("Table1", "CourseInstance"))
                    oTableMappings.Add(New System.Data.Common.DataTableMapping("Table2", "CourseTopic"))
                    oTableMappings.Add(New System.Data.Common.DataTableMapping("Table3", "CourseSession"))

                    Dim oDataSet As DataSet = DataAccess.GetDataSet("sp_Church_CourseSelectAll", oTableMappings)

                    oDataSet.Relations.Add("CourseInstance", oDataSet.Tables("Course").Columns("Course_ID"), oDataSet.Tables("CourseInstance").Columns("Course_ID"))
                    oDataSet.Relations.Add("CourseTopic", oDataSet.Tables("Course").Columns("Course_ID"), oDataSet.Tables("CourseTopic").Columns("Course_ID"))
                    oDataSet.Relations.Add("CourseSession", oDataSet.Tables("CourseInstance").Columns("CourseInstance_ID"), oDataSet.Tables("CourseSession").Columns("CourseInstance_ID"))

                    moCourse = New Hashtable
                    For Each oDataRow As DataRow In oDataSet.Tables(0).Rows
                        Dim oCourse As New CCourse(oDataRow)
                        moCourse.Add(oCourse.ID, oCourse)
                    Next

                End If
                Return moCourse
            End Get
        End Property
        Public ReadOnly Property CourseCurrent() As Hashtable
            Get
                Dim oHashTable As New Hashtable
                Dim oCourse As CCourse
                For Each oCourse In Course.Values
                    If Not oCourse.Deleted Then
                        oHashTable.Add(oCourse.ID, oCourse)
                    End If
                Next
                Return oHashTable
            End Get
        End Property

        Public ReadOnly Property CourseInstance() As Hashtable
            Get
                If moCourseInstance Is Nothing Then

                    Dim oTableMappings As New ArrayList
                    oTableMappings.Add(New System.Data.Common.DataTableMapping("Table", "CourseInstance"))
                    oTableMappings.Add(New System.Data.Common.DataTableMapping("Table1", "CourseInstanceCampus"))

                    Dim oDataSet As DataSet = DataAccess.GetDataSet("sp_Church_CourseInstanceSelectAll", oTableMappings)

                    oDataSet.Relations.Add("CourseInstanceCampus", oDataSet.Tables("CourseInstance").Columns("CourseInstance_ID"), oDataSet.Tables("CourseInstanceCampus").Columns("CourseInstance_ID"))

                    moCourseInstance = New Hashtable
                    For Each oDataRow As DataRow In oDataSet.Tables(0).Rows
                        Dim oCourseInstance As New CCourseInstance(oDataRow)
                        moCourseInstance.Add(oCourseInstance.ID, oCourseInstance)
                    Next

                End If
                Return moCourseInstance
            End Get
        End Property
        
         Public ReadOnly Property PaidCourseInstance(CourseID As Integer) As Hashtable
            Get
                Dim oDataTable As DataTable = DataAccess.GetDataTable("select * from church_courseinstance where course_id =" + CourseID.ToString() + "  and cost > 0 and Deleted = 0")
                moCourseInstance = New Hashtable
                For Each oDataRow As DataRow In oDataTable.Rows
                    Dim oCourseInstance As New CCourseInstance(oDataRow)
                    moCourseInstance.Add(oCourseInstance.Name, oCourseInstance)
                Next
                Return moCourseInstance
            End Get
        End Property
        
         Public ReadOnly Property PaidCourses(campusID As Integer) As Hashtable
            Get
                ' If moPaidCourses Is Nothing Then               
                Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("select * from church_course where course_id in (Select course_id from church_courseinstance where cost > 0 And Deleted = 0) And Deleted = 0 And course_id In (Select Course_ID from Church_courseinstancecampus where campus_id =" + campusID.ToString() + ")")
                    moPaidCourses = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oCoursePaid As New CCourse(oDataRow)
                        moPaidCourses.Add(oCoursePaid.ID, oCoursePaid)
                    Next
                '  End If
                Return moPaidCourses
            End Get
        End Property

        Public ReadOnly Property CourseDates(campusID As Integer) As Hashtable
            Get
                If moPaidCourses Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("select * from church_course where course_id in 
                                                                        (Select course_id from church_courseinstance where cost > 0 And Deleted = 0) And Deleted = 0 
                                                                        And course_id In (Select Course_ID from Church_courseinstancecampus where campus_id =" + campusID + ")")
                    moPaidCourses = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oCoursePaid As New CCourseInstance(oDataRow)
                        moPaidCourses.Add(oCoursePaid.ID, oCoursePaid)
                    Next
                End If
                Return moPaidCourses
            End Get
        End Property

        
        Public ReadOnly Property CourseInstanceCurrent() As Hashtable
            Get
                Dim oHashTable As New Hashtable
                Dim oCourseInstance As CCourseInstance
                For Each oCourseInstance In CourseInstance.Values
                    If Not oCourseInstance.Deleted Then
                        oHashTable.Add(oCourseInstance.ID, oCourseInstance)
                    End If
                Next
                Return oHashTable
            End Get
        End Property

        Public ReadOnly Property CourseTopic() As Hashtable
            Get
                If moCourseTopic Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Church_CourseTopic")

                    moCourseTopic = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oCourseTopic As New CCourseTopic(oDataRow)
                        moCourseTopic.Add(oCourseTopic.ID, oCourseTopic)
                    Next
                End If
                Return moCourseTopic
            End Get
        End Property

        Public ReadOnly Property CourseSession() As Hashtable
            Get
                If moCourseSession Is Nothing Then

                    Dim oTableMappings As New ArrayList
                    oTableMappings.Add(New System.Data.Common.DataTableMapping("Table", "CourseSession"))
                    oTableMappings.Add(New System.Data.Common.DataTableMapping("Table1", "CourseSessionTopic"))

                    Dim oDataSet As DataSet = DataAccess.GetDataSet("sp_Church_CourseSessionSelectAll", oTableMappings)

                    oDataSet.Relations.Add("CourseSessionTopic", oDataSet.Tables("CourseSession").Columns("CourseSession_ID"), oDataSet.Tables("CourseSessionTopic").Columns("CourseSession_ID"))

                    moCourseSession = New Hashtable
                    For Each oDataRow As DataRow In oDataSet.Tables(0).Rows
                        Dim oCourseSession As New CCourseSession(oDataRow)
                        moCourseSession.Add(oCourseSession.ID, oCourseSession)
                    Next
                End If
                Return moCourseSession
            End Get
        End Property

        Public ReadOnly Property DeleteReason() As Hashtable
            Get
                If moDeleteReason Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Church_DeleteReason ORDER BY SortOrder")

                    moDeleteReason = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oDeleteReason As New CDeleteReason(oDataRow)
                        moDeleteReason.Add(oDeleteReason.ID, oDeleteReason)
                    Next
                End If
                Return moDeleteReason
            End Get
        End Property

        Public ReadOnly Property KidsMinistryCode() As Hashtable
            Get
                If moKidsMinistryCode Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Church_KidsMinistryCode ORDER BY SortOrder")

                    moKidsMinistryCode = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oKidsMinistryCode As New CKidsMinistryCode(oDataRow)
                        moKidsMinistryCode.Add(oKidsMinistryCode.ID, oKidsMinistryCode)
                    Next
                End If
                Return moKidsMinistryCode
            End Get
        End Property

        Public ReadOnly Property KidsMinistryLabelPrinter() As Hashtable
            Get
                If moKidsMinistryLabelPrinter Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Church_KidsMinistryLabelPrinter ORDER BY SortOrder")

                    moKidsMinistryLabelPrinter = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oKidsMinistryLabelPrinter As New CKidsMinistryLabelPrinter(oDataRow)
                        moKidsMinistryLabelPrinter.Add(oKidsMinistryLabelPrinter.ID, oKidsMinistryLabelPrinter)
                    Next
                End If
                Return moKidsMinistryLabelPrinter
            End Get
        End Property
        Public ReadOnly Property Education() As Hashtable
            Get
                If moEducation Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Church_Education ORDER BY SortOrder")

                    moEducation = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oEducation As New CEducation(oDataRow)
                        moEducation.Add(oEducation.ID, oEducation)
                    Next
                End If
                Return moEducation
            End Get
        End Property

        Public ReadOnly Property EmploymentIndustry() As Hashtable
            Get
                If moEmploymentIndustry Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Church_EmploymentIndustry ORDER BY SortOrder")

                    moEmploymentIndustry = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oEmploymentIndustry As New CEmploymentIndustry(oDataRow)
                        moEmploymentIndustry.Add(oEmploymentIndustry.ID, oEmploymentIndustry)
                    Next
                End If
                Return moEmploymentIndustry
            End Get
        End Property

        Public ReadOnly Property EmploymentPosition() As Hashtable
            Get
                If moEmploymentPosition Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Church_EmploymentPosition ORDER BY SortOrder")

                    moEmploymentPosition = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oEmploymentPosition As New CEmploymentPosition(oDataRow)
                        moEmploymentPosition.Add(oEmploymentPosition.ID, oEmploymentPosition)
                    Next
                End If
                Return moEmploymentPosition
            End Get
        End Property

        Public ReadOnly Property EmploymentStatus() As Hashtable
            Get
                If moEmploymentStatus Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Church_EmploymentStatus ORDER BY SortOrder")

                    moEmploymentStatus = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oEmploymentStatus As New CEmploymentStatus(oDataRow)
                        moEmploymentStatus.Add(oEmploymentStatus.ID, oEmploymentStatus)
                    Next
                End If
                Return moEmploymentStatus
            End Get
        End Property

        Public ReadOnly Property EntryPoint() As Hashtable
            Get
                If moEntryPoint Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Church_EntryPoint ORDER BY SortOrder")

                    moEntryPoint = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oEntryPoint As New CEntryPoint(oDataRow)
                        moEntryPoint.Add(oEntryPoint.ID, oEntryPoint)
                    Next
                End If
                Return moEntryPoint
            End Get
        End Property

        Public ReadOnly Property Ministry() As Hashtable
            Get
                If moMinistry Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Church_Ministry ORDER BY SortOrder")

                    moMinistry = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oMinistry As New CMinistry(oDataRow)
                        moMinistry.Add(oMinistry.ID, oMinistry)
                    Next
                End If
                Return moMinistry
            End Get
        End Property

        Public ReadOnly Property ModeOfTransport() As Hashtable
            Get
                If moModeOfTransport Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Church_ModeOfTransport ORDER BY SortOrder")

                    moModeOfTransport = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oModeOfTransport As New CModeOfTransport(oDataRow)
                        moModeOfTransport.Add(oModeOfTransport.ID, oModeOfTransport)
                    Next
                End If
                Return moModeOfTransport
            End Get
        End Property

Public ReadOnly Property MatchID() As Hashtable
            Get
                If moMatchID Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT distinct Match_ID from common_databasededupematch where Match_ID Not IN ( Select Match_ID from common_databasededupematch where PercentageMatch Is NULL Or BeingViewed = 1 group by Match_ID having count(*) > 1) order by Match_ID")

                    moMatchID = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oMatchID As New CDatabaseDedupeMatch(oDataRow)
                        moMatchID.Add(oMatchID.ID, oMatchID)
                    Next
                End If
                Return moMatchID
            End Get
        End Property

        Public ReadOnly Property NCSummaryStatsMinistry() As Hashtable
            Get
                If moNCSummaryStatsMinistry Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Church_NCSummaryStatsMinistry")

                    moNCSummaryStatsMinistry = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oNCSummaryStatsMinistry As New CNCSummaryStatsMinistry(oDataRow)
                        moNCSummaryStatsMinistry.Add(oNCSummaryStatsMinistry.ID, oNCSummaryStatsMinistry)
                    Next
                End If
                Return moNCSummaryStatsMinistry
            End Get
        End Property

        Public ReadOnly Property NCDecisionType() As Hashtable
            Get
                If moNCDecisionType Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Church_NCDecisionType")

                    moNCDecisionType = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oNCDecisionType As New CNCDecisionType(oDataRow)
                        moNCDecisionType.Add(oNCDecisionType.ID, oNCDecisionType)
                    Next
                End If
                Return moNCDecisionType
            End Get
        End Property

        Public ReadOnly Property NPNCInitialStatus() As Hashtable
            Get
                If moNPNCInitialStatus Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Church_NPNCInitialStatus")

                    moNPNCInitialStatus = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oNPNCInitialStatus As New CNPNCInitialStatus(oDataRow)
                        moNPNCInitialStatus.Add(oNPNCInitialStatus.ID, oNPNCInitialStatus)
                    Next
                End If
                Return moNPNCInitialStatus
            End Get
        End Property
        Public ReadOnly Property NPNCInitialStatusByNPNCType(ByVal oNPNCType As CNPNCType) As Hashtable
            Get
                Dim oHashTable As New Hashtable
                Dim oNPNCInitialStatus As CNPNCInitialStatus
                For Each oNPNCInitialStatus In NPNCInitialStatus.Values
                    If oNPNCInitialStatus.NPNCTypeID = oNPNCType.ID Then
                        oHashTable.Add(oNPNCInitialStatus.ID, oNPNCInitialStatus)
                    End If
                Next
                Return oHashTable
            End Get
        End Property

        Public ReadOnly Property NPNCOutcomeInactive() As Hashtable
            Get
                If moNPNCOutcomeInactive Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Church_NPNCOutcomeInactive")

                    moNPNCOutcomeInactive = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oNPNCOutcomeInactive As New CNPNCOutcomeInactive(oDataRow)
                        moNPNCOutcomeInactive.Add(oNPNCOutcomeInactive.ID, oNPNCOutcomeInactive)
                    Next
                End If
                Return moNPNCOutcomeInactive
            End Get
        End Property
        Public ReadOnly Property NPNCType() As Hashtable
            Get
                If moNPNCType Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Church_NPNCType")

                    moNPNCType = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oNPNCType As New CNPNCType(oDataRow)
                        moNPNCType.Add(oNPNCType.ID, oNPNCType)
                    Next
                End If
                Return moNPNCType
            End Get
        End Property

        Public ReadOnly Property PCRDate() As Hashtable
            Get
                If moPCRDate Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Church_PCRDate")

                    moPCRDate = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oPCRDate As New CPCRDate(oDataRow)
                        moPCRDate.Add(oPCRDate.ID, oPCRDate)
                    Next
                End If
                Return moPCRDate
            End Get
        End Property

        Public ReadOnly Property PCR() As Hashtable
            Get
                If moPCR Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Church_PCR")

                    moPCR = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oPCR As New CPCR(oDataRow)
                        moPCR.Add(oPCR.ID, oPCR)
                    Next
                End If
                Return moPCRDate
            End Get
        End Property

        Public ReadOnly Property PCRDateCurrent() As Hashtable
            Get
                Dim oHashTable As New Hashtable
                Dim oPCRDate As CPCRDate
                For Each oPCRDate In PCRDate.Values
                    If Not oPCRDate.Deleted Then
                        oHashTable.Add(oPCRDate.ID, oPCRDate)
                    End If
                Next
                Return oHashTable
            End Get
        End Property
        Public ReadOnly Property Region() As Hashtable
            Get
                If moRegion Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Church_Region WHERE Deleted = 0 ORDER BY SortOrder")

                    moRegion = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oMinistryRegion As New CRegion(oDataRow)
                        moRegion.Add(oMinistryRegion.ID, oMinistryRegion)
                    Next
                End If
                Return moRegion
            End Get
        End Property
        Public ReadOnly Property RegionByCampusAndMinistry(ByVal oCampus As CCampus, ByVal oMinistry As CMinistry) As Hashtable
            Get
                Dim oHashTable As New Hashtable
                Dim oCampusMinistryRegion As CCampusMinistryRegion
                If oCampus IsNot Nothing And oMinistry IsNot Nothing Then
                    For Each oCampusMinistryRegion In CampusMinistryRegion.Values
                        If oCampusMinistryRegion.CampusID = oCampus.ID And _
                           oCampusMinistryRegion.MinistryID = oMinistry.ID And _
                            oCampusMinistryRegion.Region IsNot Nothing Then
                            oHashTable.Add(oCampusMinistryRegion.RegionID, oCampusMinistryRegion.Region)
                        End If
                    Next
                End If
                Return oHashTable
            End Get
        End Property

        Public ReadOnly Property RegionalPastor() As Hashtable
            Get
                If moRegionalPastor Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Church_RegionalPastor")

                    moRegionalPastor = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oRegionalPastor As New CRegionalPastor(oDataRow)
                        moRegionalPastor.Add(oRegionalPastor.ID, oRegionalPastor)
                    Next
                End If
                Return moRegionalPastor

            End Get
        End Property

        Public ReadOnly Property Role() As Hashtable
            Get
                If moRole Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Church_Role ORDER BY Name")

                    moRole = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oRole As New CRole(oDataRow)
                        moRole.Add(oRole.ID, oRole)
                    Next
                End If
                Return moRole
            End Get
        End Property
        'RoleName
        Public ReadOnly Property WWCCRole(ByVal RoleCategoryID As Integer) As Hashtable
            Get
                Dim strSQL As String

                If RoleCategoryID > 0 Then
                    strSQL = "SELECT * FROM Church_Role cr join Church_RoleCategoryRole rcr on cr.Role_ID = rcr.Role_ID where cr.WWCCReq=1 and and Deleted=0 and rcr.RoleCategory_ID = " + RoleCategoryID.ToString() + "order by cr.Name"
                Else
                    strSQL = "SELECT * FROM Church_Role where WWCCReq=1 and Deleted=0 order by Name"
                End If

                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable(strSQL)

                    moRole = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oRole As New CRole(oDataRow)
                        moRole.Add(oRole.ID, oRole)
                    Next

                Return moRole
            End Get
        End Property
          Public ReadOnly Property PaidCourses() As Hashtable
            Get
                ' If moPaidCourses Is Nothing Then               
                Dim oDataRow As DataRow
                Dim oDataTable As DataTable = DataAccess.GetDataTable("select * from church_course where course_id in (Select course_id from church_courseinstance where cost > 0 And Deleted = 0) And Deleted = 0 And course_id In (Select Course_ID from Church_courseinstancecampus)")
                moPaidCourses = New Hashtable
                For Each oDataRow In oDataTable.Rows
                    Dim oCoursePaid As New CCourse(oDataRow)
                    moPaidCourses.Add(oCoursePaid.ID, oCoursePaid)
                Next
                '  End If
                Return moPaidCourses
            End Get
        End Property
        Public ReadOnly Property CovenantRole() As Hashtable
            Get
                Dim strSQL As String

                'strSQL = "SELECT * FROM Church_Role cr join Church_RoleCategoryRole rcr on cr.Role_ID = rcr.Role_ID where cr.CovenantReq=1 and and Deleted=0 and rcr.RoleCategory_ID = " + RoleCategoryID.ToString() + "order by cr.Name"
                strSQL = "SELECT * FROM Church_Role where CovenantReq=1 and Deleted=0 order by Name"

                Dim oDataRow As DataRow
                Dim oDataTable As DataTable = DataAccess.GetDataTable(strSQL)

                moRole = New Hashtable
                For Each oDataRow In oDataTable.Rows
                    Dim oRole As New CRole(oDataRow)
                    moRole.Add(oRole.ID, oRole)
                Next

                Return moRole
            End Get
        End Property

        Public ReadOnly Property RoleCategory() As Hashtable
            Get
                If moCategory Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT RoleCategory_ID,Name FROM church_RoleCategory order by Name")

                    moCategory = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oRole As New CRoleCategory(oDataRow)
                        moCategory.Add(oRole.ID, oRole)
                    Next
                End If
                Return moCategory
            End Get
        End Property

        Public ReadOnly Property RoleCurrent() As Hashtable
            Get
                Dim oHashTable As New Hashtable
                Dim oRole As CRole
                For Each oRole In Role.Values
                    If Not oRole.Deleted Then
                        oHashTable.Add(oRole.ID, oRole)
                    End If
                Next
                Return oHashTable
            End Get
        End Property

        Public ReadOnly Property RoleType() As Hashtable
            Get
                If moRoleType Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Church_RoleType ORDER BY SortOrder")

                    moRoleType = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oRoleType As New CRoleType(oDataRow)
                        moRoleType.Add(oRoleType.ID, oRoleType)
                    Next
                End If
                Return moRoleType
            End Get
        End Property

        Public ReadOnly Property RoleTypeUL() As Hashtable
            Get
                If moRoleTypeUL Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Church_RoleTypeUL ORDER BY SortOrder")

                    moRoleTypeUL = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oRoleTypeUL As New CRoleTypeUL(oDataRow)
                        moRoleTypeUL.Add(oRoleTypeUL.ID, oRoleTypeUL)
                    Next
                End If
                Return moRoleTypeUL
            End Get
        End Property

        Public ReadOnly Property Room() As Hashtable
            Get
                If moRoom Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Church_Room ORDER BY SortOrder")

                    moRoom = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oRoom As New CRoom(oDataRow)
                        moRoom.Add(oRoom.ID, oRoom)
                    Next

                End If
                Return moRoom
            End Get
        End Property

        Public ReadOnly Property RoomCurrent() As Hashtable
            Get
                Dim oHashTable As New Hashtable
                Dim oRoom As CRoom
                For Each oRoom In Room.Values
                    If Not oRoom.Deleted Then
                        oHashTable.Add(oRoom.ID, oRoom)
                    End If
                Next
                Return oHashTable
            End Get
        End Property

        Public ReadOnly Property ChurchService() As Hashtable
            Get
                If moChurchService Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Church_ChurchService ORDER BY SortOrder")

                    moChurchService = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oChurchService As New CChurchService(oDataRow)
                        moChurchService.Add(oChurchService.ID, oChurchService)
                    Next
                End If
                Return moChurchService
            End Get
        End Property

        Public ReadOnly Property ChurchServiceByCampus(ByVal oCampus As CCampus) As ArrayList
            Get
                Dim oChurchService As CChurchService
                Dim oArrayList As New ArrayList
                If oCampus IsNot Nothing Then
                    For Each oChurchService In ChurchService().Values
                        If oChurchService.CampusID = oCampus.ID Then
                            oArrayList.Add(oChurchService)
                        End If
                    Next
                End If
                Return oArrayList
            End Get
        End Property
        Public ReadOnly Property ULG() As Hashtable
            Get
                If moULG Is Nothing OrElse _
                    DateAdd(DateInterval.Minute, 10, gdULGLastRefresh) < GetDateTime Then

                    Dim oTableMappings As New ArrayList
                    oTableMappings.Add(New System.Data.Common.DataTableMapping("Table", "ULG"))
                    oTableMappings.Add(New System.Data.Common.DataTableMapping("Table1", "ULGContact"))

                    Dim oDataRow As DataRow
                    Dim oDataSet As DataSet = DataAccess.GetDataSet("sp_Church_ULGSelectAll", oTableMappings)

                    oDataSet.Relations.Add("ULGContact", oDataSet.Tables("ULG").Columns("ULG_ID"), oDataSet.Tables("ULGContact").Columns("ULG_ID"))

                    moULG = New Hashtable
                    For Each oDataRow In oDataSet.Tables(0).Rows
                        Dim oULG As New CULG(oDataRow)
                        moULG.Add(oULG.ID, oULG)
                    Next

                    gdULGLastRefresh = GetDateTime

                End If
                Return moULG
            End Get
        End Property
        Public ReadOnly Property PKRoom() As Hashtable
            Get
                If moPKRoom Is Nothing OrElse _
                    DateAdd(DateInterval.Minute, 10, gdULGLastRefresh) < GetDateTime Then

                    Dim oTableMappings As New ArrayList
                    oTableMappings.Add(New System.Data.Common.DataTableMapping("Table", "ULG"))
                    oTableMappings.Add(New System.Data.Common.DataTableMapping("Table1", "ULGContact"))

                    Dim oDataRow As DataRow
                    Dim oDataSet As DataSet = DataAccess.GetDataSet("sp_Church_ULGSelectAll", oTableMappings)

                    oDataSet.Relations.Add("ULGContact", oDataSet.Tables("ULG").Columns("ULG_ID"), oDataSet.Tables("ULGContact").Columns("ULG_ID"))

                    moPKRoom = New Hashtable
                    For Each oDataRow In oDataSet.Tables(0).Rows
                        Dim oPKRoom As New CULG(oDataRow)
                        If (oPKRoom.ID = 114 _
                           Or oPKRoom.ID = 115 _
                           Or oPKRoom.ID = 116 _
                           Or oPKRoom.ID = 117 _
                           Or oPKRoom.ID = 118 _
                           Or oPKRoom.ID = 119 _
                           Or oPKRoom.ID = 120 _
                           Or oPKRoom.ID = 121 _
                           Or oPKRoom.ID = 156 _
                           Or oPKRoom.ID = 164 _
                           Or oPKRoom.ID = 165 _
                           Or oPKRoom.ID = 166 _
                           Or oPKRoom.ID = 199 _
                           Or oPKRoom.ID = 200 _
                           Or oPKRoom.ID = 201 _
                           Or oPKRoom.ID = 202 _
                           Or oPKRoom.ID = 225 _
                           Or oPKRoom.ID = 338 _
                           Or oPKRoom.ID = 364 _
                           Or oPKRoom.ID = 366 _
                           Or oPKRoom.ID = 367 _
                           Or oPKRoom.ID = 374 _
                           ) Then
                            moPKRoom.Add(oPKRoom.ID, oPKRoom)
                        End If
                    Next

                    gdULGLastRefresh = GetDateTime

                End If
                Return moPKRoom
            End Get
        End Property
        Public ReadOnly Property PKRoomSA() As Hashtable
            Get
                If moPKRoom Is Nothing OrElse _
                    DateAdd(DateInterval.Minute, 10, gdULGLastRefresh) < GetDateTime Then

                    Dim oTableMappings As New ArrayList
                    oTableMappings.Add(New System.Data.Common.DataTableMapping("Table", "ULG"))
                    oTableMappings.Add(New System.Data.Common.DataTableMapping("Table1", "ULGContact"))

                    Dim oDataRow As DataRow
                    Dim oDataSet As DataSet = DataAccess.GetDataSet("sp_Church_ULGSelectAll", oTableMappings)

                    oDataSet.Relations.Add("ULGContact", oDataSet.Tables("ULG").Columns("ULG_ID"), oDataSet.Tables("ULGContact").Columns("ULG_ID"))

                    moPKRoom = New Hashtable
                    For Each oDataRow In oDataSet.Tables(0).Rows
                        Dim oPKRoom As New CULG(oDataRow)
                        If (oPKRoom.ID = 5 _
                           Or oPKRoom.ID = 30 _
                           Or oPKRoom.ID = 31 _
                           Or oPKRoom.ID = 32 _
                           Or oPKRoom.ID = 46 _
                           Or oPKRoom.ID = 47 _
                           Or oPKRoom.ID = 48 _
                           Or oPKRoom.ID = 49 _
                           Or oPKRoom.ID = 50 _
                           Or oPKRoom.ID = 51 _
                           ) Then
                            moPKRoom.Add(oPKRoom.ID, oPKRoom)
                        End If
                    Next

                    gdULGLastRefresh = GetDateTime

                End If
                Return moPKRoom
            End Get
        End Property

        Public ReadOnly Property ULGCurrentBDay(Optional ByVal oCampus As CCampus = Nothing, Optional ByVal oMinistry As CMinistry = Nothing,
                                                Optional ByVal oRegion As Int32 = 0) As Hashtable
            Get
                Dim oHashTable As New Hashtable
                Dim oULG As CULG
                For Each oULG In ULG.Values
                    If Not oULG.Inactive Then
                        If (oCampus Is Nothing OrElse oULG.CampusID = oCampus.ID) And
                           (oMinistry Is Nothing OrElse oULG.MinistryID = oMinistry.ID) And
                           (oRegion = 0 OrElse oULG.RegionID = oRegion) Then
                            oHashTable.Add(oULG.ID, oULG)
                        End If
                    End If
                Next
                Return oHashTable
            End Get
        End Property

        Public ReadOnly Property ULGCurrent(Optional ByVal oCampus As CCampus = Nothing, Optional ByVal oMinistry As CMinistry = Nothing,
                                            Optional ByVal oRegion As CRegion = Nothing) As Hashtable
            Get
                Dim oHashTable As New Hashtable
                Dim oULG As CULG
                For Each oULG In ULG.Values
                    If Not oULG.Inactive Then
                        If (oCampus Is Nothing OrElse oULG.CampusID = oCampus.ID) And
                           (oMinistry Is Nothing OrElse oULG.MinistryID = oMinistry.ID) And
                           (oRegion Is Nothing OrElse oULG.RegionID = oRegion.ID) Then
                            oHashTable.Add(oULG.ID, oULG)
                        End If
                    End If
                Next
                Return oHashTable
            End Get
        End Property

        Public ReadOnly Property ULGDateDay() As Hashtable
            Get
                If moULGDateDay Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Church_ULGDateDay ORDER BY SortOrder")

                    moULGDateDay = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oULGDateDay As New CULGDateDay(oDataRow)
                        moULGDateDay.Add(oULGDateDay.ID, oULGDateDay)
                    Next
                End If
                Return moULGDateDay
            End Get
        End Property

        Public ReadOnly Property ULGDateTime() As Hashtable
            Get
                If moULGDateTime Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Church_ULGDateTime ORDER BY SortOrder")

                    moULGDateTime = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oULGDateTime As New CULGDateTime(oDataRow)
                        moULGDateTime.Add(oULGDateTime.ID, oULGDateTime)
                    Next
                End If
                Return moULGDateTime
            End Get
        End Property
        '======================== Another language ==============================================
        Public ReadOnly Property AnotherLanguage() As Hashtable
            Get
                If moAnotherLanguage Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Common_AnotherLanguage ORDER BY Language")

                    moAnotherLanguage = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oAnotherLanguage As New CAnotherLanguage(oDataRow)
                        moAnotherLanguage.Add(oAnotherLanguage.ID, oAnotherLanguage)
                    Next
                End If
                Return moAnotherLanguage
            End Get
        End Property
        '========================================================================================
        '======================== Nationality ===================================================
        Public ReadOnly Property Nationality() As Hashtable
            Get
                If moNationality Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Common_Nationality ORDER BY Nationality")

                    moNationality = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oNationality As New CNationality(oDataRow)
                        moNationality.Add(oNationality.ID, oNationality)
                    Next
                End If
                Return moNationality
            End Get
        End Property
        '========================================================================================
        '╔══════════════════════ Volunteer START ═══════════════════════════════════════════════╗
        Public ReadOnly Property VolunteerArea() As Hashtable
            Get
                If moVolunteerArea Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Volunteer_Area ORDER BY Name")

                    moVolunteerArea = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oVolunteerArea As New CVolunteerArea(oDataRow)
                        moVolunteerArea.Add(oVolunteerArea.ID, oVolunteerArea)
                    Next
                End If
                Return moVolunteerArea
            End Get
        End Property
        Public ReadOnly Property VolunteerApplication() As Hashtable
            Get
                If moVolunteerApplication Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Volunteer_Application")

                    moVolunteerApplication = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oVolunteerApplication As New CVolunteerApplication(oDataRow)
                        moVolunteerApplication.Add(oVolunteerApplication.ID, oVolunteerApplication)
                    Next
                End If
                Return moVolunteerApplication
            End Get
        End Property
        Public ReadOnly Property VolunteerApproval() As Hashtable
            Get
                If moVolunteerApproval Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Volunteer_ApplicationApproval")

                    moVolunteerApproval = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oVolunteerApproval As New CVolunteerApplicationApproval(oDataRow)
                        moVolunteerApproval.Add(oVolunteerApproval.ID, oVolunteerApproval)
                    Next
                End If
                Return moVolunteerApproval
            End Get
        End Property
        Public ReadOnly Property VolunteerApprovalType() As Hashtable
            Get
                If moVolunteerApprovalType Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Volunteer_ApplicationApprovalType")

                    moVolunteerApprovalType = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oVolunteerApprovalType As New CVolunteerApplicationApprovalType(oDataRow)
                        moVolunteerApprovalType.Add(oVolunteerApprovalType.ID, oVolunteerApprovalType)
                    Next
                End If
                Return moVolunteerApprovalType
            End Get
        End Property
        Public ReadOnly Property VolunteerApprover() As Hashtable
            Get
                If moVolunteerApprover Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Volunteer_ApplicationApprover")

                    moVolunteerApprover = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oVolunteerApprover As New CVolunteerApplicationApprover(oDataRow)
                        moVolunteerApprover.Add(oVolunteerApprover.ID, oVolunteerApprover)
                    Next
                End If
                Return moVolunteerApprover
            End Get
        End Property
        Public ReadOnly Property VolunteerAnswer() As Hashtable
            Get
                If moVolunteerAnswer Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Volunteer_ApplicationAnswer")

                    moVolunteerAnswer = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oVolunteerAnswer As New CVolunteerApplicationAnswer(oDataRow)
                        moVolunteerAnswer.Add(oVolunteerAnswer.ID, oVolunteerAnswer)
                    Next
                End If
                Return moVolunteerAnswer
            End Get
        End Property
        Public ReadOnly Property VolunteerQuestion() As Hashtable
            Get
                If moVolunteerQuestion Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Volunteer_ApplicationQuestion")

                    moVolunteerQuestion = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oVolunteerQuestion As New CVolunteerApplicationQuestion(oDataRow)
                        moVolunteerQuestion.Add(oVolunteerQuestion.ID, oVolunteerQuestion)
                    Next
                End If
                Return moVolunteerQuestion
            End Get
        End Property
        '╚══════════════════════ Volunteer END ═════════════════════════════════════════════════╝
        Public Function Get247PrayerContactMethodByID(ByVal iID As Integer) As C247PrayerContactMethod
            Return Prayer247ContactMethod(iID)
        End Function
        Public Function Get247PrayerDayByID(ByVal iID As Integer) As C247PrayerDay
            Return Prayer247Day(iID)
        End Function
        Public Function Get247PrayerDurationByID(ByVal iID As Integer) As C247PrayerDuration
            Return Prayer247Duration(iID)
        End Function
        Public Function Get247PrayerTimeByID(ByVal iID As Integer) As C247PrayerTime
            Return Prayer247Time(iID)
        End Function
        Public Function GetCampusByID(ByVal iID As Integer) As CCampus
            Return Campus(iID)
        End Function
        Public Function GetCarParkUsedByID(ByVal iID As Integer) As CCarParkUsed
            Return CarParkUsed(iID)
        End Function
        Public Function GetChurchStatusByID(ByVal iID As Integer) As CChurchStatus
            Return ChurchStatus(iID)
        End Function
        Public Function GetCourseByID(ByVal iID As Integer) As CCourse
            Return Course(iID)
        End Function
        Public Function GetCourseInstanceByID(ByVal iID As Integer) As CCourseInstance
            Return CourseInstance(iID)
        End Function
        Public Function GetCourseTopicByID(ByVal iID As Integer) As CCourseTopic
            Return CourseTopic(iID)
        End Function
        Public Function GetCourseSessionByID(ByVal iID As Integer) As CCourseSession
            Return CourseSession(iID)
        End Function
        Public Function GetDeleteReasonByID(ByVal iID As Integer) As CDeleteReason
            Return DeleteReason(iID)
        End Function
        Public Function GetKidsMinistryCodeByID(ByVal iID As Integer) As CKidsMinistryCode
            Return KidsMinistryCode(iID)
        End Function
        Public Function GetKidsMinistryLabelPrinterByID(ByVal iID As Integer) As CKidsMinistryLabelPrinter
            Return KidsMinistryLabelPrinter(iID)
        End Function
        Public Function GetMinistryByID(ByVal iID As Integer) As CMinistry
            Return Ministry(iID)
        End Function
        Public Function GetEducationByID(ByVal iID As Integer) As CEducation
            Return Education(iID)
        End Function
        Public Function GetEmploymentIndustryByID(ByVal iID As Integer) As CEmploymentIndustry
            Return EmploymentIndustry(iID)
        End Function
        Public Function GetEmploymentPositionByID(ByVal iID As Integer) As CEmploymentPosition
            Return EmploymentPosition(iID)
        End Function
        Public Function GetEmploymentStatusByID(ByVal iID As Integer) As CEmploymentStatus
            Return EmploymentStatus(iID)
        End Function
        Public Function GetEntryPointByID(ByVal iID As Integer) As CEntryPoint
            Return EntryPoint(iID)
        End Function
        Public Function GetNCSummaryStatsMinistryByID(ByVal iID As Integer) As CNCSummaryStatsMinistry
            Return NCSummaryStatsMinistry(iID)
        End Function
        Public Function GetNCDecisionTypeByID(ByVal iID As Integer) As CNCDecisionType
            Return NCDecisionType(iID)
        End Function
        Public Function GetNPNCInitialStatusByID(ByVal iID As Integer) As CNPNCInitialStatus
            Return NPNCInitialStatus(iID)
        End Function
        Public Function GetNPNCOutcomeInactiveByID(ByVal iID As Integer) As CNPNCOutcomeInactive
            Return NPNCOutcomeInactive(iID)
        End Function
        Public Function GetNPNCTypeByID(ByVal iID As Integer) As CNPNCType
            Return NPNCType(iID)
        End Function
        Public Function GetModeOfTransportByID(ByVal iID As Integer) As CModeOfTransport
            Return ModeOfTransport(iID)
        End Function
        Public Function GetCommentSourceByID(ByVal iID As Integer) As CCommentSource
            Return CommentSource()(iID)
        End Function
        Public Function GetCommentTypeByID(ByVal iID As Integer) As CCommentType
            Return CommentType(iID)
        End Function
        Public Function GetPCRDateByID(ByVal iID As Integer) As CPCRDate
            Return PCRDate(iID)
        End Function
        Public Function GetRegionByID(ByVal iID As Integer) As CRegion
            Return Region(iID)
        End Function
        Public Function GetRegionalPastorByID(ByVal iID As Integer) As CRegionalPastor
            Return RegionalPastor(iID)
        End Function
        Public Function GetRoleByID(ByVal iID As Integer) As CRole
            Return Role(iID)
        End Function

        Public Function GetRoleCategoryByID(ByVal iID As Integer) As CRoleCategory
            Return RoleCategory(iID)
        End Function

        Public Function GetRoleTypeByID(ByVal iID As Integer) As CRoleType
            Return RoleType(iID)
        End Function
        Public Function GetRoleTypeULByID(ByVal iID As Integer) As CRoleTypeUL
            Return RoleTypeUL(iID)
        End Function
        Public Function GetRoomByID(ByVal iID As Integer) As CRoom
            Return Room(iID)
        End Function
        Public Function GetChurchServiceByID(ByVal iID As Integer) As CChurchService
            Return ChurchService(iID)
        End Function
        Public Function GetULGByID(ByVal iID As Integer) As CULG
            Return ULG(iID)
        End Function
        Public Function GetULGDateDayByID(ByVal iID As Integer) As CULGDateDay
            Return ULGDateDay(iID)
        End Function
        Public Function GetULGDateTimeByID(ByVal iID As Integer) As CULGDateTime
            Return ULGDateTime(iID)
        End Function
        '╔══════════════════════ Volunteer START ═══════════════════════════════════════════════╗
        Public Function GetVolunteerAreaByID(ByVal iID As Integer) As CVolunteerArea
            Return VolunteerArea(iID)
        End Function

        Public Function GetVolunteerApplicationByID(ByVal iID As Integer) As CVolunteerApplication
            Return VolunteerApplication(iID)
        End Function

        Public Function GetVolunteerApprovalByID(ByVal iID As Integer) As CVolunteerApplicationApproval
            Return VolunteerApproval(iID)
        End Function

        Public Function GetVolunteerApprovalTypeByID(ByVal iID As Integer) As CVolunteerApplicationApprovalType
            Return VolunteerApprovalType(iID)
        End Function

        Public Function GetVolunteerApproverByID(ByVal iID As Integer) As CVolunteerApplicationApprover
            Return VolunteerApprover(iID)
        End Function

        Public Function GetVolunteerAnswerByID(ByVal iID As Integer) As CVolunteerApplicationAnswer
            Return VolunteerAnswer(iID)
        End Function

        Public Function GetVolunteerQuestionByID(ByVal iID As Integer) As CVolunteerApplicationQuestion
            Return VolunteerQuestion(iID)
        End Function
        '╚══════════════════════ Volunteer END ═════════════════════════════════════════════════╝
    End Class

End Namespace