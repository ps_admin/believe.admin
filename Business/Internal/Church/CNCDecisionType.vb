Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CNCDecisionType

        Private miNCDecisionTypeID As Integer
        Private msName As String
        Private mbShowInNPNC As Boolean
        Private miSortOrder As Integer

        Public Sub New()

            miNCDecisionTypeID = 0
            msName = String.Empty
            mbShowInNPNC = False
            miSortOrder = 0

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miNCDecisionTypeID = oDataRow("NCDecisionType_ID")
            msName = oDataRow("Name")
            mbShowInNPNC = oDataRow("ShowInNPNC")
            miSortOrder = oDataRow("SortOrder")

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miNCDecisionTypeID
            End Get
        End Property
        Public Property Name() As String
            Get
                Return msName
            End Get
            Set(ByVal value As String)
                msName = value
            End Set
        End Property
        Public Property ShowInNPNC() As Boolean
            Get
                Return mbShowInNPNC
            End Get
            Set(ByVal value As Boolean)
                mbShowInNPNC = value
            End Set
        End Property
        Public Property SortOrder() As Integer
            Get
                Return miSortOrder
            End Get
            Set(ByVal value As Integer)
                miSortOrder = value
            End Set
        End Property

    End Class

End Namespace