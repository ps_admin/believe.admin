Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CNPNC

        Private miNPNCID As Integer
        Private miContactID As Integer
        Private miNPNCTypeID As Integer
        Private mdFirstContactDate As Date
        Private mdFirstFollowUpCallDate As Date
        Private mdFirstFollowUpVisitDate As Date
        Private miInitialStatusID As Integer
        Private miNCDecisionTypeID As Integer
        Private miNCTeamMemberID As Integer
        Private miCarerID As Integer
        Private moCarer As CContact
        Private mbSendNewCarerEmail As Boolean
        Private miCampusDecisionID As Integer
        Private miMinistryID As Integer
        Private miRegionID As Integer
        Private miULGID As Integer
        Private mdULGDateOfIssue As Date
        Private miOutcomeStatusID As Integer
        Private mdOutcomeDate As Date
        Private miOutcomeInactiveID As Integer
        Private msGUID As String
        Private msCarerNames As String

        Private mbChanged As Boolean
        Private mbOutcomed As Boolean           'Used to signify if this NP/NC record has just been given an outcome. Used as a trigger to update the Campus/Ministry/Region

        Public Sub New()

            miNPNCID = 0
            miContactID = 0
            miNPNCTypeID = 0
            mdFirstContactDate = Nothing
            mdFirstFollowUpCallDate = Nothing
            mdFirstFollowUpVisitDate = Nothing
            miInitialStatusID = 0
            miNCDecisionTypeID = 0
            miNCTeamMemberID = 0
            miCarerID = 0
            moCarer = Nothing
            mbSendNewCarerEmail = False
            miCampusDecisionID = 0
            miMinistryID = 0
            miRegionID = 0
            miULGID = 0
            mdULGDateOfIssue = Nothing
            miOutcomeStatusID = 0
            mdOutcomeDate = Nothing
            miOutcomeInactiveID = 0
            msGUID = System.Guid.NewGuid().ToString
            msCarerNames = ""

            mbChanged = False
            mbOutcomed = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miNPNCID = oDataRow("NPNC_ID")
            miContactID = oDataRow("Contact_ID")
            miNPNCTypeID = oDataRow("NPNCType_ID")
            mdFirstContactDate = oDataRow("FirstContactDate")
            mdFirstFollowUpCallDate = IsNull(oDataRow("FirstFollowUpCallDate"), Nothing)
            mdFirstFollowUpVisitDate = IsNull(oDataRow("FirstFollowUpVisitDate"), Nothing)
            miInitialStatusID = oDataRow("InitialStatus_ID")
            miNCDecisionTypeID = IsNull(oDataRow("NCDecisionType_ID"), 0)
            miNCTeamMemberID = IsNull(oDataRow("NCTeamMember_ID"), 0)
            miCarerID = IsNull(oDataRow("Carer_ID"), 0)
            miCampusDecisionID = IsNull(oDataRow("CampusDecision_ID"), 0)
            miULGID = IsNull(oDataRow("ULG_ID"), 0)
            mdULGDateOfIssue = IsNull(oDataRow("ULGDateOfIssue"), Nothing)
            miOutcomeStatusID = IsNull(oDataRow("OutcomeStatus_ID"), 0)
            mdOutcomeDate = IsNull(oDataRow("OutcomeDate"), Nothing)
            miOutcomeInactiveID = IsNull(oDataRow("OutcomeInactive_ID"), 0)
            msGUID = oDataRow("GUID").ToString
            msCarerNames = ""

            mbChanged = False
            mbOutcomed = False

        End Sub

        Public Sub Save(ByVal oContact As CContact, ByVal oUser As CUser)

            If mdOutcomeDate = Nothing And miOutcomeStatusID <> 0 Then
                mdOutcomeDate = GetDateTime
            End If

            If mbChanged Then

                Dim oParameters As New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", miContactID))
                oParameters.Add(New SqlClient.SqlParameter("@NPNCType_ID", SQLObject(NPNCType)))
                oParameters.Add(New SqlClient.SqlParameter("@FirstContactDate", SQLDate(mdFirstContactDate)))
                oParameters.Add(New SqlClient.SqlParameter("@FirstFollowUpCallDate", SQLDate(mdFirstFollowUpCallDate)))
                oParameters.Add(New SqlClient.SqlParameter("@FirstFollowUpVisitDate", SQLDate(mdFirstFollowUpVisitDate)))
                oParameters.Add(New SqlClient.SqlParameter("@InitialStatus_ID", SQLObject(InitialStatus)))
                oParameters.Add(New SqlClient.SqlParameter("@NCDecisionType_ID", SQLObject(NCDecisionType)))
                oParameters.Add(New SqlClient.SqlParameter("@NCTeamMember_ID", SQLObject(NCTeamMember)))
                oParameters.Add(New SqlClient.SqlParameter("@Carer_ID", SQLNull(miCarerID)))
                oParameters.Add(New SqlClient.SqlParameter("@CampusDecision_ID", SQLNull(miCampusDecisionID)))
                oParameters.Add(New SqlClient.SqlParameter("@Ministry_ID", miMinistryID))
                oParameters.Add(New SqlClient.SqlParameter("@Region_ID", SQLNull(miRegionID)))
                oParameters.Add(New SqlClient.SqlParameter("@ULG_ID", SQLObject(ULG)))
                oParameters.Add(New SqlClient.SqlParameter("@ULGDateOfIssue", SQLDate(mdULGDateOfIssue)))
                oParameters.Add(New SqlClient.SqlParameter("@OutcomeStatus_ID", SQLObject(OutcomeStatus)))
                oParameters.Add(New SqlClient.SqlParameter("@OutcomeDate", SQLDateTime(mdOutcomeDate)))
                oParameters.Add(New SqlClient.SqlParameter("@OutcomeInactive_ID", SQLObject(OutcomeInactive)))
                oParameters.Add(New SqlClient.SqlParameter("@GUID", msGUID))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))

                If Me.ID = 0 Then
                    miNPNCID = DataAccess.GetValue("sp_Church_NPNCInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@NPNC_ID", ID))
                    DataAccess.ExecuteCommand("sp_Church_NPNCUpdate", oParameters)
                End If

                'If mbSendNewCarerEmail Then
                '    If Me.CarerID > 0 Then
                '        Business.Internal.Church.SendEmailCarerNewAssignment(oContact, Me)
                '    End If
                '    mbSendNewCarerEmail = False

                '    If Me.ULG IsNot Nothing Then
                '        Dim oCoach As CContact
                '        For Each oCoach In Me.ULG.GetCoachList
                '            Business.Internal.Church.SendEmailULCoachCarerNewAssignment(oContact, Me, oCoach)
                '        Next

                '        If Me.ULG.GetRegionalPastors.Count > 0 Then
                '            Business.Internal.Church.SendEmailRegionalPastorNewAssignmentConfirmation( _
                '                    Me.ULG.GetRegionalPastors, _
                '                    Me, _
                '                    oContact, _
                '                    Me.ULG.GetCoachList, _
                '                    Me.Carer)
                '        End If

                '    End If
                'End If

                mbChanged = False
                mbOutcomed = False

            End If

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miNPNCID
            End Get
        End Property
        Public Property ContactID() As Integer
            Get
                Return miContactID
            End Get
            Set(ByVal value As Integer)
                If miContactID <> value Then mbChanged = True
                miContactID = value
            End Set
        End Property
        Public Property NPNCTypeID() As Integer
            Get
                Return miNPNCTypeID
            End Get
            Set(ByVal value As Integer)
                If miNPNCTypeID <> value Then mbChanged = True
                miNPNCTypeID = value
            End Set
        End Property
        Public ReadOnly Property NPNCType() As CNPNCType
            Get
                Return Lists.Internal.Church.GetNPNCTypeByID(miNPNCTypeID)
            End Get
        End Property
        Public Property FirstContactDate() As Date
            Get
                Return mdFirstContactDate
            End Get
            Set(ByVal value As Date)
                If mdFirstContactDate <> value Then mbChanged = True
                mdFirstContactDate = FormatDate(value)
            End Set
        End Property
        Public Property FirstFollowupCallDate() As Date
            Get
                Return mdFirstFollowUpCallDate
            End Get
            Set(ByVal value As Date)
                If mdFirstFollowUpCallDate <> value Then mbChanged = True
                mdFirstFollowUpCallDate = FormatDate(value)
            End Set
        End Property
        Public Property FirstFollowupVisitDate() As Date
            Get
                Return mdFirstFollowUpVisitDate
            End Get
            Set(ByVal value As Date)
                If mdFirstFollowUpVisitDate <> value Then mbChanged = True
                mdFirstFollowUpVisitDate = FormatDate(value)
            End Set
        End Property
        Public Property InitialStatusID() As Integer
            Get
                Return miInitialStatusID
            End Get
            Set(ByVal value As Integer)
                If miInitialStatusID <> value Then mbChanged = True
                miInitialStatusID = value
            End Set
        End Property
        Public ReadOnly Property InitialStatus() As CNPNCInitialStatus
            Get
                Return Lists.Internal.Church.GetNPNCInitialStatusByID(miInitialStatusID)
            End Get
        End Property
        Public Property NCDecisionTypeID() As Integer
            Get
                Return miNCDecisionTypeID
            End Get
            Set(ByVal value As Integer)
                If miNCDecisionTypeID <> value Then mbChanged = True
                miNCDecisionTypeID = value
            End Set
        End Property
        Public ReadOnly Property NCDecisionType() As CNCDecisionType
            Get
                Return Lists.Internal.Church.GetNCDecisionTypeByID(miNCDecisionTypeID)
            End Get
        End Property
        Public Property NCTeamMemberID() As Integer
            Get
                Return miNCTeamMemberID
            End Get
            Set(ByVal value As Integer)
                If miNCTeamMemberID <> value Then mbChanged = True
                miNCTeamMemberID = value
            End Set
        End Property
        Public ReadOnly Property NCTeamMember() As CUser
            Get
                Return Lists.GetUserByID(miNCTeamMemberID)
            End Get
        End Property
        Public Property CarerID() As Integer
            Get
                Return miCarerID
            End Get
            Set(ByVal value As Integer)
                If miCarerID <> value Then mbChanged = True
                miCarerID = value
            End Set
        End Property
        Public ReadOnly Property Carer() As CContact
            Get
                If miCarerID > 0 And moCarer Is Nothing Or _
                    (moCarer IsNot Nothing AndAlso moCarer.ID <> miCarerID) Then
                    moCarer = New CContact
                    moCarer.LoadByID(miCarerID)
                End If
                Return moCarer
            End Get
        End Property
        Public ReadOnly Property CarerNames() As String
            Get
                If miContactID > 0 Then
                    Dim oParameters As New ArrayList
                    Dim Table As DataTable
                    oParameters.Add(New SqlClient.SqlParameter("@ContactId", miContactID))
                    Table = DataAccess.GetDataTable("sp_PCR_GetCarerByContactId", oParameters)
                    msCarerNames = ""
                    For Each row As DataRow In Table.Rows
                        msCarerNames = msCarerNames + row.Item("FirstName") + " " + row.Item("LastName") + ", "
                    Next row
                    If Len(msCarerNames) > 0 Then
                        Return Left(msCarerNames, Len(msCarerNames) - 2)
                    Else
                        Return Nothing
                    End If

                Else
                    Return Nothing
                End If

            End Get
        End Property
        Public Property SendNewCarerEmail() As Boolean
            Get
                Return mbSendNewCarerEmail
            End Get
            Set(ByVal value As Boolean)
                mbSendNewCarerEmail = value
            End Set
        End Property
        Public Property CampusDecisionID() As Integer
            Get
                Return miCampusDecisionID
            End Get
            Set(ByVal value As Integer)
                If miCampusDecisionID <> value Then mbChanged = True
                miCampusDecisionID = value
            End Set
        End Property
        Public ReadOnly Property CampusDecision() As CCampus
            Get
                Return Lists.Internal.Church.GetCampusByID(miCampusDecisionID)
            End Get
        End Property
        Public Property MinistryID() As Integer
            Get
                Return miMinistryID
            End Get
            Set(ByVal value As Integer)
                If miMinistryID <> value Then mbChanged = True
                miMinistryID = value
            End Set
        End Property
        Public ReadOnly Property Ministry() As CMinistry
            Get
                Return Lists.Internal.Church.GetMinistryByID(miMinistryID)
            End Get
        End Property
        Public Property RegionID() As Integer
            Get
                Return miRegionID
            End Get
            Set(ByVal value As Integer)
                If miRegionID <> value Then mbChanged = True
                miRegionID = value
            End Set
        End Property
        Public ReadOnly Property Region() As CRegion
            Get
                Return Lists.Internal.Church.GetRegionByID(miRegionID)
            End Get
        End Property
        Public Property ULGID() As Integer
            Get
                Return miULGID
            End Get
            Set(ByVal value As Integer)
                If miULGID <> value Then mbChanged = True
                miULGID = value
            End Set
        End Property
        Public ReadOnly Property ULG() As CULG
            Get
                Return Lists.Internal.Church.GetULGByID(miULGID)
            End Get
        End Property
        Public Property ULGDateOfIssue() As Date
            Get
                Return mdULGDateOfIssue
            End Get
            Set(ByVal value As Date)
                If mdULGDateOfIssue <> value Then mbChanged = True
                mdULGDateOfIssue = value
            End Set
        End Property
        Public Property OutcomeStatusID() As Integer
            Get
                Return miOutcomeStatusID
            End Get
            Set(ByVal value As Integer)
                If miOutcomeStatusID = 0 And value <> 0 Then mbOutcomed = True
                If miOutcomeStatusID <> value Then mbChanged = True
                miOutcomeStatusID = value
            End Set
        End Property
        Public ReadOnly Property OutcomeStatus() As CChurchStatus
            Get
                Return Lists.Internal.Church.GetChurchStatusByID(miOutcomeStatusID)
            End Get
        End Property
        Public Property OutcomeDate() As Date
            Get
                Return mdOutcomeDate
            End Get
            Set(ByVal value As Date)
                If mdOutcomeDate <> value Then mbChanged = True
                mdOutcomeDate = value
            End Set
        End Property
        Public Property OutcomeInactiveID() As Integer
            Get
                Return miOutcomeInactiveID
            End Get
            Set(ByVal value As Integer)
                If miOutcomeInactiveID <> value Then mbChanged = True
                miOutcomeInactiveID = value
            End Set
        End Property
        Public ReadOnly Property OutcomeInactive() As CNPNCOutcomeInactive
            Get
                Return Lists.Internal.Church.GetNPNCOutcomeInactiveByID(miOutcomeInactiveID)
            End Get
        End Property
        Public ReadOnly Property Changed() As Boolean
            Get
                Return mbChanged
            End Get
        End Property
        Public ReadOnly Property Outcomed() As Boolean
            Get
                Return (OutcomeStatus IsNot Nothing)
            End Get
        End Property
        Public Sub ResetChanges()
            mbChanged = False
        End Sub
        Public ReadOnly Property GUID() As String
            Get
                Return msGUID
            End Get
        End Property

    End Class

End Namespace