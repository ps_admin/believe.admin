Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CNPSummaryStats

        Private miNPSummaryStatsID As Integer
        Private mdDate As Date
        Private miCampusID As Integer
        Private miInitialStatusID As Integer
        Private miNumber As Integer
        Private miEnteredByID As Integer
        Private mdDateEntered As Date

        Private mbChanged As Boolean

        Public Sub New()

            miNPSummaryStatsID = 0
            mdDate = Nothing
            miCampusID = 0
            miInitialStatusID = 0
            miNumber = 0
            miEnteredByID = 0
            mdDateEntered = Nothing

            mbChanged = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miNPSummaryStatsID = oDataRow("SummaryStats_ID")
            mdDate = oDataRow("Date")
            miCampusID = oDataRow("Campus_ID")
            miInitialStatusID = oDataRow("InitialStatus_ID")
            miNumber = oDataRow("Number")
            miEnteredByID = oDataRow("EnteredBy_ID")
            mdDateEntered = oDataRow("DateEntered")

            mbChanged = False

        End Sub

        Public Sub Save()

            If mbChanged Then

                Dim oParameters As New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@Date", SQLDate(mdDate)))
                oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(Campus)))
                oParameters.Add(New SqlClient.SqlParameter("@InitialStatus_ID", SQLObject(InitialStatus)))
                oParameters.Add(New SqlClient.SqlParameter("@Number", miNumber))
                oParameters.Add(New SqlClient.SqlParameter("@EnteredBy_ID", SQLObject(EnteredBy)))
                oParameters.Add(New SqlClient.SqlParameter("@DateEntered", SQLDateTime(mdDateEntered)))

                If Me.ID = 0 Then
                    miNPSummaryStatsID = DataAccess.GetValue("sp_Church_NPSummaryStatsInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@SummaryStats_ID", miNPSummaryStatsID))
                    DataAccess.ExecuteCommand("sp_Church_NPSummaryStatsIssuedUpdate", oParameters)
                End If

            End If

        End Sub

        Public Sub Delete()

            If miNPSummaryStatsID > 0 Then
                Dim oParameters As New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@SummaryStats_ID", miNPSummaryStatsID))
                DataAccess.ExecuteCommand("sp_Church_NPSummaryStatsDelete", oParameters)
            End If

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miNPSummaryStatsID
            End Get
        End Property
        Public Property [Date]() As Date
            Get
                Return mdDate
            End Get
            Set(ByVal value As Date)
                If mdDate <> value Then mbChanged = True
                mdDate = value
            End Set
        End Property
        Public Property CampusID() As Integer
            Get
                Return miCampusID
            End Get
            Set(ByVal value As Integer)
                If miCampusID <> value Then mbChanged = True
                miCampusID = value
            End Set
        End Property
        Public ReadOnly Property Campus() As CCampus
            Get
                Return Lists.Internal.Church.GetCampusByID(miCampusID)
            End Get
        End Property
        Public Property InitialStatusID() As Integer
            Get
                Return miInitialStatusID
            End Get
            Set(ByVal value As Integer)
                If miInitialStatusID <> value Then mbChanged = True
                miInitialStatusID = value
            End Set
        End Property
        Public ReadOnly Property InitialStatus() As CNPNCInitialStatus
            Get
                Return Lists.Internal.Church.GetNPNCInitialStatusByID(miInitialStatusID)
            End Get
        End Property
        Public Property Number() As Integer
            Get
                Return miNumber
            End Get
            Set(ByVal value As Integer)
                If miNumber <> value Then mbChanged = True
                miNumber = value
            End Set
        End Property
        Public Property EnteredByID() As Integer
            Get
                Return miEnteredByID
            End Get
            Set(ByVal value As Integer)
                If miEnteredByID <> value Then mbChanged = True
                miEnteredByID = value
            End Set
        End Property
        Public ReadOnly Property EnteredBy() As CUser
            Get
                Return Lists.GetUserByID(miEnteredByID)
            End Get
        End Property
        Public Property DateEntered() As Date
            Get
                Return mdDateEntered
            End Get
            Set(ByVal value As Date)
                If mdDateEntered <> value Then mbChanged = True
                mdDateEntered = value
            End Set
        End Property

    End Class

End Namespace