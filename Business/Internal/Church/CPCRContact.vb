Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CPCRContact

        Private miPCRContactID As Integer
        Private miPCRID As Integer
        Private miContactID As Integer
        Private moContact As CContact
        Private msContactFirstName As String             'Cache the Contact Name here so we don't have to load the whole contact
        Private msContactLastName As String              'Cache the Contact Name here so we don't have to load the whole contact
        Private miContactChurchStatusID As Integer       'Cache the Contact's Status here so we don't have to load the whole contact
        Private msContactGUID As String                  'Cache the Contact's GUID here so we don't have to load the whole contact
        Private mbSundayAttendance As Boolean
        Private miServiceAttendedID As Integer
        Private mbULGAttendance As Boolean
        Private mbBoomAttendance As Boolean
        Private mbFNLAttendance As Boolean
        Private miCall As Integer
        Private miVisit As Integer
        Private miOther As Integer
        Private miMonthSundayTotal As Integer
        Private miMonthULGTotal As Integer
        Private miMonthBoomTotal As Integer
        Private miMonthCallTotal As Integer
        Private miMonthVisitTotal As Integer
        Private mdDateJoined As Date
        Private mbNoCarer As Boolean
        Private msGUID As String
        Private miVisitorID As Integer
        Private miVisitedPCRID As Integer
        Private msCarerName As String

        Private mcComment As CArrayList(Of CContactComment)

        Private mbChanged As Boolean

        Public Sub New()

            miPCRContactID = 0
            miPCRID = 0
            miContactID = 0
            moContact = Nothing
            msContactFirstName = String.Empty
            msContactLastName = String.Empty
            miContactChurchStatusID = 0
            msContactGUID = String.Empty
            mbSundayAttendance = False
            miServiceAttendedID = 0
            mbULGAttendance = False
            mbBoomAttendance = False
            mbFNLAttendance = False
            miCall = 0
            miVisit = 0
            mcComment = New CArrayList(Of CContactComment)
            mbChanged = False
            mdDateJoined = Nothing
            mbNoCarer = False
            msGUID = System.Guid.NewGuid.ToString
            miVisitorID = 0
            miVisitedPCRID = 0
            msCarerName = String.Empty

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miPCRContactID = oDataRow("PCRContact_ID")
            miPCRID = oDataRow("PCR_ID")
            miContactID = oDataRow("Contact_ID")
            msContactFirstName = oDataRow("ContactFirstName")
            msContactLastName = oDataRow("ContactLastName")
            miContactChurchStatusID = IsNull(oDataRow("ChurchStatus_ID"), 0)
            msContactGUID = oDataRow("ContactGUID").ToString
            mbSundayAttendance = oDataRow("SundayAttendance")
            miServiceAttendedID = IsNull(oDataRow("ServiceAttended_ID"), 0)
            mbULGAttendance = oDataRow("ULGAttendance")
            mbBoomAttendance = oDataRow("BoomAttendance")
            mbFNLAttendance = oDataRow("FNLAttendance")
            miCall = oDataRow("Call")
            miVisit = oDataRow("Visit")
            miOther = oDataRow("Other")
            miMonthSundayTotal = oDataRow("MonthSundayTotal")
            miMonthULGTotal = oDataRow("MonthULGTotal")
            miMonthBoomTotal = oDataRow("MonthBoomTotal")
            miMonthCallTotal = oDataRow("MonthCallTotal")
            miMonthVisitTotal = oDataRow("MonthVisitTotal")
            mdDateJoined = IsNull(oDataRow("DateJoined"), Nothing)
            mbNoCarer = oDataRow("NoCarer")
            miVisitorID = IsNull(oDataRow("Visitor_ID"), 0)
            miVisitedPCRID = IsNull(oDataRow("VisitedPCR_ID"), 0)
            msCarerName = IsNull(oDataRow("carername"), String.Empty)

            Dim oCommentRow As DataRow
            For Each oCommentRow In oDataRow.GetChildRows("Comment")
                mcComment.Add(New CContactComment(oCommentRow))
            Next

            mbChanged = False

        End Sub

        Public Sub Save(ByVal oUser As CUser)

            If mbChanged Then

                Dim oParameters As New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@PCR_ID", miPCRID))
                oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", miContactID))
                oParameters.Add(New SqlClient.SqlParameter("@SundayAttendance", mbSundayAttendance))
                oParameters.Add(New SqlClient.SqlParameter("@ServiceAttended_ID", SQLNull(miServiceAttendedID)))
                oParameters.Add(New SqlClient.SqlParameter("@ULGAttendance", mbULGAttendance))
                oParameters.Add(New SqlClient.SqlParameter("@BoomAttendance", mbBoomAttendance))
                oParameters.Add(New SqlClient.SqlParameter("@FNLAttendance", mbFNLAttendance))
                oParameters.Add(New SqlClient.SqlParameter("@Call", miCall))
                oParameters.Add(New SqlClient.SqlParameter("@Visit", miVisit))
                oParameters.Add(New SqlClient.SqlParameter("@Other", miOther))

                If miVisitorID > 0 Then oParameters.Add(New SqlClient.SqlParameter("@Visitor_ID", miVisitorID))
                If miVisitedPCRID > 0 Then oParameters.Add(New SqlClient.SqlParameter("@VisitedPCR_ID", miVisitedPCRID))

                If Me.ID = 0 Then
                    miPCRContactID = DataAccess.GetValue("sp_Church_PCRContactInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@PCRContact_ID", miPCRContactID))
                    DataAccess.ExecuteCommand("sp_Church_PCRContactUpdate", oParameters)
                End If

                mbChanged = False

            End If

            'This will save any updates to the NPNC Call/Visit details
            If moContact IsNot Nothing Then
                If moContact.Changed Then
                    moContact.Save(oUser)
                End If
            End If

            SavePCRContactComment(oUser)
            mcComment.ResetChanges()

        End Sub

        Private Sub SavePCRContactComment(ByVal oUser As CUser)

            Dim oParameters As ArrayList
            Dim oComment As CContactComment

            'Remove the Deleted Contact Comments
            For Each oComment In mcComment.Removed
                oParameters = New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@Comment_ID", oComment.ID))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))
                DataAccess.ExecuteCommand("sp_Church_ContactCommentDelete", oParameters)
            Next

            For Each oComment In mcComment
                oComment.Save(ouser)
            Next

        End Sub

        Public ReadOnly Property Comments() As CArrayList(Of CContactComment)
            Get
                Return mcComment
            End Get
        End Property

        Public ReadOnly Property ID() As Integer
            Get
                Return miPCRContactID
            End Get
        End Property
        Public Property PCRID() As Integer
            Get
                Return miPCRID
            End Get
            Set(ByVal value As Integer)
                If miPCRID <> value Then mbChanged = True
                miPCRID = value
            End Set
        End Property
        Public Property ContactID() As Integer
            Get
                Return miContactID
            End Get
            Set(ByVal value As Integer)
                If miContactID <> value Then mbChanged = True
                miContactID = value
            End Set
        End Property
        Public ReadOnly Property Contact() As CContact
            Get
                If moContact Is Nothing Then
                    moContact = New CContact
                    moContact.LoadByID(miContactID)
                End If
                Return moContact
            End Get
        End Property
        Public ReadOnly Property ContactFirstName() As String
            Get
                Return msContactFirstName
            End Get
        End Property
        Public ReadOnly Property ContactLastName() As String
            Get
                Return msContactLastName
            End Get
        End Property
        Public ReadOnly Property ContactChurchStatusID() As Integer
            Get
                Return miContactChurchStatusID
            End Get
        End Property
        Public ReadOnly Property ContactGUID() As String
            Get
                Return msContactGUID
            End Get
        End Property
        Public Property SundayAttendance() As Boolean
            Get
                Return mbSundayAttendance
            End Get
            Set(ByVal value As Boolean)
                If mbSundayAttendance <> value Then mbChanged = True
                mbSundayAttendance = value
            End Set
        End Property
        Public Property ServiceAttendedID() As Integer
            Get
                Return miServiceAttendedID
            End Get
            Set(ByVal value As Integer)
                If miServiceAttendedID <> value Then mbChanged = True
                miServiceAttendedID = value
            End Set
        End Property
        Public ReadOnly Property ServiceAttended() As CChurchService
            Get
                Return Lists.Internal.Church.GetChurchServiceByID(miServiceAttendedID)
            End Get
        End Property
        Public Property ULGAttendance() As Boolean
            Get
                Return mbULGAttendance
            End Get
            Set(ByVal value As Boolean)
                If mbULGAttendance <> value Then mbChanged = True
                mbULGAttendance = value
            End Set
        End Property
        Public Property BoomAttendance() As Boolean
            Get
                Return mbBoomAttendance
            End Get
            Set(ByVal value As Boolean)
                If mbBoomAttendance <> value Then mbChanged = True
                mbBoomAttendance = value
            End Set
        End Property
        Public Property FNLAttendance() As Boolean
            Get
                Return mbFNLAttendance
            End Get
            Set(ByVal value As Boolean)
                If mbFNLAttendance <> value Then mbChanged = True
                mbFNLAttendance = value
            End Set
        End Property
        Public Property [Call]() As Integer
            Get
                Return miCall
            End Get
            Set(ByVal value As Integer)
                If miCall <> value Then mbChanged = True
                miCall = value
            End Set
        End Property
        Public Property Visit() As Integer
            Get
                Return miVisit
            End Get
            Set(ByVal value As Integer)
                If miVisit <> value Then mbChanged = True
                miVisit = value
            End Set
        End Property
        Public Property Other() As Integer
            Get
                Return miOther
            End Get
            Set(ByVal value As Integer)
                If miOther <> value Then mbChanged = True
                miOther = value
            End Set
        End Property
        Public ReadOnly Property MonthSundayTotal() As Integer
            Get
                Return miMonthSundayTotal
            End Get
        End Property
        Public ReadOnly Property MonthULGTotal() As Integer
            Get
                Return miMonthULGTotal
            End Get
        End Property
        Public ReadOnly Property MonthBoomTotal() As Integer
            Get
                Return miMonthBoomTotal
            End Get
        End Property
        Public ReadOnly Property MonthCallTotal() As Integer
            Get
                Return miMonthCallTotal
            End Get
        End Property
        Public ReadOnly Property MonthVisitTotal() As Integer
            Get
                Return miMonthVisitTotal
            End Get
        End Property
        Public ReadOnly Property DateJoined() As Date
            Get
                Return mdDateJoined
            End Get
        End Property
        Public ReadOnly Property NoCarer() As Boolean
            Get
                Return mbNoCarer
            End Get
        End Property
        Public ReadOnly Property GUID() As String
            Get
                Return msGUID
            End Get
        End Property
        Public Property VisitorID() As Integer
            Get
                Return miVisitorID
            End Get
            Set(value As Integer)
                If miVisitorID <> value Then mbChanged = True
                miVisitorID = value
            End Set
        End Property
        Public ReadOnly Property Visitor() As CPCRVisitor
            Get
                Dim oPCRVisitor As New CPCRVisitor
                oPCRVisitor.LoadByID(VisitorID)
                Return oPCRVisitor
            End Get
        End Property
        Public Property VisitedPCRID() As Integer
            Get
                Return miVisitedPCRID
            End Get
            Set(value As Integer)
                If miVisitedPCRID <> value Then mbChanged = True
                miVisitedPCRID = value
            End Set
        End Property
        Public ReadOnly Property LastCommentMade() As Date
            Get
                Dim dDate As Date = Nothing
                Dim oComment As CContactComment
                For Each oComment In mcComment
                    If dDate = Nothing OrElse CDate(dDate) < CDate(oComment.CommentDate) Then
                        dDate = oComment.CommentDate
                    End If
                Next
                Return dDate
            End Get
        End Property
        Public ReadOnly Property LastCommentMadeDays() As Integer
            Get
                Dim dDate As Date = Me.LastCommentMade
                Return DateDiff(DateInterval.Day, dDate, Date.Now)
            End Get
        End Property

        Public ReadOnly Property CarerName() As String
            Get
                Return msCarerName
            End Get
        End Property
    End Class

End Namespace