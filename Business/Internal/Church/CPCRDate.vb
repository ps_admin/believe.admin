Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CPCRDate

        Private miPCRDateID As Integer
        Private mdDate As Date
        Private mbULG As Boolean
        Private mbBoom As Boolean
        Private mbFNL As Boolean
        Private mbReportIsDue As Boolean
        Private mbDeleted As Boolean

        Private mbChanged As Boolean

        Public Sub New()

            miPCRDateID = 0
            mdDate = Nothing
            mbULG = False
            mbBoom = False
            mbFNL = False
            mbReportIsDue = False
            mbDeleted = False

            mbChanged = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miPCRDateID = oDataRow("PCRDate_ID")
            mdDate = oDataRow("Date")
            mbULG = oDataRow("ULG")
            mbBoom = oDataRow("Boom")
            mbFNL = oDataRow("FNL")
            mbReportIsDue = oDataRow("ReportIsDue")
            mbDeleted = oDataRow("Deleted")

            mbChanged = False

        End Sub

        Public Sub Save()

            If mbChanged Then

                Dim oParameters As New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@Date", SQLDate(mdDate)))
                oParameters.Add(New SqlClient.SqlParameter("@ULG", mbULG))
                oParameters.Add(New SqlClient.SqlParameter("@Boom", mbBoom))
                oParameters.Add(New SqlClient.SqlParameter("@FNL", mbFNL))
                oParameters.Add(New SqlClient.SqlParameter("@ReportIsDue", mbReportIsDue))
                oParameters.Add(New SqlClient.SqlParameter("@Deleted", mbDeleted))

                If Me.ID = 0 Then
                    miPCRDateID = DataAccess.GetValue("sp_Church_PCRDateInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@PCRDate_ID", miPCRDateID))
                    DataAccess.ExecuteCommand("sp_Church_PCRDateUpdate", oParameters)
                End If

                mbChanged = False

            End If
        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miPCRDateID
            End Get
        End Property
        Public Property [Date]() As Date
            Get
                Return mdDate
            End Get
            Set(ByVal value As Date)
                If mdDate <> value Then mbChanged = True
                mdDate = value
            End Set
        End Property
        Public Property ULG() As Boolean
            Get
                Return mbULG
            End Get
            Set(ByVal value As Boolean)
                If mbULG <> value Then mbChanged = True
                mbULG = value
            End Set
        End Property
        Public Property Boom() As Boolean
            Get
                Return mbBoom
            End Get
            Set(ByVal value As Boolean)
                If mbBoom <> value Then mbChanged = True
                mbBoom = value
            End Set
        End Property
        Public Property FNL() As Boolean
            Get
                Return mbFNL
            End Get
            Set(ByVal value As Boolean)
                If mbFNL <> value Then mbChanged = True
                mbFNL = value
            End Set
        End Property
        Public Property ReportIsDue() As Boolean
            Get
                Return mbReportIsDue
            End Get
            Set(ByVal value As Boolean)
                If mbReportIsDue <> value Then mbChanged = True
                mbReportIsDue = value
            End Set
        End Property
        Public Property Deleted() As Boolean
            Get
                Return mbDeleted
            End Get
            Set(ByVal value As Boolean)
                If mbDeleted <> value Then mbChanged = True
                mbDeleted = value
            End Set
        End Property

    End Class

End Namespace