Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CRegion

        Private miRegionID As Integer
        Private msName As String
        Private miSortOrder As Integer
        Private mbDeleted As Boolean

        Public Sub New()

            miRegionID = 0
            msName = String.Empty
            miSortOrder = 0
            mbDeleted = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miRegionID = oDataRow("Region_ID")
            msName = oDataRow("Name")
            miSortOrder = oDataRow("SortOrder")
            mbDeleted = oDataRow("Deleted")

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miRegionID
            End Get
        End Property
        Public Property Name() As String
            Get
                Return msName
            End Get
            Set(ByVal value As String)
                msName = value
            End Set
        End Property
        Public Property SortOrder() As Integer
            Get
                Return miSortOrder
            End Get
            Set(ByVal value As Integer)
                miSortOrder = value
            End Set
        End Property
        Public Property Deleted() As Boolean
            Get
                Return mbDeleted
            End Get
            Set(ByVal value As Boolean)
                mbDeleted = value
            End Set
        End Property

    End Class

End Namespace