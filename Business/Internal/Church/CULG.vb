Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CULG

        Private miULGID As Integer
        Private msCode As String
        Private msName As String
        Private msGroupType As String
        Private miCampusID As Integer
        Private miMinistryID As Integer
        Private miRegionID As Integer
        Private mdActiveDate As Date
        Private miULGDateTimeID As Integer
        Private miULGDateDayID As Integer
        Private msAddress1 As String
        Private msAddress2 As String
        Private msSuburb As String
        Private miStateID As Integer
        Private msPostcode As String
        Private miCountryID As Integer
        Private mbInactive As Boolean
        Private miSortOrder As Integer

        Private mcULGContact As CArrayList(Of CULGContact)

        Private mbChanged As Boolean

        Public Sub New()

            miULGID = 0
            msCode = String.Empty
            msName = String.Empty
            msGroupType = String.Empty
            miCampusID = 0
            miMinistryID = 0
            miRegionID = 0
            mdActiveDate = Nothing
            miULGDateTimeID = 0
            miULGDateDayID = 0
            msAddress1 = String.Empty
            msAddress2 = String.Empty
            msSuburb = String.Empty
            miStateID = 0
            msPostcode = String.Empty
            miCountryID = 0
            mbInactive = False
            miSortOrder = 0

            mcULGContact = New CArrayList(Of CULGContact)

            mbChanged = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miULGID = oDataRow("ULG_ID")
            msCode = oDataRow("Code")
            msName = oDataRow("Name")
            msGroupType = oDataRow("GroupType")
            miCampusID = oDataRow("Campus_ID")
            miMinistryID = oDataRow("Ministry_ID")
            miRegionID = IsNull(oDataRow("Region_ID"), 0)
            mdActiveDate = oDataRow("ActiveDate")
            miULGDateTimeID = IsNull(oDataRow("ULGDateTime_ID"), 0)
            miULGDateDayID = IsNull(oDataRow("ULGDateDay_ID"), 0)
            msAddress1 = oDataRow("Address1")
            msAddress2 = oDataRow("Address2")
            msSuburb = oDataRow("Suburb")
            miStateID = oDataRow("State_ID")
            msPostcode = oDataRow("Postcode")
            miCountryID = oDataRow("Country_ID")
            mbInactive = oDataRow("Inactive")
            miSortOrder = oDataRow("SortOrder")

            Dim oULGContactRow As DataRow
            For Each oULGContactRow In oDataRow.GetChildRows("ULGContact")
                mcULGContact.Add(New CULGContact(oULGContactRow))
            Next

            mcULGContact.ResetChanges()

            mbChanged = False

        End Sub

        Public Sub Save()

            If mbChanged Then

                Dim oParameters As New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@Code", msCode))
                oParameters.Add(New SqlClient.SqlParameter("@Name", msName))
                oParameters.Add(New SqlClient.SqlParameter("@GroupType", msGroupType))
                oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLNull(miCampusID)))
                oParameters.Add(New SqlClient.SqlParameter("@Ministry_ID", SQLNull(miMinistryID)))
                oParameters.Add(New SqlClient.SqlParameter("@Region_ID", SQLNull(miRegionID)))
                oParameters.Add(New SqlClient.SqlParameter("@ActiveDate", SQLDate(mdActiveDate)))
                oParameters.Add(New SqlClient.SqlParameter("@ULGDateTime_ID", SQLNull(miULGDateTimeID)))
                oParameters.Add(New SqlClient.SqlParameter("@ULGDateDay_ID", SQLNull(miULGDateDayID)))
                oParameters.Add(New SqlClient.SqlParameter("@Address1", msAddress1))
                oParameters.Add(New SqlClient.SqlParameter("@Address2", msAddress2))
                oParameters.Add(New SqlClient.SqlParameter("@Suburb", msSuburb))
                oParameters.Add(New SqlClient.SqlParameter("@State_ID", SQLNull(miStateID)))
                oParameters.Add(New SqlClient.SqlParameter("@Postcode", msPostcode))
                oParameters.Add(New SqlClient.SqlParameter("@Country_ID", SQLNull(miCountryID)))
                oParameters.Add(New SqlClient.SqlParameter("@Inactive", mbInactive))

                If Me.ID = 0 Then
                    miULGID = DataAccess.GetValue("sp_Church_ULGInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@ULG_ID", miULGID))
                    DataAccess.ExecuteCommand("sp_Church_ULGUpdate", oParameters)
                End If

                mbChanged = False

            End If

        End Sub

        Public Function ULGContacts() As CArrayList(Of CULGContact)
            Return mcULGContact
        End Function

        Public Function ULGContactsCurrent() As CArrayList(Of CULGContact)

            Dim oULGContact As CULGContact
            Dim oArrayList As New CArrayList(Of CULGContact)
            For Each oULGContact In ULGContacts()
                If Not oULGContact.Inactive Then oArrayList.Add(oULGContact)
            Next
            Return oArrayList

        End Function
        Public Function GetCarerList(ByVal NPNCType_ID As Integer, ByVal Ministry_ID As Integer) As ArrayList

            Dim oArrayList As New ArrayList
            Dim oParameters As New ArrayList
            Dim oDataRow As DataRow

            oParameters.Add(New SqlClient.SqlParameter("@ULG_ID", miULGID))
            oParameters.Add(New SqlClient.SqlParameter("@NPNCType_ID", NPNCType_ID))
            oParameters.Add(New SqlClient.SqlParameter("@Ministry_ID", Ministry_ID)) 'to filter if planetkids - need to be reflected on stored proc'
            Dim oDataTable As DataTable = DataAccess.GetDataTable("sp_Church_ULGCarerListWithUpdatedRoles", oParameters)

            For Each oDataRow In oDataTable.Rows
                Dim oCarer As New NPNCCarer
                oCarer.Name = oDataRow("Name")
                oCarer.ID = oDataRow("Contact_ID")
                oArrayList.Add(oCarer)
            Next

            Return oArrayList

        End Function

        Public Function GetCarerList(ByVal ULGID As Integer) As ArrayList

            Dim oArrayList As New ArrayList
            Dim oParameters As New ArrayList
            Dim oDataRow As DataRow

            oParameters.Add(New SqlClient.SqlParameter("@ULG_ID", ULGID))
            Dim oDataTable As DataTable = DataAccess.GetDataTable("sp_church_GetCarerforPartnerContact", oParameters)

            For Each oDataRow In oDataTable.Rows
                Dim oCarer As New NPNCCarer
                oCarer.Name = oDataRow("FullName")
                oCarer.ID = oDataRow("Contact_ID")
                oArrayList.Add(oCarer)
            Next

            Return oArrayList

        End Function

        Public Function GetULMemberList() As ArrayList
            Dim oArrayList As New ArrayList
            Dim oParameters As New ArrayList

            oParameters.Add(New SqlClient.SqlParameter("@ULG_ID", miULGID))


            Dim oDataTable As DataTable = DataAccess.GetDataTable("sp_Church_ULGMemberList", oParameters)

            For Each oDataRow In oDataTable.Rows
                Dim oMember As New CContact
                oMember.FirstName = oDataRow("FirstName")
                oMember.LastName = oDataRow("LastName")
                oMember.ID = oDataRow("Contact_ID")
                oArrayList.Add(oMember)
            Next

            Return oArrayList

        End Function

        Public Function GetCoachList() As CArrayList(Of CContact)

            Dim oArrayList As New CArrayList(Of CContact)
            Dim oParameters As New ArrayList
            Dim oDataRow As DataRow

            oParameters.Add(New SqlClient.SqlParameter("@ULG_ID", miULGID))
            Dim oDataTable As DataTable = DataAccess.GetDataTable("sp_Church_ULGCoachList", oParameters)

            For Each oDataRow In oDataTable.Rows
                Dim oContact As New CContact
                oContact.LoadByID(oDataRow("Contact_ID"))
                oArrayList.Add(oContact)
            Next

            Return oArrayList

        End Function

        Public Function GetRegionalPastors() As CArrayList(Of CRegionalPastor)

            Dim oArrayList As New CArrayList(Of CRegionalPastor)
            Dim oRegionalPastor As CRegionalPastor
            For Each oRegionalPastor In Lists.Internal.Church.RegionalPastor.Values
                If oRegionalPastor.CampusID = CampusID Then
                    If oRegionalPastor.MinistryID = MinistryID Then
                        If oRegionalPastor.RegionID = 0 Or oRegionalPastor.RegionID = miRegionID Then
                            oArrayList.Add(oRegionalPastor)
                        End If
                    End If
                End If
            Next

            Return oArrayList

        End Function

        Public ReadOnly Property ID() As Integer
            Get
                Return miULGID
            End Get
        End Property
        Public Property Code() As String
            Get
                Return msCode
            End Get
            Set(ByVal value As String)
                If msCode <> value Then mbChanged = True
                msCode = value
            End Set
        End Property
        Public Property Name() As String
            Get
                Return msName
            End Get
            Set(ByVal value As String)
                If msName <> value Then mbChanged = True
                msName = value
            End Set
        End Property
        Public Property GroupType() As String
            Get
                Return msGroupType
            End Get
            Set(ByVal value As String)
                If msGroupType <> value Then mbChanged = True
                msGroupType = value
            End Set
        End Property
        Public Property CampusID() As Integer
            Get
                Return miCampusID
            End Get
            Set(ByVal Value As Integer)
                If miCampusID <> Value Then mbChanged = True
                miCampusID = Value
            End Set
        End Property
        Public ReadOnly Property Campus() As CCampus
            Get
                Return Lists.Internal.Church.GetCampusByID(miCampusID)
            End Get
        End Property
        Public Property MinistryID() As Integer
            Get
                Return miMinistryID
            End Get
            Set(ByVal value As Integer)
                If miMinistryID <> value Then mbChanged = True
                miMinistryID = value
            End Set
        End Property
        Public ReadOnly Property Ministry() As CMinistry
            Get
                Return Lists.Internal.Church.GetMinistryByID(miMinistryID)
            End Get
        End Property
        Public Property RegionID() As Integer
            Get
                Return miRegionID
            End Get
            Set(ByVal value As Integer)
                If miRegionID <> value Then mbChanged = True
                miRegionID = value
            End Set
        End Property
        Public ReadOnly Property Region() As CRegion
            Get
                Return Lists.Internal.Church.GetRegionByID(miRegionID)
            End Get
        End Property
        Public Property ActiveDate() As Date
            Get
                Return mdActiveDate
            End Get
            Set(ByVal value As Date)
                If mdActiveDate <> value Then mbChanged = True
                mdActiveDate = value
            End Set
        End Property
        Public Property ULGDateTimeID() As Integer
            Get
                Return miULGDateTimeID
            End Get
            Set(ByVal value As Integer)
                If miULGDateTimeID <> value Then mbChanged = True
                miULGDateTimeID = value
            End Set
        End Property
        Public ReadOnly Property ULGDateTime() As CULGDateTime
            Get
                Return Lists.Internal.Church.GetULGDateTimeByID(miULGDateTimeID)
            End Get
        End Property
        Public Property ULGDateDayID() As Integer
            Get
                Return miULGDateDayID
            End Get
            Set(ByVal value As Integer)
                If miULGDateDayID <> value Then mbChanged = True
                miULGDateDayID = value
            End Set
        End Property
        Public ReadOnly Property ULGDateDay() As CULGDateDay
            Get
                Return Lists.Internal.Church.GetULGDateDayByID(miULGDateDayID)
            End Get
        End Property
        Public Property Address1() As String
            Get
                Return msAddress1
            End Get
            Set(ByVal value As String)
                If msAddress1 <> value Then mbChanged = True
                msAddress1 = value
            End Set
        End Property
        Public Property Address2() As String
            Get
                Return msAddress2
            End Get
            Set(ByVal value As String)
                If msAddress2 <> value Then mbChanged = True
                msAddress2 = value
            End Set
        End Property
        Public Property Suburb() As String
            Get
                Return msSuburb
            End Get
            Set(ByVal value As String)
                If msSuburb <> value Then mbChanged = True
                msSuburb = value
            End Set
        End Property
        Public Property StateID() As Integer
            Get
                Return miStateID
            End Get
            Set(ByVal value As Integer)
                If miStateID <> value Then mbChanged = True
                miStateID = value
            End Set
        End Property
        Public ReadOnly Property State() As CState
            Get
                Return Lists.GetStateByID(miStateID)
            End Get
        End Property
        Public Property Postcode() As String
            Get
                Return msPostcode
            End Get
            Set(ByVal value As String)
                If msPostcode <> value Then mbChanged = True
                msPostcode = value
            End Set
        End Property
        Public Property CountryID() As Integer
            Get
                Return miCountryID
            End Get
            Set(ByVal value As Integer)
                If miCountryID <> value Then mbChanged = True
                miCountryID = value
            End Set
        End Property
        Public ReadOnly Property Country() As CCountry
            Get
                Return Lists.GetCountryByID(miCountryID)
            End Get
        End Property
        Public Property Inactive() As Boolean
            Get
                Return mbInactive
            End Get
            Set(ByVal value As Boolean)
                If mbInactive <> value Then mbChanged = True
                mbInactive = value
            End Set
        End Property
        Public Property SortOrder() As Integer
            Get
                Return miSortOrder
            End Get
            Set(ByVal value As Integer)
                miSortOrder = value
            End Set
        End Property

        Public ReadOnly Property CodeAndName() As String
            Get
                Return msCode + " " + msName
            End Get
        End Property

    End Class

End Namespace