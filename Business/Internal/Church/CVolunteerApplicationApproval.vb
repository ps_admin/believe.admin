﻿Imports System.Linq
Namespace Internal.Church


    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CVolunteerApplicationApproval
        'Private Variables
        Private miApprovalID As Integer
        Private miApplicationID As Integer
        Private miActionedByID As Integer?
        Private moActionedBy As CContact
        Private miApprovalTypeID As Integer
        Private mbApproved As Nullable(Of Boolean)
        Private msComments As String
        Private mdActionDate As Date?
        Public mbChanged As Boolean

        'Initialise
        Public Sub New()
            miApprovalID = 0
            miApplicationID = 0
            miActionedByID = 0
            moActionedBy = Nothing
            miApprovalTypeID = 0
            mbApproved = Nothing
            msComments = Nothing
            mdActionDate = Nothing

            mbChanged = False
        End Sub
        Public Sub New(ByRef oDataRow As DataRow)
            Me.New()

            miApprovalID = oDataRow("Approval_ID")
            miApplicationID = oDataRow("Application_ID")
            If Not oDataRow.IsNull("ActionedBy") Then
                miActionedByID = oDataRow("ActionedBy")
            End If
            miApprovalTypeID = oDataRow("ApprovalType_ID")
            If Not oDataRow.IsNull("Approved") Then
                mbApproved = oDataRow("Approved")
            End If

            If Not oDataRow.IsNull("Comments") Then
                msComments = oDataRow("Comments")
            End If
            If Not oDataRow.IsNull("ActionDate") Then
                mdActionDate = oDataRow("ActionDate")
            End If
        End Sub

        Public Sub SetApprovalAction(ByVal bApproved As Boolean, _
                                     ByVal bApprovedByID As Integer)

            miActionedByID = bApprovedByID
            mbApproved = bApproved

            mbChanged = True
        End Sub

        'Main Methods
        Public Sub ActionApplication()
            'update the object
            mdActionDate = Date.Now

            'Prepare the parameters to pass into the stored procedures
            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@ApprovalID", miApprovalID))
            oParameters.Add(New SqlClient.SqlParameter("@ActionedBy", miActionedByID))
            oParameters.Add(New SqlClient.SqlParameter("@Approved", mbApproved))
            oParameters.Add(New SqlClient.SqlParameter("@Comments", msComments))
            oParameters.Add(New SqlClient.SqlParameter("@ActionDate", mdActionDate))

            'Execute stored procedure to update
            DataAccess.ExecuteCommand("sp_Volunteer_ActionApproval", oParameters)

            'If this approval is of priority 1... send email to UL Coach and Ministry Head
            If mbApproved And ApprovalType.Priority = 1 Then
                ' Search for the ministry heads
                oParameters = New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@MinistryID", Application.MinistryID))
                oParameters.Add(New SqlClient.SqlParameter("@RegionID", Application.RegionID))
                oParameters.Add(New SqlClient.SqlParameter("@CampusID", Application.CampusID))
                oParameters.Add(New SqlClient.SqlParameter("@VolunteerAreaID", Application.VolunteerAreaID))

                'Execute stored procedure to retrive Ministry Heads
                'Dim oContact As CContact
                'Dim oDataRow As DataRow
                Dim oDataTable As DataTable
                oDataTable = DataAccess.GetDataTable("sp_Volunteer_GetMinistryHead", oParameters)

                'Send emails to the ministry head(s)
                'For Each oDataRow In oDataTable.Rows
                '    oContact = New CContact
                '    oContact.LoadDataRow(oDataRow)
                '    SendEmailNewVolunteerNotificationToMinistry(oContact, Application.Contact, _
                '                                                ApprovalType.VolunteerArea.Name)
                'Next

                'Execute stored procedure to retrieve Applicant's Coach
                'oParameters = New ArrayList
                'oParameters.Add(New SqlClient.SqlParameter("@ApplicantID", Application.ContactID))
                'oDataTable = DataAccess.GetDataTable("sp_Volunteer_GetApplicantCoach", oParameters)

                'Send email to coaches
                'For Each oDataRow In oDataTable.Rows
                '    oContact = New CContact
                '    oContact.LoadDataRow(oDataRow)
                '    SendEmailNewVolunteerNotificationToCoach(oContact, Application.Contact, _
                '                                                ApprovalType.VolunteerArea.Name)
                'Next
            End If
        End Sub

        'Public readonly methods
        ''' <summary>
        ''' The Approval ID
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property ID() As Integer
            Get
                Return miApprovalID
            End Get
        End Property
        ''' <summary>
        ''' Volunteer Application ID
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property ApplicationID() As Integer
            Get
                Return miApplicationID
            End Get
        End Property
        ''' <summary>
        ''' The Volunteer Application object
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Application As CVolunteerApplication
            Get
                Return Lists.Internal.Church.GetVolunteerApplicationByID(miApplicationID)
            End Get
        End Property
        ''' <summary>
        ''' Identifier depicting who can action this approval request
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property ApprovalTypeID() As Integer
            Get
                Return miApprovalTypeID
            End Get
        End Property
        ''' <summary>
        ''' The Approval Type object
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property ApprovalType() As CVolunteerApplicationApprovalType
            Get
                Return Lists.Internal.Church.GetVolunteerApprovalTypeByID(miApprovalTypeID)
            End Get
        End Property
        ''' <summary>
        ''' ID of person who actioned the approval request
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property ActionedByID() As Integer
            Get
                Return miActionedByID
            End Get
        End Property
        Public ReadOnly Property ActionedBy As CContact
            Get
                If moActionedBy Is Nothing And miActionedByID > 0 Then
                    moActionedBy = New CContact
                    moActionedBy.LoadByID(miActionedByID)
                End If
                Return moActionedBy
            End Get
        End Property
        ''' <summary>
        ''' Whether or not the application has been approved
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Approved() As Nullable(Of Boolean)
            Get
                Return mbApproved
            End Get
        End Property
        ''' <summary>
        ''' Approval/Deny comments
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property Comments() As String
            Get
                Return msComments
            End Get
            Set(value As String)
                msComments = value
            End Set
        End Property
        ''' <summary>
        ''' The date when this was actioned
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property ActionDate() As Date
            Get
                Return mdActionDate
            End Get
        End Property
    End Class

End Namespace