﻿Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CVolunteerApplicationApprovalType

        'Private Variables
        Private miApprovalTypeID As Integer
        Private miVolunteerAreaID As Integer
        Private msName As String
        Private mbActive As Boolean
        Private miPriority As Integer

        Public Sub New()

            miApprovalTypeID = 0
            miVolunteerAreaID = 0
            msName = String.Empty
            mbActive = False
            miPriority = 0

        End Sub
        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()
            miApprovalTypeID = oDataRow("ApprovalType_ID")
            miVolunteerAreaID = oDataRow("VolunteerArea_ID")
            msName = oDataRow("Name")
            mbActive = oDataRow("Active")
            miPriority = oDataRow("Priority")

        End Sub

        ''' <summary>
        ''' ID of the Approval Type
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property ID As Integer
            Get
                Return miApprovalTypeID
            End Get
        End Property
        ''' <summary>
        ''' ID of the Volunteer Area
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property VolunteerAreaID() As Integer
            Get
                Return miVolunteerAreaID
            End Get
        End Property
        ''' <summary>
        ''' Volunteer Area
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property VolunteerArea() As CVolunteerArea
            Get
                Return Lists.Internal.Church.VolunteerArea(miVolunteerAreaID)
            End Get
        End Property
        ''' <summary>
        ''' The Name of the Approval Type
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Name() As String
            Get
                Return msName
            End Get
        End Property
        ''' <summary>
        ''' Whether the Approval Type is active or not
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Active() As Boolean
            Get
                Return mbActive
            End Get
        End Property
        ''' <summary>
        ''' The Priority level of the Approval Type, The lower the number the higher the authority
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Priority() As Integer
            Get
                Return miPriority
            End Get
        End Property
        Public ReadOnly Property VerifyApprover(ByVal user As CUser, application As CVolunteerApplication) As Boolean
            Get
                Dim oDataTable As DataTable

                If String.Equals(Name, "UL") Then
                    'Special case if Approval Type Name is 'UL'
                    Dim oParameters As New ArrayList
                    oParameters.Add(New SqlClient.SqlParameter("@CoachID", user.ID))
                    oParameters.Add(New SqlClient.SqlParameter("@ApplicantID", application.ContactID))

                    oDataTable = DataAccess.GetDataTable("sp_Volunteer_VerifyCoach", oParameters)
                    If oDataTable.Rows.Count > 0 Then
                        Return True
                    Else
                        Return False
                    End If
                Else
                    'Prepare the parameters to pass into the stored procedures
                    Dim oParameters As New ArrayList
                    oParameters.Add(New SqlClient.SqlParameter("@ApprovalTypeID", miApprovalTypeID))
                    oParameters.Add(New SqlClient.SqlParameter("@ContactID", user.ID))

                    If application.MinistryID > 0 Then
                        oParameters.Add(New SqlClient.SqlParameter("@MinistryID", application.MinistryID))
                    End If
                    If application.RegionID > 0 Then
                        oParameters.Add(New SqlClient.SqlParameter("@RegionID", application.RegionID))
                    End If
                    If application.CampusID > 0 Then
                        oParameters.Add(New SqlClient.SqlParameter("@CampusID", application.CampusID))
                    End If

                    oDataTable = DataAccess.GetDataTable("sp_Volunteer_VerifyApprover", oParameters)
                    If oDataTable.Rows.Count > 0 Then
                        Return True
                    Else
                        Return False
                    End If
                End If

            End Get
        End Property
    End Class
End Namespace


