﻿Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CVolunteerApplicationQuestion

        'Private Variables
        Private miQuestionID As Integer
        Private msQuestion As String
        Private msType As String
        Private mbRequired As Boolean
        Private mbActive As Boolean
        Private miSortOrder As Integer

        'Initialise
        Public Sub New()

            miQuestionID = 0
            msQuestion = String.Empty
            msType = String.Empty
            mbRequired = True
            mbActive = True
            miSortOrder = 0

        End Sub
        Public Sub New(ByRef oDataRow As DataRow)
            Me.New()

            miQuestionID = oDataRow("Question_ID")
            msQuestion = oDataRow("Question")
            msType = oDataRow("Type")
            mbRequired = oDataRow("Required")
            mbActive = oDataRow("Active")
            miSortOrder = oDataRow("SortOrder")

        End Sub

        ''' <summary>
        ''' The Question ID
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property ID() As Integer
            Get
                Return miQuestionID
            End Get
        End Property
        ''' <summary>
        ''' The Question
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Question() As String
            Get
                Return msQuestion
            End Get
        End Property
        ''' <summary>
        ''' The type of question
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Type() As String
            Get
                Return msType
            End Get
        End Property
        ''' <summary>
        ''' Whether the question is required
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Required() As Boolean
            Get
                Return mbRequired
            End Get
        End Property
        ''' <summary>
        ''' Whether the question is active
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Active() As Boolean
            Get
                Return mbActive
            End Get
        End Property
        ''' <summary>
        ''' The sorting order of the questions
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property SortOrder() As Integer
            Get
                Return miSortOrder
            End Get
        End Property
    End Class

End Namespace

