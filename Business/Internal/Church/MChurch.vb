Namespace Internal.Church

    Public Module MChurch

        Public Structure NPNCCarer
            Private miID As Integer
            Private msName As String
            Public Property ID() As Integer
                Get
                    Return miID
                End Get
                Set(ByVal value As Integer)
                    miID = value
                End Set
            End Property
            Public Property Name() As String
                Get
                    Return msName
                End Get
                Set(ByVal value As String)
                    msName = value
                End Set
            End Property
        End Structure

        Public Sub ActionComment(ByVal iCommentID As Integer, ByVal iUserID As Integer)

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Comment_ID", iCommentID))
            oParameters.Add(New SqlClient.SqlParameter("@User_ID", iUserID))

            DataAccess.GetDataTable("sp_Church_ActionComment", oParameters)

        End Sub

Public Function WelcomeCardRead(ByVal CampusID As String, ByVal MinistryID As String)
            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", CampusID))
            oParameters.Add(New SqlClient.SqlParameter("@Ministry_ID", MinistryID))
            Return DataAccess.GetDataTable("sp_church_WelcomeCardList", oParameters)
        End Function

        Public Function BoomWelcomeCardRead(ByVal CampusID As Integer)
            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", CampusID))

            Return DataAccess.GetDataTable("sp_church_BoomWelcomeCardList", oParameters)
        End Function

        Public Function Contactcheckupfamily(ByVal FamilyID As Integer)
            Dim oParameters As New ArrayList

            oParameters.Add(New SqlClient.SqlParameter("@Family_ID", FamilyID))
            'Dim oDataTable As DataTable =
            'Business.DataAccess.GetDataTable("sp_Common_FamilyMembersSelect", oParameters)
            Return DataAccess.GetDataTable("sp_Common_FamilyMembersSelect", oParameters)
        End Function

        Public Function BoomContactMailingListVisit(ByRef Contactid As Integer, ByRef Campusid As Integer, ByRef Comment As String, ByRef AddedBy_ID As Integer, ByRef BoomEmergencycontactName As String, ByRef BoomEmergencycontactNumber As String, ByRef MailingList As Integer) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", Contactid))
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", Campusid))
            oParameters.Add(New SqlClient.SqlParameter("@Comment", Comment))
            oParameters.Add(New SqlClient.SqlParameter("@AddedBy_ID", AddedBy_ID))
            oParameters.Add(New SqlClient.SqlParameter("@EmergencyContactName", BoomEmergencycontactName))
            oParameters.Add(New SqlClient.SqlParameter("@EmergencyContactNumber", BoomEmergencycontactNumber))
            oParameters.Add(New SqlClient.SqlParameter("@MailingList", MailingList))
            'DataAccess.ExecuteCommand("sp_church_InsertMailingListVisiting", oParameters)
            Return DataAccess.GetDataTable("sp_church_BoomContactMailingListVisiting", oParameters)

        End Function

        Public Function BoomContactMailingListAdd(ByRef Contactid As Integer, ByRef Campusid As Integer, ByRef NpncId As Integer, ByRef Comment As String, ByRef AddedBy_ID As Integer, ByRef BoomEmergencycontactName As String, ByRef BoomEmergencycontactNumber As String, ByRef MailingListclick As Integer) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Contactid", Contactid))
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", Campusid))
            oParameters.Add(New SqlClient.SqlParameter("@NpncId", NpncId))
            oParameters.Add(New SqlClient.SqlParameter("@Comment", Comment))
            oParameters.Add(New SqlClient.SqlParameter("@AddedBy_ID", AddedBy_ID))
            oParameters.Add(New SqlClient.SqlParameter("@EmergencyContactName", BoomEmergencycontactName))
            oParameters.Add(New SqlClient.SqlParameter("@EmergencyContactNumber", BoomEmergencycontactNumber))
			oParameters.Add(New SqlClient.SqlParameter("@MailingListClick", MailingListclick))

            Return DataAccess.GetDataTable("sp_church_BoomContactMailingListAdd", oParameters)

        End Function

        Public Function CheckPreviousPCRCompleted(ByVal oULG As CULG) As DataTable

            'This is used when the ULG leader opens the 'current report', to check if they
            'have completed the previous report first. It will give them a dialog box if the 
            'previous report has not been completed.

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@ULG_ID", SQLObject(oULG)))

            Return DataAccess.GetDataTable("sp_Church_ULGCheckPreviousPCRReportCompleted", oParameters)

        End Function

        Public Function NCCardRead(ByVal CampusID As String, ByVal MinistryID As String)
            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", CampusID))
            oParameters.Add(New SqlClient.SqlParameter("@Ministry_ID", MinistryID))
            Return DataAccess.GetDataTable("sp_church_NCCardList", oParameters)
        End Function

        Public Function ReportBoomNightStats(ByVal sEventDate As Date) As DataSet

            Dim oParameters As New ArrayList

            Dim oTableMappings As New ArrayList
            oTableMappings.Add(New Common.DataTableMapping("Table", "Church_BoomContactEntry"))
            oTableMappings.Add(New Common.DataTableMapping("Table1", "Church_BoomDateTransportOptions"))

            oParameters.Add(New SqlClient.SqlParameter("@EventDate", sEventDate))

            Return DataAccess.GetDataSet("sp_Church_ReportBoomNightStats", oTableMappings, oParameters)

        End Function

        Public Sub Clear247PrayerList(ByVal oCampus As CCampus)

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))

            DataAccess.ExecuteCommand("sp_Church_247PrayerReset", oParameters)

        End Sub

        Public Sub ClearCovenantFormDates(ByVal oCampus As CCampus)

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))

            DataAccess.ExecuteCommand("sp_Church_CovenantFormDatesClear", oParameters)

        End Sub

        Public Function GetCourseEnrolmentContactList(ByVal iCourseInstanceID As Integer) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@CourseInstance_ID", iCourseInstanceID))

            Return DataAccess.GetDataTable("sp_Church_CourseEnrolmentGetContactList", oParameters)

        End Function

        Public Function GetLatestPCRDate() As CPCRDate

            Return Lists.Internal.Church.GetPCRDateByID(DataAccess.GetValue("SELECT PCRDate_ID FROM vw_Church_LatestPCRReportDate"))

        End Function

        Public Function GetNPNCInitialStatusList(ByVal oNPNCType As CNPNCType, ByVal bShowInEntryForm As Boolean, ByVal bShowInSummaryStatsEntry As Boolean) As ArrayList

            Dim oArrayList As New ArrayList
            Dim oNPNCInitialStatus As CNPNCInitialStatus
            For Each oNPNCInitialStatus In Lists.Internal.Church.NPNCInitialStatusByNPNCType(oNPNCType).Values
                If oNPNCInitialStatus.ShowInEntryForm = bShowInEntryForm Or oNPNCInitialStatus.ShowInSummaryStatsEntry = bShowInSummaryStatsEntry Then
                    oArrayList.Add(oNPNCInitialStatus)
                End If
            Next
            Return oArrayList

        End Function

        Public Function GetPCRDate(ByVal dDate As Date) As CPCRDate

            Return Lists.Internal.Church.GetPCRDateByID(DataAccess.GetValue("SELECT PCRDate_ID FROM Church_PCRDate WHERE CONVERT(NVARCHAR, [Date], 106) = CONVERT(NVARCHAR, '" & SQLDate(dDate) & "', 106)"))

        End Function

        Public Function GetPCRReminderList(ByVal iContactID As Integer) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", iContactID))

            Return DataAccess.GetDataTable("sp_Church_PCRMainScreenReminder", oParameters)

        End Function

        Public Function DedupeByEvents(ByVal sCheckEvent As Boolean, ByVal sEventId As Integer, ByVal sNoofrecord As Integer, ByVal sAttended As Boolean) As DataSet

            Dim oParameters As New ArrayList

            Dim oTableMappings As New ArrayList
            oTableMappings.Add(New Common.DataTableMapping("Table", "Common_Contact"))

            oParameters.Add(New SqlClient.SqlParameter("@CheckEvents", sCheckEvent))
            oParameters.Add(New SqlClient.SqlParameter("@Conference_ID", sEventId))
            oParameters.Add(New SqlClient.SqlParameter("@Attended", sAttended))
            oParameters.Add(New SqlClient.SqlParameter("@NumberofTimes", sNoofrecord))

            Return DataAccess.GetDataSet("sp_Common_DatabaseDedupesExecute", oTableMappings, oParameters)

        End Function

        Public Function KidsMinistryCheckAttendance(ByVal iContactID As Integer, ByVal iChurchServiceID As Integer, ByVal dDate As Date) As CKidsMinistryAttendance

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", iContactID))
            oParameters.Add(New SqlClient.SqlParameter("@ChurchService_ID", iChurchServiceID))
            oParameters.Add(New SqlClient.SqlParameter("@Date", SQLDate(dDate)))

            Dim oDataTable As DataTable = DataAccess.GetDataTable("sp_Church_KidsMinistryCheckAttendance", oParameters)

            If oDataTable.Rows.Count = 0 Then
                Return Nothing
            Else
                Return New CKidsMinistryAttendance(oDataTable.Rows(0))
            End If

        End Function

        Public Function KidsMinistryCheckedInList(ByVal iChurchServiceID As Integer, _
                                                  ByVal dDate As Date) As DataSet

            Dim oDataSet As DataSet

            Dim oTableMappings As New ArrayList
            oTableMappings.Add(New Common.DataTableMapping("Table", "Children"))
            oTableMappings.Add(New Common.DataTableMapping("Table1", "Clearance"))

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@ChurchService_ID", iChurchServiceID))
            oParameters.Add(New SqlClient.SqlParameter("@Date", SQLDate(dDate)))

            oDataSet = DataAccess.GetDataSet("sp_Church_KidsMinistryCheckedInList", oTableMappings, oParameters)


            oDataSet.Relations.Add("Clearance", oDataSet.Tables("Children").Columns("Contact_ID"), _
                                                  oDataSet.Tables("Clearance").Columns("Contact_ID"))

            Return oDataSet

        End Function

        Public Function KidsMinistryFamilySearch(ByVal sName As String, _
                                                 ByVal iCampusID As Integer, _
                                                 ByVal iChurchServiceID As Integer, _
                                                 ByVal dDate As Date) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Name", sName))
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", iCampusID))
            oParameters.Add(New SqlClient.SqlParameter("@ChurchService_ID", iChurchServiceID))
            oParameters.Add(New SqlClient.SqlParameter("@Date", dDate))

            Return DataAccess.GetDataTable("sp_Church_KidsMinistryFamilySearch", oParameters)

        End Function

        Public Function KidsMinistryLeadersList(ByVal dDate As Date, ByVal iChurchServiceID As Integer) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Date", dDate))
            oParameters.Add(New SqlClient.SqlParameter("@ChurchService_ID", iChurchServiceID))

            Return DataAccess.GetDataTable("sp_Church_KidsMinistryAttendanceLeaderList", oParameters)

        End Function
        Public Function KidsMinistryReportAttendance(ByVal dDate As Date, ByVal iChurchServiceID As Integer, ByVal iULGID As Integer, ByVal iCampusID As Integer, ByVal iAttended As Integer) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Date", dDate))
            oParameters.Add(New SqlClient.SqlParameter("@ChurchService_ID", iChurchServiceID))
            oParameters.Add(New SqlClient.SqlParameter("@ULG_ID", iULGID))
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", iCampusID))
            oParameters.Add(New SqlClient.SqlParameter("@Attended", iAttended))

            Return DataAccess.GetDataTable("sp_Church_KidsMinistryReportAttendance", oParameters)

        End Function

        Public Function KidsMinistryReportAttendanceLeader(ByVal dDate As Date, ByVal iChurchServiceID As Integer) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Date", dDate))
            oParameters.Add(New SqlClient.SqlParameter("@ChurchService_ID", iChurchServiceID))


            Return DataAccess.GetDataTable("sp_Church_KidsMinistryReportAttendanceLeader", oParameters)

        End Function

        Public Function KidsMinistryReportToiletBreak(ByVal dDate As Date, ByVal iChurchServiceID As Integer, ByVal iULGID As Integer) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Date", dDate))
            oParameters.Add(New SqlClient.SqlParameter("@ChurchService_ID", iChurchServiceID))
            oParameters.Add(New SqlClient.SqlParameter("@ULG_ID", iULGID))


            Return DataAccess.GetDataTable("sp_Church_KidsMinistryReportToiletBreak", oParameters)

        End Function

        Public Function KidsMinistrySelectFamily(ByVal iFamilyID As Integer, _
                                                 ByVal iChurchServiceID As Integer, _
                                                 ByVal dDate As Date, _
                                                 ByVal bCheckedInOnly As Boolean) As DataSet

            Dim oDataSet As DataSet

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Family_ID", iFamilyID))
            oParameters.Add(New SqlClient.SqlParameter("@ChurchService_ID", iChurchServiceID))
            oParameters.Add(New SqlClient.SqlParameter("@Date", dDate))
            oParameters.Add(New SqlClient.SqlParameter("@CheckedInOnly", bCheckedInOnly))

            Dim oTableMappings As New ArrayList
            oTableMappings.Add(New Common.DataTableMapping("Table", "Members"))
            oTableMappings.Add(New Common.DataTableMapping("Table1", "Codes"))
            oTableMappings.Add(New Common.DataTableMapping("Table2", "Clearance"))

            oDataSet = DataAccess.GetDataSet("sp_Church_KidsMinistrySelectFamily", oTableMappings, oParameters)


            oDataSet.Relations.Add("Codes", oDataSet.Tables("Members").Columns("Contact_ID"), _
                                                  oDataSet.Tables("Codes").Columns("Contact_ID"))

            oDataSet.Relations.Add("Clearance", oDataSet.Tables("Members").Columns("Contact_ID"), _
                                                  oDataSet.Tables("Clearance").Columns("Contact_ID"))

            Return oDataSet

        End Function

        Public Function KidsMinistryToiletBreaksIncompleted() As DataTable

            Return DataAccess.GetDataTable("sp_Church_KidsMinistryToiletBreakListIncompleted", New ArrayList)

        End Function

        Public Function ListAltarDecisions(ByVal oNCDecisionType As CNCDecisionType, _
                                            ByVal iEntryType As Integer, _
                                            ByVal oCampusDecision As CCampus, _
                                            ByVal oCampus As CCampus, _
                                            ByVal oMinistry As CMinistry, _
                                            ByVal oMinistryRegion As CRegion, _
                                            ByVal dStartDate As Date, _
                                            ByVal dEndDate As Date) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@NCDecisionType_ID", SQLObject(oNCDecisionType)))
            oParameters.Add(New SqlClient.SqlParameter("@EntryType", iEntryType))
            oParameters.Add(New SqlClient.SqlParameter("@CampusDecision_ID", SQLObject(oCampusDecision)))
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))
            oParameters.Add(New SqlClient.SqlParameter("@Ministry_ID", SQLObject(oMinistry)))
            oParameters.Add(New SqlClient.SqlParameter("@Region_ID", SQLObject(oMinistryRegion)))
            oParameters.Add(New SqlClient.SqlParameter("@StartDate", SQLDate(dStartDate)))
            oParameters.Add(New SqlClient.SqlParameter("@EndDate", SQLDate(dEndDate)))

            Return DataAccess.GetDataTable("sp_Church_NPNCAltarDecisionsList", oParameters)

        End Function

        Public Function ListNCSummaryStats(ByVal dStartDate As Date, ByVal dEndDate As Date, _
                                           ByVal oCampus As CCampus, ByVal oMinistry As CMinistry, ByVal oNCDecisionType As CNCDecisionType) As CArrayList(Of CNCSummaryStats)

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@StartDate", dStartDate))
            oParameters.Add(New SqlClient.SqlParameter("@EndDate", dEndDate))
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))
            oParameters.Add(New SqlClient.SqlParameter("@Ministry_ID", SQLObject(oMinistry)))
            oParameters.Add(New SqlClient.SqlParameter("@DecisionType_ID", SQLObject(oNCDecisionType)))

            Dim oDataTable As DataTable = DataAccess.GetDataTable("sp_Church_NCSummaryStatsList", oParameters)
            Dim oDataRow As DataRow
            Dim oReturn As New CArrayList(Of CNCSummaryStats)

            For Each oDataRow In oDataTable.Rows
                oReturn.Add(New CNCSummaryStats(oDataRow))
            Next

            Return oReturn

        End Function

        Public Function ListNPNC(ByVal oNPNCType As CNPNCType,
                                            ByVal oCarer As CUser,
                                            ByVal oCampus As CCampus,
                                            ByVal oMinistry As CMinistry,
                                            ByVal oRegion As CRegion,
                                            ByVal oCampusDecision As CCampus,
                                            ByVal dStartDate As Date,
                                            ByVal dEndDate As Date) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@NPNCType_ID", SQLObject(oNPNCType)))
            oParameters.Add(New SqlClient.SqlParameter("@Carer_ID", SQLObject(oCarer)))
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))
            oParameters.Add(New SqlClient.SqlParameter("@Ministry_ID", SQLObject(oMinistry)))
            oParameters.Add(New SqlClient.SqlParameter("@Region_ID", SQLObject(oRegion)))
            oParameters.Add(New SqlClient.SqlParameter("@CampusDecision_ID", SQLObject(oCampusDecision)))
            oParameters.Add(New SqlClient.SqlParameter("@StartDate", SQLDate(dStartDate)))
            oParameters.Add(New SqlClient.SqlParameter("@EndDate", SQLDate(dEndDate)))

            Return DataAccess.GetDataTable("sp_Church_NPNCListPerfOpt", oParameters)

        End Function

        Public Function ListNPNCCarers(ByVal oCampus As CCampus, ByVal oNPNCType As Integer) As ArrayList

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))
            oParameters.Add(New SqlClient.SqlParameter("@NPNCType_ID", oNPNCType))

            Dim oDataTable As DataTable = DataAccess.GetDataTable("sp_Church_NPNCCarerList", oParameters)
            Dim oDataRow As DataRow
            Dim oArraylist As New ArrayList

            For Each oDataRow In oDataTable.Rows
                Dim oCarer As New NPNCCarer
                oCarer.Name = oDataRow("Name")
                oCarer.ID = oDataRow("Contact_ID")
                oArraylist.Add(oCarer)
            Next

            Return oArraylist

        End Function

        Public Function ListNPNCComments(ByVal oNPNCType As CNPNCType, _
                                         ByVal oCarer As CUser, _
                                         ByVal oCampus As CCampus, _
                                         ByVal oMinistry As CMinistry, _
                                         ByVal oRegion As CRegion, _
                                         ByVal oCampusDecision As CCampus, _
                                         ByVal iComments As Integer, _
                                         ByVal dStartDate As Date, _
                                         ByVal dEndDate As Date, _
                                         ByVal bIncludeActionedComments As Boolean) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@NPNCType_ID", SQLObject(oNPNCType)))
            oParameters.Add(New SqlClient.SqlParameter("@Carer_ID", SQLObject(oCarer)))
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))
            oParameters.Add(New SqlClient.SqlParameter("@Ministry_ID", SQLObject(oMinistry)))
            oParameters.Add(New SqlClient.SqlParameter("@Region_ID", SQLObject(oRegion)))
            oParameters.Add(New SqlClient.SqlParameter("@CampusDecision_ID", SQLObject(oCampusDecision)))
            oParameters.Add(New SqlClient.SqlParameter("@Comments", iComments))
            oParameters.Add(New SqlClient.SqlParameter("@StartDate", SQLDate(dStartDate)))
            oParameters.Add(New SqlClient.SqlParameter("@EndDate", SQLDate(dEndDate)))
            oParameters.Add(New SqlClient.SqlParameter("@IncludeActionedComments", bIncludeActionedComments))

            'Return DataAccess.GetDataTable("sp_Church_NPNCCommentsList", oParameters)
            Return DataAccess.GetDataTable("sp_Church_NPNCCommentsListGeneral", oParameters)        'sp_Church_NPNCCommentsListGeneral

        End Function

        Public Function ListNPNCWelcomePacksIssued(ByVal dStartDate As Date, ByVal dEndDate As Date) As CArrayList(Of CNPNCWelcomePacksIssued)

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@StartDate", dStartDate))
            oParameters.Add(New SqlClient.SqlParameter("@EndDate", dEndDate))

            Dim oDataTable As DataTable = DataAccess.GetDataTable("sp_Church_NPNCWelcomePacksIssuedList", oParameters)
            Dim oDataRow As DataRow
            Dim oReturn As New CArrayList(Of CNPNCWelcomePacksIssued)

            For Each oDataRow In oDataTable.Rows
                oReturn.Add(New CNPNCWelcomePacksIssued(oDataRow))
            Next

            Return oReturn

        End Function

        Public Function ListNPSummaryStats(ByVal dStartDate As Date, ByVal dEndDate As Date, _
                                           ByVal oCampus As CCampus, ByVal oInitialStatus As CNPNCInitialStatus) As CArrayList(Of CNPSummaryStats)

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@StartDate", dStartDate))
            oParameters.Add(New SqlClient.SqlParameter("@EndDate", dEndDate))
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))
            oParameters.Add(New SqlClient.SqlParameter("@InitialStatus_ID", SQLObject(oInitialStatus)))

            Dim oDataTable As DataTable = DataAccess.GetDataTable("sp_Church_NPSummaryStatsList", oParameters)
            Dim oDataRow As DataRow
            Dim oReturn As New CArrayList(Of CNPSummaryStats)

            For Each oDataRow In oDataTable.Rows
                oReturn.Add(New CNPSummaryStats(oDataRow))
            Next

            Return oReturn

        End Function

        Public Function ListPastoralCareComments(ByVal oCampus As CCampus,
                                                 ByVal oMinistry As CMinistry,
                                                 ByVal oRegion As CRegion,
                                                 ByVal oCommentType As CCommentType,
                                                 ByVal oChurchStatus As CChurchStatus,
                                                 ByVal oULG As CULG,
                                                 ByVal iComments As Integer,
                                                 ByVal dStartDate As Date,
                                                 ByVal dEndDate As Date,
                                                 ByVal bIncludeActionedComments As Boolean,
                                                 ByVal sCommentTypeDoNotShow As String) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))
            oParameters.Add(New SqlClient.SqlParameter("@Ministry_ID", SQLObject(oMinistry)))
            oParameters.Add(New SqlClient.SqlParameter("@Region_ID", SQLObject(oRegion)))
            oParameters.Add(New SqlClient.SqlParameter("@CommentType_ID", SQLObject(oCommentType)))
            oParameters.Add(New SqlClient.SqlParameter("@ChurchStatus_ID", SQLObject(oChurchStatus)))
            oParameters.Add(New SqlClient.SqlParameter("@ULG_ID", SQLObject(oULG)))
            oParameters.Add(New SqlClient.SqlParameter("@Comments", iComments))
            oParameters.Add(New SqlClient.SqlParameter("@StartDate", SQLDate(dStartDate)))
            oParameters.Add(New SqlClient.SqlParameter("@EndDate", SQLDate(dEndDate)))
            oParameters.Add(New SqlClient.SqlParameter("@IncludeActionedComments", bIncludeActionedComments))
            oParameters.Add(New SqlClient.SqlParameter("@CommentTypeDoNotShow", sCommentTypeDoNotShow))

            Return DataAccess.GetDataTable("sp_Church_PastoralCareCommentsList", oParameters)

        End Function

        Public Function ListUnsubsribeMailingList(ByVal firstname As String, ByVal lastname As String, ByVal phoneno As String) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@FirstName", firstname))
            oParameters.Add(New SqlClient.SqlParameter("@LastName", lastname))
            oParameters.Add(New SqlClient.SqlParameter("@PhoneNum", phoneno))

            Return DataAccess.GetDataTable("sp_Church_UnsubscribeMailingList", oParameters)

        End Function

        Public Function ListCourseAttendance(ByVal oCampus As CCampus, _
                                             ByVal dStartDate As Date, _
                                             ByVal dEndDate As Date) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))
            oParameters.Add(New SqlClient.SqlParameter("@StartDate", SQLDate(dStartDate)))
            oParameters.Add(New SqlClient.SqlParameter("@EndDate", SQLDate(dEndDate)))

            Return DataAccess.GetDataTable("sp_Church_CourseAttendanceList", oParameters)

        End Function

        Public Function ListPCR(ByVal oCampus As CCampus, _
                                ByVal oMinistry As CMinistry, _
                                ByVal oRegion As CRegion, _
                                ByVal oUser As CUser) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))
            oParameters.Add(New SqlClient.SqlParameter("@Ministry_ID", SQLObject(oMinistry)))
            oParameters.Add(New SqlClient.SqlParameter("@Region_ID", SQLObject(oRegion)))
            oParameters.Add(New SqlClient.SqlParameter("@User_ID", SQLObject(oUser)))

            Return DataAccess.GetDataTable("sp_Church_PCRList", oParameters)

        End Function

        Public Function ListRegionalPastors(ByVal oCampus As CCampus) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))

            Return DataAccess.GetDataTable("sp_Church_RegionalPastorList", oParameters)

        End Function

        Public Function ListULG(ByVal oCampus As CCampus, _
                                ByVal oMinistry As CMinistry, _
                                ByVal oRegion As CRegion, _
                                ByVal iInactive As Integer) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))
            oParameters.Add(New SqlClient.SqlParameter("@Ministry_ID", SQLObject(oMinistry)))
            oParameters.Add(New SqlClient.SqlParameter("@Region_ID", SQLObject(oRegion)))
            oParameters.Add(New SqlClient.SqlParameter("@Inactive", iInactive))

            Return DataAccess.GetDataTable("sp_Church_ULGList", oParameters)

        End Function

        Public Sub NightlyPCRIncompleteEmail()

            'Only run this if it's the day after PCR's are due

            Dim iPCRDueHours As Integer
            Dim dPCRDate As Date

            iPCRDueHours = DataAccess.GetValue("SELECT PCRDueHours FROM Church_PCROptions")
            dPCRDate = IsNull(DataAccess.GetValue("SELECT MAX([Date]) FROM Church_PCRDate WHERE [Date] < dbo.GetRelativeDate()"), Nothing)

            If dPCRDate <> Nothing Then
                If GetDate.DayOfWeek = dPCRDate.AddHours(iPCRDueHours).DayOfWeek Then

                    Dim oDataTable As DataTable
                    Dim oDataRow As DataRow
                    Dim oPCRDate As CPCRDate = Nothing

                    oDataTable = DataAccess.GetDataTable("sp_Church_NightlyIncompletePCREmail")

                    If oDataTable.Rows.Count > 0 Then
                        oPCRDate = Lists.Internal.Church.GetPCRDateByID(oDataTable.Rows(0)("PCRDate_ID"))
                    End If

                    'Only send reminder emails if the report is due.
                    If oPCRDate.ReportIsDue Then
                        For Each oDataRow In oDataTable.Rows
                            Dim oContact As New CContact
                            Dim oULG As CULG = Lists.Internal.Church.GetULGByID(oDataRow("ULG_ID"))
                        Next
                    End If

                End If
            End If

        End Sub

        Public Function Prayer247EmailList(ByVal oCampus As CCampus) As DataSet

            Dim oDataSet As DataSet

            Dim oTableMappings As New ArrayList
            oTableMappings.Add(New Common.DataTableMapping("Table", "Contact"))
            oTableMappings.Add(New Common.DataTableMapping("Table1", "247Prayer"))

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))

            oDataSet = DataAccess.GetDataSet("sp_Church_247PrayerEmailList", oTableMappings, oParameters)


            oDataSet.Relations.Add("247Prayer", oDataSet.Tables("Contact").Columns("Contact_ID"), _
                                                  oDataSet.Tables("247Prayer").Columns("Contact_ID"))

            Return oDataSet

        End Function

        Public Function Report247PrayerList(ByVal oCampus As CCampus, ByVal oRegion As CRegion, ByVal oMinistry As CMinistry, ByVal o247PrayerDay As C247PrayerDay, _
                                            ByVal o247PrayerStartTime As C247PrayerTime, ByVal o247PrayerEndTime As C247PrayerTime, _
                                            ByVal o247PrayerContactMethod As C247PrayerContactMethod) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))
            oParameters.Add(New SqlClient.SqlParameter("@Region_ID", SQLObject(oRegion)))
            oParameters.Add(New SqlClient.SqlParameter("@Ministry_ID", SQLObject(oMinistry)))
            oParameters.Add(New SqlClient.SqlParameter("@Day_ID", SQLObject(o247PrayerDay)))
            oParameters.Add(New SqlClient.SqlParameter("@StartTime_ID", SQLObject(o247PrayerStartTime)))
            oParameters.Add(New SqlClient.SqlParameter("@EndTime_ID", SQLObject(o247PrayerEndTime)))
            oParameters.Add(New SqlClient.SqlParameter("@ContactMethod_ID", SQLObject(o247PrayerContactMethod)))

            Return DataAccess.GetDataTable("sp_Church_Report247PrayerList", oParameters)

        End Function

        Public Function Report247PrayerStats(ByVal oCampus As CCampus) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))

            Return DataAccess.GetDataTable("sp_Church_Report247PrayerStats", oParameters)

        End Function

        Public Function ReportCourseCompletion(ByVal oCourseInstance As CCourseInstance, ByVal iDisplayType As Integer) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@CourseInstance_ID", SQLObject(oCourseInstance)))
            oParameters.Add(New SqlClient.SqlParameter("@DisplayType", iDisplayType))

            Return DataAccess.GetDataTable("sp_Church_ReportCourseCompletion", oParameters)

        End Function

        Public Function ReportCourseCompletion2(ByVal sCourseInstance As String, ByVal iDisplayType As Integer) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@CourseInstance_ID", sCourseInstance))
            oParameters.Add(New SqlClient.SqlParameter("@DisplayType", iDisplayType))

            Return DataAccess.GetDataTable("sp_Church_ReportCourseCompletion2", oParameters)

        End Function

        Public Function ReportCourseMarketing(ByVal oCourseInstance As CCourseInstance, ByVal iDisplayType As Integer) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@CourseInstance_ID", SQLObject(oCourseInstance)))
            oParameters.Add(New SqlClient.SqlParameter("@DisplayType", iDisplayType))

            Return DataAccess.GetDataTable("sp_Church_ReportCourseMarketing", oParameters)

        End Function

        Public Function ReportCustomPerfOpt(ByVal sChurchStatus As String, ByVal sCampus As String, ByVal sMinistry As String,
                                     ByVal sRegion As String, ByVal sULG As String,
                                     ByVal sRole As String, ByVal iCampusRole As Integer, ByVal iMinAge As Integer,
                                     ByVal iMaxAge As Integer, ByVal sGender As String, ByVal sSuburbs As String,
                                     ByVal sStates As String,
                                     ByVal oRegisteredEvent As External.Events.CConference,
                                     ByVal oNotRegisteredEvent As External.Events.CConference, ByVal sCourse As String,
                                     ByVal sCourseNotReg As String, ByVal sCourseInstanceReg As String, ByVal sCourseInstanceNotReg As String,
                                     ByVal bUseMailingList As Boolean,
                                     ByVal sMailingList As String, ByVal oMailingListType As CMailingListType,
                                     ByVal oUser As CUser, ByVal bShowRoles As Boolean,
                                     ByVal iAnotherLanguageID As Integer, ByVal iNationality_ID As Integer) As DataTable


            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@ChurchStatus", sChurchStatus))
            oParameters.Add(New SqlClient.SqlParameter("@Campus", sCampus))
            oParameters.Add(New SqlClient.SqlParameter("@Ministry", sMinistry))
            oParameters.Add(New SqlClient.SqlParameter("@Region", sRegion))
            oParameters.Add(New SqlClient.SqlParameter("@ULG", sULG))
            oParameters.Add(New SqlClient.SqlParameter("@Role", sRole))
            oParameters.Add(New SqlClient.SqlParameter("@CampusRole_ID", iCampusRole))
            oParameters.Add(New SqlClient.SqlParameter("@MinAge", iMinAge))
            oParameters.Add(New SqlClient.SqlParameter("@MaxAge", iMaxAge))
            oParameters.Add(New SqlClient.SqlParameter("@Gender", sGender))
            oParameters.Add(New SqlClient.SqlParameter("@Suburbs", sSuburbs))
            oParameters.Add(New SqlClient.SqlParameter("@States", sStates))
            oParameters.Add(New SqlClient.SqlParameter("@Conference_ID", SQLObject(oRegisteredEvent)))
            oParameters.Add(New SqlClient.SqlParameter("@NotRegisteredConference_ID", SQLObject(oNotRegisteredEvent)))
            oParameters.Add(New SqlClient.SqlParameter("@Course", sCourse))
            oParameters.Add(New SqlClient.SqlParameter("@NotRegisteredCourse", sCourseNotReg))
            oParameters.Add(New SqlClient.SqlParameter("@CourseInstance", sCourseInstanceReg))
            oParameters.Add(New SqlClient.SqlParameter("@NotRegisteredCourseInstance", sCourseInstanceNotReg))
            oParameters.Add(New SqlClient.SqlParameter("@UseMailingList", bUseMailingList))
            oParameters.Add(New SqlClient.SqlParameter("@MailingList", sMailingList))
            oParameters.Add(New SqlClient.SqlParameter("@MailingListType_ID", SQLObject(oMailingListType)))
            oParameters.Add(New SqlClient.SqlParameter("@User_ID", SQLObject(oUser)))
            oParameters.Add(New SqlClient.SqlParameter("@ShowRoles", bShowRoles))
            oParameters.Add(New SqlClient.SqlParameter("@Nationality_ID", iNationality_ID))
            oParameters.Add(New SqlClient.SqlParameter("@AnotherLanguage_ID", iAnotherLanguageID))

            Return DataAccess.GetDataTable("sp_Church_ReportCustomCourseInstancePerfOpt", oParameters)

        End Function


        Public Function ReportCustom(ByVal sChurchStatus As String, ByVal sCampus As String, ByVal sMinistry As String,
                                     ByVal sRegion As String, ByVal sULG As String,
                                     ByVal sRole As String, ByVal iCampusRole As Integer, ByVal iMinAge As Integer,
                                     ByVal iMaxAge As Integer, ByVal sGender As String, ByVal sSuburbs As String,
                                     ByVal sStates As String,
                                     ByVal oRegisteredEvent As External.Events.CConference,
                                     ByVal oNotRegisteredEvent As External.Events.CConference, ByVal sCourse As String,
                                     ByVal sCourseNotReg As String, ByVal sCourseInstanceReg As String, ByVal sCourseInstanceNotReg As String,
                                     ByVal bUseMailingList As Boolean,
                                     ByVal sMailingList As String, ByVal oMailingListType As CMailingListType,
                                     ByVal oUser As CUser, ByVal bShowRoles As Boolean,
                                     ByVal iAnotherLanguageID As Integer, ByVal iNationality_ID As Integer) As DataTable


            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@ChurchStatus", sChurchStatus))
            oParameters.Add(New SqlClient.SqlParameter("@Campus", sCampus))
            oParameters.Add(New SqlClient.SqlParameter("@Ministry", sMinistry))
            oParameters.Add(New SqlClient.SqlParameter("@Region", sRegion))
            oParameters.Add(New SqlClient.SqlParameter("@ULG", sULG))
            oParameters.Add(New SqlClient.SqlParameter("@Role", sRole))
            oParameters.Add(New SqlClient.SqlParameter("@CampusRole_ID", iCampusRole))
            oParameters.Add(New SqlClient.SqlParameter("@MinAge", iMinAge))
            oParameters.Add(New SqlClient.SqlParameter("@MaxAge", iMaxAge))
            oParameters.Add(New SqlClient.SqlParameter("@Gender", sGender))
            oParameters.Add(New SqlClient.SqlParameter("@Suburbs", sSuburbs))
            oParameters.Add(New SqlClient.SqlParameter("@States", sStates))
            oParameters.Add(New SqlClient.SqlParameter("@Conference_ID", SQLObject(oRegisteredEvent)))
            oParameters.Add(New SqlClient.SqlParameter("@NotRegisteredConference_ID", SQLObject(oNotRegisteredEvent)))
            oParameters.Add(New SqlClient.SqlParameter("@Course", sCourse))
            oParameters.Add(New SqlClient.SqlParameter("@NotRegisteredCourse", sCourseNotReg))
            oParameters.Add(New SqlClient.SqlParameter("@CourseInstance", sCourseInstanceReg))
            oParameters.Add(New SqlClient.SqlParameter("@NotRegisteredCourseInstance", sCourseInstanceNotReg))
            oParameters.Add(New SqlClient.SqlParameter("@UseMailingList", bUseMailingList))
            oParameters.Add(New SqlClient.SqlParameter("@MailingList", sMailingList))
            oParameters.Add(New SqlClient.SqlParameter("@MailingListType_ID", SQLObject(oMailingListType)))
            oParameters.Add(New SqlClient.SqlParameter("@User_ID", SQLObject(oUser)))
            oParameters.Add(New SqlClient.SqlParameter("@ShowRoles", bShowRoles))
            oParameters.Add(New SqlClient.SqlParameter("@Nationality_ID", iNationality_ID))
            oParameters.Add(New SqlClient.SqlParameter("@AnotherLanguage_ID", iAnotherLanguageID))

            Return DataAccess.GetDataTable("sp_Church_ReportCustomCourseInstance", oParameters)

        End Function

        Public Function InsertMailingListVisiting(ByRef Contactid As Integer, ByRef Campusid As Integer, ByRef Comment As String, ByRef AddedBy_ID As Integer, ByRef MailingListClick As Integer, ByRef CardType As Integer) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", Contactid))
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", Campusid))
            oParameters.Add(New SqlClient.SqlParameter("@Comment", Comment))
            oParameters.Add(New SqlClient.SqlParameter("@AddedBy_ID", AddedBy_ID))
            oParameters.Add(New SqlClient.SqlParameter("@MailingList", MailingListClick))
            oParameters.Add(New SqlClient.SqlParameter("@CardType", CardType))

            'DataAccess.ExecuteCommand("sp_church_InsertMailingListVisiting", oParameters)
            Return DataAccess.GetDataTable("sp_church_InsertMailingListVisiting", oParameters)

        End Function

        Public Function InsertMailingListAdd(ByRef Contactid As Integer, ByRef Campusid As Integer, ByRef NpncId As Integer, ByRef Comment As String, ByRef AddedBy_ID As Integer, ByRef MailingListClick As Integer) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Contactid", Contactid))
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", Campusid))
            oParameters.Add(New SqlClient.SqlParameter("@NpncId", NpncId))
            oParameters.Add(New SqlClient.SqlParameter("@Comment", Comment))
            oParameters.Add(New SqlClient.SqlParameter("@AddedBy_ID", AddedBy_ID))
            oParameters.Add(New SqlClient.SqlParameter("@MailingListClick", MailingListClick))


            Return DataAccess.GetDataTable("sp_church_InsertMailingListAdd", oParameters)

        End Function


        Public Function ReportFamily(ByRef oCampus As CCampus, ByRef oMinistry As CMinistry, _
                                     ByRef oRegion As CRegion, ByVal bIncludeInactive As Boolean) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))
            oParameters.Add(New SqlClient.SqlParameter("@Ministry_ID", SQLObject(oMinistry)))
            oParameters.Add(New SqlClient.SqlParameter("@Region_ID", SQLObject(oRegion)))
            oParameters.Add(New SqlClient.SqlParameter("@IncludeInactive", bIncludeInactive))

            Return DataAccess.GetDataTable("sp_Church_ReportFamily", oParameters)

        End Function

        Public Function ReportPCRPastReports(ByVal oULG As CULG) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@ULG_ID", SQLObject(oULG)))

            Return DataAccess.GetDataTable("sp_Church_PCRPastReportsList", oParameters)

        End Function

        Public Function ReportULGAttendanceRoll(ByVal oULG As CULG, _
                                                ByVal dStartDate As Date, ByVal dEndDate As Date) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@ULG_ID", SQLObject(oULG)))
            oParameters.Add(New SqlClient.SqlParameter("@StartDate", dStartDate))
            oParameters.Add(New SqlClient.SqlParameter("@EndDate", dEndDate))

            Return DataAccess.GetDataTable("sp_Church_ReportULGAttendanceRoll", oParameters)

        End Function

        Public Sub UpdateIntroductoryEmailSent(ByVal iContactID As Integer, ByVal iCourseInstanceID As Integer)

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", iContactID))
            oParameters.Add(New SqlClient.SqlParameter("@CourseInstance_ID", iCourseInstanceID))

            DataAccess.ExecuteCommand("sp_Church_CourseEnrolmentMarkIntroductoryEmailSent", oParameters)

        End Sub

        Public Function GetULContactsbyID(ByVal iULGID As Integer) As DataTable
            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@ULG_ID", iULGID))


            Return DataAccess.GetDataTable("sp_Church_ULGMemberList", oParameters)
        End Function

        Public Function GetUnderCare(ByVal CarerID As Integer) As DataTable
            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@CarerId", CarerID))


            Return DataAccess.GetDataTable("sp_PCR_GetContactsByCarerId", oParameters)
        End Function

        Public Sub RemoveAllUnderCare(ByVal CarerID As Integer)
            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@CarerId", CarerID))

            DataAccess.ExecuteCommand("sp_PCR_RemoveAllUnderCareByCarerID", oParameters)
        End Sub

        Public Sub RemoveAllNCUnderCare(ByVal CarerID As Integer)
            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@CarerId", CarerID))

            DataAccess.ExecuteCommand("sp_PCR_RemoveAllNCUnderCareByCarerID", oParameters)
        End Sub

        Public Function GetCarerByContactID(ByVal ContactID As Integer) As DataTable
            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@ContactId", ContactID))


            Return DataAccess.GetDataTable("sp_PCR_GetCarerByContactIdWithUpdatedRoles", oParameters)
        End Function

        Public Function EditCarerAccess(ByVal CarerID As Integer, ByVal ContactID As Integer, ByVal Command As String) As DataTable
            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@CarerId", CarerID))
            oParameters.Add(New SqlClient.SqlParameter("@ContactId", ContactID))
            oParameters.Add(New SqlClient.SqlParameter("@Command", Command))

            Return DataAccess.GetDataTable("sp_PCR_EditCarerAccess", oParameters)
        End Function

        Public Function GetOfferingReport(ByVal oCampus As CCampus,
                                            ByVal oMinistry As CMinistry,
                                            ByVal oRegion As CRegion,
                                            ByVal dStartDate As Date, ByVal dEndDate As Date)
            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))
            oParameters.Add(New SqlClient.SqlParameter("@Ministry_ID", SQLObject(oMinistry)))
            oParameters.Add(New SqlClient.SqlParameter("@Region_ID", SQLObject(oRegion)))
            oParameters.Add(New SqlClient.SqlParameter("@StartDate", SQLDate(dStartDate)))
            oParameters.Add(New SqlClient.SqlParameter("@EndDate", SQLDate(dEndDate)))

            Return DataAccess.GetDataTable("sp_Church_ReportOffering", oParameters)
        End Function

        Public Function DedupeByEvents(ByVal sEventId As Integer, ByVal sNoofrecord As Integer) As DataSet

            Dim oParameters As New ArrayList

            Dim oTableMappings As New ArrayList
            oTableMappings.Add(New Common.DataTableMapping("Table", "Common_Contact"))

            oParameters.Add(New SqlClient.SqlParameter("@Conference_ID", sEventId))
            oParameters.Add(New SqlClient.SqlParameter("@NumberofTimes", sNoofrecord))

            Return DataAccess.GetDataSet("sp_Common_DatabaseDedupesExecute", oTableMappings, oParameters)

        End Function

        Public Function ListQuickCourses(ByVal oCampus As CCampus,
                                         ByVal dStartDate As Date,
                                         ByVal dEndDate As Date) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))
            oParameters.Add(New SqlClient.SqlParameter("@StartDate", SQLDate(dStartDate)))
            oParameters.Add(New SqlClient.SqlParameter("@EndDate", SQLDate(dEndDate)))

            Return DataAccess.GetDataTable("sp_Church_CourseInstanceCurrent", oParameters)

        End Function

        Public Function ListCourseAttendees(ByVal CourseInstance As CCourseInstance) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@CourseInstance_ID", SQLObject(CourseInstance)))
            Return DataAccess.GetDataTable("sp_Church_CourseAttendees", oParameters)

        End Function

        Public Function ListQuickSearch(ByVal QuickSearchName As String) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Name", QuickSearchName))
            Return DataAccess.GetDataTable("sp_Church_CourseInstanceCurrentSearch", oParameters)

        End Function

        Public Function SaveAttendee(ByVal ContactID As Integer, ByVal CourseInstanceID As Integer, ByVal EnrolmentDate As DateTime, ByVal Comments As String, ByVal IntroductoryEmailSentDate As String, ByVal Completed As Boolean, ByVal moUser As CUser) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", ContactID))
            oParameters.Add(New SqlClient.SqlParameter("@CourseInstance_ID", CourseInstanceID))
            oParameters.Add(New SqlClient.SqlParameter("@EnrolmentDate", EnrolmentDate))
            oParameters.Add(New SqlClient.SqlParameter("@Comments", Comments))
            oParameters.Add(New SqlClient.SqlParameter("@IntroductoryEmailSentDate", IntroductoryEmailSentDate))
            oParameters.Add(New SqlClient.SqlParameter("@Completed", Completed))
            oParameters.Add(New SqlClient.SqlParameter("@User_ID", SQLObject(moUser)))

            Return DataAccess.GetDataTable("sp_Church_ContactCourseEnrolmentInsertAttendee", oParameters)

        End Function

        Public Sub TransferULGAndCarers(ByVal ContactID As Integer, ByVal CarerID As Integer, ByVal PreviousULG_ID As Integer, ByVal NewULG_ID As Integer, ByVal NPNCType As Boolean)
            Dim msGUID As String
            Dim oParameters As New ArrayList

            msGUID = System.Guid.NewGuid.ToString

            oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", ContactID))
            oParameters.Add(New SqlClient.SqlParameter("@Carer_ID", CarerID))
            oParameters.Add(New SqlClient.SqlParameter("@PreviousULG_ID", PreviousULG_ID))
            oParameters.Add(New SqlClient.SqlParameter("@NewULG_ID", NewULG_ID))
            oParameters.Add(New SqlClient.SqlParameter("@NPNCType", NPNCType))
            oParameters.Add(New SqlClient.SqlParameter("@GUID", msGUID))

            DataAccess.GetDataTable("[sp_Church_TransferULGAndCarers]", oParameters)

        End Sub

        Public Function ListServolutionManagement(ByVal CampusID As Integer) As DataTable
            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", CampusID))
            Return DataAccess.GetDataTable("sp_Church_VolunteerAreaServolutionList", oParameters)

        End Function
        Public Sub GetOfferingReportUpdate(ByVal oCampus As CCampus,
                                            ByVal oMinistry As CMinistry,
                                            ByVal oRegion As CRegion,
                                            ByVal dStartDate As Date, ByVal dEndDate As Date)
            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))
            oParameters.Add(New SqlClient.SqlParameter("@Ministry_ID", SQLObject(oMinistry)))
            oParameters.Add(New SqlClient.SqlParameter("@Region_ID", SQLObject(oRegion)))
            oParameters.Add(New SqlClient.SqlParameter("@StartDate", SQLDate(dStartDate)))
            oParameters.Add(New SqlClient.SqlParameter("@EndDate", SQLDate(dEndDate)))

            DataAccess.ExecuteCommand("sp_Church_ReportOfferingUpdate", oParameters)
        End Sub

    End Module

End Namespace
