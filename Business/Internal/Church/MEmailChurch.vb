Imports System.text

Namespace Internal.Church

    Public Module MEmailChurch

        Public Function SendEmailForgetPassword(ByVal oContact As CContact) As Boolean

            Dim sError As String = String.Empty
            Dim sEmail As New StringBuilder

            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Dear " & oContact.FirstName & ", <br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("This email has been sent automatically by the Planetshakers Church Database Webpage in response")
            sEmail.AppendLine("to your request to reset your password.<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("To reset your password and access the church database, please click the link below.<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("If nothing happens, please copy and paste the link into the address bar of your internet browser.<br />")
            sEmail.AppendLine("This link will take you to the change password page.<br />")
            sEmail.AppendLine("<br />")
            Select Case DatabaseOptions.DatabaseInstance
                Case EnumDatabaseInstance.AU
                    sEmail.AppendLine("<a href=""https://church.planetshakers.com/resetpassword.aspx?ID=" & oContact.GUID & """>https://church.planetshakers.com/resetpassword.aspx?ID=" & oContact.GUID & "</a><br />")
                Case EnumDatabaseInstance.SA
                    sEmail.AppendLine("<a href=""http://churchsa.planetshakers.com/resetpassword.aspx?ID=" & oContact.GUID & """>http://churchsa.planetshakers.com/resetpassword.aspx?ID=" & oContact.GUID & "</a><br />")
            End Select
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("God Bless,<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("The Planetshakers Church Database Team<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Planetshakers Ministries International Inc<br />")
            sEmail.AppendLine("ABN: 79 480 989 066<br />")
            sEmail.AppendLine("Post: PO Box 5171, South Melbourne Vic 3205<br />")
            sEmail.AppendLine("P: 1300 88 33 21 / +61 3 9896 7999<br />")
            sEmail.AppendLine("F: +61 3 9830 7683<br />")
            sEmail.AppendLine("E: church@planetshakers.com<br />")
            sEmail.AppendLine("W: www.planetshakers.com<br />")

            Dim oEmail As New CEmailChurch
            oEmail.EmailText = sEmail.ToString
            oEmail.Subject = "Planetshakers Church Database Password Reset"
            Return oEmail.SendEmail(oContact.Email, oContact.Name, sError)

        End Function

    End Module

End Namespace