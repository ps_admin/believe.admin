
<CLSCompliant(True)> _
Public Module MEnumerators
    Public Enum EnumDatabaseInstance
        AU = 1
        SA = 2
        LA = 3
        TX = 4
        SG = 5
        CH = 6
    End Enum
    Public Enum EnumDataAccessType
        Direct = 1
        WebService = 2
    End Enum
    Public Enum EnumDatabaseObjectType
        Menu = 1
        Other = 2
    End Enum
    Public Enum EnumDatabaseModule
        Events = 1
        Church = 2
    End Enum
    Public Enum EnumContactDatabase
        Both = 0
        Internal = 1
        External = 2
    End Enum
    Public Enum EnumMailingList
        Events = 1
        Revolution = 2
        Music = 3
        Church = 4
        KingdomLife = 5
    End Enum
    Public Enum EnumMailingListType
        Post = 1
        Email = 2
        SMS = 3
    End Enum
    Public Enum EnumMailingListUnsubscribeReason
        LeftAddress = 1
        CustomerRequestedUnsubscription = 2
        SubscribedMultipleTimes = 3
        CustomerDeletedMadeInactive = 4
    End Enum

    Public Enum EnumSystemUserNames
        Administrator = 100
        Website = 101
    End Enum

    Public Enum EnumBankAccount
        PlanetshakersMinistries = 1
        CityChurch = 2
        StripeChurch = 3
        StripeMinistries = 4
    End Enum
    Public Enum EnumPaymentType
        Cash = 1
        Cheque = 2
        CreditCard = 3
        EFTPOS = 4
        Paypal = 5
    End Enum

    Public Enum EnumSiteAddress
        PlanetshakersDotCom = 1
        Revolution = 2
        Account = 4
    End Enum

    Public Enum EnumDenomination
        Independant = 1
        ACCAOG = 2
        Catholic = 3
        Lutheran = 4
        SalvationArmy = 5
        Presbyterian = 6
        Methodist = 7
        UnitingChurch = 8
        Apostolic = 10
        Baptist = 13
        ChurchOfChrist = 16
        Bretheran = 17
        SeventhDayAdventist = 19
        Other = 20
        Anglican = 21
        Charismatic = 23
        ChristianCityChurch = 24
        ChristianOutreachCentre = 25
        Orthodox = 26
        Pentecostal = 27
        NA = 28
        FoursquareElim = 29
    End Enum

    Public Enum EnumSalutation
        None = 1
        Mr = 2
        Mrs = 3
        Miss = 4
        Ms = 5
        Pastor = 6
        Dr = 7
    End Enum

    Public Enum EnumSchoolGrade
        NA = 1
        Kindergarten = 2
        Prep = 3
        Grade1 = 4
        Grade2 = 5
        Grade3 = 6
        Grade4 = 7
        Grade5 = 8
        Grade6 = 9
    End Enum

    Public Enum EnumFamilyMemberType
        FatherHusband = 1
        MotherWife = 2
        Son = 3
        Daughter = 4
    End Enum

End Module

Namespace External
    <CLSCompliant(True)> _
    Public Module MEnumerators
        Public Enum EnumChurchInvolvement
            BehindTheScenes = 1
            ChildrensMinistry = 2
            ChurchStaff = 3
            CongregationMember = 4
            MusicianVocalist = 5
            SeniorPastorMinister = 6
            SocialJusticeCommunity = 7
            TeamPastorLeader = 8
            UniversityMinistry = 9
            VolunteerAtChurch = 10
            WorshipPastorLeader = 11
            YouthMinistry = 12
            YouthPastorLeader = 13
        End Enum
    End Module

    Namespace Events
        <CLSCompliant(True)> _
        Public Module MEnumerators

            Public Enum EnumSaleType As Integer
                SaleTypeSale = 1
                SaleTypeReturn = 2
            End Enum
            Public Enum EnumTransType As Integer
                TransactionTypePOS = 1
                TransactionTypeSessionOrder = 2
            End Enum
            Public Enum EnumGroupCreditStatus As Integer
                NonCredit = 1
                AwaitingApproval = 2
                CreditApproved = 3
                CreditDeclined = 4
            End Enum
            Public Enum EnumGroupType As Integer
                YouthGroup = 1
                NonYouthGroup = 2
            End Enum
            Public Enum EnumGroupPromoPack As Integer
                NoneRequested = 1
                RegularPack = 2
                LargePack = 3
            End Enum

            Public Enum EnumBulkPurchaseType As Integer
                Accommodation = 1
                Catering = 2
                LeadershipBreakfast = 3
            End Enum

            Public Enum EnumGroupRegoType
                Bulk = 1
                [New] = 2
            End Enum

            Public Enum EnumConferenceType As Integer
                PlanetShakers = 1
                PlanetWorship = 2
                PlanetKids = 3
                BeautifulWoman = 4
                PlanetUni = 5
                MightyMen = 6
                PlanetBoom = 7
            End Enum

            Public Enum EnumWebPageStyle As Integer
                GenericStyle = 1
                PlanetWorship06 = 4
                PlanetShakers07 = 5
                PlanetKids07 = 6
                PlanetShakers08 = 7
                PlanetKidsSpace07 = 8
                PlanetBoom07 = 9
                BeautifulWoman2008 = 10
                PlanetshakersEvolution09 = 11
                BeautifulWoman2009 = 12
                Planetshakers2010 = 14
                BeautifulWoman2010 = 15
                MightyMen2009 = 16
                PlanetUniCamp2009 = 17
                MightyMen2010 = 19
                Planetshakers2011 = 20
                BeautifulWoman2011 = 21
                PlanetUniCamp2010 = 22
                LeadershipSummit2011 = 23
                Planetshakers2012 = 24
                BeautifulWoman2012 = 26
                PlanetUniCamp2011 = 27
                MightyMen2011 = 28
                Planetshakers2013 = 30
            End Enum
            Public Enum EnumWebPageType As Integer
                None = 0
                SingleRegistration = 1
                FamilyRegistration = 2
                FriendsRegistration = 3
                GroupRegistration = 4
            End Enum

            Public Enum EnumConference As Integer
                Planetshakers2007 = 4
                Planetshakers2008 = 7
                BeautifulWoman2008 = 10
                Planetshakers2009 = 11
                BeautifulWoman2009 = 12
                PlanetBoomCamp2008 = 13
                Planetshakers2010 = 14
                BeautifulWoman2010 = 15
                MightyMen2009 = 16
                PlanetUniCamp2009 = 17
                MightyMen2010 = 19
                Planetshakers2011 = 20
                BeautifulWoman2011 = 21
                PlanetUniCamp2010 = 22
                LeadershipSummit2011 = 23
                Planetshakers2012 = 24
                BeautifulWoman2012 = 26
                PlanetUniCamp2011 = 27
                MightyMen2011 = 28
                Planetshakers2013 = 30
                MightyMen2012 = 29
            End Enum

        End Module

    End Namespace

End Namespace

Namespace External.Revolution
    <CLSCompliant(True)> _
    Public Module MEnumerators
        Public Enum EnumSubscriptions
            TwelveMonthSubscription = 1
            TwelveMonthGiftCard = 2
            OneMonthTrial = 3
            AdditionalContentPack = 4
            OneMonthSubscription = 5
        End Enum

        Public Enum EnumBonusContentPacks
            PS08Sermons = 17
        End Enum

        Enum EnumRevolutionSubscriptionReportType
            CurrentlyActiveSubscriptions = 1
            NewSubscriptionsByMonth = 2
            ActiveSubscriptionsByMonth = 3
        End Enum

    End Module

End Namespace


Namespace Internal.Church
    <CLSCompliant(True)> _
    Public Module MEnumerators
        Public Enum EnumChurchCampus As Integer
            MelbourneCity = 1
            Geelong = 2
        End Enum

        Public Enum EnumChurchMinistry As Integer
            PlanetKids = 1
            Boom = 2
            PlanetUni = 3
            ChurchMinistry = 4
        End Enum

        Public Enum EnumCommentSource As Integer
            ContactDetails = 1
            PCR = 2
            NPNCInitialComment = 3
            NPNCOutcomeComment = 4
            PCRInboxComment = 5
            VolunteerDepartmentPCR = 6
        End Enum

        Public Enum EnumCommentType As Integer
            Administrative = 1
            Pastoral = 2
            Staff = 3
        End Enum

        Public Enum EnumNPNCType As Integer
            NewPeople = 1
            NewChristian = 2
        End Enum

        Public Enum EnumRole As Integer
            NCCarer = 19
        End Enum

        Public Enum EnumChurchStatus As Integer
            Partner = 1
            Contact = 2
            NewPeople = 4
            NewChristian = 5
            MailingList = 6
            Inactive = 7
        End Enum

        Public Enum EnumCardType As Integer
            WelcomeCard = 1
            NewChristianCard = 2
            BoomWelcomeCard = 3
            AddNewPeopleCard = 4
            AddNewChristianCard = 5
        End Enum

        Public Enum EnumReports As Integer

            PCPartnershipComparison = 100
            PCPartnershipComparisonChurchStatus = 105
            PCPartnershipStatusBreakdown = 110
            PCPastoralCareSummary = 115
            PCPastoralContacts = 120
            PCSundayChurch = 125
            PCGoneMissing = 130
            PCGoneMissingContacted = 135
            PCGoneMissingList = 140
            PCULGOfferings = 145
            PCULGAttendance = 150
            PCAdditionsAndDeletions = 155
            PCMovement = 156

            MSMasterStatsOverview = 201
            MSChurchPartnershipComparison = 202
            MSWhosBeenAttendingOverview = 203

            NPNCNewPeopleEntryPoints = 301
            NPNCMinistryAllocation = 302
            NPNCInitialStatus = 303
            NPNCSummaryStats = 304
            NPNCNewPeopleWelcomePacksMonthly = 306
            NPNCInFollowup = 307
            NPNCNewPeopleWelcomePacksYearly = 308
            NPNCOutcome = 309
            NPNCAgeDemographic = 310
            NPNCFirstContact = 311


            ULGMonthlySummary = 400
            UniqueULGAttendanceReport = 401
            EventsRegistrationReport = 500
            EventsMissingVolunteersTagReport = 501
            EventsMissingPriorityTagReport = 502
        End Enum

        Public Enum EnumRoleType
            Ministry = 1
            Sunday = 2
        End Enum

        Public Enum EnumRoleTypeUL
            NotAnULRole = 1
            ULCoach = 2
            ULCarer = 3
        End Enum

        Public Enum Enum247PrayerContactMethod
            Email = 1
            Post = 2
        End Enum

        Public Enum Enum247PrayerDuration
            HalfHour = 1
            OneHour = 2
            OneAndAHalfHours = 3
            TwoHour = 4
        End Enum

        Public Enum EnumInternalContactPageStyle
            General = 1
            PlanetKids = 2
        End Enum

        Public Enum EnumChurchService
            ThreeOClock = 1
            FiveFifteen = 2
            All = 3
            TenFourtyFive = 4
        End Enum

        Public Enum EnumKidsMinistryAttendanceReporting
            NotAttended = 0
            Attended = 1
            Both = 2
        End Enum
    End Module

End Namespace
