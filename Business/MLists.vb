Public Module MLists

    Dim moLists As CLists

    Public ReadOnly Property Lists() As CLists
        Get

            If moLists Is Nothing Then
                moLists = New CLists
            End If
            Return moLists

        End Get
    End Property

    Public Sub ClearLists()
        moLists = Nothing
    End Sub

End Module
