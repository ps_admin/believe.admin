Imports Planetshakers.DataAccess

Public Module MPlanetshakersCommon

    Private msApplicationPath As String
    Private msCurrentWebDomain As String

    Private msStartDateTime As String               'Holds the starting date/t34ime from the server
    Private moStopWatch As CStopWatch               'Keeps amount of time elapsed since start, independent of system time

    Private miMinutesToKeepLoggedIn As Integer = 20
    Private miMinutesToKeepLoggedInLong As Integer = (60 * 24) * 30

    Private moDatabaseOptions As CDatabaseOptions

#Region "Global Application Properties"

    Public Property ApplicationPath() As String
        Get
            Return msApplicationPath
        End Get
        Set(ByVal Value As String)
            msApplicationPath = Value
        End Set
    End Property
    Public ReadOnly Property CurrentWebDomain() As String
        Get
            If System.Web.HttpContext.Current Is Nothing Then
                Return ""
            Else
                Return System.Web.HttpContext.Current.Request.ServerVariables("HTTP_HOST")
            End If
        End Get
    End Property

    Public ReadOnly Property GetSiteAddress(ByVal iSite As EnumSiteAddress) As String
        Get
            If My.Computer.Name.ToLower.Contains("josh") Then
                Select Case iSite
                    Case EnumSiteAddress.Account
                        Return "http://localhost/Planetshakers.Accounts.Web"
                    Case EnumSiteAddress.PlanetshakersDotCom
                        Return "http://www.planetshakers.com"
                    Case EnumSiteAddress.Revolution
                        Return "http://localhost/Planetshakers.Revolution.Web"
                End Select
            ElseIf CurrentWebDomain.StartsWith("dev.") Then
                Select Case iSite
                    Case EnumSiteAddress.Account
                        Return "http://dev.accounts.planetshakers.com"
                    Case EnumSiteAddress.PlanetshakersDotCom
                        Return "http://www.planetshakers.com"
                    Case EnumSiteAddress.Revolution
                        Return "http://dev.revolution.planetshakers.com"
                End Select
            Else
                Select Case iSite
                    Case EnumSiteAddress.Account
                        Return "https://accounts.planetshakers.com"
                    Case EnumSiteAddress.PlanetshakersDotCom
                        Return "http://www.planetshakers.com"
                    Case EnumSiteAddress.Revolution
                        Return "http://revolution.planetshakers.com"
                End Select
            End If

            Return String.Empty

        End Get

    End Property

    Public ReadOnly Property DatabaseOptions() As CDatabaseOptions
        Get
            If moDatabaseOptions Is Nothing Then
                Dim oDataRow As DataRow = DataAccess.GetDataRow("SELECT * FROM Common_DatabaseInstance")
                moDatabaseOptions = New CDatabaseOptions(oDataRow)
            End If
            Return moDatabaseOptions
        End Get
    End Property

#End Region

#Region "General Functions"

    Public ReadOnly Property GetDateTime() As Date
        Get
            If msStartDateTime = "" Then
                msStartDateTime = Format(DataAccess.GetValue("SELECT dbo.GetRelativeDate()"), "dd MMM yyyy HH:mm:ss.fff")
                moStopWatch = New CStopWatch
            End If
            Return CDate(msStartDateTime).AddMilliseconds(moStopWatch.Peek)
        End Get
    End Property

    Public ReadOnly Property GetDate() As Date
        Get
            Dim dDate As DateTime = GetDateTime
            Return CDate(Format(dDate, "dd MMM yyyy"))
        End Get
    End Property

    Public Function GetDateDiff(ByVal dDate1 As Date, ByVal ddate2 As Date) As String

        Dim sString As String = String.Empty
        Dim oTimeSpan As TimeSpan = New Date(Math.Floor(ddate2.Ticks / 10000000) * 10000000) -
                                    New Date(Math.Floor(dDate1.Ticks / 10000000) * 10000000)

        If oTimeSpan.Days > 0 Then sString += oTimeSpan.Days & "d "
        If oTimeSpan.Hours > 0 Then sString += oTimeSpan.Hours & "h "
        If oTimeSpan.Minutes > 0 Then sString += oTimeSpan.Minutes & "m "
        If oTimeSpan.Seconds Then sString += oTimeSpan.Seconds & "s "

        Return sString.Trim

    End Function

    Public Function MaxDate(ByVal dDate1 As Date, ByVal dDate2 As Date) As Date
        If dDate1 > dDate2 Then
            Return dDate1
        Else
            Return dDate2
        End If
    End Function
    Public Function MinDate(ByVal dDate1 As Date, ByVal dDate2 As Date) As Date
        If dDate1 < dDate2 Then
            Return dDate1
        Else
            Return dDate2
        End If
    End Function

    Public Function GetAge(ByVal Birthdate As Date, ByVal dFromDate As Date) As Long

        Try
            Select Case Birthdate.Month
                Case Is < dFromDate.Month
                    Return DateDiff("YYYY", Birthdate, dFromDate)
                Case Is = dFromDate.Month
                    Select Case Birthdate.Day
                        Case Is < dFromDate.Day
                            Return DateDiff("YYYY", Birthdate, dFromDate)
                        Case Is = dFromDate.Day
                            Return DateDiff("YYYY", Birthdate, dFromDate)
                        Case Is > dFromDate.Day
                            Return DateDiff("YYYY", Birthdate, dFromDate) - 1
                    End Select
                Case Is > dFromDate.Month
                    Return DateDiff("YYYY", Birthdate, dFromDate) - 1
                Case Else
                    Return 0
            End Select
        Catch ex As System.Exception
            'Error handling code does here
        End Try
        Return False

    End Function

    Public Function GetAgeDecimal(ByVal Birthdate As Date, ByVal dFromDate As Date) As Decimal

        Return (DateDiff("d", Birthdate, dFromDate) / 365.25)

    End Function
    Public Function FormatDate(ByVal sDate As String) As String
        If sDate.Trim.Length > 0 AndAlso IsDate(sDate) Then
            Return Format(CDate(sDate), "dd MMM yyyy")
        Else
            Return sDate
        End If
    End Function

    Public Function FormatDateTime(ByVal sDate As String) As String
        If sDate.Trim.Length > 0 AndAlso IsDate(sDate) Then
            Return Format(CDate(sDate), "dd MMM yyyy HH:mm")
        Else
            Return sDate
        End If
    End Function

    Public Function ContainsCharacters(ByVal sString As String, ByVal sCharacters As String) As Boolean

        Dim iCount As Integer
        For iCount = 0 To sString.Length - 1
            If sCharacters.Contains(sString.Substring(iCount, 1)) Then Return True
        Next

        Return False

    End Function

    Public Function GetItemByGUID(ByRef oHashTable As Hashtable, ByVal sGUID As String) As Object
        Dim oItem As Object
        For Each oItem In oHashTable.Values
            If oItem.GUID.ToString.ToLower = sGUID.ToLower Then Return oItem
        Next
        Return Nothing
    End Function

#End Region

#Region "Object Functions"

    Public Function ObjectID(ByVal oObject As Object) As Integer
        If oObject Is Nothing Then
            Return 0
        Else
            Return oObject.ID
        End If
    End Function

#End Region

#Region "Database Functions"

    Public Function SearchContact(ByVal iSearchBase As EnumContactDatabase,
                                  ByVal sQuickSearch As String,
                                  ByVal sFirstName As String,
                                  ByVal sLastName As String,
                                  ByVal sChurchName As String,
                                  ByVal sRegNum As String,
                                  ByVal sAddress As String,
                                  ByVal sSuburb As String,
                                  ByVal sPostcode As String,
                                  ByVal oCountry As CCountry,
                                  ByVal oState As CState,
                                  ByVal sGender As String,
                                  ByVal sEmail As String,
                                  ByVal sEmergencyContact As String,
                                  ByVal sPhone As String,
                                  ByVal sFax As String,
                                  ByVal sMobile As String,
                                  ByVal sLeaderName As String,
                                  ByVal oSubscription As External.Revolution.CSubscription,
                                  ByVal dSignupDateStart As Date,
                                  ByVal dSignupDateEnd As Date,
                                  ByVal oUser As CUser) As DataTable

        Dim oParameters As New ArrayList

        oParameters.Add(New SqlClient.SqlParameter("@SearchBase", iSearchBase))
        oParameters.Add(New SqlClient.SqlParameter("@QuickSearch", SearchString(sQuickSearch)))
        oParameters.Add(New SqlClient.SqlParameter("@FirstName", SearchString(sFirstName)))
        oParameters.Add(New SqlClient.SqlParameter("@LastName", SearchString(sLastName)))
        oParameters.Add(New SqlClient.SqlParameter("@ChurchName", SearchString(sChurchName)))
        oParameters.Add(New SqlClient.SqlParameter("@ContactNum", SearchString(sRegNum)))
        oParameters.Add(New SqlClient.SqlParameter("@Address", SearchString(sAddress)))
        oParameters.Add(New SqlClient.SqlParameter("@Suburb", SearchString(sSuburb)))
        oParameters.Add(New SqlClient.SqlParameter("@Postcode", SearchString(sPostcode)))
        oParameters.Add(New SqlClient.SqlParameter("@Country_ID", SQLObject(oCountry)))
        oParameters.Add(New SqlClient.SqlParameter("@State_ID", SQLObject(oState)))
        oParameters.Add(New SqlClient.SqlParameter("@Gender", sGender))
        oParameters.Add(New SqlClient.SqlParameter("@Email", SearchString(sEmail)))
        oParameters.Add(New SqlClient.SqlParameter("@EmergencyContact", sEmergencyContact))
        oParameters.Add(New SqlClient.SqlParameter("@Phone", SearchString(sPhone)))
        oParameters.Add(New SqlClient.SqlParameter("@Fax", SearchString(sFax)))
        oParameters.Add(New SqlClient.SqlParameter("@Mobile", SearchString(sMobile)))
        oParameters.Add(New SqlClient.SqlParameter("@LeaderName", SearchString(sLeaderName)))
        oParameters.Add(New SqlClient.SqlParameter("@Subscription_ID", SQLObject(oSubscription)))
        oParameters.Add(New SqlClient.SqlParameter("@SignupDateStart", SQLDate(dSignupDateStart)))
        oParameters.Add(New SqlClient.SqlParameter("@SignupDateEnd", SQLDate(dSignupDateEnd)))
        oParameters.Add(New SqlClient.SqlParameter("@User_ID", SQLObject(oUser)))


        Return DataAccess.GetDataTable("sp_Common_ContactSearch", oParameters)

    End Function

    Public Function SearchFamilyMembers(ByVal iContactID As Integer, ByVal sName As String) As CArrayList(Of CContactFamily)

        Dim oArrayList As New CArrayList(Of CContactFamily)
        Dim oParameters As New ArrayList
        Dim Name As String = ""
        If sName.Contains("'") Then
            Name = sName.Replace("'", "''")
        End If

        oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", iContactID))
        oParameters.Add(New SqlClient.SqlParameter("@Name", Name))
        Dim oDataTable As DataTable = DataAccess.GetDataTable("sp_Common_ContactSearchFamilyMember", oParameters)
        Dim oDataRow As DataRow

        For Each oDataRow In oDataTable.Rows
            oArrayList.Add(New CContactFamily(oDataRow))
        Next

        Return oArrayList

    End Function

    Public Function SearchPopup(ByVal iContactID As Integer, ByVal sName As String, ByVal iMinistryID As Integer) As CArrayList(Of CSearchPopupResult)

        Dim oArrayList As New CArrayList(Of CSearchPopupResult)
        Dim oParameters As New ArrayList
        oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", iContactID))
        oParameters.Add(New SqlClient.SqlParameter("@Name", sName))
        oParameters.Add(New SqlClient.SqlParameter("@Ministry_ID", iMinistryID))

        Dim oDataTable As DataTable = DataAccess.GetDataTable("sp_Common_ContactSearchPopup", oParameters)
        Dim oDataRow As DataRow

        For Each oDataRow In oDataTable.Rows
            Dim oSearchResult As New CSearchPopupResult(oDataRow)
            oArrayList.Add(oSearchResult)
        Next

        Return oArrayList

    End Function

    Public Sub WebSessionStart(ByVal oContact As CContact, ByVal bKeepSignedIn As Boolean)

        Dim sSessionID As String = (Guid.NewGuid.ToString + Guid.NewGuid.ToString).Replace("-", "")
        Dim iSessionTimeout As Integer = IIf(bKeepSignedIn, miMinutesToKeepLoggedInLong, miMinutesToKeepLoggedIn)

        With System.Web.HttpContext.Current

            Dim oWebSession As New CWebSession
            oWebSession.ClientIP = System.Web.HttpContext.Current.Request.ServerVariables("REMOTE_ADDR")
            oWebSession.ContactID = oContact.ID
            oWebSession.SessionStartDate = Now
            oWebSession.SessionLastActivity = Now
            oWebSession.SessionID = sSessionID
            oWebSession.SessionTimeout = iSessionTimeout
            oWebSession.Save()

            .Session("WebSession") = oWebSession

            Dim oUserCookie As New System.Web.HttpCookie("PlanetshakersSession", sSessionID)
            oUserCookie.Expires = DateAdd(DateInterval.Minute, iSessionTimeout, Now)
            If Not My.Computer.Name.ToLower.Contains("josh") Then oUserCookie.Domain = "planetshakers.com"
            .Response.Cookies.Add(oUserCookie)

            'Set ASP session timeout to one day. If the user logs in after this time, the cookie
            'will re-initialise the session
            .Session.Timeout = 20
            .Session("SessionID") = sSessionID

        End With

    End Sub

    Public Sub WebSessionRetrieve()

        With System.Web.HttpContext.Current

            Dim oWebSession As CWebSession = .Session("WebSession")
            Dim iSessionTimeout As Integer = (24 * 60)

            If .Request.Cookies("PlanetshakersSession") IsNot Nothing Then

                'Check if the session needs to be re-initiated or reset (cookie "SessionID" and Session Variable "SessionID" must match)
                If .Session("SessionID") Is Nothing Then
                    .Session("SessionID") = .Request.Cookies("PlanetshakersSession").Value
                Else
                    If .Session("SessionID").ToString <> .Request.Cookies("PlanetshakersSession").Value Then
                        .Session.RemoveAll()
                    End If
                End If

                'Load the web session details in.
                If oWebSession Is Nothing Then
                    oWebSession = New CWebSession
                    If oWebSession.LoadBySessionID(System.Web.HttpContext.Current.Request.Cookies("PlanetshakersSession").Value.ToString) Then
                        .Session("WebSession") = oWebSession
                        iSessionTimeout = oWebSession.SessionTimeout
                    Else
                        'Close the session - we can't find the matching session in the database
                        WebSessionEnd()
                        Exit Sub
                    End If
                End If


                'Update the Web Session's Last Activity Date (Only once a day)
                If oWebSession IsNot Nothing Then
                    If oWebSession.SessionLastActivity.AddHours(24) < Now Then
                        oWebSession.SessionLastActivity = Now
                        oWebSession.Save()
                    End If
                End If


                'If this is a new session, then we should load the contact in.
                If .Session("Contact") Is Nothing Then
                    If oWebSession IsNot Nothing Then
                        .Session("Contact") = oWebSession.Contact
                    End If
                End If


                'Update the cookie with a new expiry date
                .Response.Cookies.Set(.Request.Cookies("PlanetshakersSession"))
                .Response.Cookies("PlanetshakersSession").Expires = DateAdd(DateInterval.Minute, iSessionTimeout, Now)

                'We need to re-set the cookie domain - it appears to 'forget' this value...
                If Not My.Computer.Name.ToLower.Contains("josh") Then .Response.Cookies("PlanetshakersSession").Domain = "planetshakers.com"

                'Set ASP session timeout to one day. If the user logs in after this time, the cookie
                'will re-initialise the session
                .Session.Timeout = 20

            Else
                'If no cookie, then kill the session (ONLY if a user is logged in)
                If .Session("Contact") IsNot Nothing Then
                    .Session.RemoveAll()
                    .Session.Abandon()
                End If
            End If

        End With

    End Sub

    Public Sub WebSessionEnd()

        With System.Web.HttpContext.Current

            If .Request.Cookies("PlanetshakersSession") IsNot Nothing Then

                'Expire the cookie
                .Response.Cookies.Remove("PlanetshakersSession")
                .Response.Cookies("PlanetshakersSession").Expires = DateAdd(DateInterval.Year, -100, Now)

                'We need to re-set the cookie domain - it appears to 'forget' this value...
                If Not My.Computer.Name.ToLower.Contains("josh") Then .Response.Cookies("PlanetshakersSession").Domain = "planetshakers.com"

                'Delete the web session
                Dim oWebSession As New CWebSession
                If oWebSession.LoadBySessionID(.Request.Cookies("PlanetshakersSession").Value.ToString) Then
                    oWebSession.Delete()
                End If

            End If

            'Remove the session details
            .Session.RemoveAll()
            .Session.Abandon()

        End With

    End Sub

    Public Sub LogSignIn(ByVal sClientIP As String,
                                ByVal sEmail As String,
                                ByVal sPassword As String,
                                ByVal bSuccess As Boolean,
                                ByVal oContact As CContact,
                                ByVal bCurrentMember As Boolean)

        Dim oParameters As New ArrayList
        oParameters.Add(New SqlClient.SqlParameter("@ClientIP", sClientIP))
        oParameters.Add(New SqlClient.SqlParameter("@Email", sEmail))
        oParameters.Add(New SqlClient.SqlParameter("@Password", sPassword))
        oParameters.Add(New SqlClient.SqlParameter("@Success", bSuccess))
        oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", SQLObject(oContact)))
        oParameters.Add(New SqlClient.SqlParameter("@CurrentMember", bCurrentMember))

        DataAccess.ExecuteCommand("sp_Revolution_SignInLogInsert", oParameters)

    End Sub

    Public Function GetNewFamilyID() As Integer

        Return DataAccess.GetValue("INSERT Common_Family(DateCreated) VALUES (dbo.GetRelativeDate()); SELECT SCOPE_IDENTITY();")

    End Function

    Public Function ValidateContactID(ByVal iContactID As Integer) As Boolean
        Return DataAccess.GetValue("SELECT COUNT(*) FROM Common_Contact WHERE Contact_ID = " & iContactID) > 0
    End Function



    Public Function GetDedupeMatchIDs() As DataTable
        Dim odata As New DataTable
        odata = DataAccess.GetDataTable("sp_Common_DedupeMatchIDs")
        Return odata
    End Function

    Public Sub DedupeIDUpdate(ByVal ContactID As Integer, ByVal MatchID As Integer)

        Dim oParameters As New ArrayList
        oParameters.Add(New SqlClient.SqlParameter("@ID", ContactID))
        oParameters.Add(New SqlClient.SqlParameter("@Match_ID", MatchID))

        DataAccess.ExecuteCommand("sp_Common_DedupeMatchIDUpdate", oParameters)

    End Sub

    Public Function CoursePaidAndNotPAid(ByVal CourseInstance_ID As Integer) As DataTable
        Dim odata As New DataTable
        Dim oParameters As New ArrayList
        oParameters.Add(New SqlClient.SqlParameter("@CourseInstance_ID", CourseInstance_ID))
        odata = DataAccess.GetDataTable("sp_Church_CourseContactPaidAndNotPaid", oParameters)
        Return odata
    End Function

    Public Sub BoomBusSetUp(ByVal EventDate As Date, ByVal ID As Integer)

        Dim oParameters As New ArrayList
        oParameters.Add(New SqlClient.SqlParameter("@Event_Date", EventDate))
        oParameters.Add(New SqlClient.SqlParameter("@TranportOption_ID", ID))

        DataAccess.ExecuteCommand("sp_SetUpBoomTransport", oParameters)

    End Sub

    Public Sub RemoveDuplicateContact(ByVal iMasterContactID As Integer, ByVal iDuplicateContactID As Integer)

        Dim oParameters As New ArrayList
        oParameters.Add(New SqlClient.SqlParameter("@MasterContact_ID", iMasterContactID))
        oParameters.Add(New SqlClient.SqlParameter("@DuplicateContact_ID", iDuplicateContactID))

        DataAccess.ExecuteCommand("sp_Common_DedupeContacts", oParameters)

    End Sub

    Public Sub RemoveDuplicateContact(ByVal iMasterContactID As Integer, ByVal iDuplicateContactID As Integer, ByVal iSelectedMatchID As Integer, ByVal iUserID As Integer)
        Try
            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@MasterContact_ID", iMasterContactID))
            oParameters.Add(New SqlClient.SqlParameter("@DuplicateContact_ID", iDuplicateContactID))

            DataAccess.ExecuteCommand("sp_Common_DedupeContacts", oParameters)

            DataAccess.ExecuteCommand("UPDATE Common_DatabaseDedupeMatch Set BeingViewed=0, ViewedAt = NULL, MatchedBy = " + iUserID.ToString() + ",
            MatchedAt = GetDate() where Match_ID = " + iSelectedMatchID.ToString() + " And Contact_ID = " + iMasterContactID.ToString() + "")

            DataAccess.ExecuteCommand("UPDATE Common_DatabaseDedupeMatch Set BeingViewed=0, ViewedAt = NULL, MatchedBy = " + iUserID.ToString() + ",
            MatchedAt = GetDate() where Match_ID = " + iSelectedMatchID.ToString() + " And Contact_ID = " + iDuplicateContactID.ToString() + "")

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Public Sub CourseContactPaymentInsert(ContactID As Integer, CourseInstanceID As Integer, PaymentTypeID As Integer, PaymentAmount As Decimal, CCNumber As String,
                                          CCExpiry As String, CCName As String, CCPhone As String, CCManual As Boolean, CCtransactionRef As String, CCRefund As Boolean,
                                          ChequeDrawer As String, ChequeBank As String, ChequeBranch As String, PaypalTransRef As String, Comment As String,
                                          PaymentDate As DateTime, PaymentByID As Integer, BankAccountID As Integer, UserID As Integer)

        Dim oParameters As New ArrayList
        oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", ContactID))
        oParameters.Add(New SqlClient.SqlParameter("@CourseInstance_ID", CourseInstanceID))
        oParameters.Add(New SqlClient.SqlParameter("@PaymentType_ID", PaymentTypeID))
        oParameters.Add(New SqlClient.SqlParameter("@PaymentAmount", PaymentAmount))
        oParameters.Add(New SqlClient.SqlParameter("@CCNumber", CCNumber))
        oParameters.Add(New SqlClient.SqlParameter("@CCExpiry", CCExpiry))
        oParameters.Add(New SqlClient.SqlParameter("@CCName", CCName))
        oParameters.Add(New SqlClient.SqlParameter("@CCPhone", CCPhone))
        oParameters.Add(New SqlClient.SqlParameter("@CCManual", CCManual))
        oParameters.Add(New SqlClient.SqlParameter("@CCTransactionRef", CCtransactionRef))
        oParameters.Add(New SqlClient.SqlParameter("@CCRefund", CCRefund))
        oParameters.Add(New SqlClient.SqlParameter("@ChequeDrawer", ChequeDrawer))
        oParameters.Add(New SqlClient.SqlParameter("@ChequeBank", ChequeBank))
        oParameters.Add(New SqlClient.SqlParameter("@ChequeBranch", ChequeBranch))
        oParameters.Add(New SqlClient.SqlParameter("@PaypalTransactionRef", PaypalTransRef))
        oParameters.Add(New SqlClient.SqlParameter("@Comment", Comment))
        oParameters.Add(New SqlClient.SqlParameter("@PaymentDate", PaymentDate))
        oParameters.Add(New SqlClient.SqlParameter("@PaymentBy_ID", PaymentByID))
        oParameters.Add(New SqlClient.SqlParameter("@BankAccount_ID", BankAccountID))
        oParameters.Add(New SqlClient.SqlParameter("@User_ID", UserID))
        DataAccess.ExecuteCommand("sp_Church_CourseContactPaymentInsert", oParameters)
    End Sub

    Public Function FindContact(ByVal Contact_ID As Integer) As Boolean
        Return DataAccess.GetValue("SELECT COUNT(*) FROM Common_ContactInternal WHERE Contact_ID = " & Contact_ID) > 0
    End Function

    Public Function CheckBoomTransport(ByVal TranspportID As Integer) As Boolean
        Return DataAccess.GetValue("SELECT COUNT(*) FROM Church_BoomDateTransportOptions WHERE TransportOption_ID = " & TranspportID) > 0
    End Function


    Public Function GetChurchAccess(ByVal Contact_ID As Integer) As Boolean
        Dim oParameters As New ArrayList
        oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", Contact_ID))
        Return DataAccess.GetValue("sp_church_RoleBasedSSO", oParameters) > 0
    End Function

    Public Function GetStaffCommentAccess(ByVal iContactID As Integer) As Boolean

        Dim oParameters As New ArrayList
        oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", iContactID))
        Return DataAccess.GetValue("sp_Church_StaffCommentAccess", oParameters) > 0

    End Function



#End Region

End Module
