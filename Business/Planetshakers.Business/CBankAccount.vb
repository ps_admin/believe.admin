
<Serializable()> _
<CLSCompliant(True)> _
Public Class CBankAccount

    Private miBankAccountID As Integer
    Private msDescription As String
    Private msClientID As String
    Private msCertificateName As String
    Private msCertificatePassPhrase As String

    Public Sub New()
        NewBankAccount()
    End Sub

    Private Sub NewBankAccount()

        miBankAccountID = 0
        msDescription = ""
        msClientID = ""
        msCertificateName = ""
        msCertificatePassPhrase = ""

    End Sub

    Public Sub New(ByRef oDataRow As DataRow)

        Me.New()

        miBankAccountID = oDataRow("BankAccount_ID")
        msDescription = oDataRow("Description")
        msClientID = oDataRow("ClientID")
        msCertificateName = oDataRow("CertificateName")
        msCertificatePassPhrase = oDataRow("CertificatePassPhrase")

    End Sub

    Public Property ID() As Integer
        Get
            Return miBankAccountID
        End Get
        Set(ByVal Value As Integer)
            miBankAccountID = Value
        End Set
    End Property
    Public Property Description() As String
        Get
            Return msDescription
        End Get
        Set(ByVal Value As String)
            msDescription = Value
        End Set
    End Property
    Public Property ClientID() As String
        Get
            Return msClientID
        End Get
        Set(ByVal Value As String)
            msClientID = Value
        End Set
    End Property
    Public Property CertificateName() As String
        Get
            Return msCertificateName
        End Get
        Set(ByVal Value As String)
            msCertificateName = Value
        End Set
    End Property
    Public Property CertificatePassPhrase() As String
        Get
            Return msCertificatePassPhrase
        End Get
        Set(ByVal Value As String)
            msCertificatePassPhrase = Value
        End Set
    End Property
    Public Overrides Function toString() As String
        Return msDescription
    End Function

End Class