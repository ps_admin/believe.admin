Imports Planetshakers.Business.Internal.Church

<Serializable()> _
<CLSCompliant(True)> _
Public Class CContact

    Private miContactID As Integer

    Private miSalutationID As Integer
    Private msFirstName As String
    Private msLastName As String
    Private msGender As String
    Private mdDateOfBirth As Date

    Private msAddress1 As String
    Private msAddress2 As String
    Private msSuburb As String
    Private msPostcode As String
    Private miStateID As Integer
    Private msStateOther As String
    Private miCountryID As Integer

    Private msPhoneHome As String
    Private msPhoneWork As String
    Private msFax As String
    Private msPhoneMobile As String

    Private msEmail As String
    Private msEmail2 As String
    Private mbDoNotIncludeEmail1InMailingList As Boolean
    Private mbDoNotIncludeEmail2InMailingList As Boolean
    Private miFamilyID As Integer
    Private miFamilyMemberTypeID As Integer
    Private msPassword As String

    Private msVolunteerPoliceCheck As String
    Private mdVolunteerPoliceCheckDate As Date
    Private msVolunteerWWCC As String
    Private mdVolunteerWWCCDate As Date
    Private miVolunteerWWCCTypeID As Integer

    Private mdCreationDate As Date
    Private miCreatedByID As Integer
    Private mdModificationDate As Date
    Private miModifiedByID As Integer

    Private moUserDetail As CUser

    Private mcMailingLists As CArrayList(Of CContactMailingList)
    Private mcMailingListTypes As CArrayList(Of CContactMailingListType)
    Private mcCombinedMailingLists As CArrayList(Of CCombinedContactMailingList)
    Private mcFamilyMembers As CArrayList(Of CContactFamily)

    Private moExternal As External.CContactExternal
    Public moInternal As Internal.CContactInternal

    Private msGUID As String

    Public mbChanged As Boolean
    Private mbStreetAddressChanged As Boolean
    Private mbHomePhoneChanged As Boolean
    Private mbMobilePhoneChanged As Boolean
    Private mcVolunteerTags As CArrayList(Of CVolunteerTag)

    Private miAnotherLanguageID As Integer
    Private miNationalityID As Integer
    Private mdLastLogin As Date

    Public Sub New()

        miContactID = 0
        miSalutationID = EnumSalutation.None 'Default to blank salutation
        msFirstName = String.Empty
        msLastName = String.Empty
        msGender = String.Empty
        mdDateOfBirth = Nothing

        msAddress1 = String.Empty
        msAddress2 = String.Empty
        msSuburb = String.Empty
        msPostcode = String.Empty
        miStateID = 0
        msStateOther = String.Empty
        miCountryID = 0

        msPhoneHome = String.Empty
        msPhoneWork = String.Empty
        msPhoneMobile = String.Empty
        msFax = String.Empty

        msEmail = String.Empty
        msEmail2 = String.Empty
        mbDoNotIncludeEmail1InMailingList = False
        mbDoNotIncludeEmail2InMailingList = False
        miFamilyID = 0
        miFamilyMemberTypeID = 0
        msPassword = String.Empty

        msVolunteerPoliceCheck = String.Empty
        mdVolunteerPoliceCheckDate = Nothing
        msVolunteerWWCC = String.Empty
        mdVolunteerWWCCDate = Nothing
        miVolunteerWWCCTypeID = 0

        mdCreationDate = Nothing
        miCreatedByID = 0
        mdModificationDate = Nothing
        miModifiedByID = 0

        moUserDetail = Nothing

        mcMailingLists = New CArrayList(Of CContactMailingList)
        mcMailingListTypes = New CArrayList(Of CContactMailingListType)
        mcCombinedMailingLists = New CArrayList(Of CCombinedContactMailingList)
        mcFamilyMembers = New CArrayList(Of CContactFamily)
        mcVolunteerTags = New CArrayList(Of CVolunteerTag)

        msGUID = System.Guid.NewGuid.ToString

        moExternal = Nothing
        moInternal = Nothing

        'Load blank Mailing List entries
        Dim oMailingList As CMailingList
        For Each oMailingList In Lists.MailingLists.Values
            Dim oContactMailingList As New CContactMailingList
            oContactMailingList.MailingListID = oMailingList.ID
            oContactMailingList.ResetChanges()
            'Only tick this list at the start if Ticked by Default and there's no 'Ticked For Campus'
            If oMailingList.TickedByDefault And oMailingList.TickedForCampusID = 0 Then oContactMailingList.SubscriptionActive = True
            mcMailingLists.Add(oContactMailingList)
        Next
        mcMailingLists.ResetChanges()

        'Load blank Mailing List Type entries
        Dim oMailingListType As CMailingListType
        For Each oMailingListType In Lists.MailingListTypes.Values
            Dim oContactMailingListType As New CContactMailingListType
            oContactMailingListType.MailingListTypeID = oMailingListType.ID
            oContactMailingListType.ResetChanges()
            mcMailingListTypes.Add(oContactMailingListType)
        Next
        mcMailingListTypes.ResetChanges()

        mbChanged = False
        mbStreetAddressChanged = False
        mbHomePhoneChanged = False
        miAnotherLanguageID = 0
        miNationalityID = 0
        mdLastLogin = Nothing
    End Sub

    Public Sub New(ByVal iContactID As Integer)
        Me.New()
        Me.LoadByID(iContactID)
    End Sub

    Public Function LoadByGUID(ByVal sGUID As String) As Boolean

        Dim oParameter As New ArrayList
        oParameter.Add(New SqlClient.SqlParameter("@GUID", sGUID))

        Return LoadByID(DataAccess.GetValue("sp_Common_ContactReadByGUID", oParameter))

    End Function
    Public Sub updateLoginDate()

        Dim oParameter As New ArrayList
        oParameter.Add(New SqlClient.SqlParameter("@Contact_ID", miContactID))
        oParameter.Add(New SqlClient.SqlParameter("@LastLoginDate", DateTime.Now))

        DataAccess.ExecuteCommand("sp_Common_ContactUpdateLoginDate", oParameter)

    End Sub

    Public Function LoadByEmail(ByVal sEmail As String) As Boolean

        Dim oParameter As New ArrayList
        oParameter.Add(New SqlClient.SqlParameter("@Email", sEmail))

        Return LoadByID(DataAccess.GetValue("sp_Common_ContactReadByEmail", oParameter))

    End Function

    Public Function LoadByID(ByVal iContactID As Integer) As Boolean

        Dim oDataSet As DataSet

        Dim oTableMappings As New ArrayList
        oTableMappings.Add(New Common.DataTableMapping("Table", "Contact"))
        'oTableMappings.Add(New Common.DataTableMapping("Table1", "ContactMailingList"))
        'oTableMappings.Add(New Common.DataTableMapping("Table2", "ContactMailingListType"))
        oTableMappings.Add(New Common.DataTableMapping("Table1", "CombinedContactMailingList"))
        oTableMappings.Add(New Common.DataTableMapping("Table2", "ContactFamily"))
        oTableMappings.Add(New Common.DataTableMapping("Table3", "UserDetails"))
        oTableMappings.Add(New Common.DataTableMapping("Table4", "DatabaseRole"))

        'oDataSet = DataAccess.GetDataSet("sp_Common_ContactRead " & iContactID.ToString, oTableMappings)

        'oDataSet.Relations.Add("ContactMailingList", oDataSet.Tables("Contact").Columns("Contact_ID"), _
        '                                      oDataSet.Tables("ContactMailingList").Columns("Contact_ID"))

        'oDataSet.Relations.Add("ContactMailingListType", oDataSet.Tables("Contact").Columns("Contact_ID"), _
        '                                      oDataSet.Tables("ContactMailingListType").Columns("Contact_ID"))

        oDataSet = DataAccess.GetDataSet("sp_Common_ContactRead2 " & iContactID.ToString, oTableMappings)

        'oDataSet.Relations.Add("ContactMailingList", oDataSet.Tables("Contact").Columns("Contact_ID"), _
        '                                      oDataSet.Tables("ContactMailingList").Columns("Contact_ID"))

        'oDataSet.Relations.Add("ContactMailingListType", oDataSet.Tables("Contact").Columns("Contact_ID"), _
        '                                      oDataSet.Tables("ContactMailingListType").Columns("Contact_ID"))

        oDataSet.Relations.Add("CombinedContactMailingList", oDataSet.Tables("Contact").Columns("Contact_ID"), _
                                              oDataSet.Tables("CombinedContactMailingList").Columns("Contact_ID"))

        oDataSet.Relations.Add("ContactFamily", oDataSet.Tables("Contact").Columns("Contact_ID"), _
                                              oDataSet.Tables("ContactFamily").Columns("Contact_ID"))

        oDataSet.Relations.Add("UserDetails", oDataSet.Tables("Contact").Columns("Contact_ID"), _
                                              oDataSet.Tables("UserDetails").Columns("Contact_ID"))

        oDataSet.Relations.Add("DatabaseRole", oDataSet.Tables("UserDetails").Columns("Contact_ID"), _
                                              oDataSet.Tables("DatabaseRole").Columns("Contact_ID"))

        If oDataSet.Tables("Contact").Rows.Count = 1 Then
            Return LoadDataRow(oDataSet.Tables("Contact").Rows(0))
        Else
            Return False
        End If

    End Function

    Public Function LoadDataRow(ByRef oDataRow As DataRow) As Boolean

        miContactID = oDataRow("Contact_ID")

        miSalutationID = oDataRow("Salutation_ID")

        msFirstName = oDataRow("FirstName")
        msLastName = oDataRow("LastName")
        msGender = oDataRow("Gender")
        mdDateOfBirth = IsNull(oDataRow("DateOfBirth"), Nothing)

        msAddress1 = oDataRow("Address1")
        msAddress2 = oDataRow("Address2")
        msSuburb = oDataRow("Suburb")
        msPostcode = oDataRow("Postcode")
        miStateID = IsNull(oDataRow("State_ID"), 0)
        msStateOther = oDataRow("StateOther")
        miCountryID = IsNull(oDataRow("Country_ID"), 0)

        msPhoneHome = oDataRow("Phone")
        msPhoneWork = oDataRow("PhoneWork")
        msPhoneMobile = oDataRow("Mobile")
        msFax = oDataRow("Fax")

        msEmail = oDataRow("Email")
        msEmail2 = oDataRow("Email2")
        mbDoNotIncludeEmail1InMailingList = oDataRow("DoNotIncludeEmail1InMailingList")
        mbDoNotIncludeEmail2InMailingList = oDataRow("DoNotIncludeEmail2InMailingList")
        miFamilyID = IsNull(oDataRow("Family_ID"), 0)
        miFamilyMemberTypeID = IsNull(oDataRow("FamilyMemberType_ID"), 0)
        msPassword = oDataRow("Password")

        msVolunteerPoliceCheck = oDataRow("VolunteerPoliceCheck")
        mdVolunteerPoliceCheckDate = IsNull(oDataRow("VolunteerPoliceCheckDate"), Nothing)
        msVolunteerWWCC = oDataRow("VolunteerWWCC")
        mdVolunteerWWCCDate = IsNull(oDataRow("VolunteerWWCCDate"), Nothing)
        miVolunteerWWCCTypeID = IsNull(oDataRow("VolunteerWWCCType_ID"), 0)

        mdCreationDate = oDataRow("CreationDate")
        miCreatedByID = oDataRow("CreatedBy_ID")
        mdModificationDate = IsNull(oDataRow("ModificationDate"), Nothing)
        miModifiedByID = IsNull(oDataRow("ModifiedBy_ID"), 0)

        msGUID = oDataRow("GUID").ToString

        If oDataRow.GetChildRows("UserDetails").Length > 0 Then
            moUserDetail = New CUser(oDataRow.GetChildRows("UserDetails")(0))
        End If

        mcMailingLists.Clear()
        Dim oMailingListRow As DataRow
        For Each oMailingListRow In oDataRow.GetChildRows("ContactMailingList")
            mcMailingLists.Add(New CContactMailingList(oMailingListRow))
        Next

        mcMailingListTypes.Clear()
        Dim oMailingListTypeRow As DataRow
        For Each oMailingListTypeRow In oDataRow.GetChildRows("ContactMailingListType")
            mcMailingListTypes.Add(New CContactMailingListType(oMailingListTypeRow))
        Next

        mcCombinedMailingLists.Clear()
        Dim oCombinedMailingListRow As DataRow
        For Each oCombinedMailingListRow In oDataRow.GetChildRows("CombinedContactMailingList")
            mcCombinedMailingLists.Add(New CCombinedContactMailingList(oCombinedMailingListRow))
        Next

        mcFamilyMembers.Clear()
        Dim oFamilyRow As DataRow
        For Each oFamilyRow In oDataRow.GetChildRows("ContactFamily")
            mcFamilyMembers.Add(New CContactFamily(oFamilyRow))
        Next

        mcMailingLists.ResetChanges()
        mcMailingListTypes.ResetChanges()
        mcFamilyMembers.ResetChanges()

        'Load the external contact information if it is present
        If oDataRow.GetChildRows("ContactExternal").Length > 0 Then
            Dim oExternalDataRow As DataRow = oDataRow.GetChildRows("ContactExternal")(0)
            moExternal = New External.CContactExternal()
            moExternal.LoadDataRow(oExternalDataRow)
        End If

        'Load the internal contact information if it is present
        If oDataRow.GetChildRows("ContactInternal").Length > 0 Then
            Dim oInternalDataRow As DataRow = oDataRow.GetChildRows("ContactInternal")(0)
            moInternal = New Internal.CContactInternal
            moInternal.LoadDataRow(oInternalDataRow)
        End If

        mbChanged = False
        mbStreetAddressChanged = False
        mbHomePhoneChanged = False
        If Not IsDBNull(oDataRow("AnotherLanguage_ID")) Then
            miAnotherLanguageID = oDataRow("AnotherLanguage_ID")

        End If
        If Not IsDBNull(oDataRow("Nationality_ID")) Then
            miNationalityID = oDataRow("Nationality_ID")

        End If
        LoadVolunteerTags()
        mdLastLogin = IsNull(oDataRow("LastLoginDate"), Nothing)
        Return True
    End Function

    Private Sub LoadVolunteerTags()
        Dim oParameters As New ArrayList
        oParameters.Add(New SqlClient.SqlParameter("@ContactID", Me.ID))
        Dim oDataTable As DataTable = Business.DataAccess.GetDataTable("sp_Events_VolunteerTagRead", oParameters)

        mcVolunteerTags = New CArrayList(Of CVolunteerTag)
        For Each oDataRow As DataRow In oDataTable.Rows
            mcVolunteerTags.Add(New CVolunteerTag(oDataRow))
        Next
    End Sub

    Public Sub Save(ByRef oUser As CUser)

        If HasInternal Then

            'Unsubscribe from Mailing List if the person's status is inactive
            If moInternal.ChurchStatusID = EnumChurchStatus.Inactive Then
                Dim oMailingListType As CContactMailingListType
                For Each oMailingListType In MailingListTypes
                    oMailingListType.SubscriptionActive = False
                Next
                Dim oMailingList As CContactMailingList
                For Each oMailingList In MailingLists
                    oMailingList.SubscriptionActive = False
                Next
                For Each oCombinedMailingList As CCombinedContactMailingList In CombinedMailingLists
                    oCombinedMailingList.SubscriptionActive = False
                    oCombinedMailingList.Save(oUser)
                Next
            End If

            If moInternal.ChurchStatusID = EnumChurchStatus.Inactive Or _
               moInternal.ChurchStatusID = EnumChurchStatus.MailingList Then

                'Remove Database Roles if a person is made inactive
                If UserDetail IsNot Nothing Then
                    Dim oDatabaseRole As CContactDatabaseRole
                    For Each oDatabaseRole In UserDetail.DatabaseRoles.Clone
                        If oDatabaseRole.DatabaseRole.ModuleName = EnumDatabaseModule.Church.ToString Then
                            UserDetail.DatabaseRoles.Remove(oDatabaseRole)
                        End If
                    Next
                End If

                'Remove Ministry & Region if person is made inactive
                moInternal.MinistryID = 0
                moInternal.RegionID = 0

            End If

        Else

            'If there is no internal, make sure they are not subscribed to internal mailing lists
            'Dim oContactMailingList As CContactMailingList
            'For Each oContactMailingList In MailingLists
            '    If oContactMailingList.MailingList.Database = EnumContactDatabase.Internal.ToString Then
            '        If oContactMailingList.SubscriptionActive Then
            '            oContactMailingList.SubscriptionActive = False
            '        End If
            '    End If
            'Next

        End If


        If mbChanged Or _
            (moExternal IsNot Nothing AndAlso moExternal.Changed) Or _
            (moInternal IsNot Nothing AndAlso moInternal.Changed) Then

            If Me.ID = 0 Then
                mdCreationDate = GetDateTime
                miCreatedByID = oUser.ID
            Else
                mdModificationDate = GetDateTime
                miModifiedByID = oUser.ID
            End If

            Dim oParameters As New ArrayList

            oParameters.Add(New SqlClient.SqlParameter("@Salutation_ID", miSalutationID))
            oParameters.Add(New SqlClient.SqlParameter("@FirstName", FirstName))
            oParameters.Add(New SqlClient.SqlParameter("@LastName", LastName))
            oParameters.Add(New SqlClient.SqlParameter("@Gender", Gender))
            oParameters.Add(New SqlClient.SqlParameter("@DateOfBirth", SQLDate(mdDateOfBirth)))
            oParameters.Add(New SqlClient.SqlParameter("@Address1", Address1))
            oParameters.Add(New SqlClient.SqlParameter("@Address2", Address2))
            oParameters.Add(New SqlClient.SqlParameter("@Suburb", Suburb))
            oParameters.Add(New SqlClient.SqlParameter("@Postcode", Postcode))
            oParameters.Add(New SqlClient.SqlParameter("@State_ID", SQLNull(miStateID)))
            oParameters.Add(New SqlClient.SqlParameter("@StateOther", StateOther))
            oParameters.Add(New SqlClient.SqlParameter("@Country_ID", SQLNull(miCountryID)))
            oParameters.Add(New SqlClient.SqlParameter("@Phone", PhoneHome))
            oParameters.Add(New SqlClient.SqlParameter("@PhoneWork", PhoneWork))
            oParameters.Add(New SqlClient.SqlParameter("@Fax", Fax))
            oParameters.Add(New SqlClient.SqlParameter("@Mobile", PhoneMobile))
            oParameters.Add(New SqlClient.SqlParameter("@Email", Email))
            oParameters.Add(New SqlClient.SqlParameter("@Email2", Email2))
            oParameters.Add(New SqlClient.SqlParameter("@DoNotIncludeEmail1InMailingList", mbDoNotIncludeEmail1InMailingList))
            oParameters.Add(New SqlClient.SqlParameter("@DoNotIncludeEmail2InMailingList", mbDoNotIncludeEmail2InMailingList))
            oParameters.Add(New SqlClient.SqlParameter("@Family_ID", SQLNull(miFamilyID)))
            oParameters.Add(New SqlClient.SqlParameter("@FamilyMemberType_ID", SQLNull(miFamilyMemberTypeID)))
            oParameters.Add(New SqlClient.SqlParameter("@Password", msPassword))
            oParameters.Add(New SqlClient.SqlParameter("@VolunteerPoliceCheck", msVolunteerPoliceCheck))
            oParameters.Add(New SqlClient.SqlParameter("@VolunteerPoliceCheckDate", SQLDate(mdVolunteerPoliceCheckDate)))
            oParameters.Add(New SqlClient.SqlParameter("@VolunteerWWCC", msVolunteerWWCC))
            oParameters.Add(New SqlClient.SqlParameter("@VolunteerWWCCDate", SQLDate(mdVolunteerWWCCDate)))
            oParameters.Add(New SqlClient.SqlParameter("@VolunteerWWCCType_ID", SQLNull(miVolunteerWWCCTypeID)))
            oParameters.Add(New SqlClient.SqlParameter("@CreationDate", SQLDateTime(mdCreationDate)))
            oParameters.Add(New SqlClient.SqlParameter("@CreatedBy_ID", SQLNull(miCreatedByID)))
            oParameters.Add(New SqlClient.SqlParameter("@ModificationDate", SQLDateTime(mdModificationDate)))
            oParameters.Add(New SqlClient.SqlParameter("@ModifiedBy_ID", SQLNull(miModifiedByID)))
            oParameters.Add(New SqlClient.SqlParameter("@GUID", msGUID))
            oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))
            oParameters.Add(New SqlClient.SqlParameter("@AnotherLanguage_ID", miAnotherLanguageID))
            oParameters.Add(New SqlClient.SqlParameter("@Nationality_ID", miNationalityID))

            If Me.ID = 0 Then
                miContactID = DataAccess.GetValue("sp_Common_ContactInsert", oParameters)
            Else
                oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", ID))
                DataAccess.ExecuteCommand("sp_Common_ContactUpdate", oParameters)
            End If

            mbChanged = False
            mbStreetAddressChanged = False
            mbHomePhoneChanged = False

        End If

        If moUserDetail IsNot Nothing Then

            'Mark user inactive if  all database roles have been removed
            moUserDetail.Inactive = (moUserDetail.DatabaseRoles.Count = 0)

            'Save the user
            moUserDetail.ID = miContactID
            moUserDetail.Save(oUser)

            'If user is new, add them to the list
            If Not Lists.Users.ContainsKey(miContactID) Then
                Lists.Users.Add(miContactID, moUserDetail)
            Else
                'If they are not new, update them in the list
                Lists.Users.Item(miContactID) = moUserDetail
            End If

            ' Check if user is still a carer
            ' 19 = Carer | 84 = NC Carer | 132 = UL Carer
            Dim isCarer = (From r In Internal.RolesCurrent _
                           Where r.RoleID = 19 Or r.RoleID = 84 Or r.RoleID = 132 _
                              Or r.Role.Name.Contains("Coach") Or r.Role.Name.Contains("Assistant") _
                           Select r).ToList()

            'If no longer a carer, make sure no contacts are being cared for by this person.
            If Not isCarer.Any Then
                Business.Internal.Church.MChurch.RemoveAllUnderCare(ID)
            End If

            ' Check if user is an NC Carer
            Dim isNCCarer = (From r In Internal.RolesCurrent _
                             Where r.RoleID = 19 Or r.RoleID = 84
                             Select r).ToList()

            'If contact is neither a 'Carer' or a 'NC Carer' remove all NCs under their care
            If Not isNCCarer.Any Then
                Business.Internal.Church.MChurch.RemoveAllNCUnderCare(ID)
            End If

        End If

        'SaveMailingLists(oUser)
        'SaveMailingListTypes(oUser)
        SaveCombinedMailingLists(oUser)
        SaveFamilyMembers()

        mcMailingLists.ResetChanges()
        mcMailingListTypes.ResetChanges()
        mcFamilyMembers.ResetChanges()

        If moExternal IsNot Nothing Then
            moExternal.ContactID = Me.ID
            moExternal.Save(oUser)
        End If
        If moInternal IsNot Nothing Then
            moInternal.ContactID = Me.ID
            moInternal.Save(Me, oUser)
        End If

    End Sub

    'Private Sub SaveMailingLists(ByVal oUser As CUser)

    '    Dim oContactMailingList As CContactMailingList

    '    For Each oContactMailingList In mcMailingLists
    '        oContactMailingList.ContactID = Me.ID
    '        oContactMailingList.Save(oUser)
    '    Next

    'End Sub

    'Private Sub SaveMailingListTypes(ByVal oUser As CUser)

    '    Dim oContactMailingListType As CContactMailingListType

    '    For Each oContactMailingListType In mcMailingListTypes
    '        oContactMailingListType.ContactID = Me.ID
    '        oContactMailingListType.Save(oUser)
    '    Next

    'End Sub

    Private Sub SaveCombinedMailingLists(ByVal oUser As CUser)

        Dim oCombinedContactMailingList As CCombinedContactMailingList

        For Each oCombinedContactMailingList In mcCombinedMailingLists
            oCombinedContactMailingList.Contact_ID = Me.ID
            oCombinedContactMailingList.Save(oUser)
        Next
    End Sub

    Private Sub SaveFamilyMembers()

        Dim oParameters As ArrayList
        Dim oContactFamily As CContactFamily

        'If we have family members now, assign an ID
        If miFamilyID = 0 And mcFamilyMembers.Count > 0 Then miFamilyID = GetNewFamilyID()


        'Update current Contact with the Family ID
        oParameters = New ArrayList
        oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", miContactID))
        oParameters.Add(New SqlClient.SqlParameter("@Family_ID", SQLNull(miFamilyID)))
        oParameters.Add(New SqlClient.SqlParameter("@FamilyMemberType_ID", SQLNull(miFamilyMemberTypeID)))
        oParameters.Add(New SqlClient.SqlParameter("@ModifiedBy_ID", SQLNull(Me.ModifiedByID)))
        DataAccess.ExecuteCommand("sp_Common_ContactUpdateFamily2", oParameters)


        'Remove the Deleted family members
        For Each oContactFamily In mcFamilyMembers.Removed
            oContactFamily.FamilyID = 0
            oContactFamily.Save(Me)
        Next

        'Assign this FamilyID to all the members and save them
        For Each oContactFamily In mcFamilyMembers
            oContactFamily.FamilyID = miFamilyID
            oContactFamily.Save(Me)

            'Update the addresses/home phone numbers is applicable
            If oContactFamily.UpdateStreetAddress Then
                oContactFamily.SaveStreetAddress(Me)
            End If
            If oContactFamily.UpdatePostalAddress Then
                oContactFamily.SavePostalAddress(Me)
            End If
            If oContactFamily.UpdateHomePhone Then
                oContactFamily.SaveHomePhone(Me)
            End If
            If oContactFamily.UpdateMobilePhone Then
                oContactFamily.SaveMobilePhone(Me)
            End If

        Next

        '"Clean up" this family - remove it if there's only 1 person left.
        If miFamilyID > 0 Then
            oParameters = New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Family_ID", miFamilyID))
            oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", miContactID))
            miFamilyID = DataAccess.GetValue("sp_Common_ContactUpdateFamilyCleanup", oParameters)
        End If

    End Sub

    Public Sub LogAccess()

        If ID > 0 Then
            DataAccess.ExecuteCommand("EXEC sp_Common_ContactLogAccess " & ID.ToString)
        End If

    End Sub

    Public Function CombinedMailingList(ByVal MailingListID As Integer, ByVal MailingListTypeID As Integer) As CCombinedContactMailingList
        For Each oCombinedMailingList As CCombinedContactMailingList In mcCombinedMailingLists
            If oCombinedMailingList.MailingList_ID = MailingListID And oCombinedMailingList.MailingListType_ID = MailingListTypeID Then
                Return oCombinedMailingList
            End If
        Next

        Return Nothing
    End Function

    Public ReadOnly Property HasExternal() As Boolean
        Get
            If moExternal Is Nothing Then
                Dim oExternal As New External.CContactExternal()
                If oExternal.LoadID(miContactID, Me) Then
                    moExternal = oExternal
                    Return True
                Else
                    Return False
                End If
            Else
                Return True
            End If
        End Get
    End Property
    Public ReadOnly Property External() As External.CContactExternal
        Get
            If moExternal Is Nothing Then
                moExternal = New External.CContactExternal()
                If miContactID > 0 Then
                    moExternal.LoadID(miContactID, Me)
                End If
            End If
            Return moExternal
        End Get
    End Property

    Public ReadOnly Property HasInternal() As Boolean
        Get
            If moInternal Is Nothing Then
                Dim oInternal As New Internal.CContactInternal
                If oInternal.LoadID(miContactID, Me) Then
                    moInternal = oInternal
                    Return True
                Else
                    Return False
                End If
            Else
                Return True
            End If
        End Get
    End Property
    Public ReadOnly Property Internal() As Internal.CContactInternal
        Get
            If moInternal Is Nothing Then
                moInternal = New Internal.CContactInternal
                If miContactID > 0 Then
                    moInternal.LoadID(miContactID, Me)
                End If
            End If
            Return moInternal
        End Get
    End Property

    Public ReadOnly Property MailingLists() As CArrayList(Of CContactMailingList)
        Get
            Return mcMailingLists
        End Get
    End Property
    Public ReadOnly Property MailingListTypes() As CArrayList(Of CContactMailingListType)
        Get
            Return mcMailingListTypes
        End Get
    End Property
    Public ReadOnly Property CombinedMailingLists() As CArrayList(Of CCombinedContactMailingList)
        Get
            Return mcCombinedMailingLists
        End Get
    End Property
    Public ReadOnly Property FamilyMembers() As CArrayList(Of CContactFamily)
        Get
            If mcFamilyMembers IsNot Nothing Then
                If mcFamilyMembers.Count < 0 Then
                    Dim oParameters As New ArrayList
                    oParameters.Add(New SqlClient.SqlParameter("@Family_ID", FamilyID))

                    Dim oDataTable As DataTable = DataAccess.GetDataTable("sp_Common_FamilyMembersSelect", oParameters)
                    For Each oDataRow As DataRow In oDataTable.Rows
                        If oDataRow("FamilyMember_ID") <> Me.ID Then mcFamilyMembers.Add(New CContactFamily(oDataRow))
                    Next
                End If
            End If
            Return mcFamilyMembers
        End Get
    End Property

    Public Function HistoryRead(ByVal iModule As EnumContactDatabase) As DataTable

        Dim oParameters As New ArrayList
        oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", miContactID))
        oParameters.Add(New SqlClient.SqlParameter("@Module", iModule.ToString))

        Return DataAccess.GetDataTable("sp_Common_ContactHistoryRead", oParameters)

    End Function

    Public Sub SetMailingListAllInactive()

        Dim oParameters As New ArrayList
        oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", miContactID))

        DataAccess.ExecuteCommand("sp_Common_CombinedContactMailingListSetAllInactive", oParameters)
    End Sub

    Public Property ID() As Integer
        Get
            Return miContactID
        End Get
        Set(ByVal value As Integer)
            miContactID = value
        End Set
    End Property
    Public Property SalutationID() As Integer
        Get
            Return miSalutationID
        End Get
        Set(ByVal Value As Integer)
            If miSalutationID <> Value Then mbChanged = True
            miSalutationID = Value
        End Set
    End Property
    Public ReadOnly Property Salutation() As CSalutation
        Get
            Return Lists.GetSalutationByID(miSalutationID)
        End Get
    End Property
    Public Property FirstName() As String
        Get
            Return msFirstName
        End Get
        Set(ByVal Value As String)
            If msFirstName <> Value Then mbChanged = True
            msFirstName = Value
        End Set
    End Property
    Public Property LastName() As String
        Get
            Return msLastName
        End Get
        Set(ByVal Value As String)
            If msLastName <> Value Then mbChanged = True
            msLastName = Value
        End Set
    End Property
    Public Property Gender() As String
        Get
            Return msGender
        End Get
        Set(ByVal Value As String)
            If msGender <> Value Then mbChanged = True
            msGender = Value
        End Set
    End Property
    Public Property DateOfBirth() As Date
        Get
            Return mdDateOfBirth
        End Get
        Set(ByVal Value As Date)
            If mdDateOfBirth <> Value Then mbChanged = True
            mdDateOfBirth = Value
        End Set
    End Property
    Public ReadOnly Property IsUnder18() As Boolean
        Get
            If mdDateOfBirth <> Nothing Then
                Return DateAdd(DateInterval.Year, 18, mdDateOfBirth) > GetDateTime
            Else
                Return False
            End If
        End Get
    End Property
    Public Property Address1() As String
        Get
            Return msAddress1
        End Get
        Set(ByVal Value As String)
            If msAddress1 <> Value Then mbChanged = True : mbStreetAddressChanged = True
            msAddress1 = Value
        End Set
    End Property
    Public Property Address2() As String
        Get
            Return msAddress2
        End Get
        Set(ByVal Value As String)
            If msAddress2 <> Value Then mbChanged = True : mbStreetAddressChanged = True
            msAddress2 = Value
        End Set
    End Property
    Public Property Suburb() As String
        Get
            Return msSuburb
        End Get
        Set(ByVal Value As String)
            If msSuburb <> Value Then mbChanged = True : mbStreetAddressChanged = True
            msSuburb = Value.ToUpper
        End Set
    End Property
    Public Property Postcode() As String
        Get
            Return msPostcode
        End Get
        Set(ByVal Value As String)
            If msPostcode <> Value Then mbChanged = True : mbStreetAddressChanged = True
            msPostcode = Value
        End Set
    End Property
    Public Property StateID() As Integer
        Get
            Return miStateID
        End Get
        Set(ByVal Value As Integer)
            If miStateID <> Value Then mbChanged = True : mbStreetAddressChanged = True
            miStateID = Value
        End Set
    End Property
    Public ReadOnly Property State() As CState
        Get
            Return Lists.GetStateByID(miStateID)
        End Get
    End Property
    Public Property StateOther() As String
        Get
            Return msStateOther
        End Get
        Set(ByVal Value As String)
            If msStateOther <> Value Then mbChanged = True : mbStreetAddressChanged = True
            msStateOther = Value
        End Set
    End Property
    Public ReadOnly Property StateText() As String
        Get
            If State IsNot Nothing Then
                If State.Name.ToLower <> "other" Then
                    Return State.Name
                Else
                    Return StateOther
                End If
            Else
                Return String.Empty
            End If
        End Get
    End Property
    Public Property CountryID() As Integer
        Get
            Return miCountryID
        End Get
        Set(ByVal Value As Integer)
            If miCountryID <> Value Then mbChanged = True : mbStreetAddressChanged = True
            miCountryID = Value
        End Set
    End Property
    Public ReadOnly Property Country() As CCountry
        Get
            Return Lists.GetCountryByID(miCountryID)
        End Get
    End Property
    Public ReadOnly Property CountryText() As String
        Get
            If Country IsNot Nothing Then
                Return Country.Name
            Else
                Return String.Empty
            End If
        End Get
    End Property
    Public Property PhoneHome() As String
        Get
            Return msPhoneHome
        End Get
        Set(ByVal Value As String)
            If msPhoneHome <> Value Then mbChanged = True : mbHomePhoneChanged = True
            msPhoneHome = Value
        End Set
    End Property
    Public Property PhoneWork() As String
        Get
            Return msPhoneWork
        End Get
        Set(ByVal Value As String)
            If msPhoneWork <> Value Then mbChanged = True
            msPhoneWork = Value
        End Set
    End Property
    Public Property PhoneMobile() As String
        Get
            Return msPhoneMobile
        End Get
        Set(ByVal Value As String)
            If msPhoneMobile <> Value Then mbChanged = True : mbMobilePhoneChanged = True
            msPhoneMobile = Value
        End Set
    End Property
    Public Property Fax() As String
        Get
            Return msFax
        End Get
        Set(ByVal Value As String)
            If msFax <> Value Then mbChanged = True
            msFax = Value
        End Set
    End Property
    Public Property Email() As String
        Get
            Return msEmail
        End Get
        Set(ByVal Value As String)
            If msEmail <> Value Then mbChanged = True
            msEmail = Value
        End Set
    End Property
    Public Property Email2() As String
        Get
            Return msEmail2
        End Get
        Set(ByVal Value As String)
            If msEmail2 <> Value Then mbChanged = True
            msEmail2 = Value
        End Set
    End Property
    Public Property DoNotIncludeEmail1InMailingList() As Boolean
        Get
            Return mbDoNotIncludeEmail1InMailingList
        End Get
        Set(ByVal Value As Boolean)
            If mbDoNotIncludeEmail1InMailingList <> Value Then mbChanged = True
            mbDoNotIncludeEmail1InMailingList = Value
        End Set
    End Property
    Public Property DoNotIncludeEmail2InMailingList() As Boolean
        Get
            Return mbDoNotIncludeEmail2InMailingList
        End Get
        Set(ByVal Value As Boolean)
            If mbDoNotIncludeEmail2InMailingList <> Value Then mbChanged = True
            mbDoNotIncludeEmail2InMailingList = Value
        End Set
    End Property
    Public Property FamilyID() As Integer
        Get
            Return miFamilyID
        End Get
        Set(ByVal Value As Integer)
            If miFamilyID <> Value Then mbChanged = True
            miFamilyID = Value
        End Set
    End Property
    Public Property FamilyMemberTypeID() As Integer
        Get
            Return miFamilyMemberTypeID
        End Get
        Set(ByVal Value As Integer)
            If miFamilyMemberTypeID <> Value Then mbChanged = True
            miFamilyMemberTypeID = Value
        End Set
    End Property
    Public ReadOnly Property FamilyMemberType() As CFamilyMemberType
        Get
            Return Lists.GetFamilyMemberTypeByID(miFamilyMemberTypeID)
        End Get
    End Property
    Public Property Password() As String
        Get
            Return msPassword
        End Get
        Set(ByVal Value As String)
            If msPassword <> Value Then mbChanged = True
            msPassword = Value
        End Set
    End Property
    Public Property VolunteerPoliceCheck() As String
        Get
            Return msVolunteerPoliceCheck
        End Get
        Set(ByVal value As String)
            If msVolunteerPoliceCheck <> value Then mbChanged = True
            msVolunteerPoliceCheck = value
        End Set
    End Property
    Public Property VolunteerPoliceCheckDate() As Date
        Get
            Return mdVolunteerPoliceCheckDate
        End Get
        Set(ByVal value As Date)
            If mdVolunteerPoliceCheckDate <> value Then mbChanged = True
            mdVolunteerPoliceCheckDate = value
        End Set
    End Property
    Public Property VolunteerWWCC() As String
        Get
            Return msVolunteerWWCC
        End Get
        Set(ByVal value As String)
            If msVolunteerWWCC <> value Then mbChanged = True
            msVolunteerWWCC = value
        End Set
    End Property
    Public Property VolunteerWWCCDate() As Date
        Get
            Return mdVolunteerWWCCDate
        End Get
        Set(ByVal value As Date)
            If mdVolunteerWWCCDate <> value Then mbChanged = True
            mdVolunteerWWCCDate = value
        End Set
    End Property
    Public Property VolunteerWWCCTypeID() As Integer
        Get
            Return miVolunteerWWCCTypeID
        End Get
        Set(ByVal value As Integer)
            If miVolunteerWWCCTypeID <> value Then mbChanged = True
            miVolunteerWWCCTypeID = value
        End Set
    End Property
    Public ReadOnly Property VolunteerWWCCType() As CWWCCType
        Get
            Return Lists.GetWWCCTypeByID(miVolunteerWWCCTypeID)
        End Get
    End Property
    Public Property CreationDate() As Date
        Get
            Return mdCreationDate
        End Get
        Set(ByVal value As Date)
            If mdCreationDate <> value Then mbChanged = True
            mdCreationDate = value
        End Set
    End Property
    Public Property CreatedByID() As Integer
        Get
            Return miCreatedByID
        End Get
        Set(ByVal Value As Integer)
            If miCreatedByID <> Value Then mbChanged = True
            miCreatedByID = Value
        End Set
    End Property
    Public ReadOnly Property CreatedBy() As CUser
        Get
            Return Lists.GetUserByID(miCreatedByID)
        End Get
    End Property
    Public Property ModificationDate() As Date
        Get
            Return mdModificationDate
        End Get
        Set(ByVal value As Date)
            If mdModificationDate <> value Then mbChanged = True
            mdModificationDate = value
        End Set
    End Property
    Public Property ModifiedByID() As Integer
        Get
            Return miModifiedByID
        End Get
        Set(ByVal Value As Integer)
            If miModifiedByID <> Value Then mbChanged = True
            miModifiedByID = Value
        End Set
    End Property
    Public ReadOnly Property ModifiedBy() As CUser
        Get
            Return Lists.GetUserByID(miModifiedByID)
        End Get
    End Property
    Public Property UserDetail() As CUser
        Get
            Return moUserDetail
        End Get
        Set(ByVal value As CUser)
            If ObjectID(moUserDetail) <> ObjectID(value) Then mbChanged = True
            moUserDetail = value
        End Set
    End Property
    Public Property GUID() As String
        Get
            Return msGUID
        End Get
        Set(ByVal value As String)
            If msGUID <> value Then mbChanged = True
            msGUID = value
        End Set
    End Property

    Public ReadOnly Property Name() As String
        Get
            Return IIf(Me.FirstName <> "", Me.FirstName & " ", "") & _
                   IIf(Me.LastName <> "", Me.LastName, "")
        End Get
    End Property
    Public ReadOnly Property FullName() As String
        Get
            Return IIf(Me.Salutation.Name <> "", Me.Salutation.Name & " ", "") & _
                   IIf(Me.FirstName <> "", Me.FirstName & " ", "") & _
                   IIf(Me.LastName <> "", Me.LastName, "")
        End Get
    End Property
    Public ReadOnly Property ContactAddress() As String
        Get
            Dim oString As New Text.StringBuilder
            oString.Append(IIf(Me.Address1 <> "", Me.Address1 & vbCrLf, "") & _
                           IIf(Me.Address2 <> "", Me.Address2 & vbCrLf, "") & _
                           IIf(Me.Suburb <> "", Me.Suburb & " ", ""))
            If Me.State IsNot Nothing Then
                oString.Append(IIf(Me.State.Name.Contains("Other"), IIf(Me.StateOther <> "", Me.StateOther & " ", ""), Me.State.Name & " "))
            End If
            oString.Append(IIf(Me.Postcode <> "", Me.Postcode, "") & vbCrLf)
            If Me.Country IsNot Nothing Then
                oString.Append(IIf(Me.Country.Name <> "", Me.Country.Name, ""))
            End If
            Return oString.ToString
        End Get
    End Property
    Public Overrides Function toString() As String
        Return Name
    End Function

    Public Property Changed() As Boolean
        Get
            If mbChanged Then Return True

            If mcMailingLists.Changed Then Return True
            If mcMailingListTypes.Changed Then Return True
            If mcFamilyMembers.Changed Then Return True
            If moUserDetail IsNot Nothing AndAlso moUserDetail.Changed Then Return True

            Dim oContactMailingList As CContactMailingList
            For Each oContactMailingList In mcMailingLists
                If oContactMailingList.Changed Then Return True
            Next
            Dim oContactMailingListType As CContactMailingListType
            For Each oContactMailingListType In mcMailingListTypes
                If oContactMailingListType.Changed Then Return True
            Next
            'Dim oContactFamily As CContactFamily
            'For Each oContactFamily In mcFamilyMembers
            '    If oContactFamily.Changed Then Return True
            'Next

            If moExternal IsNot Nothing Then
                If moExternal.Changed Then Return True
            End If
            If moInternal IsNot Nothing Then
                If moInternal.Changed Then Return True
            End If

            Return False

        End Get
        Set(ByVal value As Boolean)
            mbChanged = value
        End Set
    End Property

    Public ReadOnly Property VolunteerTags() As CArrayList(Of CVolunteerTag)
        Get
            Return mcVolunteerTags
        End Get
    End Property

    Public Sub ResetChanges()

        mbChanged = False

        mcMailingLists.ResetChanges()
        mcMailingListTypes.ResetChanges()
        mcFamilyMembers.ResetChanges()
        moUserDetail.ResetChanges()

        Dim oContactMailingList As CContactMailingList
        For Each oContactMailingList In mcMailingLists
            oContactMailingList.ResetChanges()
        Next
        Dim oContactMailingListType As CContactMailingListType
        For Each oContactMailingListType In mcMailingListTypes
            oContactMailingListType.ResetChanges()
        Next
        'Dim oContactFamily As CContactFamily
        'For Each oContactFamily In mcFamilyMembers
        '   oContactFamily.ResetChanges
        'Next

        If moExternal IsNot Nothing Then
            moExternal.ResetChanges()
        End If
        If moInternal IsNot Nothing Then
            moInternal.ResetChanges()
        End If

    End Sub

    Public ReadOnly Property StreetAddressChanged() As Boolean
        Get
            Return mbStreetAddressChanged
        End Get
    End Property

    Public ReadOnly Property HomePhoneChanged() As Boolean
        Get
            Return mbHomePhoneChanged
        End Get
    End Property
    Public ReadOnly Property MobilePhoneChanged() As Boolean
        Get
            Return mbMobilePhoneChanged
        End Get
    End Property
    Public Property AnotherLanguageID() As Integer
        Get
            Return miAnotherLanguageID
        End Get
        Set(ByVal Value As Integer)
            If miAnotherLanguageID <> Value Then mbChanged = True
            miAnotherLanguageID = Value
        End Set
    End Property
    Public Property NationalityID() As Integer
        Get
            Return miNationalityID
        End Get
        Set(ByVal Value As Integer)
            If miNationalityID <> Value Then mbChanged = True
            miNationalityID = Value
        End Set
    End Property
    Public Property LastLoginDate() As Date
        Get
            Return mdLastLogin
        End Get
        Set(ByVal value As Date)
            mdLastLogin = value
        End Set
    End Property
End Class
