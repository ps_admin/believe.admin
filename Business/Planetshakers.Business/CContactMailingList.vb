
<Serializable()> _
<CLSCompliant(True)> _
Public Class CContactMailingList

    Private miContactMailingListID As Integer
    Private miContactID As Integer
    Private miMailingListID As Integer
    Private mbSubscriptionActive As Boolean
    Private mbChanged As Boolean

    Public Sub New()

        miContactMailingListID = 0
        miContactID = 0
        miMailingListID = 0
        mbSubscriptionActive = False
        mbChanged = False

    End Sub

    Public Sub New(ByRef oDataRow As DataRow)

        Me.New()

        miContactMailingListID = oDataRow("ContactMailingList_ID")
        miContactID = oDataRow("Contact_ID")
        miMailingListID = oDataRow("MailingList_ID")
        mbSubscriptionActive = oDataRow("SubscriptionActive")
        mbChanged = False

    End Sub

    Public Sub Save(ByVal oUser As CUser)

        If mbChanged Then

            Dim oParameters As New ArrayList

            oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", miContactID))
            oParameters.Add(New SqlClient.SqlParameter("@MailingList_ID", miMailingListID))
            oParameters.Add(New SqlClient.SqlParameter("@SubscriptionActive", mbSubscriptionActive))
            oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))
            oParameters.Add(New SqlClient.SqlParameter("@ContactMailingList_ID", ID))
            DataAccess.ExecuteCommand("sp_Common_ContactMailingListUpdate", oParameters)

            mbChanged = False

        End If

    End Sub

    Public ReadOnly Property ID() As Integer
        Get
            Return miContactMailingListID
        End Get
    End Property
    Public Property ContactID() As Integer
        Get
            Return miContactID
        End Get
        Set(ByVal Value As Integer)
            If miContactID <> Value Then mbChanged = True
            miContactID = Value
        End Set
    End Property
    Public Property MailingListID() As Integer
        Get
            Return miMailingListID
        End Get
        Set(ByVal Value As Integer)
            If miMailingListID <> Value Then mbChanged = True
            miMailingListID = Value
        End Set
    End Property
    Public ReadOnly Property MailingList() As CMailingList
        Get
            Return Lists.GetMailingListByID(miMailingListID)
        End Get
    End Property
    Public Property SubscriptionActive() As Boolean
        Get
            Return mbSubscriptionActive
        End Get
        Set(ByVal Value As Boolean)
            If mbSubscriptionActive <> Value Then mbChanged = True
            mbSubscriptionActive = Value
        End Set
    End Property
    Public ReadOnly Property Changed() As Boolean
        Get
            Return mbChanged
        End Get
    End Property
    Public Sub ResetChanges()
        mbChanged = False
    End Sub

End Class