
<Serializable()> _
<CLSCompliant(True)> _
Public Class CContactMailingListType

    Private miContactMailingListTypeID As Integer
    Private miContactID As Integer
    Private miMailingListTypeID As Integer
    Private mbSubscriptionActive As Boolean
    Private mbChanged As Boolean

    Public Sub New()

        miContactMailingListTypeID = 0
        miContactID = 0
        miMailingListTypeID = 0
        mbSubscriptionActive = False
        mbChanged = False

    End Sub

    Public Sub New(ByRef oDataRow As DataRow)

        Me.New()

        miContactMailingListTypeID = oDataRow("ContactMailingListType_ID")
        miContactID = oDataRow("Contact_ID")
        miMailingListTypeID = oDataRow("MailingListType_ID")
        mbSubscriptionActive = oDataRow("SubscriptionActive")
        mbChanged = False

    End Sub

    Public Sub Save(ByVal oUser As CUser)

        If mbChanged Then

            Dim oParameters As New ArrayList

            oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", miContactID))
            oParameters.Add(New SqlClient.SqlParameter("@MailingListType_ID", miMailingListTypeID))
            oParameters.Add(New SqlClient.SqlParameter("@SubscriptionActive", mbSubscriptionActive))
            oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))

            If Me.ID = 0 Then
                miContactMailingListTypeID = DataAccess.GetValue("sp_Common_ContactMailingListTypeInsert", oParameters)
            Else
                oParameters.Add(New SqlClient.SqlParameter("@ContactMailingListType_ID", ID))
                DataAccess.ExecuteCommand("sp_Common_ContactMailingListTypeUpdate", oParameters)
            End If

            mbChanged = False

        End If

    End Sub

    Public ReadOnly Property ID() As Integer
        Get
            Return miContactMailingListTypeID
        End Get
    End Property
    Public Property ContactID() As Integer
        Get
            Return miContactID
        End Get
        Set(ByVal Value As Integer)
            If miContactID <> Value Then mbChanged = True
            miContactID = Value
        End Set
    End Property
    Public Property MailingListTypeID() As Integer
        Get
            Return miMailingListTypeID
        End Get
        Set(ByVal Value As Integer)
            If miMailingListTypeID <> Value Then mbChanged = True
            miMailingListTypeID = Value
        End Set
    End Property
    Public ReadOnly Property MailingListType() As CMailingListType
        Get
            Return Lists.GetMailingListTypeByID(miMailingListTypeID)
        End Get
    End Property
    Public Property SubscriptionActive() As Boolean
        Get
            Return mbSubscriptionActive
        End Get
        Set(ByVal Value As Boolean)
            If mbSubscriptionActive <> Value Then mbChanged = True
            mbSubscriptionActive = Value
        End Set
    End Property
    Public ReadOnly Property Changed() As Boolean
        Get
            Return mbChanged
        End Get
    End Property
    Public Sub ResetChanges()
        mbChanged = False
    End Sub

End Class