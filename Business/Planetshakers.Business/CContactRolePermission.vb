﻿<Serializable()> _
<CLSCompliant(True)> _
Public Class CContactRolePermission
    Inherits CPermission
    Implements IComparable(Of CContactRolePermission)

#Region "Private Fields"
    Private miID As Integer
#End Region

#Region "Constructors"
    Public Sub New()

        MyBase.New()
        miID = 0

    End Sub

    Public Sub New(ByRef oDataRow As DataRow)

        Me.New()
        miID = oDataRow("ID")

    End Sub
#End Region

    Public Sub Save(ByVal iContactID As Integer, ByRef oUser As CUser)

        If MyBase.mbChanged Then
            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))
            oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", iContactID))
            oParameters.Add(New SqlClient.SqlParameter("@DatabaseObject_ID", MyBase.miDatabaseObjectID))
            oParameters.Add(New SqlClient.SqlParameter("@Read", MyBase.mbRead))
            oParameters.Add(New SqlClient.SqlParameter("@Write", MyBase.mbWrite))

            If DatabaseObjectID = 172 Then
                Dim i As Integer = 1
            End If

            If miID = 0 Then
                miID = DataAccess.GetValue("sp_Common_ContactPermissions_Insert", oParameters)
            Else
                oParameters.Add(New SqlClient.SqlParameter("@ID", miID))
                DataAccess.ExecuteCommand("sp_Common_ContactPermissions_Update", oParameters)
            End If
        End If

    End Sub

#Region "Public Properties"
    Public ReadOnly Property ID() As Integer
        Get
            Return miID
        End Get
    End Property
#End Region

    Public Overrides Function ToString() As String
        Return MyBase.DatabaseObject.DatabaseObjectName
    End Function

    Public Function CompareTo(ByVal other As CContactRolePermission) As Integer Implements System.IComparable(Of CContactRolePermission).CompareTo
        Return String.Compare(Me.DatabaseObjectID, other.DatabaseObjectID)
    End Function
End Class