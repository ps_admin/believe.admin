
<Serializable()> _
<CLSCompliant(True)> _
Public Class CCountry

    Private miCountryID As Integer
    Private msName As String
    Private msCurrency As String
    Private msNumeric As String
    Private msAlpha2 As String
    Private msAlpha3 As String
    Private miSortOrder As Integer

    Public Sub New()

        miCountryID = 0
        msName = ""
        msCurrency = ""
        msNumeric = ""
        msAlpha2 = ""
        msAlpha3 = ""
        miSortOrder = 0

    End Sub

    Public Sub New(ByRef oDataRow As DataRow)

        Me.New()

        miCountryID = oDataRow("Country_ID")
        msName = oDataRow("Country")
        msCurrency = oDataRow("Currency")
        msNumeric = oDataRow("Numeric")
        msAlpha2 = oDataRow("Alpha-2")
        msAlpha3 = oDataRow("Alpha-3")
        miSortOrder = oDataRow("SortOrder")

    End Sub

    Public ReadOnly Property ID() As Integer
        Get
            Return miCountryID
        End Get
    End Property

    Public Property Name() As String
        Get
            Return msName
        End Get
        Set(ByVal Value As String)
            msName = Value
        End Set
    End Property
    Public Property Currency() As String
        Get
            Return msCurrency
        End Get
        Set(ByVal Value As String)
            msCurrency = Value
        End Set
    End Property
    Public Property Numeric() As String
        Get
            Return msNumeric
        End Get
        Set(ByVal Value As String)
            msNumeric = Value
        End Set
    End Property
    Public Property Alpha2() As String
        Get
            Return msAlpha2
        End Get
        Set(ByVal Value As String)
            msAlpha2 = Value
        End Set
    End Property
    Public Property Alpha3() As String
        Get
            Return msAlpha3
        End Get
        Set(ByVal Value As String)
            msAlpha3 = Value
        End Set
    End Property
    Public Property SortOrder() As Integer
        Get
            Return miSortOrder
        End Get
        Set(ByVal value As Integer)
            miSortOrder = value
        End Set
    End Property

    Public Overrides Function toString() As String
        Return msName
    End Function

End Class