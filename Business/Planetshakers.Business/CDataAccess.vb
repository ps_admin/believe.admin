
<CLSCompliant(True)> _
Public MustInherit Class CDataAccess

    Public Sub New()
    End Sub

    Public MustOverride Function GetDataSet(ByVal sSQL As String, ByRef oTableMappings As ArrayList) As DataSet
    Public MustOverride Function GetDataSet(ByVal sSQL As String, ByRef oTableMappings As ArrayList, ByRef oParameters As ArrayList) As DataSet
    Public MustOverride Function GetDataTable(ByVal sSQL As String) As DataTable
    Public MustOverride Function GetDataTable(ByVal sSQL As String, ByRef oParameters As ArrayList) As DataTable

    Public Function GetDataRow(ByVal sSQL As String) As DataRow
        Dim oDataTable As DataTable = GetDataTable(sSQL)
        If oDataTable IsNot Nothing AndAlso oDataTable.Rows.Count > 0 Then
            Return oDataTable.Rows(0)
        Else
            Return Nothing
        End If
    End Function

    Public MustOverride Function GetValue(ByVal sSQL As String) As Object
    Public MustOverride Function GetValue(ByVal sSQL As String, ByRef sSQLParameters As ArrayList) As Object
    Public MustOverride Sub ExecuteCommand(ByVal sSQL As String)
    Public MustOverride Sub ExecuteCommand(ByVal sSQL As String, ByRef sSQLParameters As ArrayList)
    Public MustOverride Function SendEmail(ByVal sToAddress As String, ByVal sToName As String, ByVal sSubject As String, ByVal sBody As String, ByVal oAttachments As ArrayList, ByRef sError As String) As Boolean
    Public MustOverride Function ProcessCreditCard(ByVal sCard As String, ByVal sExpiry As String, ByVal sCVC2 As String, ByVal sName As String, ByVal dAmount As Decimal, ByRef sComment As String, ByRef sBankAccountClientID As String, ByRef sBankAccountCertificateName As String, ByRef sBankAccountCertificatePassPhrase As String, ByRef sTxRef As String, ByRef sResponse As String) As Boolean
    Public MustOverride Function ProcessRefund(ByVal sName As String, ByVal sCCTransactionRef As String, ByVal dAmount As Decimal, ByRef sComment As String, ByRef sBankAccountClientID As String, ByRef sBankAccountCertificateName As String, ByRef sBankAccountCertificatePassPhrase As String, ByRef sTxRef As String, ByRef sResponse As String) As Boolean
    Public MustOverride Property ConnectionString() As String
    Public MustOverride WriteOnly Property UserID As Integer


End Class