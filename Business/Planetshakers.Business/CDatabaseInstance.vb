﻿Public Class CDatabaseOptions

    Private miDatabaseInstance As Integer
    Private msDefaultEmailAddress As String
    Private miTimeDifference As Integer

    Public Sub New(ByVal oDataRow As DataRow)

        Select Case oDataRow("DatabaseInstance")
            Case "AU"
                miDatabaseInstance = EnumDatabaseInstance.AU
            Case "SA"
                miDatabaseInstance = EnumDatabaseInstance.SA
            Case "LA"
                miDatabaseInstance = EnumDatabaseInstance.LA
            Case "TX"
                miDatabaseInstance = EnumDatabaseInstance.TX
            Case "SG"
                miDatabaseInstance = EnumDatabaseInstance.SG
            Case "CH"
                miDatabaseInstance = EnumDatabaseInstance.CH


        End Select
        DefaultEmailAddress = oDataRow("DefaultEmailAddress")
        TimeDifference = oDataRow("TimeDifference")

    End Sub

    Public Sub Save()

        Dim oParameters As New ArrayList
        oParameters.Add(New SqlClient.SqlParameter("@DefaultEmailAddress", DefaultEmailAddress))
        oParameters.Add(New SqlClient.SqlParameter("@TimeDifference", TimeDifference))

        DataAccess.ExecuteCommand("sp_Common_DatabaseOptionsUpdate", oParameters)

    End Sub

    Public Property DatabaseInstance() As EnumDatabaseInstance
        Get
            Return miDatabaseInstance
        End Get
        Set(ByVal value As EnumDatabaseInstance)
            miDatabaseInstance = value
        End Set
    End Property

    Public Property DefaultEmailAddress() As String
        Get
            Return msDefaultEmailAddress
        End Get
        Set(ByVal value As String)
            msDefaultEmailAddress = value
        End Set
    End Property

    Public Property TimeDifference() As Integer
        Get
            Return miTimeDifference
        End Get
        Set(ByVal value As Integer)
            miTimeDifference = value
        End Set
    End Property

End Class
