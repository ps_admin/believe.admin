
<Serializable()> _
<CLSCompliant(True)> _
Public Class CDatabaseObject

    Private miDatabaseObjectID As Integer
    Private msModule As String
    Private miObjectType As EnumDatabaseObjectType
    Private msDatabaseObjectName As String
    Private msDatabaseObjectText As String
    Private msDatabaseObjectDesc As String
    Private miParentObjectID As Integer
    Private miSortOrder As Integer

    Public Sub New()

        miDatabaseObjectID = 0
        msModule = String.Empty
        miObjectType = 0
        msDatabaseObjectName = String.Empty
        msDatabaseObjectText = String.Empty
        msDatabaseObjectDesc = String.Empty
        miSortOrder = 0

    End Sub

    Public Sub New(ByRef oDataRow As DataRow)

        Me.New()

        miDatabaseObjectID = oDataRow("DatabaseObject_ID")
        msModule = oDataRow("Module")
        miObjectType = oDataRow("ObjectType")
        msDatabaseObjectName = oDataRow("DatabaseObjectName")
        msDatabaseObjectText = oDataRow("DatabaseObjectText")
        msDatabaseObjectDesc = oDataRow("DatabaseObjectDesc")
        miParentObjectID = IIf(IsDBNull(oDataRow("ParentObject_ID")), 0, oDataRow("ParentObject_ID"))
        miSortOrder = oDataRow("Order")

    End Sub

    Public ReadOnly Property ID() As Integer
        Get
            Return miDatabaseObjectID
        End Get
    End Property
    Public Property ModuleName() As String
        Get
            Return msModule
        End Get
        Set(ByVal Value As String)
            msModule = Value
        End Set
    End Property
    Public Property ObjectType() As EnumDatabaseObjectType
        Get
            Return miObjectType
        End Get
        Set(ByVal Value As EnumDatabaseObjectType)
            miObjectType = Value
        End Set
    End Property
    Public Property DatabaseObjectName() As String
        Get
            Return msDatabaseObjectName
        End Get
        Set(ByVal Value As String)
            msDatabaseObjectName = Value
        End Set
    End Property
    Public Property DatabaseObjectText() As String
        Get
            Return msDatabaseObjectText
        End Get
        Set(ByVal Value As String)
            msDatabaseObjectText = Value
        End Set
    End Property
    Public Property DatabaseObjectDesc() As String
        Get
            Return msDatabaseObjectDesc
        End Get
        Set(ByVal Value As String)
            msDatabaseObjectDesc = Value
        End Set
    End Property
    Public Property SortOrder() As Integer
        Get
            Return miSortOrder
        End Get
        Set(ByVal Value As Integer)
            miSortOrder = Value
        End Set
    End Property

    Public Overrides Function ToString() As String
        Return DatabaseObjectText
    End Function

End Class