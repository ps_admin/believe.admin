
<Serializable()> _
<CLSCompliant(True)> _
Public Class CDatabaseRole

    Private miDatabaseRoleID As Integer
    Private msModule As String
    Private msName As String
    Private miSortOrder As Integer
    Private mbDeleted As Boolean
    Private mcPermissions As CArrayList(Of CDatabaseRolePermission)

    Private mbChanged As Boolean

    Public Sub New()

        miDatabaseRoleID = 0
        msModule = String.Empty
        msName = String.Empty
        miSortOrder = 0
        mbDeleted = False

        mbChanged = False

    End Sub

    Public Sub New(ByRef oDataRow As DataRow)

        Me.New()

        miDatabaseRoleID = oDataRow("DatabaseRole_ID")
        msModule = oDataRow("Module")
        msName = oDataRow("Name")
        miSortOrder = oDataRow("SortOrder")
        mbDeleted = oDataRow("Deleted")

        mcPermissions = New CArrayList(Of CDatabaseRolePermission)

        Dim oPermissionsRow As DataRow
        For Each oPermissionsRow In oDataRow.GetChildRows("Permissions")
            Dim oDatabaseRolePermission As New CDatabaseRolePermission(oPermissionsRow)
            If oDatabaseRolePermission.DatabaseObject.ModuleName = msModule Then
                mcPermissions.Add(oDatabaseRolePermission)
            End If
        Next

        mcPermissions.ResetChanges()

        mbChanged = False

    End Sub

    Public Sub Save()

        Dim oParameters As New ArrayList

        If mbChanged Then

            oParameters.Add(New SqlClient.SqlParameter("@Module", msModule))
            oParameters.Add(New SqlClient.SqlParameter("@Name", msName))
            oParameters.Add(New SqlClient.SqlParameter("@SortOrder", miSortOrder))
            oParameters.Add(New SqlClient.SqlParameter("@Deleted", mbDeleted))

            If ID = 0 Then
                miDatabaseRoleID = DataAccess.GetValue("sp_Common_DatabaseRoleInsert", oParameters)
            Else
                oParameters.Add(New SqlClient.SqlParameter("@DatabaseRole_ID", ID))
                DataAccess.ExecuteCommand("sp_Common_DatabaseRoleUpdate", oParameters)
            End If

            mbChanged = False

        End If

        SaveDatabaseRolePermission()

    End Sub

    Private Sub SaveDatabaseRolePermission()

        Dim oParameters As ArrayList
        Dim oDatabaseRolePermission As CDatabaseRolePermission

        'Remove the Deleted items
        For Each oDatabaseRolePermission In mcPermissions.Removed
            oParameters = New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@DatabaseRolePermission_ID", oDatabaseRolePermission.ID))
            DataAccess.ExecuteCommand("sp_Common_ContactDatabaseroleDelete", oParameters)
        Next

        For Each oDatabaseRolePermission In mcPermissions
            oDatabaseRolePermission.Save(Me.ID)
        Next

    End Sub

    Public ReadOnly Property ID() As Integer
        Get
            Return miDatabaseRoleID
        End Get
    End Property
    Public Property ModuleName() As String
        Get
            Return msModule
        End Get
        Set(ByVal Value As String)
            If msModule <> Value Then mbChanged = True
            msModule = Value
        End Set
    End Property
    Public Property Name() As String
        Get
            Return msName
        End Get
        Set(ByVal Value As String)
            If msName <> Value Then mbChanged = True
            msName = Value
        End Set
    End Property
    Public Property SortOrder() As Integer
        Get
            Return miSortOrder
        End Get
        Set(ByVal Value As Integer)
            If miSortOrder <> Value Then mbChanged = True
            miSortOrder = Value
        End Set
    End Property
    Public Property Deleted() As Boolean
        Get
            Return mbDeleted
        End Get
        Set(ByVal Value As Boolean)
            If mbDeleted <> Value Then mbChanged = True
            mbDeleted = Value
        End Set
    End Property

    Public ReadOnly Property Permissions() As CArrayList(Of CDatabaseRolePermission)
        Get
            If mcPermissions Is Nothing Then

                mcPermissions = New CArrayList(Of CDatabaseRolePermission)

                Dim oDatabaseRolePermission As New CDatabaseRolePermission
                Dim oDatabaseObject As CDatabaseObject
                For Each oDatabaseObject In Lists.DatabaseObjects.Values
                    If oDatabaseObject.ModuleName = msModule Then
                        oDatabaseRolePermission = New CDatabaseRolePermission
                        oDatabaseRolePermission.DatabaseObjectID = oDatabaseObject.ID
                        oDatabaseRolePermission.Read = False
                        oDatabaseRolePermission.Write = False
                        oDatabaseRolePermission.ViewState = False
                        oDatabaseRolePermission.ViewCountry = False
                        mcPermissions.Add(oDatabaseRolePermission)
                    End If
                Next

            End If

            Return mcPermissions

        End Get
    End Property

    Public Overrides Function toString() As String
        Return msName
    End Function
    Public ReadOnly Property ModuleAndName() As String
        Get
            If msModule <> String.Empty Then
                Return msModule & " - " & msName
            Else
                Return msName
            End If
        End Get
    End Property

End Class