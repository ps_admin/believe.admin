
<CLSCompliant(True)> _
Public Class CLists

    Private moExternal As External.CLists
    Private moInternal As Internal.CLists


    Private moBankAccount As Hashtable
    Private moCountry As Hashtable
    Private moCreditCardType As Hashtable
    Private moDenomination As Hashtable
    Private moFamilyMemberType As Hashtable
    Private moMailingList As Hashtable
    Private moMailingListType As Hashtable
    Private moPaymentType As Hashtable
    Private moSalutation As Hashtable
    Private moSchoolGrade As Hashtable
    Private moState As Hashtable
    Private moSuburb As Hashtable
    Private moUser As Hashtable
    Private moWWCCType As Hashtable
    Private moDatabaseRole As Hashtable
    Private moDatabaseObjects As Hashtable

    Public Sub New()
        moExternal = New External.CLists
        moInternal = New Internal.CLists
    End Sub

    Public ReadOnly Property External() As External.CLists
        Get
            Return moExternal
        End Get
    End Property
    Public ReadOnly Property Internal() As Internal.CLists
        Get
            Return moInternal
        End Get
    End Property

    Public ReadOnly Property BankAccounts() As Hashtable
        Get
            If moBankAccount Is Nothing Then
                Dim oDataRow As DataRow
                Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Common_BankAccount")

                moBankAccount = New Hashtable
                For Each oDataRow In oDataTable.Rows
                    Dim oBankAccount As New CBankAccount(oDataRow)
                    moBankAccount.Add(oBankAccount.ID, oBankAccount)
                Next
            End If
            Return moBankAccount
        End Get
    End Property

    Public ReadOnly Property Countries() As Hashtable
        Get
            If moCountry Is Nothing Then
                Dim oDataRow As DataRow
                Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Common_GeneralCountry ORDER BY Country")

                moCountry = New Hashtable
                For Each oDataRow In oDataTable.Rows
                    Dim oCountry As New CCountry(oDataRow)
                    moCountry.Add(oCountry.ID, oCountry)
                Next
            End If
            Return moCountry
        End Get
    End Property

    Public ReadOnly Property Denominations() As Hashtable
        Get
            If moDenomination Is Nothing Then
                Dim oDataRow As DataRow
                Dim oDataTable As DataTable = DataAccess.GetDataTable("EXEC sp_Common_DenominationSelectAll")

                moDenomination = New Hashtable
                For Each oDataRow In oDataTable.Rows
                    Dim oDenomination As New CDenomination(oDataRow)
                    moDenomination.Add(oDenomination.ID, oDenomination)
                Next
            End If
            Return moDenomination
        End Get
    End Property

    Public ReadOnly Property FamilyMemberTypes() As Hashtable
        Get
            If moFamilyMemberType Is Nothing Then
                Dim oDataRow As DataRow
                Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Common_FamilyMemberType")

                moFamilyMemberType = New Hashtable
                For Each oDataRow In oDataTable.Rows
                    Dim oFamilyMemberType As New CFamilyMemberType(oDataRow)
                    moFamilyMemberType.Add(oFamilyMemberType.ID, oFamilyMemberType)
                Next
            End If
            Return moFamilyMemberType
        End Get
    End Property

    Public ReadOnly Property CreditCardTypes() As Hashtable
        Get
            If moCreditCardType Is Nothing Then
                Dim oDataRow As DataRow
                Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Common_CreditCardType")

                moCreditCardType = New Hashtable
                For Each oDataRow In oDataTable.Rows
                    Dim oCreditCardType As New CCreditCardType(oDataRow)
                    moCreditCardType.Add(oCreditCardType.ID, oCreditCardType)
                Next
            End If
            Return moCreditCardType
        End Get
    End Property

    Public ReadOnly Property DatabaseObjects() As Hashtable
        Get
            If moDatabaseObjects Is Nothing Then
                Dim oDataRow As DataRow
                Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Common_DatabaseObject")

                moDatabaseObjects = New Hashtable
                For Each oDataRow In oDataTable.Rows
                    Dim oObject As New CDatabaseObject(oDataRow)
                    moDatabaseObjects.Add(oObject.ID, oObject)
                Next
            End If
            Return moDatabaseObjects
        End Get
    End Property

    Public ReadOnly Property DatabaseObjectsByModule(ByVal iModule As EnumDatabaseModule) As Hashtable
        Get
            Dim oDatabaseObjects As New Hashtable
            Dim oDatabaseObject As CDatabaseObject
            For Each oDatabaseObject In DatabaseObjects.Values
                If oDatabaseObject.ModuleName = iModule.ToString Or iModule = 0 Then
                    oDatabaseObjects.Add(oDatabaseObject.ID, oDatabaseObject)
                End If
            Next
            Return oDatabaseObjects
        End Get
    End Property

    Public ReadOnly Property MailingLists() As Hashtable
        Get
            If moMailingList Is Nothing Then
                Dim oDataRow As DataRow
                Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Common_MailingList ORDER BY SortOrder")

                moMailingList = New Hashtable
                For Each oDataRow In oDataTable.Rows
                    Dim oMailingList As New CMailingList(oDataRow)
                    moMailingList.Add(oMailingList.ID, oMailingList)
                Next
            End If

            Return moMailingList
        End Get
    End Property

    Public ReadOnly Property MailingListsByDatabase(ByVal iDatabase As EnumContactDatabase) As CArrayList(Of CMailingList)
        Get
            Dim oArrayList As New CArrayList(Of CMailingList)
            Dim oMailingList As CMailingList
            For Each oMailingList In MailingLists.Values
                If oMailingList.Database = iDatabase.ToString Then
                    oArrayList.Add(oMailingList)
                End If
            Next
            Return oArrayList
        End Get
    End Property

    Public ReadOnly Property MailingListTypes() As Hashtable
        Get
            If moMailingListType Is Nothing Then
                Dim oDataRow As DataRow
                Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Common_MailingListType")

                moMailingListType = New Hashtable
                For Each oDataRow In oDataTable.Rows
                    Dim oMailingListType As New CMailingListType(oDataRow)
                    moMailingListType.Add(oMailingListType.ID, oMailingListType)
                Next
            End If

            Return moMailingListType
        End Get
    End Property
    Public ReadOnly Property PaymentTypes() As Hashtable
        Get
            If moPaymentType Is Nothing Then
                Dim oDataRow As DataRow
                Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Common_PaymentType")

                moPaymentType = New Hashtable
                For Each oDataRow In oDataTable.Rows
                    Dim oPaymentType As New CPaymentType(oDataRow)
                    moPaymentType.Add(oPaymentType.ID, oPaymentType)
                Next
            End If

            Return moPaymentType
        End Get
    End Property


    Public ReadOnly Property PaymentTypesWithoutCreditCard() As Hashtable
        Get
            If moPaymentType Is Nothing Then
                Dim oDataRow As DataRow
                Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Common_PaymentType where PaymentType_ID <> 3")

                moPaymentType = New Hashtable
                For Each oDataRow In oDataTable.Rows
                    Dim oPaymentType As New CPaymentType(oDataRow)
                    moPaymentType.Add(oPaymentType.ID, oPaymentType)
                Next
            End If

            Return moPaymentType
        End Get
    End Property

    Public ReadOnly Property Salutations() As Hashtable
        Get
            If moSalutation Is Nothing Then
                Dim oDataRow As DataRow
                Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Common_Salutation ORDER BY Salutation")

                moSalutation = New Hashtable
                For Each oDataRow In oDataTable.Rows
                    Dim oSalutation As New CSalutation(oDataRow)
                    moSalutation.Add(oSalutation.ID, oSalutation)
                Next
            End If
            Return moSalutation
        End Get
    End Property

    Public ReadOnly Property SchoolGrade() As Hashtable
        Get
            If moSchoolGrade Is Nothing Then
                Dim oDataRow As DataRow
                Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Common_SchoolGrade ORDER BY SortOrder")

                moSchoolGrade = New Hashtable
                For Each oDataRow In oDataTable.Rows
                    Dim oSchoolGrade As New CSchoolGrade(oDataRow)
                    moSchoolGrade.Add(oSchoolGrade.ID, oSchoolGrade)
                Next
            End If
            Return moSchoolGrade
        End Get
    End Property

    Public ReadOnly Property States() As Hashtable
        Get
            If moState Is Nothing Then
                Dim oDataRow As DataRow
                Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Common_GeneralState ORDER BY Country_ID, State")

                moState = New Hashtable
                For Each oDataRow In oDataTable.Rows
                    Dim oState As New CState(oDataRow)
                    moState.Add(oState.ID, oState)
                Next
            End If
            Return moState
        End Get
    End Property
    Public ReadOnly Property Suburbs() As Hashtable
        Get
            If moSuburb Is Nothing Then
                Dim oDataRow As DataRow
                Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT DISTINCT Suburb, State FROM Common_AustralianSuburb ORDER BY State, Suburb")
                oDataTable.Columns.Add("ID", Type.GetType("System.Int32"))
                Dim index As Integer = 1
                moSuburb = New Hashtable
                For Each oDataRow In oDataTable.Rows
                    oDataRow("ID") = index
                    Dim oSuburb As New CSuburb(oDataRow)
                    moSuburb.Add(oSuburb.ID, oSuburb)
                    index += 1
                Next
            End If
            Return moSuburb
        End Get
    End Property

    Public ReadOnly Property WWCCType() As Hashtable
        Get
            If moWWCCType Is Nothing Then
                Dim oDataRow As DataRow
                Dim oDataTable As DataTable = DataAccess.GetDataTable("SELECT * FROM Common_WWCCType ORDER BY [Name]")

                moWWCCType = New Hashtable
                For Each oDataRow In oDataTable.Rows
                    Dim oWWCCType As New CWWCCType(oDataRow)
                    moWWCCType.Add(oWWCCType.ID, oWWCCType)
                Next
            End If
            Return moWWCCType
        End Get
    End Property

    Public ReadOnly Property DatabaseRoles() As Hashtable
        Get
            If moDatabaseRole Is Nothing Then

                Dim oDataRow As DataRow
                Dim oTableMappings As New ArrayList
                oTableMappings.Add(New System.Data.Common.DataTableMapping("Table", "DatabaseRole"))
                oTableMappings.Add(New System.Data.Common.DataTableMapping("Table1", "DatabaseRolePermission"))

                Dim oDataSet As DataSet = DataAccess.GetDataSet("EXEC sp_Common_DatabaseRoleSelectAll", oTableMappings)

                oDataSet.Relations.Add("Permissions", oDataSet.Tables("DatabaseRole").Columns("DatabaseRole_ID"), oDataSet.Tables("DatabaseRolePermission").Columns("DatabaseRole_ID"))

                moDatabaseRole = New Hashtable
                For Each oDataRow In oDataSet.Tables("DatabaseRole").Rows
                    Dim oDatabaseRole As New CDatabaseRole(oDataRow)
                    moDatabaseRole.Add(oDatabaseRole.ID, oDatabaseRole)
                Next
            End If
            Return moDatabaseRole
        End Get
    End Property

    Public ReadOnly Property DatabaseRolesCurrent() As Hashtable
        Get
            Dim oHashTable As New Hashtable
            Dim oDatabaseRole As CDatabaseRole
            For Each oDatabaseRole In DatabaseRoles.Values
                If Not oDatabaseRole.Deleted Then
                    oHashTable.Add(oDatabaseRole.ID, oDatabaseRole)
                End If
            Next
            Return oHashTable
        End Get
    End Property

    Public ReadOnly Property DatabaseRolesByModule(ByVal iModule As EnumDatabaseModule) As Hashtable
        Get
            Dim oDatabaseRoles As New Hashtable
            Dim oDatabaseRole As CDatabaseRole
            For Each oDatabaseRole In DatabaseRolesCurrent.Values
                If oDatabaseRole.ModuleName = iModule.ToString Then
                    oDatabaseRoles.Add(oDatabaseRole.ID, oDatabaseRole)
                End If
            Next
            Return oDatabaseRoles
        End Get
    End Property

    Public ReadOnly Property Users() As Hashtable
        Get
            If moUser Is Nothing Then

                Dim oDataRow As DataRow
                Dim oDataSet As DataSet

                Dim oTableMappings As New ArrayList
                oTableMappings.Add(New Common.DataTableMapping("Table", "User"))
                oTableMappings.Add(New Common.DataTableMapping("Table1", "DatabaseRole"))

                oDataSet = DataAccess.GetDataSet("EXEC sp_Common_UsersSelectAll", oTableMappings)

                oDataSet.Relations.Add("DatabaseRole", oDataSet.Tables("User").Columns("Contact_ID"), _
                        oDataSet.Tables("DatabaseRole").Columns("Contact_ID"))


                moUser = New Hashtable
                For Each oDataRow In oDataSet.Tables("User").Rows
                    Dim oUser As New CUser(oDataRow)
                    moUser.Add(oUser.ID, oUser)
                Next

            End If
            Return moUser

        End Get
    End Property

    Public ReadOnly Property UsersCurrent(Optional ByVal oCampus As Internal.Church.CCampus = Nothing) As Hashtable
        Get
            Dim oUser As CUser
            Dim oUserList As New Hashtable

            For Each oUser In Users.Values
                If Not oUser.Inactive Then
                    If oCampus Is Nothing OrElse oUser.CampusID = oCampus.ID Then
                        oUserList.Add(oUser.ID, oUser)
                    End If
                End If
            Next
            Return oUserList

        End Get
    End Property

    Public Function GetBankAccountByID(ByVal iID As Integer) As CBankAccount
        Return BankAccounts(iID)
    End Function
    Public Function GetCountryByID(ByVal iID As Integer) As CCountry
        Return Countries(iID)
    End Function
    Public Function GetCreditCardTypeByID(ByVal iID As Integer) As CCreditCardType
        Return CreditCardTypes(iID)
    End Function
    Public Function GetDenominationByID(ByVal iID As Integer) As CDenomination
        Return Denominations(iID)
    End Function
    Public Function GetDatabaseObjectByID(ByVal iID As Integer) As CDatabaseObject
        Return DatabaseObjects(iID)
    End Function
    Public Function GetFamilyMemberTypeByID(ByVal iID As Integer) As CFamilyMemberType
        Return FamilyMemberTypes(iID)
    End Function
    Public Function GetMailingListByID(ByVal iID As Integer) As CMailingList
        Return MailingLists(iID)
    End Function
    Public Function GetMailingListTypeByID(ByVal iID As Integer) As CMailingListType
        Return MailingListTypes(iID)
    End Function
    Public Function GetPaymentTypeByID(ByVal iID As Integer) As CPaymentType
        Return PaymentTypes(iID)
    End Function
    Public Function GetSalutationByID(ByVal iID As Integer) As CSalutation
        Return Salutations(iID)
    End Function
    Public Function GetSchoolGradeByID(ByVal iID As Integer) As CSchoolGrade
        Return SchoolGrade(iID)
    End Function
    Public Function GetStateByID(ByVal iID As Integer) As CState
        Return States(iID)
    End Function
    Public Function GetWWCCTypeByID(ByVal iID As Integer) As CWWCCType
        Return WWCCType(iID)
    End Function
    Public Function GetDatabaseRoleByID(ByVal iID As Integer) As CDatabaseRole
        Return DatabaseRoles(iID)
    End Function
    Public Function GetDatabaseRoleByName(ByVal sName As String) As CDatabaseRole
        For Each oDatabaseRole As CDatabaseRole In DatabaseRoles.Values
            If oDatabaseRole.Name = sName Then
                Return oDatabaseRole
            End If
        Next

        Return Nothing
    End Function
    Public Function GetUserByID(ByVal iID As Integer) As CUser
        Return Users(iID)
    End Function
    Public Function GetUserByEmail(ByVal sEmail As String) As CUser
        Dim oUser As CUser
        For Each oUser In Users.Values
            If oUser.Email.ToLower = sEmail.ToLower Then Return oUser
        Next
        Return Nothing
    End Function

End Class
