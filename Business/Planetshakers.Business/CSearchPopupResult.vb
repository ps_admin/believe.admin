
<Serializable()> _
<CLSCompliant(True)> _
Public Class CSearchPopupResult

    Private miContactID As Integer
    Private msName As String
    Private msChurchStatus As String
    Private msStreetAddress As String
    Private msPostalAddress As String
    Private msPhone As String
    Private msMobile As String
    Private msEmail As String
    Private msGUID As String

    Public Sub New()

        miContactID = 0
        msName = String.Empty
        msChurchStatus = String.Empty
        msStreetAddress = String.Empty
        msPostalAddress = String.Empty
        msPhone = String.Empty
        msMobile = String.Empty
        msEmail = String.Empty
        msGUID = System.Guid.NewGuid.ToString

    End Sub

    Public Sub New(ByRef oDataRow As DataRow)

        Me.New()

        miContactID = oDataRow("Contact_ID")
        msName = oDataRow("Name")
        msChurchStatus = oDataRow("ChurchStatus")
        msStreetAddress = oDataRow("StreetAddress")
        msPostalAddress = oDataRow("PostalAddress")
        msPhone = oDataRow("Phone")
        msMobile = oDataRow("Mobile")
        msEmail = oDataRow("Email")

    End Sub

    Public Property ID() As Integer
        Get
            Return miContactID
        End Get
        Set(ByVal Value As Integer)
            miContactID = Value
        End Set
    End Property

    Public ReadOnly Property Name() As String
        Get
            Return msName
        End Get
    End Property
    Public ReadOnly Property ChurchStatus() As String
        Get
            Return msChurchStatus
        End Get
    End Property
    Public ReadOnly Property StreetAddress() As String
        Get
            Return msStreetAddress
        End Get
    End Property
    Public ReadOnly Property PostalAddress() As String
        Get
            Return msPostalAddress
        End Get
    End Property
    Public ReadOnly Property Phone() As String
        Get
            Return msPhone
        End Get
    End Property
    Public ReadOnly Property Mobile() As String
        Get
            Return msMobile
        End Get
    End Property
    Public ReadOnly Property Email() As String
        Get
            Return msEmail
        End Get
    End Property
    Public ReadOnly Property GUID() As String
        Get
            Return msGUID
        End Get
    End Property

End Class