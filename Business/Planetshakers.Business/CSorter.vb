Public Class CSorterContacts
    Implements System.Collections.Generic.IComparer(Of CContact)

    Public Function Compare(ByVal x As CContact, ByVal y As CContact) As Integer Implements System.Collections.Generic.IComparer(Of CContact).Compare

        If x.LastName = y.LastName Then
            Return x.FirstName < y.FirstName
        Else
            Return x.LastName < y.LastName
        End If

    End Function
End Class

Public Class CSorterProduct
    Implements System.Collections.IComparer

    Private miSortBy As EnumProductSortBy
    Public Enum EnumProductSortBy As Integer
        Code = 1
        Title = 2
    End Enum

    Public Sub New(ByVal iSortBy As EnumProductSortBy)
        miSortBy = iSortBy
    End Sub
    Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements System.Collections.IComparer.Compare

        If miSortBy = EnumProductSortBy.Code Then
            Return StringLogicalComparer.compare(x.code, y.code)
        Else
            Return x.ListDescriptionTitle.ToString.ToLower < y.ListDescriptionTitle.ToString.ToLower
        End If

    End Function

End Class


Public Class CSorterBulkRegistrations
    Implements System.Collections.Generic.IComparer(Of External.Events.CGroupBulkRegistration)

    Public Function Compare(ByVal x As External.Events.CGroupBulkRegistration, ByVal y As External.Events.CGroupBulkRegistration) As Integer Implements System.Collections.Generic.IComparer(Of External.Events.CGroupBulkRegistration).Compare

        If x.dateadded = y.dateadded Then
            Return x.GUID < y.GUID
        Else
            Return CDate(x.DateAdded) < CDate(y.DateAdded)
        End If

    End Function

End Class

Public Class ListItemComparer
    Implements IComparer

    Public Function Compare(ByVal x As Object, _
          ByVal y As Object) As Integer _
          Implements System.Collections.IComparer.Compare
        Dim a As System.Web.UI.WebControls.ListItem = CType(x, System.Web.UI.WebControls.ListItem)
        Dim b As System.Web.UI.WebControls.ListItem = CType(y, System.Web.UI.WebControls.ListItem)
        Dim c As New CaseInsensitiveComparer
        Return c.Compare(a.Text, b.Text)
    End Function

End Class

'Public Class CSorterCountryAusAtTop
'    Implements IComparer

'    Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements System.Collections.IComparer.Compare

'        If x.Name = "Australia" Then
'            Return 1 > 0
'        ElseIf y.Name = "Australia" Then
'            Return 0 > 1
'        Else
'            Return x.Name < y.Name
'        End If

'    End Function

'End Class

Public Class CSorterVenueByDateDesc
    Implements IComparer

    Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements System.Collections.IComparer.Compare

        If CType(x, External.Events.CVenue).ConferenceEndDate = CType(y, External.Events.CVenue).ConferenceEndDate Then
            Return CType(x, External.Events.CVenue).Name > CType(y, External.Events.CVenue).Name
        Else
            Return CType(x, External.Events.CVenue).ConferenceEndDate > CType(y, External.Events.CVenue).ConferenceEndDate
        End If

    End Function
End Class

Public Class CSorterConferenceByDateDesc
    Implements IComparer

    Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements System.Collections.IComparer.Compare

        Dim sDate1 As String = CType(x, External.Events.CConference).ConferenceLastEndDate
        Dim sDate2 As String = CType(y, External.Events.CConference).ConferenceLastEndDate

        If sDate1 = sDate2 Then
            Return CType(x, External.Events.CConference).Name < CType(y, External.Events.CConference).Name
        ElseIf sDate1 <> "" And sDate2 <> "" Then
            Return CDate(sDate1) > CDate(sDate2)
        ElseIf sDate1 <> "" Then
            Return 1 > 0
        Else
            Return 0 > 1
        End If

    End Function
End Class

Public Class CSorterContent
    Implements System.Collections.Generic.IComparer(Of External.Revolution.CContent)

    Public Function Compare(ByVal x As External.Revolution.CContent, ByVal y As External.Revolution.CContent) As Integer Implements System.Collections.Generic.IComparer(Of External.Revolution.CContent).Compare

        If x.ContentPack.ID = y.ContentPack.ID Then
            Return x.SortOrder < y.SortOrder
        Else
            Return CDate(x.ContentPack.StartDate) > CDate(y.ContentPack.StartDate)
        End If


    End Function
End Class

Public Class CSorterContentFile
    Implements System.Collections.Generic.IComparer(Of External.Revolution.CContentFile)

    Public Function Compare(ByVal x As External.Revolution.CContentFile, ByVal y As External.Revolution.CContentFile) As Integer Implements System.Collections.Generic.IComparer(Of External.Revolution.CContentFile).Compare

        Return x.SortOrder < y.SortOrder

    End Function
End Class

Public Class CSorterContentContentCategory
    Implements System.Collections.Generic.IComparer(Of External.Revolution.CContentContentCategory)

    Public Function Compare(ByVal x As External.Revolution.CContentContentCategory, ByVal y As External.Revolution.CContentContentCategory) As Integer Implements System.Collections.Generic.IComparer(Of External.Revolution.CContentContentCategory).Compare

        Return x.SortOrder < y.SortOrder

    End Function
End Class

Public Class CSorterCourseInstance
    Implements System.Collections.Generic.IComparer(Of Internal.Church.CCourseInstance)

    Public Function Compare(ByVal x As Internal.Church.CCourseInstance, ByVal y As Internal.Church.CCourseInstance) As Integer Implements System.Collections.Generic.IComparer(Of Internal.Church.CCourseInstance).Compare

        Return x.StartDate > y.StartDate

    End Function
End Class
Public Class CSorterContactMailingList
    Implements System.Collections.Generic.IComparer(Of CContactMailingList)

    Public Function Compare(ByVal x As CContactMailingList, ByVal y As CContactMailingList) As Integer Implements System.Collections.Generic.IComparer(Of CContactMailingList).Compare

        Return x.MailingList.SortOrder < y.MailingList.SortOrder

    End Function
End Class


Public Class CSorterBySortOrder
    Implements System.Collections.IComparer

    Private mbDescending As Boolean

    Public Sub New(Optional ByVal bDescending As Boolean = False)
        mbDescending = bDescending
    End Sub
    Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements System.Collections.IComparer.Compare

        If mbDescending Then
            Return x.SortOrder > y.SortOrder
        Else
            Return x.SortOrder < y.SortOrder
        End If

    End Function

End Class

Public Class CSorterByName
    Implements System.Collections.IComparer

    Private mbDescending As Boolean

    Public Sub New(Optional ByVal bDescending As Boolean = False)
        mbDescending = bDescending
    End Sub
    Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements System.Collections.IComparer.Compare

        If mbDescending Then
            Return x.Name.ToString.ToLower > y.Name.ToString.ToLower
        Else
            Return x.Name.ToString.ToLower < y.Name.ToString.ToLower
        End If

    End Function

End Class

Public Class CSorterByModuleAndName
    Implements System.Collections.IComparer

    Private mbDescending As Boolean

    Public Sub New(Optional ByVal bDescending As Boolean = False)
        mbDescending = bDescending
    End Sub
    Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements System.Collections.IComparer.Compare

        If mbDescending Then
            Return x.ModuleAndName.ToString.ToLower > y.ModuleAndName.ToString.ToLower
        Else
            Return x.ModuleAndName.ToString.ToLower < y.ModuleAndName.ToString.ToLower
        End If

    End Function

End Class


Public Class CSorterByDate
    Implements System.Collections.IComparer

    Private mbDescending As Boolean

    Public Sub New(Optional ByVal bDescending As Boolean = False)
        mbDescending = bDescending
    End Sub
    Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements System.Collections.IComparer.Compare

        If mbDescending Then
            Return x.Date > y.Date
        Else
            Return x.Date < y.Date
        End If

    End Function

End Class

Public Class CSorterPCRContact
    Implements System.Collections.Generic.IComparer(Of Internal.Church.CPCRContact)

    Public Function Compare(ByVal x As Internal.Church.CPCRContact, ByVal y As Internal.Church.CPCRContact) As Integer Implements System.Collections.Generic.IComparer(Of Internal.Church.CPCRContact).Compare
        If x.ContactChurchStatusID = y.ContactChurchStatusID Then
            If x.ContactLastName = y.ContactLastName Then
                If x.ContactFirstName = y.ContactFirstName Then
                    Return x.ID < y.ID
                Else
                    Return x.ContactFirstName < y.ContactFirstName
                End If
            Else
                Return x.ContactLastName < y.ContactLastName
            End If
        Else
            If x.ContactChurchStatusID = Internal.Church.EnumChurchStatus.NewChristian Then
                Return -2
            ElseIf x.ContactChurchStatusID = Internal.Church.EnumChurchStatus.NewPeople Then
                Return -3
            Else
                Return x.ContactChurchStatusID
            End If
        End If
    End Function

End Class

Public Enum EnumSortType
    SortBySortOrder = 1
    SortByName = 2
    SortByModuleAndName = 3
    SortByDate = 4
    NoSort = 5
End Enum