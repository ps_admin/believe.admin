
<Serializable()> _
<CLSCompliant(True)> _
Public Class CState

    Private miStateID As Integer
    Private msName As String
    Private msFullName As String
    Private moCountry As CCountry
    Private miSortOrder As Integer

    Public Sub New()

        miStateID = 0
        msName = ""
        msFullName = ""
        moCountry = Nothing
        miSortOrder = 0

    End Sub

    Public Sub New(ByRef oDataRow As DataRow)

        Me.New()

        miStateID = oDataRow("State_ID")
        msName = oDataRow("State")
        msFullName = oDataRow("FullName")
        moCountry = Lists.GetCountryByID(oDataRow("Country_ID"))
        miSortOrder = oDataRow("SortOrder")

    End Sub

    Public ReadOnly Property ID() As Integer
        Get
            Return miStateID
        End Get
    End Property
    Public Property Name() As String
        Get
            Return msName
        End Get
        Set(ByVal Value As String)
            msName = Value
        End Set
    End Property
    Public Property FullName() As String
        Get
            Return msFullName
        End Get
        Set(ByVal Value As String)
            msFullName = Value
        End Set
    End Property
    Public Property Country() As CCountry
        Get
            Return moCountry
        End Get
        Set(ByVal value As CCountry)
            moCountry = value
        End Set
    End Property
    Public Property SortOrder() As Integer
        Get
            Return miSortOrder
        End Get
        Set(ByVal Value As Integer)
            miSortOrder = Value
        End Set
    End Property

    Public Overrides Function toString() As String
        Return msName
    End Function

End Class