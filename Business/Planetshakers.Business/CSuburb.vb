﻿<Serializable()> _
<CLSCompliant(True)> _
Public Class CSuburb

    Private miSuburbID As Integer
    'Private miPostCode As Integer
    Private msName As String
    Private moState As CState
    'Private miLatitude As Integer
    'Private miLongitude As Integer

    Public Sub New()

        miSuburbID = 0
        'miPostCode = 0
        msName = ""
        moState = Nothing
        'miLatitude = 0
        'miLongitude = 0

    End Sub

    Public Sub New(ByRef oDataRow As DataRow)

        Me.New()

        miSuburbID = oDataRow("ID")
        'miPostCode = oDataRow("Postcode")
        msName = oDataRow("Suburb")
        moState = Lists.GetStateByID(oDataRow("State"))
        'miLatitude = oDataRow("Latitude")
        'miLongitude = oDataRow("Longitude")

    End Sub

    Public ReadOnly Property ID() As Integer
        Get
            Return miSuburbID
        End Get
    End Property
    Public Property Name() As String
        Get
            Return msName
        End Get
        Set(ByVal Value As String)
            msName = Value
        End Set
    End Property
    'Public Property PostCode() As Integer
    '    Get
    '        Return miPostCode
    '    End Get
    '    Set(ByVal Value As Integer)
    '        miPostCode = Value
    '    End Set
    'End Property

    Public Property State(ByVal state_id As Integer) As CState
        Get
            Return moState
        End Get
        Set(ByVal value As CState)
            moState = value
        End Set
    End Property

    'Public Property Latitude() As Integer
    '    Get
    '        Return miLatitude
    '    End Get
    '    Set(ByVal Value As Integer)
    '        miLatitude = Value
    '    End Set
    'End Property

    'Public Property Longitude() As Integer
    '    Get
    '        Return miLongitude
    '    End Get
    '    Set(ByVal Value As Integer)
    '        miLongitude = Value
    '    End Set
    'End Property

    Public Overrides Function toString() As String
        Return msName
    End Function

End Class
