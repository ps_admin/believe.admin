<Serializable()> _
<CLSCompliant(True)> _
Public Class CWWCCType

    Private miWWCCTypeID As Integer
    Private msName As String

    Public Sub New()

        miWWCCTypeID = 0
        msName = ""

    End Sub

    Public Sub New(ByRef oDataRow As DataRow)

        Me.New()

        miWWCCTypeID = oDataRow("WWCCType_ID")
        msName = oDataRow("Name")

    End Sub

    Public ReadOnly Property ID() As Integer
        Get
            Return miWWCCTypeID
        End Get
    End Property
    Public Property Name() As String
        Get
            Return msName
        End Get
        Set(ByVal Value As String)
            msName = Value
        End Set
    End Property

End Class