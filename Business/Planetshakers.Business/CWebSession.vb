
<Serializable()> _
<CLSCompliant(True)> _
Public Class CWebSession

    Private miWebSessionID As Integer
    Private miContactID As Integer
    Private moContact As CContact
    Private mdSessionStartDate As Date
    Private mdSessionLastActivity As Date
    Private msClientIP As String
    Private msSessionID As String
    Private miSessionTimeout As Integer

    Public Sub New()

        miWebSessionID = 0
        miContactID = 0
        moContact = Nothing
        mdSessionStartDate = Nothing
        mdSessionLastActivity = Nothing
        msClientIP = String.Empty
        msSessionID = String.Empty
        miSessionTimeout = 20

    End Sub

    Public Sub New(ByRef oDataRow As DataRow)

        Me.New()
        LoadDataRow(oDataRow)

    End Sub

    Public Sub LoadDataRow(ByRef oDataRow As DataRow)

        miWebSessionID = oDataRow("WebSession_ID")
        miContactID = oDataRow("Contact_ID")
        mdSessionStartDate = oDataRow("SessionStartDate")
        mdSessionLastActivity = oDataRow("SessionLastActivity")
        msClientIP = oDataRow("ClientIP")
        msSessionID = oDataRow("SessionID")
        miSessionTimeout = oDataRow("SessionTimeout")

    End Sub

    Public Function LoadBySessionID(ByVal sSessionID As String) As Boolean

        Dim oParameters As New ArrayList
        oParameters.Add(New SqlClient.SqlParameter("@SessionID", sSessionID))

        Dim oDataTable As DataTable
        oDataTable = DataAccess.GetDataTable("sp_Common_WebSessionRead", oParameters)

        If oDataTable.Rows.Count > 0 Then
            LoadDataRow(oDataTable.Rows(0))
            Return True
        Else
            Return False
        End If

    End Function

    Public Sub Save()

        Dim oParameters As New ArrayList
        oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", miContactID))
        oParameters.Add(New SqlClient.SqlParameter("@SessionStartDate", SQLDateTime(mdSessionStartDate)))
        oParameters.Add(New SqlClient.SqlParameter("@SessionLastActivity", SQLDateTime(mdSessionLastActivity)))
        oParameters.Add(New SqlClient.SqlParameter("@ClientIP", msClientIP))
        oParameters.Add(New SqlClient.SqlParameter("@SessionID", msSessionID))
        oParameters.Add(New SqlClient.SqlParameter("@SessionTimeout", miSessionTimeout))

        If Me.ID = 0 Then
            miWebSessionID = DataAccess.GetValue("sp_Common_WebSessionInsert", oParameters)
        Else
            oParameters.Add(New SqlClient.SqlParameter("@WebSession_ID", miWebSessionID))
            miWebSessionID = DataAccess.GetValue("sp_Common_WebSessionUpdate", oParameters)
        End If

    End Sub

    Public Sub Delete()

        Dim oParameters As New ArrayList
        oParameters.Add(New SqlClient.SqlParameter("@WebSession_ID", miWebSessionID))

        DataAccess.ExecuteCommand("sp_Common_WebSessionDelete", oParameters)

    End Sub

    Public ReadOnly Property ID() As Integer
        Get
            Return miWebSessionID
        End Get
    End Property
    Public Property ContactID() As Integer
        Get
            Return miContactID
        End Get
        Set(ByVal value As Integer)
            miContactID = value
        End Set
    End Property
    Public ReadOnly Property Contact() As CContact
        Get
            If moContact Is Nothing And miContactID > 0 Then
                moContact = New CContact
                moContact.LoadByID(miContactID)
            End If
            Return moContact
        End Get
    End Property
    Public Property SessionStartDate() As Date
        Get
            Return mdSessionStartDate
        End Get
        Set(ByVal value As Date)
            mdSessionStartDate = value
        End Set
    End Property
    Public Property SessionLastActivity() As Date
        Get
            Return mdSessionLastActivity
        End Get
        Set(ByVal value As Date)
            mdSessionLastActivity = value
        End Set
    End Property
    Public Property ClientIP() As String
        Get
            Return msClientIP
        End Get
        Set(ByVal value As String)
            msClientIP = value
        End Set
    End Property
    Public Property SessionID() As String
        Get
            Return msSessionID
        End Get
        Set(ByVal value As String)
            msSessionID = value
        End Set
    End Property
    Public Property SessionTimeout() As Integer
        Get
            Return miSessionTimeout
        End Get
        Set(ByVal value As Integer)
            miSessionTimeout = value
        End Set
    End Property

End Class
