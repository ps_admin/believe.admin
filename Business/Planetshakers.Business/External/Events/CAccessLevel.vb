﻿<Serializable()> _
<CLSCompliant(True)> _
Public Class CAccessLevel
    Private miAccessLevelID As Integer
    Private msAcessLevel As String
    Private mcPriorityTags As CArrayList(Of CPriorityTag)

    Public Sub New()
        miAccessLevelID = 0
        msAcessLevel = ""
        mcPriorityTags = New CArrayList(Of CPriorityTag)
    End Sub

    Public Sub New(ByRef oAccessLevel As DataRow)
        Me.New()

        miAccessLevelID = oAccessLevel("AccessLevel_ID")
        msAcessLevel = oAccessLevel("AccessLevel")

        LoadPriorityTags()
    End Sub

    Public Sub LoadPriorityTags()
        Dim oParameters As New ArrayList
        oParameters.Add(New SqlClient.SqlParameter("@AccessLevel_ID", Me.ID))
        Dim oDataTable As DataTable = Business.DataAccess.GetDataTable("sp_Events_PriorityTagRead", oParameters)

        mcPriorityTags = New CArrayList(Of CPriorityTag)
        For Each oDataRow As DataRow In oDataTable.Rows
            mcPriorityTags.Add(New CPriorityTag(oDataRow))
        Next
    End Sub

    Public ReadOnly Property ID() As Integer
        Get
            Return miAccessLevelID
        End Get
    End Property

    Public ReadOnly Property PriorityTags() As CArrayList(Of CPriorityTag)
        Get
            Return mcPriorityTags
        End Get
    End Property

    Public Property AccessLevel() As String
        Get
            Return msAcessLevel
        End Get
        Set(value As String)
            msAcessLevel = value
        End Set
    End Property

    Public Overrides Function ToString() As String
        Return AccessLevel
    End Function
End Class

