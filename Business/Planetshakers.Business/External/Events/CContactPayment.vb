Namespace External.Events

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CContactPayment

        Private miPaymentID As Integer
        Private miContactID As Integer
        Private miConferenceID As Integer
        Private miPaymentTypeID As Integer
        Private mdPaymentAmount As Decimal
        Private msCCNumber As String
        Private msCCExpiry As String
        Private msCCName As String
        Private msCCPhone As String
        Private mbCCManual As Boolean
        Private msCCTransactionRef As String
        Private mbCCRefund As Boolean
        Private msChequeDrawer As String
        Private msChequeBank As String
        Private msChequeBranch As String
        Private msPaypalTransactionRef As String
        Private msComment As String
        Private mdPaymentDate As Date
        Private miPaymentByID As Integer
        Private miBankAccountID As Integer
        Private msGUID As String

        Private mbChanged As Boolean

        Public Sub New()

            miPaymentID = 0
            miContactID = 0
            miConferenceID = 0
            miPaymentTypeID = 0
            mdPaymentAmount = 0
            msCCNumber = String.Empty
            msCCExpiry = String.Empty
            msCCName = String.Empty
            msCCPhone = String.Empty
            mbCCManual = False
            msCCTransactionRef = String.Empty
            mbCCRefund = False
            msChequeDrawer = String.Empty
            msChequeBank = String.Empty
            msChequeBranch = String.Empty
            msPaypalTransactionRef = String.Empty
            msComment = String.Empty
            mdPaymentDate = GetDateTime
            miPaymentByID = 0
            miBankAccountID = 0
            msGUID = System.Guid.NewGuid.ToString
            mbChanged = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miPaymentID = oDataRow("ContactPayment_ID")
            miContactID = oDataRow("Contact_ID")
            miConferenceID = oDataRow("Conference_ID")
            miPaymentTypeID = oDataRow("PaymentType_ID")
            mdPaymentAmount = oDataRow("PaymentAmount")
            msCCNumber = oDataRow("CCNumber")
            msCCExpiry = oDataRow("CCExpiry")
            msCCName = oDataRow("CCName")
            msCCPhone = oDataRow("CCPhone")
            mbCCManual = oDataRow("CCManual")
            msCCTransactionRef = oDataRow("CCTransactionRef")
            mbCCRefund = oDataRow("CCRefund")
            msChequeDrawer = oDataRow("ChequeDrawer")
            msChequeBank = oDataRow("ChequeBank")
            msChequeBranch = oDataRow("ChequeBranch")
            msPaypalTransactionRef = oDataRow("PaypalTransactionRef")
            msComment = oDataRow("Comment")
            mdPaymentDate = oDataRow("PaymentDate")
            miPaymentByID = oDataRow("PaymentBy_ID")
            miBankAccountID = oDataRow("BankAccount_ID")
            mbChanged = False

        End Sub

        Public Sub Save(ByVal oUser As CUser)

            If mbChanged Then

                Dim oParameters As New ArrayList

                oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", miContactID))
                oParameters.Add(New SqlClient.SqlParameter("@Conference_ID", miConferenceID))
                oParameters.Add(New SqlClient.SqlParameter("@PaymentType_ID", miPaymentTypeID))
                oParameters.Add(New SqlClient.SqlParameter("@PaymentAmount", mdPaymentAmount))
                oParameters.Add(New SqlClient.SqlParameter("@CCNumber", msCCNumber))
                oParameters.Add(New SqlClient.SqlParameter("@CCExpiry", msCCExpiry))
                oParameters.Add(New SqlClient.SqlParameter("@CCName", msCCName))
                oParameters.Add(New SqlClient.SqlParameter("@CCPhone", msCCPhone))
                oParameters.Add(New SqlClient.SqlParameter("@CCManual", mbCCManual))
                oParameters.Add(New SqlClient.SqlParameter("@CCTransactionRef", msCCTransactionRef))
                oParameters.Add(New SqlClient.SqlParameter("@CCRefund", mbCCRefund))
                oParameters.Add(New SqlClient.SqlParameter("@ChequeDrawer", msChequeDrawer))
                oParameters.Add(New SqlClient.SqlParameter("@ChequeBank", msChequeBank))
                oParameters.Add(New SqlClient.SqlParameter("@ChequeBranch", msChequeBranch))
                oParameters.Add(New SqlClient.SqlParameter("@PaypalTransactionRef", msPaypalTransactionRef))
                oParameters.Add(New SqlClient.SqlParameter("@Comment", msComment))
                oParameters.Add(New SqlClient.SqlParameter("@PaymentDate", SQLDateTime(mdPaymentDate)))
                oParameters.Add(New SqlClient.SqlParameter("@PaymentBy_ID", miPaymentByID))
                oParameters.Add(New SqlClient.SqlParameter("@BankAccount_ID", miBankAccountID))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))

                If Me.ID = 0 Then
                    miPaymentID = DataAccess.GetValue("sp_Events_ContactPaymentInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@ContactPayment_ID", ID))
                    DataAccess.ExecuteCommand("sp_Events_ContactPaymentUpdate", oParameters)
                End If

                mbChanged = False

            End If

        End Sub

        Public Property ID() As Integer
            Get
                Return miPaymentID
            End Get
            Set(ByVal value As Integer)
                miPaymentID = value
            End Set
        End Property
        Public Property ContactID() As Integer
            Get
                Return miContactID
            End Get
            Set(ByVal value As Integer)
                If miContactID <> value Then mbChanged = True
                miContactID = value
            End Set
        End Property
        Public Property ConferenceID() As Integer
            Get
                Return miConferenceID
            End Get
            Set(ByVal Value As Integer)
                If miConferenceID <> Value Then mbChanged = True
                miConferenceID = Value
            End Set
        End Property
        Public ReadOnly Property Conference() As CConference
            Get
                Return Lists.External.Events.GetConferenceByID(miConferenceID)
            End Get
        End Property
        Public Property PaymentTypeID() As Integer
            Get
                Return miPaymentTypeID
            End Get
            Set(ByVal Value As Integer)
                If miPaymentTypeID <> Value Then mbChanged = True
                miPaymentTypeID = Value
            End Set
        End Property
        Public ReadOnly Property PaymentType() As CPaymentType
            Get
                Return Lists.GetPaymentTypeByID(miPaymentTypeID)
            End Get
        End Property
        Property PaymentAmount() As Decimal
            Get
                Return mdPaymentAmount
            End Get
            Set(ByVal Value As Decimal)
                If mdPaymentAmount <> Value Then mbChanged = True
                mdPaymentAmount = Value
            End Set
        End Property
        Property CCNumber() As String
            Get
                Return msCCNumber
            End Get
            Set(ByVal Value As String)
                If msCCNumber <> Value Then mbChanged = True
                msCCNumber = Value
            End Set
        End Property
        Property CCExpiry() As String
            Get
                Return msCCExpiry
            End Get
            Set(ByVal Value As String)
                If msCCExpiry <> Value Then mbChanged = True
                msCCExpiry = Value
            End Set
        End Property
        Property CCName() As String
            Get
                Return msCCName
            End Get
            Set(ByVal Value As String)
                If msCCName <> Value Then mbChanged = True
                msCCName = Value
            End Set
        End Property
        Property CCPhone() As String
            Get
                Return msCCPhone
            End Get
            Set(ByVal Value As String)
                If msCCPhone <> Value Then mbChanged = True
                msCCPhone = Value
            End Set
        End Property
        Property CCManual() As Boolean
            Get
                Return mbCCManual
            End Get
            Set(ByVal Value As Boolean)
                If mbCCManual <> Value Then mbChanged = True
                mbCCManual = Value
            End Set
        End Property
        Property CCTransactionRef() As String
            Get
                Return msCCTransactionRef
            End Get
            Set(ByVal Value As String)
                If msCCTransactionRef <> Value Then mbChanged = True
                msCCTransactionRef = Value
            End Set
        End Property
        Property CCRefund() As Boolean
            Get
                Return mbCCRefund
            End Get
            Set(ByVal Value As Boolean)
                If mbCCRefund <> Value Then mbChanged = True
                mbCCRefund = Value
            End Set
        End Property
        Property ChequeDrawer() As String
            Get
                Return msChequeDrawer
            End Get
            Set(ByVal Value As String)
                If msChequeDrawer <> Value Then mbChanged = True
                msChequeDrawer = Value
            End Set
        End Property
        Property ChequeBank() As String
            Get
                Return msChequeBank
            End Get
            Set(ByVal Value As String)
                If msChequeBank <> Value Then mbChanged = True
                msChequeBank = Value
            End Set
        End Property
        Property ChequeBranch() As String
            Get
                Return msChequeBranch
            End Get
            Set(ByVal Value As String)
                If msChequeBranch <> Value Then mbChanged = True
                msChequeBranch = Value
            End Set
        End Property
        Property PaypalTransactionRef() As String
            Get
                Return msPaypalTransactionRef
            End Get
            Set(ByVal Value As String)
                If msPaypalTransactionRef <> Value Then mbChanged = True
                msPaypalTransactionRef = Value
            End Set
        End Property
        Property Comment() As String
            Get
                Return msComment
            End Get
            Set(ByVal Value As String)
                If msComment <> Value Then mbChanged = True
                msComment = Value
            End Set
        End Property
        Property PaymentDate() As Date
            Get
                Return mdPaymentDate
            End Get
            Set(ByVal Value As Date)
                If mdPaymentDate <> Value Then mbChanged = True
                mdPaymentDate = Value
            End Set
        End Property
        Property PaymentByID() As Integer
            Get
                Return miPaymentByID
            End Get
            Set(ByVal Value As Integer)
                If miPaymentByID <> Value Then mbChanged = True
                miPaymentByID = Value
            End Set
        End Property
        Public ReadOnly Property PaymentBy() As CUser
            Get
                Return Lists.GetUserByID(miPaymentByID)
            End Get
        End Property
        Property BankAccountID() As Integer
            Get
                Return miBankAccountID
            End Get
            Set(ByVal Value As Integer)
                If miBankAccountID <> Value Then mbChanged = True
                miBankAccountID = Value
            End Set
        End Property
        Public ReadOnly Property BankAccount() As CBankAccount
            Get
                Return Lists.GetBankAccountByID(miBankAccountID)
            End Get
        End Property
        Public Property GUID() As String
            Get
                Return msGUID
            End Get
            Set(ByVal value As String)
                msGUID = value
            End Set
        End Property

        Public Property Changed() As Boolean
            Get
                Return mbChanged
            End Get
            Set(ByVal value As Boolean)
                mbChanged = value
            End Set
        End Property

        Public Sub ResetChanges()
            mbChanged = False
        End Sub

    End Class

End Namespace