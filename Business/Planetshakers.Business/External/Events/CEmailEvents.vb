Namespace External.Events

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CEmailEvents

        Private msEmailText As String
        Private msSubject As String
        Private msEmailHeading As String
        Private miEmailStyle As EnumWebPageStyle
        Private moAttachments As ArrayList
        Private moBreadCrumbs As ArrayList

        Public Sub New()

            msEmailText = String.Empty
            msSubject = String.Empty
            miEmailStyle = EnumWebPageStyle.GenericStyle
            moAttachments = New ArrayList
            moBreadCrumbs = New ArrayList
            moBreadCrumbs.Add("Planetshakers Registration")

        End Sub

        Public Function SendEmail(ByVal sEmailAddress As String, ByVal sName As String, ByRef sError As String) As Boolean

            Return DataAccess.SendEmail(sEmailAddress, sName, msSubject, EmailHeader + msEmailText + EmailFooter, moAttachments, sError)

        End Function

        Public Sub AddBreadCrumb(ByVal sBreadCrumbText As String)
            moBreadCrumbs.Add(sBreadCrumbText)
        End Sub

        Public Sub AttachFile(ByVal sFileName As String, ByVal sDisplayName As String)

            Dim sAttachment(1) As String
            sAttachment(0) = sFileName
            sAttachment(1) = sDisplayName
            moAttachments.Add(sAttachment)

        End Sub
        Public Property EmailText() As String
            Get
                Return msEmailText
            End Get
            Set(ByVal value As String)
                msEmailText = value
            End Set
        End Property
        Public Property Subject() As String
            Get
                Return msSubject
            End Get
            Set(ByVal value As String)
                msSubject = value
            End Set
        End Property
        Public Property EmailHeading() As String
            Get
                Return msEmailHeading
            End Get
            Set(ByVal value As String)
                msEmailHeading = value
            End Set
        End Property
        Public Property EmailStyle() As EnumWebPageStyle
            Get
                Return miEmailStyle
            End Get
            Set(ByVal value As EnumWebPageStyle)
                miEmailStyle = value
            End Set
        End Property

        Private ReadOnly Property EmailHeader() As String
            Get

                Dim sEmail As String = ""
                Dim sBreadCrumb As String = ""

                sEmail += "<html>" + vbCrLf
                sEmail += "<head>" + vbCrLf
                sEmail += " <style type=""text/css"">" + vbCrLf
                sEmail += "        body {" + vbCrLf
                sEmail += "             margin: 0px 0px 0px 0px;" + vbCrLf
                sEmail += "             font-size: 11px;" + vbCrLf
                sEmail += " 	        line-height: 13px; " + vbCrLf
                sEmail += " 	        font-family: ""Trebuchet MS"", Verdana, Arial;" + vbCrLf
                sEmail += "             text-align: left; " + vbCrLf
                sEmail += "         }" + vbCrLf
                sEmail += "         .Heading2 {" + vbCrLf
                sEmail += " 	        font-family: Verdana, Arial;" + vbCrLf
                sEmail += " 	        font-size: 18px; " + vbCrLf
                sEmail += " 	        font-weight: bold;" + vbCrLf
                sEmail += " 	        line-height: 20px;" + vbCrLf
                sEmail += "         }" + vbCrLf
                sEmail += "         .Heading4 {" + vbCrLf
                sEmail += " 	        font-size: 15px; " + vbCrLf
                sEmail += " 	        font-weight: bold;" + vbCrLf
                sEmail += "         }" + vbCrLf
                sEmail += " </style>" + vbCrLf

                sEmail += "</head>" + vbCrLf
                sEmail += "<body>" + vbCrLf
                sEmail += "     <table cellpadding=""0"" cellspacing=""0"" border=""0"" style=""width: 770px; border: solid 2px black; margin: 0px auto 0px auto;"">" + vbCrLf
                sEmail += "         <tr>" + vbCrLf
                sEmail += "             <td>" + vbCrLf
                sEmail += "                 <img src=""http://accounts.planetshakers.com/Events/images/Header_Planetshakers.png"" alt=""Planetshakers"" />" + vbCrLf
                Select Case miEmailStyle
                    Case EnumWebPageStyle.PlanetshakersEvolution09
                        sEmail += "                 <img src=""http://accounts.planetshakers.com/Events/images/Header_PlanetshakersEvolution2009.jpg"" alt=""Header"" style=""width: 770px; height: 185px"" />" + vbCrLf
                    Case EnumWebPageStyle.Planetshakers2010
                        sEmail += "                 <img src=""http://accounts.planetshakers.com/Events/images/Header_Planetshakers2010.jpg"" alt=""Header"" style=""width: 770px; height: 170px"" />" + vbCrLf
                    Case EnumWebPageStyle.Planetshakers2011
                        sEmail += "                 <img src=""http://accounts.planetshakers.com/Events/images/Header_Planetshakers2011.jpg"" alt=""Header"" style=""width: 770px; height: 109px"" />" + vbCrLf
                    Case EnumWebPageStyle.Planetshakers2012
                        sEmail += "                 <img src=""http://accounts.planetshakers.com/Events/images/Header_Planetshakers2012.jpg"" alt=""Header"" style=""width: 770px; height: 109px"" />" + vbCrLf
                    Case EnumWebPageStyle.BeautifulWoman2009
                        sEmail += "                 <img src=""http://accounts.planetshakers.com/Events/images/Header_BeautifulWoman2009.jpg"" alt=""Header"" style=""width: 770px; height: 213px"" />" + vbCrLf
                    Case EnumWebPageStyle.BeautifulWoman2010
                        sEmail += "                 <img src=""http://accounts.planetshakers.com/Events/images/Header_BeautifulWoman2010.jpg"" alt=""Header"" style=""width: 770px; height: 213px"" />" + vbCrLf
                    Case EnumWebPageStyle.BeautifulWoman2011
                        sEmail += "                 <img src=""http://accounts.planetshakers.com/Events/images/Header_BeautifulWoman2011.jpg"" alt=""Header"" style=""width: 770px; height: 58px"" />" + vbCrLf
                    Case EnumWebPageStyle.PlanetUniCamp2009
                        sEmail += "                 <img src=""http://accounts.planetshakers.com/Events/images/Header_PlanetUniCamp2009.jpg"" alt=""Header"" style=""width: 770px; height: 215px"" />" + vbCrLf
                    Case EnumWebPageStyle.PlanetUniCamp2010
                        sEmail += "                 <img src=""http://accounts.planetshakers.com/Events/images/Header_PlanetUniCamp2010.jpg"" alt=""Header"" style=""width: 770px; height: 195px"" />" + vbCrLf
                    Case EnumWebPageStyle.LeadershipSummit2011
                        sEmail += "                 <img src=""http://accounts.planetshakers.com/Events/images/Header_LeadershipSummit2011.jpg"" alt=""Header"" style=""width: 770px; height: 150px"" />" + vbCrLf
                    Case EnumWebPageStyle.BeautifulWoman2012
                        sEmail += "                 <img src=""http://accounts.planetshakers.com/Events/images/Header_BeautifulWoman2012.jpg"" alt=""Header"" style=""width: 770px; height: 150px"" />" + vbCrLf
                    Case EnumWebPageStyle.PlanetUniCamp2011
                        sEmail += "                 <img src=""http://accounts.planetshakers.com/Events/images/Header_PlanetUniCamp2011.jpg"" alt=""Header"" style=""width: 770px; height: 150px"" />" + vbCrLf
                    Case EnumWebPageStyle.MightyMen2011
                        sEmail += "                 <img src=""http://accounts.planetshakers.com/Events/images/Header_MightyMen2011.jpg"" alt=""Header"" style=""width: 770px; height: 150px"" />" + vbCrLf
                    Case EnumWebPageStyle.Planetshakers2013
                        sEmail += "                 <img src=""http://accounts.planetshakers.com/Events/images/Header_Planetshakers2013.jpg"" alt=""Header"" style=""width: 770px; height: 109px"" />" + vbCrLf
                    Case Else
                        sEmail += "                 <img src=""http://accounts.planetshakers.com/Events/images/Header_Generic.jpg"" alt=""Header"" style=""width: 770px; height: 185px"" />" + vbCrLf
                End Select
                sEmail += "			        <br />" + vbCrLf
                sEmail += "			        <br />" + vbCrLf
                sEmail += "			        <table cellspacing=""0"" cellpadding=""3"" style=""width: 700px; margin-left: 20px"">" + vbCrLf
                If msEmailHeading <> "" Then
                    sEmail += "				       <tr>" + vbCrLf
                    sEmail += "					       <td>" + vbCrLf
                    sEmail += "						       <span class=""Heading2"">" & msEmailHeading & "</span>" + vbCrLf
                    sEmail += "					       </td>" + vbCrLf
                    sEmail += "				        </tr>" + vbCrLf
                End If
                sEmail += "				        <tr>" + vbCrLf
                sEmail += "					        <td>" + vbCrLf

                Return sEmail

            End Get
        End Property

        Private ReadOnly Property EmailFooter() As String
            Get

                Dim sEmail As String = ""

                sEmail += "				                <div style=""text-align: center;"">" + vbCrLf
                sEmail += "				                    <strong>Planet Shakers Ministries International ABN 79 480 989 066</strong><br />" + vbCrLf
                sEmail += "				                    <strong>PO Box 5171, South Melbourne Vic 3205&nbsp;&nbsp;&nbsp;PHONE: +61 3 9896 7999&nbsp;&nbsp;&nbsp;FAX: (03) 9830 7683</strong><br />" + vbCrLf
                sEmail += "				                    <br />" + vbCrLf
                sEmail += "				                </div>" + vbCrLf
                sEmail += "				            </td>" + vbCrLf
                sEmail += "                     </tr>" + vbCrLf
                sEmail += "                 </table>" + vbCrLf
                sEmail += "             </td>" + vbCrLf
                sEmail += "         </tr>" + vbCrLf
                sEmail += "     </table>" + vbCrLf
                sEmail += " </body>" + vbCrLf
                sEmail += " </html>" + vbCrLf

                Return sEmail

            End Get
        End Property

    End Class

End Namespace