Namespace External.Events

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CGroupBulkPurchase

        Private miGroupBulkPurchaseID As Integer
        Private miGroupLeaderID As Integer
        Private miVenueID As Integer
        Private miAccommodationQuantity As Integer
        Private miCateringQuantity As Integer
        Private miLeadershipBreakfastQuantity As Integer
        Private mdDateAdded As Date
        Private msGUID As String

        Private mbChanged As Boolean

        Public Sub New()

            miGroupBulkPurchaseID = 0
            miGroupLeaderID = 0
            miVenueID = 0
            miAccommodationQuantity = Nothing
            miCateringQuantity = Nothing
            miLeadershipBreakfastQuantity = 0
            mdDateAdded = Nothing
            msGUID = System.Guid.NewGuid.ToString
            mbChanged = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miGroupBulkPurchaseID = oDataRow("GroupBulkPurchase_ID")
            miGroupLeaderID = oDataRow("GroupLeader_ID")
            miVenueID = oDataRow("Venue_ID")
            miAccommodationQuantity = oDataRow("AccommodationQuantity")
            miCateringQuantity = oDataRow("CateringQuantity")
            miLeadershipBreakfastQuantity = oDataRow("LeadershipBreakfastQuantity")
            mdDateAdded = oDataRow("DateAdded")
            mbChanged = False

        End Sub

        Public Sub Save(ByVal oUser As CUser)

            If mbChanged Then

                Dim oParameters As New ArrayList

                oParameters.Add(New SqlClient.SqlParameter("@GroupLeader_ID", miGroupLeaderID))
                oParameters.Add(New SqlClient.SqlParameter("@Venue_ID", miVenueID))
                oParameters.Add(New SqlClient.SqlParameter("@AccommodationQuantity", miAccommodationQuantity))
                oParameters.Add(New SqlClient.SqlParameter("@CateringQuantity", miCateringQuantity))
                oParameters.Add(New SqlClient.SqlParameter("@LeadershipBreakfastQuantity", miLeadershipBreakfastQuantity))
                oParameters.Add(New SqlClient.SqlParameter("@DateAdded", SQLDateTime(mdDateAdded)))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))

                If Me.ID = 0 Then
                    miGroupBulkPurchaseID = DataAccess.GetValue("sp_Events_GroupBulkPurchaseInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@GroupBulkPurchase_ID", miGroupBulkPurchaseID))
                    DataAccess.ExecuteCommand("sp_Events_GroupBulkPurchaseUpdate", oParameters)
                End If

                mbChanged = False

            End If

        End Sub

        ReadOnly Property ID() As Integer
            Get
                Return miGroupBulkPurchaseID
            End Get
        End Property
        Property GroupLeaderID() As Integer
            Get
                Return miGroupLeaderID
            End Get
            Set(ByVal value As Integer)
                If miGroupLeaderID <> value Then mbChanged = True
                miGroupLeaderID = value
            End Set
        End Property
        Property VenueID() As Integer
            Get
                Return miVenueID
            End Get
            Set(ByVal value As Integer)
                If miVenueID <> value Then mbChanged = True
                miVenueID = value
            End Set
        End Property
        Public ReadOnly Property Venue() As CVenue
            Get
                Return Lists.External.Events.GetVenueByID(miVenueID)
            End Get
        End Property
        Property AccommodationQuantity() As Integer
            Get
                Return miAccommodationQuantity
            End Get
            Set(ByVal Value As Integer)
                If miAccommodationQuantity <> Value Then mbChanged = True
                miAccommodationQuantity = Value
            End Set
        End Property
        Property CateringQuantity() As Integer
            Get
                Return miCateringQuantity
            End Get
            Set(ByVal Value As Integer)
                If miCateringQuantity <> Value Then mbChanged = True
                miCateringQuantity = Value
            End Set
        End Property
        Property LeadershipBreakfastQuantity() As Integer
            Get
                Return miLeadershipBreakfastQuantity
            End Get
            Set(ByVal Value As Integer)
                If miLeadershipBreakfastQuantity <> Value Then mbChanged = True
                miLeadershipBreakfastQuantity = Value
            End Set
        End Property

        Property DateAdded() As Date
            Get
                Return mdDateAdded
            End Get
            Set(ByVal Value As Date)
                If mdDateAdded <> Value Then mbChanged = True
                mdDateAdded = Value
            End Set
        End Property
        ReadOnly Property GUID() As String
            Get
                GUID = msGUID
            End Get
        End Property

        Public ReadOnly Property Changed() As Boolean
            Get
                Return mbChanged
            End Get
        End Property

        Public ReadOnly Property AccommodationCost() As Decimal
            Get
                Return (AccommodationQuantity * Venue.Conference.AccommodationCost)
            End Get
        End Property
        Public ReadOnly Property CateringCost() As Decimal
            Get
                Return (CateringQuantity * Venue.Conference.CateringCost)
            End Get
        End Property
        Public ReadOnly Property LeadershipBreakfastCost() As Decimal
            Get
                Return (LeadershipBreakfastQuantity * Venue.Conference.LeadershipBreakfastCost)
            End Get
        End Property

        Public ReadOnly Property TotalCost() As Decimal
            Get
                Return (AccommodationQuantity * Venue.Conference.AccommodationCost) + _
                       (CateringQuantity * Venue.Conference.CateringCost) + _
                       (LeadershipBreakfastQuantity * Venue.Conference.LeadershipBreakfastCost)
            End Get
        End Property
    End Class

End Namespace