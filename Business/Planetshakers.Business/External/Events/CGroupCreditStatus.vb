Namespace External.Events

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CGroupCreditStatus

        Private miGroupCreditStatusID As Integer
        Private msName As String

        Public Sub New()

            miGroupCreditStatusID = 0
            msName = String.Empty

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miGroupCreditStatusID = oDataRow("GroupCreditStatus_ID")
            msName = oDataRow("Name")

        End Sub

        Public Property ID() As Integer
            Get
                Return miGroupCreditStatusID
            End Get
            Set(ByVal Value As Integer)
                miGroupCreditStatusID = Value
            End Set
        End Property
        Public Property Name() As String
            Get
                Return msName
            End Get
            Set(ByVal Value As String)
                msName = Value
            End Set
        End Property

        Public Overrides Function ToString() As String
            Return Name
        End Function
    End Class

End Namespace