Namespace External.Events

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CPOSTransaction

        Private miPOSTransactionID As Integer
        Private miSaleType As EnumSaleType
        Private miTransactionType As EnumTransType
        Private miContactID As Integer
        Private moContact As CContact
        Private msShipTo As String
        Private miOriginalTransactionID As Integer
        Private moOriginalTransaction As CPOSTransaction
        Private mdTransactionDate As Date
        Private moUser As CUser
        Private moRegister As CPOSRegister
        Private moVenue As CVenue

        Private mcPOSTransactionLine As CArrayList(Of CPOSTransactionLine)
        Private mcPOSTransactionPayment As CArrayList(Of CPOSTransactionPayment)

        Public Sub New()
            NewPOSTransaction()
        End Sub
        Private Sub NewPOSTransaction()
            miPOSTransactionID = 0
            miSaleType = 0
            miTransactionType = 0
            miContactID = 0
            moContact = Nothing
            msShipTo = ""
            miOriginalTransactionID = 0
            moOriginalTransaction = Nothing
            mdTransactionDate = GetDateTime
            moUser = Nothing
            moRegister = Nothing
            moVenue = Nothing
            mcPOSTransactionLine = New CArrayList(Of CPOSTransactionLine)
            mcPOSTransactionPayment = New CArrayList(Of CPOSTransactionPayment)
        End Sub

        Public Function Load(ByVal iPOSTransactionID As Long) As Boolean

            Dim oDataSet As New DataSet
            Dim oTableMappings As New ArrayList

            oTableMappings.Add(New System.Data.Common.DataTableMapping("Table", "POSTransaction"))
            oTableMappings.Add(New System.Data.Common.DataTableMapping("Table1", "POSTransactionLine"))
            oTableMappings.Add(New System.Data.Common.DataTableMapping("Table2", "POSTransactionPayment"))

            oDataSet = DataAccess.GetDataSet("sp_Events_POSTransactionRead " & iPOSTransactionID.ToString, oTableMappings)

            oDataSet.Relations.Add("POSTransactionLine", _
                oDataSet.Tables("POSTransaction").Columns("POSTransaction_ID"), _
                oDataSet.Tables("POSTransactionLine").Columns("POSTransaction_ID"))

            oDataSet.Relations.Add("POSTransactionPayment", _
                oDataSet.Tables("POSTransaction").Columns("POSTransaction_ID"), _
                oDataSet.Tables("POSTransactionPayment").Columns("POSTransaction_ID"))

            If oDataSet.Tables("POSTransaction").Rows.Count = 0 Then
                Load = False
            Else

                Dim oDataRow As DataRow
                oDataRow = oDataSet.Tables("POSTransaction").Rows(0)

                ID = oDataRow("POSTransaction_ID")
                SaleType = oDataRow("SaleType")
                TransactionType = oDataRow("TransactionType")
                ContactID = IsNull(oDataRow("Contact_ID"), 0)
                ShipTo = oDataRow("ShipTo")
                OriginalTransactionID = oDataRow("OriginalDocket")
                TransactionDate = oDataRow("TransactionDate")
                User = Lists.GetUserByID(oDataRow("User_ID"))
                Register = Lists.External.Events.GetPOSRegisterByID(oDataRow("POSRegister_ID"))
                If Not IsDBNull(oDataRow("Venue_ID")) Then Venue = Lists.External.Events.GetVenueByID(oDataRow("Venue_ID"))

                Dim oChildRow As DataRow

                For Each oChildRow In oDataRow.GetChildRows(oDataSet.Relations("POSTransactionLine"))
                    Dim oPOSTransactionLine As New CPOSTransactionLine
                    oPOSTransactionLine.NewPOSTransactionLine(oChildRow)
                    mcPOSTransactionLine.Add(oPOSTransactionLine)
                Next

                For Each oChildRow In oDataRow.GetChildRows(oDataSet.Relations("POSTransactionPayment"))
                    Dim oPOSTransactionPayment As New CPOSTransactionPayment(oChildRow)
                    mcPOSTransactionPayment.Add(oPOSTransactionPayment)
                Next

                Load = True

            End If

        End Function

        Public Sub Save()

            Dim oPOSTransactionLine As CPOSTransactionLine
            Dim oPOSTransactionPayment As CPOSTransactionPayment

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@SaleType", miSaleType))
            oParameters.Add(New SqlClient.SqlParameter("@TransactionType", miTransactionType))
            oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", IIf(ContactID > 0, ContactID, DBNull.Value)))
            oParameters.Add(New SqlClient.SqlParameter("@ShipTo", msShipTo))
            oParameters.Add(New SqlClient.SqlParameter("@OriginalDocket", miOriginalTransactionID))
            oParameters.Add(New SqlClient.SqlParameter("@TransactionDate", SQLDateTime(mdTransactionDate)))
            oParameters.Add(New SqlClient.SqlParameter("@User_ID", SQLObject(moUser)))
            oParameters.Add(New SqlClient.SqlParameter("@POSRegister_ID", SQLObject(moRegister)))
            oParameters.Add(New SqlClient.SqlParameter("@Venue_ID", SQLObject(moVenue)))


            If ID = 0 Then
                miPOSTransactionID = DataAccess.GetValue("sp_Events_POSTransactionInsert", oParameters)
            Else
                oParameters.Add(New SqlClient.SqlParameter("@POSTransaction_ID", miPOSTransactionID))
                DataAccess.ExecuteCommand("sp_Events_POSTransactionUpdate", oParameters)
            End If

            For Each oPOSTransactionLine In mcPOSTransactionLine
                If oPOSTransactionLine.ID = 0 Then
                    oPOSTransactionLine.ID = DataAccess.GetValue("sp_Events_POSTransactionLineInsert " & MSQLUtilities.SQL(ID) & ", " & MSQLUtilities.SQL(oPOSTransactionLine.Product.ID) & ", " & MSQLUtilities.SQL(oPOSTransactionLine.Price) & ", " & MSQLUtilities.SQL(oPOSTransactionLine.Discount) & ", " & MSQLUtilities.SQL(oPOSTransactionLine.Quantity) & ", " & MSQLUtilities.SQL(oPOSTransactionLine.QuantitySupplied) & ", " & MSQLUtilities.SQL(oPOSTransactionLine.QuantityReturned) & ", " & IIf(oPOSTransactionLine.OriginalPOSTransactionLineID > 0, MSQLUtilities.SQL(oPOSTransactionLine.OriginalPOSTransactionLineID), "null") & ", " & MSQLUtilities.SQL(oPOSTransactionLine.Title) & ", " & MSQLUtilities.SQL(oPOSTransactionLine.Net))
                Else
                    DataAccess.ExecuteCommand("sp_Events_POSTransactionLineUpdate " & MSQLUtilities.SQL(oPOSTransactionLine.ID) & ", " & MSQLUtilities.SQL(ID) & ", " & MSQLUtilities.SQL(oPOSTransactionLine.Product.ID) & ", " & MSQLUtilities.SQL(oPOSTransactionLine.Price) & ", " & MSQLUtilities.SQL(oPOSTransactionLine.Discount) & ", " & MSQLUtilities.SQL(oPOSTransactionLine.Quantity) & ", " & MSQLUtilities.SQL(oPOSTransactionLine.QuantitySupplied) & ", " & MSQLUtilities.SQL(oPOSTransactionLine.QuantityReturned) & ", " & IIf(oPOSTransactionLine.OriginalPOSTransactionLineID > 0, MSQLUtilities.SQL(oPOSTransactionLine.OriginalPOSTransactionLineID), "null") & ", " & MSQLUtilities.SQL(oPOSTransactionLine.Title) & ", " & MSQLUtilities.SQL(oPOSTransactionLine.Net))
                End If
            Next

            For Each oPOSTransactionPayment In mcPOSTransactionPayment
                If oPOSTransactionPayment.ID = 0 Then
                    oPOSTransactionPayment.ID = DataAccess.GetValue("sp_Events_POSTransactionPaymentInsert " & MSQLUtilities.SQL(ID) & ", " & MSQLUtilities.SQL(oPOSTransactionPayment.PaymentType.ID) & ", " & MSQLUtilities.SQL(oPOSTransactionPayment.PaymentAmount) & ", " & MSQLUtilities.SQL(oPOSTransactionPayment.PaymentTendered) & ", " & MSQLUtilities.SQL(oPOSTransactionPayment.ChequeDrawer) & ", " & MSQLUtilities.SQL(oPOSTransactionPayment.ChequeBank) & ", " & MSQLUtilities.SQL(oPOSTransactionPayment.ChequeBranch) & ", " & MSQLUtilities.SQL(oPOSTransactionPayment.CreditCardType.ID))
                Else
                    DataAccess.ExecuteCommand("sp_Events_POSTransactionPaymentUpdate " & MSQLUtilities.SQL(oPOSTransactionPayment.ID) & ", " & MSQLUtilities.SQL(ID) & ", " & MSQLUtilities.SQL(oPOSTransactionPayment.PaymentType.ID) & ", " & MSQLUtilities.SQL(oPOSTransactionPayment.PaymentAmount) & ", " & MSQLUtilities.SQL(oPOSTransactionPayment.PaymentTendered) & ", " & MSQLUtilities.SQL(oPOSTransactionPayment.ChequeDrawer) & ", " & MSQLUtilities.SQL(oPOSTransactionPayment.ChequeBank) & ", " & MSQLUtilities.SQL(oPOSTransactionPayment.ChequeBranch) & ", " & MSQLUtilities.SQL(oPOSTransactionPayment.CreditCardType.ID))
                End If
            Next

            If Me.OriginalTransaction IsNot Nothing Then
                Me.OriginalTransaction.Save()
            End If

        End Sub

        Property ID() As Integer
            Get
                Return miPOSTransactionID
            End Get
            Set(ByVal Value As Integer)
                miPOSTransactionID = Value
            End Set
        End Property
        Property SaleType() As EnumSaleType
            Get
                Return miSaleType
            End Get
            Set(ByVal Value As EnumSaleType)
                miSaleType = Value
            End Set
        End Property
        Property TransactionType() As EnumTransType
            Get
                Return miTransactionType
            End Get
            Set(ByVal Value As EnumTransType)
                miTransactionType = Value
            End Set
        End Property
        Property ContactID() As Integer
            Get
                Return miContactID
            End Get
            Set(ByVal Value As Integer)
                miContactID = Value
            End Set
        End Property
        Property Contact() As CContact
            Get
                If moContact Is Nothing Then
                    If miContactID = 0 Then Return Nothing
                    moContact = New CContact
                    moContact.LoadByID(miContactID)
                End If
                Return moContact
            End Get
            Set(ByVal Value As CContact)
                moContact = Value
                If moContact IsNot Nothing Then miContactID = moContact.ID
            End Set
        End Property
        Property ShipTo() As String
            Get
                Return msShipTo
            End Get
            Set(ByVal value As String)
                msShipTo = value
            End Set
        End Property
        Property OriginalTransactionID() As Integer
            Get
                Return miOriginalTransactionID
            End Get
            Set(ByVal Value As Integer)
                miOriginalTransactionID = Value
            End Set
        End Property
        Property OriginalTransaction() As CPOSTransaction
            Get
                If moOriginalTransaction Is Nothing Then
                    If miOriginalTransactionID = 0 Then Return Nothing
                    moOriginalTransaction = New CPOSTransaction
                    moOriginalTransaction.Load(miOriginalTransactionID)
                End If
                Return moOriginalTransaction
            End Get
            Set(ByVal Value As CPOSTransaction)
                moOriginalTransaction = Value
                If moOriginalTransaction IsNot Nothing Then miOriginalTransactionID = moOriginalTransaction.ID
            End Set
        End Property
        Property TransactionDate() As Date
            Get
                Return mdTransactionDate
            End Get
            Set(ByVal Value As Date)
                mdTransactionDate = Value
            End Set
        End Property
        Property User() As CUser
            Get
                Return moUser
            End Get
            Set(ByVal Value As CUser)
                moUser = Value
            End Set
        End Property
        Property Register() As CPOSRegister
            Get
                Return moRegister
            End Get
            Set(ByVal Value As CPOSRegister)
                moRegister = Value
            End Set
        End Property
        Property Venue() As CVenue
            Get
                Return moVenue
            End Get
            Set(ByVal Value As CVenue)
                moVenue = Value
            End Set
        End Property

        ReadOnly Property Total() As Decimal
            Get
                Dim oPOSTransactionLine As CPOSTransactionLine
                For Each oPOSTransactionLine In Lines
                    Total += oPOSTransactionLine.Net
                Next
                Total = Math.Abs(Total)
            End Get
        End Property
        ReadOnly Property TotalSansDiscount() As Decimal
            Get
                Dim oPOSTransactionLine As CPOSTransactionLine
                For Each oPOSTransactionLine In Lines
                    TotalSansDiscount += (oPOSTransactionLine.Price * oPOSTransactionLine.Quantity)
                Next
                TotalSansDiscount = Math.Abs(TotalSansDiscount)
            End Get
        End Property
        ReadOnly Property TotalDiscountAmount() As Decimal
            Get
                TotalDiscountAmount = TotalSansDiscount - Total
            End Get
        End Property
        ReadOnly Property TotalPayments() As Decimal
            Get
                Dim oPOSTransactionPayment As CPOSTransactionPayment
                For Each oPOSTransactionPayment In mcPOSTransactionPayment
                    TotalPayments += oPOSTransactionPayment.PaymentAmount
                Next
                TotalPayments = Math.Abs(TotalPayments)
            End Get
        End Property
        ReadOnly Property Lines() As CArrayList(Of CPOSTransactionLine)
            Get
                Return mcPOSTransactionLine
            End Get
        End Property
        ReadOnly Property Payments() As CArrayList(Of CPOSTransactionPayment)
            Get
                Return mcPOSTransactionPayment
            End Get
        End Property


    End Class

End Namespace