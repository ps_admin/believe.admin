﻿<Serializable()> _
<CLSCompliant(True)> _
Public Class CPriorityTag
    Private msPriorityTagID As String
    Private miAccessLevelID As Integer
    Private mbIsActive As Boolean
    Private moDateIssue As Date
    Private moDateDeactivation As Date

    Public Sub New()
        msPriorityTagID = ""
        miAccessLevelID = 0
        mbIsActive = False
        moDateIssue = New Date
        moDateDeactivation = New Date
    End Sub

    Public Sub New(ByRef oDataRow As DataRow)
        Me.New()
        LoadDataRow(oDataRow)
    End Sub

    Public Sub New(ByVal sPriorityTagID As String)
        Me.New()
        Dim sSQL As String = "SELECT * FROM Events_PriorityTag WHERE PriorityTag_ID = '" & sPriorityTagID & "'"
        Dim oDataTable As DataTable = Business.DataAccess.GetDataTable(sSQL)

        If oDataTable.Rows.Count > 0 Then
            LoadDataRow(oDataTable.Rows(0))
        End If
    End Sub

    Private Sub LoadDataRow(ByRef oDataRow As DataRow)
        msPriorityTagID = oDataRow("PriorityTag_ID")
        miAccessLevelID = CType(oDataRow("AccessLevel_ID"), Integer)
        mbIsActive = CType(oDataRow("PriorityTag_Active"), Boolean)
        moDateIssue = DateTime.Parse(oDataRow("PriorityTag_IssueDate"))

        If IsDBNull(oDataRow("PriorityTag_DeactivationDate")) Then
            moDateDeactivation = Nothing
        Else
            moDateDeactivation = DateTime.Parse(oDataRow("PriorityTag_DeactivationDate"))
        End If
    End Sub

    Public ReadOnly Property ID() As String
        Get
            Return msPriorityTagID
        End Get
    End Property

    Public ReadOnly Property AcessLevelID() As Integer
        Get
            Return miAccessLevelID
        End Get
    End Property

    Public ReadOnly Property IsActive() As Boolean
        Get
            Return mbIsActive
        End Get
    End Property

    Public ReadOnly Property DateIssue() As Date
        Get
            Return moDateIssue
        End Get
    End Property

    Public ReadOnly Property DateDeactivation() As Date
        Get
            Return moDateDeactivation
        End Get
    End Property
End Class
