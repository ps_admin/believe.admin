Namespace External.Events

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CRegistrationType
        Implements IComparable(Of CRegistrationType)

        Private miRegistrationTypeID As Integer
        Private miConferenceID As Integer
        Private msName As String
        Private miRegistrationTypeReportGroupID As Integer
        Private miRegistrationTypeSaleGroupID As Integer
        Private mdRegistrationCost As Decimal
        Private mbAvailableOnlineSingle As Boolean
        Private mbAvailableOnlineGroup As Boolean
        Private miGroupMinimumQuantity As Integer
        Private mdStartDate As Date
        Private mdEndDate As Date
        Private mbIsChildRegistrationType As Boolean
        Private mbIsYouthRegistrationType As Boolean
        Private miSortOrder As Integer

        Private moVenue As ArrayList

        Public Sub New()

            miRegistrationTypeID = 0
            miConferenceID = 0
            msName = ""
            miRegistrationTypeReportGroupID = 0
            miRegistrationTypeSaleGroupID = 0
            mdRegistrationCost = 0
            mbAvailableOnlineSingle = False
            mbAvailableOnlineGroup = False
            miGroupMinimumQuantity = 0
            mdStartDate = Nothing
            mdEndDate = Nothing
            mbIsChildRegistrationType = False
            mbIsYouthRegistrationType = False
            miSortOrder = 0

            moVenue = New ArrayList

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            Dim oMinimumQuantity As Array = oDataRow.GetChildRows("RegistrationTypeQuantity")
            
            miRegistrationTypeID = oDataRow("RegistrationType_ID")
            miConferenceID = oDataRow("Conference_ID")
            msName = oDataRow("RegistrationType")
            miRegistrationTypeReportGroupID = IsNull(oDataRow("RegistrationTypeReportGroup_ID"), 0)
            miRegistrationTypeSaleGroupID = IsNull(oDataRow("RegistrationTypeSaleGroup_ID"), 0)
            mdRegistrationCost = oDataRow("RegistrationCost")
            mbAvailableOnlineSingle = oDataRow("AvailableOnlineSingle")
            mbAvailableOnlineGroup = oDataRow("AvailableOnlineGroup")
            miGroupMinimumQuantity = oDataRow("GroupMinimumQuantity")
            mdStartDate = oDataRow("StartDate")
            mdEndDate = oDataRow("EndDate")
            mbIsChildRegistrationType = oDataRow("IsChildRegistrationType")
            mbIsYouthRegistrationType = oDataRow("IsYouthRegistrationType")
            miSortOrder = oDataRow("SortOrder")

            If oMinimumQuantity.Length < 1 Then
                miGroupMinimumQuantity = oDataRow("GroupMinimumQuantity")
            Else
                For Each oQuantityRow As DataRow In oMinimumQuantity
                    miGroupMinimumQuantity = oQuantityRow("MinimumQuantity")
                Next
            End If

            Dim oVenueRow As DataRow
            For Each oVenueRow In oDataRow.GetChildRows("RegistrationType")
                moVenue.Add(Lists.External.Events.GetVenueByID(oVenueRow("Venue_ID")))
            Next

        End Sub

        ReadOnly Property Venue() As ArrayList
            Get
                Return moVenue
            End Get
        End Property

        Property ID() As Integer
            Get
                ID = miRegistrationTypeID
            End Get
            Set(ByVal Value As Integer)
                miRegistrationTypeID = Value
            End Set
        End Property
        Property ConferenceID() As Integer
            Get
                Return miConferenceID
            End Get
            Set(ByVal Value As Integer)
                miConferenceID = Value
            End Set
        End Property
        ReadOnly Property Conference() As CConference
            Get
                Return Lists.External.Events.GetConferenceByID(miConferenceID)
            End Get
        End Property
        Property Name() As String
            Get
                Return msName
            End Get
            Set(ByVal Value As String)
                msName = Value
            End Set
        End Property

        Property RegistrationTypeReportGroupID() As Integer
            Get
                Return miRegistrationTypeReportGroupID
            End Get
            Set(ByVal Value As Integer)
                miRegistrationTypeReportGroupID = Value
            End Set
        End Property
        Public ReadOnly Property RegistrationTypeReportGroup() As CRegistrationTypeReportGroup
            Get
                Return Lists.External.Events.GetRegistrationTypeReportGroupByID(miRegistrationTypeReportGroupID)
            End Get
        End Property

        Property RegistrationTypeSaleGroupID() As Integer
            Get
                Return miRegistrationTypeSaleGroupID
            End Get
            Set(ByVal Value As Integer)
                miRegistrationTypeSaleGroupID = Value
            End Set
        End Property
        Public ReadOnly Property RegistrationTypeSaleGroup() As CRegistrationTypeSaleGroup
            Get
                Return Lists.External.Events.GetRegistrationTypeSaleGroupByID(miRegistrationTypeSaleGroupID)
            End Get
        End Property
        Property RegistrationCost() As Decimal
            Get
                Return mdRegistrationCost
            End Get
            Set(ByVal Value As Decimal)
                mdRegistrationCost = Value
            End Set
        End Property
        Public Property AvailableOnlineSingle() As Boolean
            Get
                Return mbAvailableOnlineSingle
            End Get
            Set(ByVal value As Boolean)
                mbAvailableOnlineSingle = value
            End Set
        End Property
        Public Property AvailableOnlineGroup() As Boolean
            Get
                Return mbAvailableOnlineGroup
            End Get
            Set(ByVal value As Boolean)
                mbAvailableOnlineGroup = value
            End Set
        End Property
        Public Property GroupMinimumQuantity() As Integer
            Get
                Return miGroupMinimumQuantity
            End Get
            Set(ByVal value As Integer)
                miGroupMinimumQuantity = value
            End Set
        End Property
        Property StartDate() As Date
            Get
                Return mdStartDate
            End Get
            Set(ByVal Value As Date)
                mdStartDate = Value
            End Set
        End Property
        Property EndDate() As Date
            Get
                Return mdEndDate
            End Get
            Set(ByVal Value As Date)
                mdEndDate = Value
            End Set
        End Property
        Public Property IsChildRegistrationType() As Boolean
            Get
                Return mbIsChildRegistrationType
            End Get
            Set(ByVal value As Boolean)
                mbIsChildRegistrationType = value
            End Set
        End Property
        Public Property IsYouthRegistrationType() As Boolean
            Get
                Return mbIsYouthRegistrationType
            End Get
            Set(ByVal value As Boolean)
                mbIsYouthRegistrationType = value
            End Set
        End Property
        Public ReadOnly Property IsComboRegistration() As Boolean
            Get
                For Each oComboDetail As CRegistrationTypeComboDetail In Lists.External.Events.RegistrationTypeComboDetail.Values
                    If oComboDetail.RegistrationTypeID1 = ID Or oComboDetail.RegistrationTypeID2 = ID Then
                        Return True
                    End If
                Next
                Return False
            End Get
        End Property
        Public ReadOnly Property IsComboRegistrationPrimary() As Boolean
            Get
                For Each oComboDetail As CRegistrationTypeComboDetail In Lists.External.Events.RegistrationTypeComboDetail.Values
                    If (oComboDetail.RegistrationTypeID1 = ID) Then Return True
                Next
            End Get
        End Property
        Public ReadOnly Property IsComboRegistrationSecondary(Optional ByVal oConference As CConference = Nothing) As Boolean
            Get
                For Each oComboDetail As CRegistrationTypeComboDetail In Lists.External.Events.RegistrationTypeComboDetail.Values
                    If oConference Is Nothing OrElse oComboDetail.RegistrationTypeCombo.ConferenceID = oConference.ID Then
                        If (oComboDetail.RegistrationTypeID2 = ID) Then Return True
                    End If
                Next
            End Get
        End Property
        Public ReadOnly Property IsComboRegistration(ByVal oRegistrationType As CRegistrationType) As Boolean
            Get
                Return RegistrationComboDetail(oRegistrationType) IsNot Nothing
            End Get
        End Property

        Public ReadOnly Property RegistrationComboDetail(ByVal oRegistrationType As CRegistrationType) As CRegistrationTypeComboDetail
            Get
                For Each oComboDetail As CRegistrationTypeComboDetail In Lists.External.Events.RegistrationTypeComboDetail.Values
                    If (oComboDetail.RegistrationTypeID1 = ID And oComboDetail.RegistrationTypeID2 = oRegistrationType.ID) Or _
                       (oComboDetail.RegistrationTypeID2 = ID And oComboDetail.RegistrationTypeID1 = oRegistrationType.ID) Then
                        Return oComboDetail
                    End If
                Next
                Return Nothing
            End Get
        End Property
        Property SortOrder() As Integer
            Get
                Return miSortOrder
            End Get
            Set(ByVal value As Integer)
                miSortOrder = value
            End Set
        End Property

        Public Overrides Function toString() As String
            Return msName
        End Function

        Public ReadOnly Property NameAndCost() As String
            Get
                Return msName & " - " & Format(mdRegistrationCost, "$#0.00")
            End Get
        End Property

        Public Function CompareTo(ByVal other As CRegistrationType) As Integer Implements System.IComparable(Of CRegistrationType).CompareTo
            Return SortOrder.CompareTo(other.SortOrder)
        End Function

    End Class

End Namespace