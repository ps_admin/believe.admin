Namespace External.Events

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CRegistrationTypeComboDetail

        Private miRegistrationTypeComboDetailID As Integer
        Private msName As String
        Private miRegistrationTypeComboID As Integer
        Private miRegistrationTypeID1 As Integer
        Private miRegistrationTypeID2 As Integer

        Public Sub New()

            miRegistrationTypeComboDetailID = 0
            msName = String.Empty
            miRegistrationTypeComboID = 0
            miRegistrationTypeID1 = 0
            miRegistrationTypeID2 = 0

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miRegistrationTypeComboDetailID = oDataRow("RegistrationTypeComboDetail_ID")
            msName = oDataRow("Name")
            miRegistrationTypeComboID = oDataRow("RegistrationTypeCombo_ID")
            miRegistrationTypeID1 = oDataRow("RegistrationType_ID1")
            miRegistrationTypeID2 = oDataRow("RegistrationType_ID2")

        End Sub

        Public Property ID() As Integer
            Get
                Return miRegistrationTypeComboDetailID
            End Get
            Set(ByVal Value As Integer)
                miRegistrationTypeComboDetailID = Value
            End Set
        End Property
        Public Property Name() As String
            Get
                Return msName
            End Get
            Set(ByVal Value As String)
                msName = Value
            End Set
        End Property
        Public Property RegistrationTypeComboID() As Integer
            Get
                Return miRegistrationTypeComboID
            End Get
            Set(ByVal Value As Integer)
                miRegistrationTypeComboID = Value
            End Set
        End Property
        Public ReadOnly Property RegistrationTypeCombo() As CRegistrationTypeCombo
            Get
                Return Lists.External.Events.GetRegistrationTypeComboByID(miRegistrationTypeComboID)
            End Get
        End Property

        Public Property RegistrationTypeID1() As Integer
            Get
                Return miRegistrationTypeID1
            End Get
            Set(ByVal Value As Integer)
                miRegistrationTypeID1 = Value
            End Set
        End Property
        Public ReadOnly Property RegistrationType1() As CRegistrationType
            Get
                Return Lists.External.Events.GetRegistrationTypeByID(miRegistrationTypeID1)
            End Get
        End Property
        Public Property RegistrationTypeID2() As Integer
            Get
                Return miRegistrationTypeID2
            End Get
            Set(ByVal Value As Integer)
                miRegistrationTypeID2 = Value
            End Set
        End Property
        Public ReadOnly Property RegistrationType2() As CRegistrationType
            Get
                Return Lists.External.Events.GetRegistrationTypeByID(miRegistrationTypeID2)
            End Get
        End Property

    End Class

End Namespace