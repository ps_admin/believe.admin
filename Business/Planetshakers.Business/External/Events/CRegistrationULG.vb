﻿<Serializable()> _
<CLSCompliant(True)> _
Public Class CRegistrationULG

    Private miULGID As Integer
    Private msName As String
    Private miSortOrder As Integer

    Public Sub New(ByVal oDataRow As DataRow)
        miULGID = oDataRow("ULG_ID")
        msName = oDataRow("Name")
        miSortOrder = oDataRow("SortOrder")
    End Sub

    Public ReadOnly Property ID() As Integer
        Get
            Return miULGID
        End Get
    End Property
    Public ReadOnly Property Name() As String
        Get
            Return msName
        End Get
    End Property
    Public ReadOnly Property SortOrder() As Integer
        Get
            Return miSortOrder
        End Get
    End Property

    Public Overrides Function ToString() As String
        Return msName
    End Function
End Class
