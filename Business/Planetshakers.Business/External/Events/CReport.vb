Namespace External.Events

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CReport

        Private miReportID As Integer
        Private moConference As CConference
        Private msReportName As String
        Private msReportSQL As String

        Private moColumns As ArrayList
        Private moParameters As ArrayList

        Public Sub New()

            miReportID = 0
            moConference = Nothing
            msReportName = String.Empty
            msReportSQL = String.Empty

        End Sub

        Public Sub New(ByRef oDataRow As DataRow, ByRef oColumnRows() As DataRow, ByRef oParameterRows() As DataRow)

            Me.New()

            miReportID = oDataRow("Report_ID")
            moConference = Lists.External.Events.GetConferenceByID(oDataRow("Conference_ID"))
            msReportName = oDataRow("ReportName")
            msReportSQL = oDataRow("ReportSQL")

            Dim oColumnRow As DataRow
            For Each oColumnRow In oColumnRows
                moColumns.Add(New CReportColumn(oColumnRow))
            Next

            Dim oParameterRow As DataRow
            For Each oParameterRow In oParameterRows
                moParameters.Add(New CReportParameter(oParameterRow))
            Next

        End Sub

        Property ID() As Integer
            Get
                Return miReportID
            End Get
            Set(ByVal Value As Integer)
                miReportID = Value
            End Set
        End Property
        Property Conference() As CConference
            Get
                Return moConference
            End Get
            Set(ByVal Value As CConference)
                moConference = Value
            End Set
        End Property
        Property ReportName() As String
            Get
                Return msReportName
            End Get
            Set(ByVal Value As String)
                msReportName = Value
            End Set
        End Property
        Property ReportSQL() As String
            Get
                Return msReportSQL
            End Get
            Set(ByVal Value As String)
                msReportSQL = Value
            End Set
        End Property

        Public Overrides Function ToString() As String
            Return ReportName
        End Function

    End Class

End Namespace
