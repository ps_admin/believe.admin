Namespace External.Events

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CReportColumn

        Private miReportColumnID As Integer
        Private msColumnName As String
        Private msColumnDescription As String
        Private mbShowColumn As Boolean

        Public Sub New()

            miReportColumnID = 0
            msColumnName = String.Empty
            msColumnDescription = String.Empty
            mbShowColumn = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miReportColumnID = oDataRow("Report_ID")
            msColumnName = oDataRow("ColumnName")
            msColumnDescription = oDataRow("ColumnDescription")
            mbShowColumn = oDataRow("ShowColumn")
        End Sub

        Property ID() As Integer
            Get
                Return miReportColumnID
            End Get
            Set(ByVal Value As Integer)
                miReportColumnID = Value
            End Set
        End Property
        Property ColumnName() As String
            Get
                Return msColumnName
            End Get
            Set(ByVal Value As String)
                msColumnName = Value
            End Set
        End Property
        Property ColumnDescription() As String
            Get
                Return msColumnDescription
            End Get
            Set(ByVal Value As String)
                msColumnDescription = Value
            End Set
        End Property
        Property ShowColumn() As Boolean
            Get
                Return mbShowColumn
            End Get
            Set(ByVal Value As Boolean)
                mbShowColumn = Value
            End Set
        End Property

    End Class

End Namespace
