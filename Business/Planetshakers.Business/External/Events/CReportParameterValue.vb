Namespace External.Events

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CReportParameterValue

        Private miReportParameterValueID As Integer
        Private msReportParameterValueText As String

        Public Sub New()
            NewParameterValue()
        End Sub

        Private Sub NewParameterValue()
            miReportParameterValueID = 0
            msReportParameterValueText = ""
        End Sub

        Property ID() As Integer
            Get
                Return miReportParameterValueID
            End Get
            Set(ByVal Value As Integer)
                miReportParameterValueID = Value
            End Set
        End Property
        Property Text() As String
            Get
                Return msReportParameterValueText
            End Get
            Set(ByVal Value As String)
                msReportParameterValueText = Value
            End Set
        End Property

    End Class

End Namespace