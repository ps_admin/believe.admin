Namespace External.Events

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CStockAdjustment

        Private miStockAdjustmentID As Integer
        Private miProductID As Integer
        Private miVenueID As Integer
        Private miQuantity As Integer
        Private miAdjustmentType As EnumJournalAdjustmentType
        Private mdDateAdded As Date
        Private msComments As String

        Private mbChanged As Boolean

        Public Enum EnumJournalAdjustmentType
            OpeningBalance = 1
            Adjustment = 2
            ClosingBalance = 3
            ReturnStock = 4
        End Enum

        Public Sub New()

            miStockAdjustmentID = 0
            miProductID = 0
            miVenueID = 0
            miQuantity = 0
            miAdjustmentType = EnumJournalAdjustmentType.Adjustment
            mdDateAdded = GetDateTime
            msComments = ""

            mbChanged = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miStockAdjustmentID = oDataRow("StockJournal_ID")
            miProductID = oDataRow("Product_ID")
            miVenueID = oDataRow("Venue_ID")
            miQuantity = oDataRow("Quantity")
            miAdjustmentType = oDataRow("AdjustmentType")
            mdDateAdded = oDataRow("DateAdded")
            msComments = oDataRow("Comments")

            mbChanged = False

        End Sub

        Public Sub Save()

            If mbChanged Then

                Dim oParameters As New ArrayList

                oParameters.Add(New SqlClient.SqlParameter("@Product_ID", miProductID))
                oParameters.Add(New SqlClient.SqlParameter("@Venue_ID", miVenueID))
                oParameters.Add(New SqlClient.SqlParameter("@Quantity", miQuantity))
                oParameters.Add(New SqlClient.SqlParameter("@AdjustmentType", miAdjustmentType))
                oParameters.Add(New SqlClient.SqlParameter("@DateAdded", SQLDateTime(mdDateAdded)))
                oParameters.Add(New SqlClient.SqlParameter("@Comments", msComments))

                If Me.ID = 0 Then
                    miStockAdjustmentID = DataAccess.GetValue("sp_Events_StockLevelInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@StockJournal_ID", miStockAdjustmentID))
                    DataAccess.ExecuteCommand("sp_Events_StockLevelUpdate", oParameters)
                End If

                mbChanged = False

            End If

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miStockAdjustmentID
            End Get
        End Property
        Public Property ProductID() As Integer
            Get
                Return miProductID
            End Get
            Set(ByVal value As Integer)
                If miProductID <> value Then mbChanged = True
                miProductID = value
            End Set
        End Property
        Public ReadOnly Property Product() As CProduct
            Get
                Return Lists.External.Events.GetProductByID(miProductID)
            End Get
        End Property
        Public Property VenueID() As Integer
            Get
                Return miVenueID
            End Get
            Set(ByVal value As Integer)
                If miVenueID <> value Then mbChanged = True
                miVenueID = value
            End Set
        End Property

        Public ReadOnly Property Venue() As CVenue
            Get
                Return Lists.External.Events.GetVenueByID(miVenueID)
            End Get
        End Property
        Public Property Quantity() As Integer
            Get
                Return miQuantity
            End Get
            Set(ByVal value As Integer)
                If miQuantity <> Value Then mbChanged = True
                miQuantity = Value
            End Set
        End Property
        Public Property AdjustmentType() As EnumJournalAdjustmentType
            Get
                Return miAdjustmentType
            End Get
            Set(ByVal value As EnumJournalAdjustmentType)
                If miAdjustmentType <> Value Then mbChanged = True
                miAdjustmentType = Value
            End Set
        End Property
        Public Property DateAdded() As Date
            Get
                Return mdDateAdded
            End Get
            Set(ByVal Value As Date)
                If mdDateAdded <> Value Then mbChanged = True
                mdDateAdded = Value
            End Set
        End Property
        Public Property Comments() As String
            Get
                Return msComments
            End Get
            Set(ByVal Value As String)
                If msComments <> Value Then mbChanged = True
                msComments = Value
            End Set
        End Property
        Public ReadOnly Property Changed() As Boolean
            Get
                Return mbChanged
            End Get
        End Property

    End Class

End Namespace