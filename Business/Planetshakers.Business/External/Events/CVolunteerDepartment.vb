Namespace External.Events

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CVolunteerDepartment

        Private miVolunteerDepartmentID As Integer
        Private msVolunteerDepartment As String

        Public Sub New()

            miVolunteerDepartmentID = 0
            msVolunteerDepartment = ""

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miVolunteerDepartmentID = oDataRow("VolunteerDepartment_ID")
            msVolunteerDepartment = oDataRow("VolunteerDepartment")

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miVolunteerDepartmentID
            End Get
        End Property

        Public Property ChurchDepartment() As String
            Get
                Return msVolunteerDepartment
            End Get
            Set(ByVal value As String)
                msVolunteerDepartment = value
            End Set
        End Property

        Public Overrides Function toString() As String
            Return msVolunteerDepartment
        End Function

    End Class

End Namespace