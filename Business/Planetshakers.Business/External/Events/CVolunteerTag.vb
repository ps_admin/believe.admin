﻿<Serializable()> _
<CLSCompliant(True)> _
Public Class CVolunteerTag
    Private msVolunteerTagID As String
    Private moContactID As Integer
    Private mbIsActive As Boolean
    Private moDateIssue As Date
    Private moDateDeactivation As Date

    Public Sub New()
        msVolunteerTagID = ""
        moContactID = 0
        mbIsActive = False
        moDateIssue = New Date
        moDateDeactivation = New Date
    End Sub

    Public Sub New(ByRef oDataRow As DataRow)
        Me.New()
        LoadDataRow(oDataRow)
    End Sub

    Public Sub New(ByVal iVolunteerTagID As String)
        Me.New()
        Dim sSQL As String = "SELECT * FROM Events_VolunteerTag WHERE VolunteerTag_ID = '" & iVolunteerTagID & "'"
        Dim oDataTable As DataTable = Business.DataAccess.GetDataTable(sSQL)

        If oDataTable.Rows.Count > 0 Then
            LoadDataRow(oDataTable.Rows(0))
        End If
    End Sub

    Private Sub LoadDataRow(ByRef oDataRow As DataRow)
        msVolunteerTagID = oDataRow("VolunteerTag_ID")
        moContactID = CType(oDataRow("Contact_ID"), Integer)
        mbIsActive = CType(oDataRow("VolunteerTag_Active"), Boolean)
        moDateIssue = DateTime.Parse(oDataRow("VolunteerTag_IssueDate"))

        If IsDBNull(oDataRow("VolunteerTag_DeactivationDate")) Then
            moDateDeactivation = Nothing
        Else
            moDateDeactivation = DateTime.Parse(oDataRow("VolunteerTag_DeactivationDate"))
        End If
    End Sub

    Public ReadOnly Property ID() As String
        Get
            Return msVolunteerTagID
        End Get
    End Property

    Public ReadOnly Property ContactID() As Integer
        Get
            Return moContactID
        End Get
    End Property

    Public ReadOnly Property IsActive() As Boolean
        Get
            Return mbIsActive
        End Get
    End Property

    Public ReadOnly Property DateIssue() As Date
        Get
            Return moDateIssue
        End Get
    End Property

    Public ReadOnly Property DateDeactivation() As Date
        Get
            Return moDateDeactivation
        End Get
    End Property
End Class
