Namespace External.Events

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CWaitingList

        Private miVenueWaitingID As Integer
        Private miContactID As Integer
        Private miVenueID As Integer
        Private mdDateAdded As Date
        Private msGUID As String

        Private mbChanged As Boolean

        Public Sub New()

            miVenueWaitingID = 0
            miContactID = 0
            miVenueID = 0
            mdDateAdded = Nothing
            msGUID = System.Guid.NewGuid.ToString
            mbChanged = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miVenueWaitingID = oDataRow("WaitingList_ID")
            miContactID = oDataRow("Contact_ID")
            miVenueID = oDataRow("Venue_ID")
            mdDateAdded = oDataRow("DateAdded")
            mbChanged = False

        End Sub

        Public Sub Save(ByVal oUser As CUser)

            If mbChanged Then

                Dim oParameters As New ArrayList

                oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", miContactID))
                oParameters.Add(New SqlClient.SqlParameter("@Venue_ID", miVenueID))
                oParameters.Add(New SqlClient.SqlParameter("@DateAdded", SQLDateTime(mdDateAdded)))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))

                If Me.ID = 0 Then
                    miVenueWaitingID = DataAccess.GetValue("sp_Events_WaitingListInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@WaitingList_ID", ID))
                    DataAccess.ExecuteCommand("sp_Events_WaitingListUpdate", oParameters)
                End If

                mbChanged = False

            End If

        End Sub

        Property ID() As Integer
            Get
                ID = miVenueWaitingID
            End Get
            Set(ByVal value As Integer)
                miVenueWaitingID = value
            End Set
        End Property
        Public Property ContactID() As Integer
            Get
                Return miContactID
            End Get
            Set(ByVal value As Integer)
                If miContactID <> value Then mbChanged = True
                miContactID = value
            End Set
        End Property
        Property VenueID() As Integer
            Get
                Return miVenueID
            End Get
            Set(ByVal Value As Integer)
                If miVenueID <> Value Then mbChanged = True
                miVenueID = Value
            End Set
        End Property
        ReadOnly Property Venue() As CVenue
            Get
                Return Lists.External.Events.GetVenueByID(miVenueID)
            End Get
        End Property
        Property DateAdded() As Date
            Get
                DateAdded = mdDateAdded
            End Get
            Set(ByVal Value As Date)
                If mdDateAdded <> Value Then mbChanged = True
                mdDateAdded = Value
            End Set
        End Property
        Property GUID() As String
            Get
                GUID = msGUID
            End Get
            Set(ByVal value As String)
                msGUID = value
            End Set
        End Property

        Public Property Changed() As Boolean
            Get
                Return mbChanged
            End Get
            Set(ByVal value As Boolean)
                mbChanged = value
            End Set
        End Property

        Public Sub ResetChanges()
            mbChanged = False
        End Sub

    End Class

End Namespace