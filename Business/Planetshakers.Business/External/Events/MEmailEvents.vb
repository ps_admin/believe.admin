Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports iTextSharp.text.html.simpleparser

Namespace External.Events

    Public Module MEmailEvents
        Private Class HeaderFooter
            Inherits PdfPageEventHelper

            Public Overrides Sub OnEndPage(writer As iTextSharp.text.pdf.PdfWriter, document As iTextSharp.text.Document)
                MyBase.OnEndPage(writer, document)
                Dim fontBold As iTextSharp.text.Font = FontFactory.GetFont("Arial", 10.0F, iTextSharp.text.Font.BOLD)
                Dim rect As Rectangle = writer.GetBoxSize("art")

                ColumnText.ShowTextAligned(writer.DirectContent, Element.ALIGN_CENTER, New Phrase("Planet Shakers Ministries International ABN 79 480 989 066", fontBold), (rect.GetLeft(1) + rect.GetRight(1)) / 2, rect.GetBottom(1) - 18, 0)
                ColumnText.ShowTextAligned(writer.DirectContent, Element.ALIGN_CENTER, New Phrase("PO Box 5171, South Melbourne Vic 3205   PHONE: +61 3 9896 7999   FAX: +61 3 9830 7683", fontBold), (rect.GetLeft(1) + rect.GetRight(1)) / 2, rect.GetBottom(1) - 30, 0)
            End Sub
        End Class

        Private Const A4_WIDTH_PIXELS As Integer = 595
        Private fontHeading As iTextSharp.text.Font = FontFactory.GetFont("Arial", 20.0F, iTextSharp.text.Font.BOLD)
        Private fontNormal As iTextSharp.text.Font = FontFactory.GetFont("Arial", 10.0F)
        Private fontBold As iTextSharp.text.Font = FontFactory.GetFont("Arial", 10.0F, iTextSharp.text.Font.BOLD)
        Private fontDiscount As iTextSharp.text.Font = FontFactory.GetFont("Arial", 10.0F, iTextSharp.text.Font.ITALIC)

        Public Function SingleConfirmation(ByRef oContact As CContact, ByVal oRegistration As CRegistration, ByRef sError As String) As Boolean
            Return SingleConfirmation(oContact, oRegistration, sError, IIf(oContact.Email = String.Empty, oContact.Email2, oContact.Email), oContact.Name)
        End Function

        Public Function CheckInTrialConfirmationEmail(ByRef oContact As CContact, ByVal oRegistration As CRegistration, ByVal sEmailAddress As String) As Boolean
            Dim sEmail As String = ""

            Dim sFilename As String = "barcode_" & Date.Now.Ticks & ".jpeg"
            Dim oUri As New Uri(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase) + "\" + sFilename)

            Dim imgBarcode As System.Drawing.Image = GenCode128.Code128Rendering.MakeBarcodeImage(oRegistration.ID.ToString(), 1, True)
            Dim oImageCodec As System.Drawing.Imaging.ImageCodecInfo = Nothing
            For Each oImageInfoCodec As System.Drawing.Imaging.ImageCodecInfo In System.Drawing.Imaging.ImageCodecInfo.GetImageDecoders()
                If oImageInfoCodec.FormatID = Drawing.Imaging.ImageFormat.Jpeg.Guid Then
                    oImageCodec = oImageInfoCodec
                End If
            Next

            Dim oMemoryStream As New System.IO.MemoryStream()
            imgBarcode.Save(oMemoryStream, Drawing.Imaging.ImageFormat.Jpeg)
            Dim sBarcode As String = Convert.ToBase64String(oMemoryStream.ToArray())

            Dim path As String = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase)
            path = oUri.AbsolutePath.Replace("/", "\").Replace("%20", " ")
            Dim oFileStream As System.IO.FileStream = System.IO.File.Create(path)
            oFileStream.Write(oMemoryStream.ToArray(), 0, oMemoryStream.ToArray().Length)
            oFileStream.Close()

            sEmail += "                             <table><tr><td style=""padding:0;margin:0;""><img src=""cid:barcode.jpeg"" alt=""Barcode"" /></td></tr><tr><td align=""center"" style=""padding:0;margin:0;"">" + vbCrLf
            sEmail += "                             <small><b>" + oRegistration.ID.ToString() + "</b></small></td></tr></table><br />" + vbCrLf
            sEmail += "					            Dear " & oContact.FirstName & " " & oContact.LastName & ",<br />" + vbCrLf
            sEmail += "					            <br />" + vbCrLf
            sEmail += "					            We are currently trialling a new part of our conference check-in system catered to reducing the wait time for you to collect your wrist bands in the future. This trial will only be valid on the 8th April.<br />" + vbCrLf
            sEmail += "					            <br />" + vbCrLf
            sEmail += "					            To participate in this trial, please print out this email including the barcode on the top left corner and bring it to the check-in booth. Please ensure that the print out has the barcode fully printed out. When you bring this printout to the counter, please look for Harris Soetikno and we�ll do the rest.<br />" + vbCrLf
            sEmail += "                             <br />" + vbCrLf
            sEmail += "	                            On behalf of Planetshakers, we�d like to thank you for your participation and we look forward to a great conference! See you there!<br />"
            sEmail += "			                    <br />"
            sEmail += "                             <b>Planetshakers IT Department</b>"
            sEmail += "			                    <br />" + vbCrLf
            sEmail += "			                    <br />" + vbCrLf

            Dim oEmail As New CEmailEvents
            oEmail.EmailText = sEmail
            oEmail.Subject = "Planetshakers Event Registration Confirmation"
            oEmail.EmailHeading = ""
            Select Case oRegistration.Venue.Conference.ID
                Case 11
                    oEmail.EmailStyle = EnumWebPageStyle.PlanetshakersEvolution09
                Case 12
                    oEmail.EmailStyle = EnumWebPageStyle.BeautifulWoman2009
                Case 14
                    oEmail.EmailStyle = EnumWebPageStyle.Planetshakers2010
                Case 15
                    oEmail.EmailStyle = EnumWebPageStyle.BeautifulWoman2010
                Case 16
                    oEmail.EmailStyle = EnumWebPageStyle.MightyMen2009
                Case 17
                    oEmail.EmailStyle = EnumWebPageStyle.PlanetUniCamp2009
                Case 19
                    oEmail.EmailStyle = EnumWebPageStyle.MightyMen2010
                Case 20
                    oEmail.EmailStyle = EnumWebPageStyle.Planetshakers2011
                Case 21
                    oEmail.EmailStyle = EnumWebPageStyle.BeautifulWoman2011
                Case 22
                    oEmail.EmailStyle = EnumWebPageStyle.PlanetUniCamp2010
                Case 23
                    oEmail.EmailStyle = EnumWebPageStyle.LeadershipSummit2011
                Case 24
                    oEmail.EmailStyle = EnumWebPageStyle.Planetshakers2012
                Case 26
                    oEmail.EmailStyle = EnumWebPageStyle.BeautifulWoman2012
                Case 27
                    oEmail.EmailStyle = EnumWebPageStyle.PlanetUniCamp2011
                Case 28
                    oEmail.EmailStyle = EnumWebPageStyle.MightyMen2011
                Case 30
                    oEmail.EmailStyle = EnumWebPageStyle.Planetshakers2013
                Case Else
                    oEmail.EmailStyle = EnumWebPageStyle.GenericStyle
            End Select
            oEmail.AddBreadCrumb("Registration Confirmation")
            oEmail.AttachFile(path, "barcode.jpeg")
            Dim bReturn As Boolean = oEmail.SendEmail(sEmailAddress, oContact.Name, "")

            GC.Collect()
            GC.WaitForPendingFinalizers()
            System.IO.File.Delete(path)
            Return (bReturn)

        End Function

        Private Sub GeneratePdfGroupInvoice(ByRef oGroup As CGroup, ByRef oConference As CConference, ByVal sDirectory As String, ByVal cBulkRegistration As ArrayList, ByVal cBulkRegistrationCombo As ArrayList)
            Dim cmToFloat As Decimal = 28.3464567
            Dim margin As Decimal = 0.5 * cmToFloat
            Dim pdf As Document = New Document(PageSize.A4, margin, margin, 0, margin)
            Dim oContact As CContact = oGroup.GroupLeader

            Dim oRegistration As CRegistration = Nothing
            For Each oBuffer As CRegistration In oContact.External.Events.Registrations(oConference, oGroup.GroupLeader.ID)
                oRegistration = oBuffer
                Exit For
            Next

            Dim sPdfName As String = sDirectory & "\Registration" & oRegistration.ID & ".pdf"
            Dim writer As PdfWriter = Nothing

            Dim iCountAccommodation As Integer = 0
            Dim iCountCatering As Integer = 0
            Dim iCountLeadership As Integer = 0
            Dim iTotal As Integer = 0
            Dim iPaid As Decimal = 0

            Dim oHashtable As New Hashtable

            Try
                writer = PdfWriter.GetInstance(pdf, New System.IO.FileStream(sPdfName, System.IO.FileMode.Create))
                Dim oHeaderFooter As New HeaderFooter()
                writer.SetBoxSize("art", New Rectangle(36, 54, 559, 788))
                writer.PageEvent = oHeaderFooter

                pdf.Open()

                Dim oLetterHead As Image = Nothing
                Dim oUri As New Uri(sDirectory & "\.." & oConference.LetterHead)
                If System.IO.File.Exists(sDirectory & "\.." & oConference.LetterHead) Then
                    oLetterHead = Image.GetInstance(oUri)
                Else
                    oUri = New Uri(sDirectory & "\..\images\LetterHead\Generic.jpg")
                    oLetterHead = Image.GetInstance(oUri)
                End If

                oLetterHead.ScalePercent(CType(A4_WIDTH_PIXELS / oLetterHead.Width * 100, Single))
                oLetterHead.Alignment = Image.ALIGN_CENTER
                pdf.Add(oLetterHead)

                Dim table As New PdfPTable(2)

                Dim iCellWidth As Integer = PageSize.A4.Width * 2 / 2
                'table.SetTotalWidth(New Single() {iCellWidth, iCellWidth})
                table.WidthPercentage = 100

                Dim oPdfCell = New PdfPCell(New Phrase("TAX INVOICE", fontHeading))
                oPdfCell = FormatCell(oPdfCell, Element.ALIGN_LEFT)
                oPdfCell.FixedHeight = fontHeading.Size + 5
                table.AddCell(oPdfCell)

                Dim imgBarcode As System.Drawing.Image = GenCode128.Code128Rendering.MakeBarcodeImage(oRegistration.ID.ToString(), 1, True)

                oPdfCell = New PdfPCell(Image.GetInstance(imgBarcode, System.Drawing.Imaging.ImageFormat.Jpeg))
                oPdfCell.FixedHeight = imgBarcode.Height + 1
                table.AddCell(FormatCell(oPdfCell))

                table.AddCell(FormatCell(New PdfPCell(New Phrase(""))))

                oPdfCell = New PdfPCell(New Phrase(oRegistration.ID, fontNormal))
                table.AddCell(FormatCell(oPdfCell))

                pdf.Add(New Paragraph(" ", fontNormal))
                pdf.Add(table)

                pdf.Add(New Paragraph("Date: " & oRegistration.RegistrationDate.ToString("dd MMM yyyy"), fontNormal))
                pdf.Add(New Paragraph("Group Leader Name: " & oContact.Name, fontNormal))

                table = New PdfPTable(4)
                table.SetWidths(New Integer() {6, 1, 1, 2})
                table.WidthPercentage = 85

                oPdfCell = New PdfPCell(New Phrase("Item", fontBold))
                oPdfCell.FixedHeight = fontBold.Size * 2
                table.AddCell(FormatCell(oPdfCell, Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 1, 0, 0, 1))

                oPdfCell = New PdfPCell(New Phrase("Qty", fontBold))
                oPdfCell.FixedHeight = fontBold.Size * 2
                table.AddCell(FormatCell(oPdfCell, Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 1, 0, 0, 0))

                oPdfCell = New PdfPCell(New Phrase("Price", fontBold))
                oPdfCell.FixedHeight = fontBold.Size * 2
                table.AddCell(FormatCell(oPdfCell, Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 1, 0, 0, 0))

                oPdfCell = New PdfPCell(New Phrase("Cost", fontBold))
                oPdfCell.FixedHeight = fontBold.Size * 2
                table.AddCell(FormatCell(oPdfCell, Element.ALIGN_RIGHT, Element.ALIGN_BOTTOM, 1, 1, 0, 0))

                For Each oBulkRegistration As Object In cBulkRegistration
                    Dim oRegistrationType As CRegistrationType = oBulkRegistration(0)
                    Dim oVenue As CVenue = oBulkRegistration(1)
                    Dim iQuantity As Integer = oBulkRegistration(2)

                    oPdfCell = New PdfPCell(New Phrase("Registration - " & oVenue.Name & " - " & oRegistrationType.Name, fontNormal))
                    table.AddCell(FormatCell(oPdfCell, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 0, 0, 0, 1))

                    oPdfCell = New PdfPCell(New Phrase(iQuantity, fontNormal))
                    table.AddCell(FormatCell(oPdfCell, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE))

                    oPdfCell = New PdfPCell(New Phrase(FormatCurrency(oRegistrationType.RegistrationCost, 2), fontNormal))
                    table.AddCell(FormatCell(oPdfCell, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE))

                    oPdfCell = New PdfPCell(New Phrase(FormatCurrency(iQuantity * oRegistrationType.RegistrationCost, 2), fontNormal))
                    table.AddCell(FormatCell(oPdfCell, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 0, 1, 0, 0))

                    iTotal += iQuantity * oRegistrationType.RegistrationCost
                Next

                For Each oBulkRegistrationCombo As Object In cBulkRegistrationCombo
                    Dim oComboDetail As CRegistrationTypeComboDetail = oBulkRegistrationCombo(0)
                    Dim iQuantity As Integer = oBulkRegistrationCombo(1)
                    Dim dCost As Double = oComboDetail.RegistrationType1.RegistrationCost + oComboDetail.RegistrationType2.RegistrationCost

                    oPdfCell = New PdfPCell(New Phrase("Registration - " & oComboDetail.RegistrationTypeCombo.Name & " - " & oComboDetail.Name, fontNormal))
                    table.AddCell(FormatCell(oPdfCell, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 0, 0, 0, 1))

                    oPdfCell = New PdfPCell(New Phrase(iQuantity, fontNormal))
                    table.AddCell(FormatCell(oPdfCell, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE))

                    oPdfCell = New PdfPCell(New Phrase(FormatCurrency(dCost, 2), fontNormal))
                    table.AddCell(FormatCell(oPdfCell, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE))

                    oPdfCell = New PdfPCell(New Phrase(FormatCurrency(iQuantity * dCost, 2), fontNormal))
                    table.AddCell(FormatCell(oPdfCell, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 0, 1, 0, 0))

                    iTotal += iQuantity * dCost
                Next

                For Each oBufferContact As CContact In oGroup.Contacts
                    For Each oBufferRegistration As CRegistration In oBufferContact.External.Events.Registrations(oConference, oContact.ID)
                        If oBufferRegistration.Accommodation Then iCountAccommodation += 1
                        If oBufferRegistration.Catering Then iCountCatering += 1
                        If oBufferRegistration.LeadershipBreakfast Then iCountLeadership += 1
                    Next
                Next

                For Each oBulkPurchase As CGroupBulkPurchase In oGroup.BulkPurchase()
                    If oBulkPurchase.Venue.Conference.ID = oConference.ID Then
                        iCountAccommodation += oBulkPurchase.AccommodationQuantity
                        iCountCatering += oBulkPurchase.CateringQuantity
                        iCountLeadership += oBulkPurchase.LeadershipBreakfastQuantity
                    End If
                Next

                If iCountAccommodation > 0 Then
                    oPdfCell = New PdfPCell(New Paragraph("Budget Accommodation - " & oRegistration.AccommodationVenue.VenueName, fontNormal))
                    table.AddCell(FormatCell(oPdfCell, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 0, 0, 0, 1))

                    oPdfCell = New PdfPCell(New Phrase(iCountAccommodation, fontNormal))
                    table.AddCell(FormatCell(oPdfCell, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE))

                    oPdfCell = New PdfPCell(New Paragraph(FormatCurrency(oConference.AccommodationCost, 2), fontNormal))
                    table.AddCell(FormatCell(oPdfCell, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE))

                    oPdfCell = New PdfPCell(New Paragraph(FormatCurrency(iCountAccommodation * oConference.AccommodationCost, 2), fontNormal))
                    table.AddCell(FormatCell(oPdfCell, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 0, 1, 0, 0))

                    iTotal = iCountAccommodation * oConference.AccommodationCost
                End If

                If iCountCatering > 0 Then
                    oPdfCell = New PdfPCell(New Paragraph("Budget Catering", fontNormal))
                    table.AddCell(FormatCell(oPdfCell, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 0, 0, 0, 1))

                    oPdfCell = New PdfPCell(New Phrase(iCountCatering, fontNormal))
                    table.AddCell(FormatCell(oPdfCell, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE))

                    oPdfCell = New PdfPCell(New Paragraph(FormatCurrency(oConference.CateringCost, 2), fontNormal))
                    table.AddCell(FormatCell(oPdfCell, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE))

                    oPdfCell = New PdfPCell(New Paragraph(FormatCurrency(iCountCatering * oConference.CateringCost, 2), fontNormal))
                    table.AddCell(FormatCell(oPdfCell, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 0, 1, 0, 0))

                    iTotal = iCountCatering * oConference.CateringCost
                End If

                If iCountLeadership > 0 Then
                    oPdfCell = New PdfPCell(New Paragraph("Leadership Breakfast", fontNormal))
                    table.AddCell(FormatCell(oPdfCell, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 0, 0, 0, 1))

                    oPdfCell = New PdfPCell(New Phrase(iCountLeadership, fontNormal))
                    table.AddCell(FormatCell(oPdfCell, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE))

                    oPdfCell = New PdfPCell(New Paragraph(FormatCurrency(oConference.LeadershipBreakfastCost, 2), fontNormal))
                    table.AddCell(FormatCell(oPdfCell, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE))

                    oPdfCell = New PdfPCell(New Paragraph(FormatCurrency(iCountLeadership * oConference.LeadershipBreakfastCost, 2), fontNormal))
                    table.AddCell(FormatCell(oPdfCell, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 0, 1, 0, 0))

                    iTotal = iCountLeadership * oConference.LeadershipBreakfastCost
                End If

                table.AddCell(FormatCell(New PdfPCell(New Phrase(" ", fontNormal)), , , 0, 0, 0, 1))
                table.AddCell(FormatCell(New PdfPCell(New Phrase(" ", fontNormal)), , , 0, 0, 0, 0))

                oPdfCell = New PdfPCell(New Phrase("Total", fontBold))
                oPdfCell.FixedHeight = fontBold.Size * 2
                table.AddCell(FormatCell(oPdfCell, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE))

                oPdfCell = New PdfPCell(New Phrase(FormatCurrency(iTotal, 2), fontBold))
                oPdfCell.FixedHeight = fontBold.Size * 2
                table.AddCell(FormatCell(oPdfCell, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 0, 1, 0, 0))

                For Each oGroupPayment As CGroupPayment In oGroup.Payments()
                    If oGroupPayment.ConferenceID = oConference.ID Then
                        iPaid += oGroupPayment.PaymentAmount
                    End If
                Next

                table.AddCell(FormatCell(New PdfPCell(New Phrase(" ", fontNormal)), , , 0, 0, 0, 1))
                table.AddCell(FormatCell(New PdfPCell(New Phrase(" ", fontNormal)), , , 0, 0, 0, 0))

                oPdfCell = New PdfPCell(New Phrase("Paid", fontBold))
                oPdfCell.FixedHeight = fontBold.Size * 2
                table.AddCell(FormatCell(oPdfCell, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE))

                oPdfCell = New PdfPCell(New Phrase(FormatCurrency(iPaid, 2), fontBold))
                oPdfCell.FixedHeight = fontBold.Size * 2
                table.AddCell(FormatCell(oPdfCell, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 0, 1, 0, 0))

                table.AddCell(FormatCell(New PdfPCell(New Phrase(" ", fontNormal)), , , 0, 0, 1, 1))
                table.AddCell(FormatCell(New PdfPCell(New Phrase(" ", fontNormal)), , , 0, 0, 1, 0))

                oPdfCell = New PdfPCell(New Phrase("Owing", fontBold))
                oPdfCell.FixedHeight = fontBold.Size * 2
                table.AddCell(FormatCell(oPdfCell, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 0, 0, 1, 0))

                oPdfCell = New PdfPCell(New Phrase(FormatCurrency(iTotal - iPaid, 2), fontBold))
                oPdfCell.FixedHeight = fontBold.Size * 2
                table.AddCell(FormatCell(oPdfCell, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 0, 1, 1, 0))

                table.SpacingBefore = 15

                pdf.Add(New Paragraph(" ", fontNormal))
                pdf.Add(New Paragraph("Thank you! Your registration for " & oConference.Name & " has been received.", fontNormal))
                pdf.Add(table)

                pdf.Add(New Paragraph(" ", fontNormal))
                Dim oParagraph As New Paragraph()
                oParagraph.Add(New Phrase("GST: ", fontBold))
                oParagraph.Add(New Phrase("The registration price does not include GST as this event is considered a 'religious service'. Should you require a tax invoice for your records, please print this page.", fontNormal))
                pdf.Add(oParagraph)

                pdf.Add(New Paragraph(" ", fontNormal))
                oParagraph = New Paragraph()
                oParagraph.Add(New Phrase("Refunds: ", fontBold))
                oParagraph.Add(New Phrase("No refunds will be paid on registrations however you may transfer your registration to another person. The Planetshakers office must be advised in writing of all transfers of registrations and they may incur an administration fee.", fontNormal))
                pdf.Add(oParagraph)

                pdf.Add(New Paragraph(" ", fontNormal))
                oParagraph = New Paragraph()
                oParagraph.Add(New Phrase("Changes: ", fontBold))
                oParagraph.Add(New Phrase("If you wish to make any changes to this registration, including change of venue, adding accommodation & catering, pre orders, Planetkids etc. please email us at conference@planetshakers.com.", fontNormal))
                pdf.Add(oParagraph)

                pdf.Close()
            Catch ex As Exception
            End Try
        End Sub

        Private Sub GeneratePdfInvoice(ByRef oRegistration As CRegistration, ByVal sDirectory As String)
            Dim cmToFloat As Decimal = 28.3464567
            Dim margin As Decimal = 0.5 * cmToFloat
            Dim pdf As Document = New Document(PageSize.A4, margin, margin, 0, margin)

            Dim sPdfName As String = sDirectory & "\Registration" & oRegistration.ID & ".pdf"
            Dim writer As PdfWriter = Nothing

            Dim iCountAccommodation As Integer = 0
            Dim iCountCatering As Integer = 0
            Dim iCountLeadership As Integer = 0
            Dim iTotal As Integer = 0
            Dim iPaid As Decimal = 0

            Dim oHashtable As New Hashtable

            Dim oContact As New CContact
            oContact.LoadByID(oRegistration.ContactID)

            Try
                writer = PdfWriter.GetInstance(pdf, New System.IO.FileStream(sPdfName, System.IO.FileMode.Create))
                Dim oHeaderFooter As New HeaderFooter()
                writer.SetBoxSize("art", New Rectangle(36, 54, 559, 788))
                writer.PageEvent = oHeaderFooter

                pdf.Open()

                Dim oConference As CConference = oRegistration.Venue.Conference

                Dim oLetterHead As Image = Nothing

                If System.IO.File.Exists(sDirectory & "\.." & oConference.LetterHead) Then
                    oLetterHead = Image.GetInstance(sDirectory & "\.." & oConference.LetterHead)
                Else
                    oLetterHead = Image.GetInstance(sDirectory & "\..\images\LetterHead\Generic.jpg")
                End If

                oLetterHead.ScalePercent(CType(A4_WIDTH_PIXELS / oLetterHead.Width * 100, Single))
                oLetterHead.Alignment = Image.ALIGN_CENTER
                pdf.Add(oLetterHead)

                Dim table As New PdfPTable(2)

                Dim iCellWidth As Integer = PageSize.A4.Width * 2 / 2
                'table.SetTotalWidth(New Single() {iCellWidth, iCellWidth})
                table.WidthPercentage = 100

                Dim oPdfCell = New PdfPCell(New Phrase("TAX INVOICE", fontHeading))
                oPdfCell = FormatCell(oPdfCell, Element.ALIGN_LEFT)
                oPdfCell.FixedHeight = fontHeading.Size + 5
                table.AddCell(oPdfCell)

                Dim imgBarcode As System.Drawing.Image = GenCode128.Code128Rendering.MakeBarcodeImage(oRegistration.ID.ToString(), 1, True)
                Dim oImageCodec As System.Drawing.Imaging.ImageCodecInfo = Nothing
                For Each oImageInfoCodec As System.Drawing.Imaging.ImageCodecInfo In System.Drawing.Imaging.ImageCodecInfo.GetImageDecoders()
                    If oImageInfoCodec.FormatID = Drawing.Imaging.ImageFormat.Jpeg.Guid Then
                        oImageCodec = oImageInfoCodec
                    End If
                Next

                oPdfCell = New PdfPCell(Image.GetInstance(imgBarcode, System.Drawing.Imaging.ImageFormat.Jpeg))
                oPdfCell.FixedHeight = imgBarcode.Height + 1
                table.AddCell(FormatCell(oPdfCell))

                table.AddCell(FormatCell(New PdfPCell(New Phrase(""))))

                oPdfCell = New PdfPCell(New Phrase(oRegistration.ID, fontNormal))
                table.AddCell(FormatCell(oPdfCell))

                pdf.Add(New Paragraph(" ", fontNormal))
                pdf.Add(table)

                pdf.Add(New Paragraph("Date: " & oRegistration.RegistrationDate.ToString("dd MMM yyyy"), fontNormal))
                pdf.Add(New Paragraph("Name: " & oContact.Name, fontNormal))

                table = New PdfPTable(4)
                table.SetWidths(New Integer() {6, 1, 1, 2})
                table.WidthPercentage = 85

                oPdfCell = New PdfPCell(New Phrase("Item", fontBold))
                oPdfCell.FixedHeight = fontBold.Size * 2
                table.AddCell(FormatCell(oPdfCell, Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 1, 0, 0, 1))

                oPdfCell = New PdfPCell(New Phrase("Qty", fontBold))
                oPdfCell.FixedHeight = fontBold.Size * 2
                table.AddCell(FormatCell(oPdfCell, Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 1, 0, 0, 0))

                oPdfCell = New PdfPCell(New Phrase("Price", fontBold))
                oPdfCell.FixedHeight = fontBold.Size * 2
                table.AddCell(FormatCell(oPdfCell, Element.ALIGN_LEFT, Element.ALIGN_BOTTOM, 1, 0, 0, 0))

                oPdfCell = New PdfPCell(New Phrase("Cost", fontBold))
                oPdfCell.FixedHeight = fontBold.Size * 2
                table.AddCell(FormatCell(oPdfCell, Element.ALIGN_RIGHT, Element.ALIGN_BOTTOM, 1, 1, 0, 0))

                oPdfCell = New PdfPCell(New Phrase("Registration - " & oRegistration.Venue.Name & " - " & oRegistration.RegistrationType.Name, fontNormal))
                table.AddCell(FormatCell(oPdfCell, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 0, 0, 0, 1))

                oPdfCell = New PdfPCell(New Phrase("1", fontNormal))
                table.AddCell(FormatCell(oPdfCell, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE))

                oPdfCell = New PdfPCell(New Phrase(FormatCurrency(oRegistration.RegistrationType.RegistrationCost, 2), fontNormal))
                table.AddCell(FormatCell(oPdfCell, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE))

                oPdfCell = New PdfPCell(New Phrase(FormatCurrency(oRegistration.RegistrationType.RegistrationCost, 2), fontNormal))
                table.AddCell(FormatCell(oPdfCell, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 0, 1, 0, 0))

                If oRegistration.GroupLeaderID = 0 Then iTotal += oRegistration.RegistrationType.RegistrationCost

                If oRegistration.ComboRegistrationID > 0 Then
                    Dim oSecondaryRegistration As CRegistration = Lists.External.Events.GetRegistrationByID(oRegistration.ComboRegistrationID)

                    oPdfCell = New PdfPCell(New Phrase("Registration - " & oSecondaryRegistration.Venue.Name & " - " & oSecondaryRegistration.RegistrationType.Name, fontNormal))
                    table.AddCell(FormatCell(oPdfCell, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 0, 0, 0, 1))

                    oPdfCell = New PdfPCell(New Phrase("1", fontNormal))
                    table.AddCell(FormatCell(oPdfCell, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE))

                    oPdfCell = New PdfPCell(New Phrase(FormatCurrency(oSecondaryRegistration.RegistrationType.RegistrationCost, 2), fontNormal))
                    table.AddCell(FormatCell(oPdfCell, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE))

                    oPdfCell = New PdfPCell(New Phrase(FormatCurrency(oSecondaryRegistration.RegistrationType.RegistrationCost, 2), fontNormal))
                    table.AddCell(FormatCell(oPdfCell, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 0, 1, 0, 0))

                    If oSecondaryRegistration.GroupLeaderID = 0 Then iTotal += oSecondaryRegistration.RegistrationType.RegistrationCost
                End If

                If oRegistration.Accommodation Then
                    oPdfCell = New PdfPCell(New Paragraph("Budget Accommodation - " & oRegistration.AccommodationVenue.VenueName, fontNormal))
                    table.AddCell(FormatCell(oPdfCell, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 0, 0, 0, 1))

                    oPdfCell = New PdfPCell(New Phrase("1", fontNormal))
                    table.AddCell(FormatCell(oPdfCell, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE))

                    oPdfCell = New PdfPCell(New Paragraph(FormatCurrency(oRegistration.Venue.Conference.AccommodationCost, 2), fontNormal))
                    table.AddCell(FormatCell(oPdfCell, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE))

                    oPdfCell = New PdfPCell(New Paragraph(FormatCurrency(oRegistration.Venue.Conference.AccommodationCost, 2), fontNormal))
                    table.AddCell(FormatCell(oPdfCell, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 0, 1, 0, 0))
                End If

                If oRegistration.Catering Then
                    oPdfCell = New PdfPCell(New Paragraph("Budget Catering", fontNormal))
                    table.AddCell(FormatCell(oPdfCell, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 0, 0, 0, 1))

                    oPdfCell = New PdfPCell(New Phrase("1", fontNormal))
                    table.AddCell(FormatCell(oPdfCell, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE))

                    oPdfCell = New PdfPCell(New Paragraph(FormatCurrency(oRegistration.Venue.Conference.CateringCost, 2), fontNormal))
                    table.AddCell(FormatCell(oPdfCell, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE))

                    oPdfCell = New PdfPCell(New Paragraph(FormatCurrency(oRegistration.Venue.Conference.CateringCost, 2), fontNormal))
                    table.AddCell(FormatCell(oPdfCell, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 0, 1, 0, 0))
                End If

                If oRegistration.LeadershipBreakfast Then
                    oPdfCell = New PdfPCell(New Paragraph("Leadership Breakfast", fontNormal))
                    table.AddCell(FormatCell(oPdfCell, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 0, 0, 0, 1))

                    oPdfCell = New PdfPCell(New Phrase("1", fontNormal))
                    table.AddCell(FormatCell(oPdfCell, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE))

                    oPdfCell = New PdfPCell(New Paragraph(FormatCurrency(oRegistration.Venue.Conference.LeadershipBreakfastCost, 2), fontNormal))
                    table.AddCell(FormatCell(oPdfCell, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE))

                    oPdfCell = New PdfPCell(New Paragraph(FormatCurrency(oRegistration.Venue.Conference.LeadershipBreakfastCost, 2), fontNormal))
                    table.AddCell(FormatCell(oPdfCell, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 0, 1, 0, 0))
                End If

                table.AddCell(FormatCell(New PdfPCell(New Phrase(" ", fontNormal)), , , , , 1, 1))
                table.AddCell(FormatCell(New PdfPCell(New Phrase(" ", fontNormal)), , , , , 1))

                oPdfCell = New PdfPCell(New Phrase("Total", fontBold))
                oPdfCell.FixedHeight = fontBold.Size * 2
                table.AddCell(FormatCell(oPdfCell, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 0, 0, 1, 0))

                oPdfCell = New PdfPCell(New Phrase(FormatCurrency(iTotal, 2), fontBold))
                oPdfCell.FixedHeight = fontBold.Size * 2
                table.AddCell(FormatCell(oPdfCell, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 0, 1, 1, 0))

                'If oRegistration.GroupLeaderID = 0 Then
                '    For Each oContactPayment As CContactPayment In oContact.External.Events.Payment
                '        If oContactPayment.ConferenceID = oRegistration.Venue.Conference.ID Then
                '            iPaid += oContactPayment.PaymentAmount
                '        End If
                '    Next
                'End If

                'table.AddCell(FormatCell(New PdfPCell(New Phrase(" ", fontNormal)), , , , , , 1))
                'table.AddCell(FormatCell(New PdfPCell(New Phrase(" ", fontNormal))))

                'oPdfCell = New PdfPCell(New Phrase("Paid", fontBold))
                'oPdfCell.FixedHeight = fontBold.Size * 2
                'table.AddCell(FormatCell(oPdfCell, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE))

                'oPdfCell = New PdfPCell(New Phrase(FormatCurrency(iPaid, 2), fontBold))
                'oPdfCell.FixedHeight = fontBold.Size * 2
                'table.AddCell(FormatCell(oPdfCell, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 0, 1, 0, 0))

                'table.AddCell(FormatCell(New PdfPCell(New Phrase(" ", fontNormal)), , , , , 1, 1))
                'table.AddCell(FormatCell(New PdfPCell(New Phrase(" ", fontNormal)), , , , , 1))

                'oPdfCell = New PdfPCell(New Phrase("Owing", fontBold))
                'oPdfCell.FixedHeight = fontBold.Size * 2
                'table.AddCell(FormatCell(oPdfCell, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 0, 0, 1, 0))

                'oPdfCell = New PdfPCell(New Phrase(FormatCurrency(iTotal - iPaid, 2), fontBold))
                'oPdfCell.FixedHeight = fontBold.Size * 2
                'table.AddCell(FormatCell(oPdfCell, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 0, 1, 1, 0))

                table.SpacingBefore = 15

                pdf.Add(New Paragraph(" ", fontNormal))
                pdf.Add(New Paragraph("Thank you! Your registration for " & oRegistration.Venue.Conference.Name & " has been received.", fontNormal))
                pdf.Add(table)

                pdf.Add(New Paragraph(" ", fontNormal))
                Dim oParagraph As New Paragraph()

                If oRegistration.GroupLeaderID <> 0 Then
                    oParagraph.Add(New Phrase("Note: ", fontBold))
                    oParagraph.Add(New Phrase("If this is a group registration, payments and amount owing are shown on the group tax invoice.", fontNormal))
                    pdf.Add(oParagraph)
                    pdf.Add(New Paragraph(" ", fontNormal))
                    oParagraph = New Paragraph()
                End If

                oParagraph.Add(New Phrase("GST: ", fontBold))
                oParagraph.Add(New Phrase("The registration price does not include GST as this event is considered a 'religious service'. Should you require a tax invoice for your records, please print this page.", fontNormal))
                pdf.Add(oParagraph)

                pdf.Add(New Paragraph(" ", fontNormal))
                oParagraph = New Paragraph()
                oParagraph.Add(New Phrase("Refunds: ", fontBold))
                oParagraph.Add(New Phrase("No refunds will be paid on registrations however you may transfer your registration to another person. The Planetshakers office must be advised in writing of all transfers of registrations and they may incur an administration fee.", fontNormal))
                pdf.Add(oParagraph)

                pdf.Add(New Paragraph(" ", fontNormal))
                oParagraph = New Paragraph()
                oParagraph.Add(New Phrase("Changes: ", fontBold))
                oParagraph.Add(New Phrase("If you wish to make any changes to this registration, including change of venue, adding accommodation & catering, pre orders, Planetkids etc. please email us at conference@planetshakers.com.", fontNormal))
                pdf.Add(oParagraph)

                pdf.Close()
                writer.Close()
            Catch ex As Exception
            End Try
        End Sub

        Private Function FormatCell(ByRef oPdfCell As PdfPCell, Optional ByRef HorizontalAlignment As Integer = Element.ALIGN_CENTER, Optional ByRef VerticalAlignment As Integer = Element.ALIGN_MIDDLE, Optional ByRef BorderTop As Integer = 0, Optional ByRef BorderRight As Integer = 0, Optional ByRef BorderBottom As Integer = 0, Optional ByRef BorderLeft As Integer = 0) As PdfPCell
            oPdfCell.BorderWidthTop = BorderTop
            oPdfCell.BorderWidthRight = BorderRight
            oPdfCell.BorderWidthBottom = BorderBottom
            oPdfCell.BorderWidthLeft = BorderLeft
            oPdfCell.HorizontalAlignment = HorizontalAlignment
            oPdfCell.VerticalAlignment = VerticalAlignment
            oPdfCell.FixedHeight = fontNormal.Size * 2
            Return oPdfCell
        End Function

        Public Function SingleConfirmation(ByRef oContact As CContact, ByVal oPrimaryRegistration As CRegistration, ByRef sError As String, ByVal sEmailAddress As String, ByVal sEmailName As String, Optional ByVal sDirectory As String = "") As Boolean
            Dim sEmail As String = ""
            Dim oPreOrder As CPreOrder
            Dim dCost As Decimal = 0
            Dim dTotal As Decimal = 0
            Dim oBarcodes As New Dictionary(Of String, String)
            Dim oRegistrations As CArrayList(Of CRegistration) = oContact.External.Events.Registrations(oPrimaryRegistration.Venue.Conference)
            Dim oDuplicate As CArrayList(Of CRegistration) = oRegistrations.Clone()

            For Each oBufferRegistration As CRegistration In oRegistrations
                If oBufferRegistration.GroupLeaderID > 0 And oBufferRegistration.ID <> oPrimaryRegistration.ID Then oDuplicate.Remove(oBufferRegistration)
            Next

            oRegistrations = oDuplicate

            Dim oSecondaryRegistration As CRegistration = Nothing
            Dim oComboDetail As CRegistrationTypeComboDetail = Nothing

            If oPrimaryRegistration.ComboRegistrationID <> 0 Then
                oSecondaryRegistration = oContact.External.Events.Registration.GetItemByID(oPrimaryRegistration.ComboRegistrationID)
                If oSecondaryRegistration IsNot Nothing Then oComboDetail = oPrimaryRegistration.RegistrationType.RegistrationComboDetail(oSecondaryRegistration.RegistrationType)
                If oComboDetail Is Nothing Then oSecondaryRegistration = Nothing
            End If

            sEmail += "					            <br />" + vbCrLf
            sEmail += "					            <br />" + vbCrLf
            sEmail += "					            Dear " & oContact.FirstName & " " & oContact.LastName & ",<br />" + vbCrLf
            sEmail += "					            <br />" + vbCrLf
            sEmail += "					            Thank you for registering for <strong>" & oPrimaryRegistration.Venue.Conference.Name & "</strong>." + vbCrLf
            sEmail += "					            <br />" + vbCrLf
            'sEmail += "					            <br />For a quick collection, please bring this document on the day you are collecting your registration.<br />" + vbCrLf
            sEmail += "                             <br />" + vbCrLf
            sEmail += "                             <table cellspacing=""0"" cellpadding=""3"" width=""700"" style=""border: solid 1px black;"">"
            sEmail += "					                <tr>" + vbCrLf
            sEmail += "							            <td></td>" + vbCrLf
            sEmail += "							            <td width=""470"" >" + vbCrLf
            sEmail += "								            <strong>Item</strong></td>" + vbCrLf
            sEmail += "							            <td width=""80"" Align=""Center"">" + vbCrLf
            sEmail += "								            <strong>Cost</strong></td>" + vbCrLf
            sEmail += "						            </tr>" + vbCrLf

            For Each oBufferRegistration In oRegistrations
                sEmail += "					                <tr>" + vbCrLf
                sEmail += "                                    <td>" + vbCrLf

                Try
                    Dim sFilename As String = "barcode_" & Date.Now.Ticks & ".jpeg"
                    Dim oUri As New Uri(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase) + "\" + sFilename)

                    Dim path As String = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase)
                    path = oUri.AbsolutePath.Replace("/", "\").Replace("%20", " ")
                    Dim oFileStream As System.IO.FileStream = System.IO.File.Create(path)

                    Dim imgBarcode As System.Drawing.Image = GenCode128.Code128Rendering.MakeBarcodeImage(oBufferRegistration.ID.ToString(), 1, True)
                    Dim oImageCodec As System.Drawing.Imaging.ImageCodecInfo = Nothing
                    For Each oImageInfoCodec As System.Drawing.Imaging.ImageCodecInfo In System.Drawing.Imaging.ImageCodecInfo.GetImageDecoders()
                        If oImageInfoCodec.FormatID = Drawing.Imaging.ImageFormat.Jpeg.Guid Then
                            oImageCodec = oImageInfoCodec
                        End If
                    Next

                    Dim oMemoryStream As New System.IO.MemoryStream()
                    imgBarcode.Save(oMemoryStream, Drawing.Imaging.ImageFormat.Jpeg)
                    Dim sBarcode As String = Convert.ToBase64String(oMemoryStream.ToArray())

                    oFileStream.Write(oMemoryStream.ToArray(), 0, oMemoryStream.ToArray().Length)
                    oFileStream.Close()

                    oBarcodes.Add(oBufferRegistration.ID, path)
                    sEmail += "                                    <table><tr><td align=""center""><img src=""cid:" & oBufferRegistration.ID & ".jpeg"" alt=""Barcode"" /></td></tr><tr><td align=""center""><small>" & oBufferRegistration.ID & "</small></td></tr></table>" + vbCrLf
                Catch ex As Exception
                End Try

                dCost = oBufferRegistration.RegistrationType.RegistrationCost

                sEmail += "                                    </td>" & vbCrLf
                sEmail += "							           <td width=""470"" >" & oBufferRegistration.Venue.Conference.Name & " registration - " & oBufferRegistration.Venue.Name & " (" & oBufferRegistration.RegistrationType.Name & ")</td>" + vbCrLf
                sEmail += "							           <td width=""80"" Align=""Right"" Style=""Padding-Right: 10px"" >" & Format(dCost, "$#0.00") & "</td>" + vbCrLf
                sEmail += "                                 </tr>"

                dTotal += dCost
            Next

            For Each oPreOrder In oPrimaryRegistration.PreOrder
                sEmail += "					                <tr>" + vbCrLf
                sEmail += "					                    <td></td>" + vbCrLf
                sEmail += "							            <td width=""470"">Pre-order: " & oPreOrder.Product.Title & "</td>" + vbCrLf
                sEmail += "							            <td width=""80"" Align=""Right"" Style=""Padding-Right: 10px"" >" & Format(oPreOrder.Price(oPrimaryRegistration.Venue.Conference.Country), "$#0.00") & "</td>" + vbCrLf
                sEmail += "						            </tr>" + vbCrLf
            Next

            If oPrimaryRegistration.Accommodation Then
                sEmail += "					                <tr>" + vbCrLf
                sEmail += "					                    <td></td>" + vbCrLf
                sEmail += "							            <td width=""470"">Budget Accommodation</td>" + vbCrLf
                sEmail += "							            <td width=""80"" Align=""Right"" Style=""Padding-Right: 10px"" >" & Format(oPrimaryRegistration.Venue.Conference.AccommodationCost, "$#0.00") & "</td>" + vbCrLf
                sEmail += "						            </tr>" + vbCrLf
            End If

            If oPrimaryRegistration.Catering Then
                sEmail += "					                <tr>" + vbCrLf
                sEmail += "					                    <td></td>" + vbCrLf
                sEmail += "							            <td width=""470"">Budget Catering</td>" + vbCrLf
                sEmail += "							            <td width=""80"" Align=""Right"" Style=""Padding-Right: 10px"" >" & Format(oPrimaryRegistration.Venue.Conference.CateringCost, "$#0.00") & "</td>" + vbCrLf
                sEmail += "						            </tr>" + vbCrLf
            End If

            If oPrimaryRegistration.LeadershipBreakfast Then
                sEmail += "					                <tr>" + vbCrLf
                sEmail += "					                    <td></td>" + vbCrLf
                sEmail += "							            <td width=""470"">Leadership Breakfast</td>" + vbCrLf
                sEmail += "							            <td width=""80"" Align=""Right"" Style=""Padding-Right: 10px"" >" & Format(oPrimaryRegistration.Venue.Conference.LeadershipBreakfastCost, "$#0.00") & "</td>" + vbCrLf
                sEmail += "						            </tr>" + vbCrLf
            End If

            sEmail += "					                <tr>" + vbCrLf
            sEmail += "					                    <td></td>" + vbCrLf
            sEmail += "							            <td width=""470"">&nbsp;</td>" + vbCrLf
            sEmail += "							            <td width=""80"">&nbsp;</td>" + vbCrLf
            sEmail += "						            </tr>" + vbCrLf
            sEmail += "					                <tr>" + vbCrLf
            sEmail += "							            <td colspan=""2""><strong>Total registration amount:</strong></td>" + vbCrLf
            sEmail += "							            <td width=""80"" Align=""Right""Style=""Padding-Right: 10px"" ><strong>" & Format(dTotal, "$#0.00") & "</strong></td>" + vbCrLf
            sEmail += "						            </tr>" + vbCrLf
            sEmail += "					            </table><br />" + vbCrLf
            sEmail += "					            <br />" + vbCrLf

            'Additional section for boom camp 2015
            If oPrimaryRegistration.Venue.Conference.ConferenceType = EnumConferenceType.PlanetBoom Then
                sEmail += "<p>We are so excited to have you attend this event and we believe it is going to be a life changing time for you. "
                sEmail += "If you or your parents need more information on what to bring, what's happening at camp or anything else head to "
                sEmail += "<a href='http://www.planetshakers.com/planetboom/camp15.php'>http://www.planetshakers.com/planetboom/camp15.php</a>"
                sEmail += " for these answers. </p>"

                sEmail += "<p>Specific details (such as bus times) are communicated closer to the event through Urban Life leaders to all delegates. "
                sEmail += "If you are unsure about anything- the best point of contact for you or your parents is your Urban Life Group Leader. "
                sEmail += "If you are new or you are not part of an Urban Life Group then we we would love to connect you with one of our groups for camp. "
                sEmail += "We have over 40 Urban Life Groups in suburbs across Melbourne. To get in contact with the closest Urban Life Group to you, "
                sEmail += "visit our website <a href='http://www.planetshakers.com/planetboom/termcal.php'>http://www.planetshakers.com/planetboom/termcal.php</a>"
                sEmail += "or call our church offices. </p>"
            End If

            sEmail += "					            <table cellspacing=""0"" cellpadding=""3"" border=""0"" width=""700"">" + vbCrLf
            sEmail += "						            <tr>" + vbCrLf
            sEmail += "							            <td width=""700"">" + vbCrLf
            sEmail += "								            <span class=""Heading4"">Please Note:</span>" + vbCrLf
            sEmail += "							            </td>" + vbCrLf
            sEmail += "						            </tr>" + vbCrLf
            sEmail += "						            <tr>" + vbCrLf
            sEmail += "							            <td width=""700"">" + vbCrLf
            sEmail += "								            <strong>GST</strong>: The registration price does not include GST as this event is " + vbCrLf
            sEmail += "                                         considered a 'religious service'. Should you require a tax invoice for your " + vbCrLf
            sEmail += "								            records, please print this page.<br />" + vbCrLf
            sEmail += "								            <br />" + vbCrLf
            sEmail += "								            <strong>Refunds</strong>: No refunds will be paid on registrations however you may transfer your " + vbCrLf
            sEmail += "								            registration to another person. The Planetshakers office must be advised in " + vbCrLf
            sEmail += "								            writing of all transfers of registrations and they may incur an administration " + vbCrLf
            sEmail += "								            fee.<br />" + vbCrLf
            sEmail += "								            <br />" + vbCrLf
            If oPrimaryRegistration.Venue.Conference.ConferenceType = EnumConferenceType.BeautifulWoman Then
                sEmail += "								            <strong>Changes:</strong> If you wish to make any changes to this registration, please email us at beautifulwoman@planetshakers.com or call us on 1300 88 33 21.<br />" + vbCrLf
            ElseIf oPrimaryRegistration.Venue.Conference.ConferenceType = EnumConferenceType.PlanetUni Then
                sEmail += "								            <strong>Changes:</strong> If you wish to make any changes to this registration, please email us at planetuni@planetshakers.com.<br />" + vbCrLf

            ElseIf oPrimaryRegistration.Venue.Conference.ConferenceType = EnumConferenceType.PlanetBoom Then
                sEmail += "								            <strong>Changes:</strong> If you wish to make any changes to this registration, please email us at boom@planetshakers.com or call us on 1300 88 33 21.<br />" + vbCrLf
            Else
                sEmail += "								            <strong>Changes:</strong> If you wish to make any changes to this registration, including change of venue, adding accommodation & catering, pre orders, Planetkids etc. please email us at conference@planetshakers.com or call us on 1300 88 33 21.<br />" + vbCrLf
            End If
            sEmail += "								            <br />" + vbCrLf
            sEmail += "							            </td>" + vbCrLf
            sEmail += "						            </tr>" + vbCrLf
            sEmail += "					            </table>" + vbCrLf
            sEmail += "			                    <br />" + vbCrLf
            sEmail += "			                    <br />" + vbCrLf

            Dim oEmail As New CEmailEvents
            oEmail.EmailText = sEmail
            oEmail.Subject = oPrimaryRegistration.Venue.Conference.Name
            oEmail.EmailHeading = "Registration Confirmation"

            Select Case oPrimaryRegistration.Venue.Conference.ID
                Case 11
                    oEmail.EmailStyle = EnumWebPageStyle.PlanetshakersEvolution09
                Case 12
                    oEmail.EmailStyle = EnumWebPageStyle.BeautifulWoman2009
                Case 14
                    oEmail.EmailStyle = EnumWebPageStyle.Planetshakers2010
                Case 15
                    oEmail.EmailStyle = EnumWebPageStyle.BeautifulWoman2010
                Case 16
                    oEmail.EmailStyle = EnumWebPageStyle.MightyMen2009
                Case 17
                    oEmail.EmailStyle = EnumWebPageStyle.PlanetUniCamp2009
                Case 19
                    oEmail.EmailStyle = EnumWebPageStyle.MightyMen2010
                Case 20
                    oEmail.EmailStyle = EnumWebPageStyle.Planetshakers2011
                Case 21
                    oEmail.EmailStyle = EnumWebPageStyle.BeautifulWoman2011
                Case 22
                    oEmail.EmailStyle = EnumWebPageStyle.PlanetUniCamp2010
                Case 23
                    oEmail.EmailStyle = EnumWebPageStyle.LeadershipSummit2011
                Case 24
                    oEmail.EmailStyle = EnumWebPageStyle.Planetshakers2012
                Case 26
                    oEmail.EmailStyle = EnumWebPageStyle.BeautifulWoman2012
                Case 27
                    oEmail.EmailStyle = EnumWebPageStyle.PlanetUniCamp2011
                Case 28
                    oEmail.EmailStyle = EnumWebPageStyle.MightyMen2011
                Case 30
                    oEmail.EmailStyle = EnumWebPageStyle.Planetshakers2013
                Case Else
                    oEmail.EmailStyle = EnumWebPageStyle.GenericStyle
            End Select
            oEmail.AddBreadCrumb("Registration Confirmation")

            For Each sKey As String In oBarcodes.Keys
                oEmail.AttachFile(oBarcodes(sKey), sKey & ".jpeg")
            Next

            GC.Collect()
            GC.WaitForPendingFinalizers()

            Dim bReturn As Boolean = False

            Try
                bReturn = oEmail.SendEmail(sEmailAddress, sEmailName, sError)
                For Each sKey As String In oBarcodes.Keys
                    System.IO.File.Delete(oBarcodes(sKey))
                Next
            Catch ex As Exception
                sError = ex.Message
            End Try

            Return (bReturn)

        End Function

        Private Function IsFileLocked(ByRef oFileInfo As System.IO.FileInfo) As Boolean
            Try
                Dim oString = oFileInfo.Attributes.ToString()
                If oString.Contains(System.IO.FileAttributes.Temporary.ToString()) Or
                   oString.Contains(System.IO.FileAttributes.ReadOnly.ToString()) Or
                   oString.Contains(System.IO.FileAttributes.System.ToString()) Then
                    Return True
                End If

                Using iInputStream As System.IO.FileStream = oFileInfo.Open(System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.None)
                    Return False
                End Using

            Catch ex As Exception
                Return True
            End Try
        End Function

        Public Function FamilyConfirmation(ByRef oContact As CContact, ByVal oFamilyList As CArrayList(Of CContact), ByVal oConference As CConference, ByRef sError As String) As Boolean

            Dim sEmail As String = String.Empty
            Dim dTotalCost As Decimal

            sEmail += "					            <br />" + vbCrLf
            sEmail += "					            <br />" + vbCrLf
            sEmail += "					            Dear " & oContact.FirstName & " " & oContact.LastName & ",<br />" + vbCrLf
            sEmail += "					            <br />" + vbCrLf
            sEmail += "					            Thank you! Your family registration has been received for <strong>" & oConference.Name & "</strong>" + vbCrLf
            sEmail += "					            <br />" + vbCrLf
            sEmail += "					            Please print this page as your tax invoice.<br />" + vbCrLf
            sEmail += "                             <br />" + vbCrLf
            sEmail += "	                            <table cellspacing=""0"" cellpadding=""3"" width=""700"">" + vbCrLf
            sEmail += "						            <tr>" + vbCrLf
            sEmail += "							            <td ColSpan=""2"" >" + vbCrLf
            sEmail += "							                <span class=""Heading4"">TAX INVOICE<br /><br />" + vbCrLf
            sEmail += "								            Registration Number: " & oContact.ID & "</span></td>" + vbCrLf
            sEmail += "						            </tr>" + vbCrLf
            sEmail += "			                    </table>"
            sEmail += "			                    <br />"
            sEmail += "                             <table cellspacing=""0"" cellpadding=""3"" width=""700"" style=""border: solid 1px black;"">"
            sEmail += "					                <tr>" + vbCrLf
            sEmail += "							            <td width=""570"" >" + vbCrLf
            sEmail += "								            <strong>Item</strong></td>" + vbCrLf
            sEmail += "							            <td width=""80"" Align=""Center"">" + vbCrLf
            sEmail += "								            <strong>Cost</strong></td>" + vbCrLf
            sEmail += "						            </tr>" + vbCrLf

            For Each oFamilyMember As CContact In oFamilyList
                Dim oRegistration As CRegistration = Nothing
                For Each oRegistration In oFamilyMember.External.Events.Registration
                    If oRegistration.Venue.Conference.ID = oConference.ID Then Exit For
                Next
                If oRegistration IsNot Nothing Then

                    dTotalCost += oRegistration.TotalCost

                    sEmail += "					                <tr>" + vbCrLf
                    sEmail += "							           <td width=""570"" >Registration for: " & oFamilyMember.Name & " (" & oRegistration.RegistrationType.Name & " - " & oRegistration.Venue.Name & ")</td>" + vbCrLf
                    sEmail += "							           <td width=""80"" Align=""Center"" >" & Format(oRegistration.RegistrationPrice, "$#0.00") & "</td>" + vbCrLf
                    sEmail += "						            </tr>" + vbCrLf

                    For Each oPreOrder As CPreOrder In oRegistration.PreOrder
                        sEmail += "					                <tr>" + vbCrLf
                        sEmail += "							            <td width=""570"">Pre-order: " & oPreOrder.Product.Title & "</td>" + vbCrLf
                        sEmail += "							            <td width=""80"" Align=""Center"">" & Format(oPreOrder.Price(oRegistration.Venue.Conference.Country), "$#0.00") & "</td>" + vbCrLf
                        sEmail += "						            </tr>" + vbCrLf
                    Next

                    If oRegistration.Accommodation Then
                        sEmail += "					                <tr>" + vbCrLf
                        sEmail += "							            <td width=""570"">Budget Accommodation</td>" + vbCrLf
                        sEmail += "							            <td width=""80"" Align=""Center"">" & Format(oRegistration.Venue.Conference.AccommodationCost, "$#0.00") & "</td>" + vbCrLf
                        sEmail += "						            </tr>" + vbCrLf
                    End If

                    If oRegistration.Catering Then
                        sEmail += "					                <tr>" + vbCrLf
                        sEmail += "							            <td width=""570"">Budget Catering</td>" + vbCrLf
                        sEmail += "							            <td width=""80"" Align=""Center"">" & Format(oRegistration.Venue.Conference.CateringCost, "$#0.00") & "</td>" + vbCrLf
                        sEmail += "						            </tr>" + vbCrLf
                    End If

                    If oRegistration.LeadershipBreakfast Then
                        sEmail += "					                <tr>" + vbCrLf
                        sEmail += "							            <td width=""570"">Leadership Breakfast</td>" + vbCrLf
                        sEmail += "							            <td width=""80"" Align=""Center"">" & Format(oRegistration.Venue.Conference.LeadershipBreakfastCost, "$#0.00") & "</td>" + vbCrLf
                        sEmail += "						            </tr>" + vbCrLf
                    End If

                End If
            Next

            sEmail += "					                <tr>" + vbCrLf
            sEmail += "							            <td width=""570"">&nbsp;</td>" + vbCrLf
            sEmail += "							            <td width=""80"">&nbsp;</td>" + vbCrLf
            sEmail += "						            </tr>" + vbCrLf
            sEmail += "					                <tr>" + vbCrLf
            sEmail += "							            <td width=""570""><strong>Total registration amount:</strong></td>" + vbCrLf
            sEmail += "							            <td width=""80"" Align=""Center""><strong>" & Format(dTotalCost, "$#0.00") & "</strong></td>" + vbCrLf
            sEmail += "						            </tr>" + vbCrLf
            sEmail += "					            </table><br />" + vbCrLf
            sEmail += "					            <br />" + vbCrLf
            sEmail += "					            <table cellspacing=""0"" cellpadding=""3"" border=""0"" width=""700"">" + vbCrLf
            sEmail += "						            <tr>" + vbCrLf
            sEmail += "							            <td width=""700"">" + vbCrLf
            sEmail += "								            <span class=""Heading4"">Please Note:</span>" + vbCrLf
            sEmail += "							            </td>" + vbCrLf
            sEmail += "						            </tr>" + vbCrLf
            sEmail += "						            <tr>" + vbCrLf
            sEmail += "							            <td width=""700"">" + vbCrLf
            sEmail += "								            <strong>GST</strong>: The registration price does not include GST as this event is " + vbCrLf
            sEmail += "                                         considered a 'religious service'. Should you require a tax invoice for your " + vbCrLf
            sEmail += "								            records, please print this page.<br />" + vbCrLf
            sEmail += "								            <br />" + vbCrLf
            sEmail += "								            <strong>Refunds</strong>: No refunds will be paid on registrations however you may transfer your " + vbCrLf
            sEmail += "								            registration to another person. The Planetshakers office must be advised in " + vbCrLf
            sEmail += "								            writing of all transfers of registrations and they may incur an administration " + vbCrLf
            sEmail += "								            fee.<br />" + vbCrLf
            sEmail += "								            <br />" + vbCrLf
            If oConference.ConferenceType = EnumConferenceType.BeautifulWoman Then
                sEmail += "								            <strong>Changes:</strong> If you wish to make any changes to this registration, please email us at beautifulwoman@planetshakers.com or call us on 1300 88 33 21.<br />" + vbCrLf
            ElseIf oConference.ConferenceType = EnumConferenceType.PlanetUni Then
                sEmail += "								            <strong>Changes:</strong> If you wish to make any changes to this registration, please email us at planetuni@planetshakers.com.<br />" + vbCrLf
            Else
                sEmail += "								            <strong>Changes:</strong> If you wish to make any changes to this registration, including change of venue, adding accommodation & catering, pre orders, Planetkids etc. please email us conference@planetshakers.com or call us on 1300 88 33 21.<br />" + vbCrLf
            End If
            sEmail += "								            <br />" + vbCrLf
            sEmail += "							            </td>" + vbCrLf
            sEmail += "						            </tr>" + vbCrLf
            sEmail += "					            </table>" + vbCrLf
            sEmail += "			                    <br />" + vbCrLf
            sEmail += "			                    <br />" + vbCrLf

            Dim oEmail As New CEmailEvents
            oEmail.EmailText = sEmail
            oEmail.Subject = "Planetshakers Family Registration Confirmation"
            oEmail.EmailHeading = "Registration Confirmation"
            Select Case oConference.ID
                Case 11
                    oEmail.EmailStyle = EnumWebPageStyle.PlanetshakersEvolution09
                Case 12
                    oEmail.EmailStyle = EnumWebPageStyle.BeautifulWoman2009
                Case 14
                    oEmail.EmailStyle = EnumWebPageStyle.Planetshakers2010
                Case 15
                    oEmail.EmailStyle = EnumWebPageStyle.BeautifulWoman2010
                Case 16
                    oEmail.EmailStyle = EnumWebPageStyle.MightyMen2009
                Case 17
                    oEmail.EmailStyle = EnumWebPageStyle.PlanetUniCamp2009
                Case 19
                    oEmail.EmailStyle = EnumWebPageStyle.MightyMen2010
                Case 20
                    oEmail.EmailStyle = EnumWebPageStyle.Planetshakers2011
                Case 21
                    oEmail.EmailStyle = EnumWebPageStyle.BeautifulWoman2011
                Case 22
                    oEmail.EmailStyle = EnumWebPageStyle.PlanetUniCamp2010
                Case 23
                    oEmail.EmailStyle = EnumWebPageStyle.LeadershipSummit2011
                Case 24
                    oEmail.EmailStyle = EnumWebPageStyle.Planetshakers2012
                Case 26
                    oEmail.EmailStyle = EnumWebPageStyle.BeautifulWoman2012
                Case 27
                    oEmail.EmailStyle = EnumWebPageStyle.PlanetUniCamp2011
                Case 28
                    oEmail.EmailStyle = EnumWebPageStyle.MightyMen2011
                Case 30
                    oEmail.EmailStyle = EnumWebPageStyle.Planetshakers2013
                Case Else
                    oEmail.EmailStyle = EnumWebPageStyle.GenericStyle
            End Select
            oEmail.AddBreadCrumb("Registration Confirmation")

            Return oEmail.SendEmail(IIf(oContact.Email = String.Empty, oContact.Email2, oContact.Email), oContact.Name, sError)

        End Function

        Public Function GroupApproveCredit(ByRef oGroup As CGroup, ByRef sError As String) As Boolean

            Dim sEmail As String = ""

            sEmail += "		    <br />" + vbCrLf
            sEmail += "			<br />" + vbCrLf
            sEmail += "			Dear " & oGroup.GroupLeader.FirstName & ",<br />" + vbCrLf
            sEmail += "			<br />" + vbCrLf
            sEmail += "			Thank you! Your group registration has been approved for credit.<br />" + vbCrLf
            sEmail += "			<br />"
            sEmail += "         You may now log on and modify your group registration details at <a href=""http://accounts.planetshakers.com"">accounts.planetshakers.com</a>, without having to pay the total amount up front. Please note, a 10% deposit is required on all registration purchases and is non refundable.<br /><br />" + vbCrLf
            sEmail += "			<br />" + vbCrLf
            sEmail += "			<strong>Your group credit approval has been approved with the following conditions:</strong><br />" + vbCrLf
            sEmail += "			<br />" + vbCrLf
            sEmail += "			<li>All registrations must be paid in full by Tuesday 17th February 2009. Installments or lump sum payments accepted.<br />" + vbCrLf
            sEmail += "			<li>Payments are to be made via credit card online. Please contact us on 1300 88 33 21 if you require assistance with payment methods.<br />" + vbCrLf
            sEmail += "			<li>If payment is not received in full by due date, any registrations purchased will be removed from your account, and future credit approval may be denied.<br />" + vbCrLf
            sEmail += "			<li>All registrations must be processed by the group leader on-line.<br />" + vbCrLf
            sEmail += "			<li>Registrations must be collected on day of conference check-in by the group leader (individuals cannot collect registration pack).<br />" + vbCrLf
            sEmail += "			<li>Please be aware that budget accommodation and catering cut off dates apply to group registrations.<br />" + vbCrLf
            sEmail += "			<li>Group credit approval is subject to approval by Planetshakers Conference staff and can be withdrawn at any time.<br />" + vbCrLf
            sEmail += "			<br />" + vbCrLf
            sEmail += "			<br />" + vbCrLf
            sEmail += "			Blessings," + vbCrLf
            sEmail += "			<br />" + vbCrLf
            sEmail += "			<br />" + vbCrLf
            sEmail += "			Registrations Department<br />" + vbCrLf
            sEmail += "			Planetshakers Conference<br />" + vbCrLf
            sEmail += "			PO Box 5171<br />" + vbCrLf
            sEmail += "			South Melbourne Vic 3205<br />" + vbCrLf
            sEmail += "			<br />" + vbCrLf
            sEmail += "			Phone: +61 3 9896 7999<br />" + vbCrLf
            sEmail += "		    Email: <a href=""mailto:conference@planetshakers.com"">conference@planetshakers.com</a><br />" + vbCrLf
            sEmail += "			<br />" + vbCrLf
            sEmail += "			<br />" + vbCrLf

            Dim oEmail As New CEmail
            oEmail.EmailText = sEmail
            oEmail.Subject = "Planetshakers Group Registration Credit Approval"
            oEmail.ToAddress = oGroup.GroupLeader.Email
            oEmail.ToName = oGroup.GroupLeader.FirstName & " " & oGroup.GroupLeader.LastName
            Return oEmail.SendEmail(sError)

        End Function

        Public Function GroupDenyCredit(ByRef oGroup As CGroup, ByRef sError As String) As Boolean

            Dim sEmail As String = ""

            sEmail += "		    <br />" + vbCrLf
            sEmail += "			<br />" + vbCrLf
            sEmail += "			Dear " & oGroup.GroupLeader.FirstName & ",<br />" + vbCrLf
            sEmail += "			<br />" + vbCrLf
            sEmail += "			Unfortunately, Planetshakers has decided not to approve your group for credit for our online registration system.<br />" + vbCrLf
            sEmail += "			<br />" + vbCrLf
            sEmail += "			You may still log on to the registration website and complete a group registration, however, any registrations you add will have to be paid for on the spot.<br />" + vbCrLf
            sEmail += "			<br />" + vbCrLf
            sEmail += "			Please feel free to contact me if you have any questions.<br />" + vbCrLf
            sEmail += "			<br />" + vbCrLf
            sEmail += "			<br />" + vbCrLf
            sEmail += "			Blessings," + vbCrLf
            sEmail += "			<br />" + vbCrLf
            sEmail += "			<br />" + vbCrLf
            sEmail += "			Registrations Department<br />" + vbCrLf
            sEmail += "			Planetshakers Conference<br />" + vbCrLf
            sEmail += "			PO Box 5171<br />" + vbCrLf
            sEmail += "			South Melbourne Vic 3205<br />" + vbCrLf
            sEmail += "			<br />" + vbCrLf
            sEmail += "			Phone: +61 3 9896 7999<br />" + vbCrLf
            sEmail += "			Email: <a href=""mailto:conference@planetshakers.com"">conference@planetshakers.com</a><br />" + vbCrLf
            sEmail += "			<br />" + vbCrLf
            sEmail += "			<br />" + vbCrLf

            Dim oEmail As New CEmail
            oEmail.EmailText = sEmail
            oEmail.Subject = "Planetshakers Group Registration Credit Request"
            oEmail.ToAddress = oGroup.GroupLeader.Email
            oEmail.ToName = oGroup.GroupLeader.FirstName & " " & oGroup.GroupLeader.LastName
            Return oEmail.SendEmail(sError)

        End Function

        Public Function GroupTaxInvoice(ByRef oGroup As CGroup, ByRef oConference As CConference, ByRef sError As String, Optional ByVal sDirectory As String = "") As Boolean
            Dim cBulkRegistrations As New ArrayList
            Dim cBulkRegistrationCombos As New ArrayList

            'Put together a list of purchased registrations, so we can work out which one are the combo ones, then account for and remove them.
            Dim cRegistrationPurchasedList As New ArrayList
            For Each oBulkRegistration As CGroupBulkRegistration In oGroup.BulkRegistrations
                cRegistrationPurchasedList.Add(New Object() {oBulkRegistration, oBulkRegistration.Quantity})
            Next

            'Sort out the Combo Registrations before all others
            For Each oComboDetail As CRegistrationTypeComboDetail In Lists.External.Events.RegistrationTypeComboDetail.Values
                If oComboDetail.RegistrationTypeCombo.ConferenceID = oConference.ID Then
                    Dim iQuantity As Integer = CheckPurchasedForCombo(cRegistrationPurchasedList, oComboDetail)
                    If iQuantity > 0 Then cBulkRegistrationCombos.Add(New Object() {oComboDetail, iQuantity})
                End If
            Next

            'Group together the other registrations
            For Each oVenue As CVenue In Lists.External.Events.ConferenceVenues(oConference).Values
                For Each oRegistrationType As CRegistrationType In Lists.External.Events.RegistrationTypesByVenue(oVenue)
                    If oRegistrationType.ConferenceID = oConference.ID Then

                        Dim iQuantity As Integer = 0

                        For Each oObj As Object In cRegistrationPurchasedList
                            If oObj(0).Venue.ID = oVenue.ID And oObj(0).RegistrationTypeID = oRegistrationType.ID Then
                                iQuantity += oObj(1)
                            End If
                        Next

                        If iQuantity > 0 Then
                            cBulkRegistrations.Add(New Object() {oRegistrationType, oVenue, iQuantity})
                        End If
                    End If
                Next
            Next

            Return GroupTaxInvoice(oGroup, oConference, cBulkRegistrations, cBulkRegistrationCombos, oGroup.BulkPurchase, oGroup.Payments, sError, sDirectory)

        End Function

        Public Function GroupTaxInvoice(ByRef oGroup As CGroup, ByRef oConference As CConference, _
                                        ByVal cBulkRegistration As CArrayList(Of CGroupBulkRegistration), _
                                        ByVal cBulkRegistrationCombo As ArrayList, _
                                        ByRef cBulkPurchase As CArrayList(Of CGroupBulkPurchase), _
                                        ByVal cPayments As CArrayList(Of CGroupPayment), ByRef sError As String, Optional ByVal sDirectory As String = "") As Boolean

            Dim oBulkRegistrationList As New ArrayList
            For Each oBulkRegistration As CGroupBulkRegistration In cBulkRegistration
                oBulkRegistrationList.Add(New Object() {oBulkRegistration.RegistrationType, oBulkRegistration.Venue, oBulkRegistration.Quantity})
            Next

            Return GroupTaxInvoice(oGroup, oConference, oBulkRegistrationList, cBulkRegistrationCombo, cBulkPurchase, cPayments, sError, sDirectory)

        End Function

        Private Function GroupTaxInvoice(ByRef oGroup As CGroup, ByRef oConference As CConference, _
                                        ByVal cBulkRegistration As ArrayList, _
                                        ByVal cBulkRegistrationCombo As ArrayList, _
                                        ByRef cBulkPurchase As CArrayList(Of CGroupBulkPurchase), _
                                        ByVal cPayments As CArrayList(Of CGroupPayment), ByRef sError As String, Optional ByVal sDirectory As String = "") As Boolean

            Dim sEmail As New Text.StringBuilder
            Dim oRegistrations As CArrayList(Of CRegistration) = oGroup.GroupLeader.External.Events.Registrations(oConference, oGroup.GroupLeader.ID)
            Dim oPrimaryRegistration As CRegistration = Nothing
            Dim sFilename As String = "barcode_" & Date.Now.Ticks & ".jpeg"
            Dim oUri As New Uri(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase) + "\" + sFilename)
            Dim path As String = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase)

            If oRegistrations.Count > 0 Then
                oPrimaryRegistration = oRegistrations(0)

                Dim imgBarcode As System.Drawing.Image = GenCode128.Code128Rendering.MakeBarcodeImage(oPrimaryRegistration.ID.ToString(), 1, True)
                Dim oImageCodec As System.Drawing.Imaging.ImageCodecInfo = Nothing
                For Each oImageInfoCodec As System.Drawing.Imaging.ImageCodecInfo In System.Drawing.Imaging.ImageCodecInfo.GetImageDecoders()
                    If oImageInfoCodec.FormatID = Drawing.Imaging.ImageFormat.Jpeg.Guid Then
                        oImageCodec = oImageInfoCodec
                    End If
                Next

                Dim oMemoryStream As New System.IO.MemoryStream()
                imgBarcode.Save(oMemoryStream, Drawing.Imaging.ImageFormat.Jpeg)
                Dim sBarcode As String = Convert.ToBase64String(oMemoryStream.ToArray())

                path = oUri.AbsolutePath.Replace("/", "\").Replace("%20", " ")
                Dim oFileStream As System.IO.FileStream = System.IO.File.Create(path)
                oFileStream.Write(oMemoryStream.ToArray(), 0, oMemoryStream.ToArray().Length)
                oFileStream.Close()
            End If

            sEmail.AppendLine("<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Dear " & oGroup.GroupLeader.Name & ",<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Thank you for registering for <b>" & oConference.Name & "</b>.<br />")
            sEmail.AppendLine("<br />")
            ' sEmail.AppendLine("For a quick collection, please bring the attached document on the day you are collecting your registration.")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("<strong>TAX INVOICE</strong><br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("<br />")

            If oPrimaryRegistration IsNot Nothing Then
                sEmail.AppendLine("<table cellspacing=""0"" cellpadding=""3"" width=""700"">")
                sEmail.AppendLine("  <tr>")
                sEmail.AppendLine("    <td ColSpan=""2"" >")
                sEmail.AppendLine("      Registration Number: " & oPrimaryRegistration.ID & "</span>")
                sEmail.AppendLine("      <p><img src=""cid:barcode.jpeg"" alt=""Barcode"" /></p></td>")
                sEmail.AppendLine("  </tr>")
                sEmail.AppendLine("</table>")
            End If

            sEmail.AppendLine("<br />")

            sEmail.AppendLine("Planetshakers Ministries International Inc.<br />")
            sEmail.AppendLine("ABN: 79 480 989 066<br />")
            sEmail.AppendLine("<br /><br />")
            sEmail.AppendLine("<table cellpadding=""5"">")
            sEmail.AppendLine("<tr><td width=""400""><strong>Item Purchased</strong></td><td width=""80""><strong>Quantity</strong></td><td width=""80""><strong>Price</strong></td><td width=""80""><strong>Total</strong></td></tr>")

            For Each oBulkRegistration As Object In cBulkRegistration
                Dim oRegistrationType As CRegistrationType = oBulkRegistration(0)
                Dim oVenue As CVenue = oBulkRegistration(1)
                Dim iQuantity As Integer = oBulkRegistration(2)
                If oRegistrationType.Conference.ID = oConference.ID Then
                    sEmail.Append("<tr><td style=""padding 5px;"">" & "Registration - " & oVenue.Name & " - " & oRegistrationType.Name & "</td>")
                    sEmail.Append("<td style=""padding 5px;"">" & iQuantity & "</td><td>" & Format(oRegistrationType.RegistrationCost, "$#0.00") & "</td>")
                    sEmail.AppendLine("<td style=""padding 5px;"">" & Format(oRegistrationType.RegistrationCost * iQuantity, "$#0.00") & "</td></tr>")
                End If
            Next

            If cBulkRegistrationCombo IsNot Nothing Then
                For Each oBulkRegistrationCombo As Object In cBulkRegistrationCombo
                    Dim oComboDetail As CRegistrationTypeComboDetail = oBulkRegistrationCombo(0)
                    Dim iQuantity As Integer = oBulkRegistrationCombo(1)
                    sEmail.Append("<tr><td style=""padding 5px;"">" & "Registration - " & oComboDetail.RegistrationTypeCombo.Name & " - " & oComboDetail.Name & "</td>")
                    sEmail.Append("<td style=""padding 5px;"">" & iQuantity & "</td><td>" & Format(oComboDetail.RegistrationType1.RegistrationCost + oComboDetail.RegistrationType2.RegistrationCost, "$#0.00") & "</td>")
                    sEmail.AppendLine("<td style=""padding 5px;"">" & Format((oComboDetail.RegistrationType1.RegistrationCost + oComboDetail.RegistrationType2.RegistrationCost) * iQuantity, "$#0.00") & "</td></tr>")
                Next
            End If

            For Each oBulkPurchase As CGroupBulkPurchase In cBulkPurchase
                If oBulkPurchase.Venue.Conference.ID = oConference.ID Then
                    If oBulkPurchase.AccommodationQuantity > 0 Then
                        sEmail.Append("<tr><td>Budget Accommodation</td>")
                        sEmail.Append("<td>" & oBulkPurchase.AccommodationQuantity & "</td><td>" & Format(oBulkPurchase.Venue.Conference.AccommodationCost, "$#0.00") & "</td>")
                        sEmail.AppendLine("<td>" & Format(oBulkPurchase.AccommodationCost, "$#0.00") & "</td></tr>")
                    End If
                    If oBulkPurchase.CateringQuantity > 0 Then
                        sEmail.Append("<tr><td>Catering</td>")
                        sEmail.Append("<td>" & oBulkPurchase.CateringQuantity & "</td><td>" & Format(oBulkPurchase.Venue.Conference.CateringCost, "$#0.00") & "</td>")
                        sEmail.AppendLine("<td>" & Format(oBulkPurchase.CateringCost, "$#0.00") & "</td></tr>")
                    End If
                    If oBulkPurchase.LeadershipBreakfastQuantity > 0 Then
                        sEmail.Append("<tr><td>Leadership Breakfast</td>")
                        sEmail.Append("<td>" & oBulkPurchase.LeadershipBreakfastQuantity & "</td><td>" & Format(oBulkPurchase.Venue.Conference.LeadershipBreakfastCost, "$#0.00") & "</td>")
                        sEmail.AppendLine("<td>" & Format(oBulkPurchase.LeadershipBreakfastCost, "$#0.00") & "</td></tr>")
                    End If
                End If
            Next

            sEmail.AppendLine("</table>")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("<strong>Payments</strong><br />")
            sEmail.AppendLine("<table cellpadding=""5"">")
            For Each oPayment As CGroupPayment In cPayments
                If oPayment.Conference.ID = oConference.ID Then
                    sEmail.Append("<tr><td width=""100"">" & Format(oPayment.PaymentDate, "dd MMM yyyy") & "</td>")
                    sEmail.Append("<td width=""150"">" & IIf(oPayment.PaymentAmount < 0, "Refund", "Payment") & " by " & oPayment.PaymentType.Name & ":</td>")
                    sEmail.AppendLine("<td width=""100"">" & Format(oPayment.PaymentAmount, "$#0.00") & " AUD</td></tr>")
                End If
            Next
            sEmail.AppendLine("</table>")


            sEmail.AppendLine("<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("<strong>Please Note:</strong><br />")
            sEmail.AppendLine("If you have not allocated your registrations yet, you will need to log onto https://accounts.planetshakers.com <br />and fill in your group member's registration details there.<br />")
            sEmail.AppendLine("Please use your email address (" & oGroup.GroupLeader.Email & ") to log in.<br />")
            sEmail.AppendLine("If you don't have a password, or have forgotten it, please go to: https://accounts.planetshakers.com/ForgotPassword.aspx to reset your password.<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("<br />")

            sEmail.AppendLine("Blessings,")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Registrations Department<br />")
            sEmail.AppendLine("Planetshakers Conference<br />")
            sEmail.AppendLine("PO Box 5171<br />")
            sEmail.AppendLine("South Melbourne Vic 3205<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Phone: +61 3 9896 7999<br />")
            sEmail.AppendLine("Email: <a href=""mailto:conference@planetshakers.com"">conference@planetshakers.com</a><br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("<br />")

            Dim oRegistration As CRegistration = Nothing
            For Each oBuffer As CRegistration In oGroup.GroupLeader.External.Events.Registrations(oConference, oGroup.GroupLeader.ID)
                If oBuffer.Venue.Conference.ID = oConference.ID Then
                    oRegistration = oBuffer
                    Exit For
                End If
            Next
            Dim oEmail As New CEmailEvents

            oEmail.EmailStyle = oConference.ID
            oEmail.EmailText = sEmail.ToString
            oEmail.Subject = oConference.Name

            Dim bReturn As Boolean = False

            oEmail.AttachFile(path, "barcode.jpeg")
            Try
                oUri = New Uri(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase))
                Dim directory As String = oUri.AbsolutePath.Replace("/", "\").Replace("%20", " ").Replace("\", "\\")

                oUri = New Uri(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase) & "\Registration" & oRegistration.ID & ".pdf")
                path = oUri.AbsolutePath.Replace("/", "\").Replace("%20", " ").Replace("\", "\\")

                If Not System.IO.File.Exists(path) Then
                    GeneratePdfGroupInvoice(oGroup, oConference, directory, cBulkRegistration, cBulkRegistrationCombo)
                End If

                oEmail.AttachFile(path, "TAX INVOICE.pdf")
                bReturn = oEmail.SendEmail(oGroup.GroupLeader.Email, oGroup.GroupLeader.Name, sError)
                System.IO.File.Delete(path)
            Catch ex As Exception
                sError = ex.Message
            End Try

            Return bReturn

        End Function

        Public Function PlanetKidsConsentForm(ByVal iContactID As Integer, ByVal iRegistrationID As Integer, ByVal iContactToSendTo As Integer) As Boolean

            'For this email, we hand over control to the web server to do this.
            'This email takes a bit of time to generate and send, and has an attachment as well.
            'It's much easier doing this with this method - trying to send an email w/attachment through the webservice would be quite difficult...

            Dim wc As New System.Net.WebClient
            wc.DownloadString("http://accounts.planetshakers.com/events/administration/SendEmailConsentForm.aspx?ContactID=" & iContactID.ToString & "&RegistrationID=" & iRegistrationID.ToString & "&ContactToSendToID=" & iContactToSendTo.ToString)

            Return True

        End Function

        Public Function SingleTravelInformation(ByVal oContact As CContact, ByVal oConference As CConference, ByRef sError As String) As Boolean

            Dim sEmail As New Text.StringBuilder

            sEmail.AppendLine("Dear Planetshakers Travel,<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Could you please email the person listed below regarding travel/accommodation information for " & oConference.Name & "<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("<strong>Name:</strong> " & oContact.Name & "<br />")
            sEmail.AppendLine("<strong>Email:</strong> " & oContact.Email & "<br />")
            sEmail.AppendLine("<br />")

            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Blessings,")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Registrations Department<br />")
            sEmail.AppendLine("Planetshakers Conference<br />")
            sEmail.AppendLine("PO Box 5171<br />")
            sEmail.AppendLine("South Melbourne Vic 3205<br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("Phone: +61 3 9896 7999<br />")
            sEmail.AppendLine("Email: <a href=""mailto:conference@planetshakers.com"">conference@planetshakers.com</a><br />")
            sEmail.AppendLine("<br />")
            sEmail.AppendLine("<br />")

            Dim oEmail As New CEmail
            oEmail.EmailText = sEmail.ToString
            oEmail.Subject = "Request for travel/accommodation information"
            oEmail.ToAddress = "travel@planetshakers.com"
            oEmail.ToName = "Planetshakers Travel"
            Return oEmail.SendEmail(sError)

        End Function

    End Module

End Namespace