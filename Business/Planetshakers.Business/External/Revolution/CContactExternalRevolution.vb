Namespace External.Revolution

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CContactExternalRevolution

        Private miContactID As Integer
        Private miReferredByID As Integer
        Private mbAcceptedTermsAndConditions As Boolean

        Private mcSubscription As CArrayList(Of CContactSubscription)

        Private mbChanged As Boolean

        Public Sub New()

            miContactID = 0
            miReferredByID = 0
            mbAcceptedTermsAndConditions = False

            mcSubscription = New CArrayList(Of CContactSubscription)

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()
            LoadDataRow(oDataRow)

        End Sub

        Public Sub LoadDataRow(ByRef oDataRow As DataRow)

            Dim oChildRow As DataRow

            miContactID = oDataRow("Contact_ID")
            miReferredByID = oDataRow("ReferredBy_ID")
            mbAcceptedTermsAndConditions = oDataRow("AcceptedTermsAndConditions")

            For Each oChildRow In oDataRow.GetChildRows("ContactSubscription")
                mcSubscription.Add(New CContactSubscription(oChildRow))
            Next

            mcSubscription.ResetChanges()

            mbChanged = False

        End Sub

        Public Sub LoadID(ByVal iContactID As Integer)

            Dim oDataSet As DataSet

            Dim oTableMappings As New ArrayList
            oTableMappings.Add(New Common.DataTableMapping("Table", "ContactExternalRevolution"))
            oTableMappings.Add(New Common.DataTableMapping("Table1", "ContactSubscription"))
            oTableMappings.Add(New Common.DataTableMapping("Table2", "SubscriptionPayment"))

            oDataSet = DataAccess.GetDataSet("sp_Common_ContactExternalRevolutionRead " & iContactID.ToString, oTableMappings)

            oDataSet.Relations.Add("ContactSubscription", oDataSet.Tables("ContactExternalRevolution").Columns("Contact_ID"), _
                    oDataSet.Tables("ContactSubscription").Columns("Contact_ID"))

            oDataSet.Relations.Add("SubscriptionPayment", oDataSet.Tables("ContactSubscription").Columns("ContactSubscription_ID"), _
                    oDataSet.Tables("SubscriptionPayment").Columns("ContactSubscription_ID"))

            If oDataSet.Tables("ContactExternalRevolution").Rows.Count = 1 Then
                LoadDataRow(oDataSet.Tables("ContactExternalRevolution").Rows(0))
            End If

        End Sub


        Public Sub Save(ByVal oUser As CUser)

            Dim oParameters As New ArrayList

            If mbChanged Then

                oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", miContactID))
                oParameters.Add(New SqlClient.SqlParameter("@ReferredBy_ID", SQLNull(miReferredByID)))
                oParameters.Add(New SqlClient.SqlParameter("@AcceptedTermsAndConditions", mbAcceptedTermsAndConditions))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))

                DataAccess.ExecuteCommand("sp_Common_ContactExternalRevolutionUpdate", oParameters)

                mbChanged = False

            End If

            SaveSubscriptions(oUser)

            mcSubscription.ResetChanges()

        End Sub

        Private Sub SaveSubscriptions(ByVal oUser As CUser)

            Dim oParameters As ArrayList
            Dim oSubscription As CContactSubscription

            'Remove the Deleted subscriptions
            For Each oSubscription In mcSubscription.Removed
                oParameters = New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@ContactSubscription_ID", oSubscription.ID))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))
                DataAccess.ExecuteCommand("sp_Revolution_ContactSubscriptionDelete", oParameters)
            Next

            'Add/Update all the others...
            For Each oSubscription In mcSubscription
                oSubscription.ContactID = miContactID
                oSubscription.Save(oUser)
            Next

        End Sub

        Public Function ContentDownloadsRead() As DataTable

            Return DataAccess.GetDataTable("EXEC sp_Revolution_ContentDownloadReadByContact " & miContactID.ToString)

        End Function
        Public Function ContentDownloadLimitRead() As DataTable

            Return DataAccess.GetDataTable("EXEC sp_Revolution_ContentDownloadLimitReadByContact " & miContactID.ToString)

        End Function

        Public Sub ResetDownload(ByVal iContentID As Integer)

            DataAccess.ExecuteCommand("sp_Revolution_ContentResetDownloads " & miContactID.ToString & ", " & iContentID.ToString)

        End Sub

        Public ReadOnly Property Subscription() As CArrayList(Of CContactSubscription)
            Get
                Return mcSubscription
            End Get
        End Property

        Public Property ContactID() As Integer
            Get
                Return miContactID
            End Get
            Set(ByVal value As Integer)
                If miContactID <> value Then mbChanged = True
                miContactID = value
            End Set
        End Property
        Public Property ReferredByID() As Integer
            Get
                Return miReferredByID
            End Get
            Set(ByVal value As Integer)
                If miReferredByID <> value Then mbChanged = True
                miReferredByID = value
            End Set
        End Property
        Public ReadOnly Property ReferredBy() As CReferredBy
            Get
                Return Lists.External.Revolution.GetReferredByByID(miReferredByID)
            End Get
        End Property
        Public Property AcceptedTermsAndConditions() As Boolean
            Get
                Return mbAcceptedTermsAndConditions
            End Get
            Set(ByVal value As Boolean)
                If mbAcceptedTermsAndConditions <> value Then mbChanged = True
                mbAcceptedTermsAndConditions = value
            End Set
        End Property

        Public ReadOnly Property SubscriptionActive() As CArrayList(Of CContactSubscription)
            Get
                Dim oArrayList As New CArrayList(Of CContactSubscription)
                Dim oContactSubscription As CContactSubscription
                For Each oContactSubscription In Subscription
                    If Not oContactSubscription.SubscriptionCancelled Then
                        If GetDateTime < DateAdd(DateInterval.Month, oContactSubscription.Subscription.SubscriptionActive, CDate(oContactSubscription.SubscriptionStartDate)) Then
                            oArrayList.Add(oContactSubscription)
                        End If
                    End If
                Next
                Return oArrayList
            End Get
        End Property

        Public ReadOnly Property SubscriptionCurrent() As CArrayList(Of CContactSubscription)
            Get
                Dim oArrayList As New CArrayList(Of CContactSubscription)
                Dim oContactSubscription As CContactSubscription
                For Each oContactSubscription In Subscription
                    If Not oContactSubscription.SubscriptionCancelled Then
                        If GetDateTime < DateAdd(DateInterval.Month, oContactSubscription.Subscription.SubscriptionLength, CDate(oContactSubscription.SubscriptionStartDate)) Then
                            oArrayList.Add(oContactSubscription)
                        End If
                    End If
                Next
                Return oArrayList
            End Get
        End Property

        Public ReadOnly Property SubscriptionLength() As CArrayList(Of CContactSubscription)
            Get
                Dim oArrayList As New CArrayList(Of CContactSubscription)
                Dim oContactSubscription As CContactSubscription
                For Each oContactSubscription In Subscription
                    If Not oContactSubscription.SubscriptionCancelled Then
                        If GetDateTime < DateAdd(DateInterval.Month, oContactSubscription.Subscription.SubscriptionActive, CDate(oContactSubscription.SubscriptionStartDate)) Then
                            oArrayList.Add(oContactSubscription)
                        End If
                    End If
                Next
                Return oArrayList
            End Get
        End Property

        Public ReadOnly Property SubscriptionEndDate() As Date
            Get
                If SubscriptionActive.Count = 0 Then
                    Return Nothing
                Else
                    Dim oContactSubscription As CContactSubscription
                    Dim dDate As Date
                    For Each oContactSubscription In SubscriptionActive
                        If dDate = Nothing OrElse DateAdd(DateInterval.Day, -1, DateAdd(DateInterval.Month, oContactSubscription.Subscription.SubscriptionLength, CDate(oContactSubscription.SubscriptionStartDate))) > dDate Then
                            dDate = DateAdd(DateInterval.Day, -1, DateAdd(DateInterval.Month, oContactSubscription.Subscription.SubscriptionLength, CDate(oContactSubscription.SubscriptionStartDate)))
                        End If
                    Next
                    Return dDate
                End If
            End Get
        End Property

        Public ReadOnly Property AvailableContentPacks() As CArrayList(Of AdditionalContentPack)
            Get
                'Retrieve a list of Content Packs available for purchase - content packs that are not covered by subscription
                Dim oArrayList As New CArrayList(Of AdditionalContentPack)
                Dim oContentPack As CContentPack
                Dim oContactSubscription As CContactSubscription
                Dim oSubscription As CSubscription = Lists.External.Revolution.GetSubscriptionByID(EnumSubscriptions.OneMonthSubscription)

                For Each oContentPack In Lists.External.Revolution.ContentPack.Values
                    Dim bCovered As Boolean = False
                    For Each oContactSubscription In mcSubscription
                        If oContentPack.StartDate >= oContactSubscription.SubscriptionStartDate And _
                            oContentPack.StartDate < oContactSubscription.SubscriptionStartDate.AddMonths(oContactSubscription.Subscription.SubscriptionLength) Then
                            bCovered = True
                        End If
                    Next
                    If Not bCovered Then
                        If oContentPack.StartDate <= GetDateTime Then
                            Dim oAdditionalContentPack As New AdditionalContentPack
                            oAdditionalContentPack.ContentPack = oContentPack
                            oAdditionalContentPack.Price = oSubscription.Cost
                            oArrayList.Add(oAdditionalContentPack)
                        End If
                    End If
                Next
                Return oArrayList
            End Get
        End Property

        Public ReadOnly Property HasActiveSubscription() As Boolean
            Get
                Return SubscriptionActive.Count > 0
            End Get
        End Property
        Public ReadOnly Property HasActiveYearlySubscription() As Boolean
            Get
                Dim bReturn As Boolean = False
                Dim oSubscription As CContactSubscription
                For Each oSubscription In SubscriptionActive
                    If oSubscription.Subscription.ID = EnumSubscriptions.TwelveMonthGiftCard Or _
                       oSubscription.Subscription.ID = EnumSubscriptions.TwelveMonthSubscription Then bReturn = True
                Next
                Return bReturn
            End Get
        End Property

        Public ReadOnly Property ContentAuthorised(ByVal oContent As CContent) As Boolean
            Get
                Dim oContactSubscription As CContactSubscription
                Dim oCheckContent As CContent
                Dim oContentPack As CContentPack

                For Each oContactSubscription In Subscription
                    If Not oContactSubscription.SubscriptionCancelled Then
                        For Each oContentPack In oContactSubscription.Subscription.ContentPacks
                            If oContentPack.StartDate >= CDate(oContactSubscription.SubscriptionStartDate) And _
                                oContentPack.StartDate < DateAdd(DateInterval.Month, oContactSubscription.Subscription.SubscriptionLength, CDate(oContactSubscription.SubscriptionStartDate)) Then
                                For Each oCheckContent In oContentPack.Content
                                    If oCheckContent.ID = oContent.ID Then Return True
                                Next
                            End If
                        Next
                    End If
                Next

                Dim iSubscriptionCount As Integer
                Select Case oContent.ContentPack.ID
                    Case EnumBonusContentPacks.PS08Sermons
                        'To get the PS08 Sermons, the user must have 2 x 12 month subscriptions on their account
                        For Each oContactSubscription In Subscription
                            If oContactSubscription.Subscription.SubscriptionLength >= 12 Then iSubscriptionCount += 1
                        Next
                        If iSubscriptionCount >= 2 Then
                            Return True
                        End If

                    Case Else
                End Select

                Return False

            End Get
        End Property

        Public ReadOnly Property Changed() As Boolean
            Get

                If mbChanged Then Return True

                If mcSubscription.Changed Then Return True

                Dim oContactSubscription As CContactSubscription
                For Each oContactSubscription In mcSubscription
                    If oContactSubscription.Changed Then Return True
                Next

                Return False

            End Get
        End Property

        Public Sub ResetChanges()

            mbChanged = False

            mcSubscription.ResetChanges()

            Dim oContactSubscription As CContactSubscription
            For Each oContactSubscription In mcSubscription
                oContactSubscription.ResetChanges()
            Next

        End Sub

        Public Structure AdditionalContentPack
            Private mdPrice As Decimal
            Private moContentPack As CContentPack
            Public Property ContentPack() As CContentPack
                Get
                    Return moContentPack
                End Get
                Set(ByVal value As CContentPack)
                    moContentPack = value
                End Set
            End Property
            Public Property Price() As Decimal
                Get
                    Return mdPrice
                End Get
                Set(ByVal value As Decimal)
                    mdPrice = value
                End Set
            End Property
        End Structure

    End Class

End Namespace