Namespace External.Revolution

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CContactSubscription
        Implements IComparable

        Private miContactSubscriptionID As Integer
        Private miContactID As Integer
        Private miSubscriptionID As Integer
        Private mdSubscriptionDiscount As Decimal
        Private mdSignupDate As Date
        Private mdSubscriptionStartDate As Date
        Private mbSubscriptionCancelled As Boolean
        Private mbEndOfTrialEmailSent As Boolean
        Private miSubscribedByID As Integer
        Private mcPayment As CArrayList(Of CSubscriptionPayment)
        Private msGUID As String

        Private mbChanged As Boolean

        Public Sub New()

            miContactSubscriptionID = 0
            miContactID = 0
            miSubscriptionID = 0
            mdSubscriptionDiscount = 0
            mdSignupDate = Nothing
            mdSubscriptionStartDate = Nothing
            mbSubscriptionCancelled = False
            mbEndOfTrialEmailSent = False
            mcPayment = New CArrayList(Of CSubscriptionPayment)
            miSubscribedByID = 0
            msGUID = System.Guid.NewGuid().ToString

            mbChanged = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miContactSubscriptionID = oDataRow("ContactSubscription_ID")
            miContactID = oDataRow("Contact_ID")
            miSubscriptionID = oDataRow("Subscription_ID")
            mdSubscriptionDiscount = oDataRow("SubscriptionDiscount")
            mdSignupDate = oDataRow("SignupDate")
            mdSubscriptionStartDate = oDataRow("SubscriptionStartDate")
            mbSubscriptionCancelled = oDataRow("SubscriptionCancelled")
            mbEndOfTrialEmailSent = oDataRow("EndOfTrialEmailSent")
            miSubscribedByID = oDataRow("SubscribedBy_ID")

            Dim oPaymentRow As DataRow
            For Each oPaymentRow In oDataRow.GetChildRows("SubscriptionPayment")
                mcPayment.Add(New CSubscriptionPayment(oPaymentRow))
            Next

            mcPayment.ResetChanges()

            mbChanged = False

        End Sub

        Public Sub Save(ByVal oUser As CUser)

            If mbChanged Then

                Dim oParameters As New ArrayList

                oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", miContactID))
                oParameters.Add(New SqlClient.SqlParameter("@Subscription_ID", miSubscriptionID))
                oParameters.Add(New SqlClient.SqlParameter("@SubscriptionDiscount", mdSubscriptionDiscount))
                oParameters.Add(New SqlClient.SqlParameter("@SignupDate", SQLDateTime(mdSignupDate)))
                oParameters.Add(New SqlClient.SqlParameter("@SubscriptionStartDate", SQLDate(mdSubscriptionStartDate)))
                oParameters.Add(New SqlClient.SqlParameter("@SubscriptionCancelled", mbSubscriptionCancelled))
                oParameters.Add(New SqlClient.SqlParameter("@EndOfTrialEmailSent", mbEndOfTrialEmailSent))
                oParameters.Add(New SqlClient.SqlParameter("@SubscribedBy_ID", miSubscribedByID))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))

                If Me.ID = 0 Then
                    miContactSubscriptionID = DataAccess.GetValue("sp_Revolution_ContactSubscriptionInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@ContactSubscription_ID", ID))
                    DataAccess.ExecuteCommand("sp_Revolution_ContactSubscriptionUpdate", oParameters)
                End If

            End If

            SavePayments(oUser)

            mcPayment.ResetChanges()

        End Sub

        Public Sub SavePayments(ByVal oUser As CUser)

            Dim oParameters As ArrayList
            Dim oSubscriptionPayment As CSubscriptionPayment

            'Remove the Deleted payments
            For Each oSubscriptionPayment In mcPayment.Removed
                oParameters = New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@SubscriptionPayment_ID", oSubscriptionPayment.ID))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))
                DataAccess.ExecuteCommand("sp_Revolution_SubscriptionPaymentDelete", oParameters)
            Next

            'Add/Update all the othersw
            For Each oSubscriptionPayment In mcPayment
                oSubscriptionPayment.ContactSubscriptionID = Me.ID
                oSubscriptionPayment.Save(oUser)
            Next

        End Sub

        Public ReadOnly Property Payments() As CArrayList(Of CSubscriptionPayment)
            Get
                Return mcPayment
            End Get
        End Property

        Public ReadOnly Property ID() As Integer
            Get
                Return miContactSubscriptionID
            End Get
        End Property

        Public Property ContactID() As Integer
            Get
                Return miContactID
            End Get
            Set(ByVal value As Integer)
                If miContactID <> value Then mbChanged = True
                miContactID = value
            End Set
        End Property

        Public Property SubscriptionID() As Integer
            Get
                Return miSubscriptionID
            End Get
            Set(ByVal value As Integer)
                If miSubscriptionID <> value Then mbChanged = True
                miSubscriptionID = value
            End Set
        End Property

        Public ReadOnly Property Subscription() As CSubscription
            Get
                Return Lists.External.Revolution.GetSubscriptionByID(miSubscriptionID)
            End Get
        End Property
        Public Property SubscriptionDiscount() As Decimal
            Get
                Return mdSubscriptionDiscount
            End Get
            Set(ByVal value As Decimal)
                If mdSubscriptionDiscount <> value Then mbChanged = True
                mdSubscriptionDiscount = value
            End Set
        End Property

        Public Property SignupDate() As Date
            Get
                Return mdSignupDate
            End Get
            Set(ByVal value As Date)
                If mdSignupDate <> value Then mbChanged = True
                mdSignupDate = value
            End Set
        End Property

        Public Property SubscriptionStartDate() As Date
            Get
                Return mdSubscriptionStartDate
            End Get
            Set(ByVal value As Date)
                If CheckDateChanged(mdSubscriptionStartDate, value) Then mbChanged = True
                mdSubscriptionStartDate = value
            End Set
        End Property

        Public Property SubscriptionCancelled() As Boolean
            Get
                Return mbSubscriptionCancelled
            End Get
            Set(ByVal value As Boolean)
                If mbSubscriptionCancelled <> value Then mbChanged = True
                mbSubscriptionCancelled = value
            End Set
        End Property

        Public Property EndOfTrialEmailSent() As Boolean
            Get
                Return mbEndOfTrialEmailSent
            End Get
            Set(ByVal value As Boolean)
                If mbEndOfTrialEmailSent <> value Then mbChanged = True
                mbEndOfTrialEmailSent = value
            End Set
        End Property
        Public Property SubscribedByID() As Integer
            Get
                Return miSubscribedByID
            End Get
            Set(ByVal value As Integer)
                If miSubscribedByID <> value Then mbChanged = True
                miSubscribedByID = value
            End Set
        End Property
        Public ReadOnly Property SubscribedBy() As CUser
            Get
                Return Lists.GetUserByID(miSubscribedByID)
            End Get
        End Property
        Public ReadOnly Property GUID() As String
            Get
                Return msGUID
            End Get
        End Property

        Public ReadOnly Property Changed() As Boolean
            Get
                If mbChanged Then Return True

                If mcPayment.Changed Then Return True

                Dim oSubscriptionPayment As CSubscriptionPayment
                For Each oSubscriptionPayment In mcPayment
                    If oSubscriptionPayment.Changed Then Return True
                Next

            End Get
        End Property

        Public Sub ResetChanges()

            mbChanged = False

            mcPayment.ResetChanges()

            Dim oSubscriptionPayment As CSubscriptionPayment
            For Each oSubscriptionPayment In mcPayment
                oSubscriptionPayment.ResetChanges()
            Next

        End Sub
        Public ReadOnly Property SubscriptionCost() As Decimal
            Get
                If mbSubscriptionCancelled Then
                    Return 0
                Else
                    Return Math.Round(Subscription.Cost * ((100 - mdSubscriptionDiscount) / 100), 2)
                End If
            End Get
        End Property

        Public ReadOnly Property PaymentsTotal() As Decimal
            Get
                Dim dTotal As Decimal
                Dim oPayment As CSubscriptionPayment
                For Each oPayment In mcPayment
                    dTotal += oPayment.PaymentAmount
                Next
                Return dTotal
            End Get
        End Property

        Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo

            Dim otherContactSubscription As CContactSubscription = DirectCast(obj, CContactSubscription)
            Return Date.Compare(CDate(Me.SubscriptionStartDate), CDate(otherContactSubscription.SubscriptionStartDate))

        End Function

    End Class

End Namespace
