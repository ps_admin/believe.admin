Namespace External.Revolution

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CContentFile

        Private miContentFileID As Integer
        Private miContentID As Integer
        Private msFileName As String
        Private mbAllowDownload As Boolean
        Private msExternalDownloadLocation As String
        Private miContentFileTypeID As Integer
        Private miContentFileCategoryID As Integer
        Private mbVisible As Boolean
        Private miSortOrder As Integer
        Private msGUID As String

        Private mbChanged As Boolean

        Public Sub New()

            miContentFileID = 0
            miContentID = 0
            msFileName = String.Empty
            mbAllowDownload = False
            msExternalDownloadLocation = String.Empty
            miContentFileTypeID = 0
            miContentFileCategoryID = 0
            mbVisible = False
            miSortOrder = 0
            msGUID = System.Guid.NewGuid.ToString

            mbChanged = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miContentFileID = oDataRow("ContentFile_ID")
            miContentID = oDataRow("Content_ID")
            msFileName = oDataRow("FileName")
            mbAllowDownload = oDataRow("AllowDownload")
            msExternalDownloadLocation = oDataRow("ExternalDownloadLocation")
            miContentFileTypeID = oDataRow("ContentFileType_ID")
            miContentFileCategoryID = oDataRow("ContentFileCategory_ID")
            mbVisible = oDataRow("Visible")
            miSortOrder = oDataRow("SortOrder")
            msGUID = oDataRow("GUID").ToString

            mbChanged = False

        End Sub

        Public Sub Save()
            Save(miContentID)
        End Sub

        Public Sub Save(ByVal iContentID As Integer)

            If mbChanged Then

                Dim oParameters As New ArrayList

                oParameters.Add(New SqlClient.SqlParameter("@Content_ID", iContentID))
                oParameters.Add(New SqlClient.SqlParameter("@FileName", msFileName))
                oParameters.Add(New SqlClient.SqlParameter("@AllowDownload", mbAllowDownload))
                oParameters.Add(New SqlClient.SqlParameter("@ExternalDownloadLocation", msExternalDownloadLocation))
                oParameters.Add(New SqlClient.SqlParameter("@ContentFileType_ID", miContentFileTypeID))
                oParameters.Add(New SqlClient.SqlParameter("@ContentFileCategory_ID", miContentFileCategoryID))
                oParameters.Add(New SqlClient.SqlParameter("@Visible", mbVisible))
                oParameters.Add(New SqlClient.SqlParameter("@SortOrder", miSortOrder))

                If Me.ID = 0 Then
                    miContentFileID = DataAccess.GetValue("sp_Revolution_ContentFileInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@ContentFile_ID", ID))
                    DataAccess.ExecuteCommand("sp_Revolution_ContentFileUpdate", oParameters)
                End If

                mbChanged = False

            End If

        End Sub

        Public ReadOnly Property ContentFileLocation() As String
            Get
                Return Options.ContentFolder & "\" & miContentFileID & "\" & msFileName
            End Get
        End Property
        Public ReadOnly Property ID() As Integer
            Get
                Return miContentFileID
            End Get
        End Property
        Public Property ContentID() As Integer
            Get
                Return miContentID
            End Get
            Set(ByVal value As Integer)
                miContentID = value
            End Set
        End Property
        Public ReadOnly Property Content() As CContent
            Get
                Return Lists.External.Revolution.GetContentByID(miContentID)
            End Get
        End Property
        Public Property FileName() As String
            Get
                Return msFileName
            End Get
            Set(ByVal value As String)
                If msFileName <> value Then mbChanged = True
                msFileName = value
            End Set
        End Property
        Public Property AllowDownload() As Boolean
            Get
                Return mbAllowDownload
            End Get
            Set(ByVal value As Boolean)
                If mbAllowDownload <> value Then mbChanged = True
                mbAllowDownload = value
            End Set
        End Property
        Public Property ExternalDownloadLocation() As String
            Get
                Return msExternalDownloadLocation
            End Get
            Set(ByVal value As String)
                If msExternalDownloadLocation <> value Then mbChanged = True
                msExternalDownloadLocation = value
            End Set
        End Property
        Public Property ContentFileTypeID() As Integer
            Get
                Return miContentFileTypeID
            End Get
            Set(ByVal value As Integer)
                If miContentFileTypeID <> value Then mbChanged = True
                miContentFileTypeID = value
            End Set
        End Property
        Public ReadOnly Property ContentFileType() As CContentFileType
            Get
                Return Lists.External.Revolution.GetContentFileTypeByID(miContentFileTypeID)
            End Get
        End Property
        Public Property ContentFileCategoryID() As Integer
            Get
                Return miContentFileCategoryID
            End Get
            Set(ByVal value As Integer)
                If miContentFileCategoryID <> value Then mbChanged = True
                miContentFileCategoryID = value
            End Set
        End Property
        Public ReadOnly Property ContentFileCategory() As CContentFileCategory
            Get
                Return Lists.External.Revolution.GetContentFileCategoryByID(miContentFileCategoryID)
            End Get
        End Property
        Public Property Visible() As Boolean
            Get
                Return mbVisible
            End Get
            Set(ByVal value As Boolean)
                If mbVisible <> value Then mbChanged = True
                mbVisible = value
            End Set
        End Property
        Public Property SortOrder() As Integer
            Get
                Return miSortOrder
            End Get
            Set(ByVal value As Integer)
                If miSortOrder <> value Then mbChanged = True
                miSortOrder = value
            End Set
        End Property
        Public ReadOnly Property GUID() As String
            Get
                Return msGUID
            End Get
        End Property

        Public ReadOnly Property FileSize() As Long
            Get
                If FileName <> "" Then
                    Dim sFileLocation As String

                    sFileLocation = Options.ContentFolder & miContentID & "\" & msFileName

                    If System.IO.File.Exists(sFileLocation) Then
                        Dim oFile As IO.FileInfo
                        oFile = New IO.FileInfo(sFileLocation)
                        Return oFile.Length
                    End If
                End If
            End Get
        End Property

    End Class

End Namespace