Namespace External.Revolution

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CContentFileType

        Private miContentFileTypeID As Integer
        Private msName As String
        Private msMimeType As String
        Private mbRestrictedDownload As Boolean

        Public Sub New()

            miContentFileTypeID = 0
            msName = String.Empty
            msMimeType = String.Empty
            mbRestrictedDownload = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miContentFileTypeID = oDataRow("ContentFileType_ID")
            msName = oDataRow("Name")
            msMimeType = oDataRow("MimeType")
            mbRestrictedDownload = oDataRow("RestrictedDownload")

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miContentFileTypeID
            End Get
        End Property
        Public Property Name() As String
            Get
                Return msName
            End Get
            Set(ByVal value As String)
                msName = value
            End Set
        End Property
        Public Property MimeType() As String
            Get
                Return msMimeType
            End Get
            Set(ByVal value As String)
                msMimeType = value
            End Set
        End Property
        Public Property RestrictedDownload() As Boolean
            Get
                Return mbRestrictedDownload
            End Get
            Set(ByVal value As Boolean)
                mbRestrictedDownload = False
            End Set
        End Property

    End Class

End Namespace
