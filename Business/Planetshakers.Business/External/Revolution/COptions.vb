Public Class COptions

    Dim miPremiumDownloadLimit As Integer
    Dim miDownloadWindow As Integer
    Dim msContentFolder As String
    Dim miBankAccountID As Integer

    Dim mdOptionsRefresh As Date

    Private miRefreshInterval As Integer = 10

    Public Sub New()
        miPremiumDownloadLimit = -1
        miDownloadWindow = -1
        msContentFolder = String.Empty
        miBankAccountID = 0
    End Sub

    Private Sub RefreshOptions()

        Dim oDataRow As DataRow = DataAccess.GetDataRow("SELECT * FROM Revolution_Options")
        miPremiumDownloadLimit = oDataRow("PremiumDownloadLimit")
        miDownloadWindow = oDataRow("DownloadWindow")
        msContentFolder = oDataRow("ContentFolder")
        miBankAccountID = oDataRow("BankAccount_ID")

        If msContentFolder.EndsWith("\") Then
            msContentFolder = msContentFolder.Substring(0, msContentFolder.Length - 1)
        End If

    End Sub

    Public Function PremiumDownloadLimit() As Integer
        If miPremiumDownloadLimit = -1 Or DateDiff(DateInterval.Minute, GetDateTime, mdOptionsRefresh) > miRefreshInterval Then
            RefreshOptions()
        End If
        Return miPremiumDownloadLimit
    End Function

    Public Function DownloadWindow() As Integer
        If miDownloadWindow = -1 Or DateDiff(DateInterval.Minute, GetDateTime, mdOptionsRefresh) > miRefreshInterval Then
            RefreshOptions()
        End If
        Return miDownloadWindow
    End Function

    Public Function ContentFolder() As String
        If msContentFolder = String.Empty Or DateDiff(DateInterval.Minute, GetDateTime, mdOptionsRefresh) > miRefreshInterval Then
            RefreshOptions()
        End If
        Return msContentFolder
    End Function

    Public Function BankAccountID() As Integer
        If miBankAccountID = 0 Or DateDiff(DateInterval.Minute, GetDateTime, mdOptionsRefresh) > miRefreshInterval Then
            RefreshOptions()
        End If
        Return miBankAccountID
    End Function
    Public Function BankAccount() As CBankAccount
        Return Lists.GetBankAccountByID(BankAccountID)
    End Function

End Class
