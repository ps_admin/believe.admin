Namespace External.Revolution

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CSubscriptionPayment

        Private miSubscriptionPaymentID As Integer
        Private miContactSubscriptionID As Integer
        Private miPaymentTypeID As Integer
        Private mdPaymentAmount As Decimal
        Private msCCNumber As String
        Private msCCExpiry As String
        Private msCCName As String
        Private msCCPhone As String
        Private mbCCManual As Boolean
        Private msCCTransactionRef As String
        Private mbCCRefund As Boolean
        Private msChequeDrawer As String
        Private msChequeBank As String
        Private msChequeBranch As String
        Private msComment As String
        Private mdPaymentDate As Date
        Private miPaymentByID As Integer
        Private miBankAccountID As Integer
        Private msGUID As String

        Private mbChanged As Boolean

        Public Sub New()

            miSubscriptionPaymentID = 0
            miContactSubscriptionID = 0
            miPaymentTypeID = 0
            mdPaymentAmount = 0
            msCCNumber = String.Empty
            msCCExpiry = String.Empty
            msCCName = String.Empty
            msCCPhone = String.Empty
            mbCCManual = False
            msCCTransactionRef = String.Empty
            mbCCRefund = False
            msChequeDrawer = String.Empty
            msChequeBank = String.Empty
            msChequeBranch = String.Empty
            msComment = String.Empty
            mdPaymentDate = GetDateTime
            miPaymentByID = 0
            miBankAccountID = 0
            msGUID = System.Guid.NewGuid.ToString

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miSubscriptionPaymentID = oDataRow("SubscriptionPayment_ID")
            miContactSubscriptionID = oDataRow("ContactSubscription_ID")
            miPaymentTypeID = oDataRow("PaymentType_ID")
            mdPaymentAmount = oDataRow("PaymentAmount")
            msCCNumber = oDataRow("CCNumber")
            msCCExpiry = oDataRow("CCExpiry")
            msCCName = oDataRow("CCName")
            msCCPhone = oDataRow("CCPhone")
            mbCCManual = oDataRow("CCManual")
            msCCTransactionRef = oDataRow("CCTransactionRef")
            mbCCRefund = oDataRow("CCRefund")
            msChequeDrawer = oDataRow("ChequeDrawer")
            msChequeBank = oDataRow("ChequeBank")
            msChequeBranch = oDataRow("ChequeBranch")
            msComment = oDataRow("Comment")
            mdPaymentDate = oDataRow("PaymentDate")
            miPaymentByID = oDataRow("PaymentBy_ID")
            miBankAccountID = oDataRow("BankAccount_ID")

        End Sub

        Public Sub Save(ByVal oUser As CUser)

            If mbChanged Then

                Dim oParameters As New ArrayList

                oParameters.Add(New SqlClient.SqlParameter("@ContactSubscription_ID", miContactSubscriptionID))
                oParameters.Add(New SqlClient.SqlParameter("@PaymentType_ID", SQLObject(PaymentType)))
                oParameters.Add(New SqlClient.SqlParameter("@PaymentAmount", mdPaymentAmount))
                oParameters.Add(New SqlClient.SqlParameter("@CCNumber", msCCNumber))
                oParameters.Add(New SqlClient.SqlParameter("@CCExpiry", msCCExpiry))
                oParameters.Add(New SqlClient.SqlParameter("@CCName", msCCName))
                oParameters.Add(New SqlClient.SqlParameter("@CCPhone", msCCPhone))
                oParameters.Add(New SqlClient.SqlParameter("@CCManual", mbCCManual))
                oParameters.Add(New SqlClient.SqlParameter("@CCTransactionRef", msCCTransactionRef))
                oParameters.Add(New SqlClient.SqlParameter("@CCRefund", mbCCRefund))
                oParameters.Add(New SqlClient.SqlParameter("@ChequeDrawer", msChequeDrawer))
                oParameters.Add(New SqlClient.SqlParameter("@ChequeBank", msChequeBank))
                oParameters.Add(New SqlClient.SqlParameter("@ChequeBranch", msChequeBranch))
                oParameters.Add(New SqlClient.SqlParameter("@Comment", msComment))
                oParameters.Add(New SqlClient.SqlParameter("@PaymentDate", SQLDateTime(mdPaymentDate)))
                oParameters.Add(New SqlClient.SqlParameter("@PaymentBy_ID", SQLObject(PaymentBy)))
                oParameters.Add(New SqlClient.SqlParameter("@BankAccount_ID", SQLObject(BankAccount)))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))

                If Me.ID = 0 Then
                    miSubscriptionPaymentID = DataAccess.GetValue("sp_Revolution_SubscriptionPaymentInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@SubscriptionPayment_ID", miSubscriptionPaymentID))
                    DataAccess.ExecuteCommand("sp_Revolution_SubscriptionPaymentUpdate", oParameters)
                End If

            End If

        End Sub

        ReadOnly Property ID() As Integer
            Get
                Return miSubscriptionPaymentID
            End Get
        End Property
        Public Property ContactSubscriptionID() As Integer
            Get
                Return miContactSubscriptionID
            End Get
            Set(ByVal value As Integer)
                If miContactSubscriptionID <> value Then mbChanged = True
                miContactSubscriptionID = value
            End Set
        End Property
        Property PaymentTypeID() As Integer
            Get
                Return miPaymentTypeID
            End Get
            Set(ByVal Value As Integer)
                If miPaymentTypeID <> Value Then mbChanged = True
                miPaymentTypeID = Value
            End Set
        End Property
        ReadOnly Property PaymentType() As CPaymentType
            Get
                Return Lists.GetPaymentTypeByID(miPaymentTypeID)
            End Get
        End Property
        Property PaymentAmount() As Decimal
            Get
                Return mdPaymentAmount
            End Get
            Set(ByVal Value As Decimal)
                If mdPaymentAmount <> Value Then mbChanged = True
                mdPaymentAmount = Value
            End Set
        End Property
        Property CCNumber() As String
            Get
                Return msCCNumber
            End Get
            Set(ByVal Value As String)
                If msCCNumber <> Value Then mbChanged = True
                msCCNumber = Value
            End Set
        End Property
        Property CCExpiry() As String
            Get
                Return msCCExpiry
            End Get
            Set(ByVal Value As String)
                If msCCExpiry <> Value Then mbChanged = True
                msCCExpiry = Value
            End Set
        End Property
        Property CCName() As String
            Get
                Return msCCName
            End Get
            Set(ByVal Value As String)
                If msCCName <> Value Then mbChanged = True
                msCCName = Value
            End Set
        End Property
        Property CCPhone() As String
            Get
                Return msCCPhone
            End Get
            Set(ByVal Value As String)
                If msCCPhone <> Value Then mbChanged = True
                msCCPhone = Value
            End Set
        End Property
        Property CCManual() As Boolean
            Get
                Return mbCCManual
            End Get
            Set(ByVal Value As Boolean)
                If mbCCManual <> Value Then mbChanged = True
                mbCCManual = Value
            End Set
        End Property
        Property CCTransactionRef() As String
            Get
                Return msCCTransactionRef
            End Get
            Set(ByVal Value As String)
                If msCCTransactionRef <> Value Then mbChanged = True
                msCCTransactionRef = Value
            End Set
        End Property
        Property CCRefund() As Boolean
            Get
                Return mbCCRefund
            End Get
            Set(ByVal Value As Boolean)
                If mbCCRefund <> Value Then mbChanged = True
                mbCCRefund = Value
            End Set
        End Property
        Property ChequeDrawer() As String
            Get
                Return msChequeDrawer
            End Get
            Set(ByVal Value As String)
                If msChequeDrawer <> Value Then mbChanged = True
                msChequeDrawer = Value
            End Set
        End Property
        Property ChequeBank() As String
            Get
                Return msChequeBank
            End Get
            Set(ByVal Value As String)
                If msChequeBank <> Value Then mbChanged = True
                msChequeBank = Value
            End Set
        End Property
        Property ChequeBranch() As String
            Get
                Return msChequeBranch
            End Get
            Set(ByVal Value As String)
                If msChequeBranch <> Value Then mbChanged = True
                msChequeBranch = Value
            End Set
        End Property
        Property Comment() As String
            Get
                Return msComment
            End Get
            Set(ByVal Value As String)
                If msComment <> Value Then mbChanged = True
                msComment = Value
            End Set
        End Property
        Property PaymentDate() As Date
            Get
                Return mdPaymentDate
            End Get
            Set(ByVal Value As Date)
                If mdPaymentDate <> Value Then mbChanged = True
                mdPaymentDate = Value
            End Set
        End Property
        Property PaymentByID() As Integer
            Get
                Return miPaymentByID
            End Get
            Set(ByVal Value As Integer)
                If miPaymentByID <> Value Then mbChanged = True
                miPaymentByID = Value
            End Set
        End Property
        ReadOnly Property PaymentBy() As CUser
            Get
                Return Lists.GetUserByID(miPaymentByID)
            End Get
        End Property
        Property BankAccountID() As Integer
            Get
                Return miBankAccountID
            End Get
            Set(ByVal Value As Integer)
                If miBankAccountID <> Value Then mbChanged = True
                miBankAccountID = Value
            End Set
        End Property
        ReadOnly Property BankAccount() As CBankAccount
            Get
                Return Lists.GetBankAccountByID(miBankAccountID)
            End Get
        End Property
        ReadOnly Property GUID() As String
            Get
                GUID = msGUID
            End Get
        End Property

        Public ReadOnly Property Changed() As Boolean
            Get
                Return mbChanged
            End Get
        End Property

        Public Sub ResetChanges()
            mbChanged = False
        End Sub

    End Class

End Namespace