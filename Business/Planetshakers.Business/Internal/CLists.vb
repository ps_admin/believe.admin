Namespace Internal

    <CLSCompliant(True)> _
    Public Class CLists

        Private moChurch As Church.CLists

        Public Sub New()
            moChurch = New Church.CLists
        End Sub

        Public ReadOnly Property Church() As Church.CLists
            Get
                Return moChurch
            End Get
        End Property

    End Class

End Namespace