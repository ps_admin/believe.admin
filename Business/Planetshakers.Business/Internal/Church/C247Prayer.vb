Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class C247Prayer

        Private mi247PrayerID As Integer
        Private miContactID As Integer
        Private miDayID As Integer
        Private miTimeID As Integer
        Private miDurationID As Integer
        Private miContactMethodID As Integer
        Private mdDateAdded As Date
        Private msGUID As String

        Private mbChanged As Boolean

        Public Sub New()

            mi247PrayerID = 0
            miContactID = 0
            miDayID = 0
            miTimeID = 0
            miDurationID = 0
            miContactMethodID = 0
            mdDateAdded = Nothing
            msGUID = System.Guid.NewGuid.ToString
            mbChanged = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            mi247PrayerID = oDataRow("247Prayer_ID")
            miContactID = oDataRow("Contact_ID")
            miDayID = oDataRow("Day_ID")
            miTimeID = oDataRow("Time_ID")
            miDurationID = oDataRow("Duration_ID")
            miContactMethodID = oDataRow("ContactMethod_ID")
            mdDateAdded = oDataRow("DateAdded")
            mbChanged = False

        End Sub

        Public Sub Save(ByVal oUser As CUser)

            If mbChanged Then

                Dim oParameters As New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", miContactID))
                oParameters.Add(New SqlClient.SqlParameter("@Day_ID", miDayID))
                oParameters.Add(New SqlClient.SqlParameter("@Time_ID", miTimeID))
                oParameters.Add(New SqlClient.SqlParameter("@Duration_ID", miDurationID))
                oParameters.Add(New SqlClient.SqlParameter("@ContactMethod_ID", miContactMethodID))
                oParameters.Add(New SqlClient.SqlParameter("@DateAdded", SQLDateTime(mdDateAdded)))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))

                If Me.ID = 0 Then
                    mi247PrayerID = DataAccess.GetValue("sp_Church_247PrayerInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@247Prayer_ID", mi247PrayerID))
                    DataAccess.ExecuteCommand("sp_Church_247PrayerUpdate", oParameters)
                End If

                mbChanged = False

            End If

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return mi247PrayerID
            End Get
        End Property
        Public Property ContactID() As Integer
            Get
                Return miContactID
            End Get
            Set(ByVal value As Integer)
                If miContactID <> value Then mbChanged = True
                miContactID = value
            End Set
        End Property
        Public Property DayID() As Integer
            Get
                Return miDayID
            End Get
            Set(ByVal value As Integer)
                If miDayID <> value Then mbChanged = True
                miDayID = value
            End Set
        End Property
        Public ReadOnly Property Day() As C247PrayerDay
            Get
                Return Lists.Internal.Church.Get247PrayerDayByID(miDayID)
            End Get
        End Property
        Public Property TimeID() As Integer
            Get
                Return miTimeID
            End Get
            Set(ByVal value As Integer)
                If miTimeID <> value Then mbChanged = True
                miTimeID = value
            End Set
        End Property
        Public ReadOnly Property Time() As C247PrayerTime
            Get
                Return Lists.Internal.Church.Get247PrayerTimeByID(miTimeID)
            End Get
        End Property
        Public Property DurationID() As Integer
            Get
                Return miDurationID
            End Get
            Set(ByVal value As Integer)
                If miDurationID <> value Then mbChanged = True
                miDurationID = value
            End Set
        End Property
        Public ReadOnly Property Duration() As C247PrayerDuration
            Get
                Return Lists.Internal.Church.Get247PrayerDurationByID(miDurationID)
            End Get
        End Property
        Public Property ContactMethodID() As Integer
            Get
                Return miContactMethodID
            End Get
            Set(ByVal value As Integer)
                If miContactMethodID <> value Then mbChanged = True
                miContactMethodID = value
            End Set
        End Property
        Public ReadOnly Property ContactMethod() As C247PrayerContactMethod
            Get
                Return Lists.Internal.Church.Get247PrayerContactMethodByID(miContactMethodID)
            End Get
        End Property
        Public Property DateAdded() As Date
            Get
                Return mdDateAdded
            End Get
            Set(ByVal value As Date)
                If mdDateAdded <> value Then mbChanged = True
                mdDateAdded = value
            End Set
        End Property
        Public ReadOnly Property GUID() As String
            Get
                Return msGUID
            End Get
        End Property

    End Class

End Namespace