Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CCommentType

        Private miCommentTypeID As Integer
        Private msName As String
        Private miSortOrder As Integer

        Public Sub New()

            miCommentTypeID = 0
            msName = String.Empty

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miCommentTypeID = oDataRow("CommentType_ID")
            msName = oDataRow("Name")
            miSortOrder = oDataRow("SortOrder")

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miCommentTypeID
            End Get
        End Property
        Public Property Name() As String
            Get
                Return msName
            End Get
            Set(ByVal value As String)
                msName = value
            End Set
        End Property
        Public Property SortOrder() As String
            Get
                Return miSortOrder
            End Get
            Set(ByVal value As String)
                miSortOrder = value
            End Set
        End Property

    End Class

End Namespace