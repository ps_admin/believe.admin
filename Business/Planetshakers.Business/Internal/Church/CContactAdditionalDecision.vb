Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CContactAdditionalDecision

        Private miAdditionalDecisionID As Integer
        Private miContactID As Integer
        Private moContact As CContact
        Private mdDecisionDate As Date
        Private miDecisionTypeID As Integer
        Private miCampusDecisionID As Integer
        Private msComments As String
        Private mdEnteredDate As Date
        Private miEnteredByID As Integer
        Private msGUID As String
        Private mbChanged As Boolean

        Public Sub New()

            miAdditionalDecisionID = 0
            miContactID = 0
            mdDecisionDate = Nothing
            miDecisionTypeID = 0
            miCampusDecisionID = 0
            msComments = String.Empty
            mdEnteredDate = Nothing
            miEnteredByID = 0
            msGUID = System.Guid.NewGuid.ToString
            mbChanged = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miAdditionalDecisionID = oDataRow("AdditionalDecision_ID")
            miContactID = oDataRow("Contact_ID")
            mdDecisionDate = oDataRow("DecisionDate")
            miDecisionTypeID = oDataRow("DecisionType_ID")
            miCampusDecisionID = IsNull(oDataRow("CampusDecision_ID"), 0)
            msComments = oDataRow("Comments")
            mdEnteredDate = oDataRow("EnteredDate")
            miEnteredByID = oDataRow("EnteredBy_ID")
            mbChanged = False

        End Sub

        Public Sub Save(ByVal oUser As CUser)

            If mbChanged Then

                Dim oParameters As New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", miContactID))
                oParameters.Add(New SqlClient.SqlParameter("@DecisionDate", SQLDate(mdDecisionDate)))
                oParameters.Add(New SqlClient.SqlParameter("@DecisionType_ID", SQLNull(miDecisionTypeID)))
                oParameters.Add(New SqlClient.SqlParameter("@CampusDecision_ID", SQLNull(miCampusDecisionID)))
                oParameters.Add(New SqlClient.SqlParameter("@Comments", Comments))
                oParameters.Add(New SqlClient.SqlParameter("@EnteredDate", SQLDateTime(mdEnteredDate)))
                oParameters.Add(New SqlClient.SqlParameter("@EnteredBy_ID", SQLNull(miEnteredByID)))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))

                If Me.ID = 0 Then
                    miAdditionalDecisionID = DataAccess.GetValue("sp_Church_ContactAdditionalDecisionInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@AdditionalDecision_ID", miAdditionalDecisionID))
                    DataAccess.ExecuteCommand("sp_Church_ContactAdditionalDecisionUpdate", oParameters)
                End If

                mbChanged = False

            End If

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miAdditionalDecisionID
            End Get
        End Property
        Public Property ContactID() As Integer
            Get
                Return miContactID
            End Get
            Set(ByVal value As Integer)
                If miContactID <> value Then mbChanged = True
                miContactID = value
            End Set
        End Property
        Public ReadOnly Property Contact() As CContact
            Get
                If moContact Is Nothing And miContactID > 0 Then
                    moContact = New CContact
                    moContact.LoadByID(miContactID)
                End If
                Return moContact
            End Get
        End Property
        Public Property DecisionDate() As Date
            Get
                Return mdDecisionDate
            End Get
            Set(ByVal value As Date)
                If mdDecisionDate <> value Then mbChanged = True
                mdDecisionDate = value
            End Set
        End Property
        Public Property DecisionTypeID() As Integer
            Get
                Return miDecisionTypeID
            End Get
            Set(ByVal value As Integer)
                If miDecisionTypeID <> value Then mbChanged = True
                miDecisionTypeID = value
            End Set
        End Property
        Public ReadOnly Property DecisionType() As CNCDecisionType
            Get
                Return Lists.Internal.Church.GetNCDecisionTypeByID(miDecisionTypeID)
            End Get
        End Property
        Public Property CampusDecisionID() As Integer
            Get
                Return miCampusDecisionID
            End Get
            Set(ByVal value As Integer)
                If miCampusDecisionID <> value Then mbChanged = True
                miCampusDecisionID = value
            End Set
        End Property
        Public ReadOnly Property CampusDecision() As CCampus
            Get
                Return Lists.Internal.Church.GetCampusByID(miCampusDecisionID)
            End Get
        End Property
        Public Property Comments() As String
            Get
                Return msComments
            End Get
            Set(ByVal value As String)
                If msComments <> value Then mbChanged = True
                msComments = value
            End Set
        End Property
        Public Property EnteredDate() As Date
            Get
                Return mdEnteredDate
            End Get
            Set(ByVal value As Date)
                If mdEnteredDate <> value Then mbChanged = True
                mdEnteredDate = value
            End Set
        End Property
        Public Property EnteredByID() As Integer
            Get
                Return miEnteredByID
            End Get
            Set(ByVal value As Integer)
                If miEnteredByID <> value Then mbChanged = True
                miEnteredByID = value
            End Set
        End Property
        Public ReadOnly Property EnteredBy() As CUser
            Get
                Return Lists.GetUserByID(miEnteredByID)
            End Get
        End Property
        Public ReadOnly Property GUID() As String
            Get
                Return msGUID
            End Get
        End Property
        Public ReadOnly Property Changed() As Boolean
            Get
                Return mbChanged
            End Get
        End Property

    End Class

End Namespace