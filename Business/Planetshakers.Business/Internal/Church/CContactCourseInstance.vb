Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CContactCourseInstance
        Implements IComparable(Of CContactCourseInstance)

        Private miContactCourseInstanceID As Integer
        Private miContactID As Integer
        Private miCourseInstanceID As Integer
        Private mdDateAdded As Date
        Private msGUID As String

        Private mbChanged As Boolean

        Public Sub New()

            miContactCourseInstanceID = 0
            miContactID = 0
            miCourseInstanceID = 0
            mdDateAdded = Nothing
            msGUID = System.Guid.NewGuid.ToString

            mbChanged = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miContactCourseInstanceID = oDataRow("ContactCourseInstance_ID")
            miContactID = oDataRow("Contact_ID")
            miCourseInstanceID = oDataRow("CourseInstance_ID")
            mdDateAdded = oDataRow("DateAdded")

            mbChanged = False

        End Sub

        Public Sub Save(ByVal oUser As CUser)

            If mbChanged Then

                Dim oParameters As New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", miContactID))
                oParameters.Add(New SqlClient.SqlParameter("@CourseInstance_ID", miCourseInstanceID))
                oParameters.Add(New SqlClient.SqlParameter("@DateAdded", SQLDateTime(mdDateAdded)))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))

                If Me.ID = 0 Then
                    miContactCourseInstanceID = DataAccess.GetValue("sp_Church_ContactCourseInstanceInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@ContactCourseInstance_ID", miContactCourseInstanceID))
                    DataAccess.ExecuteCommand("sp_Church_ContactCourseInstanceUpdate", oParameters)
                End If

                mbChanged = False

            End If

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miContactCourseInstanceID
            End Get
        End Property
        Public Property ContactID() As Integer
            Get
                Return miContactID
            End Get
            Set(ByVal value As Integer)
                If miContactID <> value Then mbChanged = True
                miContactID = value
            End Set
        End Property
        Public Property CourseInstanceID() As Integer
            Get
                Return miCourseInstanceID
            End Get
            Set(ByVal value As Integer)
                If miCourseInstanceID <> value Then mbChanged = True
                miCourseInstanceID = value
            End Set
        End Property
        Public ReadOnly Property CourseInstance() As CCourseInstance
            Get
                Return Lists.Internal.Church.GetCourseInstanceByID(miCourseInstanceID)
            End Get
        End Property
        Public Property DateAdded() As Date
            Get
                Return mdDateAdded
            End Get
            Set(ByVal value As Date)
                If mdDateAdded <> value Then mbChanged = True
                mdDateAdded = value
            End Set
        End Property
        Public ReadOnly Property Changed() As Boolean
            Get
                Return mbChanged
            End Get
        End Property
        Public Sub ResetChanges()
            mbChanged = False
        End Sub
        Public ReadOnly Property GUID() As String
            Get
                Return msGUID
            End Get
        End Property

        Public Function CompareTo(ByVal other As CContactCourseInstance) As Integer Implements System.IComparable(Of CContactCourseInstance).CompareTo
            If Me.CourseInstance.CourseID = other.CourseInstance.CourseID Then
                Return Date.Compare(Me.CourseInstance.StartDate, other.CourseInstance.StartDate)
            Else
                Return String.Compare(Me.CourseInstance.Course.Name, other.CourseInstance.Course.Name)
            End If
        End Function
    End Class

End Namespace