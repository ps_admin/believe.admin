Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CCourse
        Implements IComparable(Of CCourse)

        Private miCourseID As Integer
        Private msName As String
        Private miPointsRequiredForCompletion As Integer
        Private mbShowInCustomReport As Boolean
        Private mbLogCompletedSession As Boolean
        Private miSortOrder As Integer
        Private mbDeleted As Boolean

        Private mbChanged As Boolean

        Public Sub New()

            miCourseID = 0
            msName = String.Empty
            miPointsRequiredForCompletion = 0
            mbShowInCustomReport = False
            mbLogCompletedSession = False
            miSortOrder = 0
            mbDeleted = False

            mbChanged = False

        End Sub

        Public Sub New(ByVal oDataRow As DataRow)
            Me.New()
            Load(oDataRow)
        End Sub

        Private Sub Load(ByRef oDataRow As DataRow)

            miCourseID = oDataRow("Course_ID")
            msName = oDataRow("Name")
            miPointsRequiredForCompletion = oDataRow("PointsRequiredForCompletion")
            mbShowInCustomReport = oDataRow("ShowInCustomReport")
            mbLogCompletedSession = oDataRow("LogCompletedSession")
            mbDeleted = oDataRow("Deleted")
            miSortOrder = oDataRow("SortOrder")

            mbChanged = False

        End Sub

        Public Sub Save()

            If mbChanged Then

                Dim oParameters As New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@Name", msName))
                oParameters.Add(New SqlClient.SqlParameter("@PointsRequiredForCompletion", miPointsRequiredForCompletion))
                oParameters.Add(New SqlClient.SqlParameter("@ShowInCustomReport", mbShowInCustomReport))
                oParameters.Add(New SqlClient.SqlParameter("@LogCompletedSession", mbLogCompletedSession))
                oParameters.Add(New SqlClient.SqlParameter("@Deleted", mbDeleted))
                oParameters.Add(New SqlClient.SqlParameter("@SortOrder", miSortOrder))

                If Me.ID = 0 Then
                    miCourseID = DataAccess.GetValue("sp_Church_CourseInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@Course_ID", miCourseID))
                    DataAccess.ExecuteCommand("sp_Church_CourseUpdate", oParameters)
                End If

                mbChanged = False

                'Update course in the Global List
                Lists.Internal.Church.Course(Me.ID) = Me

            End If

        End Sub

        Public ReadOnly Property CourseInstance() As CArrayList(Of CCourseInstance)
            Get
                Dim oArrayList As New CArrayList(Of CCourseInstance)

                For Each oCourseInstance As CCourseInstance In Lists.Internal.Church.CourseInstance.Values
                    If oCourseInstance.CourseID = miCourseID Then
                        oArrayList.Add(oCourseInstance)
                    End If
                Next
                oArrayList.Sort()
                Return oArrayList
            End Get
        End Property
        Public ReadOnly Property CourseTopic() As CArrayList(Of CCourseTopic)
            Get
                Dim oArrayList As New CArrayList(Of CCourseTopic)

                For Each oCourseTopic As CCourseTopic In Lists.Internal.Church.CourseTopic.Values
                    If oCourseTopic.CourseID = miCourseID Then
                        oArrayList.Add(oCourseTopic)
                    End If
                Next
                oArrayList.Sort()
                Return oArrayList
            End Get
        End Property

        Public ReadOnly Property CourseInstanceCurrent() As CArrayList(Of CCourseInstance)
            Get
                Dim oArraylist As New CArrayList(Of CCourseInstance)
                Dim oCourseInstance As CCourseInstance
                For Each oCourseInstance In CourseInstance
                    If Not oCourseInstance.Deleted Then oArraylist.Add(oCourseInstance)
                Next
                oArraylist.Sort()
                Return oArraylist
            End Get
        End Property
        Public ReadOnly Property CourseTopicCurrent() As CArrayList(Of CCourseTopic)
            Get
                Dim oArraylist As New CArrayList(Of CCourseTopic)
                For Each oCourseTopic As CCourseTopic In CourseTopic
                    If Not oCourseTopic.Deleted Then oArraylist.Add(oCourseTopic)
                Next
                oArraylist.Sort()
                Return oArraylist
            End Get
        End Property

        Public ReadOnly Property ID() As Integer
            Get
                Return miCourseID
            End Get
        End Property
        Public Property Name() As String
            Get
                Return msName
            End Get
            Set(ByVal value As String)
                If msName <> value Then mbChanged = True
                msName = value
            End Set
        End Property
        Public Property PointsRequiredForCompletion() As Integer
            Get
                Return miPointsRequiredForCompletion
            End Get
            Set(ByVal value As Integer)
                If miPointsRequiredForCompletion <> value Then mbChanged = True
                miPointsRequiredForCompletion = value
            End Set
        End Property
        Public Property ShowInCustomReport() As Boolean
            Get
                Return mbShowInCustomReport
            End Get
            Set(ByVal value As Boolean)
                If mbShowInCustomReport <> value Then mbChanged = True
                mbShowInCustomReport = value
            End Set
        End Property
        Public Property LogCompletedSession() As Boolean
            Get
                Return mbLogCompletedSession
            End Get
            Set(ByVal value As Boolean)
                If mbLogCompletedSession <> value Then mbChanged = True
                mbLogCompletedSession = value
            End Set
        End Property
        Public Property Deleted() As Boolean
            Get
                Return mbDeleted
            End Get
            Set(ByVal value As Boolean)
                If mbDeleted <> value Then mbChanged = True
                mbDeleted = value
            End Set
        End Property
        Public Property SortOrder() As Integer
            Get
                Return miSortOrder
            End Get
            Set(ByVal value As Integer)
                If miSortOrder <> value Then mbChanged = True
                miSortOrder = value
            End Set
        End Property
        Public ReadOnly Property LastestCourseDate() As String
            Get
                If CourseInstance.Count = 0 Then Return String.Empty
                Dim dDate As Date = Nothing
                Dim sName As String = String.Empty
                For Each oCourseInstance As CCourseInstance In CourseInstanceCurrent
                    If oCourseInstance.StartDate > dDate Then
                        sName = If(Not IsDate(oCourseInstance.Name), oCourseInstance.Name & " (Starting " & Format(oCourseInstance.StartDate, "dd MMM yyyy") & ")", oCourseInstance.Name)
                        dDate = oCourseInstance.StartDate
                    End If
                Next
                Return sName
            End Get
        End Property

        Public Function CompareTo(ByVal other As CCourse) As Integer Implements System.IComparable(Of CCourse).CompareTo
            Return String.Compare(Me.Name, other.Name)
        End Function

    End Class

End Namespace