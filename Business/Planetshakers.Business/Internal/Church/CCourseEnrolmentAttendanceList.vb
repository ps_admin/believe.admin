﻿Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CCourseEnrolmentAttendanceList

        Private miCourseInstanceID As Integer
        Private mcCourseEnrolmentList As CArrayList(Of CCourseEnrolmentAttendance)

        Public Sub New(Optional ByVal iCourseInstanceID As Integer = 0)

            mcCourseEnrolmentList = New CArrayList(Of CCourseEnrolmentAttendance)

            If iCourseInstanceID > 0 Then
                miCourseInstanceID = iCourseInstanceID
                mcCourseEnrolmentList = Load()
            End If

        End Sub

        Private Function Load() As CArrayList(Of CCourseEnrolmentAttendance)

            Dim oDataSet As DataSet
            Dim oParameters As New ArrayList
            Dim cCourseEnrolmentAttendance As New CArrayList(Of CCourseEnrolmentAttendance)

            oParameters.Add(New SqlClient.SqlParameter("@CourseInstance_ID", miCourseInstanceID))

            Dim oTableMappings As New ArrayList
            oTableMappings.Add(New System.Data.Common.DataTableMapping("Table", "CourseEnrolmentAttendance"))
            oTableMappings.Add(New System.Data.Common.DataTableMapping("Table1", "CourseEnrolmentAttendanceTopicsCovered"))

            oDataSet = DataAccess.GetDataSet("sp_Church_CourseEnrolmentGetAttendanceList", oTableMappings, oParameters)

            oDataSet.Relations.Add("CourseEnrolmentAttendanceTopicsCovered", oDataSet.Tables("CourseEnrolmentAttendance").Columns("CourseEnrolmentAttendance_ID"), _
                                    oDataSet.Tables("CourseEnrolmentAttendanceTopicsCovered").Columns("CourseEnrolmentAttendance_ID"))

            For Each oDataRow As DataRow In oDataSet.Tables(0).Rows
                cCourseEnrolmentAttendance.Add(New CCourseEnrolmentAttendance(oDataRow))
            Next

            cCourseEnrolmentAttendance.ResetChanges()

            Return cCourseEnrolmentAttendance

        End Function

        Public Sub Reload()

            If miCourseInstanceID > 0 Then
                mcCourseEnrolmentList = Load()
            End If

        End Sub

        Public Sub Save(ByVal oUser As CUser)

            Dim oParameters As ArrayList

            'Remove the Deleted Attendance Records
            For Each oCourseEnrolmentAttendance As CCourseEnrolmentAttendance In mcCourseEnrolmentList.Removed
                oParameters = New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@CourseEnrolmentAttendance_ID", oCourseEnrolmentAttendance.ID))
                DataAccess.ExecuteCommand("sp_Church_CourseEnrolmentAttendanceDelete", oParameters)
            Next

            For Each oCourseEnrolmentAttendance As CCourseEnrolmentAttendance In mcCourseEnrolmentList.Clone
                oCourseEnrolmentAttendance.Save(oUser)
                'If the ID returns as zero, that means the entry was already in the DB. Remove from list.
                If oCourseEnrolmentAttendance.ID = 0 Then
                    mcCourseEnrolmentList.Remove(oCourseEnrolmentAttendance)
                End If
            Next

            mcCourseEnrolmentList.ResetChanges()

            oParameters = New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))
            DataAccess.ExecuteCommand("sp_Church_CourseEnrolmentMarkCompletedCheck", oParameters)

        End Sub

        Public Sub MarkOffAttendance(ByVal iContactID As Integer, ByVal iCourseSessionID As Integer, ByVal iCourseTopicID As Integer, ByVal bAttended As Boolean, Optional ByVal iMarkedOffByID As Integer = 0, Optional ByVal dMarkedOffDateTime As Date = Nothing)

            If bAttended Then

                Dim bFound As Boolean = False
                Dim oCourseEnrolmentAttendance As CCourseEnrolmentAttendance = Nothing
                Dim oCourseSession As CCourseSession = Lists.Internal.Church.GetCourseSessionByID(iCourseSessionID)

                'Add the CourseEnrolmentAttendance record for this contact for this session
                For Each oCourseEnrolmentAttendance In mcCourseEnrolmentList
                    If oCourseEnrolmentAttendance.ContactID = iContactID And _
                        oCourseEnrolmentAttendance.CourseSessionID = iCourseSessionID Then
                        bFound = True
                        Exit For
                    End If
                Next
                If Not bFound Then
                    oCourseEnrolmentAttendance = New CCourseEnrolmentAttendance
                    oCourseEnrolmentAttendance.ContactID = iContactID
                    oCourseEnrolmentAttendance.CourseSessionID = iCourseSessionID
                    oCourseEnrolmentAttendance.MarkedOffByID = iMarkedOffByID
                    oCourseEnrolmentAttendance.MarkedOffDateTime = dMarkedOffDateTime
                    mcCourseEnrolmentList.Add(oCourseEnrolmentAttendance)
                End If

                'Add to the list of topics covered for this enrolment attendance.
                bFound = False
                For Each oTopicCovered As CCourseEnrolmentAttendanceTopicsCovered In oCourseEnrolmentAttendance.TopicsCovered
                    If oTopicCovered.CourseTopicID = iCourseTopicID Then
                        bFound = True
                        Exit For
                    End If
                Next
                If Not bFound Then
                    Dim oTopicCovered As New CCourseEnrolmentAttendanceTopicsCovered
                    oTopicCovered.CourseTopicID = iCourseTopicID
                    oCourseEnrolmentAttendance.TopicsCovered.Add(oTopicCovered)
                End If

            Else
                For Each oCourseEnrolmentAttendance As CCourseEnrolmentAttendance In mcCourseEnrolmentList.Clone
                    If oCourseEnrolmentAttendance.ContactID = iContactID And _
                        oCourseEnrolmentAttendance.CourseSessionID = iCourseSessionID Then

                        For Each oTopicCovered As CCourseEnrolmentAttendanceTopicsCovered In oCourseEnrolmentAttendance.TopicsCovered.Clone
                            If oTopicCovered.CourseTopicID = iCourseTopicID Then
                                oCourseEnrolmentAttendance.TopicsCovered.Remove(oTopicCovered)
                            End If
                        Next

                        If oCourseEnrolmentAttendance.TopicsCovered.Count = 0 Then
                            mcCourseEnrolmentList.Remove(oCourseEnrolmentAttendance)
                        End If
                    End If
                Next
            End If

        End Sub

        Public Function CheckAttendance(ByVal iContactID As Integer, ByVal iCourseSessionID As Integer, Optional ByVal iCourseTopicID As Integer = 0) As Boolean

            For Each oCourseEnrolmentAttendance As CCourseEnrolmentAttendance In mcCourseEnrolmentList
                If oCourseEnrolmentAttendance.ContactID = iContactID Then
                    If oCourseEnrolmentAttendance.CourseSessionID = iCourseSessionID Then
                        If iCourseTopicID = 0 Then
                            Return True
                        Else
                            For Each oTopicCovered As CCourseEnrolmentAttendanceTopicsCovered In oCourseEnrolmentAttendance.TopicsCovered
                                If oTopicCovered.CourseTopicID = iCourseTopicID Then
                                    Return True
                                End If
                            Next
                        End If

                    End If
                End If
            Next

            Return False

        End Function

        Public Function CheckForChanges() As Boolean

            Dim cCourseEnrolmentList As CArrayList(Of CCourseEnrolmentAttendance) = Load()

            If cCourseEnrolmentList.Count <> mcCourseEnrolmentList.Count Then Return True

            For Each oCourseEnrolmentAttendance As CCourseEnrolmentAttendance In cCourseEnrolmentList
                If oCourseEnrolmentAttendance.CheckChanges(mcCourseEnrolmentList.GetItemByID(oCourseEnrolmentAttendance.ID)) Then Return True
            Next

        End Function

        Public ReadOnly Property List() As CArrayList(Of CCourseEnrolmentAttendance)
            Get
                Return mcCourseEnrolmentList
            End Get
        End Property

    End Class

End Namespace