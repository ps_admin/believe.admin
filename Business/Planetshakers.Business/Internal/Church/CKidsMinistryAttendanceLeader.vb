Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CKidsMinistryAttendanceLeader

        Private miKidsMinistryAttendanceLeaderID As Integer
        Private miContactID As Integer
        Private mdServiceDate As Date
        Private miChurchServiceID As Integer
        Private mdCheckInTime As Date
        Private mdCheckOutTime As Date
        Private mbChanged As Boolean
        Private msGUID As String

        Public Sub New()

            miKidsMinistryAttendanceLeaderID = 0
            miContactID = 0
            mdServiceDate = Nothing
            miChurchServiceID = 0
            mdCheckInTime = Nothing
            mdCheckOutTime = Nothing
            mbChanged = False
            msGUID = System.Guid.NewGuid.ToString

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()
            LoadDataRow(oDataRow)

        End Sub

        Public Sub LoadDataRow(ByRef oDataRow As DataRow)

            miKidsMinistryAttendanceLeaderID = oDataRow("KidsMinistryAttendanceLeader_ID")
            miContactID = oDataRow("Contact_ID")
            mdServiceDate = oDataRow("Date")
            miChurchServiceID = oDataRow("ChurchService_ID")
            mdCheckInTime = oDataRow("CheckInTime")
            mdCheckOutTime = IsNull(oDataRow("CheckOutTime"), Nothing)

        End Sub

        Public Function LoadByID(ByVal iKidsMinistryAttendanceLeaderID As Integer) As Boolean

            Dim oDataTable As DataTable
            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("KidsMinistryAttendanceLeader_ID", iKidsMinistryAttendanceLeaderID))

            oDataTable = DataAccess.GetDataTable("sp_Church_KidsMinistryAttendanceLeaderRead", oParameters)

            If oDataTable.Rows.Count > 0 Then
                LoadDataRow(oDataTable.Rows(0))
                Return True
            Else
                Return False
            End If

        End Function

        Public Sub Save()

            If mbChanged Then

                Dim oParameters As New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", miContactID))
                oParameters.Add(New SqlClient.SqlParameter("@Date", SQLDate(mdServiceDate)))
                oParameters.Add(New SqlClient.SqlParameter("@ChurchService_ID", miChurchServiceID))
                oParameters.Add(New SqlClient.SqlParameter("@CheckInTime", SQLDateTime(mdCheckInTime)))
                oParameters.Add(New SqlClient.SqlParameter("@CheckOutTime", SQLDateTime(mdCheckOutTime)))

                If Me.ID = 0 Then
                    miKidsMinistryAttendanceLeaderID = DataAccess.GetValue("sp_Church_KidsMinistryAttendanceLeaderInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@KidsMinistryAttendanceLeader_ID", miKidsMinistryAttendanceLeaderID))
                    DataAccess.ExecuteCommand("sp_Church_KidsMinistryAttendanceLeaderUpdate", oParameters)
                End If

                mbChanged = False

            End If

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miKidsMinistryAttendanceLeaderID
            End Get
        End Property
        Public Property ContactID() As Integer
            Get
                Return miContactID
            End Get
            Set(ByVal value As Integer)
                If value <> miContactID Then mbChanged = True
                miContactID = value
            End Set
        End Property
        Public Property ServiceDate() As Date
            Get
                Return mdServiceDate
            End Get
            Set(ByVal value As Date)
                If value <> mdServiceDate Then mbChanged = True
                mdServiceDate = value
            End Set
        End Property
        Property ChurchServiceID() As Integer
            Get
                Return miChurchServiceID
            End Get
            Set(ByVal Value As Integer)
                If Value <> miChurchServiceID Then mbChanged = True
                miChurchServiceID = Value
            End Set
        End Property
        Public Property CheckInTime() As Date
            Get
                Return mdCheckInTime
            End Get
            Set(ByVal value As Date)
                If value <> mdCheckInTime Then mbChanged = True
                mdCheckInTime = value
            End Set
        End Property
        Public Property CheckOutTime() As Date
            Get
                Return mdCheckOutTime
            End Get
            Set(ByVal value As Date)
                If value <> mdCheckOutTime Then mbChanged = True
                mdCheckOutTime = value
            End Set
        End Property
        Public ReadOnly Property Changed() As Boolean
            Get
                Return mbChanged
            End Get
        End Property
        Public ReadOnly Property GUID() As String
            Get
                Return msGUID
            End Get
        End Property

    End Class

End Namespace