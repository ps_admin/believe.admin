Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CKidsMinistryClearance

        Private miKidsMinistryClearanceID As Integer
        Private miContactID As Integer
        Private miClearanceContactID As Integer
        Private moClearanceContact As CContact
        Private mdDateAdded As Date
        Private mbChanged As Boolean
        Private msGUID As String

        Public Sub New()

            miKidsMinistryClearanceID = 0
            miContactID = 0
            miClearanceContactID = 0
            moClearanceContact = Nothing
            mdDateAdded = Nothing
            mbChanged = False
            msGUID = System.Guid.NewGuid.ToString

        End Sub

        Public Sub New(ByVal iClearanceContactID As Integer)

            Me.New()

            miClearanceContactID = iClearanceContactID
            mdDateAdded = GetDateTime

        End Sub


        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miKidsMinistryClearanceID = oDataRow("KidsMinistryClearance_ID")
            miContactID = oDataRow("Contact_ID")
            miClearanceContactID = IsNull(oDataRow("ClearanceContact_ID"), Nothing)
            mdDateAdded = oDataRow("DateAdded")

        End Sub

        Public Sub Save(ByVal oUser As CUser)

            If mbChanged Then

                Dim oParameters As New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", miContactID))
                oParameters.Add(New SqlClient.SqlParameter("@ClearanceContact_ID", miClearanceContactID))
                oParameters.Add(New SqlClient.SqlParameter("@DateAdded", SQLDateTime(mdDateAdded)))
                oParameters.Add(New SqlClient.SqlParameter("@User_ID", oUser.ID))

                If Me.ID = 0 Then
                    miKidsMinistryClearanceID = DataAccess.GetValue("sp_Church_KidsMinistryClearanceInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@KidsMinistryClearance_ID", miKidsMinistryClearanceID))
                    DataAccess.ExecuteCommand("sp_Church_KidsMinistryClearanceUpdate", oParameters)
                End If

                mbChanged = False

            End If

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miKidsMinistryClearanceID
            End Get
        End Property
        Public Property ContactID() As Integer
            Get
                Return miContactID
            End Get
            Set(ByVal value As Integer)
                If value <> miContactID Then mbChanged = True
                miContactID = value
            End Set
        End Property
        Property ClearanceContactID() As Integer
            Get
                Return miClearanceContactID
            End Get
            Set(ByVal Value As Integer)
                If Value <> miClearanceContactID Then mbChanged = True
                miClearanceContactID = Value
            End Set
        End Property
        Public ReadOnly Property GetClearanceContact() As CContact
            Get
                If moClearanceContact Is Nothing Then
                    moClearanceContact = New CContact
                    moClearanceContact.LoadByID(miClearanceContactID)
                End If
                Return moClearanceContact
            End Get
        End Property
        Public Property DateAdded() As Date
            Get
                Return mdDateAdded
            End Get
            Set(ByVal value As Date)
                If value <> mdDateAdded Then mbChanged = True
                mdDateAdded = value
            End Set
        End Property
        Public ReadOnly Property Changed() As Boolean
            Get
                Return mbChanged
            End Get
        End Property
        Public Sub ResetChanges()
            mbChanged = False
        End Sub
        Public ReadOnly Property GUID() As String
            Get
                Return msGUID
            End Get
        End Property


    End Class

End Namespace