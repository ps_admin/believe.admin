Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CNCSummaryStats

        Private miNCSummaryStatsID As Integer
        Private mdDate As Date
        Private miCampusID As Integer
        Private miNCSummaryStatsMinistryID As Integer
        Private miNCDecisionTypeID As Integer
        Private miNumber As Integer
        Private miEnteredByID As Integer
        Private mdDateEntered As Date

        Private mbChanged As Boolean

        Public Sub New()

            miNCSummaryStatsID = 0
            mdDate = Nothing
            miCampusID = 0
            miNCSummaryStatsMinistryID = 0
            miNCDecisionTypeID = 0
            miNumber = 0
            miEnteredByID = 0
            mdDateEntered = Nothing

            mbChanged = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miNCSummaryStatsID = oDataRow("SummaryStats_ID")
            mdDate = oDataRow("Date")
            miCampusID = oDataRow("Campus_ID")
            miNCSummaryStatsMinistryID = oDataRow("NCSummaryStatsMinistry_ID")
            miNCDecisionTypeID = oDataRow("NCDecisionType_ID")
            miNumber = oDataRow("Number")
            miEnteredByID = oDataRow("EnteredBy_ID")
            mdDateEntered = oDataRow("DateEntered")

            mbChanged = False

        End Sub

        Public Sub Save()

            If mbChanged Then

                Dim oParameters As New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@Date", SQLDate(mdDate)))
                oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", miCampusID))
                oParameters.Add(New SqlClient.SqlParameter("@NCSummaryStatsMinistry_ID", miNCSummaryStatsMinistryID))
                oParameters.Add(New SqlClient.SqlParameter("@NCDecisionType_ID", miNCDecisionTypeID))
                oParameters.Add(New SqlClient.SqlParameter("@Number", miNumber))
                oParameters.Add(New SqlClient.SqlParameter("@EnteredBy_ID", SQLObject(EnteredBy)))
                oParameters.Add(New SqlClient.SqlParameter("@DateEntered", SQLDateTime(mdDateEntered)))

                If Me.ID = 0 Then
                    miNCSummaryStatsID = DataAccess.GetValue("sp_Church_NCSummaryStatsInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@SummaryStats_ID", miNCSummaryStatsID))
                    DataAccess.ExecuteCommand("sp_Church_NCSummaryStatsIssuedUpdate", oParameters)
                End If

            End If

        End Sub

        Public Sub Delete()

            If miNCSummaryStatsID > 0 Then
                Dim oParameters As New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@SummaryStats_ID", miNCSummaryStatsID))
                DataAccess.ExecuteCommand("sp_Church_NCSummaryStatsDelete", oParameters)
            End If

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miNCSummaryStatsID
            End Get
        End Property
        Public Property [Date]() As Date
            Get
                Return mdDate
            End Get
            Set(ByVal value As Date)
                If mdDate <> value Then mbChanged = True
                mdDate = value
            End Set
        End Property
        Public Property CampusID() As Integer
            Get
                Return miCampusID
            End Get
            Set(ByVal value As Integer)
                If miCampusID <> value Then mbChanged = True
                miCampusID = value
            End Set
        End Property
        Public ReadOnly Property Campus() As CCampus
            Get
                Return Lists.Internal.Church.GetCampusByID(miCampusID)
            End Get
        End Property
        Public Property NCSummaryStatsMinistryID() As Integer
            Get
                Return miNCSummaryStatsMinistryID
            End Get
            Set(ByVal value As Integer)
                If miNCSummaryStatsMinistryID <> value Then mbChanged = True
                miNCSummaryStatsMinistryID = value
            End Set
        End Property
        Public ReadOnly Property NCSummaryStatsMinistry() As cNCSummaryStatsMinistry
            Get
                Return Lists.Internal.Church.GetNCSummaryStatsMinistryByID(miNCSummaryStatsMinistryID)
            End Get
        End Property
        Public Property NCDecisionTypeID() As Integer
            Get
                Return miNCDecisionTypeID
            End Get
            Set(ByVal value As Integer)
                If miNCDecisionTypeID <> value Then mbChanged = True
                miNCDecisionTypeID = value
            End Set
        End Property
        Public ReadOnly Property NCDecisionType() As CNCDecisionType
            Get
                Return Lists.Internal.Church.GetNCDecisionTypeByID(miNCDecisionTypeID)
            End Get
        End Property
        Public Property Number() As Integer
            Get
                Return miNumber
            End Get
            Set(ByVal value As Integer)
                If miNumber <> value Then mbChanged = True
                miNumber = value
            End Set
        End Property
        Public Property EnteredByID() As Integer
            Get
                Return miEnteredByID
            End Get
            Set(ByVal value As Integer)
                If miEnteredByID <> value Then mbChanged = True
                miEnteredByID = value
            End Set
        End Property
        Public ReadOnly Property EnteredBy() As CUser
            Get
                Return Lists.GetUserByID(miEnteredByID)
            End Get
        End Property
        Public Property DateEntered() As Date
            Get
                Return mdDateEntered
            End Get
            Set(ByVal value As Date)
                If mdDateEntered <> value Then mbChanged = True
                mdDateEntered = value
            End Set
        End Property

    End Class

End Namespace