Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CNPNCInitialStatus

        Private miNPNCInitialStatus As Integer
        Private miNPNCTypeID As Integer
        Private msName As String
        Private mbShowInEntryForm As Boolean
        Private mbShowInSummaryStatsEntry As Boolean
        Private miSortOrder As Integer

        Public Sub New()

            miNPNCInitialStatus = 0
            miNPNCTypeID = 0
            msName = String.Empty
            mbShowInEntryForm = False
            mbShowInSummaryStatsEntry = False
            miSortOrder = 0

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miNPNCInitialStatus = oDataRow("InitialStatus_ID")
            miNPNCTypeID = oDataRow("NPNCType_ID")
            msName = oDataRow("Name")
            mbShowInEntryForm = oDataRow("ShowInEntryForm")
            mbShowInSummaryStatsEntry = oDataRow("ShowInSummaryStatsEntry")
            miSortOrder = oDataRow("SortOrder")

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miNPNCInitialStatus
            End Get
        End Property
        Public Property NPNCTypeID() As Integer
            Get
                Return miNPNCTypeID
            End Get
            Set(ByVal value As Integer)
                miNPNCTypeID = value
            End Set
        End Property
        Public ReadOnly Property NPNCType() As CNPNCType
            Get
                Return Lists.Internal.Church.GetNPNCTypeByID(miNPNCTypeID)
            End Get
        End Property
        Public Property Name() As String
            Get
                Return msName
            End Get
            Set(ByVal value As String)
                msName = value
            End Set
        End Property
        Public Property ShowInEntryForm() As Boolean
            Get
                Return mbShowInEntryForm
            End Get
            Set(ByVal value As Boolean)
                mbShowInEntryForm = value
            End Set
        End Property
        Public Property ShowInSummaryStatsEntry() As Boolean
            Get
                Return mbShowInSummaryStatsEntry
            End Get
            Set(ByVal value As Boolean)
                mbShowInSummaryStatsEntry = value
            End Set
        End Property
        Public Property SortOrder() As Integer
            Get
                Return miSortOrder
            End Get
            Set(ByVal value As Integer)
                miSortOrder = value
            End Set
        End Property

    End Class

End Namespace