﻿Namespace Internal.Church


<Serializable()> _
<CLSCompliant(True)> _
    Public Class CNationality
        Private miNationalityID As Integer
        Private msNationality As String

        Public Sub New()
            miNationalityID = 0
            msNationality = ""
        End Sub

        Public Sub New(ByRef oDataRow As DataRow)
            Me.New()

            miNationalityID = oDataRow("Nationality_ID")
            msNationality = oDataRow("Nationality")

        End Sub

        Public ReadOnly Property ID() As Integer
            Get
                Return miNationalityID
            End Get
        End Property

        Public Property Name() As String
            Get
                Return msNationality
            End Get
            Set(ByVal value As String)
                msNationality = value
            End Set
        End Property
    End Class
End Namespace
