﻿Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CPCRVisitor
        Private miID As Integer
        Private msFirstName As String
        Private msLastName As String
        Private msPhoneMobile As String
        Private msEmail As String
        Private miPCRID As Integer

        Private mbChanged As Boolean

        Public Sub New()
            miID = 0
            msFirstName = ""
            msLastName = ""
            msPhoneMobile = ""
            msEmail = ""
            miPCRID = 0
        End Sub

        Public Sub Load(ByRef oDataRow As DataRow)
            miID = oDataRow("Visitor_ID")
            msFirstName = oDataRow("FirstName")
            msLastName = oDataRow("LastName")
            msPhoneMobile = oDataRow("PhoneMobile")
            msEmail = oDataRow("Email")
            miPCRID = oDataRow("PCR_ID")
        End Sub

        Public Sub LoadByID(ByVal ID As Integer)
            Dim oArrayList As New ArrayList
            oArrayList.Add(New SqlClient.SqlParameter("@Visitor_ID", ID))

            Dim oDataTable As DataTable = DataAccess.GetDataTable("sp_Church_PCRVisitorRead", oArrayList)
            If oDataTable.Rows.Count > 0 Then Load(oDataTable.Rows(0))
        End Sub

        Public Sub Save(ByVal oUser As CUser)
            If mbChanged Then
                Dim oArrayList As New ArrayList

                oArrayList.Add(New SqlClient.SqlParameter("@FirstName", msFirstName))
                oArrayList.Add(New SqlClient.SqlParameter("@LastName", msLastName))
                oArrayList.Add(New SqlClient.SqlParameter("@PhoneMobile", msPhoneMobile))
                oArrayList.Add(New SqlClient.SqlParameter("@Email", msEmail))

                If miID > 0 Then
                    oArrayList.Add(New SqlClient.SqlParameter("@Visitor_ID", miID))
                    DataAccess.ExecuteCommand("sp_Church_PCRVisitorUpdate", oArrayList)
                Else
                    oArrayList.Add(New SqlClient.SqlParameter("@PCR_ID", miPCRID))
                    oArrayList.Add(New SqlClient.SqlParameter("@CreatedBy_ID", oUser.ID))
                    DataAccess.ExecuteCommand("sp_Church_PCRVisitorInsert", oArrayList)
                End If

                mbChanged = False
            End If
        End Sub

        Public ReadOnly Property ID As Integer
            Get
                Return miID
            End Get
        End Property

        Public Property FirstName As String
            Get
                Return msFirstName
            End Get
            Set(value As String)
                If msFirstName <> value Then mbChanged = True
                msFirstName = value
            End Set
        End Property

        Public Property LastName As String
            Get
                Return msLastName
            End Get
            Set(value As String)
                If msLastName <> value Then mbChanged = True
                msLastName = value
            End Set
        End Property

        Public Property PhoneMobile As String
            Get
                Return msPhoneMobile
            End Get
            Set(value As String)
                If msPhoneMobile <> value Then mbChanged = True
                msPhoneMobile = value
            End Set
        End Property

        Public Property Email As String
            Get
                Return msEmail
            End Get
            Set(value As String)
                If msEmail <> value Then mbChanged = True
                msEmail = value
            End Set
        End Property

        Public Property PCRID As Integer
            Get
                Return miPCRID
            End Get
            Set(value As Integer)
                If miPCRID <> value Then mbChanged = True
                miPCRID = value
            End Set
        End Property
    End Class
End Namespace
