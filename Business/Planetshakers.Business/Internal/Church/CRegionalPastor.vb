﻿Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CRegionalPastor

        Private miRegionalPastorID As Integer
        Private miCampusID As Integer
        Private miMinistryID As Integer
        Private miRegionID As Integer
        Private miContactID As Integer

        Private moContact As CContact
        Private mbChanged As Boolean

        Public Sub New()

            miRegionalPastorID = 0
            miCampusID = 0
            miMinistryID = 0
            miRegionID = 0
            miContactID = 0
            moContact = Nothing

            mbChanged = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miRegionalPastorID = oDataRow("RegionalPastor_ID")
            miCampusID = oDataRow("Campus_ID")
            miMinistryID = oDataRow("Ministry_ID")
            miRegionID = IsNull(oDataRow("Region_ID"), 0)
            miContactID = oDataRow("Contact_ID")

            mbChanged = False

        End Sub

        Public Sub Save()
            If mbChanged Then

                Dim oParameters As New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", miCampusID))
                oParameters.Add(New SqlClient.SqlParameter("@Ministry_ID", miMinistryID))
                oParameters.Add(New SqlClient.SqlParameter("@Region_ID", SQLNull(miRegionID)))
                oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", miContactID))

                If Me.ID = 0 Then
                    miRegionalPastorID = DataAccess.GetValue("sp_Church_RegionalPastorInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@RegionalPastor_ID", miRegionalPastorID))
                    DataAccess.ExecuteCommand("sp_Church_RegionalPastorUpdate", oParameters)
                End If

                mbChanged = False

            End If

        End Sub

        Public Sub Delete()

            If miRegionalPastorID > 0 Then
                Dim oParameters As New ArrayList
                oParameters.Add(New SqlClient.SqlParameter("@RegionalPastor_ID", miRegionalPastorID))
                DataAccess.ExecuteCommand("sp_Church_RegionalPastorDelete", oParameters)
            End If

        End Sub

        Public Property ID() As Integer
            Get
                Return miRegionalPastorID
            End Get
            Set(ByVal value As Integer)
                If miRegionalPastorID <> value Then mbChanged = True
                miRegionalPastorID = value
            End Set
        End Property
        Public Property CampusID() As Integer
            Get
                Return miCampusID
            End Get
            Set(ByVal value As Integer)
                If miCampusID <> value Then mbChanged = True
                miCampusID = value
            End Set
        End Property
        Public Property MinistryID() As Integer
            Get
                Return miMinistryID
            End Get
            Set(ByVal value As Integer)
                If miMinistryID <> value Then mbChanged = True
                miMinistryID = value
            End Set
        End Property
        Public Property RegionID() As Integer
            Get
                Return miRegionID
            End Get
            Set(ByVal value As Integer)
                If miRegionID <> value Then mbChanged = True
                miRegionID = value
            End Set
        End Property
        Public Property ContactID() As Integer
            Get
                Return miContactID
            End Get
            Set(ByVal value As Integer)
                If miContactID <> value Then mbChanged = True
                If moContact IsNot Nothing AndAlso miContactID <> moContact.ID Then moContact = Nothing
                miContactID = value
            End Set
        End Property
        Public ReadOnly Property Contact() As CContact
            Get
                If moContact Is Nothing Then
                    moContact = New CContact(miContactID)
                End If
                Return moContact
            End Get
        End Property

    End Class

End Namespace
