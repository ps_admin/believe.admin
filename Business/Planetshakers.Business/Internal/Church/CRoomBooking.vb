Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CRoomBooking

        Private miRoomBookingID As Integer
        Private miRoomID As Integer
        Private msBookingReason As String
        Private miBookedByID As Integer
        Private mdStartDate As DateTime
        Private mdEndDate As DateTime
        Private mbAllDayBooking As Boolean
        Private msRecurrenceInfo As String
        Private miBookingType As Integer
        Private mbDeleted As Boolean

        Private mbChanged As Boolean

        Public Sub New()

            miRoomBookingID = 0
            miRoomID = 0
            msBookingReason = String.Empty
            miBookedByID = 0
            mdStartDate = Nothing
            mdEndDate = Nothing
            mbAllDayBooking = False
            msRecurrenceInfo = String.Empty
            mbDeleted = False

            mbChanged = False

        End Sub

        Public Sub New(ByRef oDataRow As DataRow)

            Me.New()

            miRoomBookingID = oDataRow("RoomBooking_ID")
            miRoomID = oDataRow("Room_ID")
            msBookingReason = oDataRow("BookingReason")
            miBookedByID = oDataRow("BookedBy_ID")
            mdStartDate = oDataRow("StartDate")
            mdEndDate = oDataRow("EndDate")
            mbAllDayBooking = oDataRow("AllDayBooking")
            msRecurrenceInfo = oDataRow("RecurrenceInfo")
            miBookingType = oDataRow("BookingType")
            mbDeleted = oDataRow("Deleted")

            mbChanged = False

        End Sub

        Public Sub Save(ByVal oUser As CUser)

            If mbChanged Then

                Dim oParameters As New ArrayList

                oParameters.Add(New SqlClient.SqlParameter("@Room_ID", miRoomID))
                oParameters.Add(New SqlClient.SqlParameter("@BookingReason", msBookingReason))
                oParameters.Add(New SqlClient.SqlParameter("@BookedBy_ID", miBookedByID))
                oParameters.Add(New SqlClient.SqlParameter("@StartDate", mdStartDate))
                oParameters.Add(New SqlClient.SqlParameter("@EndDate", mdEndDate))
                oParameters.Add(New SqlClient.SqlParameter("@AllDayBooking", mbAllDayBooking))
                oParameters.Add(New SqlClient.SqlParameter("@RecurrenceInfo", IIf(IsNothing(msRecurrenceInfo), String.Empty, msRecurrenceInfo)))
                oParameters.Add(New SqlClient.SqlParameter("@BookingType", miBookingType))
                oParameters.Add(New SqlClient.SqlParameter("@Deleted", mbDeleted))

                If miRoomBookingID = 0 Then
                    miRoomBookingID = DataAccess.GetValue("sp_Church_RoomBookingInsert", oParameters)
                Else
                    oParameters.Add(New SqlClient.SqlParameter("@RoomBooking_ID", miRoomBookingID))
                    DataAccess.ExecuteCommand("sp_Church_RoomBookingUpdate", oParameters)
                End If

                mbChanged = False

            End If

        End Sub

        Public Property ID() As Integer
            Get
                Return miRoomBookingID
            End Get
            Set(ByVal value As Integer)
                If miRoomBookingID <> value Then mbChanged = True
                miRoomBookingID = value
            End Set
        End Property
        Public Property RoomID() As Integer
            Get
                Return miRoomID
            End Get
            Set(ByVal value As Integer)
                If miRoomID <> value Then mbChanged = True
                miRoomID = value
            End Set
        End Property
        Public Property BookingReason() As String
            Get
                Return msBookingReason
            End Get
            Set(ByVal value As String)
                If msBookingReason <> value Then mbChanged = True
                msBookingReason = value
            End Set
        End Property
        Public Property BookedByID() As Integer
            Get
                Return miBookedByID
            End Get
            Set(ByVal value As Integer)
                If miBookedByID <> value Then mbChanged = True
                miBookedByID = value
            End Set
        End Property
        Public ReadOnly Property BookedBy() As CUser
            Get
                Return Lists.GetUserByID(miBookedByID)
            End Get
        End Property
        Public Property StartDate() As DateTime
            Get
                Return mdStartDate
            End Get
            Set(ByVal value As DateTime)
                If mdStartDate <> value Then mbChanged = True
                mdStartDate = value
            End Set
        End Property
        Public Property EndDate() As DateTime
            Get
                Return mdEndDate
            End Get
            Set(ByVal value As DateTime)
                If mdEndDate <> value Then mbChanged = True
                mdEndDate = value
            End Set
        End Property
        Public Property AllDayBooking() As Boolean
            Get
                Return mbAllDayBooking
            End Get
            Set(ByVal value As Boolean)
                If mbAllDayBooking <> value Then mbChanged = True
                mbAllDayBooking = value
            End Set
        End Property
        Public Property RecurrenceInfo() As String
            Get
                Return msRecurrenceInfo
            End Get
            Set(ByVal value As String)
                If msRecurrenceInfo <> value Then mbChanged = True
                msRecurrenceInfo = value
            End Set
        End Property
        Public Property BookingType() As Integer
            Get
                Return miBookingType
            End Get
            Set(ByVal value As Integer)
                If miBookingType <> value Then mbChanged = True
                miBookingType = value
            End Set
        End Property
        Public Property Deleted() As Boolean
            Get
                Return mbDeleted
            End Get
            Set(ByVal value As Boolean)
                If mbDeleted <> value Then mbChanged = True
                mbDeleted = value
            End Set
        End Property
        Public Property BookedByName() As String
            Get
                If BookedBy IsNot Nothing Then
                    Return BookedBy.Name
                Else
                    Return String.Empty
                End If
            End Get
            Set(ByVal value As String)

            End Set
        End Property

    End Class

End Namespace