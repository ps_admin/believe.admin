﻿Namespace Internal.Church

    <Serializable()> _
    <CLSCompliant(True)> _
    Public Class CVolunteerApplication

        'Private Variables
        Private miApplicationID As Integer
        Private miContactID As Integer
        Private moContact As CContact
        Private miVolunteerAreaID As Integer
        Private miCampusID As Integer
        Private miMinistryID As Integer
        Private miRegionID As Integer
        Private miULGID As Integer
        Private mdDateRequested As Date
        Private mbActioned As Boolean?
        Private mdDateActioned As Date?
        Private mbApproved As Boolean?
        Private miActionedByID As Integer?
        Private moActionedBy As CContact

        Private moApprovals As Hashtable
        Private moAnswers As Hashtable

        'Initialise 
        Public Sub New()
            miApplicationID = 0
            miContactID = 0
            moContact = Nothing
            miVolunteerAreaID = 0
            miCampusID = 0
            miMinistryID = 0
            miRegionID = 0
            miULGID = 0
            mdDateRequested = Nothing
            mbActioned = Nothing
            mdDateActioned = Nothing
            mbApproved = Nothing
            miActionedByID = 0
            moActionedBy = Nothing
        End Sub
        Public Sub New(ByRef oDataRow As DataRow)
            Me.New()

            miApplicationID = oDataRow("Application_ID")
            miContactID = oDataRow("Contact_ID")
            miVolunteerAreaID = oDataRow("VolunteerArea_ID")
            miCampusID = oDataRow("Campus_ID")
            miMinistryID = oDataRow("Ministry_ID")

            If Not oDataRow.IsNull("Region_ID") Then
                miRegionID = oDataRow("Region_ID")
            End If

            If Not oDataRow.IsNull("ULG_ID") Then
                miULGID = oDataRow("ULG_ID")
            End If

            mdDateRequested = oDataRow("DateRequested")

            If Not oDataRow.IsNull("Actioned") Then
                mbActioned = oDataRow("Actioned")
            End If

            If Not oDataRow.IsNull("DateActioned") Then
                mdDateActioned = oDataRow("DateActioned")
            End If

            If Not oDataRow.IsNull("Approved") Then
                mbApproved = oDataRow("Approved")
            End If

            If Not oDataRow.IsNull("ActionedBy") Then
                miActionedByID = oDataRow("ActionedBy")
            End If

        End Sub

        'Main Methods
        Public Sub ActionApplication(ByVal iActionedBy As Integer, _
                                     ByVal bApproved As Boolean)

            'update the object
            miActionedByID = iActionedBy
            mbApproved = bApproved
            mdDateActioned = Date.Now

            'Prepare the parameters to pass into the stored procedures
            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@ApplicationID", miApplicationID))
            oParameters.Add(New SqlClient.SqlParameter("@ActionedBy", miActionedByID))
            oParameters.Add(New SqlClient.SqlParameter("@DateActioned", mdDateActioned))

            'Execute stored procedure to update 
            DataAccess.ExecuteCommand("sp_Volunteer_ActionApplication", oParameters)

        End Sub

        Public Sub UpdateApplication()
            ' Check for changes in the approval and update when necessary
            For Each oApproval As CVolunteerApplicationApproval In moApprovals.Values
                If oApproval.mbChanged Then
                    oApproval.ActionApplication()
                    oApproval.mbChanged = False
                End If
            Next
        End Sub

        'Public readonly methods
        ''' <summary>
        ''' The application ID
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property ID() As Integer
            Get
                Return miApplicationID
            End Get
        End Property
        ''' <summary>
        ''' The applicant's Contact ID
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property ContactID() As Integer
            Get
                Return miContactID
            End Get
        End Property
        ''' <summary>
        ''' Applicant's contact detail
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Contact() As CContact
            Get
                If moContact Is Nothing And miContactID > 0 Then
                    moContact = New CContact
                    moContact.LoadByID(miContactID)
                End If
                Return moContact
            End Get
        End Property
        ''' <summary>
        ''' ID of the Volunteer Area
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property VolunteerAreaID() As Integer
            Get
                Return miVolunteerAreaID
            End Get
        End Property
        ''' <summary>
        ''' Volunteer Area the contact is applying for
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property VolunteerArea() As CVolunteerArea
            Get
                Return Lists.Internal.Church.VolunteerArea(miVolunteerAreaID)
            End Get
        End Property
        ''' <summary>
        ''' Contact's campus ID
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property CampusID() As Integer
            Get
                Return miCampusID
            End Get
        End Property
        ''' <summary>
        ''' Contact's campus
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Campus() As CCampus
            Get
                Return Lists.Internal.Church.GetCampusByID(miCampusID)
            End Get
        End Property
        ''' <summary>
        ''' Contact's ministry ID
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property MinistryID() As Integer
            Get
                Return miMinistryID
            End Get
        End Property
        ''' <summary>
        ''' Contact's ministry
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Ministry() As CMinistry
            Get
                Return Lists.Internal.Church.GetMinistryByID(miMinistryID)
            End Get
        End Property

        Public ReadOnly Property RegionID() As Integer
            Get
                Return miRegionID
            End Get
        End Property

        Public ReadOnly Property Region() As CRegion
            Get
                Return Lists.Internal.Church.GetRegionByID(miRegionID)
            End Get
        End Property

        ''' <summary>
        ''' Contact's ULG ID
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property ULGID() As Integer
            Get
                Return miULGID
            End Get
        End Property
        ''' <summary>
        ''' Contact's ULG
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property ULG() As CULG
            Get
                Return Lists.Internal.Church.GetULGByID(miULGID)
            End Get
        End Property
        ''' <summary>
        ''' The date when the application was made/sent
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property DateRequested() As Date
            Get
                Return mdDateRequested
            End Get
        End Property
        ''' <summary>
        ''' Whether or not the application has been actioned
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Actioned() As Boolean
            Get
                Return mbActioned
            End Get
        End Property
        ''' <summary>
        ''' The date when the application was actioned
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property DateActioned() As Date
            Get
                Return mdDateActioned
            End Get
        End Property
        ''' <summary>
        ''' Whether or not application has been approved
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Approved() As Boolean
            Get
                Return mbApproved
            End Get
        End Property
        ''' <summary>
        ''' ID of contact that actioned this application
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property ActionedByID() As Integer
            Get
                Return miActionedByID
            End Get
        End Property
        ''' <summary>
        ''' contact that actioned this application
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property ActionedBy() As CContact
            Get
                If moActionedBy Is Nothing And miActionedByID > 0 Then
                    moActionedBy = New CContact
                    moActionedBy.LoadByID(miActionedByID)
                End If
                Return moActionedBy
            End Get
        End Property
        Public ReadOnly Property Approvals() As Hashtable
            Get
                If moApprovals Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable

                    'Prepare the parameters to pass into the stored procedures
                    Dim oParameters As New ArrayList
                    oParameters.Add(New SqlClient.SqlParameter("@ApplicationID", miApplicationID))

                    oDataTable = DataAccess.GetDataTable("sp_Volunteer_GetApprovalsByApplicationID", oParameters)

                    moApprovals = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oApprovals As New CVolunteerApplicationApproval(oDataRow)
                        moApprovals.Add(oApprovals.ID, oApprovals)
                    Next
                End If
                Return moApprovals
            End Get
        End Property
        Public ReadOnly Property Answers() As Hashtable
            Get
                If moAnswers Is Nothing Then
                    Dim oDataRow As DataRow
                    Dim oDataTable As DataTable

                    'Prepare the parameters to pass into the stored procedures
                    Dim oParameters As New ArrayList
                    oParameters.Add(New SqlClient.SqlParameter("@ApplicationID", miApplicationID))

                    oDataTable = DataAccess.GetDataTable("sp_Volunteer_GetAnswersByApplicationID", oParameters)

                    moAnswers = New Hashtable
                    For Each oDataRow In oDataTable.Rows
                        Dim oAnswer As New CVolunteerApplicationAnswer(oDataRow)
                        moAnswers.Add(oAnswer.ID, oAnswer)
                    Next
                End If
                Return moAnswers
            End Get
        End Property

    End Class
End Namespace

