Imports Planetshakers.Business.Internal.Church

Namespace Internal

    Public Module MInternal

        Public Function VolunteerApplicationList(ByVal bActioned As Boolean?, ByVal oVolunteerArea As CVolunteerArea, _
                                                 ByVal oCampus As CCampus, ByVal oMinistry As CMinistry, _
                                                 ByVal oRegion As CRegion) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Actioned", bActioned))
            oParameters.Add(New SqlClient.SqlParameter("@VolunteerArea", SQLObject(oVolunteerArea)))
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))
            oParameters.Add(New SqlClient.SqlParameter("@Ministry_ID", SQLObject(oMinistry)))
            oParameters.Add(New SqlClient.SqlParameter("@Region_ID", SQLObject(oRegion)))

            Return DataAccess.GetDataTable("sp_Volunteer_ApplicationList", oParameters)

        End Function

        Public Function welcomecardstatsList(ByVal startDate As DateTime, ByVal endDate As DateTime,
                                                 ByVal oCampus As String, ByVal oMinistry As String) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@StartDate", startDate))
            oParameters.Add(New SqlClient.SqlParameter("@EndDate", endDate))
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", oCampus))
            oParameters.Add(New SqlClient.SqlParameter("@Ministry_ID", oMinistry))

            Return DataAccess.GetDataTable("sp_Church_WelcomeCardStats", oParameters)

        End Function

        Public Function NCcardstatsList(ByVal startDate As DateTime, ByVal endDate As DateTime,
                                                 ByVal oCampus As String, ByVal oMinistry As String) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@StartDate", startDate))
            oParameters.Add(New SqlClient.SqlParameter("@EndDate", endDate))
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", oCampus))
            oParameters.Add(New SqlClient.SqlParameter("@Ministry_ID", oMinistry))

            Return DataAccess.GetDataTable("sp_church_NcCardstats", oParameters)

        End Function

        Public Function PreviousBoomBuses(ByVal startDate As Date, ByVal endDate As Date) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@StartDate", startDate))
            oParameters.Add(New SqlClient.SqlParameter("@EndDate", endDate))

            Return DataAccess.GetDataTable("sp_BoomTransportPreviousBusesList", oParameters)

        End Function

        Public Function ReportMasterStats(ByVal month As Integer, ByVal year As Integer, ByVal campusID As Object, ByVal SP As String) As DataSet

            Dim oParameters As New ArrayList
            ' SP = "sp_Church_ReportMS03MasterStatsDetailed" Then

            oParameters.Add(New SqlClient.SqlParameter("@Month", month))
            oParameters.Add(New SqlClient.SqlParameter("@Year", year))
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", campusID))

            Dim oTableMappings As New ArrayList
            oTableMappings.Add(New System.Data.Common.DataTableMapping("Table", "Church_Ministry"))
            oTableMappings.Add(New System.Data.Common.DataTableMapping("Table1", "Church_ChurchStatus"))
            'Return DataAccess.GetDataSet("sp_Church_ReportWWCC", oTableMappings, oParameters)
            Return DataAccess.GetDataSet(SP, oTableMappings, oParameters)

        End Function

        Public Function MovementTables(ByVal EndDate As DateTime, ByVal StartDate As DateTime, ByVal campusID As Object, ByVal Region_ID As Object, ByVal Ministry_ID As Object, ByVal SP As String) As DataSet

            Dim oParameters As New ArrayList
            ' SP = "sp_Church_ReportMS03MasterStatsDetailed" Then

            oParameters.Add(New SqlClient.SqlParameter("@EndDate", EndDate))
            oParameters.Add(New SqlClient.SqlParameter("@StartDate", StartDate))
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", campusID))
            oParameters.Add(New SqlClient.SqlParameter("@Region_ID", Region_ID))
            oParameters.Add(New SqlClient.SqlParameter("@Ministry_ID", campusID))

            Dim oTableMappings As New ArrayList
            oTableMappings.Add(New System.Data.Common.DataTableMapping("Table", "RegionMovement"))
            oTableMappings.Add(New System.Data.Common.DataTableMapping("Table1", "CampusMovement"))
            oTableMappings.Add(New System.Data.Common.DataTableMapping("Table2", "MinistryMovement"))
            'Return DataAccess.GetDataSet("sp_Church_ReportWWCC", oTableMappings, oParameters)
            Return DataAccess.GetDataSet(SP, oTableMappings, oParameters)

        End Function

        Public Function ReportCovenant(ByVal RoleIdCategory As String, ByVal SP As String) As DataSet

            Dim oParameters As New ArrayList
            If SP = "sp_Church_ReportCovenant" Then
                oParameters.Add(New SqlClient.SqlParameter("@Roleid", RoleIdCategory))
            Else
                oParameters.Add(New SqlClient.SqlParameter("@CategoryRoleID", RoleIdCategory))
            End If

            Dim oTableMappings As New ArrayList
            oTableMappings.Add(New System.Data.Common.DataTableMapping("Table", "common_contact"))
            oTableMappings.Add(New System.Data.Common.DataTableMapping("Table1", "Church_ContactRole"))
            oTableMappings.Add(New System.Data.Common.DataTableMapping("Table2", "common_contactinternal"))
            oTableMappings.Add(New System.Data.Common.DataTableMapping("Table3", "Church_Campus"))
            oTableMappings.Add(New System.Data.Common.DataTableMapping("Table4", "Church_Ministry"))
            oTableMappings.Add(New System.Data.Common.DataTableMapping("Table5", "Common_CovenantForm"))
            'Return DataAccess.GetDataSet("sp_Church_ReportWWCC", oTableMappings, oParameters)
            Return DataAccess.GetDataSet(SP, oTableMappings, oParameters)

        End Function

        Public Function getMatchedContactsForMatchID(ByVal MatchID As Integer) As DataSet
            Dim oParameters As New ArrayList

            oParameters.Add(New SqlClient.SqlParameter("@Match_ID", MatchID))

            Dim oTableMappings As New ArrayList
            oTableMappings.Add(New System.Data.Common.DataTableMapping("Table", "common_databasededupematch"))
            Return DataAccess.GetDataSet("sp_Common_GetDedupes", oTableMappings, oParameters)
        End Function

        Public Function getMatchIDsForDedupe() As DataSet
            Dim oParameters As New ArrayList

            'Get all the match IDs for Dedupe to show in dropdown
            Return DataAccess.GetDataSet("sp_Common_GetDedupeMatchIDs", oParameters)
        End Function

        Public Function PendingStatusChangesList(ByVal bAdditions As Boolean, ByVal bDeletions As Boolean,
                                                 ByVal oCampus As CCampus, ByVal oMinistry As CMinistry) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Additions", bAdditions))
            oParameters.Add(New SqlClient.SqlParameter("@Deletions", bDeletions))
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))
            oParameters.Add(New SqlClient.SqlParameter("@Ministry_ID", SQLObject(oMinistry)))

            Return DataAccess.GetDataTable("sp_Church_ChurchStatusChangeRequestList", oParameters)

        End Function

        'Public Function welcomecardstatsList(ByVal startDate As Date, ByVal endDate As Date,
        '                                         ByVal oCampus As String, ByVal oMinistry As String) As DataTable

        '    Dim oParameters As New ArrayList
        '    oParameters.Add(New SqlClient.SqlParameter("@StartDate", startDate))
        '    oParameters.Add(New SqlClient.SqlParameter("@EndDate", endDate))
        '    oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))
        '    oParameters.Add(New SqlClient.SqlParameter("@Ministry_ID", SQLObject(oMinistry)))

        '    Return DataAccess.GetDataTable("sp_Church_WelcomeCardStats", oParameters)

        'End Function

        Public Function GetUsersByRole(ByVal oRole As Church.CRole) As ArrayList

            Dim oDataRow As DataRow
            Dim oArrayList As New ArrayList

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Role_ID", oRole.ID))

            Dim oDataTable As DataTable = DataAccess.GetDataTable("sp_Common_ChurchListUsersByRole", oParameters)

            For Each oDataRow In oDataTable.Rows
                oArrayList.Add(Lists.GetUserByID(oDataRow("Contact_ID")))
            Next

            oArrayList.Sort(New CSorterByName)
            Return oArrayList

        End Function

        Public Function ReportBirthday(ByVal iMonth As Integer, ByVal oCampus As CCampus,
                                        ByVal oMinistry As CMinistry, ByVal oRegion As CRegion,
                                        ByVal oULG As CULG, ByVal oUser As CUser) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Month", iMonth))
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))
            oParameters.Add(New SqlClient.SqlParameter("@Ministry_ID", SQLObject(oMinistry)))
            oParameters.Add(New SqlClient.SqlParameter("@Region_ID", SQLObject(oRegion)))
            oParameters.Add(New SqlClient.SqlParameter("@ULG_ID", SQLObject(oULG)))
            oParameters.Add(New SqlClient.SqlParameter("@User_ID", SQLObject(oUser)))

            Return DataAccess.GetDataTable("sp_Church_ReportBirthday", oParameters)

        End Function

        Public Function getExpiringEmails(ByVal Fromdate As Date, ByVal SP As String) As DataSet
            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Fromdate", Fromdate))    '[sp_Church_Expiringsoon]

            Dim oTableMappings As New ArrayList
            oTableMappings.Add(New System.Data.Common.DataTableMapping("Table", "common_contact"))
            Return DataAccess.GetDataSet(SP, oTableMappings, oParameters)
        End Function

        Public Function ReportWWCC(ByVal RoleIdCategory As String, ByVal Campusid As String, ByVal Ministryid As String, ByVal SP As String) As DataSet

            Dim oParameters As New ArrayList
            If SP = "sp_Church_ReportWWCC" Then
                oParameters.Add(New SqlClient.SqlParameter("@Roleid", RoleIdCategory))
                oParameters.Add(New SqlClient.SqlParameter("@campusid", Campusid))
                oParameters.Add(New SqlClient.SqlParameter("@MinistryID", Ministryid))
            Else
                oParameters.Add(New SqlClient.SqlParameter("@CategoryRoleID", RoleIdCategory))
                oParameters.Add(New SqlClient.SqlParameter("@campusid", Campusid))
                oParameters.Add(New SqlClient.SqlParameter("@MinistryID", Ministryid))
            End If

            Dim oTableMappings As New ArrayList
            oTableMappings.Add(New System.Data.Common.DataTableMapping("Table", "common_contact"))
            oTableMappings.Add(New System.Data.Common.DataTableMapping("Table1", "Church_ContactRole"))
            oTableMappings.Add(New System.Data.Common.DataTableMapping("Table2", "common_contactinternal"))
            oTableMappings.Add(New System.Data.Common.DataTableMapping("Table3", "Church_Campus"))
            oTableMappings.Add(New System.Data.Common.DataTableMapping("Table4", "Church_Ministry"))
            'Return DataAccess.GetDataSet("sp_Church_ReportWWCC", oTableMappings, oParameters)
            Return DataAccess.GetDataSet(SP, oTableMappings, oParameters)

        End Function
        Public Function ReportWWCC() As DataSet

            Dim oParameters As New ArrayList
            Return DataAccess.GetDataSet("sp_Church_ReportWWCC", oParameters)

        End Function

        Public Function ReportPrivacyNumber(ByVal oCampus As CCampus) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))

            Return DataAccess.GetDataTable("sp_Church_ReportPrivacyNumber", oParameters)

        End Function

        Public Function ReportCovenant(ByVal RoleIdCategory As String, ByVal Campusid As String, ByVal Ministryid As String, ByVal SP As String) As DataSet

            Dim oParameters As New ArrayList
            If SP = "sp_Church_ReportCovenant" Then
                oParameters.Add(New SqlClient.SqlParameter("@Roleid", RoleIdCategory))
                oParameters.Add(New SqlClient.SqlParameter("@campusid", Campusid))
                oParameters.Add(New SqlClient.SqlParameter("@MinistryID", Ministryid))
            Else
                oParameters.Add(New SqlClient.SqlParameter("@CategoryRoleID", RoleIdCategory))
                oParameters.Add(New SqlClient.SqlParameter("@campusid", Campusid))
                oParameters.Add(New SqlClient.SqlParameter("@MinistryID", Ministryid))
            End If

            Dim oTableMappings As New ArrayList
            oTableMappings.Add(New System.Data.Common.DataTableMapping("Table", "common_contact"))
            oTableMappings.Add(New System.Data.Common.DataTableMapping("Table1", "Church_ContactRole"))
            oTableMappings.Add(New System.Data.Common.DataTableMapping("Table2", "common_contactinternal"))
            oTableMappings.Add(New System.Data.Common.DataTableMapping("Table3", "Church_Campus"))
            oTableMappings.Add(New System.Data.Common.DataTableMapping("Table4", "Church_Ministry"))
            oTableMappings.Add(New System.Data.Common.DataTableMapping("Table5", "Common_CovenantForm"))
            'Return DataAccess.GetDataSet("sp_Church_ReportWWCC", oTableMappings, oParameters)
            Return DataAccess.GetDataSet(SP, oTableMappings, oParameters)

        End Function

        Public Function ReportDatabaseRole(ByVal oCampus As CCampus, ByVal sModule As String, ByVal oDatabaseRole As CDatabaseRole) As DataTable

            Dim oParameters As New ArrayList
            oParameters.Add(New SqlClient.SqlParameter("@Campus_ID", SQLObject(oCampus)))
            oParameters.Add(New SqlClient.SqlParameter("@Module", sModule))
            oParameters.Add(New SqlClient.SqlParameter("@DatabaseRole_ID", SQLObject(oDatabaseRole)))

            Return DataAccess.GetDataTable("sp_Common_ReportDatabaseRole", oParameters)

        End Function

    End Module

End Namespace
