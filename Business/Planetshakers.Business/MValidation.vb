Public Module MValidation

#Region "Validation Regex's"

    Public Function ValidatePostcode(ByVal sPostCode As String) As Boolean
        Dim oRegEx As New System.Text.RegularExpressions.Regex("^(((2|8|9)\d{2})|((02|08|09)\d{2})|([1-9]\d{3}))$")
        Return oRegEx.IsMatch(sPostCode)
    End Function

    Public Function ValidatePhone(ByVal sPhone As String, ByVal sCountry As String) As Boolean
        Select Case sCountry
            Case "Australia"
                Dim oRegEx As New System.Text.RegularExpressions.Regex("^0[2,3,4,7,8][0-9]{8}$")
                Return oRegEx.IsMatch(sPhone) Or sPhone = String.Empty
            Case "South Africa"
                Dim oRegEx As New System.Text.RegularExpressions.Regex("^0[0-9]{9}$")
                Return oRegEx.IsMatch(sPhone) Or sPhone = String.Empty
            Case Else
                Return True
        End Select
    End Function
    Public Function ValidateMobile(ByVal sPhone As String, ByVal sCountry As String) As Boolean
        Select Case sCountry
            Case "Australia"
                Dim oRegEx As New System.Text.RegularExpressions.Regex("^04[0-9]{8}$")
                Return oRegEx.IsMatch(sPhone) Or sPhone = String.Empty
            Case "South Africa"
                Dim oRegEx As New System.Text.RegularExpressions.Regex("^0[0-9]{9}$")
                Return oRegEx.IsMatch(sPhone) Or sPhone = String.Empty
            Case Else
                Return True
        End Select
    End Function
    Public Function ValidatePhoneHome(ByVal sPhone As String, ByVal sCountry As String, ByRef sError As String) As Boolean
        Select Case sCountry
            Case "Australia"
                If Not ValidatePhone(sPhone, sCountry) Then
                    sError = "Home phone number is not valid. It must be in the format: '0xxxxxxxxx'." : Return False
                Else
                    sError = String.Empty : Return True
                End If
            Case "South Africa"
                If Not ValidatePhone(sPhone, sCountry) Then
                    sError = "Home phone number is not valid. It must be in the format: '0xxxxxxxxx'." : Return False
                Else
                    sError = String.Empty : Return True
                End If
            Case Else
                Return True
        End Select
    End Function
    Public Function ValidatePhoneMobile(ByVal sPhone As String, ByVal sCountry As String, ByRef sError As String) As Boolean
        Select Case sCountry
            Case "Australia"
                If Not ValidateMobile(sPhone, sCountry) Then
                    sError = "Mobile phone number is not valid. It must be in the format: '04xxxxxxxx'" : Return False
                Else
                    sError = String.Empty : Return True
                End If
            Case "South Africa"
                If Not ValidateMobile(sPhone, sCountry) Then
                    sError = "Mobile phone number is not valid. It must be in the format: '0xxxxxxxxx'." : Return False
                Else
                    sError = String.Empty : Return True
                End If
            Case Else
                Return True
        End Select
    End Function

    Public Function ValidatePhoneWork(ByVal sPhone As String, ByVal sCountry As String, ByRef sError As String) As Boolean
        Select Case sCountry
            Case "Australia"
                If Not ValidateMobile(sPhone, sCountry) And Not ValidatePhone(sPhone, sCountry) Then
                    sError = "Work phone number is not valid. It must be in the format: '0xxxxxxxxx' or '04xxxxxxxx'" : Return False
                Else
                    sError = String.Empty : Return True
                End If
            Case "South Africa"
                If Not ValidateMobile(sPhone, sCountry) And Not ValidatePhone(sPhone, sCountry) Then
                    sError = "Work phone number is not valid. It must be in the format: '0xx xxx xxxx'." : Return False
                Else
                    sError = String.Empty : Return True
                End If
            Case Else
                Return True
        End Select
    End Function

    Public Function ValidateEmail(ByVal sEmail As String) As Boolean
        Dim oRegEx As New System.Text.RegularExpressions.Regex("^[\w-\.]{1,}\@([\da-zA-Z-]{1,}\.){1,}[\da-zA-Z-]{2,6}$")
        Return oRegEx.IsMatch(sEmail)
    End Function

    Public Function ValidateDate(ByVal sDate As String) As Boolean
        Dim oRegex As New System.Text.RegularExpressions.Regex("^([1-9]|0[1-9]|[12][0-9]|3[01])[- /.\/]([1-9]|0[1-9]|1[012])[- /.\/](19|20|)\d\d$")
        Dim oRegex2 As New System.Text.RegularExpressions.Regex("^([1-9]|0[1-9]|[12][0-9]|3[01])[- /.\/]([a-zA-Z][a-zA-Z][a-zA-Z])[- /.\/](19|20|)\d\d$")
        Return ((oRegex.IsMatch(sDate) Or oRegex2.IsMatch(sDate)) AndAlso IsDate(sDate))
    End Function

#End Region

End Module
