Imports Planetshakers.Business.Internal.Church
Imports Planetshakers.Business.External.Events
Imports Planetshakers.Business.External.Revolution
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Web
Imports System.IO
Imports System.Threading
Imports System.Web.HttpBrowserCapabilities

Public Module MWebMain

    Public Property Browser As HttpBrowserCapabilities

    Public Sub LogAction(ByRef Category As String, ByRef Criteria As String, userID As String)
        Dim actionLogItem As New CAccessLog
        Dim RecordAccessed As String

        Dim contactid As Integer = Convert.ToInt32(userID)

        If userID <> "0" Then
            actionLogItem.ContactID = userID

            'find the site name
            Dim ModuleName As String = ""
            Dim nowip As String

            If My.Application.Info.AssemblyName = "AutoSendEmails" Then
                ModuleName = "AutoSendEmails"
                actionLogItem.ScreenName = "AutoSendEmails"
                actionLogItem.UserBrowser = "None"
                nowip = "None"
                actionLogItem.URlLink = "None"
            Else
                ModuleName = HttpContext.Current.Request.Url.AbsoluteUri.ToString()
                actionLogItem.ScreenName = HttpContext.Current.Request.Url.AbsolutePath
                actionLogItem.UserBrowser = System.Web.HttpContext.Current.Request.ServerVariables("HTTP_USER_AGENT")

                nowip = System.Web.HttpContext.Current.Request.ServerVariables("HTTP_X_FORWARDED_FOR")
                If nowip = "" Then
                    nowip = System.Web.HttpContext.Current.Request.ServerVariables("REMOTE_ADDR")
                End If

                actionLogItem.URlLink = HttpContext.Current.Request.Url.AbsoluteUri

            End If

            If ModuleName.Contains("localhost") = True Then
                ModuleName = "LocalHost"
            ElseIf ModuleName.Contains("mobiledev") = True Then
                ModuleName = "MobileDev"
            ElseIf ModuleName.Contains("mobilestage") = True Then
                ModuleName = "MobileStage"
            ElseIf ModuleName.Contains("mobile.planetshakers") = True Then
                ModuleName = "Mobile"
            ElseIf ModuleName.Contains("mobilesa") = True Then
                ModuleName = "MobileSA"
            ElseIf ModuleName.Contains("mobileaustin") = True Then
                ModuleName = "MobileTX"
            ElseIf ModuleName.Contains("mobilesg") = True Then
                ModuleName = "MobileSG"
            ElseIf ModuleName.Contains("mobilech") = True Then
                ModuleName = "MobileCH"
            ElseIf ModuleName.Contains("churchdev") = True Then
                ModuleName = "ChurchDev"
            ElseIf ModuleName.Contains("churchstage") = True Then
                ModuleName = "ChurchStage"
            ElseIf ModuleName.Contains("churchaustin") = True Then
                ModuleName = "ChurchTX"
            ElseIf ModuleName.Contains("churchsa") = True Then
                ModuleName = "ChurchSA"
            ElseIf ModuleName.Contains("churchch") = True Then
                ModuleName = "ChurchCH"
            ElseIf ModuleName.Contains("churchsg") = True Then
                ModuleName = "ChurchSG"
            ElseIf ModuleName.Contains("church.planetshakers") = True Then
                ModuleName = "Church"
            ElseIf ModuleName.Contains("registerdev") = True Then
                ModuleName = "RegisterDev"
            ElseIf ModuleName.Contains("registerstage") = True Then
                ModuleName = "RegisterStage"
            ElseIf ModuleName.Contains("register.planetshakers") = True Then
                ModuleName = "Register"

            End If

            actionLogItem.ModuleName = ModuleName

            actionLogItem.LogTime = DateTime.Now

            'nowip = HttpContext.Current.Request.UserHostAddress
            actionLogItem.IPaddress = nowip


            If (Criteria.Length > 1000) Then
                RecordAccessed = Left(Criteria, 1000)
            Else
                RecordAccessed = Criteria
            End If

            actionLogItem.ItemCategory = Category
            actionLogItem.RecordAccessed = RecordAccessed

            actionLogItem.SaveWithBrowserIP()
        End If

    End Sub

    Public Sub LogError(ByVal oException As Exception, ByVal oSession As System.Web.SessionState.HttpSessionState, ByVal oRequest As System.Web.HttpRequest, ByVal bRedirect As Boolean)

        Dim oError As New CErrorLog(oException, oSession, oRequest)

        If oRequest.QueryString("ContactGUID") <> String.Empty Then
            Dim oContact As CContact = CType(oSession("Contacts"), Hashtable)(oRequest.QueryString("ContactGUID"))
            If oContact IsNot Nothing Then
                oError.ExtraInformation += "Contact Name: " & oContact.FirstName & " " & oContact.LastName & ", ID:" & oContact.ID.ToString + vbCrLf
            End If
        End If

        If oSession("Contact") IsNot Nothing Then
            oError.ExtraInformation += "Contact Name: " & oSession("Contact").FirstName & " " & oSession("Contact").LastName & ", ID:" & oSession("Contact").ID.ToString + vbCrLf
        End If

        If oSession("User") IsNot Nothing Then
            Dim oUser As CUser = oSession("User")
            oError.ExtraInformation += "Current User: " & oUser.Name + vbCrLf
        End If

        oError.ExtraInformation += "IP Address: " & oRequest.ServerVariables("REMOTE_ADDR") + vbCrLf
        oError.ExtraInformation += "User's Browser: " & oRequest.ServerVariables("HTTP_USER_AGENT") + vbCrLf
        oError.ExtraInformation += "Last Site: " & oRequest.ServerVariables("HTTP_REFERER") + vbCrLf
        oError.ExtraInformation += "Query String: " & oRequest.ServerVariables("QUERY_STRING") + vbCrLf
        oError.ExtraInformation += "URL: " & oRequest.ServerVariables("URL") + vbCrLf

        oError.LogError()

    End Sub

    Public Function CheckCommentAccess() As Boolean

        Dim HasAccess As Boolean = False
        Dim nowip As String = System.Web.HttpContext.Current.Request.ServerVariables("HTTP_X_FORWARDED_FOR")
        If nowip = "" Then
            nowip = System.Web.HttpContext.Current.Request.ServerVariables("REMOTE_ADDR")
        End If

        Dim Matched As Boolean = False
        Dim tempIP As String = "10.1.1.23"
        Dim IpBlocks As String() = tempIP.Split(New Char() {"."c})
        Dim IpDatatable As New DataTable
        IpDatatable = DataAccess.GetDataTable("select * from Church_commentIP")

        For i = 0 To IpDatatable.Rows.Count - 1

            Dim FromIP As String = IpDatatable.Rows(i)("From_IP").ToString()
            Dim ToIP As String = Convert.ToString(IpDatatable.Rows(i)("To_IP"))

            If ToIP = "" Then
                If tempIP = FromIP Then
                    HasAccess = True
                    Exit For
                End If
            Else
                Dim FromIpBlocks As String() = FromIP.Split(New Char() {"."c})
                Dim ToIpBlocks As String() = ToIP.Split(New Char() {"."c})
                Dim IPs As String
                Dim k As Integer = 0
                For Each IPs In ToIpBlocks

                    If k = 3 Then

                        Dim FirstIP As Integer = Convert.ToInt32(FromIpBlocks(k))
                        Dim LastIP As Integer = Convert.ToInt32(ToIpBlocks(k))
                        Dim BetweenIP As Integer = Convert.ToInt32(IpBlocks(k))

                        If BetweenIP >= FirstIP And BetweenIP <= LastIP Then
                            HasAccess = True
                            Exit For
                        Else
                            HasAccess = False
                            Exit For
                        End If

                    End If

                    If IPs = IpBlocks(k) Then
                        Matched = True
                    Else
                        Matched = False
                    End If

                    If Matched = False Then
                        HasAccess = False
                        Exit For
                    End If
                    k = k + 1
                Next
            End If
        Next

        Return HasAccess
    End Function

    Public Function CheckOldPasswords(ByVal iContactID As Integer, ByVal sPassword As String, ByVal iNumberOfPreviousPasswords As Integer) As Integer

        Dim oParameters As New ArrayList
        oParameters.Add(New SqlClient.SqlParameter("@Contact_ID", iContactID))
        oParameters.Add(New SqlClient.SqlParameter("@Password", sPassword))
        oParameters.Add(New SqlClient.SqlParameter("@Number", iNumberOfPreviousPasswords))

        Return DataAccess.GetValue("sp_Common_ContactOldPasswordCheck", oParameters)

    End Function

    Public Sub SortDropDown(ByVal dd As DropDownList)

        Dim ar As ListItem() = Nothing
        Dim i As Integer = 0
        For Each li As ListItem In dd.Items
            ReDim Preserve ar(i)
            ar(i) = li
            i += 1
        Next
        Dim ar1 As Array = ar

        Array.Sort(ar1, New ListItemComparer)
        dd.Items.Clear()
        dd.Items.AddRange(ar1)

    End Sub

    Public Function Sorted(ByVal oCollection As ICollection) As ICollection

        Dim oSortedList As New SortedList
        Dim oItem As Object
        For Each oItem In oCollection
            oSortedList.Add(oItem.ToString, oItem)
        Next
        Return oSortedList.Values

    End Function

    Public Function SortedByID(ByVal oCollection As ICollection) As ICollection

        Dim oSortedList As New SortedList
        Dim oItem As Object
        For Each oItem In oCollection
            oSortedList.Add(oItem.Id, oItem)
        Next
        Return oSortedList.Values

    End Function

    Public Function SortedConference(ByVal oCollection As ICollection) As ICollection

        Dim oSortedList As New SortedList
        Dim oItem As Object
        For Each oItem In oCollection
            oSortedList.Add(IIf(oItem.ConferenceFirstStartDate = Nothing, GetDateTime, oItem.ConferenceFirstStartDate), oItem)
        Next
        Return oSortedList.Values

    End Function

    Public Function IsPhoneNumberNumeric(ByVal sNumber As String) As Boolean

        Dim sValidChars As String = "()+0123456789 "
        Dim iCount As Integer

        For iCount = 1 To sNumber.Length
            If InStr(sValidChars, Mid(sNumber, iCount, 1)) = 0 Then
                Return False
            End If
        Next

        Return True

    End Function

    Public Sub ViewCache()

        Dim entry As New DictionaryEntry            '= default(DictionaryEntry)

        LogMessage("*******************START**********************************")
        LogMessage("Started logging cache values in internal contacts.aspx")

        For Each entry In System.Web.HttpContext.Current.Cache
            LogMessage("Key: " + entry.Key.ToString() + "; Value: " + entry.Value.ToString())
        Next

        LogMessage("Started logging session values in internal contact.aspx")

        For i As Integer = 0 To HttpContext.Current.Session.Count - 1
            If Not (HttpContext.Current.Session(i) Is Nothing) Then
                LogMessage("Session Data: " + HttpContext.Current.Session.Keys(i).ToString() + " - " + HttpContext.Current.Session(i).ToString())

                If HttpContext.Current.Session(i).ToString() = "System.Collections.Hashtable" Then
                    Dim table As Hashtable = New Hashtable
                    table = HttpContext.Current.Session(i)

                    For Each element As DictionaryEntry In table
                        LogMessage("Hashtable Key: " + element.Key.ToString() + " - " + element.Value.ToString())
                    Next
                End If
            End If
        Next

        LogMessage("Completed logging cache & session values in internal contact_New.aspx")
        LogMessage("*******************END**********************************")
        LogMessage("")


    End Sub

    Public Sub LogMessage(ByVal sMessage As String)

        Dim log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod.DeclaringType)

        log.Debug(sMessage)

    End Sub

    Public Sub FillDropDownListMet(ddlSelect As HtmlSelect, ByVal oHashTable As Hashtable, ByVal sDataTextField As String, ByVal sDataValueField As String, ByVal bBlankFirstItem As Boolean, Optional ByVal iSortType As EnumSortType = EnumSortType.SortBySortOrder)

        Dim oArrayList As New ArrayList
        oArrayList.AddRange(oHashTable.Values)

        FillDropDownListMet(ddlSelect, oArrayList, sDataTextField, sDataValueField, bBlankFirstItem, iSortType)
    End Sub

    Public Sub FillDropDownListMet(ddlSelect As HtmlSelect, ByVal oArray As ArrayList, ByVal sDataTextField As String, ByVal sDataValueField As String, ByVal bBlankFirstItem As Boolean, Optional ByVal iSortType As EnumSortType = EnumSortType.SortBySortOrder)

        Dim oArrayList As New ArrayList
        Dim CheckArrayCount As Integer

        oArrayList = oArray.Clone

        If oArrayList.Count > 0 Then
            Dim oPropertyInfo As System.Reflection.PropertyInfo
            For Each oPropertyInfo In oArrayList.Item(0).GetType().GetProperties()
                Select Case iSortType
                    Case EnumSortType.SortBySortOrder
                        If oPropertyInfo.Name = "SortOrder" Then
                            'Success - we have a sortorder property. We can sort now.
                            oArrayList.Sort(New CSorterBySortOrder())
                        End If
                    Case EnumSortType.SortByName
                        If oPropertyInfo.Name = "Name" Then
                            'Success - we have a sortorder property. We can sort now.
                            oArrayList.Sort(New CSorterByName())
                        End If
                    Case EnumSortType.SortByModuleAndName
                        If oPropertyInfo.Name = "ModuleAndName" Then
                            'Success - we have a sortorder property. We can sort now.
                            oArrayList.Sort(New CSorterByModuleAndName())
                        End If
                    Case EnumSortType.SortByDate
                        If oPropertyInfo.Name = "Date" Then
                            'Success - we have a sortorder property. We can sort now.
                            oArrayList.Sort(New CSorterByDate(True))
                        End If
                End Select
            Next
        End If

        If bBlankFirstItem Then
            If oArrayList.Count > 0 Then
                If TypeOf oArrayList(0) Is Internal.Church.CULG Then
                    oArrayList.Insert(0, New Internal.Church.CULG)
                ElseIf TypeOf oArrayList(0) Is CDatabaseRole Then
                    oArrayList.Insert(0, New CDatabaseRole)
                Else
                    oArrayList.Insert(0, New CBlankComboItem)
                End If
            End If
        End If

        If bBlankFirstItem = False Then
            CheckArrayCount = 0
        Else
            CheckArrayCount = 1
        End If

        For i = 0 To oArrayList.Count - 1
            If TypeOf oArrayList(CheckArrayCount) Is CRole Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim ThisRole As CRole = oArrayList(i)

                    ddlSelect.Items.Add(New ListItem(ThisRole.Name.ToString(), ThisRole.ID))
                End If
            ElseIf TypeOf oArrayList(CheckArrayCount) Is CRoleCategory Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim ThisRoleCategory As CRoleCategory = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(ThisRoleCategory.Name.ToString(), ThisRoleCategory.ID))
                End If

            ElseIf TypeOf oArrayList(CheckArrayCount) Is CRoleType Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim ThisRoleType As CRoleType = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(ThisRoleType.Name.ToString(), ThisRoleType.ID))
                End If

            ElseIf TypeOf oArrayList(CheckArrayCount) Is CRoleTypeUL Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim ThisRoleType As CRoleTypeUL = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(ThisRoleType.Name.ToString(), ThisRoleType.ID))
                End If

            ElseIf TypeOf oArrayList(CheckArrayCount) Is CChurchStatus Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim ThisChurchStatus As CChurchStatus = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(ThisChurchStatus.Name.ToString(), ThisChurchStatus.ID))
                End If
            ElseIf TypeOf oArrayList(CheckArrayCount) Is CConference Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim ThisConference As CConference = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(ThisConference.Name.ToString(), ThisConference.ID))
                End If



            ElseIf TypeOf oArrayList(CheckArrayCount) Is CPaymentType Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim ThisPaymentType As CPaymentType = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(ThisPaymentType.Name.ToString(), ThisPaymentType.ID))
                End If

            ElseIf TypeOf oArrayList(CheckArrayCount) Is CCourse Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim ThisCourse As CCourse = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(ThisCourse.Name.ToString(), ThisCourse.ID))
                End If
            ElseIf TypeOf oArrayList(CheckArrayCount) Is CMailingList Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim ThisMailingList As CMailingList = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(ThisMailingList.Name.ToString(), ThisMailingList.ID))
                End If
            ElseIf TypeOf oArrayList(CheckArrayCount) Is CMailingListType Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim ThisMailingListType As CMailingListType = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(ThisMailingListType.Name.ToString(), ThisMailingListType.ID))
                End If

            ElseIf TypeOf oArrayList(CheckArrayCount) Is CDenomination Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim ThisDenomination As CDenomination = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(ThisDenomination.Name.ToString(), ThisDenomination.ID))
                End If

            ElseIf TypeOf oArrayList(CheckArrayCount) Is CCommentSource Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim ThisCommentSource As CCommentSource = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(ThisCommentSource.Name.ToString(), ThisCommentSource.ID))
                End If

            ElseIf TypeOf oArrayList(CheckArrayCount) Is CCampus Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim ThisCampus As CCampus = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(ThisCampus.Name.ToString(), ThisCampus.ID))
                End If

            ElseIf TypeOf oArrayList(CheckArrayCount) Is CMinistry Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim ThisMinistry As CMinistry = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(ThisMinistry.Name.ToString(), ThisMinistry.ID))
                End If

            ElseIf TypeOf oArrayList(CheckArrayCount) Is CRegion Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim ThisRegion As CRegion = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(ThisRegion.Name.ToString(), ThisRegion.ID))
                End If

            ElseIf TypeOf oArrayList(CheckArrayCount) Is CULGDateDay Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim ThisDay As CULGDateDay = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(ThisDay.Name.ToString(), ThisDay.ID))
                End If

            ElseIf TypeOf oArrayList(CheckArrayCount) Is CULGDateTime Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim ThisTime As CULGDateTime = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(ThisTime.Name.ToString(), ThisTime.ID))
                End If

            ElseIf TypeOf oArrayList(CheckArrayCount) Is CState Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim ThisState As CState = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(ThisState.Name.ToString(), ThisState.ID))
                End If


            ElseIf TypeOf oArrayList(CheckArrayCount) Is CAnotherLanguage Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim ThisAnotherLanguage As CAnotherLanguage = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(ThisAnotherLanguage.Name.ToString(), ThisAnotherLanguage.ID))
                End If

            ElseIf TypeOf oArrayList(CheckArrayCount) Is CNationality Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim ThisNationality As CNationality = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(ThisNationality.Name.ToString(), ThisNationality.ID))
                End If

            ElseIf TypeOf oArrayList(CheckArrayCount) Is CSalutation Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim ThisSalutation As CSalutation = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(ThisSalutation.Name.ToString(), ThisSalutation.ID))
                End If

            ElseIf TypeOf oArrayList(CheckArrayCount) Is CCountry Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim ThisCountry As CCountry = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(ThisCountry.Name.ToString(), ThisCountry.ID))
                End If

            ElseIf TypeOf oArrayList(CheckArrayCount) Is CRoom Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim ThisRoom As CRoom = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(ThisRoom.Name.ToString(), ThisRoom.ID))
                End If

            ElseIf TypeOf oArrayList(CheckArrayCount) Is CEntryPoint Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim ThisEntryPoint As CEntryPoint = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(ThisEntryPoint.Name.ToString(), ThisEntryPoint.ID))
                End If
            ElseIf TypeOf oArrayList(CheckArrayCount) Is CModeOfTransport Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim ThisModeOfTransport As CModeOfTransport = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(ThisModeOfTransport.Name.ToString(), ThisModeOfTransport.ID))
                End If
            ElseIf TypeOf oArrayList(CheckArrayCount) Is CCarParkUsed Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim ThisCCarParkUsed As CCarParkUsed = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(ThisCCarParkUsed.Name.ToString(), ThisCCarParkUsed.ID))
                End If
            ElseIf TypeOf oArrayList(CheckArrayCount) Is CEmploymentStatus Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim ThisCEmploymentStatus As CEmploymentStatus = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(ThisCEmploymentStatus.Name.ToString(), ThisCEmploymentStatus.ID))
                End If
            ElseIf TypeOf oArrayList(CheckArrayCount) Is CEmploymentPosition Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim ThisCEmploymentPosition As CEmploymentPosition = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(ThisCEmploymentPosition.Name.ToString(), ThisCEmploymentPosition.ID))
                End If
            ElseIf TypeOf oArrayList(CheckArrayCount) Is CEmploymentIndustry Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim ThisCEmploymentIndustry As CEmploymentIndustry = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(ThisCEmploymentIndustry.Name.ToString(), ThisCEmploymentIndustry.ID))
                End If
            ElseIf TypeOf oArrayList(CheckArrayCount) Is CVolunteerArea Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim ThisArea As CVolunteerArea = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(ThisArea.Name.ToString(), ThisArea.ID))
                End If

            ElseIf TypeOf oArrayList(CheckArrayCount) Is CRegistrationType Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim ThisType As CRegistrationType = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(ThisType.Name.ToString(), ThisType.ID))
                End If

            ElseIf TypeOf oArrayList(CheckArrayCount) Is CEducation Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim ThisEducation As CEducation = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(ThisEducation.Name.ToString(), ThisEducation.ID))
                End If
            ElseIf TypeOf oArrayList(CheckArrayCount) Is CSubscription Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim ThisSubscription As CSubscription = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(ThisSubscription.Name.ToString(), ThisSubscription.ID))
                End If
            ElseIf TypeOf oArrayList(CheckArrayCount) Is CFamilyMemberType Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim ThisFamilyMemberType As CFamilyMemberType = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(ThisFamilyMemberType.Name.ToString(), ThisFamilyMemberType.ID))
                End If
            ElseIf TypeOf oArrayList(CheckArrayCount) Is C247PrayerDay Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim This247PrayerDay As C247PrayerDay = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(This247PrayerDay.Name.ToString(), This247PrayerDay.ID))
                End If

            ElseIf TypeOf oArrayList(CheckArrayCount) Is C247PrayerTime Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim This247PrayerTime As C247PrayerTime = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(This247PrayerTime.Name.ToString(), This247PrayerTime.ID))
                End If

            ElseIf TypeOf oArrayList(CheckArrayCount) Is C247PrayerContactMethod Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim This247PrayerContactMethod As C247PrayerContactMethod = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(This247PrayerContactMethod.Name.ToString(), This247PrayerContactMethod.ID))
                End If
            ElseIf TypeOf oArrayList(CheckArrayCount) Is C247PrayerDuration Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim This247PrayerDuration As C247PrayerDuration = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(This247PrayerDuration.Name.ToString(), This247PrayerDuration.ID))
                End If
            ElseIf TypeOf oArrayList(CheckArrayCount) Is CChurchService Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim ThisChurchService As CChurchService = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(ThisChurchService.Name.ToString(), ThisChurchService.ID))
                End If
            ElseIf TypeOf oArrayList(CheckArrayCount) Is CWWCCType Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim ThisWWCCType As CWWCCType = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(ThisWWCCType.Name.ToString(), ThisWWCCType.ID))
                End If
            ElseIf TypeOf oArrayList(CheckArrayCount) Is CKidsMinistryCode Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim ThisKidsMinistryCode As CKidsMinistryCode = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(ThisKidsMinistryCode.Name.ToString(), ThisKidsMinistryCode.ID))
                End If
            ElseIf TypeOf oArrayList(CheckArrayCount) Is CSchoolGrade Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim ThisSchoolGrade As CSchoolGrade = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(ThisSchoolGrade.Name.ToString(), ThisSchoolGrade.ID))
                End If

            ElseIf TypeOf oArrayList(CheckArrayCount) Is CDatabaseRole Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim Thisdatabaserole As CDatabaseRole = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(Thisdatabaserole.ModuleAndName.ToString(), Thisdatabaserole.ID))
                End If

            ElseIf TypeOf oArrayList(CheckArrayCount) Is CNPNCType Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim ThisNPNCType As CNPNCType = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(ThisNPNCType.Name.ToString(), ThisNPNCType.ID))
                End If

            ElseIf TypeOf oArrayList(CheckArrayCount) Is CNPNCOutcomeInactive Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim ThisOutcome As CNPNCOutcomeInactive = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(ThisOutcome.Name.ToString(), ThisOutcome.ID))
                End If

            ElseIf TypeOf oArrayList(CheckArrayCount) Is CNPNCInitialStatus Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim ThisInitialStatus As CNPNCInitialStatus = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(ThisInitialStatus.Name.ToString(), ThisInitialStatus.ID))
                End If

            ElseIf TypeOf oArrayList(CheckArrayCount) Is NPNCCarer Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim ThisCarer As NPNCCarer = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(ThisCarer.Name.ToString(), ThisCarer.ID))
                End If

            ElseIf TypeOf oArrayList(CheckArrayCount) Is CGroupSize Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim ThisType As CGroupSize = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(ThisType.Name.ToString(), ThisType.ID))
                End If

            ElseIf TypeOf oArrayList(CheckArrayCount) Is CGroupType Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim ThisType As CGroupType = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(ThisType.Name.ToString(), ThisType.ID))
                End If

            ElseIf TypeOf oArrayList(CheckArrayCount) Is CGroupCreditStatus Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim ThisStatus As CGroupCreditStatus = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(ThisStatus.Name.ToString(), ThisStatus.ID))
                End If

            ElseIf TypeOf oArrayList(CheckArrayCount) Is CNCDecisionType Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim ThisDecisonType As CNCDecisionType = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(ThisDecisonType.Name.ToString(), ThisDecisonType.ID))
                End If

            ElseIf TypeOf oArrayList(CheckArrayCount) Is CUser Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim Thisuser As CUser = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(Thisuser.Name.ToString(), Thisuser.ID))
                End If

                'ElseIf TypeOf oArrayList(CheckArrayCount) Is CDatabaseDedupeMatch Then
                '    If i = 0 And bBlankFirstItem = True Then
                '        ddlSelect.Items.Add(New ListItem("", 0))
                '    Else
                '        Dim ThisDatabaseDedupeMatch As CDatabaseDedupeMatch = oArrayList(i)
                '        ddlSelect.Items.Add(New ListItem(ThisDatabaseDedupeMatch.ID.ToString(), ThisDatabaseDedupeMatch.ID))
                '    End If

            ElseIf TypeOf oArrayList(CheckArrayCount) Is CCourseInstance Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim Thisinstance As CCourseInstance = oArrayList(i)

                    If sDataTextField = "CampusListAndName" Then
                        ddlSelect.Items.Add(New ListItem(Thisinstance.CampusListAndName.ToString(), Thisinstance.ID))
                    Else
                        ddlSelect.Items.Add(New ListItem(Thisinstance.Name.ToString(), Thisinstance.ID))
                    End If
                End If

            ElseIf TypeOf oArrayList(CheckArrayCount) Is CNCSummaryStatsMinistry Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim Thisinstance As CNCSummaryStatsMinistry = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(Thisinstance.Name.ToString(), Thisinstance.ID))
                End If
            ElseIf TypeOf oArrayList(CheckArrayCount) Is CDeleteReason Then
                If i = 0 And bBlankFirstItem = True Then
                    ddlSelect.Items.Add(New ListItem("", 0))
                Else
                    Dim Thisinstance As CDeleteReason = oArrayList(i)
                    ddlSelect.Items.Add(New ListItem(Thisinstance.Name.ToString(), Thisinstance.ID))
                End If


            ElseIf TypeOf oArrayList(CheckArrayCount) Is CULG Then
                Dim ThisULG As CULG = oArrayList(i)
                ddlSelect.Items.Add(New ListItem(ThisULG.CodeAndName.ToString(), ThisULG.ID))

            End If

        Next
    End Sub

    'Private Function GetListValue(ByVal oArray As ArrayList, ByVal sDataTextField As String) As String
    '    If sDataTextField = "CodeAndName" Then
    '        Return
    '    ElseIf sDataTextField = "Name" Then
    '    End If
    '    End If
    'End Function

    Public Sub FillCombo(ByVal oCombo As DropDownList, ByVal oHashTable As Hashtable, ByVal sDataTextField As String, ByVal sDataValueField As String, ByVal bBlankFirstItem As Boolean, Optional ByVal iSortType As EnumSortType = EnumSortType.SortBySortOrder)

        Dim oArrayList As New ArrayList
        oArrayList.AddRange(oHashTable.Values)

        FillCombo(oCombo, oArrayList, sDataTextField, sDataValueField, bBlankFirstItem, iSortType)

    End Sub

    Public Sub FillCombo(ByVal oListBox As ListBox, ByVal oHashTable As Hashtable, ByVal sDataTextField As String, ByVal sDataValueField As String, ByVal bBlankFirstItem As Boolean, Optional ByVal iSortType As EnumSortType = EnumSortType.SortBySortOrder)

        Dim oArrayList As New ArrayList
        oArrayList.AddRange(oHashTable.Values)

        FillCombo(oListBox, oArrayList, sDataTextField, sDataValueField, bBlankFirstItem, iSortType)

    End Sub

    Public Sub FillCombo(ByVal oCombo As System.Web.UI.WebControls.ListControl, ByVal oArray As ArrayList, ByVal sDataTextField As String, ByVal sDataValueField As String, ByVal bBlankFirstItem As Boolean, Optional ByVal iSortType As EnumSortType = EnumSortType.SortBySortOrder)

        Dim oArrayList As New ArrayList
        oArrayList = oArray.Clone

        If oArrayList.Count > 0 Then
            Dim oPropertyInfo As System.Reflection.PropertyInfo
            For Each oPropertyInfo In oArrayList.Item(0).GetType().GetProperties()
                Select Case iSortType
                    Case EnumSortType.SortBySortOrder
                        If oPropertyInfo.Name = "SortOrder" Then
                            'Success - we have a sortorder property. We can sort now.
                            oArrayList.Sort(New CSorterBySortOrder())
                        End If
                    Case EnumSortType.SortByName
                        If oPropertyInfo.Name = "Name" Then
                            'Success - we have a sortorder property. We can sort now.
                            oArrayList.Sort(New CSorterByName())
                        End If
                    Case EnumSortType.SortByModuleAndName
                        If oPropertyInfo.Name = "ModuleAndName" Then
                            'Success - we have a sortorder property. We can sort now.
                            oArrayList.Sort(New CSorterByModuleAndName())
                        End If
                    Case EnumSortType.SortByDate
                        If oPropertyInfo.Name = "Date" Then
                            'Success - we have a sortorder property. We can sort now.
                            oArrayList.Sort(New CSorterByDate(True))
                        End If
                End Select
            Next
        End If

        If bBlankFirstItem Then
            If oArrayList.Count > 0 Then
                If TypeOf oArrayList(0) Is Internal.Church.CULG Then
                    oArrayList.Insert(0, New Internal.Church.CULG)
                ElseIf TypeOf oArrayList(0) Is CDatabaseRole Then
                    oArrayList.Insert(0, New CDatabaseRole)
                Else
                    oArrayList.Insert(0, New CBlankComboItem)
                End If
            End If
        End If

        oCombo.Items.Clear()
        oCombo.SelectedValue = Nothing
        oCombo.DataSource = oArrayList
        oCombo.DataTextField = sDataTextField
        oCombo.DataValueField = sDataValueField
        oCombo.DataBind()

    End Sub

    Public Function FormatForTable(ByVal InputDate As DateTime) As String
        Dim FormattedDate As String
        'We want to format dates for display

        If String.IsNullOrEmpty(InputDate.ToString()) Then
            FormattedDate = ""
        Else
            FormattedDate = InputDate.ToString("dd MMM yyyy")
        End If

        If FormattedDate = "01 Jan 0001" Then
            FormattedDate = ""
        End If
        Return FormattedDate

    End Function

    Public Function FormatForTableTime(ByVal InputDate As DateTime) As String
        'We want to format dates for display
        Dim FormattedDate As String = InputDate.ToString("dd MMM yyyy")
        'Dim date0 As DateTime = Nothing
        'Dim date1 = date0.ToString("dd MMM yyyy")

        If FormattedDate = "01 Jan 0001" Then
            FormattedDate = ""
        Else
            FormattedDate = InputDate.ToString("dd MMM yyyy HH:mm")
        End If

        Return FormattedDate

    End Function

    Public Function FormatObjectForTable(ByRef oColumn As Object) As String
        'We want to format objects (Region) for display
        Dim ColumnName As String

        If oColumn Is Nothing Then
            ColumnName = ""
        Else
            ColumnName = oColumn.Name.ToString()
        End If

        Return ColumnName
    End Function

    Public Function FormatDateTimeForTable(ByVal InputDate As DateTime) As String
        'We want to format dates for display
        Dim FormattedDate As String = InputDate.ToString("dd MMM yyyy HH:MM")
        'Dim date0 As DateTime = Nothing
        'Dim date1 = date0.ToString("dd MMM yyyy")


        If FormattedDate = "01 Jan 0001" Then
            FormattedDate = ""
        End If
        Return FormattedDate


    End Function

    Public Function FormatForTable(ByVal Flag As Boolean, Optional bReverse As Boolean = False) As String
        Dim formattedFlag As String

        If bReverse = True Then
            Flag = Not Flag
        End If

        If (Flag = True) Then
            formattedFlag = "Yes"
        Else
            formattedFlag = "No"
        End If

        Return formattedFlag
    End Function

    'This is called from Church.Web to create a cookie
    Public Sub DeleteAllCookies(request As HttpRequestBase, response As HttpResponseBase)

        If (request.Cookies("Planetshakers-Mobile") IsNot Nothing) Then
            'Call a common function to delete cookie 
            WriteCookie("Planetshakers-Mobile", 0, response, True)
        End If

        If (request.Cookies("Planetshakers-Church") IsNot Nothing) Then
            'Call a common function to delete cookie 
            WriteCookie("Planetshakers-Church", 0, response, True)
        End If
    End Sub

    Public Sub DeleteAllCookies(request As HttpRequest, response As HttpResponse)
        If (request.Cookies("Planetshakers-Mobile") IsNot Nothing) Then
            'Call a common function to delete cookie 
            WriteCookie("Planetshakers-Mobile", 0, response, True)
        End If

        If (request.Cookies("Planetshakers-Church") IsNot Nothing) Then
            'Call a common function to delete cookie 
            WriteCookie("Planetshakers-Church", 0, response, True)

        End If
    End Sub
    'This is called from Church.Web to create a cookie
    Public Sub WriteCookie(cookieName As String, cookieValue As Integer, response As HttpResponse, Optional deleteCookie As Boolean = False)
        'Call a common function to create cookie & return here
        response.Cookies.Add(CreateCookie(cookieName, cookieValue, deleteCookie))
    End Sub

    'This is called from Mobile & Register i.e. MVC to create a cookie
    Public Sub WriteCookie(cookieName As String, cookieValue As Integer, response As HttpResponseBase, Optional deleteCookie As Boolean = False)
        'Call a common function to create cookie & return here
        response.Cookies.Add(CreateCookie(cookieName, cookieValue, deleteCookie))
    End Sub

    Private Function CreateCookie(cookieName As String, cookieValue As Integer, deleteCookie As Boolean) As HttpCookie

        Dim myCookie As HttpCookie = New HttpCookie(cookieName)
        If deleteCookie = True Then
            myCookie.Expires = DateTime.Now.AddHours(-1D)
        Else
            myCookie.Value = cookieValue
            myCookie.Expires = DateTime.Now.AddHours(5)
        End If
        Dim ModuleName As String = ""
        ModuleName = HttpContext.Current.Request.Url.AbsoluteUri.ToString()
        If ModuleName.Contains("localhost") = True Then
            ModuleName = "LocalHost"
        Else
            myCookie.Domain = ".planetshakers.com"
        End If

        Return myCookie
    End Function

    Public Function PSCookieAliveValue(Request As HttpRequest) As String
        Dim CookieValue As String = ""

        If (Request.Cookies("Planetshakers-Mobile") IsNot Nothing) Then
            CookieValue = Request.Cookies("Planetshakers-Mobile").Value.ToString()
            Return CookieValue
        ElseIf (Request.Cookies("Planetshakers-Church") IsNot Nothing) Then
            CookieValue = Request.Cookies("Planetshakers-Church").Value.ToString()
            Return CookieValue
        ElseIf (Request.Cookies("Planetshakers-Register") IsNot Nothing) Then
            CookieValue = Request.Cookies("Planetshakers-Register").Value.ToString()
            Return CookieValue
        Else
            Return String.Empty
        End If

    End Function

    Public Function PSCookieAliveValue(Request As HttpRequestBase) As String
        Dim CookieValue As String = ""

        If (Request.Cookies("Planetshakers-Mobile") IsNot Nothing) Then
            CookieValue = Request.Cookies("Planetshakers-Mobile").Value.ToString()
            Return CookieValue
        ElseIf (Request.Cookies("Planetshakers-Church") IsNot Nothing) Then
            CookieValue = Request.Cookies("Planetshakers-Church").Value.ToString()
            Return CookieValue
        ElseIf (Request.Cookies("Planetshakers-Register") IsNot Nothing) Then
            CookieValue = Request.Cookies("Planetshakers-Register").Value.ToString()
            Return CookieValue
        Else
            Return String.Empty
        End If

    End Function

    Private Class CBlankComboItem

        Public ReadOnly Property ID() As Integer
            Get
                Return 0
            End Get
        End Property
        Public ReadOnly Property Name() As String
            Get
                Return String.Empty
            End Get
        End Property
        Public ReadOnly Property FullName() As String 'for CState
            Get
                Return String.Empty
            End Get
        End Property
        Public ReadOnly Property NameAndCost() As String 'for CRegistrationType
            Get
                Return String.Empty
            End Get
        End Property
        Public ReadOnly Property CodeAndName() As String 'for CULG
            Get
                Return String.Empty
            End Get
        End Property
        Public ReadOnly Property CampusListAndName() As String 'for CCourseInstance
            Get
                Return String.Empty
            End Get
        End Property

    End Class

End Module
