Imports System.Data.SqlClient
Imports System.Web.SessionState
Imports System.Web
Imports System.Configuration
Imports System.IO
Imports System.Text

<CLSCompliant(True)> _
Public Class CErrorLog

    Private mobjException As Exception
    Private mobjHTTPSessionState As HttpSessionState
    Private mobjHTTPRequest As HttpRequest
    Private mstrErrorAsString As String

    Private msExtraInformation As String

    Public Sub New(ByVal pobjException As Exception, ByVal pobjHTTPSessionState As HttpSessionState, ByVal pobjHTTPRequest As HttpRequest)
        mobjException = pobjException
        mobjHTTPSessionState = pobjHTTPSessionState
        mobjHTTPRequest = pobjHTTPRequest
    End Sub

    ReadOnly Property ErrorAsString() As String
        Get
            'if this text hasn't been set yet, then fill it in.
            If mstrErrorAsString Is Nothing Then mstrErrorAsString = Me.GetErrorAsString
            Return mstrErrorAsString
        End Get

    End Property

    Public Sub LogError()

        'read in the XML config settings and see what logging options should be used.
        Dim strLogEmail As String = ConfigurationManager.AppSettings.Item("ErrorLoggingLogEmail")
        Dim strLogFile As String = System.Web.HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings.Item("ErrorLoggingLogFile"))

        'This error handling may seem strange, however nothing exists in the catch blocks because an error may fail
        'being logged to one location. If thats the case, we don't want to stop trying the other locations by exiting 
        'this sub. If say the event log is full, we still want to log to a file or database without bombing out.
        Try
            If ConfigurationManager.AppSettings("ErrorLoggingLogToEmail").ToUpper = "TRUE" Then LogToEmail(strLogEmail)
        Catch
            'don't bomb out
        End Try

        Try
            If ConfigurationManager.AppSettings.Item("ErrorLoggingLogToFile").ToUpper = "TRUE" Then LogToFile(strLogFile)
        Catch ex As Exception
            'don't bomb out
        End Try

    End Sub

    Private Sub LogToEmail(ByVal strEmail As String)

        Dim objEmail As New CEmail

        objEmail.EmailText = Replace(ErrorAsString(), vbCrLf, "<br />")
        objEmail.Subject = "Planetshakers Website Error - " & Format(Now, "dd MMM yyyy HH:mm:ss") & " - on " & System.Web.HttpContext.Current.Request.ServerVariables("SERVER_NAME")
        objEmail.ToAddress = strEmail
        objEmail.SendEmail(String.Empty)

    End Sub

    Private Sub LogToFile(ByVal pstrFileName As String)

        Dim objSRLogFile As New StreamWriter(pstrFileName, True)

        Try
            objSRLogFile.Write(Me.ErrorAsString)
            objSRLogFile.Close()
        Catch ex As Exception
            'hopefully you also have the log to event log selected and that may have worked. If not, you will probably
            'not know the error information since the attempt to log to file failed. 
            'It may have failed for some reason writing to a file (this could be because of an exclusive lock on the file
            'or security permissions, aspnet_wp may not have permissions to the file, etc etc. 

            'one option here may be shell off an email or net send, depending on your system requirements.
            Throw ex
        End Try

    End Sub

    Private Function GetErrorAsString() As String

        Dim objTempException As Exception
        Dim intExceptionLevel As Integer
        Dim objSB As New StringBuilder
        Dim strFormData As String
        objSB.Append("-----------------" & Now.ToString & "-----------------" & vbCrLf)
        objSB.Append("SessionID:" & mobjHTTPSessionState.SessionID & vbCrLf)

        strFormData = vbTab & Me.GetStringFromArray(mobjHTTPRequest.Form.AllKeys).Replace(vbCrLf, vbCrLf & vbTab)
        If strFormData.Length > 1 Then '1 because if the form is empty it will just contain the tab prefixed to the line.
            objSB.Append("Form Data:" & vbCrLf)
            'remove the last tab so it doesn't screw up formatting on the line after it.
            objSB.Append(strFormData.Substring(0, strFormData.Length - 1) & vbCrLf)
        Else
            objSB.Append("Form Data: No Form Data Found" & vbCrLf)
        End If

        objTempException = mobjException
        While Not (objTempException Is Nothing)
            'is this the 1st, 2nd, etc exception in the hierarchy
            intExceptionLevel += 1
            objSB.Append(intExceptionLevel & ": Error Description:" & objTempException.Message & vbCrLf)

            objSB.Append(intExceptionLevel & ": Source:" & Replace(objTempException.Source, vbCrLf, vbCrLf & intExceptionLevel & ": ") & vbCrLf)

            objSB.Append(intExceptionLevel & ": Stack Trace:" & Replace(objTempException.StackTrace, vbCrLf, vbCrLf & intExceptionLevel & ": ") & vbCrLf)

            objSB.Append(intExceptionLevel & ": Target Site:" & Replace(objTempException.TargetSite.ToString, vbCrLf, vbCrLf & intExceptionLevel & ": ") & vbCrLf)

            'get the next exception to log
            objTempException = objTempException.InnerException

        End While

        objSB.Append("E: Extra Information - " & Replace(msExtraInformation, vbCrLf, vbCrLf & "E: "))
        objSB.Append(vbCrLf & vbCrLf & vbCrLf)

        msExtraInformation = ""

        Return objSB.ToString()

    End Function

    'There may be a better way in .Net to do this, but as of now I am not aware of one. 
    'since Array.ToString doesn't yet exist (and why should it, since it is would have to assume 
    'all types of array and be converted to a string, which is not a true statement (unless 
    'each class is coded to provide its own ToString)
    Private Function GetStringFromArray(ByVal pobjArray As String()) As String
        Dim i As Integer
        Dim objSB As New StringBuilder
        Dim strKey As String

        For i = pobjArray.GetLowerBound(0) To pobjArray.GetUpperBound(0)
            strKey = CType(pobjArray.GetValue(i), String)
            If Not strKey.StartsWith("__") Then 'Exclude Viewstate, etc
                objSB.Append(strKey & " - " & mobjHTTPRequest.Form.Item(strKey) & vbCrLf)
            End If
        Next i
        Return objSB.ToString
    End Function

    Public Property ExtraInformation() As String
        Get
            Return msExtraInformation
        End Get
        Set(ByVal Value As String)
            msExtraInformation = Value
        End Set
    End Property

End Class