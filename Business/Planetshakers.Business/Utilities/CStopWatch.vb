Imports System
Imports System.Runtime.InteropServices

'This is a timing class used to keep track of the current system time, 
'no matter if the user's clock is incorrect, or if the change their system time
'This is only useful on the Conference Database system, which runs on the client.

<CLSCompliant(True)> _
Public NotInheritable Class CStopWatch

    Private Declare Ansi Function QueryPerformanceCounter Lib "kernel32.dll" (ByRef x As Long) As Integer
    Private Declare Ansi Function QueryPerformanceFrequency Lib "kernel32.dll" (ByRef x As Long) As Integer

    Private m_StartTime As Long
    Private m_Frequency As Long

    Public Sub New()
        Frequency = GetFrequency()
        Reset()
    End Sub

    Public Sub Reset()
        StartTime = GetValue()
    End Sub

    Public Function Peek() As Long
        Return CType((((GetValue() - StartTime) / CType(Frequency, Double)) * 1000), Long)
    End Function

    Private Function GetValue() As Long
        Dim ret As Long = 0
        If QueryPerformanceCounter(ret) = 0 Then Throw New NotSupportedException("Error while querying the high-resolution performance counter.")
        Return ret
    End Function

    Private Function GetFrequency() As Long
        Dim ret As Long = 0
        If QueryPerformanceFrequency(ret) = 0 Then Throw New NotSupportedException("Error while querying the performance counter frequency.")
        Return ret
    End Function

    Private Property StartTime() As Long
        Get
            Return m_StartTime
        End Get
        Set(ByVal Value As Long)
            m_StartTime = Value
        End Set
    End Property

    Private Property Frequency() As Long
        Get
            Return m_Frequency
        End Get
        Set(ByVal Value As Long)
            m_Frequency = Value
        End Set
    End Property

End Class