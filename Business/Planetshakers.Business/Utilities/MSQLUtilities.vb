Public Module MSQLUtilities

    Public Function IsNull(ByVal vData As Object, ByVal vAlternative As Object) As Object
        If IsDBNull(vData) Then
            Return vAlternative
        Else
            Return vData
        End If
    End Function
    Function IfNothing(ByVal vObject As Object, ByVal vSubstitute As Object) As Object

        If vObject Is Nothing Then
            Return vSubstitute
        Else
            Return vObject
        End If

    End Function

    Public Function SQL(ByVal sData As String) As String
        Return "'" & Replace(sData, "'", "''") & "'"
    End Function
    Public Function SQL(ByVal iData As Integer) As String
        Return CStr(iData)
    End Function
    Public Function SQL(ByVal bData As Boolean) As String
        Return IIf(bData, "1", "0")
    End Function
    Public Function SQL(ByVal dDecimal As Decimal) As String
        Return CStr(Format(dDecimal, "#0.0000"))
    End Function
    Public Function SQL(ByVal dDate As Date) As String
        Return "'" & CStr(Format(dDate, "dd MMM yyyy HH:mm:ss")) & "'"
    End Function
    Public Function SQLDate(ByVal dDate As Date) As Object
        If dDate = Nothing Then Return DBNull.Value
        Return CStr(Format(dDate, "dd MMM yyyy"))
    End Function
    Public Function SQLDateTime(ByVal dDate As Date) As Object
        If dDate = Nothing Then Return DBNull.Value
        Return CStr(Format(dDate, "dd MMM yyyy HH:mm:ss.fff"))
    End Function
    Public Function SQLObject(ByVal oObject As Object) As Object
        If oObject Is Nothing Then Return DBNull.Value
        Return oObject.id
    End Function
    Public Function SQLNull(ByVal iData As Integer) As Object
        If iData = 0 Then Return DBNull.Value
        Return iData.ToString
    End Function


    Function SearchString(ByVal sSearch As String) As String
        '* return a string that can be used in a sql LIKE operation
        '* replace all *'s with %'s
        '* if there are no %'s then add to start and end

        sSearch = Replace(sSearch, "'", "''")
        'sSearch = Replace(sSearch, "'", "''")
        sSearch = Replace(sSearch, "%", "")
        'sSearch = Replace(sSearch, "*", "%")
        If sSearch = Nothing Then sSearch = ""
        ' If InStr(sSearch, "%") = 0 And sSearch.Length > 0 Then
        'sSearch = "%" + sSearch + "%"
        'End If
        Return sSearch

    End Function

    Public Function GetCharCount(ByVal sText As String, ByVal sChar As String) As Integer

        '* return the number of ocurrences of sChar in sText
        '* sChar must be a single character
        '* eg. CharCount("AB|CD|EF","|") = 2

        Dim i As Integer
        Dim iResult As Integer

        iResult = 0
        For i = 1 To Len(sText)
            If Mid(sText, i, 1) = sChar Then
                iResult = iResult + 1
            End If
        Next i

        Return iResult

    End Function

    Public Function CheckDateChanged(ByVal date1 As Date, ByVal date2 As Date) As Boolean

        If IsNothing(date1) And IsNothing(date2) Then Return True
        If IsNothing(date1) And Not IsNothing(date2) Then Return False
        If Not IsNothing(date1) And IsNothing(date2) Then Return False
        Return date1 <> date2

    End Function

End Module
