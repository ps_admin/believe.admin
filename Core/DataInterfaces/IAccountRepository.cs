﻿using System.Collections.Generic;
using Planetshakers.Core.Models;

namespace Planetshakers.Core.DataInterfaces
{
    public interface IAccountRepository
    {
        Account Login(string email, string password);
        Account GPLogin(string email);
        List<Course> GetAvailable(int iContactid);
        List<int> GetAllUrbanLifeIdsByRegionId(List<int> regionIds);
        IList<RegionalUrbanLife> GetAllRegionByContactId(int contactId);
        IList<RestrictedAccessUrbanLife> GetAllRestrictedAccessUrbanLifeGroupByContactId(int contactId);
        IList<UrbanLife> GetUrbanLifeGroupsById(List<int> urbanLifeGroupIds);
        IList<UrbanLife> GetUrbanLifeGroups(int contactId, int regionId);
        IList<RoleItem> GetUserRoles(int contactId);
        List<ULMember> GetCarer(int ULG_ID);
        List<ULMember> GetNCCarer(int ULG_ID);

        List<ULMember> GetCarerByContactID(int Contact_ID);
        List<ULMember> GetContactByCarerID(int Carer_ID);
        List<ULMember> GetULGMembers(int ULGId);
        IList<ContactRole> GetContactRoles(int contactId);
        IList<CarerAccessDetail> GetCarerAccessDetails(int carerId);
        IList<CareForDetail> GetCareForDetails(int carerId);
    }
}
