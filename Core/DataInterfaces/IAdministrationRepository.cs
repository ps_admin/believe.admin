﻿using System;
using System.Collections.Generic;
using Planetshakers.Core.Models;

namespace Planetshakers.Core.DataInterfaces
{
    public interface IAdministrationRepository
    {
        void insertWelcome(WelcomeCard welcomeCardEntry);

        Tuple<List<SponsorshipCampusTotal>, List<SponsorshipMinistryTotal>> SponsorshipCampusMinistryBreakdown(int Conference_ID);

        List<SponsorshipPaymentTypeTotal> SponsorshipPaymentTypeBreakdown(int Conference_ID);
    }
}
