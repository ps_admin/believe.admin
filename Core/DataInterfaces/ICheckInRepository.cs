﻿
using System.Collections.Generic;
using Planetshakers.Core.Models;

namespace Planetshakers.Core.DataInterfaces
{
    public interface ICheckInRepository
    {
        Registration FindById(int id, int venue_id, int conference_id);
        List<Registration> FindByGroup(int group_id, int venue_id, int conference_id);
        void MarkById(int id);
        void InsertRegistration(int id, int venue_id, int registrationtype_id);

        decimal GetOutstandingBalance(int contact_id, int conference_id);
    }
}
