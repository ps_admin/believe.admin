﻿using Planetshakers.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planetshakers.Core.DataInterfaces
{
    public interface IManagementRepository
    {
        List<AccessLevelPermission> GetContactAccessLevelPermission(int Contact_ID);
    }
}
