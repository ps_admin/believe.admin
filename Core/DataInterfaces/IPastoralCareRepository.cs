﻿using System.Collections.Generic;
using Planetshakers.Core.Models;

namespace Planetshakers.Core.DataInterfaces
{
    public interface IPastoralCareRepository
    {
        PCRContact FindPCRContactById(int contactId, int ulgid);

        /// <summary>
        /// Find all members of given urban life that are active on given PCR
        /// </summary>
        /// <param name="urbanLifeId"></param>
        /// <param name="pcrId">If null, indicates the latest PCR</param>
        /// <returns></returns>
        IEnumerable<PCRContact> FindPCRContactsByUrbanLife(int urbanLifeId, int? pcrId = null);

        IEnumerable<PCRContact> FindPCRContactsByUrbanLifeAndCarer(int urbanLifeId, int carerId, int? pcrId = null);

        void UpdateAttendanceForUrbanLife(int pcrContactId, bool attend);

        void UpdateAttendanceForSundayService(int pcrContactId, bool attend);

        void UpdateAttendanceForBoom(int userId, int? bdtoId, int pcrContactId, bool attend);

        void AddComment(int contactId, int pcrContactId, int commenterId, int numofcall, int numofothers, int numofvisit, string comments, bool pastoralComment);

        List<CommentItem> GetComments(int contactId, bool PastoralAccess);

        void SubmitReport(int pcrId, double totalOffering, int totalVisitor, int totalChildren, string comments, bool markAsCompleted);

        PCRWeeklyItem GetWeeklyReportItem(int urbanLifeId, int? pcrDateId = null);

        IList<PCRHistoryItem> GetHistoricalReportItems(int urbanLifeId, int ageInDay = 90);

        List<BoomPCRContact> GetBoomSearch(string subquery);

        List<BoomPCRContact> GetBoomPCRContact(List<BoomPCRContact> boomList);

        void BoomBusesCheckOutUpdate(List<int> list, int bdtoID, int userID);
    }
}
