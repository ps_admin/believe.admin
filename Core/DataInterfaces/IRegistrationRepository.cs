﻿using System.Collections.Generic;
using Planetshakers.Core.Models;

namespace Planetshakers.Core.DataInterfaces
{
    public interface IRegistrationRepository
    {
        List<SingleRegistration> FindSingle(SearchModel model);
        List<GroupRegistration> FindGroup(SearchModel model);
        List<Registration> UpdateRegistration(Registration model);
        List<PCRContact> GetValidInternalExternal(int user_id, string firstname, string lastname, string mobile, string email);
        int InsertRegistration(Registration rego);
        void RemoveRegistration(int reg_id, int user_id);
        void InsertBulkRegistration(BulkRegistration model, int user_id);
        void DeleteBulkRegistration(int BulkRego_ID, int user_id);
        int InsertPayment(Payment model);
        int InsertGroupPayment(Payment model);
        void RemovePayment(int pay_id, int user_id);
        void RemoveGroupPayment(int pay_id, int user_id);
        OutstandingBalance GetOutstandingBalance(string GroupSingle, int Contact_ID, int Conference_ID);
    }
}
