﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Planetshakers.Core.Models;

namespace Planetshakers.Core.DataInterfaces
{
    public interface IReportRepository
    {
        EventSummary GenerateSummaryReport(int conferenceId, String reportType, DateTime StartDate, DateTime EndDate);
        EventsTotalRegistration GenerateTotalRegistrationReport(int conferenceId);
        EventSummary GenerateBankingReport(int conference_ID, DateTime? StartDate, DateTime? EndDate, string selectedUser, Boolean IncludeTransactionReport);
        EventSummary GenerateRegReport(int conference_ID, int venue_ID, String reportType, List<int> SelectRegoTypes);
        EventSummary GenerateTotalRegistrantsReport();
    }


}
