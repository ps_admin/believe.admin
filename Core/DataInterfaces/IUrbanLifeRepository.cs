﻿using Planetshakers.Core.Models;

namespace Planetshakers.Core.DataInterfaces
{
    public interface IUrbanLifeRepository
    {
        UrbanLife FindById(int id);
    }
}
 