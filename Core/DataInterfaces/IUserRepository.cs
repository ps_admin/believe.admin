﻿using System.Collections.Generic;
using Planetshakers.Core.Models;

namespace Planetshakers.Core.DataInterfaces
{
    public interface IUserRepository
    {
        IList<CommentStream> GetCommentActivityStreams(int userId);
    }
}
