﻿using System;
using System.Globalization;
using System.Linq;

namespace Planetshakers.Core.Helpers
{
    public static class DateTimeExtensions
    {
        private static readonly Calendar calendar = CultureInfo.InvariantCulture.Calendar;

        private static readonly string[] NAMES = {
                "year",
                "month",
                "day",
                "hour",
                "minute",
                "second"
            };

        public static String DisplayFormattedDate(this DateTime datetime)
        {
            if (datetime == default(DateTime))
                return "-";

            return datetime.ToString("dd MMM yyyy");
        }

        public static String DisplayFormattedDateWithoutYear(this DateTime datetime)
        {
            if (datetime == default(DateTime))
                return "-";

            return datetime.ToString("dd MMM");
        }        

        // This presumes that weeks start with Monday.
        // Week 1 is the 1st week of the year with a Thursday in it.
        public static int GetWeekNumber(this DateTime time)
        {
            // Seriously cheat.  If its Monday, Tuesday or Wednesday, then it'll 
            // be the same week# as whatever Thursday, Friday or Saturday are,
            // and we always get those right
            DayOfWeek day = calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }

            // Return the week of our adjusted day
            return calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }

        public static string TimeAgo(this DateTime dateTime, int maxDayToProcess = -1)
        {
            if (dateTime == default(DateTime))
                return "Never";            

            TimeSpan ts = DateTime.Now - dateTime;

            if (ts.TotalDays > maxDayToProcess)
            {
                return "On " + dateTime.ToString("dd MMM yy");
            }

            int[] ints = {
                         (int)Math.Floor(ts.Days / 365d),
                         (int)Math.Floor(ts.Days / 30d),
                         ts.Days,
                         ts.Hours,
                         ts.Minutes,
                         ts.Seconds
                     };

            double[] doubles = {
                               Math.Floor(ts.TotalDays / 365d),
                               Math.Floor(ts.TotalDays / 30d),
                               ts.TotalDays,
                               ts.TotalHours,
                               ts.TotalMinutes,
                               ts.TotalSeconds
                           };

            var firstNonZero = ints
                .Select((value, index) => new { value, index })
                .FirstOrDefault(x => x.value != 0);

            if (firstNonZero == null)
            {
                return "just now";
            }

            int i = firstNonZero.index;

            var quantity = (int)Math.Round(doubles[i]);
            return Tense(quantity, NAMES[i]) + " ago";
        }

        private static string Tense(int quantity, string noun)
        {
            return quantity == 1
                ? "1 " + noun
                : string.Format("{0} {1}s", quantity, noun);
        }
    }
}