﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planetshakers.Core.Models
{
    public class AccessLevelPermission
    {
        public int Contact_ID { get; set; }
        public int DbObjID { get; set; }
        public string DbObjText { get; set; }
        public string DbObjName { get; set; }
        public bool RoleRead { get; set; }
        public bool RoleWrite { get; set; }
        public bool CustomRead { get; set; }
        public bool CustomWrite { get; set; }
    }
}
