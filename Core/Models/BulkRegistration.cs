﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Planetshakers.Core.Models
{
    public class BulkRegistration
    {
        public int ID { get; set; }
        public int GroupLeader_ID { get; set; }
        public string GroupLeader_Name { get; set; }
        public int Venue_ID { get; set; }
        public string Venue_Name { get; set; }

        public int RegistrationType_ID { get; set; }
        public string RegistrationType { get; set; }

        public int? Quantity { get; set; }

        public DateTime DateAdded { get; set; }

        public decimal TotalCost { get; set; }
    }
}
