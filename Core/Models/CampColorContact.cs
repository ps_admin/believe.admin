﻿namespace Planetshakers.Core.Models
{
    public class CampColorContact
    {
        public int Contact_ID { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Mobile { get; set; }

        public string Email { get; set; }

        public string Email2 { get; set; }

        public string CampColor { get; set; }

    }
}
