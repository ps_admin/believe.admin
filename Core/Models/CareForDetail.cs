﻿using System;

namespace Planetshakers.Core.Models
{
    public class CareForDetail
    {
        public int CarerAccessId { get; set; }

        public int ContactId { get; set; }

        public int CarerId { get; set; }

        public DateTime DateAllocated { get; set; }

        public DateTime DateInactive { get; set; }

        public bool Inactive { get; set; }

        public int UrbanLifeGroupContactId { get; set; }

        public int UrbanLifeId { get; set; }

        public string UrbanLifeCode { get; set; }

        public string UrbanLifeName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Mobile { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }
    }
}
