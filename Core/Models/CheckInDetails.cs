﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planetshakers.Core.Models
{
    public class CheckInDetails
    {
        public int RegistrationID { get; set; }
        public bool Attended { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string RegistrationType { get; set; }
        public int RegistrationType_ID { get; set; }
        
    }
}
