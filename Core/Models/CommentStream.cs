﻿using System;

namespace Planetshakers.Core.Models
{
    public class CommentStream
    {
        public string ContactFirstName { get; set; }
        public string ContactLastName { get; set; }
        public DateTime CommentDate { get; set; }
        public string FullComment { get; set; }
        public CommentType CommentType { get; set; }
        public bool Actioned { get; set; }

        private string _action;
        private string _comment;

        public string Action {
            get
            {
                if (_action != null)
                    return _action;

                if (FullComment != null && FullComment.StartsWith("["))
                {
                    var closeBracketIdx = FullComment.IndexOf("]", System.StringComparison.Ordinal);
                    _action = FullComment.Substring(1, closeBracketIdx - 1);

                    if (_action == "Visit")
                        _action = "Visited";
                    else if (_action == "Call")
                        _action = "Called";
                    else
                        _action = "Commented";
                }

                return _action;
            }
        }

        public string Comment
        {
            get
            {
                if (_comment != null)
                    return _comment;

                if (FullComment != null && FullComment.StartsWith("["))
                {
                    var closeBracketIdx = FullComment.IndexOf("]", System.StringComparison.Ordinal);
                    _comment = FullComment.Substring(closeBracketIdx + 1).Trim();
                }
                else
                {
                    _comment = FullComment;
                }

                return _comment;
            }
        }
    }
}
