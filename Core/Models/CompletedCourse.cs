﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Planetshakers.Core.Models
{
    public class CompletedCourse
    {
        public int Course_ID { get; set; }
        public string CourseName { get; set; }
    }

}
