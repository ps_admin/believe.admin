﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Planetshakers.Core.Models
{
    public class Conference
    {
        public Conference()
        {

        }

        public Conference(int id)
        {
            this.ID = id;
        }

        public int ID { get; set; }
        public string ConferenceName { get; set; }

        public int Venue_ID { get; set; }
        /*
        public string ConferenceNameShort { get; set; }
        public string ConferenceDate { get; set; }
        public string ConferenceLocation { get; set; }
        public string VenueLocation { get; set; }
        public string VenueName { get; set; }
        public int State_ID { get; set; }
        public int MaxCapacity { get; set; }
        public bool AllowMultipleRegistrants { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string ConferenceTagline { get; set; }
        public int ConferenceType { get; set; }
        public int Country_ID { get; set; }
        public bool AllowOnlineRegistrationSingle { get; set; }
        public bool AllowOnlineRegistrationGroup { get; set; }
        public bool ShowConferenceOnWeb { get; set; }
        public bool AllowGroupCreditApproval { get; set; }
        public bool AllowGroupOnlineBulkRegistrationPurchase { get; set; }
        public bool RequireEmergencyContactInfo { get; set; }
        public bool RequireMedicalInfo { get; set; }
        public bool IncludeULGOption { get; set; }
        public bool CrecheAvailable { get; set; }
        public bool PreOrderAvailable { get; set; }
        public bool ElectivesAvailable { get; set; }
        public bool AccommodationAvailable { get; set; }
        public bool CateringAvailable { get; set; }
        public bool LeadershipBreakfastAvailable { get; set; }
        public decimal AccommodationCost { get; set; }
        public decimal CateringCost { get; set; }
        public decimal LeadershipBreakfastCost { get; set; }
        public Nullable<decimal> PlanetKidsMinAge { get; set; }
        public Nullable<decimal> PlanetKidsMaxAge { get; set; }
        public int BankAccount_ID { get; set; }
        public int SortOrder { get; set; }
        public string LetterHead { get; set; }
        public Nullable<System.DateTime> VolunteersManagementCutOffDate { get; set; }
        public Nullable<System.DateTime> VolunteersManagerCutOffDate { get; set; }
        public Nullable<int> VolunteersManager_ID { get; set; }
        public Nullable<System.DateTime> LastDateAllocateGroupRegistrations { get; set; }
        */

    }
}
