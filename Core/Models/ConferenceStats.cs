﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planetshakers.Core.Models
{
    public class ConferenceStats
    {
        public int RegistrationType_ID { get; set; }
        public string RegistrationTypeName { get; set; }
        public int? TotalRegistration { get; set; }
        public int? Allocated { get; set; }
        public int? Unallocated { get; set; }
        public int? Capacity { get; set; }

    }
}
