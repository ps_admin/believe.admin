﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planetshakers.Core.Models
{
    public class ConferenceStatsList
    {
        public int Conference_ID { get; set; }
        public string ConferenceName { get; set; }
        public List<ConferenceStats> StatsList { get; set; }
        public ConferenceStats AdultList { get; set; }
        public ConferenceStats BoomList { get; set; }
        public ConferenceStats KidsList { get; set; }
        public int StaffRegistration { get; set; }

    }
}
