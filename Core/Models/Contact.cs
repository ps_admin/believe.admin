﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Planetshakers.Core.Models
{
    public abstract class Contact
    {
        public int Id { get; set; }
        public string Salutation { get; set; }
        public string barcode { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Postcode { get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public string Country { get; set; }

        public string ContactNumber { get; set; }
        public string Email { get; set; }

        public DateTime? DateOfBirth { get; set; }
        public string Gender { get; set; }
        public string Status { get; set; }
        public int Carer { get; set; }
        public string CarerName { get; set; }
        public DateTime? WaterBaptismDate { get; set; }

        public DateTime DateLastComment { get; set; }
        public DateTime DateRegistered { get; set; }

        public string FullName
        {
            get { return FirstName + " " + LastName; }
        }

        public string StatusAbbr
        {
            get
            {
                switch (Status)
                {
                    case "New People":
                        return "NP";
                    case "New Christian":
                        return "NC";

                    case "Partner":
                        return "PARTNER";

                    case "Contact":
                        return "CONTACT";

                    default:
                        return "";
                }
            }
        }

        public bool Internal { get; set; }
        public bool External { get; set; }

        public string Campus { get; set; }
    }
}
