﻿using System;

namespace Planetshakers.Core.Models
{
    public class ContactAccessLevel
    {
        public int ContactId { get; set; }

        public int DbRoleId { get; set; }

        public string Name { get; set; }

        public string Module { get; set; }

        public DateTime DateAdded { get; set; }
    }
}
