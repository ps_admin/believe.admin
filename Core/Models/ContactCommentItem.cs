﻿using System;

namespace Planetshakers.Core.Models
{
    public class CommentItem
    {
        public int Id { get; set; }
        public DateTime CommentDate { get; set; }
        public String sCommentDate { get; set; }

        public int Conference_ID { get; set; }

        public int ContactId { get; set; }
        public string ContactName { get; set; }

        public string Comment { get; set; }

        public int Commenter_ID { get; set; }

        public string CommenterFirstName { get; set; }
        public string CommenterLastName { get; set; }

        public string CommenterName
        {
            get { return CommenterFirstName + " " + CommenterLastName; }
        }
    }

    public enum CommentType
    {
        Administrative = 1,
        Pastoral = 2,
        Staff = 3
    }
}
