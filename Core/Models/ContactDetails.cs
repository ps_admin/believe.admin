﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Planetshakers.Core.Models
{
    public class ContactDetails : Contact
    {
        public ContactDetails()
        {

        }

        public ContactDetails(int contact_id)
        {
            Id = contact_id;
        }

        public string Password { get; set; }
        public string MedicalInformation { get; set; }
        public string MedicalAllergies { get; set; }

        public bool IsVIP { get; set; }
        public bool IsPastor { get; set; }
    } 
}
