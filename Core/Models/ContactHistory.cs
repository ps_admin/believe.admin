﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Planetshakers.Core.Models
{
    public class ContactHistory
    {
        public int CH_ID { get; set; }
        public int Contact_ID { get; set; }

        public string Module { get; set; }
        public string Description { get; set; }
        public DateTime DateChanged { get; set; }

        public int User_ID { get; set; }
        public string User { get; set; }

        public string Description2 { get; set; }
        public string Action { get; set; }
        public string Item { get; set; }

        public string OldText { get; set; }
        public string NewText { get; set; }

    }
}
