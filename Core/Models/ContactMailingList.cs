﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planetshakers.Core.Models
{
    public class ContactMailingList
    {
        public int Contact_ID { get; set; }

        public string FullName { get; set; }

        public string Email { get; set; }

        public string Mobile { get; set; }

        public string MailingListName { get; set; }

        public string MailingListTypeName { get; set; }

        public bool MailingListSubscription { get; set; }

        public bool Internal { get; set; }

        public bool External { get; set; }

        public bool BothInternalExternal
        {
            get
            {
                return Internal && External;
            }
        }

        public string FilterType
        {
            get
            {
                if (BothInternalExternal)
                {
                    return "both";
                }
                else
                {
                    if (Internal)
                    {
                        return "internal";
                    }
                    else
                    {
                        return "external";
                    }
                }
            }
        }

    }
}
