﻿
namespace Planetshakers.Core.Models
{
    public class ContactRole
    {
        public int ContactId { get; set; }

        public int RoleId { get; set; }

        public bool IsDeleted { get; set; }

        public int RoleTypeUlId { get; set; }

        public string Name { get; set; }
    }
}
