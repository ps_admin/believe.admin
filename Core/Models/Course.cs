﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Planetshakers.Core.Models
{
    public class Course
    {

        public int Course_ID { get; set; }

        public int CourseInstance_ID { get; set; }

        public string Name { get; set; }

        public string StartDate { get; set; }

        public string Campus { get; set; }

        public bool Completed { get; set; }

        public string scost { get; set; }

        public decimal cost { get; set; }
    }
}
