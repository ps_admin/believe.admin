﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planetshakers.Core.Models
{
    public class DatabaseRolePermission
    {
        public int DatabaseRolePermissionID { get; set; }
        public int DatabaseRoleID { get; set; }
        public string RoleName { get; set; }
        public int DatabaseObjectID { get; set; }
        public string DatabaseObjectName { get; set; }
        public string DatabaseObjectDescription { get; set; }
        public bool Read { get; set; }
        public bool Write { get; set; }
    }
}
