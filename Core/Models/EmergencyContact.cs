﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Planetshakers.Core.Models
{
    public class EmergencyContact
    {
        public int Contact_ID { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Mobile { get; set; }

        public string Email { get; set; }

        public UrbanLife ULG { get; set; }

        public string Campus { get; set; }

        public string Ministry { get; set; }

        public string EmergencyContactName { get; set; }

        public string EmergencyContactPhone { get; set; }

        public string EmergencyContactRelationship { get; set; }

        public string MedicalInformation { get; set; }

        public string MedicalAllegies { get; set; }

        public string AccessibilityInformation { get; set; }
    }
}
