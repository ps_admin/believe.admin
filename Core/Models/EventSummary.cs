﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Planetshakers.Core.Models
{
    public class EventSummary
    {
        public DataSet ds { get; set; }
    }

    public class BankingReport
    {
        public DataSet ds { get; set; }
    }
}
