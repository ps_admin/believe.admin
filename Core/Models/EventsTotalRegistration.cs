﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planetshakers.Core.Models
{
    public class EventsTotalRegistration
    {
        public EventsReportRegistrationCampusBreakdown totalCampusBreakdown { get; set; }
        public EventsReportRegistrationMinistryBreakdown totalMinistryBreakdown { get; set; }
        public EventsReportRegistrationTypeTotal totalRegoTypeBreakdown { get; set; }
        public int grandTotal
        {
            get
            {
                return totalCampusBreakdown.grandTotal;
            }
        }
        public int externalAllocated { get
            {
                return totalCampusBreakdown.externalAllocated;
            }
        }
        public int externalUnallocated
        {
            get
            {
                return totalCampusBreakdown.externalUnallocated;
            }
        }

        public int Unallocated { get
            {
                return externalUnallocated + totalCampusBreakdown.subtotalUnallocated;
            }
        }
        public int ministryGrandTotal { get
            {
                return Unallocated + externalAllocated + totalMinistryBreakdown.subTotal;
            }
        }
    }
    public class EventsReportRegistrationTypeTotal
    {
        public DataSet ds { get; set; }
        public int grandTotal
        {
            get
            {
                int total = Int32.Parse(ds.Tables[0].Rows[0]["GrandTotal"].ToString());
                return total;
            }

        }
    }

    public class EventsReportRegistrationCampusBreakdown
    {
        public DataSet ds { get; set; }
        public int subtotalAllocated { get
            {
                int total = 0;
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    if (row["Campus"].ToString() != "External")
                    {
                        if (!String.IsNullOrEmpty(row["Allocated"].ToString()))
                        {
                            total += Int32.Parse(row["Allocated"].ToString());
                        }  
                    }
                    
                }
                return total;

            }

        }
        
        public int subtotalUnallocated { get
            {
                int total = 0;
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    if (row["Campus"].ToString() != "External")
                    {
                        if (!String.IsNullOrEmpty(row["Unallocated"].ToString()))
                        {
                            total += Int32.Parse(row["Unallocated"].ToString());
                        }
                            
                    }

                }
                return total;

            }
        }
        public int externalAllocated
        {
            get
            {
                int total = 0;
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    if (row["Campus"].ToString() == "External")
                    {
                        var allocated = row["Allocated"];

                        if (!String.IsNullOrEmpty(allocated.ToString()))
                        {
                            total = Int32.Parse(row["Allocated"].ToString());
                        }

                    }

                }
                return total;
            }
        }
        public int externalUnallocated
        {
            get
            {
                int total = 0;
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    if (row["Campus"].ToString() == "External")
                    {
                        if (!String.IsNullOrEmpty(row["Unallocated"].ToString()))
                        {
                            total = Int32.Parse(row["Unallocated"].ToString());
                        }  
                    }

                }
                return total;
            }
        }
        public int grandTotal { get
            {
                int total = 0;
                total += externalAllocated;
                total += externalUnallocated;
                total += subtotalAllocated;
                total += subtotalUnallocated;
                return total;

            }
        }

    }
    public class EventsReportRegistrationMinistryBreakdown
    {
        public DataSet ds { get; set; }
        public int subTotal { get
            {
                int total = 0;
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    if (!String.IsNullOrEmpty(row["Total"].ToString()))
                    {
                        total += Int32.Parse(row["Total"].ToString());
                    }  
                }
                return total;
            }
        }
    }

}
