﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Planetshakers.Core.Models
{
    public class Group
    {
        public int ID { get; set; }

        public string GroupName { get; set; }
        public string Church { get; set; }
        public string GroupColeaders { get; set; }
        public string YouthLeaderName { get; set; }
        
        public bool DetailsVerified { get; set; }

        public bool Deleted { get; set; }

        public int? GroupSizeID { get; set; }

        public int? GroupTypeID { get; set; }

        public string Comments { get; set; }
        
    }
}
