﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Planetshakers.Core.Models
{
    public class GroupDetails : Group
    {
        // default constructor
        public GroupDetails()
        {

        }

        // constructor taking 1 argument
        // group_id : ID of the group
        public GroupDetails(int group_id)
        {
            this.ID = group_id;
        }

        public string LeaderFirstName { get; set; }
        public string LeaderLastName { get; set; }

        public string LeaderName
        {
            get
            { return LeaderFirstName + " " + LeaderLastName; }
        }

        public string LeaderEmail { get; set; }
        public string LeaderMobile { get; set; }
        public string LeaderPhone { get; set; }
        public string LeaderAddress { get; set; }
        public string Gender { get; set; }
    }
}
