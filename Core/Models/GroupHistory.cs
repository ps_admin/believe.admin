﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planetshakers.Core.Models
{
    public class GroupHistory
    {
        public int GH_ID { get; set; }
        public int GroupLeader_ID { get; set; }

        public int Conference_ID { get; set; }
        public string ConferenceName { get; set; }
        public int Venue_ID { get; set; }

        public DateTime DateChanged { get; set; }
        public int User_ID { get; set; }
        public string User { get; set; }

        public string Action { get; set; }
        public string Item { get; set; }

        public int OldValue { get; set; }
        public string OldText { get; set; }
        public int NewValue { get; set; }
        public string NewText { get; set; }
    }
}
