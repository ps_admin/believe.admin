﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planetshakers.Core.Models
{
    public class GroupPayment
    {
        public int GroupPayment_ID { get; set; }
        public int GroupLeader_ID { get; set; }
        public string GroupLeader_Name { get; set; }

        public string Email { get; set; }
        public int Conference_ID { get; set; }
        public string Conference_Name { get; set; }
        public int PaymentType_ID { get; set; }
        public string PaymentType { get; set; }
        public int PaymentSource_ID { get; set; }
        public string PaymentSource { get; set; }
        public decimal PaymentAmount { get; set; }
        public string VoucherCode { get; set; }

        public string CCTransactionRef { get; set; }
        public bool CCRefund { get; set; }
        public DateTime? PaymentDate { get; set; }
        public int PaymentBy_ID { get; set; }
        public string PaymentBy_Name { get; set; }
        public int BankAccount_ID { get; set; }
        public string BankDescription { get; set; }
        public string PaymentCategory
        {
            get
            {
                return "Group";
            }
        }

        public int? VoucherID { get; set; }

        public string Comment { get; set; }

    }
}
