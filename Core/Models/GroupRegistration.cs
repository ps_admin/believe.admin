﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Planetshakers.Core.Models
{
    public class GroupRegistration
    {
        public int ID { get; set; }
        public string GroupName { get; set; }
        public string LeaderFirstName { get; set; }
        public string LeaderLastName { get; set; }

        public int Conference_ID { get; set; }
        public int Venue_ID { get; set; }

        public decimal Balance { get; set; }
        public DateTime CreationDate { get; set; }

        public int? NumRegistered { get; set; }
        public int? UnallocatedRegoCount { get; set; }

        public List<Registration> Regos { get; set; }
        public List<Purchase> GroupPurchases { get; set; }
    }
}
