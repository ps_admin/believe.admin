﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planetshakers.Core.Models
{
    public class Guest
    {
        public int ID { get; set; }
        public int Contact_ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Church { get; set; }
        public string ChurchRole { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }

        public int ConferencePriorityType_ID { get; set; }

        public int PriorityType_ID { get; set; }
        public string PriorityType { get; set; }

        public int Conference_ID { get; set; }
        public string ConferenceName { get; set; }

        public int? Registration_ID { get; set; }
        public string Password { get; set; }
        public bool Attended { get; set; }
        public string PAName { get; set; }
        public string PAMobile { get; set; }
        public string PAEmail { get; set; }
        public bool? LeadershipSummit { get; set; }
    }
}
