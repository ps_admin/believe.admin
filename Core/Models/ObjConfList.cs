﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planetshakers.Core.Models
{
    public class ObjConfList
    {
        public string Conference { get; set; }
        public int? TotalRegistrations { get; set; }
        public int Conference_ID { get; set; }
    }
}
