﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planetshakers.Core.Models
{
    public class OutstandingBalance
    {
        public string GroupSingle { get; set; }
        public int GroupRegistrant_ID { get; set; }
        public string GroupRegistrantName { get; set; }
        public decimal TotalCost { get; set; }
        public decimal TotalPayment { get; set; }
        public decimal Balance { get; set; }
    }
}
