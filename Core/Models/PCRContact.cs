﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Planetshakers.Core.Models
{
    public class PCRContact : Contact
    {
        public PCRContact()
        {
            this.PastComments = new List<CommentItem>();
        }
        
        public int PCRContactId { get; set; }        

        public int NumOfCall { get; set; }
        public int NumOfVisit { get; set; }
        public int NumOfOthers { get; set; }

        public int NumOfCallWeek { get; set; }
        public int NumOfVisitWeek { get; set; }
        public int NumOfOthersWeek { get; set; }
        
        public bool AttendUrbanLife { get; set; }
        public bool AttendSundayService { get; set; }
        public bool AttendBoom { get; set; }

        public bool HasComment
        {
            get { return DateLastComment != default(DateTime); }
        }

        public List<CommentItem> PastComments { get; set; }

        public List<EventRegistered> EventsRegistered { get; set; }

        public List<CompletedCourse> CompletedCourses { get; set; }
    }

    public class BoomPCRContact : Contact
    {
        public int PCRContactId { get; set; }
        public int ULG_ID { get; set; }

        public bool AttendUrbanLife { get; set; }
        public bool AttendSundayService { get; set; }
        public bool AttendBoom { get; set; }        
    }

}
