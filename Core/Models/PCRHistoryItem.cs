﻿using System;

namespace Planetshakers.Core.Models
{
    public class PCRHistoryItem
    {
        public int PcrDateId { get; set; }
        public DateTime ReportDate { get; set; }
        public bool IsCompleted { get; set; }
    }
}