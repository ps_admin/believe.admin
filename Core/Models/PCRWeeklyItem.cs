﻿using System;
using Planetshakers.Core.Helpers;

namespace Planetshakers.Core.Models
{
    public class PCRWeeklyItem
    {
        public int PCRId { get; set; }
        public int PCRDateId { get; set; }
        public int TotalChildren { get; set; }
        public int TotalVisitor { get; set; }
        public double TotalOffering { get; set; }
        public string Comments { get; set; }
        public bool Completed { get; set; }
        public DateTime ReportDate { get; set; }
        public bool UrbanLifeWeek { get; set; }
        public bool BoomIsOn { get; set; }

        public int TotalMember { get; set; }
        public int TotalSundayServiceAttendance { get; set; }
        public int TotalUrbanLifeAttendance { get; set; }

        /// <summary>
        /// Indicates the underlying report is the latest
        /// </summary>
        public bool LatestReport { get; set; }

        public bool DueSoon
        {
            get { return (ReportDate - DateTime.Now).TotalDays < 0; }
        }

        public string StatusText
        {
            get
            {
                if (Completed)
                    return "Submitted";

                if (DueSoon)
                    return "Due soon";

                if (LatestReport)
                    return "Current Week";

                return "Overdue";
            }
        }

        public string LabelClassName
        {
            get
            {
                if (Completed)
                    return "success";

                if (DueSoon)
                    return "alert";

                if (LatestReport)
                    return "info";

                return "alert";
            }
        }

        public string DateWeekRange
        {
            get { return ReportDate.AddDays(-6).DisplayFormattedDateWithoutYear() + " - " + ReportDate.DisplayFormattedDate(); }
        }
    }
}