﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Planetshakers.Core.Models
{
    public class Payment
    {
        public int ID { get; set;}
        public int Contact_ID { get; set; }
        public string ContactName { get; set; }
        public int Group_ID { get; set; }

        public int Conference_ID { get; set; }
        public string ConferenceName { get; set; }
        public string Category { get; set; }

        public int PaymentType_ID { get; set; }
        public string PaymentType { get; set; }

        public int PaymentSource_ID { get; set; }
        public string PaymentSource { get; set; }
        public string PaidBy { get; set; }

        public string TransactionRef { get; set; }

        public string Currency { get; set; }

        public decimal Amount { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? CompletedDate { get; set; }

        public string PaymentDate { get; set; }

        public bool? Completed { get; set; }

        public int PaymentBy_ID { get; set; }
        public int BankAccount_ID { get; set; }

        public string Comment { get; set; }

        public int User_ID { get; set; }

        public int? Voucher_ID { get; set; }
        public string VoucherCode { get; set; }

        public int? Sponsorship_ID { get; set; }
        public SponsorshipDetails sponsorship { get; set; }
    }

    public class SponsorshipDetails
    {
        public int Sponsorship_ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public bool Anonymous { get; set; }
    }
}
