﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planetshakers.Core.Models
{
    public class PreferenceCategory
    {
        public string categoryName { get; set; }
        public string categoryValue { get; set; }
    }
}
