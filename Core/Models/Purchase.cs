﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Planetshakers.Core.Models
{
    public class Purchase
    {
        public int RegistrationType_ID { get; set; }

        public string RegistrationType { get; set; }

        public int? PurchasedCount { get; set; }
        public int? AllocatedCount { get; set; }
        public int? UnallocatedCount { get; set; }
    }
}
