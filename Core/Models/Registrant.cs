﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planetshakers.Core.Models
{
    public class Registrant
    {
        public int ID { get; set; }
        public int FirstName { get; set; }
        public int LastName { get; set; }
    }
}
