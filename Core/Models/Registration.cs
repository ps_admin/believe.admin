﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Planetshakers.Core.Models
{
    public class Registration
    {
        public Registration()
        {

        }

        public int ID { get; set; }
        public int ContactID { get; set; }
        public bool Attended { get; set; }
        public string GroupName { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }

        public DateTime? DoB { get; set; }
        public int Country_ID { get; set; }

        public string PriorityType { get; set; }

       
        public int RegoType_ID { get; set; }
        public string RegoType { get; set; }
        public decimal RegoDiscount { get; set; }

        public int? VenueId { get; set; }
        public int Conference_ID { get; set; }
        public string ConferenceName { get; set; }

        public DateTime RegistrationDate { get; set; }
        public string RegoDateStr { get; set; }
        public string CustomTag { get; set; }

        public int RegisteredBy_ID { get; set; }
        public string RegisteredBy { get; set; }

        public int quantity { get; set; }
        public int unallocated { get; set; }
        public decimal OutstandingBalance { get; set; }

        public bool Volunteer { get; set; }
        public int? VolunteerDepartment_ID { get; set; }

        public bool IsGroupLeader { get; set; }

        public bool HasGroup { get; set; }
        public int? GroupLeader_ID { get; set; }
        
        public int User_ID { get; set; }
    }

    public class EventModel
    {
        public int VenueID { get; set; }
        public int ConferenceID { get; set; }
        public string ConferenceName { get; set; }
    }
    
    public class UnallocatedRegistration
    {
        public int GroupLeader_ID { get; set; }
        public string GroupLeader { get; set; }

        public int Venue_ID { get; set; }
        public int Conference_ID { get; set; }

        public int RegistrationType_ID { get; set; }
        public string RegistrationType { get; set; }

        public int Unallocated { get; set; }
        public int Allocated { get; set; }
    }

    public class RegistrationSummary
    {
        public int Conference_ID { get; set; }
        public string RegoType { get; set; }
        public string Currency { get; set; }
        public double Amount { get; set; }
        public double Paid { get; set; }
        public double Outstanding { get; set; }
    }
}
