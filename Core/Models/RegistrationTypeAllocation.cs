﻿
namespace Planetshakers.Core.Models
{
    public class RegistrationTypeAllocation
    {
        public int RegistrationType_ID { get; set; }

        public string RegistrationType { get; set; }

        public int Total { get; set; }

        public int Allocated { get; set; }

        public int Unallocated { get {
                return Total - Allocated;
            } 
        }

        public string AllocatedPercentage {
            get
            {
                if (Total == 0)
                {
                    return "n/a";
                }
                else
                {
                    double percentage = (double)Allocated / Total * 100; //Change into percentage value
                    return percentage.ToString("0.00") + "%";
                }
            }
        }
    }
}
