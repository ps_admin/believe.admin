﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Planetshakers.Core.Models
{
    public class ResultsModel
    {
        public List<string> Headers { get; set; }
        public List<SingleRegistration> SingleRego { get; set; }
        public List<GroupRegistration> GroupRego { get; set; }
    }
}
