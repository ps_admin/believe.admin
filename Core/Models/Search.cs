﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Planetshakers.Core.Models
{
    public class SearchModel
    {
        #region Enumerations
        public enum ContactRegistrationStatus
        {
            Both,
            Registered,
            Unregistered
        }

        public enum ContactBalanaceStatus
        {
            All,
            NoBalance,
            DebitCredit
        }
        #endregion

        #region Personal Details
        public string QuickSearch { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string ChurchName { get; set; }

        public string Address { get; set; }

        public string Suburb { get; set; }

        public string Postcode { get; set; }

        public int Country_ID { get; set; }

        public int State_ID { get; set; }

        public string Gender { get; set; }

        public string Email { get; set; }

        public string EmergencyContactName { get; set; }

        public string Phone { get; set; }

        public string Fax { get; set; }

        public string Mobile { get; set; }

        public string YouthLeaderName { get; set; }
        #endregion

        #region Group Details
        public int GroupLeader_ID { get; set; }

        public string GroupName { get; set; }

        public string Church { get; set; }

        public string YouthLeader { get; set; }

        public string GroupLeaderFirstName { get; set; }

        public string GroupLeaderLastName { get; set; }

        public string GroupLeaderSuburb { get; set; }

        public string GroupLeaderEmail { get; set; }

        public string GroupLeaderPhone { get; set; }

        public string GroupLeaderMobile { get; set; }

        public int GroupSize_ID { get; set; }

        public int UnallocatedRegosCount { get; set; }
        #endregion

        #region Conference Details
        public int Conference_ID { get; set; }

        public int Venue_ID { get; set; }

        public int RegistrationType_ID { get; set; }

        public DateTime? DateFrom { get; set; }

        public DateTime? DateTo { get; set; }

        public string Attended { get; set; }

        public int PaymentType_ID { get; set; }

        public int Registered { get; set; }

        public int CreditDebit { get; set; }

        public int WaitingList { get; set; }

        public int ShowInactiveRegistrants { get; set; }
        #endregion
    }

}
