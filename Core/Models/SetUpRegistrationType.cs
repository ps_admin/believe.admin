﻿using System;

namespace Planetshakers.Core.Models
{
    public class SetUpRegistrationType
    {
        public int RegistrationTypeID { get; set; }
        public int ConferenceID { get; set; }
        public string ConferenceName { get; set; }
        public string RegistrationTypeName { get; set; }
        public int? RegistrationTypeQuantityGroupID { get; set; }
        public int? RegistrationTypeReportGroupID { get; set; }
        public int? RegistrationTypeSaleGroupID { get; set; }

        public decimal RegistrationCost { get; set; }
        public bool AvailableOnlineSingle { get; set; }
        public bool AvailableOnlineGroup { get; set; }
        public int GroupMinimumQuantity { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool IsCollegeRegoType { get; set; }
        public bool ApplicationRegoType { get; set; }
        public bool AddOnRegistrationType { get; set; }
        public int SortOrder { get; set; }
        public string Currency { get; set; }
        public bool IncludeInReportGroup { get; set; }
        public bool IncludeInSaleGroup { get; set; }
        public decimal MinimumAge { get; set; }
        public decimal MaximumAge { get; set; }
        public int? Limit { get; set; }
        public bool hasFundingGoal { get; set; }
    }
}
