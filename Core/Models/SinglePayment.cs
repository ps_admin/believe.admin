﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planetshakers.Core.Models
{
    public class SinglePayment
    {
        public int ContactPayment_ID { get; set; }
        public int Contact_ID { get; set; }
        public string Contact_Name { get; set; }

        public string Email { get; set; }
        public int Conference_ID { get; set; }
        public string Conference_Name { get; set; }
        public int PaymentType_ID { get; set; }
        public string PaymentType { get; set; }
        public decimal PaymentAmount { get; set; }

        public string CCTransactionRef { get; set; }
        public bool CCRefund { get; set; }
        public DateTime PaymentDate { get; set; }
        public int PaymentBy_ID { get; set; }
        public string PaymentBy_Name { get; set; }
        public int BankAccount_ID { get; set; }
        public string BankDescription { get; set; }
        public string PaymentCategory
        {
            get
            {
                return "Single";
            }
        }
        public string Comment { get; set; }
    }
}
