﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Planetshakers.Core.Models
{
    public class SingleRegistration
    {
        public int Contact_ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Suburb { get; set; }
        public string Gender { get; set; }
        public string GroupName { get; set; }
        public bool Registered { get; set; }
    }
}
