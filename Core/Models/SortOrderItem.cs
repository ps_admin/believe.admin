﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planetshakers.Core.Models
{
    public class SortOrderItem
    {
        public int ItemID { get; set; }
        public int ItemSortOrderID { get; set; }
    }
}
