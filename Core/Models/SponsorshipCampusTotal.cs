﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planetshakers.Core.Models
{
    public class SponsorshipCampusTotal
    {
        public string Campus { get; set; }

        public decimal Donor { get; set; }

        public decimal Recipient { get; set; }
    }
}
