﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planetshakers.Core.Models
{
    public class SponsorshipCartItem
    {
        public int event_id { get; set; }

        // event venue
        public int event_venue_id { get; set; }

        // event name
        public string event_name { get; set; }

        // total sponsorship amount
        public decimal totalSponsoredAmount { get; set; }
    }
}
