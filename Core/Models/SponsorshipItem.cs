﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planetshakers.Core.Models
{
    public class SponsorshipItem
    {
        public int VariablePaymentID { get; set; }
        public int Contact_ID { get; set; }
        public int Conference_ID { get; set; }
        public string Currency { get; set; }
        public string FullName { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public bool? Anonymous { get; set; }
        public bool Donor { get; set; }
        public bool Recipient { get; set; }
        public int PaymentType_ID { get; set; }
        public string PaymentType { get; set; }
        public decimal PaymentAmount { get; set; }
        public string CCTransactionRef { get; set; }
        public bool Refund { get; set; }
        public string Comment { get; set; }
        public DateTime PaymentDate { get; set; }
        public int PaymentByID { get; set; }
        public string PaymentByName { get; set; }
        public string PaymentByEmail { get; set; }
        public string PaymentByMobile { get; set; }
        public string Campus { get; set; }
        public string Ministry { get; set; }


        //For Transfer Sponsorship Use
        public decimal TransferAmount { get; set; }
    }
}
