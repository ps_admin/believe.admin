﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planetshakers.Core.Models
{
    public class SponsorshipMinistryTotal
    {
        public string Ministry { get; set; }

        public decimal Donor { get; set; }

        public decimal Recipient { get; set; }
    }
}
