﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planetshakers.Core.Models
{
    public class SponsorshipPaymentTypeTotal
    {
        public string PaymentType { get; set; }

        public decimal DonorAmount { get; set; }

        public decimal RecipientAmount { get; set; }
    }
}
