﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planetshakers.Core.Models
{
    public class TicketModel
    {
        public TicketModel()
        {

        }

        public TicketModel(string firstname, string email)
        {
            this.Tickets = new List<Ticket>();
            this.FirstName = firstname;
            this.Email = email;
        }

        public List<Ticket> Tickets { get; set; }

        public int ID { get; set; }
        public string FirstName { get; set; }
        public string Email { get; set; }
    }
    public class TicketRegistration
    {
        public int GroupBulkRegistration_ID { get; set; }
        public int GroupTicket_ID { get; set; }

        public int Group_ID { get; set; }
        public string GroupName { get; set; }

        public int Venue_ID { get; set; }

        public string EventName { get; set; }

        public int RegistrationType_ID { get; set; }
        public string RegistrationType { get; set; }
        public int Qty { get; set; }

        public bool Attended { get; set; }
    }

    public class SGConcert2020
    {
        [JsonProperty("FirstName")]
        public string FirstName { get; set; }
    }

    public class Ticket
    {
        public int reg_barcode { get; set; }
    }
}
