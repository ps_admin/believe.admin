﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planetshakers.Core.Models
{
    public class TimeSelection
    {
        public int dateId { get; set; }
        public DateTime startdate { get; set; }
        public DateTime enddate { get; set; }
        public string selectionname { get; set; }

    }
}
