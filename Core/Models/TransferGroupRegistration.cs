﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planetshakers.Core.Models
{
    public class TransferGroupRegistration
    {
        public int RegistrationType_ID { get; set; }

        public string RegistrationType { get; set; }

        public int TransferQty { get; set; }

        public int UnallocatedQty { get; set; }
    }
}
