﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Planetshakers.Core.Models
{
    public class WelcomeCard
    {
        public int Salutation { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string Gender { get; set; }

        public string Address { get; set; }
        public string Suburb { get; set; }
        public int State { get; set; }
        public int PostCode { get; set; }
        public int Country { get; set; }

        public string Email { get; set; }
        public string Mobile { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public string CameWith { get; set; }
        public string Outcome { get; set; }
        public int Ministry { get; set; }
        public string MinistryOther { get; set; }

        public int Campus { get; set; }        
        public string Comments { get; set; }

        public int AddedBy { get; set; }
        public DateTime AddedDate { get; set; }
    }
}
