﻿Public Module MCreditCard

    Private moCreditCard As New CCreditCard

    Public Function ProcessCreditCard(ByVal sCard As String, ByVal sExpiry As String, ByVal sCVC2 As String, ByVal sName As String, ByVal dAmount As Decimal, ByVal sComment As String, ByVal sBankAccountClientID As String, ByVal sBankAccountCertificateName As String, ByVal sBankAccountCertificatePassPhrase As String, ByRef sTxRef As String, ByRef sResponse As String) As Boolean
        Return moCreditCard.ProcessCreditCard(sCard, sExpiry, sCVC2, sName, dAmount, sComment, sBankAccountClientID, sBankAccountCertificateName, sBankAccountCertificatePassPhrase, sTxRef, sResponse)
    End Function

    Public Function ProcessRefund(ByVal sName As String, ByVal sCCTransactionRef As String, ByVal dAmount As Decimal, ByVal sComment As String, ByVal sBankAccountClientID As String, ByVal sBankAccountCertificateName As String, ByVal sBankAccountCertificatePassPhrase As String, ByRef sTxRef As String, ByRef sResponse As String) As Boolean
        Return moCreditCard.ProcessRefund(sName, sCCTransactionRef, dAmount, sComment, sBankAccountClientID, sBankAccountCertificateName, sBankAccountCertificatePassPhrase, sTxRef, sResponse)
    End Function

End Module
