﻿using System;
using System.Data.SqlClient;

namespace Planetshakers.Data.SqlServer.Helpers
{
    public static class SqlDataReaderExtension
    {
        public static T Get<T>(this SqlDataReader dr, string name)
        {
            if (dr == null)
                return default(T);

            var output = dr[name];
            if (output == null)
                return default(T);

            if (output == DBNull.Value)
                return default(T);

            return (T)Convert.ChangeType(output, typeof(T));
        }

        public static TEnum GetEnum<TEnum>(this SqlDataReader dr, string name) where TEnum : struct
        {
            if (dr == null)
                return default(TEnum);

            var output = dr[name];
            if (output == null)
                return default(TEnum);

            TEnum enumObj;
            Enum.TryParse(output.ToString(), true, out enumObj);
            return enumObj;
        }
    }
}