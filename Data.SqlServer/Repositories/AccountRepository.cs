﻿using Planetshakers.Core.DataInterfaces;
using Planetshakers.Core.Models;
using Planetshakers.Data.SqlServer.Helpers;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace Planetshakers.Data.SqlServer.Repositories
{
    public class AccountRepository : BaseRepository, IAccountRepository
    {
        public AccountRepository(string connectionString) : base(connectionString)
        {
        }

        public Account Login(string email, string password)
        {
            Account account = null;

            ExecuteReader(
                //"dbo.usp_Account_Login",
                "dbo.sp_MOBILEPCR_AccountLogin",
                new[] {
                    new SqlParameter("@Email", email),
                    new SqlParameter("@Password", password),
                },
                (dr =>
                {
                    if (dr.Read())
                    {
                        account = new Account
                        {
                            ContactId = dr.Get<int>("ContactId"),
                            Barcode = dr.Get<string>("Barcode"),
                            FirstName = dr.Get<string>("FirstName"),
                            LastName = dr.Get<string>("LastName"),
                            PasswordResetRequired = dr.Get<bool>("PasswordResetRequired"),
                            Email = email,
                            PastorOfRegionId = dr.Get<int>("PastorOfRegionId")
                        };
                    }
                }));

            return account;
        }

        public Account GPLogin(string email)
        {
            Account account = null;

            ExecuteReader(
                //"dbo.usp_Account_Login",
                "dbo.sp_GPMOBILEPCR_AccountLogin",
                new[] {
                    new SqlParameter("@Email", email), 
                  //new SqlParameter("@Password", password), 
                },
                (dr =>
                {
                    if (dr.Read())
                    {
                        account = new Account
                        {
                            ContactId = dr.Get<int>("ContactId"),
                            FirstName = dr.Get<string>("FirstName"),
                            LastName = dr.Get<string>("LastName"),
                            Email = email,
                            PastorOfRegionId = dr.Get<int>("PastorOfRegionId")
                        };
                    }
                }));

            return account;
        }

        public List<Course> GetAvailable(int iContact_id)
        {
            List<Course> course = new List<Course>();
            Course acourse = null;

            ExecuteReader(
                "dbo.sp_Church_GetAllowedCourses",
                new[] {
                    new SqlParameter("@Contact_id", iContact_id.ToString()),
                },
                (dr =>
                {
                    while (dr.Read())
                    {
                        acourse = new Course
                        {
                            CourseInstance_ID = dr.Get<int>("CourseInstance_ID"),
                            Course_ID = dr.Get<int>("Course_ID"),
                            Name = dr.Get<string>("CourseName"),
                            StartDate = dr.Get<string>("CourseInstanceName"),
                            Campus = dr.Get<string>("ShortName"),
                        };
                        course.Add(acourse);
                    }
                }));

            return course;
        }

        public List<int> GetAllUrbanLifeIdsByRegionId(List<int> regionIds)
        {
            var urbanLifeIds = new List<int>();

            ExecuteReaderBySqlViewObject(
                "SELECT * FROM dbo.Church_ULG WHERE Region_ID = " + regionIds.First() + " AND Inactive = 0",
                (dr =>
                {
                    while (dr.Read())
                    {
                        urbanLifeIds.Add(dr.Get<int>("ULG_ID"));
                    }
                }));

            return urbanLifeIds.Distinct().ToList();
        }

        public IList<RegionalUrbanLife> GetAllRegionByContactId(int contactId)
        {
            var regionalUrbanLife = new List<RegionalUrbanLife>();

            var spName = "dbo.sp_MOBILEPCR_GetRegionByContactId";

            ExecuteReader(
                spName,
                new[] { new SqlParameter("@ContactId", contactId) },
                (dr =>
                {
                    while (dr.Read())
                    {
                        var temp = new RegionalUrbanLife
                        {
                            ContactId = dr.Get<int>("Contact_ID"),
                            RegionId = dr.Get<int>("Region_ID"),
                            RoleId = dr.Get<int>("Role_ID")
                        };

                        regionalUrbanLife.Add(temp);
                    }
                }));

            return regionalUrbanLife;
        }

        public IList<RestrictedAccessUrbanLife> GetAllRestrictedAccessUrbanLifeGroupByContactId(int contactId)
        {
            var restrictedAccessUrbanLife = new List<RestrictedAccessUrbanLife>();

            // Inactive Urban Life and Contact will be filtered out.
            //var spName = "dbo.sp_GetUrbanLifeByContactId";
            var spName = "SELECT * FROM dbo.vw_mobilePCRroles where Contact_ID = "  + contactId;

            // Be mindful that the below will return duplicate Urban Life Group ID.
            // The reason being is that the same Urban Life Group ID will 
            // contain different values in its' own properties.
            ExecuteReaderBySqlViewObject(
                spName,
                (dr =>
                {
                    while (dr.Read())
                    {
                        var temp = new RestrictedAccessUrbanLife
                        {
                            RestrictedAccessULGId = dr.Get<int>("RestrictedAccessULG_ID"),
                            ContactRoleId = dr.Get<int>("ContactRole_ID"),
                            ULGId = dr.Get<int>("ULG_ID"),
                            ContactId = dr.Get<int>("Contact_ID"),
                            RoleID = dr.Get<int>("Role_ID"),
                            CampusId = dr.Get<int>("Campus_ID"),
                            DateAdded = dr.Get<string>("DateAdded")
                        };

                        restrictedAccessUrbanLife.Add(temp);
                    }
                }));

            return restrictedAccessUrbanLife.Where(ra => ra.ContactId == contactId).ToList();
        }

        public IList<UrbanLife> GetUrbanLifeGroupsById(List<int> urbanLifeGroupIds)
        {
            var urbanLifeGroups = new List<UrbanLife>();

            if (urbanLifeGroupIds.Any())
            {
                var spName = "dbo.sp_MOBILEPCR_UrbanLife_GetByUrbanLifeGroupId";

                foreach (var ulgId in urbanLifeGroupIds)
                {
                    ExecuteReader(
                        spName,
                        new[] { new SqlParameter("@UrbanLifeGroupId", ulgId) },
                        (dr =>
                        {
                            while (dr.Read())
                            {
                                var urbanLife = new UrbanLife
                                {
                                    Id = dr.Get<int>("ID"),
                                    Code = dr["Code"].ToString(),
                                    Name = dr["Name"].ToString(),
                                    AddressLine1 = dr["Address1"].ToString(),
                                    AddressLine2 = dr["Address2"].ToString(),
                                    Suburb = dr["Suburb"].ToString(),
                                    Postcode = dr["Postcode"].ToString(),
                                    Region = dr["Region"].ToString(),
                                    Ministry = dr["Ministry"].ToString(),
                                    Campus = dr["Campus"].ToString(),
                                    MeetingDay = dr["MeetingDay"].ToString(),
                                    MeetingTime = dr["MeetingTime"].ToString()
                                };

                                urbanLifeGroups.Add(urbanLife);
                            }
                        }));
                }
            }

            return urbanLifeGroups;
        }

        public IList<UrbanLife> GetUrbanLifeGroups(int contactId, int regionId)
        {
            var urbanLifeGroups = new List<UrbanLife>();

            var spName = (regionId > 0) ? "dbo.usp_UrbanLife_GetByRegionId" : "dbo.usp_UrbanLife_GetByContactId";
            var sqlParam = (regionId > 0) ? new SqlParameter("@RegionId", regionId) : new SqlParameter("@ContactId", contactId);

            ExecuteReader(
                spName,
                new[] { sqlParam },
                (dr =>
                {
                    while (dr.Read())
                    {
                        var urbanLife = new UrbanLife
                        {
                            Id = dr.Get<int>("ID"),
                            Code = dr["Code"].ToString(),
                            Name = dr["Name"].ToString(),
                            AddressLine1 = dr["Address1"].ToString(),
                            AddressLine2 = dr["Address2"].ToString(),
                            Suburb = dr["Suburb"].ToString(),
                            Postcode = dr["Postcode"].ToString(),
                            Region = dr["Region"].ToString(),
                            Ministry = dr["Ministry"].ToString(),
                            Campus = dr["Campus"].ToString(),
                            MeetingDay = dr["MeetingDay"].ToString(),
                            MeetingTime = dr["MeetingTime"].ToString()
                        };

                        urbanLifeGroups.Add(urbanLife);
                    }
                }));

            return urbanLifeGroups;
        }

        public IList<UrbanLife> GetUrbanLifeMembersByContactId(int contactId)
        {
            throw new NotImplementedException();
        }

        public IList<RoleItem> GetUserRoles(int contactId)
        {
            IList<RoleItem> roles = new List<RoleItem>();

            ExecuteReader(
                "dbo.sp_Account_GetRoles",
                new[] {
                    new SqlParameter("@ContactID", contactId)
                },
                (dr =>
                {
                    while (dr.Read())
                    {
                        roles.Add(new RoleItem
                        {
                            Id = dr.Get<int>("RoleID"),
                            Module = dr.Get<string>("RoleModule"),
                            Name = dr.Get<string>("RoleName")
                        });
                    }
                }));

            return roles;
        }


        /***************************************** Mobile PCR ************************************************/

        public IList<ContactRole> GetContactRoles(int contactId)
        {
            var contactRoles = new List<ContactRole>();

            ExecuteReader(
                "dbo.sp_MOBILEPCR_GetContactRoles",
                new[]
                {
                    new SqlParameter("@ContactID", contactId)
                },
                (dr =>
                {
                    while (dr.Read())
                    {
                        contactRoles.Add(new ContactRole
                        {
                            ContactId = dr.Get<int>("ContactId"),
                            RoleId = dr.Get<int>("RoleId"),
                            IsDeleted = dr.Get<bool>("IsDeleted"),
                            RoleTypeUlId = dr.Get<int>("RoleTypeUlId"),
                            Name = dr.Get<string>("Name")
                        });
                    }
                }));

            return contactRoles;
        }

        public IList<CarerAccessDetail> GetCarerAccessDetails(int carerId)
        {
            var carerAccessDetails = new List<CarerAccessDetail>();

            ExecuteReader(
               "dbo.sp_MOBILEPCR_CarersAccess_GetContactsByCarerId",
               new[]
                {
                    new SqlParameter("@CarerId", carerId)
                },
               (dr =>
               {
                   while (dr.Read())
                   {
                       carerAccessDetails.Add(new CarerAccessDetail
                       {
                           CarerAccessId = dr.Get<int>("CarerAccessId"),
                           ContactId = dr.Get<int>("ContactId"),
                           CarerId = dr.Get<int>("CarerId"),
                           DateAllocated = dr.Get<DateTime>("DateAllocated"),
                           DateInactive = dr.Get<DateTime>("DateInactive"),
                           Inactive = dr.Get<bool>("Inactive"),
                           UrbanLifeGroupContactId = dr.Get<int>("UrbanLifeGroupContactId"),
                           UrbanLifeId = dr.Get<int>("UrbanLifeId"),
                           UrbanLifeCode = dr.Get<string>("UrbanLifeCode"),
                           UrbanLifeName = dr.Get<string>("UrbanLifeName"),
                           FirstName = dr.Get<string>("FirstName"),
                           LastName = dr.Get<string>("LastName"),
                           Mobile = dr.Get<string>("Mobile"),
                           Address1 = dr.Get<string>("Address1"),
                           Address2 = dr.Get<string>("Address2")
                       });
                   }
               }));

            return carerAccessDetails;
        }

        public IList<CareForDetail> GetCareForDetails(int carerId)
        {
            var careForDetails = new List<CareForDetail>();

            ExecuteReader(
               "dbo.sp_MOBILEPCR_CarersAccess_GetContactsByCarerId",
               new[]
                {
                    new SqlParameter("@CarerId", carerId)
                },
               (dr =>
               {
                   while (dr.Read())
                   {
                       careForDetails.Add(new CareForDetail
                       {
                           CarerAccessId = dr.Get<int>("CarerAccessId"),
                           ContactId = dr.Get<int>("ContactId"),
                           CarerId = dr.Get<int>("CarerId"),
                           DateAllocated = dr.Get<DateTime>("DateAllocated"),
                           DateInactive = dr.Get<DateTime>("DateInactive"),
                           Inactive = dr.Get<bool>("Inactive"),
                           UrbanLifeGroupContactId = dr.Get<int>("UrbanLifeGroupContactId"),
                           UrbanLifeId = dr.Get<int>("UrbanLifeId"),
                           UrbanLifeCode = dr.Get<string>("UrbanLifeCode"),
                           UrbanLifeName = dr.Get<string>("UrbanLifeName"),
                           FirstName = dr.Get<string>("FirstName"),
                           LastName = dr.Get<string>("LastName"),
                           Mobile = dr.Get<string>("Mobile"),
                           Address1 = dr.Get<string>("Address1"),
                           Address2 = dr.Get<string>("Address2")
                       });
                   }
               }));

            return careForDetails;
        }
        public List<ULMember> GetULGMembers(int ULG_ID)
        {
            var members = new List<ULMember>();
            ExecuteReader(
               "dbo.sp_Church_ULGMemberList",
               new[]
                {
                    new SqlParameter("@ULG_ID", ULG_ID)
                },
               (dr =>
               {
                   while (dr.Read())
                   {
                       ULMember member = new ULMember
                       {
                           ID = dr.Get<int>("Contact_ID"),
                           First_Name = dr["FirstName"].ToString(),
                           Last_Name = dr["LastName"].ToString(),
                           Name = dr["FirstName"].ToString() + " " + dr["LastName"].ToString(),
                           Mobile = dr["Mobile"].ToString(),
                           Status = dr["Status"].ToString(),
                           LastCommentMade = dr.Get<DateTime>("LastCommentMade"),
                           Carers = dr["carerlist"].ToString()
                       };
                       members.Add(member);

                   }
               }));

            return members.ToList();

        }

        public List<ULMember> GetCarer(int ULG_ID)
        {
            var members = new List<ULMember>();
            ExecuteReader(
               "dbo.sp_Church_ULGCarerList",
               new[]
               {
                    new SqlParameter("@ULG_ID", ULG_ID),
                    new SqlParameter("@NPNCType_ID", 1),
                    new SqlParameter("@Ministry_ID", 2)
               },
               (dr =>
               {
                   while (dr.Read())
                   {
                       members.Add(new ULMember
                       {
                           ID = dr.Get<int>("Contact_ID"),
                           Name = dr.Get<string>("Name")
                       });
                   }
               }));

            return members;
        }

        public List<ULMember> GetNCCarer(int ULG_ID)
        {
            var members = new List<ULMember>();
            ExecuteReader(
               "dbo.sp_Church_ULGNCcarerList",
               new[]
               {
                    new SqlParameter("@ULG_ID", ULG_ID),
                    new SqlParameter("@NPNCType_ID", 1),
                    new SqlParameter("@Ministry_ID", 2)
               },
               (dr =>
               {
                   while (dr.Read())
                   {                        
                       members.Add(new ULMember
                           {
                               ID = dr.Get<int>("Contact_ID"),
                               Name = dr.Get<string>("Name")
                           });                                             
                   }
               }));

            return members;
        }

        public List<ULMember> GetCarerByContactID(int Contact_ID)
        {
            var members = new List<ULMember>();
            ExecuteReader(
               "dbo.sp_PCR_GetCarerByContactId",
               new[]
               {
                    new SqlParameter("@ContactId", Contact_ID)
               },
               (dr =>
               {
                   while (dr.Read())
                   {
                       members.Add(new ULMember
                       {
                           ID = dr.Get<int>("CarerId"),
                           First_Name = dr.Get<string>("FirstName"),
                           Last_Name = dr.Get<string>("LastName"),
                           Mobile = dr.Get<string>("Mobile")
                       });
                   }
               }));

            return members;
        }

        public List<ULMember> GetContactByCarerID(int Carer_ID)
        {
            var members = new List<ULMember>();
            ExecuteReader(
               "dbo.sp_PCR_GetContactsByCarerId",
               new[]
               {
                    new SqlParameter("@CarerId", Carer_ID)
               },
               (dr =>
               {
                   while (dr.Read())
                   {
                       members.Add(new ULMember
                       {
                           ID = dr.Get<int>("ContactId"),
                           First_Name = dr.Get<string>("FirstName"),
                           Last_Name = dr.Get<string>("LastName"),
                           Mobile = dr.Get<string>("Mobile"),
                           Status = dr.Get<string>("Status")
                       });
                   }
               }));

            return members;
        }

    }
}
