﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Planetshakers.Core.DataInterfaces;
using Planetshakers.Core.Models;
using Planetshakers.Data.SqlServer.Helpers;

namespace Planetshakers.Data.SqlServer.Repositories
{
    public class AdministrationRepository : BaseRepository, IAdministrationRepository
    {
        public AdministrationRepository(string connectionString)
            : base(connectionString)
        {
        }

        public void insertWelcome(WelcomeCard a)
        {

            ExecuteReader(
               "dbo.sp_Church_ContactWelcomeCardInsert",
               new[] { new SqlParameter("@Salutation", a.Salutation),
                        new SqlParameter("@FirstName", a.FirstName),
                        new SqlParameter("@LastName", a.LastName),
                        new SqlParameter("@Gender", a.Gender),
                        new SqlParameter("@Address", a.Address),
                        new SqlParameter("@Suburb", a.Suburb),
                        new SqlParameter("@State", a.State),
                        new SqlParameter("@PostCode", a.PostCode),
                        new SqlParameter("@Country", a.Country),
                        new SqlParameter("@Email", a.Email),
                        new SqlParameter("@Mobile", a.Mobile),
                        new SqlParameter("@DateOfBirth", a.DateOfBirth),
                        new SqlParameter("@CameWith", a.CameWith),
                        new SqlParameter("@Outcome", a.Outcome),
                        new SqlParameter("@Ministry", a.Ministry),
                        new SqlParameter("@Comments", a.Comments),
                        new SqlParameter("@AddedBy", a.AddedBy),
                        new SqlParameter("@AddedDate", a.AddedDate),
                        new SqlParameter("@Campus", a.Campus)
                        },
               (ma =>
               {

               }));
        }

        public Tuple<List<SponsorshipCampusTotal>, List<SponsorshipMinistryTotal>> SponsorshipCampusMinistryBreakdown(int Conference_ID)
        {
            List<SponsorshipCampusTotal> CampusBreakdown = new List<SponsorshipCampusTotal>();
            List<SponsorshipMinistryTotal> MinistryBreakdown = new List<SponsorshipMinistryTotal>();

            ExecuteReader(
                "dbo.sp_Events_VariablePaymentTotalCampusMinistryBreakdown",
                new[] { new SqlParameter("@Conference_ID", Conference_ID) },
                (dr =>
                {
                    while (dr.Read())
                    {
                        var campustotal = new SponsorshipCampusTotal()
                        {
                            Campus = dr.Get<string>("Campus"),
                            Donor = dr.Get<decimal>("Incoming"),
                            Recipient = dr.Get<decimal>("Outgoing")
                        };
                        CampusBreakdown.Add(campustotal);
                    }
                    if (dr.NextResult())
                    {
                        while (dr.Read())
                        {
                            var ministrytotal = new SponsorshipMinistryTotal()
                            {
                                Ministry = dr.Get<string>("Ministry"),
                                Donor = dr.Get<decimal>("Incoming"),
                                Recipient = dr.Get<decimal>("Outgoing")
                            };
                            MinistryBreakdown.Add(ministrytotal);
                        }
                    }
                }));

            var merged = new Tuple<List<SponsorshipCampusTotal>, List<SponsorshipMinistryTotal>>(CampusBreakdown, MinistryBreakdown);
            return merged;
        }

        public List<SponsorshipPaymentTypeTotal> SponsorshipPaymentTypeBreakdown(int Conference_ID)
        {
            List<SponsorshipPaymentTypeTotal> paymentTypeTotal = new List<SponsorshipPaymentTypeTotal>();

            ExecuteReader(
                "dbo.sp_Events_VariablePaymentPaymentTypeBreakdown",
                new[] { new SqlParameter("@Conference_ID", Conference_ID) },
                (dr =>
                {
                    while (dr.Read())
                    {
                        var paymentTotal = new SponsorshipPaymentTypeTotal()
                        {
                            PaymentType = dr.Get<string>("PaymentType"),
                            DonorAmount = dr.Get<decimal>("Incoming"),
                            RecipientAmount = dr.Get<decimal>("Outgoing")
                        };
                        paymentTypeTotal.Add(paymentTotal);
                    }
                    
                }));

            return paymentTypeTotal;
        }
    }
}
