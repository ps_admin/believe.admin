﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace Planetshakers.Data.SqlServer.Repositories
{
    public abstract class BaseRepository
    {
        private readonly string _connectionString;

        protected BaseRepository(string connectionString)
        {
            this._connectionString = connectionString;
        }

        protected void ExecuteNonQuery(string storeProcedure, SqlParameter[] sqlparams)
        {
            using (var cn = new SqlConnection(_connectionString))
            {
                using (var cmd = cn.CreateCommand())
                {
                    cmd.CommandText = storeProcedure;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddRange(sqlparams);

                    cn.Open();

                    cmd.ExecuteNonQuery();
                }
            }
        }

        protected void ExecuteReader(string storeProcedure, SqlParameter[] sqlparams, Action<SqlDataReader> func)
        {
            using (var cn = new SqlConnection(_connectionString))
            {
                using (var cmd = cn.CreateCommand())
                {
                    cmd.CommandText = storeProcedure;
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddRange(sqlparams);

                    cn.Open();

                    using (var dr = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        func(dr);
                    }
                }
            }
        }

        protected void ExecuteReaderBySqlViewObject(string storeProcedure, Action<SqlDataReader> func)
        {
            using (var cn = new SqlConnection(_connectionString))
            {
                using (var cmd = cn.CreateCommand())
                {
                    cmd.CommandText = storeProcedure;
                    cmd.CommandType = CommandType.Text;

                    cn.Open();

                    using (var dr = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        func(dr);
                    }
                }
            }
        }
    }
}