﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Planetshakers.Core.DataInterfaces;
using Planetshakers.Core.Models;

namespace Planetshakers.Data.SqlServer.Repositories
{
    public class CheckInRepository : BaseRepository, ICheckInRepository
    {

        public CheckInRepository(string connectionString)
            : base(connectionString)
        {
        }

        #region Conference

        public Registration FindById(int id, int venue_id, int conference_id)
        {
            Registration registration = null;
            
            ExecuteReader(
                "dbo.sp_Events_CheckIn",
                new[] { new SqlParameter("@Reg_ID", id),
                        new SqlParameter("@Venue_ID", venue_id),
                        new SqlParameter("@Conference_ID", conference_id)
                      },
                (dr =>
                {
                    if (dr.Read())
                    {
                        registration = new Registration
                        {
                            ID = Convert.ToInt32(dr["ID"]),
                            ContactID = Convert.ToInt32(dr["CID"]),
                            Attended = Convert.ToBoolean(dr["Attended"]),
                            GroupName = dr["GroupName"].ToString(),
                            FirstName = dr["FirstName"].ToString(),
                            LastName = dr["LastName"].ToString(),
                            Mobile = dr["Mobile"].ToString(),
                            Email = dr["Email"].ToString(),
                            RegoType = dr["RegistrationType"].ToString(),
                            ConferenceName = dr["ConferenceName"].ToString(),
                            RegistrationDate = DateTime.Parse(dr["RegistrationDate"].ToString())
                        };                                              

                    }

                }));

            return registration;
        }

        public List<Registration> FindByGroup (int group_id, int venue_id, int conference_id)
        {
            List<Registration> registration_list = new List<Registration>();

            ExecuteReader(
                "dbo.sp_Events_GroupCheckIn",
                new[] { new SqlParameter("@GroupLeader_ID", group_id),
                        new SqlParameter("@Venue_ID", venue_id),
                        new SqlParameter("@Conference_ID", conference_id)
                      },
                (dr =>
                {
                    while (dr.Read())
                    {
                        registration_list.Add(new Registration
                        {
                            ID = Convert.ToInt32(dr["ID"]),
                            ContactID = Convert.ToInt32(dr["CID"]),
                            Attended = Convert.ToBoolean(dr["Attended"]),
                            GroupName = dr["GroupName"].ToString(),
                            FirstName = dr["FirstName"].ToString(),
                            LastName = dr["LastName"].ToString(),
                            Mobile = dr["Mobile"].ToString(),
                            Email = dr["Email"].ToString(),
                            RegoType = dr["RegistrationType"].ToString(),
                            ConferenceName = dr["ConferenceName"].ToString(),
                            RegistrationDate = DateTime.Parse(dr["RegistrationDate"].ToString())
                        });  
                    }
                }));

            return registration_list;
        }

        public void MarkById(int id)
        {
            Registration registration = null;

            ExecuteReader(
                "dbo.sp_Events_MarkAttendance",
                new[] { new SqlParameter("@Reg_ID", id) },
                (dr =>
                {
                    if (dr.Read())
                    {
                        registration = new Registration
                        {
                            Attended = true
                        };
                    }

                }));
        }

        public void InsertRegistration(int contact_id, int venue_id, int registrationtype_id)
        {
            ExecuteReader(
               "dbo.sp_Events_RegistrationInsert",
               new[] { new SqlParameter("@Contact_ID", contact_id),
                        new SqlParameter("@GroupLeader_ID", contact_id),
                        new SqlParameter("@FamilyRegistration", false),
                        new SqlParameter("@Venue_ID", venue_id),
                        new SqlParameter("@RegistrationType_ID", registrationtype_id),
                        new SqlParameter("@RegistrationDiscount", 0.00),
                        new SqlParameter("@Accommodation", false),
                        new SqlParameter("@Catering", false),
                        new SqlParameter("@CateringNeeds", ""),
                        new SqlParameter("@LeadershipBreakfast", false),
                        new SqlParameter("@ParentGuardian", ""),
                        new SqlParameter("@ParentGuardianPhone", ""),
                        new SqlParameter("@AcceptTermsRego", true),
                        new SqlParameter("@AcceptTermsParent", false),
                        new SqlParameter("@AcceptTermsCreche", false),
                        new SqlParameter("@RegistrationDate", DateTime.Now),
                        new SqlParameter("@RegisteredBy_ID", contact_id),
                        new SqlParameter("@Attended", false),
                        new SqlParameter("@Volunteer", false),
                        new SqlParameter("@User_ID", contact_id)
                     },
               (ma =>
               {

               }));
        }

        public decimal GetOutstandingBalance(int contact_id, int conference_id)
        {
            decimal balance = 0;

            ExecuteReader("dbo.sp_Events_OutstandingBalanceGroup",
                new[] {
                    new SqlParameter("@Conference_ID", conference_id),
                    new SqlParameter("@Contact_ID", contact_id)
                },
                (o => {
                    while (o.Read())
                    {
                        balance = Convert.ToDecimal(o["Balance"]);
                    }
                }));

            if (balance == 0)
            {
                ExecuteReader("dbo.sp_Events_OutstandingBalanceSingle",
                    new[] {
                        new SqlParameter("@Conference_ID", conference_id),
                        new SqlParameter("@Contact_ID", contact_id)
                    },
                    (o =>
                    {
                        while (o.Read())
                        {
                            balance = Convert.ToDecimal(o["Balance"]);
                        }
                    }));
            }

            return balance;
        }

        #endregion
    }
}
