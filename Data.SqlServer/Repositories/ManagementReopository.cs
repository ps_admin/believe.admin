﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Planetshakers.Core.DataInterfaces;
using Planetshakers.Core.Models;
using Planetshakers.Data.SqlServer.Helpers;

namespace Planetshakers.Data.SqlServer.Repositories
{
    public class ManagementRepository : BaseRepository, IManagementRepository
    {
        public ManagementRepository(string connectionString)
            : base(connectionString)
        {
        }

        public List<AccessLevelPermission> GetContactAccessLevelPermission(int Contact_ID)
        {
            List<AccessLevelPermission> permissionList = new List<AccessLevelPermission>();

            ExecuteReader(
                "dbo.sp_AggregateEventsPermissions_Read",
                new[] { new SqlParameter("@Contact_ID", Contact_ID) },
                (dr =>
                {
                    while (dr.Read())
                    {
                        var newperm = new AccessLevelPermission()
                        {
                            DbObjID = dr.Get<int>("DatabaseObject_ID"),
                            DbObjName = dr.Get<string>("DatabaseObjectName"),
                            DbObjText = dr.Get<string>("DatabaseObjectText"),
                            RoleRead = dr.Get<bool>("RoleRead"),
                            RoleWrite = dr.Get<bool>("RoleWrite"),
                            CustomRead = dr.Get<bool>("CustomRead"),
                            CustomWrite = dr.Get<bool>("CustomWrite")
                        };
                        newperm.Contact_ID = Contact_ID;
                        permissionList.Add(newperm);
                    }
                }));

            return permissionList;
        }
        

    }
}
