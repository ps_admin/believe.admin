﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Planetshakers.Core.DataInterfaces;
using Planetshakers.Core.Exceptions;
using Planetshakers.Core.Models;
using Planetshakers.Data.SqlServer.Helpers;

namespace Planetshakers.Data.SqlServer.Repositories
{
    public class PastoralCareRepository : BaseRepository, IPastoralCareRepository
    {
        public PastoralCareRepository(string connectionString)
            : base(connectionString)
        {
        }

        public PCRContact FindPCRContactById(int contactId, int ulgid)
        {
            PCRContact contact = null;

            ExecuteReader(
                "dbo.sp_PCR_GetContactDetails",
                new[] { new SqlParameter("@ContactID", contactId),
                        new SqlParameter("@ULG_ID",ulgid)},
                (dr =>
                {
                    if (dr.Read())
                    {
                        contact = new PCRContact
                        {
                            Id = dr.Get<int>("ID"),
                            FirstName = dr["FirstName"].ToString(),
                            LastName = dr["LastName"].ToString(),
                            AddressLine1 = dr["Address1"].ToString(),
                            AddressLine2 = dr["Address2"].ToString(),
                            Suburb = dr["Suburb"].ToString(),
                            Postcode = dr["Postcode"].ToString(),
                            Email = dr["Email"].ToString(),
                            Gender = dr["Gender"].ToString(),
                            ContactNumber = dr["Mobile"].ToString(),
                            DateOfBirth = dr.Get<DateTime>("DOB"),
                            WaterBaptismDate = dr.Get<DateTime>("WaterBaptismDate"),
                            Status = dr["Status"].ToString(),
                            DateRegistered = dr.Get<DateTime>("DateRegistered"),
                            DateLastComment = dr.Get<DateTime>("DateLastComment"),

                            PCRContactId = dr.Get<int>("PCRContactID"),
                            NumOfCall = dr.Get<int>("MonthCallTotal"),
                            NumOfVisit = dr.Get<int>("MonthVisitTotal"),

                            NumOfCallWeek = dr.Get<int>("WeekCallTotal"),
                            NumOfVisitWeek = dr.Get<int>("WeekVisitTotal"),
                            NumOfOthersWeek = dr.Get<int>("WeekOtherTotal")
                        };
                    }
                }));

            return contact;
        }

        public IEnumerable<PCRContact> FindPCRContactsByUrbanLife(int urbanLifeId, int? pcrId = null)
        {
            var contacts = new List<PCRContact>();

            ExecuteReader(
                "dbo.sp_PCR_GetUrbanLifeContacts",
                new[]
                    {
                        new SqlParameter("@ULG_ID", urbanLifeId),
                        new SqlParameter("@PCR_ID", pcrId)
                    },
                (dr =>
                {
                    while (dr.Read())
                    {
                        contacts.Add(new PCRContact
                        {
                            Id = Convert.ToInt32(dr["ID"]),
                            PCRContactId = Convert.ToInt32(dr["PCRContactID"]),
                            FirstName = dr["FirstName"].ToString(),
                            LastName = dr["LastName"].ToString(),
                            Email = dr["Email"].ToString(),
                            Status = dr["Status"].ToString(),
                            Carer = (!dr.IsDBNull(dr.GetOrdinal("Carer"))) ? Convert.ToInt32(dr["Carer"]) : 0,
                            CarerName = (!dr.IsDBNull(dr.GetOrdinal("Carer"))) ? dr["CarerFirstName"].ToString() + " " + dr["CarerLastName"].ToString() : "N/A",
                            DateRegistered = dr.Get<DateTime>("DateRegistered"),
                            DateLastComment = dr.Get<DateTime>("LastCommentMade"),
                            AttendSundayService = dr.Get<bool>("AttendSundayService"),
                            AttendUrbanLife = dr.Get<bool>("AttendUrbanLife"),
                            AttendBoom = dr.Get<bool>("AttendBoom")
                        });
                    }
                }));

            return contacts.OrderBy(x => x.FirstName).ToList();
        }

        public IEnumerable<PCRContact> FindPCRContactsByUrbanLifeAndCarer(int urbanLifeId, int carerId, int? pcrId = null) {
            var contacts = new List<PCRContact>();

            ExecuteReader(
                "dbo.sp_PCR_GetUrbanLifeContactsByCarerID",
                new[]
                    {
                        new SqlParameter("@ULG_ID", urbanLifeId),
                        new SqlParameter("@PCR_ID", pcrId),
                        new SqlParameter("@Carer_ID",carerId)
                    },
                (dr => {
                    while (dr.Read()) {
                        contacts.Add(new PCRContact {
                            Id = Convert.ToInt32(dr["ID"]),
                            PCRContactId = Convert.ToInt32(dr["PCRContactID"]),
                            FirstName = dr["FirstName"].ToString(),
                            LastName = dr["LastName"].ToString(),
                            Email = dr["Email"].ToString(),
                            Carer = (!dr.IsDBNull(dr.GetOrdinal("Carer"))) ? Convert.ToInt32(dr["Carer"]) : 0,
                            CarerName = (!dr.IsDBNull(dr.GetOrdinal("Carer"))) ? dr["CarerFirstName"].ToString() + " " + dr["CarerLastName"].ToString() : "N/A",
                            DateRegistered = dr.Get<DateTime>("DateRegistered"),
                            DateLastComment = dr.Get<DateTime>("LastCommentMade"),
                            AttendSundayService = dr.Get<bool>("AttendSundayService"),
                            AttendUrbanLife = dr.Get<bool>("AttendUrbanLife"),
                            AttendBoom = dr.Get<bool>("AttendBoom")
                        });
                    }
                }));

            return contacts.OrderBy(x => x.FirstName).ToList();
        }

        /* Mark Attendance for Urban Life */
        public void UpdateAttendanceForUrbanLife(int pcrContactId, bool attend)
        {
            UpdateAttendance(pcrContactId, attend, "UrbanLife");
        }

        /* Mark Attendance for Sunday Service */
        public void UpdateAttendanceForSundayService(int pcrContactId, bool attend)
        {
            UpdateAttendance(pcrContactId, attend, "SundayService");
        }

        /* Mark Attendance for Boom */
        public void UpdateAttendanceForBoom(int userId, int? bdtoId, int pcrContactId, bool attend)
        {
            Console.WriteLine("bdtoId = " + bdtoId);

            if (bdtoId != null)
            {
                ExecuteNonQuery(
                "dbo.sp_Church_BoomContactEntryInsert",
                new[] {
                    new SqlParameter("@PCRContact_ID", pcrContactId),
                    new SqlParameter("@BDTO_ID", bdtoId),
                    new SqlParameter("@Attend", attend),
                    new SqlParameter("@CheckInTime", DateTime.Now),
                    new SqlParameter("@User_ID", userId)
                });
            }
            
            UpdateAttendance(pcrContactId, attend, "Boom");
        }

        public void AddComment(int contactId, int pcrContactId, int commenterId, int numofcall, int numofothers, int numofvisit, string comments, bool pastoralComment)
        {
            if (comments == null)
                comments = "";
            ExecuteNonQuery(
                "dbo.sp_PCR_AddContactComments",
                new[] {
                    new SqlParameter("@PCRContact_ID", pcrContactId),
                    new SqlParameter("@Contact_ID", contactId),
                    new SqlParameter("@CommentBy_ID", commenterId),
                    new SqlParameter("@CommentType", pastoralComment ? 2 : 1),
                    new SqlParameter("@Comment",  comments),
                    new SqlParameter("@NumOfCalls", numofcall),
                    new SqlParameter("@NumOfOthers", numofothers),
                    new SqlParameter("@NumOfVisits", numofvisit),
                });
        }

        public List<CommentItem> GetComments(int contactId, bool PastoralAccess)
        {
            var comments = new List<CommentItem>();

            ExecuteReader(
                "dbo.sp_PCR_GetContactComments",
                new[]
                    {
                        new SqlParameter("@ContactID", contactId),
                        new SqlParameter("@PastoralAccess", PastoralAccess) // Get only admin comments,
                    },
                (dr =>
                {
                    while (dr.Read())
                    {
                        comments.Add(new CommentItem
                        {
                            Id = dr.Get<int>("ID"),
                            ContactId = contactId,
                            ContactName = dr.Get<string>("ContactName"),
                            Comment = dr.Get<string>("Comment"),
                            CommentDate = dr.Get<DateTime>("CommentDate"),
                            CommenterFirstName = dr.Get<string>("CommenterFirstName"),
                            CommenterLastName = dr.Get<string>("CommenterLastName")
                        });
                    }
                }));

            return comments;
        }

        public void SubmitReport(int pcrId, double totalOffering, int totalVisitor, int totalChildren, string comments, bool markAsCompleted)
        {
            ExecuteNonQuery(
                "dbo.sp_PCR_SubmitReport",
                new[] {
                    new SqlParameter("@PCR_ID", pcrId),
                    new SqlParameter("@TotalOffering", totalOffering),
                    new SqlParameter("@TotalVisitor", totalVisitor),
                    new SqlParameter("@TotalChildren", totalChildren),
                    new SqlParameter("@Comments", comments),
                    new SqlParameter("@MarkAsCompleted", markAsCompleted),
                    new SqlParameter("@CompletedDate", DateTime.Now)
                });
        }

        public PCRWeeklyItem GetWeeklyReportItem(int urbanLifeId, int? pcrDateId = null)
        {
            PCRWeeklyItem item = null;

            try
            {
                ExecuteReader(
                   "dbo.sp_PCR_GetReport",
                   new[] { 
                        new SqlParameter("@ULG_ID", urbanLifeId),
                        new SqlParameter("@PCRDate_ID", pcrDateId) 
                    },
                   (dr =>
                   {
                       if (dr.Read())
                       {
                           item = new PCRWeeklyItem
                           {
                               PCRId = dr.Get<int>("PCR_ID"),
                               PCRDateId = dr.Get<int>("PCRDate_ID"),
                               TotalChildren = dr.Get<int>("TotalChildren"),
                               TotalVisitor = dr.Get<int>("TotalVisitor"),
                               TotalOffering = dr.Get<double>("TotalOffering"),
                               TotalMember = dr.Get<int>("TotalMember"),
                               TotalSundayServiceAttendance = dr.Get<int>("TotalSundayAttendance"),
                               TotalUrbanLifeAttendance = dr.Get<int>("TotalULGAttendance"),
                               Comments = dr["Comments"].ToString(),
                               Completed = dr.Get<bool>("Completed"),
                               ReportDate = dr.Get<DateTime>("Date"),
                               UrbanLifeWeek = dr.Get<bool>("UrbanLifeWeek"),
                               BoomIsOn = dr.Get<bool>("BoomIsOn"),
                               LatestReport = dr.Get<bool>("LatestReport")
                           };
                       }
                   }));
            }
            catch (SqlException ex)
            {
                if (ex.Message.StartsWith("Cannot insert the value NULL into column 'PCRDate_ID'"))
                    throw new ReportIsNotReadyException("PCR for this week has not yet setup.", ex);
            }

            return item;
        }

        public IList<PCRHistoryItem> GetHistoricalReportItems(int urbanLifeId, int ageInDay = 90)
        {
            var items = new List<PCRHistoryItem>();

            ExecuteReader(
                "dbo.sp_PCR_GetReports",
                new[]
                    {
                        new SqlParameter("@UrbanLifeId", urbanLifeId),
                        new SqlParameter("@AgeInDay", ageInDay)
                    },
                (dr =>
                {
                    while (dr.Read())
                    {
                        items.Add(new PCRHistoryItem
                        {
                            PcrDateId = dr.Get<int>("PCRDate_ID"),
                            ReportDate = dr.Get<DateTime>("PCRDate"),
                            IsCompleted = dr.Get<bool>("Completed")
                        });
                    }
                }));

            return items;
        }

        public List<BoomPCRContact> GetBoomSearch(string query)
        {
            var list = new List<BoomPCRContact>();

            char[] delimiters = {' '};
            string[] subquery = query.Split(delimiters);

            foreach (string s in subquery)
            {
                ExecuteReader(
                "dbo.sp_PCR_GetBoomSearch",
                new[]
                    {
                        new SqlParameter("@Query", s)
                    },
                (dr =>
                {
                    while (dr.Read())
                    {
                        if (!list.Select(x => x.Id).ToList().Contains(dr.Get<int>("Contact_ID")))
                        {
                            list.Add(new BoomPCRContact
                            {
                                Id = dr.Get<int>("Contact_ID"),
                                FirstName = dr.Get<string>("FirstName"),
                                LastName = dr.Get<string>("LastName"),
                                ContactNumber = dr.Get<string>("Mobile"),
                                Email = dr.Get<string>("Email"),
                                Gender = dr.Get<string>("Gender"),
                                ULG_ID = dr.Get<int>("ULG_ID")
                            });
                        }
                        else
                        {
                            list.Find(x => x.Id == dr.Get<int>("Contact_ID")).ULG_ID = dr.Get<int>("ULG_ID");
                        }
                    }
                }));
            }                       

            return list;
        }

        public List<BoomPCRContact> GetBoomPCRContact(List<BoomPCRContact> list)
        {
            foreach (var item in list)
            {
                ExecuteReader(
                "dbo.sp_PCR_GetBoomPCRContact",
                new[]
                    {
                        new SqlParameter("@Contact_ID", item.Id),
                        new SqlParameter("@ULG_ID", item.ULG_ID)
                    },
                (dr =>
                {
                    while (dr.Read())
                    {
                        item.PCRContactId = dr.Get<int>("PCRContact_ID");
                        item.AttendSundayService = dr.Get<bool>("SundayAttendance");
                        item.AttendUrbanLife = dr.Get<bool>("ULGAttendance");
                        item.AttendBoom = dr.Get<bool>("BoomAttendance");
                    }
                }));
            }

            return list;
        }

        public void BoomBusesCheckOutUpdate(List<int> list, int bdtoID, int userID)
        {
            foreach (int boomContactEntry_ID in list)
            {
                    ExecuteNonQuery(
                        "dbo.sp_Church_BoomContactEntryCheckOutUpdate",
                        new[]
                        {
                            new SqlParameter("@BoomContactEntry_ID", boomContactEntry_ID),
                            new SqlParameter("@CheckOutDateTransportOptions_ID", bdtoID),
                            new SqlParameter("@CheckOutTime", DateTime.Now),
                            new SqlParameter("@CheckOutBy", userID)
                        });
                
            }            
        }

        #region Private Helpers

        private void UpdateAttendance(int pcrContactId, bool attend, string @event)
        {           
            ExecuteNonQuery(
                "dbo.sp_PCR_UpdateAttendance",
                new[] {
                    new SqlParameter("@PCRContact_ID", pcrContactId),
                    new SqlParameter("@Event", @event),
                    new SqlParameter("@Attend", attend)
                });
                        
        }

        #endregion
    }
}
