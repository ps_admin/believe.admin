﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Planetshakers.Core.DataInterfaces;
using Planetshakers.Core.Models;
using Planetshakers.Data.SqlServer.Helpers;

namespace Planetshakers.Data.SqlServer.Repositories
{
    public class RegistrationRepository : BaseRepository, IRegistrationRepository
    {
        public RegistrationRepository(string connectionString)
            : base(connectionString)
        {
        }

        #region Search
        public List<SingleRegistration> FindSingle(SearchModel model)
        {
            List<SingleRegistration> lst = new List<SingleRegistration>();

            ExecuteReader(
                "dbo.sp_Events_RegistrantSearch2",
                new[] { new SqlParameter("@QuickSearch", model.QuickSearch),
                        new SqlParameter("@FirstName", model.FirstName),
                        new SqlParameter("@LastName", model.LastName),
                        new SqlParameter("@ChurchName", model.ChurchName),
                        new SqlParameter("@Address", model.Address),
                        new SqlParameter("@Suburb", model.Suburb),
                        new SqlParameter("@Postcode", model.Postcode),
                        new SqlParameter("@Country_ID", model.Country_ID),
                        new SqlParameter("@State_ID", model.State_ID),
                        new SqlParameter("@Gender", model.Gender),
                        new SqlParameter("@Email", model.Email),
                        new SqlParameter("@EmergencyContact", model.EmergencyContactName),
                        new SqlParameter("@Phone", model.Phone),
                        new SqlParameter("@Fax", model.Fax),
                        new SqlParameter("@Mobile", model.Mobile),
                        new SqlParameter("@LeaderName", model.YouthLeaderName),
                        new SqlParameter("@Conference_ID", model.Conference_ID),
                        new SqlParameter("@Registered", model.Registered),
                        new SqlParameter("@CreditDebit", model.CreditDebit),
                        new SqlParameter("@DateFrom", model.DateFrom),
                        new SqlParameter("@DateTo", model.DateTo),
                        new SqlParameter("@GroupName", model.GroupName),
                        new SqlParameter("@Attended", model.Attended),
                        new SqlParameter("@Venue_ID", model.Venue_ID),
                        new SqlParameter("@RegistrationType_ID", model.RegistrationType_ID),
                        new SqlParameter("@PaymentType_ID", model.PaymentType_ID),
                        new SqlParameter("@WaitingList", null),
                        new SqlParameter("@PreOrder", null),
                        new SqlParameter("@SessionOrderProduct_ID", null),
                        new SqlParameter("@SessionOrderVenue_ID", null),
                        new SqlParameter("@RegistrationOptions", null),
                        new SqlParameter("@UserState_ID", null),
                        new SqlParameter("@UserCountry_ID", null),
                        new SqlParameter("@ShowInactiveRegistrants", false),
                        },
                (dr =>
                {
                    while (dr.Read())
                    {
                        lst.Add(new SingleRegistration
                        {
                            Contact_ID = Convert.ToInt32(dr["Contact_ID"]),
                            FirstName = dr["FirstName"].ToString(),
                            LastName = dr["LastName"].ToString(),
                            Mobile = dr["Mobile"].ToString(),
                            Email = dr["Email"].ToString(),
                            Suburb = dr["Suburb"].ToString(),
                            Gender = dr["Gender"].ToString(),
                            GroupName = dr["GroupName"].ToString(),
                            Registered = Convert.ToBoolean(dr["Registered"])
                        });
                    }
                }));

            return lst;
        }

        public List<GroupRegistration> FindGroup(SearchModel model)
        {
            List<GroupRegistration> lst = new List<GroupRegistration>();

            ExecuteReader(
                "dbo.sp_Events_GroupSearch",
                new[] { new SqlParameter("@QuickSearch", model.QuickSearch),
                        new SqlParameter("@GroupLeader_ID", model.GroupLeader_ID),
                        new SqlParameter("@GroupName", model.GroupName),
                        new SqlParameter("@Church", model.Church),
                        new SqlParameter("@YouthLeaderName", model.YouthLeaderName),
                        new SqlParameter("@GroupLeaderFirstName", model.GroupLeaderFirstName),
                        new SqlParameter("@GroupLeaderLastName", model.GroupLeaderLastName),
                        new SqlParameter("@GroupLeaderPhone", model.GroupLeaderPhone),
                        new SqlParameter("@GroupLeaderSuburb", model.GroupLeaderSuburb),
                        new SqlParameter("@State_ID", null),
                        new SqlParameter("@GroupLeaderEmail", model.GroupLeaderEmail),
                        new SqlParameter("@GroupLeaderMobile", model.GroupLeaderMobile),
                        new SqlParameter("@GroupSize_ID", null),
                        new SqlParameter("@GroupCreditStatus_ID", null),
                        new SqlParameter("@Conference_ID", model.Conference_ID),
                        new SqlParameter("@Venue_ID", model.Venue_ID),
                        new SqlParameter("@Registered", model.Registered),
                        new SqlParameter("@CreditDebit", null),
                        new SqlParameter("@UnallocatedRegos", model.UnallocatedRegosCount),
                        new SqlParameter("@GroupPromo_ID", null),
                        new SqlParameter("@UserState_ID", null)
                        },
                (dr =>
                {
                    while (dr.Read())
                    {
                        lst.Add(new GroupRegistration
                        {
                            ID = Convert.ToInt32(dr["GroupLeader_ID"]),
                            GroupName = dr["GroupName"].ToString(),
                            LeaderFirstName = dr["FirstName"].ToString(),
                            LeaderLastName = dr["LastName"].ToString(),
                            Balance = Convert.ToDecimal(dr["Balance"]),
                            CreationDate = Convert.ToDateTime(dr["CreationDate"]),
                            NumRegistered = (dr["NumRegistered"] != DBNull.Value ? Convert.ToInt32(dr["NumRegistered"]) : 0),
                            UnallocatedRegoCount = (dr["UnAllocatedRegos"] != DBNull.Value ? Convert.ToInt32(dr["UnAllocatedRegos"]) : 0)
                        });
                    }
                }));

            return lst;
        }
        #endregion

        #region Registrations
        public List<Registration> UpdateRegistration(Registration model)
        {
            var lst = new List<Registration>();
            var rego = new Registration();
            var id = 0;

            ExecuteReader(
                "dbo.sp_Events_RegistrationUpdate",
                new[] { new SqlParameter("@Registration_ID", model.ID),
                        new SqlParameter("@Contact_ID", model.ContactID),
                        new SqlParameter("@GroupLeader_ID", model.GroupLeader_ID),
                        new SqlParameter("@FamilyRegistration", false),
                        new SqlParameter("@RegisteredByFriend_ID", null),
                        new SqlParameter("@Venue_ID", model.VenueId),
                        new SqlParameter("@RegistrationType_ID", model.RegoType_ID),
                        new SqlParameter("@RegistrationDiscount", model.RegoDiscount), 
                        new SqlParameter("@Elective_ID", null),
                        new SqlParameter("@Accommodation", false),
                        new SqlParameter("@Accommodation_ID", null),
                        new SqlParameter("@Catering", false),
                        new SqlParameter("@CateringNeeds", String.Empty),
                        new SqlParameter("@LeadershipBreakfast", false),
                        new SqlParameter("@LeadershipSummit", false),
                        new SqlParameter("@ULG_ID", null),
                        new SqlParameter("@ParentGuardian", String.Empty),
                        new SqlParameter("@ParentGuardianPhone", String.Empty),
                        new SqlParameter("@AcceptTermsRego", true),
                        new SqlParameter("@AcceptTermsParent", false),
                        new SqlParameter("@AcceptTermsCreche", false),
                        new SqlParameter("@RegistrationDate", model.RegistrationDate),
                        new SqlParameter("@RegisteredBy_ID", model.RegisteredBy_ID),
                        new SqlParameter("@Attended", model.Attended),
                        new SqlParameter("@Volunteer", model.Volunteer),
                        new SqlParameter("@VolunteerDepartment_ID", model.VolunteerDepartment_ID),
                        new SqlParameter("@ComboRegistration_ID", null),
                        new SqlParameter("@User_ID", model.User_ID)
                        },
                (dr =>
                {   while (dr.Read())
                    {
                        id = Convert.ToInt32(dr["Registration_ID"]);
                    }
                }));

            ExecuteReader(
                "dbo.sp_Events_RegistrationRead",
                new[] { new SqlParameter("@Registration_ID", id) },
                (dr =>
                {
                    while (dr.Read())
                    {
                        rego = new Registration
                        {
                            ID = Convert.ToInt32(dr["Registration_ID"]),
                            GroupLeader_ID = Convert.ToInt32(dr["GroupLeader_ID"]),
                            RegoType_ID = Convert.ToInt32(dr["RegistrationType_ID"]),
                            RegoDateStr = dr["RegistrationDate"].ToString(),
                            Attended = Convert.ToBoolean(dr["Attended"]),
                            Volunteer = Convert.ToBoolean(dr["Volunteer"])  
                        };
                    }
                }));

            lst.Add(rego);

            return lst;
        }

        public int InsertRegistration(Registration rego)
        {
            var id = 0;

            ExecuteReader(
                "dbo.sp_Events_RegistrationInsert",
                new[] { new SqlParameter("@Contact_ID", rego.ContactID),
                        new SqlParameter("@GroupLeader_ID", rego.GroupLeader_ID),
                        new SqlParameter("@FamilyRegistration", false),
                        new SqlParameter("@RegisteredByFriend_ID", null),
                        new SqlParameter("@Venue_ID", rego.VenueId),
                        new SqlParameter("@RegistrationType_ID", rego.RegoType_ID),
                        new SqlParameter("@RegistrationDiscount", rego.RegoDiscount),
                        new SqlParameter("@Elective_ID", null),
                        new SqlParameter("@Accommodation", false),
                        new SqlParameter("@Accommodation_ID", null),
                        new SqlParameter("@Catering", false),
                        new SqlParameter("@CateringNeeds", String.Empty),
                        new SqlParameter("@LeadershipBreakfast", false),
                        new SqlParameter("@LeadershipSummit", false),
                        new SqlParameter("@ULG_ID", null),
                        new SqlParameter("@ParentGuardian", String.Empty),
                        new SqlParameter("@ParentGuardianPhone", String.Empty),
                        new SqlParameter("@AcceptTermsRego", true),
                        new SqlParameter("@AcceptTermsParent", false),
                        new SqlParameter("@AcceptTermsCreche", false),
                        new SqlParameter("@RegistrationDate", rego.RegistrationDate),
                        new SqlParameter("@RegisteredBy_ID", rego.RegisteredBy_ID),
                        new SqlParameter("@Attended", rego.Attended),
                        new SqlParameter("@Volunteer", rego.Volunteer),
                        new SqlParameter("@VolunteerDepartment_ID", rego.VolunteerDepartment_ID),
                        new SqlParameter("@ComboRegistration_ID", null),
                        new SqlParameter("@User_ID", rego.User_ID)
                        },
                (dr =>
                {
                    while (dr.Read())
                    {
                        id = Convert.ToInt32(dr["Registration_ID"]);
                    }
                }));

            return id;
        }

        public void RemoveRegistration(int reg_id, int user_id)
        {
            ExecuteNonQuery(
                "dbo.sp_Events_RegistrationDelete",
                new[] { new SqlParameter("@Registration_ID", reg_id),
                        new SqlParameter("@User_ID", user_id)
                        }
                );
        }

        public void InsertBulkRegistration(BulkRegistration model, int user_id)
        {
            ExecuteNonQuery(
                "dbo.sp_Events_GroupBulkRegistrationInsert",
                new[] { new SqlParameter("@GroupLeader_ID", model.GroupLeader_ID),
                        new SqlParameter("@Venue_ID", model.Venue_ID),
                        new SqlParameter("@RegistrationType_ID", model.RegistrationType_ID),
                        new SqlParameter("@Quantity", model.Quantity),
                        new SqlParameter("@DateAdded", DateTime.Now),
                        new SqlParameter("@User_ID", user_id)
                        }
                );
        }

        public void DeleteBulkRegistration(int BulkRego_ID, int user_id)
        {
            ExecuteNonQuery(
                "dbo.sp_Events_GroupBulkRegistrationDelete",
                new[] { new SqlParameter("@GroupBulkRegistration_ID", BulkRego_ID),
                        new SqlParameter("@User_ID", user_id)
                        }
                );
        }
        #endregion

        #region Payment
        public int InsertPayment(Payment model)
        {
            var pay_id = 0;

            ExecuteReader(
                "dbo.sp_Events_ContactPaymentInsert",
                new[] { new SqlParameter("@Contact_ID", model.Contact_ID),
                        new SqlParameter("@Conference_ID", model.Conference_ID),
                        new SqlParameter("@PaymentType_ID", model.PaymentType_ID),
                        new SqlParameter("@PaymentAmount", model.Amount),
                        new SqlParameter("@CCNumber", String.Empty),
                        new SqlParameter("@CCExpiry", String.Empty),
                        new SqlParameter("@CCName", String.Empty),
                        new SqlParameter("@CCPhone", String.Empty),
                        new SqlParameter("@CCManual", false),
                        new SqlParameter("@CCTransactionRef", String.Empty),
                        new SqlParameter("@CCRefund", (model.Category == "Refund" ? true : false)),
                        new SqlParameter("@ChequeDrawer", String.Empty),
                        new SqlParameter("@ChequeBank", String.Empty),
                        new SqlParameter("@ChequeBranch", String.Empty),
                        new SqlParameter("@PaypalTransactionRef", String.Empty),
                        new SqlParameter("@Comment", model.Comment),
                        new SqlParameter("@PaymentDate", model.CompletedDate),
                        new SqlParameter("@PaymentBy_ID", model.PaymentBy_ID),
                        new SqlParameter("@BankAccount_ID", model.BankAccount_ID),
                        new SqlParameter("@User_ID", model.User_ID)
                        },
                (dr =>
                {
                    while (dr.Read())
                    {
                        pay_id = Convert.ToInt32(dr["ContactPayment_ID"]);
                    }
                }
                )
            );

            return pay_id;
        }

        public void RemovePayment(int pay_id, int user_id)
        {
            ExecuteNonQuery(
                "dbo.sp_Events_ContactPaymentDelete",
                new[] { new SqlParameter("@ContactPayment_ID", pay_id),
                        new SqlParameter("@User_ID", user_id)
                        }
                );
        }

        public int InsertGroupPayment(Payment model)
        {
            var pay_id = 0;

            ExecuteReader(
                "dbo.sp_Events_GroupPaymentInsert",
                new[] { new SqlParameter("@GroupLeader_ID", model.Group_ID),
                        new SqlParameter("@Conference_ID", model.Conference_ID),
                        new SqlParameter("@PaymentType_ID", model.PaymentType_ID),
                        new SqlParameter("@PaymentSource_ID", model.PaymentSource_ID),
                        new SqlParameter("@PaymentAmount", model.Amount),
                        new SqlParameter("@CCNumber", String.Empty),
                        new SqlParameter("@CCExpiry", String.Empty),
                        new SqlParameter("@CCName", String.Empty),
                        new SqlParameter("@CCPhone", String.Empty),
                        new SqlParameter("@CCManual", false),
                        new SqlParameter("@CCTransactionRef", String.Empty),
                        new SqlParameter("@CCRefund", (model.Category == "Refund" ? true : false)),
                        new SqlParameter("@ChequeDrawer", String.Empty),
                        new SqlParameter("@ChequeBank", String.Empty),
                        new SqlParameter("@ChequeBranch", String.Empty),
                        new SqlParameter("@PaypalTransactionRef", String.Empty),
                        new SqlParameter("@Comment", model.Comment),
                        new SqlParameter("@PaymentStartDate", model.StartDate),
                        new SqlParameter("@PaymentCompletedDate", model.CompletedDate),
                        new SqlParameter("@PaymentCompleted", model.Completed),
                        new SqlParameter("@PaymentBy_ID", model.PaymentBy_ID),
                        new SqlParameter("@BankAccount_ID", model.BankAccount_ID),
                        new SqlParameter("@User_ID", model.User_ID)
                        },
                (dr =>
                {
                    while (dr.Read())
                    {
                        pay_id = Convert.ToInt32(dr["GroupPayment_ID"]);
                    }
                }
                )
            );

            return pay_id;
        }

        public void RemoveGroupPayment(int pay_id, int user_id)
        {
            ExecuteNonQuery(
                "dbo.sp_Events_GroupPaymentDelete",
                new[] { new SqlParameter("@GroupPayment_ID", pay_id),
                        new SqlParameter("@User_ID", user_id)
                        }
                );
        }
        #endregion

        public OutstandingBalance GetOutstandingBalance(string GroupSingle, int Contact_ID, int Conference_ID)
        {
            var outstanding = new OutstandingBalance();

            if (GroupSingle == "Single")
            {
                ExecuteReader(
                "dbo.sp_Events_OutstandingBalanceSingle",
                new[] { new SqlParameter("@Conference_ID", Conference_ID),
                        new SqlParameter("@Contact_ID", Contact_ID)
                        },
                (dr =>
                {
                    while (dr.Read())
                    {
                        outstanding = new OutstandingBalance
                        {
                            GroupSingle = dr["GroupSingle"].ToString(),
                            GroupRegistrant_ID = Convert.ToInt32(dr["GroupRegistrant_ID"]),
                            GroupRegistrantName = dr["GroupRegistrantName"].ToString(),
                            TotalCost = Convert.ToDecimal(dr["TotalCost"]),
                            TotalPayment = Convert.ToDecimal(dr["Payments"]),
                            Balance = Convert.ToDecimal(dr["TotalCost"]) - Convert.ToDecimal(dr["Payments"])
                        };
                    }
                }));

            } else if (GroupSingle == "Group")
            {
                ExecuteReader(
                "dbo.sp_Events_OutstandingBalanceGroup",
                new[] { new SqlParameter("@Conference_ID", Conference_ID),
                        new SqlParameter("@Contact_ID", Contact_ID)
                        },
                (dr =>
                {
                    while (dr.Read())
                    {
                        outstanding = new OutstandingBalance
                        {
                            GroupSingle = dr["GroupSingle"].ToString(),
                            GroupRegistrant_ID = Convert.ToInt32(dr["GroupRegistrant_ID"]),
                            GroupRegistrantName = dr["GroupRegistrantName"].ToString(),
                            TotalCost = Convert.ToDecimal(dr["TotalCost"]),
                            TotalPayment = Convert.ToDecimal(dr["TotalPayment"]),
                            Balance = Convert.ToDecimal(dr["TotalCost"]) - Convert.ToDecimal(dr["TotalPayment"])
                        };
                    }
                }));

            } else
            {
                // Error
            }

            return outstanding;
        }

        public List<PCRContact> GetValidInternalExternal(int user_id, string firstname, string lastname, string mobile, string email)
        {
            var lst = new List<PCRContact>();
            var person = new PCRContact();

            ExecuteReader(
                "dbo.sp_Common_ContactSearch2",
                
                new[] {
                    new SqlParameter("@SearchBase", null),
                    new SqlParameter("@Name", null),
                    new SqlParameter("@QuickSearch", null),
                    new SqlParameter("@FirstName", firstname),
                    new SqlParameter("@LastName", lastname),
                    new SqlParameter("@ChurchName", null),
                    new SqlParameter("@ContactNum", null),
                    new SqlParameter("@Address", null),
                    new SqlParameter("@Suburb", null),
                    new SqlParameter("@PostCode", null),
                    new SqlParameter("@Country_ID", null),
                    new SqlParameter("@State_ID", null),
                    new SqlParameter("@Gender", null),
                    new SqlParameter("@Email", email),
                    new SqlParameter("@EmergencyContact", null),
                    new SqlParameter("@Phone", null),
                    new SqlParameter("@Fax", null),
                    new SqlParameter("@Mobile", mobile),
                    new SqlParameter("@LeaderName", null),                    
                    new SqlParameter("@Subscription_ID", null),
                    new SqlParameter("@SignupDateStart", null),
                    new SqlParameter("@SignupDateEnd", null),
                    new SqlParameter("@User_ID", user_id)
                },
                (dr =>
                {
                    while (dr.Read())
                    {
                        if(dr["Contact_ID"] != null)
                        {
                            person = new PCRContact
                            {
                                Id = Convert.ToInt32(dr["Contact_ID"]),
                                Salutation = dr["Title"]?.ToString(),
                                FirstName = dr["FirstName"]?.ToString(),
                                LastName = dr["LastName"]?.ToString(),
                                AddressLine1 = dr["Address1"]?.ToString(),
                                Suburb = dr["Suburb"]?.ToString(),
                                Postcode = dr["Postcode"]?.ToString(),
                                State = dr["State"]?.ToString(),
                                ContactNumber = dr["Mobile"]?.ToString(),
                                Email = dr["Email"]?.ToString(),
                                Gender = dr["Gender"]?.ToString(),
                                Internal = Convert.ToBoolean(dr["ActiveInternal"]),
                                External = Convert.ToBoolean(dr["ActiveExternal"]),
                                Campus = dr["Campus"]?.ToString()
                            };
                            lst.Add(person);
                        }

                    }
                }));

            

            return lst;
        }
    }

    
}
