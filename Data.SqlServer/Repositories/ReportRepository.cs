﻿using Planetshakers.Core.DataInterfaces;
using Planetshakers.Core.Models;
using Planetshakers.Data.SqlServer.Helpers;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Data.Common;
using System.Data;
using System.Configuration;

namespace Planetshakers.Data.SqlServer.Repositories
{
    // Code here written with AccountRepository as reference.
    public class ReportRepository : BaseRepository, IReportRepository
    {
        public ReportRepository(string connectionString) : base(connectionString)
        {
        }
        public EventSummary GenerateSummaryReport(int conferenceId, String reportType, DateTime StartDate, DateTime EndDate)
        {
            String sp = null;
            // Changes sp depending on reportType passed in.    
            switch (reportType)
            {
                case "Attendance":
                    sp = "dbo.sp_Events_ReportSummaryAttendance";
                    break;
                case "Registration Types":
                    sp = "dbo.sp_Events_ReportSummaryRegistrationTypes";
                    break;
                case "States/Countries":
                    sp = "dbo.sp_Events_ReportSummaryState";
                    break;
                case "Age Groups":

                    sp = "dbo.sp_Events_ReportSummaryAge";
                    break;
                case "Payment Types":
                    sp = "dbo.sp_Events_ReportSummaryPurchaseTypes";
                    break;
                default:
                    sp = "dbo.sp_Events_ReportSummaryAttendance";
                    break;

            }

            EventSummary eventSummary = null;
            DataSet dataset = new DataSet();
            using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["PlanetshakersDB"].ConnectionString))
            using (var cmd = new SqlCommand(sp, con))

            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Conference_ID", conferenceId);
                cmd.Parameters.AddWithValue("@State_ID", null);
                if(reportType == "Registration Types" || reportType == "Payment Types")
                {
                    cmd.Parameters.AddWithValue("@StartDate", StartDate);
                    cmd.Parameters.AddWithValue("@EndDate", EndDate);
                }

                da.Fill(dataset);
            }
            eventSummary = new EventSummary { ds = dataset };
            return eventSummary;

        }
        public EventsTotalRegistration GenerateTotalRegistrationReport(int conferenceId)
        {


            List<string> sp_list = new List<string> { /*"dbo.sp_Events_ReportRegistrationCampusBreakdown",*/
                "dbo.sp_Events_ReportRegistrationTypeTotal", /*"dbo.sp_Events_ReportRegistrationMinistryBreakdown"*/ };
            EventsReportRegistrationTypeTotal totalRegoType = new EventsReportRegistrationTypeTotal();
            EventsReportRegistrationCampusBreakdown totalRegoCampusBreakdown = new EventsReportRegistrationCampusBreakdown();
            EventsReportRegistrationMinistryBreakdown totalRegoMinistryBreakdown = new EventsReportRegistrationMinistryBreakdown();


            foreach (string sp in sp_list)
            {
                DataSet dataset = new DataSet();
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["PlanetshakersDB"].ConnectionString))
                using (var cmd = new SqlCommand(sp, con))

                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandTimeout = 300;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Conference_ID", conferenceId);
                    da.Fill(dataset);
                }
                switch (sp)
                {
                    case "dbo.sp_Events_ReportRegistrationCampusBreakdown":
                        totalRegoCampusBreakdown = new EventsReportRegistrationCampusBreakdown { ds = dataset };
                        break;
                    case "dbo.sp_Events_ReportRegistrationTypeTotal":
                        totalRegoType = new EventsReportRegistrationTypeTotal { ds = dataset };
                        break;
                    case "dbo.sp_Events_ReportRegistrationMinistryBreakdown":
                        totalRegoMinistryBreakdown = new EventsReportRegistrationMinistryBreakdown { ds = dataset };
                        break;
                } 
            }
            EventsTotalRegistration totalRego = new EventsTotalRegistration();
            totalRego.totalCampusBreakdown = totalRegoCampusBreakdown;
            totalRego.totalMinistryBreakdown = totalRegoMinistryBreakdown;
            totalRego.totalRegoTypeBreakdown = totalRegoType;

            return totalRego;

        }

        public EventSummary GenerateBankingReport(int conferenceId, DateTime? StartDate, DateTime? EndDate, string selectedUser, Boolean IncludeTransactionReport)
        {
            String sp = "dbo.sp_Events_ReportBanking2";
            EventSummary eventSummary = null;
            DataSet dataset = new DataSet();
            using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["PlanetshakersDB"].ConnectionString))
            using (var cmd = new SqlCommand(sp, con))

            using (var adapter = new SqlDataAdapter(cmd))
            {
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Conference_ID", conferenceId);
                cmd.Parameters.AddWithValue("@StartDate", StartDate);
                cmd.Parameters.AddWithValue("@EndDate", EndDate);
                cmd.Parameters.AddWithValue("@Users", selectedUser);
                cmd.Parameters.AddWithValue("@IncludeTransactionReport", false);

                adapter.Fill(dataset);
            }
            eventSummary = new EventSummary { ds = dataset };

            return eventSummary;
        }

        public EventSummary GenerateRegReport(int conference_ID, int venue_ID, string reportType, List<int> SelectRegoTypes)
        {
            String sp = null;
            // Changes sp depending on reportType passed in.    
            switch (reportType)
            {
                case "Single Registrations":
                    sp = "dbo.sp_Events_ReportSingleSummary";
                    break;
                case "Group Registrations":
                    // TODO: Not the right sp. 
                    sp = "dbo.sp_Events_ReportGroupSummary";
                    break;
                case "Group Summary":
                    sp = "dbo.sp_Events_ReportGroupSummary";
                    break;
                default:
                    sp = "dbo.sp_Events_ReportGroupSummary";
                    break;
            }

            EventSummary eventSummary = null;
            DataSet dataset = new DataSet();
            using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["PlanetshakersDB"].ConnectionString))
            using (var cmd = new SqlCommand(sp, con))

            using (var da = new SqlDataAdapter(cmd))
            {
                cmd.CommandTimeout = 300;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Conference_ID", conference_ID);
                cmd.Parameters.AddWithValue("@Venue_ID", venue_ID);
                //cmd.Parameters.AddWithValue("@GroupLeader_ID", null);

                da.Fill(dataset);
            }
            eventSummary = new EventSummary { ds = dataset };
            return eventSummary;

        }

        public EventSummary GenerateTotalRegistrantsReport()
        {
            // Block of code that executes this command, takes the table returned by command and puts it into a DataSet type variable.
            // Then, an eventSummary is created with that dataset. Then the eventSummary is returned.
            //String cmdString = "SELECT[Venue_ID],[Registrants] FROM[DEV_Planetshakers2].[dbo].[vw_Events_TotalRegistrants]";
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["PlanetshakersDB"].ConnectionString);
            var csb = new SqlConnectionStringBuilder(ConfigurationManager.ConnectionStrings["PlanetshakersDB"].ConnectionString);
            String initialCatalog = csb.InitialCatalog;
            // this command gets the table that displays event name and total registrants for all events that end one month before present time to all upcoming events.
            String cmdString = "";
            cmdString += "with t1 as (select c.ConferenceName, count(rt.Conference_ID) as b from [" + initialCatalog + "].[dbo].[Events_Registration] r";
            cmdString += "join [" + initialCatalog + "].[dbo].[Events_RegistrationType] rt on r.RegistrationType_ID = rt.RegistrationType_ID";
            cmdString += "join [" + initialCatalog + "].[dbo].[Events_Venue] v on rt.Conference_ID = v.Conference_ID";
            cmdString += "join [" + initialCatalog + "].[dbo].[Events_Conference] c on rt.Conference_ID = c.Conference_ID";
            cmdString += "where v.ConferenceEndDate > GETDATE() and v.Conference_ID != 54 and rt.IsChildRegistrationType = 0 and r.GroupLeader_ID is null and rt.RegistrationCost != 0";
            cmdString += "group by c.ConferenceName),";

            cmdString += "t2 as (select c.ConferenceName, IsNull(sum(br.Quantity), 0)  as a from [" + initialCatalog + "].[dbo].[Events_GroupBulkRegistration] br ";
            cmdString += "join [" + initialCatalog + "].[dbo].[Events_RegistrationType] rt on rt.RegistrationType_ID = br.RegistrationType_ID";
            cmdString += "join [" + initialCatalog + "].[dbo].[Events_Venue] v on v.Conference_ID = rt.Conference_ID";
            cmdString += "join [" + initialCatalog + "].[dbo].[Events_Conference] c on rt.Conference_ID = c.Conference_ID";
            cmdString += "where v.ConferenceEndDate > GETDATE() and v.Conference_ID != 54 and rt.IsChildRegistrationType = 0";
            cmdString += "group by c.ConferenceName)";
            cmdString += "select t2.ConferenceName, (IsNull(b, 0)+a) as TotalRegistration from t2 full outer join t1 on t1.ConferenceName = t2.ConferenceName";

            SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandText = cmdString;
            da.SelectCommand = cmd;

            EventSummary eventSummary = null;
            DataSet dataset = new DataSet();

            con.Open();
            da.Fill(dataset);
            con.Close();
            
            eventSummary = new EventSummary { ds = dataset };

            return eventSummary;
        }
    }

}
