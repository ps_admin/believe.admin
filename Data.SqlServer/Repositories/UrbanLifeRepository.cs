﻿using System;
using System.Data.SqlClient;
using Planetshakers.Core.DataInterfaces;
using Planetshakers.Core.Models;

namespace Planetshakers.Data.SqlServer.Repositories
{
    public class UrbanLifeRepository : BaseRepository, IUrbanLifeRepository
    {
        public UrbanLifeRepository(string connectionString)
            : base(connectionString)
        { 
        }

        public UrbanLife FindById(int id)
        {
            UrbanLife urbanLife = null;

            ExecuteReader(
                "dbo.sp_UrbanLife_GetDetails",
                new[] { new SqlParameter("@ULG_ID", id) },
                (dr =>
                {
                    if (dr.Read())
                    {
                        urbanLife = new UrbanLife
                        {
                            Id = Convert.ToInt32(dr["ID"]),
                            Code = dr["Code"].ToString(),
                            Name = dr["Name"].ToString(),
                            AddressLine1 = dr["Address1"].ToString(),
                            AddressLine2 = dr["Address2"].ToString(),
                            Suburb = dr["Suburb"].ToString(),
                            Postcode = dr["Postcode"].ToString(),
                            Region = dr["Region"].ToString(),
                            Ministry = dr["Ministry"].ToString(),
                            Campus = dr["Campus"].ToString(),
                            MeetingDay = dr["MeetingDay"].ToString(),
                            MeetingTime = dr["MeetingTime"].ToString()
                        };
                    }
                }));

            return urbanLife;
        }
    }
}
