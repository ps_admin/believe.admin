﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Planetshakers.Core.DataInterfaces;
using Planetshakers.Core.Models;
using Planetshakers.Data.SqlServer.Helpers;

namespace Planetshakers.Data.SqlServer.Repositories
{
    public class UserRepository : BaseRepository, IUserRepository
    {
        public UserRepository(string connectionString)
            : base(connectionString)
        { 
        }

        public IList<CommentStream> GetCommentActivityStreams(int userId)
        {
            IList<CommentStream> streams = new List<CommentStream>();

            ExecuteReader(
                "dbo.usp_User_GetCommentActivityStreams",
                new[] { new SqlParameter("@ContactId", userId) },
                (dr =>
                {
                    while (dr.Read())
                    {
                        var stream = new CommentStream()
                        {                            
                            ContactFirstName = dr.Get<string>("FirstName"),
                            ContactLastName = dr.Get<string>("LastName"),
                            FullComment = dr.Get<string>("Comment"),
                            CommentDate = dr.Get<DateTime>("CommentDate"),
                            Actioned = dr.Get<bool>("Actioned"),
                            CommentType = dr.GetEnum<CommentType>("CommentType")
                        };   

                        streams.Add(stream);
                    }
                }));

            return streams;
        }
    }
}
