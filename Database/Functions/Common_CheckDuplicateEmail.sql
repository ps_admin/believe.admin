SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Common_CheckDuplicateEmail] (
	@Email		NVARCHAR(100)
)
RETURNS		INT

AS
BEGIN
   DECLARE @retval int
   SELECT @retval = COUNT(*) FROM Common_Contact WHERE Email = @Email and Email <> ''
   RETURN @retval
END
GO
