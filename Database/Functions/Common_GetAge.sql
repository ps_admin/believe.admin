SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Common_GetAge] (	@pDateOfBirth    DATETIME )

RETURNS INT
AS
BEGIN

    DECLARE @vAge         INT
    
    IF @pDateOfBirth >= GetDate()
        RETURN 0

    SET @vAge = DATEDIFF(YY, @pDateOfBirth, GetDate())

    IF MONTH(@pDateOfBirth) > MONTH(GetDate()) OR
      (MONTH(@pDateOfBirth) = MONTH(GetDate()) AND
       DAY(@pDateOfBirth)   > DAY(GetDate()))
        SET @vAge = @vAge - 1

    RETURN @vAge
END
GO
