SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Common_GetAgeFromDate] (	
	@pDateOfBirth    DATETIME, 
	@DateFrom		 DATETIME)

RETURNS INT
AS
BEGIN

    DECLARE @vAge         INT
    
    IF @pDateOfBirth >= @DateFrom
        RETURN 0

    SET @vAge = DATEDIFF(YY, @pDateOfBirth, @DateFrom)

    IF MONTH(@pDateOfBirth) > MONTH(@DateFrom) OR
      (MONTH(@pDateOfBirth) = MONTH(@DateFrom) AND
       DAY(@pDateOfBirth)   > DAY(@DateFrom))
        SET @vAge = @vAge - 1

    RETURN @vAge
END
GO
