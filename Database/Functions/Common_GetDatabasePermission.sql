SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================

CREATE FUNCTION [dbo].[Common_GetDatabasePermission]
(
	@DatabaseObjectName		NVARCHAR(50),
	@User_ID				INT
)
RETURNS int
AS

BEGIN

	DECLARE @Return		INT

	SELECT	@Return = MAX(CONVERT(INT,[Read]))
	FROM	Common_DatabaseObject DO, Common_DatabaseRolePermission P, Common_ContactDatabaseRole R
	WHERE	DO.DatabaseObject_ID = P.DatabaseObject_ID AND P.DatabaseRole_ID = R.DatabaseRole_ID 
	AND		DO.DatabaseObjectName = @DatabaseObjectName
	AND		R.Contact_ID = @User_ID

	RETURN @Return

END
GO
