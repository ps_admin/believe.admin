SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[DateDifference](
	@Date1	datetime,
	@Date2	datetime)
RETURNS nvarchar(50)

AS

BEGIN

	DECLARE @Seconds	bigint
	DECLARE @DateDiff	nvarchar(50)
	SET @DateDiff = ''
	
	SELECT @Seconds = (FLOOR(dbo.DateTimeToTicks(@Date1) / 10000000)) - 
					(FLOOR(dbo.DateTimeToTicks(@Date2) / 10000000))
	
	IF FLOOR(@Seconds / (60 * 60 * 24)) > 0 BEGIN 
		SET @DateDiff = @DateDiff + CONVERT(NVARCHAR, FLOOR(@Seconds / (60 * 60 * 24))) + 'd '
		SET @Seconds = @Seconds - FLOOR(@Seconds / (60 * 60 * 24)) * (60 * 60 * 24)
	END
	IF FLOOR(@Seconds / (60 * 60)) > 0 BEGIN 
		SET @DateDiff = @DateDiff + CONVERT(NVARCHAR, FLOOR(@Seconds / (60 * 60))) + 'h '
		SET @Seconds = @Seconds - FLOOR(@Seconds / (60 * 60)) * (60 * 60)
	END
	IF FLOOR(@Seconds / (60)) > 0 BEGIN
		SET @DateDiff = @DateDiff + CONVERT(NVARCHAR, FLOOR(@Seconds / (60))) + 'm '
		SET @Seconds = @Seconds - FLOOR(@Seconds / (60)) * (60)
	END
	IF @Seconds % 60 > 0 SET @DateDiff = @DateDiff + CONVERT(NVARCHAR, @Seconds % 60) + 's '

	RETURN @DateDiff
	
END
GO
