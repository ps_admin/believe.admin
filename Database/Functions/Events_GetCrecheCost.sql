SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Events_GetCrecheCost] (@Registration_ID INT) RETURNS MONEY
AS
BEGIN

	DECLARE @Return		MONEY

	SELECT	@Return = SUM(RegistrationCost) 
	FROM	Events_CrecheChild CC,
			Events_CrecheChildRegistrationType RT
	WHERE	CC.CrecheChildRegistrationType_ID = RT.CrecheChildRegistrationType_ID
	AND		CC.Registration_ID = @Registration_ID
	
	RETURN ISNULL(@Return,0)
	
END
GO
