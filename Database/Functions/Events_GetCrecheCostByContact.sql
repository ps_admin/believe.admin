SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Events_GetCrecheCostByContact] (@Contact_ID INT, @Venue_ID INT) RETURNS MONEY
AS
BEGIN

	DECLARE @Return		MONEY

	SELECT	@Return = SUM(RegistrationCost) 
	FROM	Events_Registration R,
			Events_CrecheChild CC,
			Events_CrecheChildRegistrationType RT
	WHERE	R.Registration_ID = CC.Registration_ID
	AND		CC.CrecheChildRegistrationType_ID = RT.CrecheChildRegistrationType_ID
	AND		R.Contact_ID = @Contact_ID
	AND		R.Venue_ID = @Venue_ID
	
	RETURN ISNULL(@Return,0)
	
END
GO
