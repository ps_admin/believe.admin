SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[MaximumInt]
(
	@Val1	 INT,
	@Val2	 INT
)
RETURNS INT
AS
BEGIN
	RETURN CASE WHEN @Val1 > @Val2 THEN @Val1 ELSE @Val2 END
END
GO
