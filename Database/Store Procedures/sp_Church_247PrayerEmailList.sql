SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_247PrayerEmailList]

@Campus_ID	INT

AS

SELECT		DISTINCT
			C.Contact_ID,
			C.FirstName,
			C.LastName,
			C.FirstName + ' ' + C.LastName as FullName,
			CASE WHEN C.Email = '' THEN C.Email2 ELSE C.Email END as Email
FROM		Common_Contact C,
			Common_ContactInternal CI,
			Church_247Prayer P
WHERE		C.Contact_ID = CI.Contact_ID
AND			CI.Contact_ID = P.Contact_ID
AND			CASE WHEN C.Email = '' THEN C.Email2 ELSE C.Email END <> ''
AND			ContactMethod_ID = 1
AND			(CI.Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
UNION ALL	
SELECT		DISTINCT
			C.Contact_ID,
			C.FirstName,
			C.LastName,
			C.FirstName + ' ' + C.LastName as FullName,
			C.Email2 as Email
FROM		Common_Contact C,
			Common_ContactInternal CI,
			Church_247Prayer P
WHERE		C.Contact_ID = CI.Contact_ID
AND			CI.Contact_ID = P.Contact_ID
AND			C.Email <> '' AND C.Email2 <> ''
AND			ContactMethod_ID = 1
AND			(CI.Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
ORDER BY	C.Contact_ID

SELECT		P.Contact_ID,
			D.Name as [Day],
			T.Name as [Time],
			Du.Duration as Duration
FROM		Common_Contact C,
			Common_ContactInternal CI,
			Church_247Prayer P,
			Church_247PrayerDay D,
			Church_247PrayerTime T,
			Church_247PrayerDuration Du
WHERE		C.Contact_ID = CI.Contact_ID
AND			CI.Contact_ID = P.Contact_ID
AND			P.Day_ID = D.Day_ID
AND			P.Time_ID = T.Time_ID
AND			P.Duration_ID = Du.Duration_ID
AND			CASE WHEN C.Email = '' THEN C.Email2 ELSE C.Email END <> ''
AND			ContactMethod_ID = 1
AND			(CI.Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
ORDER BY	C.Contact_ID
GO
