SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_247PrayerInsert]
(
	@Contact_ID int,
	@Day_ID int,
	@Time_ID int,
	@Duration_ID int,
	@ContactMethod_ID int,
	@DateAdded datetime,
	@User_ID int = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin

	SET NOCOUNT ON

	INSERT INTO [Church_247Prayer]
	(
		[Contact_ID],
		[Day_ID],
		[Time_ID],
		[Duration_ID],
		[ContactMethod_ID],
		[DateAdded]
	)
	VALUES
	(
		@Contact_ID,
		@Day_ID,
		@Time_ID,
		@Duration_ID,
		@ContactMethod_ID,
		@DateAdded
	)
	
	SELECT SCOPE_IDENTITY() as [247Prayer_ID]
GO
