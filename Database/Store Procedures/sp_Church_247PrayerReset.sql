SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_247PrayerReset]

@Campus_ID	INT

AS

DELETE	FROM	Church_247Prayer
FROM	Common_ContactInternal CI
WHERE	CI.Contact_ID = Church_247Prayer.Contact_ID
AND		(CI.Campus_ID = @Campus_ID OR @Campus_ID IS NULL OR CI.Campus_ID IS NULL)
GO
