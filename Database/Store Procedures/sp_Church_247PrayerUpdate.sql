SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_247PrayerUpdate]
(
	@247Prayer_ID int,
	@Contact_ID int,
	@Day_ID int,
	@Time_ID int,
	@Duration_ID int,
	@ContactMethod_ID int,
	@DateAdded datetime,
	@User_ID int = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin

	SET NOCOUNT ON
	
	UPDATE [Church_247Prayer]
	SET
		[Contact_ID] = @Contact_ID,
		[Day_ID] = @Day_ID,
		[Time_ID] = @Time_ID,
		[ContactMethod_ID] = @ContactMethod_ID,
		[Duration_ID] = @Duration_ID,
		[DateAdded] = @DateAdded
	WHERE 
		[247Prayer_ID] = @247Prayer_ID
GO
