SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ActionComment]

@Comment_ID		INT,
@User_ID		INT

AS

UPDATE	Church_ContactComment
SET		Actioned = 1,
		ActionedBy_ID = @User_ID,
		ActionedDate = dbo.GetRelativeDate()
WHERE	Comment_ID = @Comment_ID
GO
