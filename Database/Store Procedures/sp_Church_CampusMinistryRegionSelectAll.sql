SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_CampusMinistryRegionSelectAll]

AS

SELECT	CMR.* 
FROM	Church_CampusMinistryRegion CMR,
		Church_Region R
WHERE	CMR.Region_ID = R.Region_ID
AND		R.Deleted = 0
GO
