SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ChurchStatusChangeRequestInsert]
(
	@Contact_ID int,
	@ChurchStatus_ID int,
	@DeleteReason_ID int = NULL,
	@CurrentCampus_ID int = NULL,
	@CurrentMinistry_ID int = NULL,
	@CurrentRegion_ID int = NULL,
	@NPNC_ID int = NULL,
	@Campus_ID int = NULL,
	@Ministry_ID int = NULL,
	@Region_ID int = NULL,
	@ULG_ID int = NULL,
	@Comments nvarchar(max),
	@DateRequested datetime,
	@RequestedBy_ID int,
	@Actioned bit,
	@GUID uniqueidentifier,
	@User_ID int = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin

	SET NOCOUNT ON

	INSERT INTO [Church_ChurchStatusChangeRequest]
	(
		[Contact_ID],
		[ChurchStatus_ID],
		[DeleteReason_ID],
		[CurrentCampus_ID],
		[CurrentMinistry_ID],
		[CurrentRegion_ID],
		[NPNC_ID], 
		[Campus_ID],
		[Ministry_ID],
		[Region_ID],
		[ULG_ID],
		[Comments],
		[DateRequested],
		[RequestedBy_ID],
		[Actioned],
		[GUID]
	)
	VALUES
	(
		@Contact_ID,
		@ChurchStatus_ID,
		@DeleteReason_ID,
		@CurrentCampus_ID,
		@CurrentMinistry_ID,
		@CurrentRegion_ID,
		@NPNC_ID,
		@Campus_ID,
		@Ministry_ID,
		@Region_ID,
		@ULG_ID,
		@Comments,
		@DateRequested,
		@RequestedBy_ID,
		@Actioned,
		@GUID
	)

	SELECT StatusChangeRequest_ID = SCOPE_IDENTITY();
GO
