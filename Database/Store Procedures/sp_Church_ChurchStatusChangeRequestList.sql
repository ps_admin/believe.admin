SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ChurchStatusChangeRequestList]

@Additions		BIT,
@Deletions		BIT,
@Campus_ID		INT = null,
@Ministry_ID	INT

AS
	
SET NOCOUNT ON

SELECT		CR.StatusChangeRequest_ID as ID,
			CR.Contact_ID,
			C.FirstName + ' ' + C.LastName as ContactName,
			CR.DateRequested,
			U.Name as RequestedBy,
			ISNULL((SELECT Name FROM Church_Campus WHERE Campus_ID = CR.CurrentCampus_ID), '') as Campus,
			ISNULL((SELECT Name FROM Church_Ministry WHERE Ministry_ID = CR.CurrentMinistry_ID), '') as Ministry,
			S.Name as [Status],
			ISNULL((SELECT Name FROM Church_DeleteReason WHERE DeleteReason_ID = CR.DeleteReason_ID), '') as DeleteReason,
			CASE WHEN CR.DeleteReason_ID IS NULL AND CI.ChurchStatus_ID IN (3,4)
				THEN ISNULL((SELECT TOP 1 Comment FROM Church_ContactComment WHERE Contact_ID = CR.Contact_ID AND CommentSource_ID = 4 ORDER BY CommentDate DESC), '')
				ELSE CR.Comments
			END as Comment,
			ISNULL((SELECT Name FROM Church_ChurchStatus WHERE ChurchStatus_ID = CR.ChurchStatus_ID), '') as RequestedStatus
FROM		Church_ChurchStatusChangeRequest CR,
			Common_ContactInternal CI,
			Common_Contact C,
			vw_Users U,
			Church_ChurchStatus S
WHERE		CR.Contact_ID = CI.Contact_ID
AND			CR.Contact_ID = C.Contact_ID
AND			CR.RequestedBy_ID = U.Contact_ID
AND			CI.ChurchStatus_ID = S.ChurchStatus_ID
AND			((@Additions = 1 AND CR.DeleteReason_ID IS NULL)
OR			(@Deletions = 1 AND CR.DeleteReason_ID IS NOT NULL))
AND			(CI.Campus_ID = @Campus_ID OR CR.Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
AND			(CI.Ministry_ID = @Ministry_ID OR @Ministry_ID IS NULL)
AND			CR.Actioned = 0
ORDER BY	DateRequested DESC
GO
