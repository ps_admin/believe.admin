SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ChurchStatusChangeRequestRead]

@StatusChangeRequest_ID		INT

AS
	
SET NOCOUNT ON

SELECT		*
FROM		Church_ChurchStatusChangeRequest
WHERE		StatusChangeRequest_ID = @StatusChangeRequest_ID
GO
