SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ChurchStatusChangeRequestUpdate]
(
	@StatusChangeRequest_ID int,
	@Contact_ID int,
	@ChurchStatus_ID int,
	@DeleteReason_ID int = NULL,
	@CurrentCampus_ID int = NULL,
	@CurrentMinistry_ID int = NULL,
	@CurrentRegion_ID int = NULL,
	@NPNC_ID int = NULL,
	@Campus_ID int = NULL,
	@Ministry_ID int = NULL,
	@Region_ID int = NULL,
	@ULG_ID int = NULL,
	@Comments nvarchar(max),
	@DateRequested datetime,
	@RequestedBy_ID int,
	@Actioned bit,
	@GUID uniqueidentifier,
	@User_ID int = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin

	SET NOCOUNT ON
	
	UPDATE [Church_ChurchStatusChangeRequest]
	SET
		[Contact_ID] = @Contact_ID,
		[ChurchStatus_ID] = @ChurchStatus_ID,
		[DeleteReason_ID] = @DeleteReason_ID,
		[CurrentCampus_ID] = @CurrentCampus_ID,
		[CurrentMinistry_ID] = @CurrentMinistry_ID,
		[CurrentRegion_ID] = @CurrentRegion_ID,
		[NPNC_ID] = @NPNC_ID,
		[Campus_ID] = @Campus_ID,
		[Ministry_ID] = @Ministry_ID,
		[Region_ID] = @Region_ID,
		[ULG_ID] = @ULG_ID,
		[Comments] = @Comments,
		[DateRequested] = @DateRequested,
		[RequestedBy_ID] = @RequestedBy_ID,
		[Actioned] = @Actioned,
		[GUID] = @GUID
	WHERE 
		[StatusChangeRequest_ID] = @StatusChangeRequest_ID
GO
