SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ChurchStatusSelectAll]

AS

SELECT		* 
FROM		Church_ChurchStatus 
WHERE		Deleted = 0
ORDER BY	SortOrder

SELECT		CSAC.* 
FROM		Church_ChurchStatusAllowedChanges CSAC,
			Church_ChurchStatus CSF,
			Church_ChurchStatus CST
WHERE		CSAC.ChurchStatusFrom_ID = CSF.ChurchStatus_ID
AND			CSAC.ChurchStatusTo_ID = CST.ChurchStatus_ID
AND			CSAC.ChurchStatusFrom_ID IS NOT NULL
AND			CSF.Deleted = 0
AND			CST.Deleted = 0
GO
