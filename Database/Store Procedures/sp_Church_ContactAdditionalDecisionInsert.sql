SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ContactAdditionalDecisionInsert]
(
	@Contact_ID int,
	@DecisionDate datetime,
	@DecisionType_ID int,
	@CampusDecision_ID int,
	@Comments ntext,
	@EnteredBy_ID int,
	@EnteredDate datetime,
	@User_ID int
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin

	SET NOCOUNT ON

	INSERT INTO [Church_ContactAdditionalDecision]
	(
		[Contact_ID],
		[DecisionDate],
		[DecisionType_ID],
		[CampusDecision_ID],
		[Comments],
		[EnteredBy_ID],
		[EnteredDate]
	)
	VALUES
	(
		@Contact_ID,
		@DecisionDate,
		@DecisionType_ID,
		@CampusDecision_ID,
		@Comments,
		@EnteredBy_ID,
		@EnteredDate
	)

	SELECT AdditionalDecision_ID = SCOPE_IDENTITY();
GO
