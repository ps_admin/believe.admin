SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ContactAdditionalDecisionUpdate]
(
	@AdditionalDecision_ID int,
	@Contact_ID int,
	@DecisionDate datetime,
	@DecisionType_ID int,
	@CampusDecision_ID int, 
	@Comments ntext,
	@EnteredBy_ID int,
	@EnteredDate datetime,
	@User_ID int
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin

	SET NOCOUNT ON
	
	UPDATE [Church_ContactAdditionalDecision]
	SET
		[Contact_ID] = @Contact_ID,
		[DecisionDate] = @DecisionDate,
		[DecisionType_ID] = @DecisionType_ID,
		[CampusDecision_ID] = @CampusDecision_ID,
		[Comments] = @Comments,
		[EnteredBy_ID] = @EnteredBy_ID,
		[EnteredDate] = @EnteredDate
	WHERE 
		[AdditionalDecision_ID] = @AdditionalDecision_ID
GO
