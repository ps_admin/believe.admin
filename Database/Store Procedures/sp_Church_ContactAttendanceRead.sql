SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ContactAttendanceRead]

@Contact_ID		int,
@StartDate		datetime,
@EndDate		datetime

AS

SELECT		PD.[Date],
			U.Code,
			U.Name,
			PC.SundayAttendance,
			PC.ULGAttendance,
			PC.BoomAttendance,
			PC.FNLAttendance,
			PC.[Call],
			PC.Visit,
			PC.Other
FROM		Church_PCRDate PD,
			Church_ULG U,
			Church_PCRContact PC,
			Church_PCR P
WHERE		P.PCR_ID = PC.PCR_ID
AND			P.PCRDate_Id = PD.PCRDate_ID
AND			P.ULG_Id = U.ULG_ID
AND			PC.Contact_ID = @Contact_ID
AND			PD.[Date] >= @StartDate
AND			PD.[Date] < DATEADD(dd, 1, @EndDate)
ORDER BY	PD.[Date] Desc
GO
