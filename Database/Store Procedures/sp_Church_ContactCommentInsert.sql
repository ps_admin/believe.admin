SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ContactCommentInsert]
(
	@Contact_ID int,
	@CommentBy_ID int,
	@CommentDate datetime,
	@Comment ntext,
	@CommentType_ID int,
	@CommentSource_ID int,
	@NPNC_ID int,
	@InReplyToComment_ID int = null,
	@Actioned bit,
	@ActionedBy_ID int,
	@ActionedDate datetime,
	@User_ID int = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin

	SET NOCOUNT ON

	INSERT INTO [Church_ContactComment]
	(
		[Contact_ID],
		[CommentBy_ID],
		[CommentDate],
		[Comment],
		[CommentType_ID],
		[CommentSource_ID],
		[NPNC_ID],
		[InReplyToComment_ID],
		[Actioned],
		[ActionedBy_ID],
		[ActionedDate]
	)
	VALUES
	(
		@Contact_ID,
		@CommentBy_ID,
		@CommentDate,
		@Comment,
		@CommentType_ID,
		@CommentSource_ID,
		@NPNC_ID,
		@InReplyToComment_ID,
		@Actioned,
		@ActionedBy_ID,
		@ActionedDate
	)

	SELECT Comment_ID = SCOPE_IDENTITY();
GO
