SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ContactCommentUpdate]
(
	@Comment_ID int,
	@Contact_ID int,
	@CommentBy_ID int,
	@CommentDate datetime,
	@Comment ntext,
	@CommentType_ID int,
	@CommentSource_ID int,
	@NPNC_ID int,
	@InReplyToComment_ID int = null,
	@Actioned bit,
	@ActionedBy_ID int,
	@ActionedDate datetime,
	@User_ID int = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin

	SET NOCOUNT ON
	
	UPDATE [Church_ContactComment]
	SET
		[Contact_ID] = @Contact_ID,
		[CommentBy_ID] = @CommentBy_ID,
		[CommentDate] = @CommentDate,
		[Comment] = @Comment,
		[CommentType_ID] = @CommentType_ID,
		[CommentSource_ID] = @CommentSource_ID,
		[NPNC_ID] = @NPNC_ID,
		[InReplyToComment_ID] = @InReplyToComment_ID,
		[Actioned] = @Actioned,
		[ActionedBy_ID] = @ActionedBy_ID,
		[ActionedDate] = @ActionedDate
	WHERE 
		[Comment_ID] = @Comment_ID
GO
