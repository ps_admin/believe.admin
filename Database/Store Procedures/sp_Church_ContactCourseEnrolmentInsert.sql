SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ContactCourseEnrolmentInsert]
(
	@Contact_ID int,
	@CourseInstance_ID int,
	@EnrolmentDate datetime,
	@Comments nvarchar(max),
	@IntroductoryEmailSentDate datetime = null,
	@Completed bit,
	@User_ID int
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin

	SET NOCOUNT ON

	INSERT INTO [Church_ContactCourseEnrolment]
	(
		[Contact_ID],
		[CourseInstance_ID],
		[Comments],
		[EnrolmentDate],
		[IntroductoryEmailSentDate],
		[Completed]
	)
	VALUES
	(
		@Contact_ID,
		@CourseInstance_ID,
		@Comments,
		@EnrolmentDate,
		@IntroductoryEmailSentDate,
		@Completed
	)

	SELECT SCOPE_IDENTITY()
GO
