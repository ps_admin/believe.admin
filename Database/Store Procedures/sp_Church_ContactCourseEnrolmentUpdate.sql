SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ContactCourseEnrolmentUpdate]
(
	@ContactCourseEnrolment_ID int,
	@Contact_ID int,
	@CourseInstance_ID int,
	@EnrolmentDate datetime,
	@Comments nvarchar(max),
	@IntroductoryEmailSentDate datetime = null,
	@Completed bit,
	@User_ID int
)
AS
	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin

	SET NOCOUNT ON
	
	UPDATE [Church_ContactCourseEnrolment]
	SET
		[Contact_ID] = @Contact_ID,
		[CourseInstance_ID] = @CourseInstance_ID,
		[EnrolmentDate] = @EnrolmentDate,
		[Comments] = @Comments,
		[IntroductoryEmailSentDate] = @IntroductoryEmailSentDate,
		[Completed] = @Completed
	WHERE 
		[ContactCourseEnrolment_ID] = @ContactCourseEnrolment_ID
GO
