SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ContactCourseInstanceUpdate]
(
	@ContactCourseInstance_ID int,
	@Contact_ID int,
	@CourseInstance_ID int,
	@DateAdded datetime,
	@User_ID int = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin

	SET NOCOUNT ON
	
	UPDATE [Church_ContactCourseInstance]
	SET
		[Contact_ID] = @Contact_ID,
		[CourseInstance_ID] = @CourseInstance_ID,
		[DateAdded] = @DateAdded
	WHERE 
		[ContactCourseInstance_ID] = @ContactCourseInstance_ID
GO
