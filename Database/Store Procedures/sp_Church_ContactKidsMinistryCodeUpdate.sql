SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ContactKidsMinistryCodeUpdate]
(
	@ContactKidsMinistryCode_ID int,
	@Contact_ID int,
	@KidsMinistryCode_ID int,
	@DateAdded datetime,
	@User_ID int = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin

	SET NOCOUNT ON
	
	UPDATE [Church_ContactKidsMinistryCode]
	SET
		[Contact_ID] = @Contact_ID,
		[KidsMinistryCode_ID] = @KidsMinistryCode_ID,
		DateAdded = @DateAdded
	WHERE 
		[ContactKidsMinistryCode_ID] = @ContactKidsMinistryCode_ID
GO
