SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ContactRoleInsert]
(
	@Contact_ID int,
	@Role_ID int,
	@Campus_ID int = 1,
	@DateAdded datetime,
	@User_ID int = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin

	SET NOCOUNT ON

	INSERT INTO [Church_ContactRole]
	(
		[Contact_ID],
		[Role_ID],
		[Campus_ID],
		[DateAdded]
	)
	VALUES
	(
		@Contact_ID,
		@Role_ID,
		@Campus_ID,
		@DateAdded
	)

	SELECT ContactRole_ID = SCOPE_IDENTITY();
GO
