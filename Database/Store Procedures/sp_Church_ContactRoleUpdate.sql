SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ContactRoleUpdate]
(
	@ContactRole_ID int,
	@Contact_ID int,
	@Role_ID int,
	@Campus_ID int = 1,
	@DateAdded datetime,
	@User_ID int = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin

	SET NOCOUNT ON
	
	UPDATE [Church_ContactRole]
	SET
		[Contact_ID] = @Contact_ID,
		[Role_ID] = @Role_ID,
		[Campus_ID] = @Campus_ID,
		[DateAdded] = @DateAdded
	WHERE 
		[ContactRole_ID] = @ContactRole_ID
GO
