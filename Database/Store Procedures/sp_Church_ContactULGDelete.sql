SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ContactULGDelete]
(
	@ContactULG_ID int
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Church_ContactULG]
	WHERE  [ContactULG_ID] = @ContactULG_ID
GO
