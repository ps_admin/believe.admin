SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ContactULGInsert]
(
	@Contact_ID int,
	@ULG_ID int,
	@DateJoined datetime,
	@Inactive bit,
	@SortIndex int
)
AS
	SET NOCOUNT ON

	INSERT INTO [Church_ContactULG]
	(
		[Contact_ID],
		[ULG_ID],
		[DateJoined],
		[Inactive],
		[SortIndex]
	)
	VALUES
	(
		@Contact_ID,
		@ULG_ID,
		@DateJoined,
		@Inactive,
		@SortIndex
	)

	SELECT ContactULG_ID = SCOPE_IDENTITY();
GO
