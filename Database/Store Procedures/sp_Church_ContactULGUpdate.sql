SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ContactULGUpdate]
(
	@ContactULG_ID int,
	@Contact_ID int,
	@ULG_ID int,
	@DateJoined datetime,
	@Inactive bit,
	@SortIndex int
)
AS
	SET NOCOUNT ON
	
	UPDATE [Church_ContactULG]
	SET
		[Contact_ID] = @Contact_ID,
		[ULG_ID] = @ULG_ID,
		[DateJoined] = @DateJoined,
		[Inactive] = @Inactive,
		[SortIndex] = @SortIndex
	WHERE 
		[ContactULG_ID] = @ContactULG_ID
GO
