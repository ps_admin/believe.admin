SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_CourseAttendanceList]

@Campus_ID	INT,
@StartDate	DATETIME,
@EndDate	DATETIME

AS

SELECT		DISTINCT CI.CourseInstance_ID,
			C.Name as CourseName,
			CI.Name as CourseDate,
			CONVERT(NVARCHAR(100),'') as Campus,
			NumberRegistered = ISNULL((SELECT COUNT(Distinct Contact_ID) FROM Church_ContactCourseEnrolment CE WHERE CE.CourseInstance_ID = CI.CourseInstance_ID), 0)
INTO		#t1
FROM		Church_Course C,
			Church_CourseInstance CI,
			Church_CourseSession CS,
			Church_CourseInstanceCampus CIC
WHERE		C.Course_ID = CI.Course_ID
AND			CI.CourseInstance_ID = CS.CourseInstance_ID
AND			((CS.Date >= @StartDate AND CS.Date <= @EndDate)
OR			(CS.Date > DATEADD(dd,-2, dbo.GetRelativeDate())))
AND			CI.CourseInstance_ID = CIC.CourseInstance_ID
AND			(CIC.Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
AND			CI.Deleted = 0 
AND			C.Deleted = 0
ORDER BY	C.Name


DECLARE	@CourseInstance_ID	INT,
		@CampusName			NVARCHAR(500)
		
DECLARE CampusCursor CURSOR FOR
	SELECT #t1.CourseInstance_ID, C.ShortName FROM Church_Campus C, Church_CourseInstanceCampus CIC, #t1
	WHERE C.Campus_ID = CIC.Campus_ID AND CIC.CourseInstance_ID = #t1.CourseInstance_ID
OPEN CampusCursor

FETCH NEXT FROM CampusCursor INTO @CourseInstance_ID, @CampusName
WHILE @@FETCH_STATUS = 0 BEGIN
	UPDATE #t1 SET Campus = Campus + @CampusName + ', ' WHERE CourseInstance_ID = @CourseInstance_ID
	FETCH NEXT FROM CampusCursor INTO @CourseInstance_ID, @CampusName
END

CLOSE CampusCursor
DEALLOCATE CampusCursor

UPDATE #t1 SET Campus = LEFT(Campus, LEN(Campus)-1) WHERE LEN(Campus)>2

SELECT * FROM #t1
GO
