SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_CourseDelete]
(
	@Course_ID int
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Church_Course]
	WHERE  [Course_ID] = @Course_ID
GO
