SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_CourseEnrolmentAttendanceDelete]
(
	@CourseEnrolmentAttendance_ID int
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM	[Church_CourseEnrolmentAttendance]
	WHERE	[CourseEnrolmentAttendance_ID] = @CourseEnrolmentAttendance_ID
GO
