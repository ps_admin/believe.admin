SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_CourseEnrolmentAttendanceInsert]
(
	@Contact_ID int,
	@CourseSession_ID int,
	@MarkedOffBy_ID int,
	@MarkedOffDateTime datetime,
	@User_ID	int
)
AS

	SET NOCOUNT ON


	IF (SELECT COUNT(*) FROM [Church_CourseEnrolmentAttendance] WHERE Contact_ID = @Contact_ID AND CourseSession_ID = @CourseSession_ID) = 0 BEGIN
	
		INSERT INTO [Church_CourseEnrolmentAttendance]
		(
			[Contact_ID],
			[CourseSession_ID],
			[MarkedOffBy_ID],
			[MarkedOffDateTime]
		)
		VALUES
		(
			@Contact_ID,
			@CourseSession_ID,
			@MarkedOffBy_ID,
			@MarkedOffDateTime
		)

		SELECT CourseEnrolmentAttendance_ID = SCOPE_IDENTITY();
		

		/* Log to the Contact's history (if required) */
		IF (SELECT C.LogCompletedSession FROM Church_CourseSession CS, Church_CourseInstance CI, Church_Course C WHERE CS.CourseInstance_ID = CI.CourseInstance_ID AND CI.Course_ID = C.Course_ID AND CS.CourseSession_ID = @CourseSession_ID) = 1 BEGIN
		
			DECLARE @Course			NVARCHAR(100),
					@CourseSession	NVARCHAR(100)
			SELECT @Course =		CONVERT(NVARCHAR,C.[Name],106) FROM Church_CourseSession CS, Church_CourseInstance CI, Church_Course C WHERE CS.CourseInstance_ID = CI.CourseInstance_ID AND CI.Course_ID = C.Course_ID AND CS.CourseSession_ID = @CourseSession_ID
			SELECT @CourseSession = CONVERT(NVARCHAR,[Date],106) FROM Church_CourseSession WHERE CourseSession_ID = @CourseSession_ID
		
			INSERT Church_ContactComment (Contact_ID,CommentBy_ID,CommentDate,Comment,CommentType_ID,CommentSource_ID,NPNC_ID,Actioned,ActionedBy_ID,ActionedDate)
			SELECT @Contact_ID, @User_ID, dbo.GetRelativeDate(), 'Attended ' + @Course + ' Course Session on ' + @CourseSession, 1, 1, null, 0, null, null
		
		END
	
	END
	ELSE BEGIN
	
		SELECT CourseEnrolmentAttendance_ID = 0
	
	END
GO
