SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_CourseEnrolmentAttendanceTopicsCoveredDelete]
(
	@CourseEnrolmentAttendanceTopicsCovered_ID int
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM	[Church_CourseEnrolmentAttendanceTopicsCovered]
	WHERE	[CourseEnrolmentAttendanceTopicsCovered_ID] = @CourseEnrolmentAttendanceTopicsCovered_ID
GO
