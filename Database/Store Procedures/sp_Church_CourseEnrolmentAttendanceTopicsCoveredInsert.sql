SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_CourseEnrolmentAttendanceTopicsCoveredInsert]
(
	@CourseEnrolmentAttendance_ID int,
	@CourseTopic_ID int
)
AS
	SET NOCOUNT ON

	INSERT INTO [Church_CourseEnrolmentAttendanceTopicsCovered]
	(
		[CourseEnrolmentAttendance_ID],
		[CourseTopic_ID]
	)
	VALUES
	(
		@CourseEnrolmentAttendance_ID,
		@CourseTopic_ID
	)

	SELECT CourseEnrolmentAttendanceTopicsCovered_ID = SCOPE_IDENTITY();
GO
