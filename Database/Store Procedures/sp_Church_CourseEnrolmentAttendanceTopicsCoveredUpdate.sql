SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_CourseEnrolmentAttendanceTopicsCoveredUpdate]
(
	@CourseEnrolmentAttendanceTopicsCovered_ID int,
	@CourseEnrolmentAttendance_ID int,
	@CourseTopic_ID int
)
AS
	SET NOCOUNT ON
	
	UPDATE [Church_CourseEnrolmentAttendanceTopicsCovered]
	SET
		[CourseEnrolmentAttendance_ID] = @CourseEnrolmentAttendance_ID,
		[CourseTopic_ID] = @CourseTopic_ID
	WHERE 
		[CourseEnrolmentAttendanceTopicsCovered_ID] = @CourseEnrolmentAttendanceTopicsCovered_ID
GO
