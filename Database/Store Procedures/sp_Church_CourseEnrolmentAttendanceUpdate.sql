SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_CourseEnrolmentAttendanceUpdate]
(
	@CourseEnrolmentAttendance_ID int,
	@Contact_ID int,
	@CourseSession_ID int,
	@MarkedOffBy_ID int,
	@MarkedOffDateTime datetime
)
AS
	SET NOCOUNT ON
	
	UPDATE [Church_CourseEnrolmentAttendance]
	SET
		[Contact_ID] = @Contact_ID,
		[CourseSession_ID] = @CourseSession_ID,
		[MarkedOffBy_ID] = @MarkedOffBy_ID,
		[MarkedOffDateTime] = @MarkedOffDateTime
	WHERE 
		[CourseEnrolmentAttendance_ID] = @CourseEnrolmentAttendance_ID
GO
