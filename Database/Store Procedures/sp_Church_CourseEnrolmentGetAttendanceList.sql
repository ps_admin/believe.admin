SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_CourseEnrolmentGetAttendanceList]

@CourseInstance_ID	int

AS

SELECT	CEA.*
FROM	Church_CourseEnrolmentAttendance CEA,
		Church_CourseSession CS
WHERE	CEA.CourseSession_ID = CS.CourseSession_ID
AND		CS.CourseInstance_ID = @CourseInstance_ID

SELECT	TC.*
FROM	Church_CourseEnrolmentAttendanceTopicsCovered TC,
		Church_CourseEnrolmentAttendance CEA,
		Church_CourseSession CS
WHERE	TC.CourseEnrolmentAttendance_ID = CEA.CourseEnrolmentAttendance_ID
AND		CEA.CourseSession_ID = CS.CourseSession_ID
AND		CS.CourseInstance_ID = @CourseInstance_ID
GO
