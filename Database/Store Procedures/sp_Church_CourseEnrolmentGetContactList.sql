SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_CourseEnrolmentGetContactList]

@CourseInstance_ID	int

AS

SELECT		DISTINCT C.Contact_ID,
			C.FirstName,
			C.LastName,
			C.Email,
			C.Mobile,
			[Ministry] = ISNULL((SELECT Name FROM Church_Ministry WHERE Ministry_ID = CI.Ministry_ID),''),
			[Region] = ISNULL((SELECT Name FROM Church_Region WHERE Region_ID = CI.Region_ID),''),
			CE.Comments
FROM		Church_ContactCourseEnrolment CE,
			Common_ContactInternal CI,
			Common_Contact C
WHERE		CE.Contact_ID = C.Contact_ID
AND			CE.Contact_ID = CI.Contact_ID
AND			CE.CourseInstance_ID = @CourseInstance_ID
ORDER BY	C.LastName,
			C.FirstName
GO
