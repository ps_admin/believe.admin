SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_CourseEnrolmentMarkCompletedCheck]

@User_ID INT

AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin


	SELECT		Contact_ID, 
				C.Course_ID,
				(SELECT MIN(CourseInstance_ID) FROM Church_ContactCourseEnrolment WHERE EnrolmentDate = MIN(CCE.EnrolmentDate) AND Contact_ID = CCE.Contact_ID) AS CourseInstance_ID,
				C.PointsRequiredForCompletion,
				CONVERT(INT, 0) as PointsCompleted
	INTO		#t1
	FROM		Church_ContactCourseEnrolment CCE,
				Church_CourseInstance CI,
				Church_Course C
	WHERE		CCE.CourseInstance_ID = CI.CourseInstance_ID
	AND			CI.Course_ID = C.Course_ID
	AND			C.PointsRequiredForCompletion > 0
	AND			CCE.Completed = 0
	GROUP BY	CCE.Contact_ID,
				C.Course_ID,
				C.PointsRequiredForCompletion

	SELECT		DISTINCT Contact_ID, 
				CI.Course_ID,
				CT.CourseTopic_ID, 
				CT.PointWeighting 
	INTO		#t2
	FROM		Church_CourseEnrolmentAttendance CEA,
				Church_CourseEnrolmentAttendanceTopicsCovered TC,
				Church_CourseTopic CT,
				Church_CourseSession CS,
				Church_CourseInstance CI
	WHERE		CEA.CourseEnrolmentAttendance_ID = TC.CourseEnrolmentAttendance_ID
	AND			TC.CourseTopic_ID = CT.CourseTopic_ID
	AND			CEA.CourseSession_ID = CS.CourseSession_ID
	AND			CS.CourseInstance_ID = CI.CourseInstance_ID

	UPDATE		#t1
	SET			PointsCompleted = ISNULL((
					SELECT	SUM(PointWeighting)
					FROM	#t2
					WHERE	#t2.Course_ID = #t1.Course_ID
					AND		#t2.Contact_ID = #t1.Contact_ID),0)
					
					
	DELETE FROM #t1 WHERE PointsCompleted < PointsRequiredForCompletion

	DELETE FROM #t1 WHERE Exists (	SELECT	CI.CourseInstance_ID
									FROM	Church_ContactCourseInstance CCI,
											Church_CourseInstance CI
									WHERE	CCI.CourseInstance_ID = CI.CourseInstance_ID
									AND		CCI.Contact_ID = #t1.Contact_ID
									AND		CI.Course_ID = #t1.Course_ID)


	INSERT Church_ContactCourseInstance (Contact_ID, CourseInstance_ID, DateAdded)
	SELECT	Contact_ID, 
			CourseInstance_ID,
			dbo.GetRelativeDate()
	FROM	#t1
	
	INSERT Common_ContactHistory (Contact_ID, Module, [Desc], Desc2, DateChanged, User_ID, Action, Item, [Table], FieldName, OldValue, OldText, NewValue, NewText)
	SELECT Contact_ID, 'Church', '', '', dbo.GetRelativeDate(), @User_ID, '', 'Course', '', '', '', '', '', 'The previous entry was an automatic addition as the contact has now completed the course.'
	FROM	#t1
	
	UPDATE	Church_ContactCourseEnrolment
	SET		Completed = 1
	FROM	Church_ContactCourseEnrolment CCE,
			Church_CourseInstance CI,
			#t1
	WHERE	CCE.Contact_ID = #t1.Contact_ID
	AND		CCE.CourseInstance_ID = CI.CourseInstance_ID
	AND		CI.Course_ID = #t1.Course_ID
GO
