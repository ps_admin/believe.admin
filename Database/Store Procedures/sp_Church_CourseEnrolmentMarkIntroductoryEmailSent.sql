SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_CourseEnrolmentMarkIntroductoryEmailSent]

@Contact_ID			INT,
@CourseInstance_ID	INT

AS

UPDATE		Church_ContactCourseEnrolment
SET			IntroductoryEmailSentDate = dbo.GetRelativeDate()
WHERE		Contact_ID = @Contact_ID
AND			CourseInstance_ID = @CourseInstance_ID
GO
