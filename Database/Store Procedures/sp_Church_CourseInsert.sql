SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_CourseInsert]
(
	@Name nvarchar(50),
	@PointsRequiredForCompletion int,
	@ShowInCustomReport bit,
	@LogCompletedSession bit = 0,
	@Deleted bit,
	@SortOrder int
)
AS
	SET NOCOUNT ON

	INSERT INTO [Church_Course]
	(
		[Name],
		[PointsRequiredForCompletion],
		[ShowInCustomReport],
		[LogCompletedSession],
		[Deleted],
		[SortOrder]
	)
	VALUES
	(
		@Name,
		@PointsRequiredForCompletion,
		@ShowInCustomReport,
		@LogCompletedSession,
		@Deleted,
		@SortOrder
	)

	SELECT Course_ID = SCOPE_IDENTITY();
GO
