SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_CourseInstanceCampusDelete]
(
	@CourseInstanceCampus_ID int
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM	[Church_CourseInstanceCampus]
	WHERE	[CourseInstanceCampus_ID] = @CourseInstanceCampus_ID
GO
