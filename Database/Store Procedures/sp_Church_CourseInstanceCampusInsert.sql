SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_CourseInstanceCampusInsert]
(
	@CourseInstance_ID int,
	@Campus_ID int
)
AS
	SET NOCOUNT ON

	INSERT INTO [Church_CourseInstanceCampus]
	(
		[CourseInstance_ID],
		[Campus_ID]
	)
	VALUES
	(
		@CourseInstance_ID,
		@Campus_ID
	)

	SELECT CourseInstanceCampus_ID = SCOPE_IDENTITY();
GO
