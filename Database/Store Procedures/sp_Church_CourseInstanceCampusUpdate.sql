SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_CourseInstanceCampusUpdate]
(
	@CourseInstanceCampus_ID int,
	@CourseInstance_ID int,
	@Campus_ID int
)
AS
	SET NOCOUNT ON
	
	UPDATE [Church_CourseInstanceCampus]
	SET
		[CourseInstance_ID] = @CourseInstance_ID,
		[Campus_ID] = @Campus_ID
	WHERE 
		[CourseInstanceCampus_ID] = @CourseInstanceCampus_ID
GO
