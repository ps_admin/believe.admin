SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_CourseInstanceDelete]
(
	@CourseInstance_ID int
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Church_CourseInstance]
	WHERE  [CourseInstance_ID] = @CourseInstance_ID

	RETURN @@Error
GO
