SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_CourseInstanceInsert]
(
	@Course_ID int,
	@Name nvarchar(50),
	@StartDate datetime,
	@Deleted bit
)
AS
	SET NOCOUNT ON

	INSERT INTO [Church_CourseInstance]
	(
		[Course_ID],
		[Name],
		[StartDate],
		[Deleted]
	)
	VALUES
	(
		@Course_ID,
		@Name,
		@StartDate,
		@Deleted
	)

	SELECT CourseInstance_ID = SCOPE_IDENTITY();
GO
