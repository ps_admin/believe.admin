SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_CourseInstanceUpdate]
(
	@CourseInstance_ID int,
	@Course_ID int,
	@Name nvarchar(50),
	@StartDate datetime,
	@Deleted bit
)
AS
	SET NOCOUNT ON
	
	UPDATE [Church_CourseInstance]
	SET
		[Course_ID] = @Course_ID,
		[Name] = @Name,
		[StartDate] = @StartDate,
		[Deleted] = @Deleted
	WHERE 
		[CourseInstance_ID] = @CourseInstance_ID
GO
