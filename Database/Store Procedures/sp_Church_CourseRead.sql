SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_CourseRead]

@Course_ID	INT	

AS

SELECT		*
FROM		Church_Course
WHERE		Course_ID = @Course_ID

SELECT		*
FROM		Church_CourseInstance
WHERE		Course_ID = @Course_ID

SELECT		*
FROM		Church_CourseTopic
WHERE		Course_ID = @Course_ID
ORDER BY	Name DESC

SELECT		CS.*
FROM		Church_CourseSession CS,
			Church_CourseInstance CI
WHERE		CS.CourseInstance_ID = CI.CourseInstance_ID
AND			CI.Course_ID = @Course_ID
ORDER BY	CS.Date ASC


SELECT		CST.*
FROM		Church_CourseSessionTopic CST,
			Church_CourseSession CS,
			Church_CourseInstance CI
WHERE		CST.CourseSession_ID = CS.CourseSession_ID
AND			CS.CourseInstance_ID = CI.CourseInstance_ID
AND			CI.Course_ID = @Course_ID
GO
