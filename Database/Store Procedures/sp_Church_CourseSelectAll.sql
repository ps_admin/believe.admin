SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_CourseSelectAll]

AS

SELECT		*
FROM		Church_Course
ORDER BY	SortOrder ASC

SELECT		*
FROM		Church_CourseInstance

SELECT		*
FROM		Church_CourseTopic
ORDER BY	Name DESC

SELECT		*
FROM		Church_CourseSession
ORDER BY	[Date] DESC

SELECT		*
FROM		Church_CourseSessionTopic
GO
