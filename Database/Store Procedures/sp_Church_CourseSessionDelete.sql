SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_CourseSessionDelete]
(
	@CourseSession_ID int
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM	[Church_CourseSession]
	WHERE	[CourseSession_ID] = @CourseSession_ID
GO
