SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_CourseSessionInsert]
(
	@CourseInstance_ID int,
	@Date datetime,
	@Deleted bit
)
AS
	SET NOCOUNT ON

	INSERT INTO [Church_CourseSession]
	(
		[CourseInstance_ID],
		[Date],
		[Deleted]
	)
	VALUES
	(
		@CourseInstance_ID,
		@Date,
		@Deleted
	)

	SELECT CourseSession_ID = SCOPE_IDENTITY();
GO
