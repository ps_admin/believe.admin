SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_CourseSessionTopicDelete]
(
	@CourseSessionTopic_ID int
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM	[Church_CourseSessionTopic]
	WHERE	[CourseSessionTopic_ID] = @CourseSessionTopic_ID
GO
