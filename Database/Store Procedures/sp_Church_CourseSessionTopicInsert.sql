SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_CourseSessionTopicInsert]
(
	@CourseInstance_ID int = NULL,
	@CourseSession_ID int,
	@CourseTopic_ID int
)
AS
	SET NOCOUNT ON

	INSERT INTO [Church_CourseSessionTopic]
	(
		[CourseInstance_ID],
		[CourseSession_ID],
		[CourseTopic_ID]
	)
	VALUES
	(
		@CourseInstance_ID,
		@CourseSession_ID,
		@CourseTopic_ID
	)

	SELECT CourseSessionTopic_ID = SCOPE_IDENTITY();
GO
