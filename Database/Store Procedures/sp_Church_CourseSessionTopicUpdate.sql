SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_CourseSessionTopicUpdate]
(
	@CourseSessionTopic_ID int,
	@CourseInstance_ID int = NULL,
	@CourseSession_ID int,
	@CourseTopic_ID int
)
AS
	SET NOCOUNT ON
	
	UPDATE [Church_CourseSessionTopic]
	SET
		[CourseInstance_ID] = @CourseInstance_ID,
		[CourseSession_ID] = @CourseSession_ID,
		[CourseTopic_ID] = @CourseTopic_ID
	WHERE 
		[CourseSessionTopic_ID] = @CourseSessionTopic_ID
GO
