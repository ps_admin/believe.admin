SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_CourseSessionUpdate]
(
	@CourseSession_ID int,
	@CourseInstance_ID int,
	@Date datetime,
	@Deleted bit
)
AS
	SET NOCOUNT ON
	
	UPDATE [Church_CourseSession]
	SET
		[CourseInstance_ID] = @CourseInstance_ID,
		[Date] = @Date,
		[Deleted] = @Deleted
	WHERE 
		[CourseSession_ID] = @CourseSession_ID
GO
