SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_CourseTopicDelete]
(
	@CourseTopic_ID int
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM	[Church_CourseTopic]
	WHERE	[CourseTopic_ID] = @CourseTopic_ID
GO
