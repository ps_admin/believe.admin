SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_CourseTopicInsert]
(
	@Course_ID int,
	@Name nvarchar(50),
	@PointWeighting int,
	@Deleted bit
)
AS
	SET NOCOUNT ON

	INSERT INTO [Church_CourseTopic]
	(
		[Course_ID],
		[Name],
		[PointWeighting],
		[Deleted]
	)
	VALUES
	(
		@Course_ID,
		@Name,
		@PointWeighting,
		@Deleted
	)

	SELECT CourseTopic_ID = SCOPE_IDENTITY();
GO
