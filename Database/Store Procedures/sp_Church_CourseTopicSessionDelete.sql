SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_CourseTopicSessionDelete]
(
	@CourseTopicSession_ID int
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM	[Church_CourseTopicSession]
	WHERE	[CourseTopicSession_ID] = @CourseTopicSession_ID
GO
