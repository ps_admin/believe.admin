SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_CourseTopicSessionInsert]
(
	@CourseSession_ID int,
	@CourseTopic_ID int,
	@SortOrder int
)
AS
	SET NOCOUNT ON

	INSERT INTO [Church_CourseTopicSession]
	(
		[CourseSession_ID],
		[CourseTopic_ID],
		[SortOrder]
	)
	VALUES
	(
		@CourseSession_ID,
		@CourseTopic_ID,
		@SortOrder
	)

	SELECT CourseTopicSession_ID = SCOPE_IDENTITY();
GO
