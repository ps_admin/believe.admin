SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_CourseTopicSessionUpdate]
(
	@CourseTopicSession_ID int,
	@CourseSession_ID int,
	@CourseTopic_ID int,
	@SortOrder int
)
AS
	SET NOCOUNT ON
	
	UPDATE [Church_CourseTopicSession]
	SET
		[CourseSession_ID] = @CourseSession_ID,
		[CourseTopic_ID] = @CourseTopic_ID,
		[SortOrder] = @SortOrder
	WHERE 
		[CourseTopicSession_ID] = @CourseTopicSession_ID
GO
