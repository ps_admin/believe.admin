SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_CourseTopicUpdate]
(
	@CourseTopic_ID int,
	@Course_ID int,
	@Name nvarchar(50),
	@PointWeighting int,
	@Deleted bit
)
AS
	SET NOCOUNT ON
	
	UPDATE [Church_CourseTopic]
	SET
		[Course_ID] = @Course_ID,
		[Name] = @Name,
		[PointWeighting] = @PointWeighting,
		[Deleted] = @Deleted
	WHERE 
		[CourseTopic_ID] = @CourseTopic_ID
GO
