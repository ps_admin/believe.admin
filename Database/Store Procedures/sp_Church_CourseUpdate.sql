SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_CourseUpdate]
(
	@Course_ID int,
	@PointsRequiredForCompletion int,
	@Name nvarchar(50),
	@ShowInCustomReport bit,
	@LogCompletedSession bit = 0,
	@Deleted bit,
	@SortOrder int
)
AS
	SET NOCOUNT ON
	
	UPDATE [Church_Course]
	SET
		[Name] = @Name,
		[Deleted] = @Deleted,
		[PointsRequiredForCompletion] = @PointsRequiredForCompletion,
		[ShowInCustomReport] = @ShowInCustomReport,
		[LogCompletedSession] = @LogCompletedSession,
		[SortOrder] = @SortOrder
	WHERE 
		[Course_ID] = @Course_ID
GO
