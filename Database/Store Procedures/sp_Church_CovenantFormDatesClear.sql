SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_CovenantFormDatesClear]

@Campus_ID		INT

AS

UPDATE	Common_ContactInternal
SET		CovenantFormDate = null
WHERE	Campus_ID = @Campus_ID
GO
