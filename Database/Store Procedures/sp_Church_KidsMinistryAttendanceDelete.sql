SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_KidsMinistryAttendanceDelete]
(
	@KidsMinistryAttendance_ID int
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Church_KidsMinistryAttendance]
	WHERE  [KidsMinistryAttendance_ID] = @KidsMinistryAttendance_ID
GO
