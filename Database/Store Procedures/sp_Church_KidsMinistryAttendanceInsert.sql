SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_KidsMinistryAttendanceInsert]
(
	@Contact_ID int,
	@PCRDate_ID int,
	@Date datetime,
	@ChurchService_ID int,
	@CheckInTime datetime,
	@CheckedInByParent_ID int,
	@CheckedInByLeader_ID int,
	@CheckOutTime datetime,
	@CheckedOutByParent_ID int,
	@CheckedOutByLeader_ID int
)
AS
	SET NOCOUNT ON

	INSERT INTO [Church_KidsMinistryAttendance]
	(
		[Contact_ID],
		[PCRDate_ID],
		[Date], 
		[ChurchService_ID],
		[CheckInTime],
		[CheckedInByParent_ID],
		[CheckedInByLeader_ID],
		[CheckOutTime],
		[CheckedOutByParent_ID],
		[CheckedOutByLeader_ID]
	)
	VALUES
	(
		@Contact_ID,
		@PCRDate_ID,
		@Date,
		@ChurchService_ID,
		@CheckInTime,
		@CheckedInByParent_ID,
		@CheckedInByLeader_ID,
		@CheckOutTime,
		@CheckedOutByParent_ID,
		@CheckedOutByLeader_ID
	)

	SELECT KidsMinistryAttendance_ID = SCOPE_IDENTITY();
GO
