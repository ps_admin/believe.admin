SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_KidsMinistryAttendanceLeaderDelete]
(
	@KidsMinistryAttendanceLeader_ID int
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Church_KidsMinistryAttendanceLeader]
	WHERE  [KidsMinistryAttendanceLeader_ID] = @KidsMinistryAttendanceLeader_ID
GO
