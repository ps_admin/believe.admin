SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_KidsMinistryAttendanceLeaderInsert]
(
	@Contact_ID int,
	@Date datetime,
	@ChurchService_ID int,
	@CheckInTime datetime,
	@CheckOutTime datetime = NULL
)
AS
	SET NOCOUNT ON

	INSERT INTO [Church_KidsMinistryAttendanceLeader]
	(
		[Contact_ID],
		[Date],
		[ChurchService_ID],
		[CheckInTime],
		[CheckOutTime]
	)
	VALUES
	(
		@Contact_ID,
		@Date,
		@ChurchService_ID,
		@CheckInTime,
		@CheckOutTime
	)

	SELECT KidsMinistryAttendanceLeader_ID = SCOPE_IDENTITY();
GO
