SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_KidsMinistryAttendanceLeaderList]

@ChurchService_ID	int,
@Date				datetime

AS

SELECT		C.Contact_ID,
			C.FirstName,
			C.LastName,
			L.CheckinTime as CheckinTime,
			L.CheckoutTime as CheckoutTime,
			KidsMinistryAttendanceLeader_ID
FROM		Common_Contact C
				JOIN Church_ContactRole R ON C.Contact_ID = R.Contact_ID
				LEFT JOIN Church_KidsMinistryAttendanceLeader L
					ON L.KidsMinistryAttendanceLeader_ID = (SELECT TOP 1 KidsMinistryAttendanceLeader_ID
															 FROM	Church_KidsMinistryAttendanceLeader L
															 WHERE	L.Contact_ID = C.Contact_ID
															 AND	L.ChurchService_ID = @ChurchService_ID
															 AND	CONVERT(NVARCHAR, L.[Date], 106) = CONVERT(NVARCHAR, @Date, 106)
															 ORDER BY L.[Date] DESC)
WHERE		R.Role_ID = 22
ORDER BY	C.LastName
GO
