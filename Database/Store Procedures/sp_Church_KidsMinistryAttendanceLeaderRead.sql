SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_KidsMinistryAttendanceLeaderRead]
(
	@KidsMinistryAttendanceLeader_ID int
)
AS
	SET NOCOUNT ON

	SELECT	*
	FROM	[Church_KidsMinistryAttendanceLeader]
	WHERE	KidsMinistryAttendanceLeader_ID = @KidsMinistryAttendanceLeader_ID
GO
