SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_KidsMinistryAttendanceLeaderUpdate]
(
	@KidsMinistryAttendanceLeader_ID int,
	@Contact_ID int,
	@Date datetime,
	@ChurchService_ID int,
	@CheckInTime datetime,
	@CheckOutTime datetime = NULL
)
AS
	SET NOCOUNT ON
	
	UPDATE [Church_KidsMinistryAttendanceLeader]
	SET
		[Contact_ID] = @Contact_ID,
		[Date] = @Date,
		[ChurchService_ID] = @ChurchService_ID,
		[CheckInTime] = @CheckInTime,
		[CheckOutTime] = @CheckOutTime
	WHERE 
		[KidsMinistryAttendanceLeader_ID] = @KidsMinistryAttendanceLeader_ID
GO
