SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_KidsMinistryAttendanceUpdate]
(
	@KidsMinistryAttendance_ID int,
	@Contact_ID int,
	@PCRDate_ID int,
	@Date datetime,
	@ChurchService_ID int,
	@CheckInTime datetime,
	@CheckedInByParent_ID int,
	@CheckedInByLeader_ID int,
	@CheckOutTime datetime,
	@CheckedOutByParent_ID int,
	@CheckedOutByLeader_ID int
)
AS
	SET NOCOUNT ON
	
	UPDATE [Church_KidsMinistryAttendance]
	SET
		[Contact_ID] = @Contact_ID,
		[PCRDate_ID] = @PCRDate_ID,
		[Date] = @Date,
		[ChurchService_ID] = @ChurchService_ID,
		[CheckInTime] = @CheckInTime,
		[CheckedInByParent_ID] = @CheckedInByParent_ID,
		[CheckedInByLeader_ID] = @CheckedInByLeader_ID,
		[CheckOutTime] = @CheckOutTime,
		[CheckedOutByParent_ID] = @CheckedOutByParent_ID,
		[CheckedOutByLeader_ID] = @CheckedOutByLeader_ID
	WHERE 
		[KidsMinistryAttendance_ID] = @KidsMinistryAttendance_ID
GO
