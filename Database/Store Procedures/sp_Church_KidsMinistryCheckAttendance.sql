SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_KidsMinistryCheckAttendance]

@Contact_ID			int,
@Date				datetime,
@ChurchService_ID	int

AS

SELECT		A.*,
			ISNULL(CIP.FirstName + ' ' + CIP.LastName, '') as CheckedInByParentName,
			ISNULL(CIL.FirstName + ' ' + CIL.LastName, '') as CheckedInByLeaderName,
			ISNULL(COP.FirstName + ' ' + COP.LastName, '') as CheckedOutByParentName,
			ISNULL(COL.FirstName + ' ' + COL.LastName, '') as CheckedOutByLeaderName
FROM		Church_KidsMinistryAttendance A
				LEFT JOIN Common_Contact COP ON A.CheckedOutByParent_ID = COP.Contact_ID
				LEFT JOIN Common_Contact COL ON A.CheckedOutByLeader_ID = COP.Contact_ID
				JOIN Common_Contact CIP ON A.CheckedInByParent_ID = CIP.Contact_ID
				JOIN Common_Contact CIL ON A.CheckedInByLeader_ID = CIL.Contact_ID
WHERE		A.Contact_ID = @Contact_ID
AND			[Date] = @Date
AND			ChurchService_ID = @ChurchService_ID
GO
