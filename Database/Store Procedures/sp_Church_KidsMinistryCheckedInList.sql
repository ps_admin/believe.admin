SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_KidsMinistryCheckedInList]

@ChurchService_ID		INT,
@Date					DATETIME

AS

DECLARE @SQL			VARCHAR(8000)
DECLARE @SQLSelect		VARCHAR(8000)
DECLARE @SQLWhere		VARCHAR(8000)
DECLARE @Contact_ID		INT
DECLARE @Clearance_ID	INT

SELECT	DISTINCT A.Contact_ID,
		C.FirstName,
		C.LastName,
		dbo.Common_GetAge(C.DateOfBirth) as Age,
		CONVERT(NVARCHAR(500),'') AS Clearance
INTO	#t1
FROM	Common_Contact C,
		Church_KidsMinistryAttendance A
WHERE	C.Contact_ID = A.Contact_ID
AND		A.CheckOutTime IS NULL
AND		[Date] = @Date
AND		ChurchService_ID = @ChurchService_ID


DECLARE ContactCursor CURSOR FOR
	SELECT Contact_ID FROM #t1
OPEN ContactCursor

FETCH NEXT FROM ContactCursor INTO @Contact_ID
WHILE (@@Fetch_Status <> -1) BEGIN

	DECLARE ClearanceCursor CURSOR FOR 
		SELECT ClearanceContact_ID FROM Church_KidsMinistryClearance WHERE Contact_ID = @Contact_ID
	OPEN ClearanceCursor
	
	FETCH NEXT FROM ClearanceCursor INTO @Clearance_ID
	WHILE (@@Fetch_Status <> -1) BEGIN
		UPDATE #t1 SET Clearance = Clearance + (SELECT FirstName + ' ' + LastName FROM Common_Contact WHERE Contact_ID = @Clearance_ID) + ', ' WHERE Contact_ID = @Contact_ID
		FETCH NEXT FROM ClearanceCursor INTO @Clearance_ID
	END
	
	CLOSE ClearanceCursor
	DEALLOCATE ClearanceCursor

	FETCH NEXT FROM ContactCursor INTO @Contact_ID
END

CLOSE ContactCursor
DEALLOCATE ContactCursor

UPDATE #t1 SET Clearance = LEFT(Clearance, LEN(Clearance) - 1) WHERE LEN(Clearance)> 0 

SELECT * FROM #t1

SELECT	Cl.Contact_ID, Cl.ClearanceContact_ID, C.FirstName + ' ' + C.LastName as [Name] 
FROM	Church_KidsMinistryClearance Cl, Common_Contact C
WHERE	Cl.ClearanceContact_ID = C.Contact_ID
AND		Cl.Contact_ID IN (SELECT Contact_ID FROM #t1)
GO
