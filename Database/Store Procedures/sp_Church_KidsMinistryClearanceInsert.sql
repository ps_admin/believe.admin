SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_KidsMinistryClearanceInsert]
(
	@Contact_ID int,
	@ClearanceContact_ID int,
	@DateAdded	datetime,
	@User_ID int = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin

	SET NOCOUNT ON

	INSERT INTO [Church_KidsMinistryClearance]
	(
		[Contact_ID],
		[ClearanceContact_ID],
		[DateAdded]
	)
	VALUES
	(
		@Contact_ID,
		@ClearanceContact_ID,
		@DateAdded
	)

	SELECT KidsMinistryClearance_ID = SCOPE_IDENTITY();
GO
