SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_KidsMinistryClearanceUpdate]
(
	@KidsMinistryClearance_ID int,
	@Contact_ID int,
	@ClearanceContact_ID int,
	@DateAdded datetime,
	@User_ID int = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin

	SET NOCOUNT ON
	
	UPDATE [Church_KidsMinistryClearance]
	SET
		[Contact_ID] = @Contact_ID,
		[ClearanceContact_ID] = @ClearanceContact_ID,
		[DateAdded] = @DateAdded
	WHERE 
		[KidsMinistryClearance_ID] = @KidsMinistryClearance_ID
GO
