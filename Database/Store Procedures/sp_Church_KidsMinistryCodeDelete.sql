SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_KidsMinistryCodeDelete]
(
	@KidsMinistryCode_ID int
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Church_KidsMinistryCode]
	WHERE  [KidsMinistryCode_ID] = @KidsMinistryCode_ID
GO
