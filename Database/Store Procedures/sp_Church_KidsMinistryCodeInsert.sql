SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_KidsMinistryCodeInsert]
(
	@Name nvarchar(100),
	@Color nvarchar(100),
	@Image image = NULL,
	@SortOrder int
)
AS
	SET NOCOUNT ON

	INSERT INTO [Church_KidsMinistryCode]
	(
		[Name],
		[Color],
		[Image],
		[SortOrder]
	)
	VALUES
	(
		@Name,
		@Color,
		@Image,
		@SortOrder
	)

	SELECT KidsMinistryCode_ID = SCOPE_IDENTITY();
GO
