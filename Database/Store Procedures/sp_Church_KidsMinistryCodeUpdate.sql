SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_KidsMinistryCodeUpdate]
(
	@KidsMinistryCode_ID int,
	@Name nvarchar(100),
	@Color nvarchar(100),
	@Image image = NULL,
	@SortOrder int
)
AS
	SET NOCOUNT ON
	
	UPDATE [Church_KidsMinistryCode]
	SET
		[Name] = @Name,
		[Color] = @Color,
		[Image] = @Image,
		[SortOrder] = @SortOrder
	WHERE 
		[KidsMinistryCode_ID] = @KidsMinistryCode_ID
GO
