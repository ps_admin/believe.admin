SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_KidsMinistryFamilySearch]

@Name					NVARCHAR(40) = '',
@Campus_ID				INT = 1,
@ChurchService_ID		INT,
@Date					DATETIME

AS

DECLARE @SQL			VARCHAR(8000)
DECLARE @SQLSelect		VARCHAR(8000)
DECLARE @SQLWhere		VARCHAR(8000)
DECLARE @Family_ID		INT
DECLARE @Contact_ID		INT

CREATE TABLE #t1 (
	Family_ID	INT
)


/********************************
*       SQL Select String		*
*********************************/
SET @SQLSelect = '	
INSERT INTO #t1
SELECT	DISTINCT Family_ID
FROM	Common_Contact C,
		Common_ContactInternal CI'
		
SET @SQLWhere = '
WHERE	C.Contact_ID = CI.Contact_ID
AND		CI.Campus_ID = ' + CONVERT(NVARCHAR, @Campus_ID) + '
AND		Family_ID IS NOT NULL'

IF (@Name <> '') BEGIN

	DECLARE @Text2 NVARCHAR(1000)

	DECLARE QuickSearchCursor CURSOR FOR 
		SELECT strval FROM dbo.SplitString(@Name,' ')

	OPEN QuickSearchCursor
		
	SET @SQLWhere = @SQLWhere + '	AND ('
	FETCH NEXT FROM QuickSearchCursor INTO @Text2
	WHILE (@@Fetch_Status <> -1) BEGIN
			SET @SQLWhere = @SQLWhere + '(C.FirstName LIKE ''%' + @Text2 + '%'' OR C.LastName LIKE ''%' + @Text2 + '%'' OR CONVERT(NVARCHAR,C.Contact_ID) LIKE ''%' + @Text2 + '%'' OR C.Mobile LIKE ''%' + @Text2 + '%'' OR C.Email LIKE ''%' + @Text2 + '%'' OR C.Email2 LIKE ''%' + @Text2 + '%'') AND'
		FETCH NEXT FROM QuickSearchCursor INTO @Text2
	END
	SET @SQLWhere = LEFT(@SQLWhere, LEN(@SQLWhere) - 4) + ')'

	Close QuickSearchCursor
	Deallocate QuickSearchCursor

END

SET @SQL = @SQLSelect + CHAR(10) + @SQLWhere
EXEC(@SQL)


SELECT	Family_ID,
		(SELECT TOP 1 LastName FROM Common_Contact WHERE Family_ID = #t1.Family_ID ORDER BY FamilyMemberType_ID ASC) as FamilyName,
		CONVERT(NVARCHAR(500),'') as Members,
		(SELECT COUNT(*) FROM Common_Contact WHERE Family_ID = #t1.Family_ID) as NumberOfMembers,
		(SELECT COUNT(*) FROM Common_Contact C, Common_ContactInternal CC 
			WHERE	C.Contact_ID = CC.Contact_ID 
			AND		C.Family_ID = #t1.Family_ID
			AND		CC.Ministry_ID = 1) as Children,
		(SELECT COUNT(*) FROM Church_KidsMinistryAttendance A, Common_Contact C
			WHERE	A.Contact_ID = C.Contact_ID 
			AND		A.ChurchService_ID = @ChurchService_ID
			AND		CONVERT(NVARCHAR, A.[Date], 106) = CONVERT(NVARCHAR, @Date, 106)
			AND		A.CheckOutTime IS NULL
			AND		C.Family_ID = #t1.Family_ID) as ChildrenCheckedIn
INTO	#t2
FROM	#t1


DECLARE FamilyCursor CURSOR FOR 
	SELECT Family_ID FROM #t2
OPEN FamilyCursor
	
FETCH NEXT FROM FamilyCursor INTO @Family_ID
WHILE (@@Fetch_Status <> -1) BEGIN

	DECLARE MemberCursor CURSOR FOR 
		SELECT Contact_ID FROM Common_Contact WHERE Family_ID = @Family_ID ORDER BY FamilyMemberType_ID ASC, DateOfBirth ASC
	OPEN MemberCursor
	
	FETCH NEXT FROM MemberCursor INTO @Contact_ID
	WHILE (@@Fetch_Status <> -1) BEGIN
		UPDATE #t2 SET Members = Members + (SELECT FirstName FROM Common_Contact WHERE Contact_ID = @Contact_ID) + ', ' WHERE Family_ID = @Family_ID
		FETCH NEXT FROM MemberCursor INTO @Contact_ID
	END
	
	Close MemberCursor
	Deallocate MemberCursor
	
	FETCH NEXT FROM FamilyCursor INTO @Family_ID
END
	
UPDATE #t2 SET Members = LEFT(Members, LEN(Members)-1) WHERE LEN(Members) > 0

SET @SQLWhere = LEFT(@SQLWhere, LEN(@SQLWhere) - 4) + ')'

Close FamilyCursor
Deallocate FamilyCursor
	
	
SELECT * FROM #t2
	

--Select the children from the family, their allergy codes, along with everyone who has clearance to check in/out these children
/*
SELECT		C.Family_ID,
			C.Contact_ID,
			C.FirstName + ' ' + C.LastName as Name,
			CI.KidsMinistryComments,
			CI.Photo,
			C.DateOfBirth
FROM		Common_Contact C,
			Common_ContactInternal CI,
			#t2
WHERE		C.Contact_ID = CI.Contact_ID
AND			C.Family_ID = #t2.Family_ID
AND			CI.Ministry_ID = 1
AND			((	SELECT COUNT(*) FROM Church_KidsMinistryAttendance A
				WHERE	A.Contact_ID = C.Contact_ID 
				AND		A.ChurchService_ID = @ChurchService_ID
				AND		CONVERT(NVARCHAR, A.[Date], 106) = CONVERT(NVARCHAR, @Date, 106)) > 0
			 OR @CheckedInOnly = 0)
ORDER BY	C.Family_ID,
			C.Contact_ID
			
SELECT		C.Contact_ID,
			KidsMinistryCode_ID
FROM		Church_ContactKidsMinistryCode K,
			Common_Contact C,
			#t2
WHERE		#t2.Family_ID = C.Family_ID
AND			C.Contact_ID = K.Contact_ID
AND			((	SELECT COUNT(*) FROM Church_KidsMinistryAttendance A
				WHERE	A.Contact_ID = C.Contact_ID 
				AND		A.ChurchService_ID = @ChurchService_ID
				AND		CONVERT(NVARCHAR, A.[Date], 106) = CONVERT(NVARCHAR, @Date, 106)) > 0
			 OR @CheckedInOnly = 0)

SELECT		C.Contact_ID,
			K.ClearanceContact_ID,
			CC.FirstName + ' ' + CC.LastName as Name
FROM		Church_KidsMinistryClearance K,
			Common_Contact C,
			Common_ContactInternal CI,
			Common_Contact CC,
			#t2
WHERE		C.Contact_ID = CI.Contact_ID
AND			C.Family_ID = #t2.Family_ID
AND			C.Contact_ID = K.Contact_ID
AND			CI.Ministry_ID = 1
AND			K.ClearanceContact_ID = CC.Contact_ID
AND			((	SELECT COUNT(*) FROM Church_KidsMinistryAttendance A
				WHERE	A.Contact_ID = C.Contact_ID 
				AND		A.ChurchService_ID = @ChurchService_ID
				AND		CONVERT(NVARCHAR, A.[Date], 106) = CONVERT(NVARCHAR, @Date, 106)) > 0
			 OR @CheckedInOnly = 0)
ORDER BY	C.Family_ID,
			C.Contact_ID
			
			
*/
GO
