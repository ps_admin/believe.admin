SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_KidsMinistryReportAttendance]

@Date				DATETIME,
@ChurchService_ID	INT,
@ULG_ID				INT	= 0,
@Campus_ID			INT = 0,
@Attended			INT = -1

AS

--SELECT	C.Contact_ID as ID,
--		C.FirstName as [First Name],
--		C.LastName as [Last Name],
--		CONVERT(NVARCHAR(500),'') as ULG,
--		CI.KidsMinistryGroup as [POD Group],
--		ISNULL(G.ShortGrade,'') as Grade,
--		CS.Name as [Church Service],
--		A.CheckInTime as [Check In Time],
--		CIP.FirstName + ' ' + CIP.LastName as [Checked In Parent],
--		CIL.FirstName + ' ' + CIL.LastName as [Checked In Leader],
--		A.CheckOutTime as [Check Out Time],
--		ISNULL(COP.FirstName + ' ' + COP.LastName,'') as [Checked Out Parent],
--		ISNULL(COL.FirstName + ' ' + COL.LastName,'') as [Checked Out Leader]
--INTO	#t1
--FROM	Common_Contact C,
--		Common_ContactInternal CI
--			LEFT JOIN Common_SchoolGrade G ON CI.SchoolGrade_ID = G.SchoolGrade_ID,
--		Church_KidsMinistryAttendance A
--			LEFT JOIN Common_Contact COP ON A.CheckedOutByParent_ID = COP.Contact_ID
--			LEFT JOIN Common_Contact COL ON A.CheckedOutByLeader_ID = COP.Contact_ID
--			JOIN Common_Contact CIP ON A.CheckedInByParent_ID = CIP.Contact_ID
--			JOIN Common_Contact CIL ON A.CheckedInByLeader_ID = CIL.Contact_ID
--			JOIN Church_ChurchService CS ON A.ChurchService_ID = CS.ChurchService_ID
--WHERE	C.Contact_ID = A.Contact_ID
--AND		C.Contact_ID = CI.Contact_ID
--AND		CONVERT(NVARCHAR, A.[Date],106) = CONVERT(NVARCHAR, @Date, 106)
--AND		(A.ChurchService_ID = @ChurchService_ID or @ChurchService_ID = 0)
SELECT	C.Contact_ID AS ID,
		C.FirstName as [First Name],
		C.LastName as [Last Name],
		CONVERT(NVARCHAR(500),'') as ULG,
		CI.KidsMinistryGroup as [POD Group],
		ISNULL(G.ShortGrade,'') as Grade,
		Co.[SundayAttendance],
		Co.[ServiceAttended_ID],
		CS.Name as [Church Service]
INTO	#t1
FROM	Common_Contact C,
		Common_ContactInternal CI
			LEFT JOIN Common_SchoolGrade G ON CI.SchoolGrade_ID = G.SchoolGrade_ID,
		Church_PCR PCR
			LEFT JOIN Church_PCRContact Co ON PCR.[PCR_ID] = Co.[PCR_ID]
			LEFT JOIN Church_ULG ULG ON PCR.[ULG_ID] = ULG.[ULG_ID]
			LEFT JOIN Church_PCRDate D ON PCR.[PCRDate_ID] = D.[PCRDate_ID]
			LEFT JOIN Church_ChurchService CS ON Co.[ServiceAttended_ID] = CS.[ChurchService_ID]
WHERE	Co.[Contact_ID] = CI.[Contact_ID]
AND		C.[Contact_ID] = CI.[Contact_ID]
AND		CONVERT(NVARCHAR, D.[Date], 106) = CONVERT(NVARCHAR, @Date, 106)
AND		(Co.[ServiceAttended_ID] = @ChurchService_ID OR @ChurchService_ID = 0)
AND		(ULG.[Ministry_ID] = 1)
AND		(ULG.ULG_ID = @ULG_ID OR @ULG_ID = 0)
AND		(ULG.[Campus_ID] = @Campus_ID OR @Campus_ID = 0)

IF @Attended = 0
	DELETE FROM #t1
	WHERE ([ServiceAttended_ID] IS NOT NULL)
ELSE IF @Attended = 1
	DELETE FROM #t1
	WHERE ([ServiceAttended_ID] IS NULL)

DECLARE @Contact_ID	INT,
		@ULG		NVARCHAR(500)
	
DECLARE ULGCursor Cursor FOR
	SELECT DISTINCT UC.Contact_ID, U.Name FROM Church_ULGContact UC, Church_ULG U, #t1 
	WHERE UC.ULG_ID = U.ULG_ID AND #t1.ID = UC.Contact_ID AND (U.ULG_ID = @ULG_ID OR @ULG_ID = 0)
	AND	UC.Inactive = 0
OPEN ULGCursor

FETCH NEXT FROM ULGCursor INTO @Contact_ID, @ULG
WHILE @@Fetch_Status = 0 BEGIN
	UPDATE #t1 SET ULG = ULG + @ULG + ', ' WHERE ID = @Contact_ID
	FETCH NEXT FROM ULGCursor INTO @Contact_ID, @ULG
END

UPDATE #t1 SET ULG = LEFT(ULG, LEN(ULG) - 1) WHERE LEN(ULG) > 1

CLOSE ULGCursor
DEALLOCATE ULGCursor
	
DELETE FROM #t1 WHERE ULG = '' AND @ULG_ID > 0

SELECT * FROM #t1
GO
