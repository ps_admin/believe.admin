SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_KidsMinistryReportToiletBreak]

@Date				datetime,
@ChurchService_ID	int,
@ULG_ID				int	= 0

AS

SELECT	T.KidsMinistryToiletBreak_ID,
		C.Contact_ID as ID,
		C.FirstName as [First Name],
		C.LastName as [Last Name],
		CL1.FirstName + ' ' + CL1.LastName as [Leader 1],
		CL2.FirstName + ' ' + CL2.LastName as [Leader 2],
		T.TimeTaken as [Time Taken],
		T.TimeReturned as [Time Returned],
		CONVERT(NVARCHAR(50),'') as [Total Time],
		T.Comments
INTO	#t1
FROM	Common_Contact C,
		Common_ContactInternal CI,
		Church_KidsMinistryToiletBreak T
			JOIN Common_Contact CL1 ON T.Leader1_ID = CL1.Contact_ID
			JOIN Common_Contact CL2 ON T.Leader2_ID = CL2.Contact_ID
WHERE	C.Contact_ID = T.Contact_ID
AND		C.Contact_ID = CI.Contact_ID
AND		CONVERT(NVARCHAR, T.[TimeTaken],106) = CONVERT(NVARCHAR, @Date, 106)
AND		T.ChurchService_ID = @ChurchService_ID

DELETE FROM #t1 
WHERE ID NOT IN (
	SELECT UC.Contact_ID FROM Church_ULGContact UC
	WHERE ULG_ID = @ULG_ID)
AND @ULG_ID > 0

UPDATE #t1 SET [Total Time] = dbo.DateDifference([Time Returned], [Time Taken])

SELECT * FROM #t1
GO
