SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_KidsMinistrySelectFamily]

@Family_ID				INT,
@ChurchService_ID		INT,
@Date					DATETIME,
@CheckedInOnly			BIT

AS

--Select the children from the family, their allergy codes, along with everyone who has clearance to check in/out these children

SELECT		C.Family_ID,
			C.Contact_ID,
			C.FirstName + ' ' + C.LastName as Name,
			CI.KidsMinistryComments,
			CI.Photo,
			C.DateOfBirth
FROM		Common_Contact C,
			Common_ContactInternal CI
WHERE		C.Contact_ID = CI.Contact_ID
AND			C.Family_ID = @Family_ID
AND			CI.Ministry_ID = 1
AND			(C.Contact_ID IN (SELECT Contact_ID FROM Church_KidsMinistryAttendance
							 WHERE	ChurchService_ID = @ChurchService_ID
							 AND	CONVERT(NVARCHAR, [Date], 106) = CONVERT(NVARCHAR, @Date, 106)
							 AND	CheckOutTime IS NULL)
			OR @CheckedInOnly = 0)	
ORDER BY	C.Family_ID,
			C.Contact_ID
			
			
SELECT		C.Contact_ID,
			KidsMinistryCode_ID
FROM		Church_ContactKidsMinistryCode K,
			Common_Contact C,
			Common_ContactInternal CI
WHERE		C.Contact_ID = K.Contact_ID
AND			C.Contact_ID = CI.Contact_ID
AND			C.Family_ID = @Family_ID
AND			CI.Ministry_ID = 1
AND			(C.Contact_ID IN (SELECT Contact_ID FROM Church_KidsMinistryAttendance
							 WHERE	ChurchService_ID = @ChurchService_ID
							 AND	CONVERT(NVARCHAR, [Date], 106) = CONVERT(NVARCHAR, @Date, 106)
							 AND	CheckOutTime IS NULL)
			OR @CheckedInOnly = 0)	
				
							 
SELECT		C.Contact_ID,
			K.ClearanceContact_ID,
			CC.FirstName + ' ' + CC.LastName as Name
FROM		Church_KidsMinistryClearance K,
			Common_Contact C,
			Common_ContactInternal CI,
			Common_Contact CC
WHERE		C.Contact_ID = CI.Contact_ID
AND			C.Family_ID = @Family_ID
AND			C.Contact_ID = K.Contact_ID
AND			CI.Ministry_ID = 1
AND			K.ClearanceContact_ID = CC.Contact_ID
AND			(C.Contact_ID IN (SELECT Contact_ID FROM Church_KidsMinistryAttendance
							 WHERE	ChurchService_ID = @ChurchService_ID
							 AND	CONVERT(NVARCHAR, [Date], 106) = CONVERT(NVARCHAR, @Date, 106)
							 AND	CheckOutTime IS NULL)
			OR @CheckedInOnly = 0)	 
ORDER BY	C.Family_ID,
			C.Contact_ID
GO
