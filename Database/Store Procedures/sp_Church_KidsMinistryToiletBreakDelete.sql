SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_KidsMinistryToiletBreakDelete]
(
	@KidsMinistryToiletBreak_ID int
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Church_KidsMinistryToiletBreak]
	WHERE  		[KidsMinistryToiletBreak_ID] = @KidsMinistryToiletBreak_ID
GO
