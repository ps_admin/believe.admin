SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_KidsMinistryToiletBreakInsert]
(
	@Contact_ID int,
	@Leader1_ID int,
	@Leader2_ID int,
	@ChurchService_ID int,
	@TimeTaken datetime,
	@TimeReturned datetime,
	@Comments nvarchar(max)
)
AS
	SET NOCOUNT ON

	INSERT INTO [Church_KidsMinistryToiletBreak]
	(
		[Contact_ID],
		[Leader1_ID],
		[Leader2_ID],
		[ChurchService_ID],
		[TimeTaken],
		[TimeReturned],
		[Comments]
	)
	VALUES
	(
		@Contact_ID,
		@Leader1_ID,
		@Leader2_ID,
		@ChurchService_ID,
		@TimeTaken,
		@TimeReturned,
		@Comments
	)

	SELECT KidsMinistryToiletBreak_ID = SCOPE_IDENTITY();
GO
