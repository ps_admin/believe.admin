SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_KidsMinistryToiletBreakListIncompleted]

AS

SELECT	KidsMinistryToiletBreak_ID,
		C.Contact_ID,
		C.FirstName + ' ' + C.LastName as ContactName,
		CS.Name as ChurchService,
		Ca.Name as Campus,
		Leader1_ID,
		CL1.FirstName + ' ' + CL1.LastName as Leader1Name,
		Leader2_ID,
		CL2.FirstName + ' ' + CL2.LastName as Leader2Name,
		TimeTaken,
		TimeReturned,
		Comments
FROM	Church_KidsMinistryToiletBreak T,
		Common_Contact C,
		Common_Contact CL1,
		Common_Contact CL2,
		Church_ChurchService CS,
		Church_Campus Ca
WHERE	T.Contact_ID = C.Contact_ID
AND		T.Leader1_ID = CL1.Contact_ID
AND		T.Leader2_ID = CL2.Contact_ID
AND		T.ChurchService_ID = CS.ChurchService_ID
AND		CS.Campus_ID = Ca.Campus_ID
AND		T.TimeReturned IS NULL

select * from Church_KidsMinistryToiletBreak
GO
