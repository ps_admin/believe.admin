SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_KidsMinistryToiletBreakRead]
(
	@KidsMinistryToiletBreak_ID		int
)
AS

	SELECT	*
	FROM	Church_KidsMinistryToiletBreak
	WHERE	KidsMinistryToiletBreak_ID = @KidsMinistryToiletBreak_ID
GO
