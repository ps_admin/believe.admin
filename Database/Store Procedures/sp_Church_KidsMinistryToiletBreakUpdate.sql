SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_KidsMinistryToiletBreakUpdate]
(
	@KidsMinistryToiletBreak_ID int,
	@Contact_ID int,
	@Leader1_ID int,
	@Leader2_ID int,
	@ChurchService_ID int,
	@TimeTaken datetime,
	@TimeReturned datetime,
	@Comments nvarchar(max)
)
AS
	SET NOCOUNT ON
	
	UPDATE [Church_KidsMinistryToiletBreak]
	SET
		[Contact_ID] = @Contact_ID,
		[Leader1_ID] = @Leader1_ID,
		[Leader2_ID] = @Leader2_ID,
		[ChurchService_ID] = @ChurchService_ID,
		[TimeTaken] = @TimeTaken,
		[TimeReturned] = @TimeReturned,
		[Comments] = @Comments
	WHERE 
		[KidsMinistryToiletBreak_ID] = @KidsMinistryToiletBreak_ID
GO
