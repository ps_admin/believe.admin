SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_MonthlyStatsSave]

AS

INSERT		Church_HistoryPartnership (Date, ChurchStatus_ID, Campus_ID, Ministry_ID, Region_ID, Number)
SELECT		LEFT(DATENAME(m, dbo.GetRelativeDate()),3) + ' ' + CONVERT(NVARCHAR,YEAR(dbo.GetRelativeDate())) as [Date],
			ChurchStatus_ID,
			Campus_ID,
			Ministry_ID,
			Region_ID,
			COUNT(*)
FROM		Common_ContactInternal
GROUP BY	ChurchStatus_ID,
			Campus_ID,
			Ministry_ID,
			Region_ID


INSERT		Church_HistoryULGMembership (Date, ULG_ID, ChurchStatus_ID, Campus_ID, Ministry_ID, Region_ID, Number)
SELECT		LEFT(DATENAME(m, dbo.GetRelativeDate()),3) + ' ' + CONVERT(NVARCHAR,YEAR(dbo.GetRelativeDate())) as [Date],
			ULG_ID,
			ChurchStatus_ID,
			Campus_ID,
			Ministry_ID,
			Region_ID,
			COUNT(*)
FROM		Common_ContactInternal CI,
			Church_ULGContact C
WHERE		CI.Contact_ID = C.Contact_ID
AND			C.Inactive = 0
GROUP BY	ULG_ID,
			ChurchStatus_ID,
			Campus_ID,
			Ministry_ID,
			Region_ID
GO
