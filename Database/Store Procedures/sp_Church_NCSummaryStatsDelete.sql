SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_NCSummaryStatsDelete]
(
	@SummaryStats_ID int
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Church_NCSummaryStats]
	WHERE  [SummaryStats_ID] = @SummaryStats_ID
GO
