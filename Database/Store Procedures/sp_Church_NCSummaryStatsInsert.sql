SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_NCSummaryStatsInsert]
(
	@Date datetime,
	@Campus_ID INT = 1,
	@NCSummaryStatsMinistry_ID int,
	@NCDecisionType_ID int,
	@Number int,
	@EnteredBy_ID int,
	@DateEntered datetime
)
AS
	SET NOCOUNT ON

	INSERT INTO [Church_NCSummaryStats]
	(
		[Date],
		[Campus_ID],
		[NCSummaryStatsMinistry_ID],
		[NCDecisionType_ID],
		[Number],
		[EnteredBy_ID],
		[DateEntered]
	)
	VALUES
	(
		@Date,
		@Campus_ID,
		@NCSummaryStatsMinistry_ID,
		@NCDecisionType_ID,
		@Number,
		@EnteredBy_ID,
		@DateEntered
	)

	SELECT SummaryStats_ID = SCOPE_IDENTITY();
GO
