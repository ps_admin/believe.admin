SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_NCSummaryStatsList]
(
	@StartDate datetime,
	@EndDate datetime,
	@Campus_ID int = null,
	@Ministry_ID int = null,
	@DecisionType_ID int = null
)
AS
	SET NOCOUNT ON

	SELECT	SummaryStats_ID,
			Campus_ID,
			[Date],
			NCSummaryStatsMinistry_ID,
			NCDecisionType_ID,
			Number,
			EnteredBy_ID,
			DateEntered			
	FROM	Church_NCSummaryStats S
	WHERE	[Date] >= @StartDate
	AND		[Date] < DATEADD(dd, 1, @EndDate)
	AND		(NCSummaryStatsMinistry_ID = @Ministry_ID OR @Ministry_ID IS NULL)
	AND		(Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
	AND		(NCDecisionType_ID = @DecisionType_ID OR @DecisionType_ID IS NULL)
GO
