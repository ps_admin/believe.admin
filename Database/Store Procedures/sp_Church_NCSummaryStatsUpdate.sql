SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_NCSummaryStatsUpdate]
(
	@SummaryStats_ID int,
	@Date datetime,
	@Campus_ID INT = 1,
	@NCSummaryStatsMinistry_ID int,
	@NCDecisionType_ID int,
	@Number int,
	@EnteredBy_ID int,
	@DateEntered datetime
)
AS
	SET NOCOUNT ON
	
	UPDATE [Church_NCSummaryStats]
	SET
		[Date] = @Date,
		[Campus_ID] = @Campus_ID,
		[NCSummaryStatsMinistry_ID] = @NCSummaryStatsMinistry_ID,
		[NCDecisionType_ID] = @NCDecisionType_ID,
		[Number] = @Number,
		[EnteredBy_ID] = @EnteredBy_ID,
		[DateEntered] = @DateEntered
	WHERE 
		[SummaryStats_ID] = @SummaryStats_ID
GO
