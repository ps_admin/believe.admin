SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_NPNCAltarDecisionsList]

@NCDecisionType_ID		INT = null,
@EntryType				INT = 0,
@ULG_ID					INT = null,
@CampusDecision_ID		INT = null,
@Campus_ID				INT = null,
@Ministry_ID			INT = null,
@Region_ID				INT = null,
@StartDate				DATETIME,
@EndDate				DATETIME 

AS

CREATE TABLE #NCList (
	ID int,
	FirstName nvarchar(50),
	LastName nvarchar(50),
	_Name nvarchar(100),
	[Home Phone] nvarchar(50),
	[Mobile Phone] nvarchar(50),
	Email nvarchar(200),
	[Type] nvarchar(50),
	[Church Status] nvarchar(50),
	[Ministry] nvarchar(50),
	[Decision Type] nvarchar(50),
	Outcome nvarchar(50),
	ULG nvarchar(200),
	[Decision Date] datetime,
	Comments nvarchar(max),
	[_GUID] nvarchar(100) null,
	[_Campus_ID] int,
	[_CampusDecision_ID] int
)

IF @EntryType = 0 OR @EntryType = 1 
	INSERT	#NCList
	SELECT	NPNC.Contact_ID,
			Firstname, 
			Lastname,
			Firstname + ' ' + Lastname,
			C.Phone,
			C.Mobile,
			C.Email,
			'New Christians',
			CS.Name,
			ISNULL((SELECT [Name] FROM Church_Ministry WHERE Ministry_ID = I.Ministry_ID), ''),
			DT.Name,
			ISNULL((SELECT [Name] FROM Church_ChurchStatus WHERE ChurchStatus_ID = NPNC.OutcomeStatus_ID), ''),
			'',
			NPNC.FirstContactDate,
			'',
			NPNC.GUID,
			I.Campus_ID,
			NPNC.CampusDecision_ID
	FROM	Church_NPNC NPNC,
			Common_Contact C,
			Common_ContactInternal I,
			Church_ChurchStatus CS,
			Church_NCDecisionType DT		
	WHERE	NPNC.Contact_ID = C.Contact_ID
	AND		NPNC.Contact_ID = I.Contact_ID
	AND		I.ChurchStatus_ID = CS.ChurchStatus_ID
	AND		NPNC.NCDecisionType_ID =  DT.NCDecisionType_ID
	AND		NPNC.NPNCType_ID = 2 -- New Christians Only
	AND		(NPNC.NCDecisionType_ID = @NCDecisionType_ID or @NCDecisionType_ID IS NULL)
	AND		(NPNC.CampusDecision_ID = @CampusDecision_ID or @CampusDecision_ID IS NULL)
	AND		(I.Campus_ID = @Campus_ID or @Campus_ID IS NULL)
	AND		(I.Ministry_ID = @Ministry_ID or @Ministry_ID IS NULL)
	AND		(I.Region_ID = @Region_ID or @Region_ID IS NULL)
	AND		(NPNC.FirstContactDate >= @StartDate or @StartDate IS NULL)
	AND		(NPNC.FirstContactDate <= @EndDate or @EndDate IS NULL)


IF @EntryType = 0 OR @EntryType = 2
	INSERT	#NCList
	SELECT	AD.Contact_ID,
			Firstname, 
			Lastname,
			Firstname + ' ' + Lastname,
			C.Phone,
			C.Mobile,
			C.Email,
			'Additional Decision',
			CS.Name,
			ISNULL((SELECT [Name] FROM Church_Ministry WHERE Ministry_ID = I.Ministry_ID), ''),
			DT.Name,
			'',
			'',
			AD.DecisionDate,
			CONVERT(NVARCHAR(MAX),''),
			convert(uniqueidentifier,null),
			I.Campus_ID,
			AD.CampusDecision_ID
	FROM	Church_ContactAdditionalDecision AD,
			Common_Contact C,
			Common_ContactInternal I,
			Church_ChurchStatus CS,
			Church_NCDecisionType DT
	WHERE	AD.Contact_ID = C.Contact_ID
	AND		AD.Contact_ID = I.Contact_ID
	AND		I.ChurchStatus_ID = CS.ChurchStatus_ID
	AND		AD.DecisionType_ID =  DT.NCDecisionType_ID
	AND		(AD.DecisionType_ID = @NCDecisionType_ID or @NCDecisionType_ID IS NULL)
	AND		(AD.CampusDecision_ID = @CampusDecision_ID or @CampusDecision_ID IS NULL)
	AND		(I.Campus_ID = @Campus_ID or @Campus_ID IS NULL)
	AND		(I.Ministry_ID = @Ministry_ID or @Ministry_ID IS NULL)
	AND		(I.Region_ID = @Region_ID or @Region_ID IS NULL)
	AND		(AD.DecisionDate >= @StartDate or @StartDate IS NULL)
	AND		(AD.DecisionDate <= @EndDate or @EndDate IS NULL)


/* Update with Urban Life Groups */
DECLARE	@Contact_ID	INT,
		@ULGName	NVARCHAR(500)

DECLARE ULGCursor CURSOR FOR
	SELECT DISTINCT L.ID, Code + ' ' + U.[Name] FROM #NCList L, Church_ULG U, Church_ULGContact UC
	WHERE L.ID = UC.Contact_ID AND UC.ULG_ID = U.ULG_ID AND UC.Inactive = 0
	ORDER BY L.ID, Code + ' ' + U.[Name]

OPEN ULGCursor

FETCH NEXT FROM ULGCursor INTO @Contact_ID, @ULGName
WHILE @@FETCH_STATUS = 0 BEGIN
	UPDATE #NCList SET ULG = ULG + @ULGName + '; ' WHERE ID = @Contact_ID
	FETCH NEXT FROM ULGCursor INTO @Contact_ID, @ULGName
END

CLOSE ULGCursor
DEALLOCATE ULGCursor

UPDATE #NCList SET ULG = LEFT(ULG, LEN(ULG)-1) WHERE LEN(ULG)>2



/* Update with Comments */
DECLARE	@Comment		NVARCHAR(500),
		@CommentDate	DATETIME

DECLARE CommentCursor CURSOR FOR
	SELECT DISTINCT Contact_ID, Comment, CommentDate FROM Church_ContactComment C, Church_CommentSource CS, #NCList N
	WHERE N.ID = C.Contact_ID AND C.CommentSource_ID = CS.CommentSource_ID AND CS.ShowInNPNC = 1
	ORDER BY Contact_ID, CommentDate DESC
	
OPEN CommentCursor

FETCH NEXT FROM CommentCursor INTO @Contact_ID, @Comment, @CommentDate
WHILE @@FETCH_STATUS = 0 BEGIN
	UPDATE #NCList SET Comments = Comments + CONVERT(NVARCHAR,@CommentDate,106) + ' - ' + @Comment + '; ' WHERE ID = @Contact_ID
	FETCH NEXT FROM CommentCursor INTO @Contact_ID, @Comment, @CommentDate
END

CLOSE CommentCursor
DEALLOCATE CommentCursor

UPDATE #NCList SET Comments = LEFT(Comments, LEN(Comments)-1) WHERE LEN(Comments)>2


SELECT * FROM #NCList
GO
