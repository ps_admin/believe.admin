SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_NPNCCarerList]

@Campus_ID		INT

AS

SELECT		DISTINCT CC.Contact_ID, 
			CC.FirstName,
			CC.LastName,
			CC.FirstName + ' ' + CC.LastName + ' (' + CONVERT(VARCHAR,(SELECT COUNT(*) FROM Church_NPNC NPNC WHERE Carer_ID = CC.Contact_ID AND NPNC.OutcomeStatus_ID IS NULL)) + ')' as [Name]
FROM		Church_Role R,
			Church_ContactRole CR,
			Church_RestrictedAccessULG RAU,
			Church_ULG U,
			Common_Contact CC
WHERE		R.Role_ID = CR.Role_ID
AND			CR.ContactRole_ID = RAU.ContactRole_ID
AND			RAU.ULG_ID = U.ULG_ID
AND			CR.Contact_ID = CC.Contact_ID
AND			RoleTypeUL_ID IN (2,3,4)
AND			U.Campus_ID = @Campus_ID
ORDER BY	CC.FirstName

/*
UNION

SELECT	DISTINCT CC.Contact_ID, 
		CC.FirstName + ' ' + CC.LastName + ' (' + CONVERT(VARCHAR,(SELECT COUNT(*) FROM Church_NPNC NPNC WHERE Carer_ID = CC.Contact_ID AND NPNC.OutcomeStatus_ID IS NULL)) + ')' as [Name]
FROM	Church_NPNC NPNC,
		Common_Contact CC,
		Common_ContactInternal I
WHERE	NPNC.Carer_ID = CC.Contact_ID
AND		CC.Contact_ID = I.Contact_ID
AND		I.Campus_ID = @Campus_ID

*/
GO
