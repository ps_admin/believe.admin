SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_NPNCCommentsList]

@NPNCType_ID				INT = null,
@Carer_ID					INT = null,
@Campus_ID					INT = null,
@Ministry_ID				INT = null,
@Region_ID					INT = null,
@CampusDecision_ID			INT = null,
@Comments					INT = 0,
@StartDate					DATETIME = null,
@EndDate					DATETIME = null,
@IncludeActionedComments	BIT = 0

AS

SELECT	NPNC.Contact_ID as ID,
		C.FirstName,
		C.LastName,
		C.Email,
		C.Suburb,
		C.Mobile as [Mobile Phone],
		C.Phone as [Home Phone],
		NPNCType.[Name] as [NP/NC Type],
		ISNULL((SELECT [Name] FROM Church_NCDecisionType WHERE NCDecisionType_ID = NPNC.NCDecisionType_ID),'') as [Decision Type],
		NPNC.FirstContactDate as [First Contact Date],
		ISNULL((SELECT M.[Name] FROM Church_Ministry M WHERE Ministry_ID = CI.Ministry_ID ), '') as Ministry,
		ISNULL((SELECT R.[Name] FROM Church_Region R WHERE Region_ID = CI.Region_ID ), '') as Region,
		ISNULL((SELECT [Code] + ' ' + [Name] FROM Church_ULG WHERE ULG_ID = NPNC.ULG_ID),'') as ULG,
		ISNULL((SELECT [Name] FROM vw_Users WHERE Contact_ID = NPNC.Carer_ID),'') as Carer,
		CS.Name as [Comment Type],
		CC.CommentDate as [Comment Date],
		ISNULL(CC.Comment, '') as Comment,
		NPNC.GUID as [_GUID],
		CI.Campus_ID as [_Campus_ID],
		C.GUID as [_ContactGUID],
		CC.Comment_ID as [_Comment_ID]
FROM	Church_NPNC NPNC
			LEFT OUTER JOIN Church_ContactComment CC ON 
				NPNC.Contact_ID = CC.Contact_ID 
				AND (CC.CommentDate >= @StartDate or @StartDate IS NULL)
				AND (CC.CommentDate < DATEADD(DD, 1, @EndDate) or @EndDate IS NULL)
			LEFT OUTER JOIN Church_CommentSource CS ON CC.CommentSource_ID = CS.CommentSource_ID,
		Church_NPNCType NPNCType,
		Church_NPNCInitialStatus S,
		Common_Contact C,
		vw_Common_ContactAddressView CAV,
		Common_ContactInternal CI
WHERE	NPNC.NPNCType_ID = NPNCType.NPNCType_ID
AND		NPNC.InitialStatus_ID = S.InitialStatus_ID
AND		NPNC.Contact_ID = C.Contact_ID
AND		NPNC.Contact_ID = CI.Contact_ID
AND		NPNC.Contact_ID = CAV.Contact_ID
AND		CI.ChurchStatus_ID NOT IN (7) -- Exclude inactive status
AND		(NPNC.NPNCType_ID = @NPNCType_ID or @NPNCType_ID IS NULL)
AND		(NPNC.CampusDecision_ID = @CampusDecision_ID OR @CampusDecision_ID IS NULL)
AND		(CI.Campus_ID = @Campus_ID  OR @Campus_ID IS NULL)
AND		(NPNC.Ministry_ID = @Ministry_ID or @Ministry_ID IS NULL)
AND		(NPNC.Region_ID = @Region_ID or @Region_ID IS NULL)
AND		(NPNC.Carer_ID = @Carer_ID or @Carer_ID IS NULL)
AND		(CC.Actioned = 0 OR CC.CommentDate IS NULL OR @IncludeActionedComments = 1)
AND		(@Comments = 0 OR
		 (CC.CommentDate IS NOT NULL AND @Comments = 1) OR
		 (CC.CommentDate IS NULL AND @Comments = 2))
AND		NPNC.OutcomeStatus_ID IS NULL
ORDER BY LastName, FirstName
GO
