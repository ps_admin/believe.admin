SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_NPNCInsert]
(
	@Contact_ID int,
	@NPNCType_ID int,
	@FirstContactDate datetime,
	@FirstFollowupCallDate datetime = NULL,
	@FirstFollowupVisitDate datetime = NULL,
	@InitialStatus_ID int,
	@NCDecisionType_ID int = NULL,
	@NCTeamMember_ID int = NULL,
	@CampusDecision_ID int = null,
	@Ministry_ID INT,
	@Region_ID INT = null,
	@ULG_ID int = NULL,
	@ULGDateOfIssue datetime = NULL,
	@Carer_ID int,
	@OutcomeStatus_ID int = NULL,
	@OutcomeDate datetime = NULL,
	@OutcomeInactive_ID int = NULL,
	@GUID uniqueidentifier,
	@User_ID int = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin

	SET NOCOUNT ON

	INSERT INTO [Church_NPNC]
	(
		[Contact_ID],
		[NPNCType_ID],
		[FirstContactDate],
		[FirstFollowupCallDate],
		[FirstFollowupVisitDate],
		[InitialStatus_ID],
		[NCDecisionType_ID],
		[NCTeamMember_ID],
		[CampusDecision_ID],
		[Ministry_ID],
		[Region_ID],
		[ULG_ID],
		[ULGDateOfIssue],
		[Carer_ID],
		[OutcomeStatus_ID],
		[OutcomeDate],
		[OutcomeInactive_ID],
		[GUID]
	)
	VALUES
	(
		@Contact_ID,
		@NPNCType_ID,
		@FirstContactDate,
		@FirstFollowupCallDate,
		@FirstFollowupVisitDate,
		@InitialStatus_ID,
		@NCDecisionType_ID,
		@NCTeamMember_ID,
		@CampusDecision_ID,
		@Ministry_ID,
		@Region_ID,
		@ULG_ID,
		@ULGDateOfIssue,
		@Carer_ID,
		@OutcomeStatus_ID,
		@OutcomeDate,
		@OutcomeInactive_ID,
		@GUID
	)

	SELECT NPNC_ID = SCOPE_IDENTITY();
GO
