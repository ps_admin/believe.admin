SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_NPNCList]

@NPNCType_ID			INT,
@Carer_ID				INT,
@Campus_ID				INT = null,
@Ministry_ID			INT,
@Region_ID				INT,
@CampusDecision_ID		INT,
@StartDate				DATETIME,
@EndDate				DATETIME

AS

SELECT	NPNC.Contact_ID as ID,
		C.FirstName,
		C.LastName,
		CAV.StreetAddress as [Street Address],
		CAV.PostalAddress as [Postal Address],
		C.Email,
		C.Mobile as [Mobile Phone],
		C.Phone as [Home Phone],
		NPNCType.[Name] as [NP/NC Type],
		ISNULL((SELECT [Name] FROM Church_NCDecisionType WHERE NCDecisionType_ID = NPNC.NCDecisionType_ID),'') as [Decision Type],
		NPNC.FirstContactDate as [First Contact Date],
		S.[Name] as [Initial Status],
		ISNULL((SELECT M.[Name] FROM Church_Ministry M WHERE Ministry_ID = CI.Ministry_ID ), '') as Ministry,
		ISNULL((SELECT R.[Name] FROM Church_Region R WHERE Region_ID = CI.Region_ID ), '') as Region,
		ISNULL((SELECT [Code] + ' ' + [Name] FROM Church_ULG WHERE ULG_ID = NPNC.ULG_ID),'') as ULG,
		ISNULL((SELECT [Name] FROM vw_Users WHERE Contact_ID = NPNC.Carer_ID),'') as Carer,
		NPNC.GUID as [_GUID],
		CI.Campus_ID as [_Campus_ID]
FROM	Church_NPNC NPNC,
		Church_NPNCType NPNCType,
		Church_NPNCInitialStatus S,
		Common_Contact C,
		vw_Common_ContactAddressView CAV,
		Common_ContactInternal CI
WHERE	NPNC.NPNCType_ID = NPNCType.NPNCType_ID
AND		NPNC.InitialStatus_ID = S.InitialStatus_ID
AND		NPNC.Contact_ID = C.Contact_ID
AND		NPNC.Contact_ID = CI.Contact_ID
AND		NPNC.Contact_ID = CAV.Contact_ID
AND		CI.ChurchStatus_ID NOT IN (7) -- Exclude inactive status
AND		(NPNC.NPNCType_ID = @NPNCType_ID or @NPNCType_ID IS NULL)
AND		(CI.Campus_ID = @Campus_ID or @Campus_ID IS NULL)
AND		(CI.Ministry_ID = @Ministry_ID or @Ministry_ID IS NULL)
AND		(CI.Region_ID = @Region_ID or @Region_ID IS NULL)
AND		(NPNC.CampusDecision_ID = @CampusDecision_ID or @CampusDecision_ID IS NULL)
AND		(NPNC.Carer_ID = @Carer_ID or @Carer_ID IS NULL)
AND		(NPNC.FirstContactDate >= @StartDate or @StartDate IS NULL)
AND		(NPNC.FirstContactDate <= @EndDate or @EndDate IS NULL)
AND		NPNC.OutcomeStatus_ID IS NULL
GO
