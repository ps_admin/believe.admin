SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_NPNCUpdate]
(
	@NPNC_ID int,
	@Contact_ID int,
	@NPNCType_ID int,
	@FirstContactDate datetime,
	@FirstFollowupCallDate datetime = NULL,
	@FirstFollowupVisitDate datetime = NULL,
	@InitialStatus_ID int,
	@NCDecisionType_ID int = NULL,
	@NCTeamMember_ID int = NULL,
	@CampusDecision_ID int = null,
	@Ministry_ID int,
	@Region_ID int = null,
	@ULG_ID int = NULL,
	@ULGDateOfIssue datetime = NULL,
	@Carer_ID int,
	@OutcomeStatus_ID int = NULL,
	@OutcomeDate datetime = NULL,
	@OutcomeInactive_ID int = NULL,
	@GUID uniqueidentifier,
	@User_ID int = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin

	SET NOCOUNT ON
	
	UPDATE [Church_NPNC]
	SET
		[Contact_ID] = @Contact_ID,
		[NPNCType_ID] = @NPNCType_ID,
		[FirstContactDate] = @FirstContactDate,
		[FirstFollowupCallDate] = @FirstFollowupCallDate,
		[FirstFollowupVisitDate] = @FirstFollowupVisitDate,
		[InitialStatus_ID] = @InitialStatus_ID,
		[NCDecisionType_ID] = @NCDecisionType_ID,
		[NCTeamMember_ID] = @NCTeamMember_ID,
		[CampusDecision_ID] = @CampusDecision_ID,
		[Ministry_ID] = @Ministry_ID,
		[Region_ID] = @Region_ID,
		[ULG_ID] = @ULG_ID,
		[ULGDateOfIssue] = @ULGDateOfIssue,
		[Carer_ID] = @Carer_ID,
		[OutcomeStatus_ID] = @OutcomeStatus_ID,
		[OutcomeDate] = @OutcomeDate,
		[OutcomeInactive_ID] = @OutcomeInactive_ID,
		[GUID] = @GUID
	WHERE 
		[NPNC_ID] = @NPNC_ID
GO
