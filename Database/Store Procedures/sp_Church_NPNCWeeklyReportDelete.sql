SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_NPNCWeeklyReportDelete]
(
	@WeeklyReport_ID int
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Church_NPNCWeeklyReport]
	WHERE  [WeeklyReport_ID] = @WeeklyReport_ID
GO
