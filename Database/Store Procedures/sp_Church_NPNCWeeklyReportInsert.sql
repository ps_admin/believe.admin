SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_NPNCWeeklyReportInsert]
(
	@NPNC_ID int,
	@ReportDate datetime,
	@WeekNumber int,
	@Contacted bit,
	@Attended bit,
	@Comments ntext
)
AS
	SET NOCOUNT ON

	INSERT INTO [Church_NPNCWeeklyReport]
	(
		[NPNC_ID],
		[ReportDate],
		[WeekNumber],
		[Contacted],
		[Attended],
		[Comments]
	)
	VALUES
	(
		@NPNC_ID,
		@ReportDate,
		@WeekNumber,
		@Contacted,
		@Attended,
		@Comments
	)

	SELECT WeeklyReport_ID = SCOPE_IDENTITY();
GO
