SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_NPNCWeeklyReportUpdate]
(
	@WeeklyReport_ID int,
	@NPNC_ID int,
	@ReportDate datetime,
	@WeekNumber int,
	@Contacted bit,
	@Attended bit,
	@Comments ntext
)
AS
	SET NOCOUNT ON
	
	UPDATE [Church_NPNCWeeklyReport]
	SET
		[NPNC_ID] = @NPNC_ID,
		[ReportDate] = @ReportDate,
		[WeekNumber] = @WeekNumber,
		[Contacted] = @Contacted,
		[Attended] = @Attended,
		[Comments] = @Comments
	WHERE 
		[WeeklyReport_ID] = @WeeklyReport_ID
GO
