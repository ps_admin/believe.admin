SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_NPNCWelcomePacksIssuedDelete]
(
	@WelcomePacksIssued_ID int
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Church_NPNCWelcomePacksIssued]
	WHERE  [WelcomePacksIssued_ID] = @WelcomePacksIssued_ID
GO
