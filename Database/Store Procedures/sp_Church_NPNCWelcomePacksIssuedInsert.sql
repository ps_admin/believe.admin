SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_NPNCWelcomePacksIssuedInsert]
(
	@Campus_ID int = 1,
	@DateIssued datetime,
	@NumberIssued int,
	@NumberReturned int,
	@EnteredBy_ID int,
	@DateEntered datetime
)
AS
	SET NOCOUNT ON

	INSERT INTO [Church_NPNCWelcomePacksIssued]
	(
		[Campus_ID],
		[DateIssued],
		[NumberIssued],
		[NumberReturned],
		[EnteredBy_ID],
		[DateEntered]
	)
	VALUES
	(
		@Campus_ID,
		@DateIssued,
		@NumberIssued,
		@NumberReturned,
		@EnteredBy_ID,
		@DateEntered
	)

	SELECT WelcomePacksIssued_ID = SCOPE_IDENTITY();
GO
