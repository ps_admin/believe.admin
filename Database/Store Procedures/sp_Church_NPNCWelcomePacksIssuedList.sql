SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_NPNCWelcomePacksIssuedList]
(
	@StartDate datetime,
	@EndDate datetime
)
AS
	SET NOCOUNT ON

	SELECT		*
	FROM		Church_NPNCWelcomePacksIssued
	WHERE		DateIssued >= @StartDate
	AND			DateIssued < DATEADD(dd, 1, @EndDate)
	ORDER BY	DateIssued DESC
GO
