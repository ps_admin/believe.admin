SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_NPNCWelcomePacksIssuedUpdate]
(
	@WelcomePacksIssued_ID int,
	@Campus_ID int = 1,
	@DateIssued datetime,
	@NumberIssued int,
	@NumberReturned int,
	@EnteredBy_ID int,
	@DateEntered datetime
)
AS
	SET NOCOUNT ON
	
	UPDATE [Church_NPNCWelcomePacksIssued]
	SET
		[Campus_ID] = @Campus_ID,
		[DateIssued] = @DateIssued,
		[NumberIssued] = @NumberIssued,
		[NumberReturned] = @NumberReturned,
		[EnteredBy_ID] = @EnteredBy_ID,
		[DateEntered] = @DateEntered
	WHERE 
		[WelcomePacksIssued_ID] = @WelcomePacksIssued_ID
GO
