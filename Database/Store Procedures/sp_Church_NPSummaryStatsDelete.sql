SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_NPSummaryStatsDelete]
(
	@SummaryStats_ID int
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Church_NPSummaryStats]
	WHERE  [SummaryStats_ID] = @SummaryStats_ID
GO
