SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_NPSummaryStatsInsert]
(
	@Date datetime,
	@Campus_ID int = 1,
	@InitialStatus_ID int,
	@Number int,
	@EnteredBy_ID int,
	@DateEntered datetime
)
AS
	SET NOCOUNT ON

	INSERT INTO Church_NPSummaryStats
	(
		[Date],
		[Campus_ID],
		[InitialStatus_ID],
		[Number],
		[EnteredBy_ID],
		[DateEntered]
	)
	VALUES
	(
		@Date,
		@Campus_ID,
		@InitialStatus_ID,
		@Number,
		@EnteredBy_ID,
		@DateEntered
	)

	SELECT SummaryStats_ID = SCOPE_IDENTITY();
GO
