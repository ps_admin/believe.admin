SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_NPSummaryStatsList]
(
	@StartDate datetime,
	@EndDate datetime,
	@Campus_ID int = null,
	@InitialStatus_ID int = null
)
AS
	SET NOCOUNT ON

	SELECT	*
	FROM	Church_NPSummaryStats S
	WHERE	[Date] >= @StartDate
	AND		[Date] < DATEADD(dd, 1, @EndDate)
	AND		(Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
	AND		(InitialStatus_ID = @InitialStatus_ID OR @InitialStatus_ID IS NULL)
GO
