SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_NightlyIncompletePCREmail]

AS

DECLARE @PCRDate_ID INT

SELECT	@PCRDate_ID = PCRDate_ID 
FROM	Church_PCRDate
WHERE	[Date] = (	SELECT	MAX([Date])
					FROM	Church_PCRDate
					WHERE	[Date] < dbo.GetRelativeDate())
					

SELECT	DISTINCT
		Contact_ID, 
		ULG.ULG_ID,
		@PCRDate_ID as PCRDate_ID,
		Code
FROM	Church_RestrictedAccessULG U,
		Church_ContactRole CR,
		Church_Role R,
		Church_ULG ULG
WHERE	U.ContactRole_ID = CR.ContactRole_ID
AND		CR.Role_ID = R.Role_ID
AND		U.ULG_ID = ULG.ULG_ID
AND		R.RoleTypeUL_ID = 2
AND		U.ULG_ID IN (	SELECT	ULG_ID 
						FROM	Church_ULG
						WHERE	Inactive = 0
						AND		ActiveDate <= (SELECT [Date] FROM Church_PCRDate WHERE PCRDate_ID = @PCRDate_ID)
						AND		ULG_ID NOT IN (	SELECT	ULG_ID
												FROM	Church_PCR		
												WHERE	PCRDate_ID = @PCRDate_ID
												AND		ReportCompleted = 1))
ORDER BY Code
GO
