SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_PCRContactUpdate]
(
	@PCRContact_ID int,
	@PCR_ID int,
	@Contact_ID int,
	@SundayAttendance bit,
	@ServiceAttended_ID int = null,
	@ULGAttendance bit,
	@BoomAttendance bit,
	@FNLAttendance bit = 0,
	@Call int,
	@Visit int,
	@Other int
)
AS
	SET NOCOUNT ON
	
	UPDATE [Church_PCRContact]
	SET
		[PCR_ID] = @PCR_ID,
		[Contact_ID] = @Contact_ID,
		[SundayAttendance] = @SundayAttendance,
		[ServiceAttended_ID] = @ServiceAttended_ID,
		[ULGAttendance] = @ULGAttendance,
		[BoomAttendance] = @BoomAttendance,
		[FNLAttendance] = @FNLAttendance,
		[Call] = @Call,
		[Visit] = @Visit,
		[Other] = @Other
	WHERE 
		[PCRContact_ID] = @PCRContact_ID
GO
