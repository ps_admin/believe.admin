SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_PCRDateInsert]
(
	@Date datetime,
	@ULG bit,
	@Boom bit,
	@FNL bit = 0,
	@ReportIsDue bit = 1,
	@Deleted bit
)
AS
	SET NOCOUNT ON

	INSERT INTO [Church_PCRDate]
	(
		[Date],
		[ULG],
		[Boom],
		[FNL],
		[ReportIsDue],
		[Deleted]
	)
	VALUES
	(
		@Date,
		@ULG,
		@Boom,
		@FNL,
		@ReportIsDue,
		@Deleted
	)

	SELECT PCRDate_ID = SCOPE_IDENTITY();
GO
