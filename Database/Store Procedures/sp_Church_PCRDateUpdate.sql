SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_PCRDateUpdate]
(
	@PCRDate_ID int,
	@Date datetime,
	@ULG bit,
	@Boom bit,
	@FNL bit = 0,
	@ReportIsDue bit = 1,
	@Deleted bit
)
AS
	SET NOCOUNT ON
	
	UPDATE [Church_PCRDate]
	SET
		[Date] = @Date,
		[ULG] = @ULG,
		[Boom] = @Boom,
		[FNL] = @FNL,
		[ReportIsDue] = @ReportIsDue,
		[Deleted] = @Deleted
	WHERE 
		[PCRDate_ID] = @PCRDate_ID
GO
