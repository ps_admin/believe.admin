SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_PCRList]

@Campus_ID				INT = null,
@Ministry_ID			INT = null,
@Region_ID		INT= null,
@User_ID				INT= null

AS

DECLARE @CurrentReportDate	DATETIME
SELECT TOP 1 @CurrentReportDate = [Date] 
FROM	Church_PCRDate
WHERE	Date <= dbo.GetRelativeDate()
AND		Deleted = 0
ORDER BY [Date] DESC

--Insert Report entries for any dates, starting from the active date of the ULG,
--through to the current date.
INSERT	Church_PCR (PCRDate_ID, ULG_ID, ULGChildren, ULGVisitors, ULGOffering, ReportCompleted)
SELECT	PCRDate_ID, 1, 0, 0, 0, 0
FROM	Church_PCRDate D,
		Church_ULG U
WHERE NOT EXISTS (	SELECT	PCRDate_ID, ULG_ID
					FROM	Church_PCR
					WHERE	PCRDate_ID = D.PCRDate_ID
					AND		ULG_ID = U.ULG_ID)
AND		U.ULG_ID = 1
AND		D.Date > U.ActiveDate
AND		D.Date < dbo.GetRelativeDate()


SELECT		ULG_ID,
			Code,
			[Name],
			Suburb,
			(SELECT COUNT(*) FROM Church_ULGContact WHERE ULG_ID = ULG.ULG_ID AND Inactive = 0) as Members,
			(SELECT MAX(Date) FROM Church_PCR P, Church_PCRDate D WHERE P.PCRDate_ID = D.PCRDate_ID AND P.ULG_ID = ULG.ULG_ID AND ReportCompleted = 1) as LastReportCompleted,
			CASE WHEN @CurrentReportDate IS NULL 
				THEN 'No' 
				ELSE	CASE WHEN (SELECT ActiveDate FROM Church_ULG WHERE ULG_ID = ULG.ULG_ID) > @CurrentReportDate
						THEN 'No'
						ELSE ISNULL((SELECT CASE WHEN ReportCompleted = 1 THEN 'No' ELSE 'Yes' END FROM Church_PCR P, Church_PCRDate D WHERE P.PCRDate_ID = D.PCRDate_ID AND P.ULG_ID = ULG.ULG_ID AND D.Date = @CurrentReportDate), 'Yes') END
			END as ReportDue
FROM		Church_ULG ULG
WHERE		(Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
AND			(Ministry_ID = @Ministry_ID OR @Ministry_ID IS NULL)
AND			(Region_ID = @Region_ID OR @Region_ID IS NULL)
AND			((@User_ID IN (SELECT Contact_ID FROM Church_RestrictedAccessULG RAU, Church_ContactRole CR WHERE RAU.ContactRole_ID = CR.ContactRole_ID AND RAU.ULG_ID = ULG.ULG_ID) AND dbo.Common_GetDatabasePermission('PCR_RESTRICTED', @User_ID) = 1)
			OR dbo.Common_GetDatabasePermission('PCR', @User_ID) = 1)
AND			ULG.Inactive = 0
ORDER BY	CASE WHEN LEFT(Code,2) = 'UL' THEN 1 ELSE 2 END,
			ULG.Code
GO
