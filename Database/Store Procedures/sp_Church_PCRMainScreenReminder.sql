SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_PCRMainScreenReminder]

@Contact_ID		INT

AS

DECLARE @PCRDate_ID INT

SELECT	@PCRDate_ID = PCRDate_ID 
FROM	Church_PCRDate
WHERE	[Date] = (	SELECT	MAX([Date])
					FROM	Church_PCRDate
					WHERE	[Date] < dbo.GetRelativeDate())

SELECT	DISTINCT
		ULG.ULG_ID, 
		PCRDate_ID, 
		ULG.Code, 
		ULG.Name,
		PD.[Date],
		DATEADD(MINUTE, -1, DATEADD(hh,(SELECT PCRDueHours FROM Church_PCROptions),[Date])) as DueDate,
		CASE WHEN DATEADD(MINUTE, -1, DATEADD(hh,(SELECT PCRDueHours FROM Church_PCROptions),[Date])) > dbo.GetRelativeDate()
			THEN 'Currently Due'
			ELSE 'Overdue' END as DueDescription
INTO	#t1
FROM	Church_RestrictedAccessULG U,
		Church_ContactRole CR,
		Church_Role R,
		Church_ULG ULG,
		Church_PCRDate PD
WHERE	U.ContactRole_ID = CR.ContactRole_ID
AND		CR.Role_ID = R.Role_ID
AND		CR.Contact_ID = @Contact_ID
AND		ULG.ULG_ID = U.ULG_ID
AND		PD.Date >= DATEADD(WEEK, -2, dbo.GetRelativeDate())
AND		PD.Date <= dbo.GetRelativeDate()
AND		PD.ReportIsDue = 1
AND		R.RoleTypeUL_ID IN (2,3)
AND		U.ULG_ID IN (	SELECT	ULG_ID 
						FROM	Church_ULG
						WHERE	Inactive = 0
						AND		ActiveDate <= (SELECT [Date] FROM Church_PCRDate WHERE PCRDate_ID = @PCRDate_ID))

DELETE	FROM #t1
FROM	Church_PCR P
WHERE	#t1.ULG_ID = P.ULG_ID
AND		#t1.PCRDate_ID = P.PCRDate_ID
AND		P.ReportCompleted = 1

SELECT		*
FROM		#t1
ORDER BY	Code
GO
