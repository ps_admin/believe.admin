SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_PCRPastReportsList]

@ULG_ID			int

AS

DECLARE		@PCRDueHours INT
SELECT		@PCRDueHours = PCRDueHours FROM Church_PCROptions

--Insert Report entries for any dates, starting from the active date of the ULG,
--through to the current date.
INSERT		Church_PCR (PCRDate_ID, ULG_ID, ULGChildren, ULGVisitors, ULGOffering, ReportCompleted)
SELECT		PCRDate_ID, @ULG_ID, 0, 0, 0, 0
FROM		Church_PCRDate D,
			Church_ULG U
WHERE		NOT EXISTS (	SELECT	PCRDate_ID, ULG_ID
					FROM	Church_PCR
					WHERE	PCRDate_ID = D.PCRDate_ID
					AND		ULG_ID = U.ULG_ID)
AND			U.ULG_ID = @ULG_ID
AND			D.Date > U.ActiveDate
AND			D.Date < dbo.GetRelativeDate()


SELECT		P.PCRDate_ID,
			P.ULG_ID,
			D.Date as ReportDate, 
			DateAdd(mi,-1,DateAdd(hh, @PCRDueHours, D.Date)) as ReportDueDate, 
			P.ReportCompletedDate,
			CASE WHEN P.ReportCompletedDate IS NULL THEN '' ELSE (
				CASE WHEN DateAdd(mi,-1,DateAdd(hh, @PCRDueHours, D.Date)) > P.ReportCompletedDate THEN 'Yes' ELSE 'No' END
			) END as CompletedOnTime
FROM		Church_PCR P RIGHT JOIN
			Church_PCRDate D
ON			D.PCRDate_ID = P.PCRDate_ID
WHERE		ULG_ID = @ULG_ID
AND			D.Deleted = 0
ORDER BY	D.Date DESC
GO
