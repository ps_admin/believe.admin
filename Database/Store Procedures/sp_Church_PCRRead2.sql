SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_PCRRead2]

@PCRDate_ID		int,
@ULG_ID			int

AS

DECLARE		@ULGStartDate	DATETIME

SELECT		@ULGStartDate = ActiveDate
FROM		Church_ULG
WHERE		ULG_ID = @ULG_ID	

INSERT	Church_PCR (PCRDate_ID, ULG_ID, ULGChildren, ULGVisitors, ULGOffering, ReportCompleted)
SELECT	P.PCRDate_ID, @ULG_ID, 0, 0, 0, 0
FROM	Church_PCRDate P
WHERE	P.PCRDate_ID = @PCRDate_ID
AND		P.Deleted = 0
AND		P.Date >= @ULGStartDate
AND		NOT EXISTS (	SELECT	PCRDate_ID, ULG_ID
						FROM	Church_PCR
						WHERE	PCRDate_ID = @PCRDate_ID
						AND		ULG_ID = @ULG_ID)


SELECT	*
FROM	Church_PCR P,
		Church_PCRDate PD
WHERE	P.PCRDate_ID = PD.PCRDate_ID
AND		P.ULG_ID = @ULG_ID
AND		PD.PCRDate_ID = @PCRDate_ID
AND		PD.Deleted = 0

-- INSERT into the PCRContact table any contact which have not been added yet for this PCR
INSERT	Church_PCRContact (PCR_ID, Contact_ID, SundayAttendance, ULGAttendance, BoomAttendance, Call, Visit)
SELECT	DISTINCT PCR_ID, Contact_ID, 0, 0, 0, 0, 0
FROM	Church_PCR P,
		Church_PCRDate PD,
		Church_ULGContact U
WHERE	P.PCRDate_ID = PD.PCRDate_ID
AND		P.ULG_ID = U.ULG_ID
AND		PD.PCRDate_ID = @PCRDate_ID
AND		PD.Deleted = 0
AND		U.ULG_ID = @ULG_ID
AND		U.Inactive = 0
AND NOT EXISTS (SELECT	*
				FROM	Church_PCRContact PC
				WHERE	PC.PCR_ID = P.PCR_ID
				AND		PC.Contact_ID = U.Contact_ID)

-- REMOVE from the PCRContact table any contacts that have been made inactive
-- Actually - Don't do this - leave them there!
/*
DELETE FROM Church_PCRContact
FROM	Church_PCR P,
		Church_PCRDate PD,
		Church_ULGContact U
WHERE	Church_PCRContact.PCR_ID = P.PCR_ID
AND		Church_PCRContact.Contact_ID = U.Contact_ID
AND		P.PCRDate_ID = PD.PCRDate_ID
AND		PD.PCRDate_ID = @PCRDate_ID
AND		P.ULG_ID = U.ULG_ID
AND		U.ULG_ID = @ULG_ID
AND		U.Inactive = 1
*/

-- SELECT the monthly totals for the previous 4 ULG's
SELECT	C.Contact_ID,
		SUM(CONVERT(INT,SundayAttendance)) as MonthSundayTotal,
		SUM(CONVERT(INT,ULGAttendance)) as MonthULGTotal,
		SUM(CONVERT(INT,BoomAttendance)) as MonthBoomTotal,
		SUM(Call) as MonthCallTotal,
		SUM(Visit) as MonthVisitTotal
INTO	#Monthly
FROM	Church_PCRContact C,
		Church_PCR PCR
WHERE	C.PCR_ID = PCR.PCR_ID
AND		PCR.PCRDate_ID IN (
			SELECT	TOP 4 PCRDate_ID
			FROM	Church_PCRDate D
			WHERE	PCRDate_ID <> @PCRDate_ID
			AND		Deleted = 0
			AND		D.Date < (SELECT Date FROM Church_PCRDate WHERE PCRDate_ID = @PCRDate_ID)
			ORDER BY D.Date DESC
)
GROUP BY C.Contact_ID

SELECT	DISTINCT C.*,
		CC.FirstName as ContactFirstName,
		CC.LastName as ContactLastName,
		CI.ChurchStatus_ID,
		ISNULL(M.MonthSundayTotal,0) as MonthSundayTotal,
		ISNULL(M.MonthULGTotal,0) as MonthULGTotal,
		ISNULL(M.MonthBoomTotal,0) as MonthBoomTotal,
		ISNULL(M.MonthCallTotal,0) as MonthCallTotal,
		ISNULL(M.MonthVisitTotal,0) as MonthVisitTotal,
		(SELECT MIN(DateJoined) FROM Church_ULGContact WHERE ULG_ID = @ULG_ID AND Contact_ID = C.Contact_ID) AS DateJoined
FROM	Church_PCR PCR,
		Common_Contact CC,
		Common_ContactInternal CI,
		Church_PCRContact C LEFT JOIN #Monthly M
		ON C.Contact_ID = M.Contact_ID
WHERE	CC.Contact_ID = C.Contact_ID
AND		CC.Contact_ID = CI.Contact_ID
AND		PCR.PCR_ID = C.PCR_ID
AND		CI.ChurchStatus_ID NOT IN (7)
AND		PCR.PCRDate_ID = @PCRDate_Id
AND		PCR.ULG_ID = @ULG_ID
ORDER BY C.Contact_ID

SELECT	DISTINCT CC.*
FROM	Church_ContactComment CC,
		Church_PCRContact PC,
		Church_PCR PCR,
		Common_ContactInternal CI
WHERE	CC.Contact_ID = PC.Contact_ID
AND		PCR.PCR_ID = PC.PCR_ID
AND		PCR.PCRDate_ID = @PCRDate_ID
AND		PCR.ULG_ID = @ULG_ID	
AND		CC.Contact_ID = CI.Contact_ID
AND		CI.ChurchStatus_ID NOT IN (7)
GO
