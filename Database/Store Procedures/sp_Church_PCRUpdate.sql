SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_PCRUpdate]
(
	@PCR_ID int,
	@PCRDate_ID int,
	@ULG_ID int,
	@ULGChildren int,
	@ULGVisitors int,
	@ULGOffering money,
	@GeneralComments nvarchar(max) = '',
	@ReportCompleted bit,
	@ReportCompletedDate datetime
)
AS
	SET NOCOUNT ON
	
	UPDATE [Church_PCR]
	SET
		[PCRDate_ID] = @PCRDate_ID,
		[ULG_ID] = @ULG_ID,
		[ULGChildren] = @ULGChildren,
		[ULGVisitors] = @ULGVisitors,
		[ULGOffering] = @ULGOffering,
		[GeneralComments] = @GeneralComments,
		[ReportCompleted] = @ReportCompleted,
		[ReportCompletedDate] = @ReportCompletedDate
	WHERE 
		[PCR_ID] = @PCR_ID
GO
