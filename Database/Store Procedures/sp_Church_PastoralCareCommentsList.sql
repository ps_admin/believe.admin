SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_PastoralCareCommentsList]


@Campus_ID					INT = null,
@Ministry_ID				INT = null,
@Region_ID					INT = null,
@CommentType_ID				INT = null,
@ChurchStatus_ID			INT = null,
@ULG_ID						INT = null,
@Comments					INT = 0,
@StartDate					DATETIME = null,
@EndDate					DATETIME = null,
@IncludeActionedComments	BIT = 0,
@CommentTypeDoNotShow		NVARCHAR(100) = ''

AS

SELECT	C.Contact_ID as ID,
		C.FirstName,
		C.LastName,
		C.Email,
		C.Suburb,
		C.Mobile as [Mobile Phone],
		C.Phone as [Home Phone],
		ISNULL((SELECT M.[Name] FROM Church_Ministry M WHERE Ministry_ID = CI.Ministry_ID ), '') as Ministry,
		ISNULL((SELECT St.[Name] FROM Church_ChurchStatus St WHERE ChurchStatus_ID = CI.ChurchStatus_ID), '') as [Church Status],
		ISNULL((SELECT TOP 1 [Code] + ' ' + [Name] FROM Church_ULG U, Church_ULGContact UC WHERE U.ULG_ID = UC.ULG_ID AND UC.Inactive=0 AND UC.Contact_ID = C.Contact_ID),'') as ULG,
		ISNULL((SELECT TOP 1 [FirstName] + ' ' + [LastName] FROM Church_NPNC N, Common_Contact NC WHERE N.Contact_ID = C.Contact_ID AND N.Carer_ID = NC.Contact_ID AND N.OutcomeStatus_ID IS NULL),'') as Carer,
		CS.Name as [Comment Source],
		CT.Name as [Comment Type],
		cc.Comment_ID as _Comment_ID,
		CC.CommentDate as [Comment Date],
		ISNULL(U.FirstName + ' ' + U.LastName,'') as [Comment By],
		ISNULL(CC.Comment, '') as Comment,
		ISNULL(CC.Actioned,0) as [Actioned],
		C.GUID as [_GUID],
		CI.Campus_ID as [_Campus_ID],
		[_NoCarer] = CONVERT(BIT, ISNULL((SELECT TOP 1 CASE WHEN Carer_ID IS NULL THEN 1 ELSE 0 END FROM Church_NPNC WHERE OutcomeStatus_ID IS NULL AND Contact_ID = C.Contact_ID ORDER BY Carer_ID DESC),0))
INTO	#t1
FROM	Common_Contact C
			LEFT OUTER JOIN (Church_ContactComment CC 
				JOIN Church_CommentSource CS ON CC.CommentSource_ID = CS.CommentSource_ID
				JOIN Church_CommentType CT ON CC.CommentType_ID = CT.CommentType_ID 
					AND (CT.CommentType_ID = @CommentType_ID or @CommentType_ID IS NULL)
					AND	(CT.CommentType_ID NOT IN (@CommentTypeDoNotShow)))
				JOIN Common_Contact U ON CC.CommentBy_ID = U.Contact_ID
			ON C.Contact_ID = CC.Contact_ID 
				AND (CC.CommentDate >= @StartDate or @StartDate IS NULL)
				AND (CC.CommentDate < DateAdd(DD, 1, @EndDate) or @EndDate IS NULL),
		vw_Common_ContactAddressView CAV,
		Common_ContactInternal CI
WHERE	C.Contact_ID = CI.Contact_ID
AND		C.Contact_ID = CAV.Contact_ID
AND		CI.ChurchStatus_ID NOT IN (7) -- Exclude inactive status
AND		(CI.Campus_ID = @Campus_ID or @Campus_ID IS NULL)
AND		(CI.Ministry_ID = @Ministry_ID or @Ministry_ID IS NULL)
AND		(CI.Region_ID = @Region_ID or @Region_ID IS NULL)
AND		(CI.ChurchStatus_ID = @ChurchStatus_ID or @ChurchStatus_ID IS NULL)
AND		(CI.Contact_ID IN (SELECT Contact_ID FROM Church_ULGContact WHERE Inactive = 0 AND ULG_ID = @ULG_ID) or @ULG_ID IS NULL)
AND		(CC.Actioned = 0 OR CC.CommentDate IS NULL OR @IncludeActionedComments = 1)
AND		(@Comments = 0 OR
		 (CC.CommentDate IS NOT NULL AND @Comments = 1) OR
		 (CC.CommentDate IS NULL AND @Comments = 2))
ORDER BY LastName, FirstName

SELECT	*
FROM	#t1
GO
