SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_RegionalPastorDelete]
(
	@RegionalPastor_ID int
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM	[Church_RegionalPastor]
	WHERE	[RegionalPastor_ID] = @RegionalPastor_ID
GO
