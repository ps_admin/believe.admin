SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_RegionalPastorInsert]
(
	@Campus_ID int,
	@Ministry_ID int,
	@Region_ID int,
	@Contact_ID int
)
AS
	SET NOCOUNT ON

	INSERT INTO [Church_RegionalPastor]
	(
		[Campus_ID],
		[Ministry_ID],
		[Region_ID],
		[Contact_ID]
	)
	VALUES
	(
		@Campus_ID,
		@Ministry_ID,
		@Region_ID,
		@Contact_ID
	)

	SELECT RegionalPastor_ID = SCOPE_IDENTITY();
GO
