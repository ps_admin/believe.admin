SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_RegionalPastorList]

@Campus_ID	INT

AS

SELECT		RP.RegionalPastor_ID AS ID,
			C.Contact_ID AS ContactID,
			C.FirstName + ' ' + C.LastName as Name,
			Ca.Name Campus,
			M.Name as Ministry,
			ISNULL((SELECT Name from Church_Region WHERE Region_ID = RP.Region_ID),'') as Region
FROM		Church_RegionalPastor RP,
			Common_Contact C,
			Church_Campus Ca,
			Church_Ministry M
WHERE		RP.Contact_ID = C.Contact_ID
AND			RP.Campus_ID = Ca.Campus_ID
AND			RP.Ministry_ID = M.Ministry_ID
AND			(Ca.Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
ORDER BY	C.LastName
GO
