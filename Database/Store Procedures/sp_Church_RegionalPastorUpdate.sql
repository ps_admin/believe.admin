SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_RegionalPastorUpdate]
(
	@RegionalPastor_ID int,
	@Campus_ID int,
	@Ministry_ID int,
	@Region_ID int,
	@Contact_ID int
)
AS
	SET NOCOUNT ON
	
	UPDATE [Church_RegionalPastor]
	SET
		[Campus_ID] = @Campus_ID,
		[Ministry_ID] = @Ministry_ID,
		[Region_ID] = @Region_ID,
		[Contact_ID] = @Contact_ID
	WHERE 
		[RegionalPastor_ID] = @RegionalPastor_ID
GO
