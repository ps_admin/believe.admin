SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_Report247PrayerList]

@Campus_ID			int = null,
@Day_ID				int,
@StartTime_ID		int,
@EndTime_ID			int,
@ContactMethod_ID	int

AS

SELECT		ID = C.Contact_ID,
			PD.Name as [Day],
			PT.Name as [Time],
			D.Name as [Duration],
			C.FirstName as [First Name],
			C.LastName as [Last Name],
			CM.Name as [Contact Method],
			CASE WHEN C.Email = '' THEN C.Email2 ELSE Email END as Email,
			C.Mobile,
			[Street Address 1] = C.Address1,
			[Street Address 2] = C.Address2,
			C.Suburb,
			[State] = ISNULL(CASE WHEN C.State_ID = 9 THEN StateOther ELSE S.State END,''),
			C.PostCode
FROM		Common_Contact C
				LEFT JOIN Common_GeneralState S ON C.State_ID = S.State_ID,
			Common_ContactInternal CI,
			Church_247Prayer P,
			Church_247PrayerDay PD,
			Church_247PrayerTime PT,
			Church_247PrayerDuration D,
			Church_247PrayerContactMethod CM
WHERE		C.Contact_ID = P.Contact_ID
AND			C.Contact_ID = CI.Contact_ID
AND			P.Day_ID = PD.Day_ID
AND			P.Time_ID = PT.Time_ID
AND			P.Duration_ID = D.Duration_ID
AND			P.ContactMethod_ID = CM.ContactMethod_ID
AND			CI.ChurchStatus_ID <> 7 --Do not include inactive people.
AND			(CI.Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
AND			(P.Day_ID = @Day_ID OR @Day_ID IS NULL)
AND			(P.Time_ID >= @StartTime_ID OR @StartTime_ID IS NULL)
AND			(P.Time_ID <= @EndTime_ID OR @EndTime_ID IS NULL)
AND			(P.ContactMethod_ID = @ContactMethod_ID OR @ContactMethod_ID IS NULL)

UNION ALL

SELECT		ID = C.Contact_ID,
			PT.Name as [Day],
			PT.Name as [Time],
			D.Name as [Duration],
			C.FirstName as [First Name],
			C.LastName as [Last Name],
			CM.Name as [Contact Method],
			C.Email2,
			C.Mobile,
			[Street Address 1] = C.Address1,
			[Street Address 2] = C.Address2,
			C.Suburb,
			[State] = ISNULL(CASE WHEN C.State_ID = 9 THEN StateOther ELSE S.State END,''),
			C.PostCode
FROM		Common_Contact C
				LEFT JOIN Common_GeneralState S ON C.State_ID = S.State_ID,
			Common_ContactInternal CI,
			Church_247Prayer P,
			Church_247PrayerDay PD,
			Church_247PrayerTime PT,
			Church_247PrayerDuration D,
			Church_247PrayerContactMethod CM
WHERE		C.Contact_ID = P.Contact_ID
AND			C.Contact_ID = CI.Contact_ID
AND			P.Day_ID = PD.Day_ID
AND			P.Time_ID = PT.Time_ID
AND			P.Duration_ID = D.Duration_ID
AND			P.ContactMethod_ID = CM.ContactMethod_ID
AND			C.Email <> '' AND C.Email2 <> ''
AND			CI.ChurchStatus_ID <> 7 --Do not include inactive people.
AND			(CI.Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
AND			(P.Day_ID = @Day_ID OR @Day_ID IS NULL)
AND			(P.Time_ID >= @StartTime_ID OR @StartTime_ID IS NULL)
AND			(P.Time_ID <= @EndTime_ID OR @EndTime_ID IS NULL)
AND			(P.ContactMethod_ID = 1 AND @ContactMethod_ID = 1)
GO
