SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_Report247PrayerStats]

@Campus_ID	INT = null

AS

--Create Master Table
SELECT	Time_ID,
		[Name] as [Time]
INTO	#Master
FROM	Church_247PrayerTime
UNION ALL 
SELECT	100, 'Total'


--Create Stats Calculation Table

SELECT	IDENTITY(int) as ID,
		D.Day_ID,
		T.Time_ID,
		D.Name as [Day],
		T.Name as [Time],
		CONVERT(INT,0) as Number
INTO	#Stats
FROM	Church_247PrayerDay D, 
		Church_247PrayerTime T
ORDER BY	D.SortOrder,
			T.SortOrder
			
CREATE INDEX IX_ID ON #Stats(ID)
CREATE INDEX IX_Day ON #Stats(Day_ID)
CREATE INDEX IX_Time ON #Stats(Time_ID)
			
UPDATE #Stats SET Number = (SELECT	COUNT(*) 
							FROM	Common_ContactInternal C,
									Church_247Prayer P,
									Church_247PrayerDuration D
							WHERE	C.Contact_ID = P.Contact_ID
							AND		P.Duration_ID = D.Duration_ID
							AND		(C.Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
							AND		C.ChurchStatus_ID <> 7 --Do not include inactive people.
							AND		((P.Time_ID = #Stats.Time_ID AND P.Day_ID = #Stats.Day_ID)
							OR		#Stats.ID
									BETWEEN (SELECT ID FROM #Stats WHERE Day_ID = P.Day_ID AND Time_ID = P.Time_ID)
									AND	((SELECT ID FROM #Stats WHERE Day_ID = P.Day_ID AND Time_ID = P.Time_ID) + (D.Duration * 2) - 1)
							OR		#Stats.ID
									BETWEEN 1 
									AND	CASE WHEN (SELECT ID FROM #Stats WHERE Day_ID = P.Day_ID AND Time_ID = P.Time_ID) + (D.Duration * 2) - 1 > (SELECT MAX(ID) FROM #Stats)
										THEN (SELECT ID FROM #Stats WHERE Day_ID = P.Day_ID AND Time_ID = P.Time_ID) + (D.Duration * 2) - 1 - (SELECT MAX(ID) FROM #Stats)
										ELSE 0 END
									)
						)

--Create a Cursor to populate the Master Table
DECLARE @Day_ID INT,
		@Day	NVARCHAR(50),
		@SQL	NVARCHAR(MAX)

DECLARE DayCursor CURSOR FOR
	SELECT Day_ID, Name FROM Church_247PrayerDay
OPEN DayCursor

FETCH NEXT FROM DayCursor INTO @Day_ID, @Day
WHILE @@FETCH_STATUS = 0 BEGIN
	
	SET @SQL = 'ALTER TABLE #Master ADD [' + @Day + '] int;'
	EXEC(@SQL)
	SET @SQL = 'UPDATE #Master SET [' + @Day + '] = (
					SELECT	SUM(Number)
					FROM	#Stats 
					WHERE	Day_ID = ' + CONVERT(NVARCHAR, @Day_ID) + ' 
					AND		(Time_ID = #Master.Time_ID OR #Master.Time_ID = 100))'
	EXEC(@SQL)

	FETCH NEXT FROM DayCursor INTO @Day_ID, @Day
END

CLOSE DayCursor
DEALLOCATE DayCursor

ALTER TABLE #Master ADD [Total] int
UPDATE #Master SET [Total] = (SELECT SUM(Number) FROM #Stats WHERE Time_ID = #Master.Time_ID OR #Master.Time_ID = 100)

ALTER TABLE #Master DROP COLUMN Time_ID

SELECT * FROM #Master

DROP TABLE #Master
GO
