SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ReportBirthday]

@Month					INT,
@Campus_ID				INT,
@Ministry_ID			INT,
@Region_ID		INT,
@ULG_ID					INT,
@User_ID				INT

AS
SELECT		FirstName as [First Name],
			LastName as [Last Name],
			FirstName + ' ' + LastName as [Full Name],
			CONVERT(NVARCHAR,DAY(DateOfBirth)) + ' ' + DATENAME(mm,DateOfBirth) + ' ' + CONVERT(NVARCHAR,YEAR(DateOfBirth)) as [Date Of Birth],
			DateOfBirth as _DateOfBirthSort,
			CONVERT(NVARCHAR,DAY(DateOfBirth)) + ' ' + DATENAME(mm,DateOfBirth) as [Date],
			DAY(DateOfBirth) as _DateSort,
			Ca.Name as Campus,
			M.[Name] as Ministry,
			ISNULL((SELECT [Name] FROM Church_Region WHERE Region_ID = CI.Region_ID),'') as Region
FROM		Common_Contact C,
			Common_ContactInternal CI,
			Church_Campus Ca,
			Church_Ministry M
WHERE		C.Contact_ID = CI.Contact_ID
AND			CI.Campus_ID = Ca.Campus_ID
AND			CI.Ministry_ID = M.Ministry_ID
AND			CI.ChurchStatus_ID IN (1, 2, 3, 4 ,5) --Partners, NP and NC Only
AND			MONTH(DateOfBirth) = @Month
AND			(M.Ministry_ID = @Ministry_ID OR @Ministry_ID IS NULL)
AND			(CI.Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
AND			(CI.Region_ID = @Region_ID OR @Region_ID IS NULL)
AND			(C.Contact_ID IN (SELECT Contact_ID FROM Church_ULGContact WHERE ULG_ID = @ULG_ID and Inactive = 0) OR @ULG_ID IS NULL)

--Add Access Restrictions
AND			(C.Contact_ID IN (
							SELECT	Contact_ID FROM Common_ContactInternal
							WHERE	InformationIsConfidential = 0 
							AND		dbo.Common_GetDatabasePermission('CONTACT_DETAILS_INTERNAL_FULL', @User_ID) = 1)

			 OR	C.Contact_ID IN (
							SELECT	Contact_ID FROM Common_ContactInternal
							WHERE	InformationIsConfidential = 0 
							AND		dbo.Common_GetDatabasePermission('CONTACT_DETAILS_INTERNAL_RESTRICTED', @User_ID) = 1
							AND		(Contact_ID IN (SELECT	CR.Contact_ID 
												FROM	Church_ContactRole CR,
														Church_Role R,
														Church_RestrictedAccessRole RAR,
														Church_ContactRole CR2
												WHERE	R.Role_ID = CR.Role_ID
												AND		RAR.Role_Id = R.Role_ID
												AND		CR2.ContactRole_ID = RAR.ContactRole_ID
												AND		CR2.Contact_ID = @User_ID)
							OR Contact_ID IN  (SELECT	UC.Contact_ID 
												FROM	Church_ContactRole CR,
														Church_RestrictedAccessULG RAU,
														Church_ULGContact UC
												WHERE	CR.ContactRole_ID = RAU.ContactRole_ID
												AND		RAU.ULG_ID = UC.ULG_ID
												AND		UC.InActive=0
												AND		CR.Contact_ID = @User_ID)))
			 OR	C.Contact_ID IN (
							SELECT	Contact_ID FROM Common_ContactInternal
							WHERE	InformationIsConfidential = 1
							AND		dbo.Common_GetDatabasePermission('CONTACT_DETAILS_INTERNAL_CONFIDENTIAL', @User_ID) = 1))
GO
