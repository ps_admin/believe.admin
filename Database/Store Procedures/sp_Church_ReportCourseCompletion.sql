SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ReportCourseCompletion]

@CourseInstance_ID	INT,
@DisplayType		INT

AS

SELECT		DISTINCT 
			C.Contact_ID as ID,
			C.FirstName as [First Name],
			C.LastName as [Last Name],
			C.Email,
			C.Mobile,
			CI.Course_ID,
			Completed = CONVERT(BIT, 0),
			Points = CONVERT(INT, 0),
			[Sessions Completed] = CONVERT(NVARCHAR(200),''),
			[Topics Completed] = CONVERT(NVARCHAR(MAX),''),
			CCE.Comments
INTO		#t1
FROM		Common_Contact C,
			Church_ContactCourseEnrolment CCE,
			Church_CourseInstance CI
WHERE		C.Contact_ID = CCE.Contact_ID	
AND			CCE.CourseInstance_ID = CI.CourseInstance_ID
AND			CCE.CourseInstance_ID = @CourseInstance_ID		

UPDATE		#t1
SET			Email = ''
WHERE		ID NOT IN (SELECT Contact_ID FROM Common_ContactMailingList WHERE MailingList_ID IN (SELECT MailingList_ID FROM Common_MailingList WHERE TickedForCampus_ID IS NOT NULL) AND SubscriptionActive = 1)
OR			ID NOT IN (SELECT Contact_ID FROM Common_ContactMailingListType WHERE MailingListType_ID = 2 AND SubscriptionActive = 1)
OR			ID IN (SELECT Contact_ID FROM Common_Contact WHERE DoNotIncludeEmail1InMailingList = 1)

/*
UPDATE		#t1
SET			Mobile = ''
WHERE		ID NOT IN (SELECT Contact_ID FROM Common_ContactMailingList WHERE MailingList_ID IN (SELECT MailingList_ID FROM Common_MailingList WHERE TickedForCampus_ID IS NOT NULL) AND SubscriptionActive = 1)
OR			ID NOT IN (SELECT Contact_ID FROM Common_ContactMailingListType WHERE MailingListType_ID = 3 AND SubscriptionActive = 1)
*/

SELECT		DISTINCT CEA.Contact_ID, 
			CI.Course_ID,
			CT.CourseTopic_ID, 
			CT.PointWeighting 
INTO		#t2
FROM		Church_CourseEnrolmentAttendance CEA,
			Church_CourseEnrolmentAttendanceTopicsCovered TC,
			Church_CourseTopic CT,
			Church_CourseSession CS,
			Church_CourseInstance CI,
			#t1
WHERE		CEA.CourseEnrolmentAttendance_ID = TC.CourseEnrolmentAttendance_ID
AND			TC.CourseTopic_ID = CT.CourseTopic_ID
AND			CEA.CourseSession_ID = CS.CourseSession_ID
AND			CS.CourseInstance_ID = CI.CourseInstance_ID
AND			CEA.Contact_ID = #t1.ID

UPDATE		#t1
SET			Points = ISNULL((
				SELECT	SUM(PointWeighting)
				FROM	#t2
				WHERE	#t2.Course_ID = #t1.Course_ID
				AND		#t2.Contact_ID = #t1.ID),0)

UPDATE		#t1
SET			Completed = CASE WHEN Points >= (	SELECT	SUM(C.PointsRequiredForCompletion) 
												FROM	Church_Course C,
														Church_CourseInstance CI
												WHERE	C.Course_ID = CI.Course_ID
												AND		CI.CourseInstance_ID = @CourseInstance_ID)
						THEN 1 else 0 END

IF @DisplayType = 1  DELETE FROM #t1 WHERE Completed = 0
IF @DisplayType = 2  DELETE FROM #t1 WHERE Completed = 1


DECLARE	@Contact_ID	INT
DECLARE	@Session	DATETIME
DECLARE	@Topic		NVARCHAR(50)

DECLARE	ContactCursor CURSOR FOR
	SELECT	ID
	FROM	#t1
OPEN ContactCursor

FETCH NEXT FROM ContactCursor INTO @Contact_ID
WHILE @@FETCH_STATUS = 0 BEGIN

	DECLARE SessionCursor CURSOR FOR
		SELECT		DISTINCT CS.Date 
		FROM		Church_CourseEnrolmentAttendance CEA,
					Church_CourseSession CS,
					Church_CourseInstance CoI,
					#t1
		WHERE		CEA.CourseSession_ID = CS.CourseSession_ID
		AND			CEA.Contact_ID = #t1.ID
		AND			CS.CourseInstance_ID = CoI.CourseInstance_ID
		AND			CoI.Course_ID = #t1.Course_ID
		AND			#t1.ID = @Contact_ID
		ORDER BY	CS.Date
	OPEN SessionCursor
	FETCH NEXT FROM SessionCursor INTO @Session
	WHILE @@FETCH_STATUS = 0 BEGIN
		UPDATE #t1 SET [Sessions Completed] = [Sessions Completed] + CONVERT(NVARCHAR,@Session,106) + ', ' WHERE ID = @Contact_ID
		FETCH NEXT FROM SessionCursor INTO @Session
	END
	CLOSE SessionCursor
	DEALLOCATE SessionCursor
	
	
	DECLARE TopicCursor CURSOR FOR
		SELECT		DISTINCT CT.Name
		FROM		Church_CourseTopic CT,
					Church_CourseEnrolmentAttendanceTopicsCovered TC,
					Church_CourseEnrolmentAttendance CEA,
					Church_CourseSession CS,
					Church_CourseInstance CoI,
					#t1
		WHERE		CT.CourseTopic_ID = TC.CourseTopic_ID
		AND			TC.CourseEnrolmentAttendance_ID = CEA.CourseEnrolmentAttendance_ID
		AND			CEA.Contact_ID = #t1.ID
		AND			CEA.CourseSession_ID = CS.CourseSession_ID
		AND			CS.CourseInstance_ID = CoI.CourseInstance_ID
		AND			CoI.Course_ID = #t1.Course_ID
		AND			#t1.ID = @Contact_ID
		ORDER BY	CT.Name
	OPEN TopicCursor
	FETCH NEXT FROM TopicCursor INTO @Topic
	WHILE @@FETCH_STATUS = 0 BEGIN
		UPDATE #t1 SET [Topics Completed] = [Topics Completed] + @Topic + ', ' WHERE ID = @Contact_ID
		FETCH NEXT FROM TopicCursor INTO @Topic
	END
	CLOSE TopicCursor
	DEALLOCATE TopicCursor
		
	FETCH NEXT FROM ContactCursor INTO @Contact_ID
END

CLOSE ContactCursor
DEALLOCATE ContactCursor

UPDATE #t1 SET	[Sessions Completed] = LEFT([Sessions Completed], LEN([Sessions Completed])-1) WHERE LEN([Sessions Completed]) > 0
UPDATE #t1 SET	[Topics Completed] = LEFT([Topics Completed], LEN([Topics Completed])-1) WHERE LEN([Topics Completed]) > 0

ALTER TABLE #t1
DROP COLUMN Course_ID

SELECT		*
FROM		#t1
ORDER BY	3,2
GO
