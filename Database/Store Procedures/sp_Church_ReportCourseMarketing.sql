SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ReportCourseMarketing]

@CourseInstance_ID	INT,
@DisplayType		INT

AS

SELECT		DISTINCT 
			CCE.CourseInstance_ID AS _CourseInstance_ID,
			C.Contact_ID as ID,
			C.FirstName as [First Name],
			C.LastName as [Last Name],
			CASE WHEN C.DoNotIncludeEmail1InMailingList=1 THEN '' ELSE C.Email END as Email,
			CASE WHEN C.DoNotIncludeEmail2InMailingList=1 THEN '' ELSE C.Email2 END as [Email #2],
			C.Mobile,
			[Introductory Email Sent] = CONVERT(BIT, CASE WHEN CCE.IntroductoryEmailSentDate is null THEN 0 ELSE 1 END),
			[Date Introductory Email Sent] = CCE.IntroductoryEmailSentDate
INTO		#t1
FROM		Common_Contact C,
			Church_ContactCourseEnrolment CCE
WHERE		C.Contact_ID = CCE.Contact_ID	
AND			CCE.CourseInstance_ID = @CourseInstance_ID		

IF @DisplayType = 1  DELETE FROM #t1 WHERE [Introductory Email Sent] = 1
IF @DisplayType = 2  DELETE FROM #t1 WHERE [Introductory Email Sent] = 0

SELECT		*
FROM		#t1
ORDER BY	3,2
GO
