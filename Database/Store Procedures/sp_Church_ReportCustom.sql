SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ReportCustom]

@ChurchStatus			NVARCHAR(MAX) = '',
@Campus					NVARCHAR(MAX) = '',
@Ministry				NVARCHAR(MAX) = '',
@Region					NVARCHAR(MAX) = '',
@ULG					NVARCHAR(MAX) = '',
@Role					NVARCHAR(MAX) = '',
@CampusRole_ID			INT = 0,
@MinAge					INT = 0,
@MaxAge					INT = 0,
@Gender					NVARCHAR(6) = '',
@Suburbs				NVARCHAR(MAX) = '',
@States					NVARCHAR(MAX) = '',
@Conference_ID			INT = 0,
@NotRegisteredConference_ID			INT = 0,
@Course					NVARCHAR(MAX) = '',
@UseMailingList			BIT = 0,
@MailingList			NVARCHAR(MAX) = '',
@MailingListType_ID		INT = null,
@User_ID				INT = 0,
@ShowRoles				BIT = 0

AS

DECLARE @SQL	NVARCHAR(MAX)

SELECT		C.Contact_ID as ID,
			C.FirstName as [First Name],
			C.LastName as [Last Name],
			C.Address1,
			C.Address2,
			C.Suburb,
			ISNULL(CASE WHEN State_ID = 9 THEN C.StateOther ELSE (SELECT State FROM Common_GeneralState WHERE State_ID = C.State_ID) END,'') as State,
			C.Postcode,
			ISNULL((SELECT Country FROM Common_GeneralCountry WHERE Country_ID = C.Country_ID), '') as Country,
			ISNULL((SELECT [Name] FROM Church_Campus WHERE Campus_ID = CI.Campus_ID),'') as [Campus],
			ISNULL((SELECT [Name] FROM Church_Ministry WHERE Ministry_ID = CI.Ministry_ID),'') as [Ministry],
			ISNULL((SELECT [Name] FROM Church_Region WHERE Region_ID = CI.Region_ID),'') as [Region],
			C.Email,
			C.Email2,
			C.Mobile,
			C.Phone,
			C.PhoneWork as [Work Phone],
			C.Gender,
			C.DateOfBirth as [DOB],
			CI.Occupation,
			ISNULL((SELECT [Name] FROM Church_ChurchStatus WHERE ChurchStatus_ID = CI.ChurchStatus_ID),'') as [Church Status],
			CONVERT(NVARCHAR(500), '''') AS [ULG],
			CI.KidsMinistryGroup as [Pod Group],
			CONVERT(NVARCHAR(MAX), '') AS [Roles],
			ISNULL((SELECT TOP 1 Name FROM vw_Users U, Church_NPNC N WHERE U.Contact_ID = N.Carer_ID AND N.OutcomeStatus_ID IS NULL AND N.Contact_ID = C.Contact_ID),'') as [Carer],
			CI.PrimaryCarer as [Primary Carer],
			CI.CovenantFormDate as [Covenant Form Date],
			CI.VolunteerFormDate as [Volunteer Form Date],
			C.VolunteerWWCC [WWCC],
			C.VolunteerWWCCDate as [WWCC Date],
			C.VolunteerPoliceCheck as [Police Check],
			C.VolunteerPoliceCheckDate as [Police Check Date],
			ISNULL((SELECT [Name] FROM Church_EntryPoint WHERE EntryPoint_ID = CI.EntryPoint_ID),'') as [Entry Point],
			ISNULL((SELECT [Name] FROM Church_ChurchService WHERE ChurchService_ID = CI.ServiceAttending_ID),'') as [Service Attending],
			ISNULL((SELECT [Name] FROM Church_ModeOfTransport WHERE ModeOfTransport_ID = CI.ModeOfTransport_ID),'') as [Mode of Transport],
			ISNULL((SELECT [Name] FROM Church_CarParkUsed WHERE CarParkUsed_ID = CI.CarParkUsed_ID),'') as [Car Park],
			CONVERT(NVARCHAR(500),'') as [Conferences Registered],
			C.State_ID
INTO		#CustomReport
FROM		Common_Contact C,
			Common_ContactInternal CI
WHERE		1=0

SET	@SQL = '
	INSERT  #CustomReport
	SELECT	C.Contact_ID as ID,
			C.FirstName as [First Name],
			C.LastName as [Last Name],
			C.Address1,
			C.Address2,
			C.Suburb,
			CASE WHEN ISNULL((SELECT State FROM Common_GeneralState WHERE State_ID = C.State_ID),'''') = ''Other''
				THEN C.StateOther
				ELSE ISNULL((SELECT State FROM Common_GeneralState WHERE State_ID = C.State_ID),'''')
			END as State,
			C.Postcode,
			ISNULL((SELECT Country FROM Common_GeneralCountry WHERE Country_ID = C.Country_ID), '''') as Country,
			ISNULL((SELECT [Name] FROM Church_Campus WHERE Campus_ID = CI.Campus_ID),'''') as [Campus],
			ISNULL((SELECT [Name] FROM Church_Ministry WHERE Ministry_ID = CI.Ministry_ID),'''') as [Ministry],
			ISNULL((SELECT [Name] FROM Church_Region WHERE Region_ID = CI.Region_ID),'''') as [Region],
			C.Email,
			C.Email2,
			C.Mobile,
			C.Phone,
			C.PhoneWork as [Work Phone],
			C.Gender,
			C.DateOfBirth as [DOB],
			ISNULL(CI.Occupation,''''),
			ISNULL((SELECT [Name] FROM Church_ChurchStatus WHERE ChurchStatus_ID = CI.ChurchStatus_ID),'''') as [Church Status],
			CONVERT(NVARCHAR(500), '''') AS [ULG],
			ISNULL(CI.KidsMinistryGroup, '''') as [Pod Group],
			CONVERT(NVARCHAR(500), '''') AS [Roles],
			ISNULL((SELECT TOP 1 Name FROM vw_Users U, Church_NPNC N WHERE U.Contact_ID = N.Carer_ID AND N.OutcomeStatus_ID IS NULL AND N.Contact_ID = C.Contact_ID),'''') as [Carer],
			ISNULL(CI.PrimaryCarer,'''') as [Primary Carer],
			CI.CovenantFormDate as [Covenant Form Date],
			CI.VolunteerFormDate as [Volunteer Form Date],
			C.VolunteerWWCC [WWCC],
			C.VolunteerWWCCDate as [WWCC Date],
			C.VolunteerPoliceCheck as [Police Check],
			C.VolunteerPoliceCheckDate as [Police Check Date],
			ISNULL((SELECT [Name] FROM Church_EntryPoint WHERE EntryPoint_ID = CI.EntryPoint_ID),'''') as [Entry Point],
			ISNULL((SELECT [Name] FROM Church_ChurchService WHERE ChurchService_ID = CI.ServiceAttending_ID),'''') as [Service Attending],
			ISNULL((SELECT [Name] FROM Church_ModeOfTransport WHERE ModeOfTransport_ID = CI.ModeOfTransport_ID),'''') as [Mode of Transport],
			ISNULL((SELECT [Name] FROM Church_CarParkUsed WHERE CarParkUsed_ID = CI.CarParkUsed_ID),'''') as [Car Park],
			CONVERT(NVARCHAR(500),'''') as [Conferences Registered],
			C.State_ID
FROM		Common_Contact C LEFT JOIN Common_ContactInternal CI
			ON C.Contact_ID = CI.Contact_ID

--Add Access Restrictions
WHERE		(C.Contact_ID IN (
							SELECT	Contact_ID FROM Common_ContactInternal
							WHERE	InformationIsConfidential = 0 
							AND		dbo.Common_GetDatabasePermission(''CONTACT_DETAILS_INTERNAL_FULL'', ' + CONVERT(NVARCHAR, @User_ID) + ') = 1)

			 OR	C.Contact_ID IN (
							SELECT	Contact_ID FROM Common_ContactInternal
							WHERE	InformationIsConfidential = 0 
							AND		dbo.Common_GetDatabasePermission(''CONTACT_DETAILS_INTERNAL_RESTRICTED'', ' + CONVERT(NVARCHAR, @User_ID) + ') = 1
							AND		(Contact_ID IN (SELECT	CR.Contact_ID 
												FROM	Church_ContactRole CR,
														Church_Role R,
														Church_RestrictedAccessRole RAR,
														Church_ContactRole CR2
												WHERE	R.Role_ID = CR.Role_ID
												AND		RAR.Role_Id = R.Role_ID
												AND		CR2.ContactRole_ID = RAR.ContactRole_ID
												AND		CR2.Contact_ID = ' + CONVERT(NVARCHAR, @User_ID) + ')
							OR Contact_ID IN  (SELECT	UC.Contact_ID 
												FROM	Church_ContactRole CR,
														Church_RestrictedAccessULG RAU,
														Church_ULGContact UC
												WHERE	CR.ContactRole_ID = RAU.ContactRole_ID
												AND		RAU.ULG_ID = UC.ULG_ID
												AND		UC.InActive=0
												AND		CR.Contact_ID = ' + CONVERT(NVARCHAR, @User_ID) + ')))
			 OR	C.Contact_ID IN (
							SELECT	Contact_ID FROM Common_ContactInternal
							WHERE	InformationIsConfidential = 1
							AND		dbo.Common_GetDatabasePermission(''CONTACT_DETAILS_INTERNAL_CONFIDENTIAL'', ' + CONVERT(NVARCHAR, @User_ID) + ') = 1)
			 
			 OR	C.Contact_ID IN (
							SELECT	Contact_ID FROM Common_ContactExternal
							WHERE	dbo.Common_GetDatabasePermission(''CONTACT_DETAILS_EXTERNAL'', ' + CONVERT(NVARCHAR, @User_ID) + ') = 1))' + CHAR(10) + CHAR(10)

IF @ChurchStatus <> ''	
	SET @SQL = @SQL + ' AND CI.ChurchStatus_ID IN (' + @ChurchStatus + ')'
ELSE IF @Conference_ID = 0 				
	SET @SQL = @SQL + ' AND ISNULL(CI.ChurchStatus_ID,0) NOT IN (7)' --Default to not including Inactive

IF @Campus <> ''		SET @SQL = @SQL + ' AND CI.Campus_ID IN (' + @Campus + ')'
IF @Ministry <> ''		SET @SQL = @SQL + ' AND CI.Ministry_ID IN (' + @Ministry + ')'
IF @ULG <> ''			SET @SQL = @SQL + ' AND CI.Contact_ID IN (SELECT Contact_ID FROM Church_ULGContact WHERE ULG_ID IN (' + @ULG + ') AND Inactive=0)'
IF @Course <> ''		SET @SQL = @SQL + ' AND CI.Contact_ID IN (SELECT Contact_ID FROM Church_ContactCourseInstance CCI, Church_CourseInstance CC WHERE CCI.CourseInstance_Id = CC.CourseInstance_ID AND CC.Course_ID IN (' + @Course + '))'
IF @Role <> ''			SET @SQL = @SQL + ' AND CI.Contact_ID IN (SELECT Contact_ID FROM Church_ContactRole CR WHERE CR.Role_ID IN (' + @Role + ') AND (CR.Campus_ID = ' + CONVERT(NVARCHAR,@CampusRole_ID) + ' OR ' + CONVERT(NVARCHAR,@CampusRole_ID) + ' = 0))'
IF @States <> ''		SET @SQL = @SQL + ' AND C.State_ID IN (' + @States + ')'

IF @MinAge > 0			SET @SQL = @SQL + ' AND DATEADD(YY, ' + CONVERT(NVARCHAR, @MinAge + 1) + ', C.DateOfBirth) <= dbo.GetRelativeDate()'
IF @MaxAge > 0			SET @SQL = @SQL + ' AND DATEADD(YY, ' + CONVERT(NVARCHAR, @MaxAge + 1) + ', C.DateOfBirth) >= dbo.GetRelativeDate()'
IF @Gender <> ''		SET @SQL = @SQL + ' AND Gender = ''' + @Gender + ''''


DECLARE @RegionSQL NVARCHAR(MAX)
DECLARE @Ministry_ID NVARCHAR(10)
DECLARE @Region_ID NVARCHAR(10)
DECLARE @Campus_ID NVARCHAR(10)
DECLARE @RoleSQL NVARCHAR(MAX)
DECLARE @Role_ID NVARCHAR(10)

IF @Region <> '' BEGIN
	DECLARE RegionCursor CURSOR FOR
		SELECT strval FROM dbo.SplitString(@Region,',')
	OPEN RegionCursor
	SET @RegionSQL = ''
	
	FETCH NEXT FROM RegionCursor INTO @Campus_ID
	FETCH NEXT FROM RegionCursor INTO @Ministry_ID
	FETCH NEXT FROM RegionCursor INTO @Region_ID
	WHILE @@FETCH_STATUS = 0 BEGIN
		SET @RegionSQL = @RegionSQL + ' OR (CI.Campus_ID = ' + @Campus_ID + ' AND ' +
										   'CI.Ministry_ID = ' + @Ministry_ID + ' AND ' +
										   'CI.Region_ID = ' + @Region_ID + ') '
		FETCH NEXT FROM RegionCursor INTO @Campus_ID
		FETCH NEXT FROM RegionCursor INTO @Ministry_ID
		FETCH NEXT FROM RegionCursor INTO @Region_ID
	END
	PRINT @RegionSQL
	IF @RegionSQL <> '' 
		SET @SQL = @SQL + ' AND (' + RIGHT(@RegionSQL, LEN(@RegionSQL) - 3) + ')'

	CLOSE RegionCursor
	DEALLOCATE RegionCursor
END

IF @Suburbs <> ''	BEGIN
	DECLARE @Suburb NVARCHAR(MAX)
	DECLARE @SuburbSQL NVARCHAR(MAX)
	DECLARE SuburbCursor CURSOR FOR
		SELECT strval FROM dbo.SplitString(@Suburbs,',')
	OPEN SuburbCursor
	SET @SuburbSQL = ''

	FETCH NEXT FROM SuburbCursor INTO @Suburb
	WHILE @@FETCH_STATUS = 0 BEGIN
		IF @Suburb <> ''
			SET @SuburbSQL = @SuburbSQL + ' OR C.Suburb LIKE ''%' + RTRIM(LTRIM(@Suburb)) + '%'''
		FETCH NEXT FROM SuburbCursor INTO @Suburb
	END

	IF @SuburbSQL <> '' 
		SET @SQL = @SQL + ' AND (' + RIGHT(@SuburbSQL, LEN(@SuburbSQL) - 3) + ')'

	CLOSE SuburbCursor
	DEALLOCATE SuburbCursor
END
IF @Conference_ID > 0	SET @SQL = @SQL + ' AND C.Contact_ID IN (SELECT Contact_ID FROM Events_Registration R, Events_Venue V WHERE R.Venue_ID = V.Venue_ID AND V.Conference_ID = ' + CONVERT(NVARCHAR, @Conference_ID) + ')'
IF @NotRegisteredConference_ID > 0	SET @SQL = @SQL + ' AND C.Contact_ID NOT IN (SELECT Contact_ID FROM Events_Registration R, Events_Venue V WHERE R.Venue_ID = V.Venue_ID AND V.Conference_ID = ' + CONVERT(NVARCHAR, @NotRegisteredConference_ID) + ')'

IF @UseMailingList = 1 BEGIN
	SET @SQL = @SQL + ' AND C.Contact_ID IN (SELECT Contact_ID FROM Common_ContactMailingList WHERE SubscriptionActive = 1 AND MailingList_ID IN (' + @MailingList + '))' + CHAR(10)
	SET @SQL = @SQL + ' AND C.Contact_ID IN (SELECT Contact_ID FROM Common_ContactMailingListType WHERE SubscriptionActive = 1 AND MailingListType_ID  = ' + CONVERT(NVARCHAR, @MailingListType_ID) + ')' + CHAR(10)
	IF @MailingListType_ID = 1
		SET @SQL = @SQL + ' AND C.Address1 <> '''' AND C.Suburb <> '''' AND C.Postcode <> '''' AND C.State_ID IS NOT NULL'
	IF @MailingListType_ID = 3
		SET @SQL = @SQL + ' AND C.Mobile <> '''''
END

SET @SQL = @SQL + '; '

EXEC(@SQL)


IF @UseMailingList = 1 BEGIN
	IF @MailingListType_ID = 2 BEGIN
		UPDATE #CustomReport SET Email = '' FROM Common_Contact C WHERE #CustomReport.ID = C.Contact_ID AND C.DoNotIncludeEmail1InMailingList = 1
		UPDATE #CustomReport SET Email2 = '' FROM Common_Contact C WHERE #CustomReport.ID = C.Contact_ID AND C.DoNotIncludeEmail2InMailingList = 1
		DELETE FROM #CustomReport WHERE Email = '' AND Email2 = ''
	END
END


/* Update with Urban Life Groups */
DECLARE	@Contact_ID	INT,
		@ULGName	NVARCHAR(500)

DECLARE ULGCursor CURSOR FOR
	SELECT ID, Code + ' ' + [Name] FROM #CustomReport, Church_ULG U, Church_ULGContact UC
	WHERE #CustomReport.ID = UC.Contact_ID AND UC.ULG_ID = U.ULG_ID AND UC.Inactive = 0
	ORDER BY ID, Code + ' ' + [Name]
OPEN ULGCursor

FETCH NEXT FROM ULGCursor INTO @Contact_ID, @ULGName
WHILE @@FETCH_STATUS = 0 BEGIN
	UPDATE #CustomReport SET ULG = ULG + @ULGName + '; ' WHERE ID = @Contact_ID
	FETCH NEXT FROM ULGCursor INTO @Contact_ID, @ULGName
END

CLOSE ULGCursor
DEALLOCATE ULGCursor

UPDATE #CustomReport SET ULG = LEFT(ULG, LEN(ULG)-1) WHERE LEN(ULG)>2


/* Update with Roles */
DECLARE	@ContactRole_ID		INT,
		@ContactRole		NVARCHAR(MAX),
		@RestrictedRole		NVARCHAR(MAX),
		@RestrictedULG		NVARCHAR(MAX),
		@Roles				NVARCHAR(MAX)

DECLARE RoleCursor CURSOR FOR
	SELECT ID, ContactRole_ID, R.Name FROM #CustomReport C, Church_ContactRole CR, Church_Role R
	WHERE C.ID = CR.Contact_ID AND CR.Role_ID = R.Role_ID AND R.Deleted =0
OPEN RoleCursor

FETCH NEXT FROM RoleCursor INTO @Contact_ID, @ContactRole_ID, @ContactRole
WHILE @@FETCH_STATUS = 0 BEGIN
		
	DECLARE RestrictedRoleCursor CURSOR FOR
		SELECT R.Name FROM Church_RestrictedAccessRole RA, Church_Role R
		WHERE RA.Role_ID = R.Role_ID AND ContactRole_ID = @ContactRole_ID
	OPEN RestrictedRoleCursor
	
	set @Roles = ''
	FETCH NEXT FROM RestrictedRoleCursor INTO @RestrictedRole
	WHILE @@FETCH_STATUS = 0 BEGIN
		SET @Roles = @Roles + @RestrictedRole + ', '
		FETCH NEXT FROM RestrictedRoleCursor INTO @RestrictedRole
	END
	CLOSE RestrictedRoleCursor
	DEALLOCATE RestrictedRoleCursor
	
	DECLARE RestrictedULGCursor CURSOR FOR
		SELECT U.Code + ' ' + U.Name FROM Church_RestrictedAccessULG RA, Church_ULG U
		WHERE RA.ULG_ID = U.ULG_ID AND RA.ContactRole_ID = @ContactRole_ID
	OPEN RestrictedULGCursor
	
	set @Roles = ''
	FETCH NEXT FROM RestrictedULGCursor INTO @RestrictedULG
	WHILE @@FETCH_STATUS = 0 BEGIN
		SET @Roles = @Roles + @RestrictedULG + ', '
		FETCH NEXT FROM RestrictedULGCursor INTO @RestrictedULG
	END
	CLOSE RestrictedULGCursor
	DEALLOCATE RestrictedULGCursor
	
	IF LEN(@Roles) > 0 SET @Roles = LEFT(@Roles, LEN(@Roles) - 1)

	UPDATE #CustomReport SET [Roles] = [Roles] + @ContactRole + CASE WHEN LEN(@Roles) >0 THEN ' (' + @Roles + '), ' ELSE ', ' END WHERE ID = @Contact_ID
	FETCH NEXT FROM RoleCursor INTO @Contact_ID, @ContactRole_ID, @ContactRole
END

CLOSE RoleCursor
DEALLOCATE RoleCursor

UPDATE #CustomReport SET [Roles] = LEFT([Roles], LEN([Roles])-1) WHERE RIGHT([Roles],2) = ', '


/* Update with Conferenced Registered */
DECLARE	@ConferenceName	NVARCHAR(500)

DECLARE ConferenceCursor CURSOR FOR
	SELECT ID, ConferenceName FROM #CustomReport, Events_Registration R, Events_Venue V, Events_Conference C
	WHERE #CustomReport.ID = R.Contact_ID AND R.Venue_ID = V.Venue_ID AND V.Conference_ID = C.Conference_ID
	AND V.ConferenceEndDate > dbo.GetRelativeDate()
	ORDER BY ID, V.ConferenceStartDate

OPEN ConferenceCursor

FETCH NEXT FROM ConferenceCursor INTO @Contact_ID, @ConferenceName
WHILE @@FETCH_STATUS = 0 BEGIN
	UPDATE #CustomReport SET [Conferences Registered] = [Conferences Registered] + @ConferenceName + '; ' WHERE ID = @Contact_ID
	FETCH NEXT FROM ConferenceCursor INTO @Contact_ID, @ConferenceName
END

CLOSE ConferenceCursor
DEALLOCATE ConferenceCursor

UPDATE #CustomReport SET [Conferences Registered] = LEFT([Conferences Registered], LEN([Conferences Registered])-1) WHERE LEN([Conferences Registered])>2


/* Update with Courses Completed */
DECLARE	@Course_ID	INT
DECLARE	@CourseName NVARCHAR(500)

DECLARE CourseCursor CURSOR FOR
	SELECT C.Course_ID, C.Name FROM Church_Course C WHERE ShowInCustomReport = 1
	ORDER BY SortOrder

OPEN CourseCursor

FETCH NEXT FROM CourseCursor INTO @Course_ID, @CourseName
WHILE @@FETCH_STATUS = 0 BEGIN

	SET @SQL = 'ALTER TABLE #CustomReport ADD [' + @CourseName + '] NVARCHAR(10)'
	EXEC(@SQL)
	
	SET @SQL = 'UPDATE #CustomReport SET [' + @CourseName + '] = CASE WHEN (SELECT COUNT(*) FROM Church_ContactCourseInstance CCI, Church_CourseInstance CI WHERE CCI.CourseInstance_ID = CI.CourseInstance_ID AND CCI.Contact_ID = #CustomReport.ID AND CI.Course_ID = ' + CONVERT(NVARCHAR, @Course_ID) + ') > 0 THEN ''Yes'' ELSE '''' END'
	EXEC(@SQL)
	
	FETCH NEXT FROM CourseCursor INTO @Course_ID, @CourseName
END

CLOSE CourseCursor
DEALLOCATE CourseCursor


IF @ShowRoles = 0 
	ALTER TABLE #CustomReport DROP COLUMN Roles
	
IF (SELECT COUNT(*) FROM #CustomReport WHERE [Pod Group] <> '') = 0
	ALTER TABLE #CustomReport DROP COLUMN [Pod Group]
	
SELECT * FROM #CustomReport
GO
