SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ReportFamily]

@Campus_ID			INT,
@Ministry_ID		INT,
@Region_ID			INT,
@IncludeInactive	BIT

AS

/* Get a list of the familes first. If one family member matches the criteria, then we include them all */

SELECT	DISTINCT Family_ID
INTO	#Families
FROM	Common_Contact C,
		Common_ContactInternal CI
WHERE	C.Contact_ID = CI.Contact_ID
AND		(CI.Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
AND		(CI.Ministry_ID = @Ministry_ID OR @Ministry_ID IS NULL)
AND		(CI.Region_ID = @Region_ID OR @Region_ID IS NULL)
AND		(CI.ChurchStatus_ID <> 7 OR @IncludeInactive = 1) 


SELECT	C.Family_ID as _Family_ID, 
		C.Contact_ID as ID, 
		CONVERT(NVARCHAR(50),'') as [Family Name],
		CONVERT(NVARCHAR(50),'') as [Primary First Name],
		CONVERT(NVARCHAR(50),'') as [Primary Last Name],
		C.FirstName as [Secondary First Name],
		C.LastName as [Secondary Last Name],
		C.FamilyMemberType_ID as _FamilyMemberType_ID,
		ISNULL((SELECT Name FROM Common_FamilyMemberType WHERE FamilyMemberType_ID = C.FamilyMemberType_ID),'') as [Family Member Type],
		ISNULL((SELECT Name FROM Church_ChurchStatus WHERE ChurchStatus_ID = CI.ChurchStatus_ID),'') as [Church Status],
		C.Address1,
		C.Address2,
		C.Suburb,
		ISNULL(CASE WHEN State_ID = 9 THEN C.StateOther ELSE (SELECT State FROM Common_GeneralState WHERE State_ID = C.State_ID) END,'') as [State],
		C.Postcode,
		ISNULL((SELECT Country FROM Common_GeneralCountry WHERE Country_ID = C.Country_ID), '') as [Country],
		C.Email,
		C.Email2,
		C.Mobile,
		C.Phone,
		ISNULL((SELECT [Name] FROM Church_Campus WHERE Campus_ID = CI.Campus_ID),'') as [Campus],
		ISNULL((SELECT [Name] FROM Church_Ministry WHERE Ministry_ID = CI.Ministry_ID),'') as [Ministry],
		ISNULL((SELECT [Name] FROM Church_Region WHERE Region_ID = CI.Region_ID),'') as [Region],
		CONVERT(NVARCHAR(500),'') as [ULG],
		Gender,
		DateOfBirth as [Date Of Birth],
		CONVERT(INT,0) as _FamilyCount
INTO	#Family
FROM	Common_Contact C,
		Common_ContactInternal CI,
		#Families Fa
WHERE	C.Contact_ID = CI.Contact_ID
AND		C.Family_ID = Fa.Family_ID
AND		(CI.ChurchStatus_ID <> 7 OR @IncludeInactive = 1) 
AND		C.Family_ID IS NOT NULL

DELETE FROM	#Family
WHERE	_Family_ID IN (SELECT _Family_ID FROM #Family Group BY _Family_ID HAVING COUNT(*) <= 1)


SELECT	DISTINCT _Family_ID,
		(SELECT		TOP 1 Co.Contact_ID 
		 FROM		Common_Contact Co, Common_ContactInternal CI, #Family Fa
		 WHERE		Co.Contact_ID = CI.Contact_ID AND Co.Contact_ID = Fa.ID AND Co.Family_ID = F._Family_ID
 		 ORDER BY	ISNULL(FamilyMemberType_ID, 1000)) as Contact_ID
INTO	#Primary
FROM	#Family F


UPDATE	#Family
SET		[Primary First Name] = [Secondary First Name],
		[Primary Last Name] = [Secondary Last Name],
		[Secondary First Name] = '',
		[Secondary Last Name] = ''
FROM	#Primary P
WHERE	P.Contact_ID = #Family.ID

UPDATE	#Family
SET		[Family Name] = (SELECT [Primary Last Name] FROM #Family WHERE ID = P.Contact_ID)
FROM	#Primary P
WHERE	P._Family_ID = #Family._Family_ID

/* Update with Urban Life Groups */
DECLARE	@Contact_ID	INT,
		@ULGName	NVARCHAR(500)

DECLARE ULGCursor CURSOR FOR
	SELECT DISTINCT F.ID, Code + ' ' + [Name] FROM #Family F, Church_ULG U, Church_ULGContact UC
	WHERE F.ID = UC.Contact_ID AND UC.ULG_ID = U.ULG_ID AND UC.Inactive = 0
	ORDER BY F.ID, Code + ' ' + [Name]
OPEN ULGCursor

FETCH NEXT FROM ULGCursor INTO @Contact_ID, @ULGName
WHILE @@FETCH_STATUS = 0 BEGIN
	UPDATE #Family SET ULG = ULG + @ULGName + '; ' WHERE ID = @Contact_ID
	FETCH NEXT FROM ULGCursor INTO @Contact_ID, @ULGName
END

CLOSE ULGCursor
DEALLOCATE ULGCursor

UPDATE #Family SET ULG = LEFT(ULG, LEN(ULG)-1) WHERE LEN(ULG)>2

UPDATE #Family SET _FamilyCount = (SELECT COUNT(DISTINCT _Family_ID) FROM #Family)

SELECT		*
FROM		#Family
ORDER BY	 [Family Name], _Family_ID, [Primary Last Name] DESC, ISNULL(_FamilyMemberType_ID,1000), [Date Of Birth] ASC
GO
