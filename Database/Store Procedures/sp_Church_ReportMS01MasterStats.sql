SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ReportMS01MasterStats]

@Month		INT,
@Year		INT,
@Campus_ID	INT = 1

AS

CREATE TABLE #Results (
	ChurchStatus_ID		INT,
	Ministry_ID			INT,
	[Status]			NVARCHAR(50),
	[Ministry]			NVARCHAR(50),
	Number				INT
)

CREATE TABLE #Report (
	[Status]			NVARCHAR(50),
	[Ministry]			NVARCHAR(50),
	Number				INT,
	SortOrder			DECIMAL (18,2)
)

SELECT		CONVERT(INT, Ministry_ID) AS Ministry_ID,
			[Name]
INTO		#Ministry
FROM		Church_Ministry

INSERT		#Ministry
SELECT		999, 'Total'


INSERT		#Results
SELECT		S.ChurchStatus_ID,
			M.Ministry_ID,
			S.[Name],
			M.[Name],
			ISNULL((SELECT SUM(Number) FROM vw_Church_HistoryPartnership H
					WHERE (H.Ministry_ID = M.Ministry_ID OR M.Ministry_ID = 999)
					AND H.ChurchStatus_ID = S.ChurchStatus_ID
					AND MONTH([Date]) = @Month
					AND	YEAR([Date]) = @Year
					AND	(Campus_ID = @Campus_ID OR @Campus_ID IS NULL)), 0) as Number
FROM		#Ministry M,
			Church_ChurchStatus S	
WHERE		S.Deleted = 0		

INSERT #Report
SELECT Status, Ministry, Number, ChurchStatus_ID FROM #Results WHERE ChurchStatus_ID IN (1,2,3) ORDER BY ChurchStatus_ID

INSERT #Report
SELECT 'Total Partners', Ministry, SUM(Number),3.5 FROM #Results WHERE ChurchStatus_ID IN (1,2,3) GROUP BY Ministry

INSERT #Report
SELECT Status, Ministry, Number, ChurchStatus_ID FROM #Results WHERE ChurchStatus_ID IN (4,5) ORDER BY ChurchStatus_ID

INSERT #Report
SELECT 'Church Total', Ministry, SUM(Number), 5.5 FROM #Results WHERE ChurchStatus_ID IN (1,2,3,4,5) GROUP BY Ministry

INSERT #Report
SELECT Status, Ministry, Number, ChurchStatus_ID FROM #Results WHERE ChurchStatus_ID IN (6) ORDER BY ChurchStatus_ID


SELECT DISTINCT SortOrder, Status INTO #Report1 FROM #Report ORDER BY SortOrder




/* Create a Formatted results table */

DECLARE	@Ministry	NVARCHAR(100)
DECLARE @SQL		NVARCHAR(500)

DECLARE MinistryCursor CURSOR FOR
	SELECT		[Name]
	FROM		#Ministry
	ORDER BY	Ministry_ID DESC

OPEN MinistryCursor

FETCH NEXT FROM MinistryCursor INTO @Ministry
WHILE (@@FETCH_STATUS = 0) BEGIN

	SET @SQL = 'ALTER TABLE #Report1 ADD [' + @Ministry + '] int'
	EXEC(@SQL)

	SET @SQL = 'UPDATE #Report1 
				SET [' + @Ministry + '] = [Number]
				FROM	#Report R
				WHERE	R.[Status] = #Report1.[Status] 
				AND		R.[Ministry] = ''' + @Ministry  + ''''
	EXEC(@SQL)

	FETCH NEXT FROM MinistryCursor INTO @Ministry
END

CLOSE MinistryCursor
DEALLOCATE MinistryCursor


ALTER TABLE #Report1 DROP COLUMN SortOrder


SELECT	*
FROM	#Report1
GO
