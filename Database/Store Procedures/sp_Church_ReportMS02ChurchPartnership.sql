SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ReportMS02ChurchPartnership]

@Month1		INT,
@Year1		INT,
@Month2		INT,
@Year2		INT,
@Campus_ID	INT = 1

AS

SELECT	[Name] as [Name],
		ISNULL((	SELECT	SUM(Number)
					FROM	vw_Church_HistoryPartnership
					WHERE	MONTH([Date]) = @Month1
					AND		YEAR([Date]) = @Year1
					AND		(Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
					AND		ChurchStatus_ID = S.ChurchStatus_ID),0) as Num1,
		ISNULL((	SELECT	SUM(Number)
					FROM	vw_Church_HistoryPartnership
					WHERE	MONTH([Date]) = @Month2
					AND		YEAR([Date]) = @Year2
					AND		(Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
					AND		ChurchStatus_ID = S.ChurchStatus_ID),0) as Num2
INTO	#Report
FROM	Church_ChurchStatus S
WHERE	ChurchStatus_ID IN (1,2,3)
AND		S.Deleted = 0

UNION ALL

SELECT	'Total Partners',
		ISNULL((	SELECT	SUM(Number)
					FROM	vw_Church_HistoryPartnership
					WHERE	MONTH([Date]) = @Month1
					AND		YEAR([Date]) = @Year1
					AND		(Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
					AND		ChurchStatus_ID IN (1,2,3)),0),
		ISNULL((	SELECT	SUM(Number)
					FROM	vw_Church_HistoryPartnership
					WHERE	MONTH([Date]) = @Month2
					AND		YEAR([Date]) = @Year2
					AND		(Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
					AND		ChurchStatus_ID IN (1,2,3)),0)

UNION ALL

SELECT	[Name],
		ISNULL((	SELECT	SUM(Number)
					FROM	vw_Church_HistoryPartnership
					WHERE	MONTH([Date]) = @Month1
					AND		YEAR([Date]) = @Year1
					AND		(Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
					AND		ChurchStatus_ID = S.ChurchStatus_ID),0),
		ISNULL((	SELECT	SUM(Number)
					FROM	vw_Church_HistoryPartnership
					WHERE	MONTH([Date]) = @Month2
					AND		YEAR([Date]) = @Year2
					AND		(Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
					AND		ChurchStatus_ID = S.ChurchStatus_ID),0)
FROM	Church_ChurchStatus S
WHERE	ChurchStatus_ID IN (4,5)

UNION ALL

SELECT	'Church Total',
		ISNULL((	SELECT	SUM(Number)
					FROM	vw_Church_HistoryPartnership
					WHERE	MONTH([Date]) = @Month1
					AND		YEAR([Date]) = @Year1
					AND		(Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
					AND		ChurchStatus_ID IN (1,2,3,4,5)),0),
		ISNULL((	SELECT	SUM(Number)
					FROM	vw_Church_HistoryPartnership
					WHERE	MONTH([Date]) = @Month2
					AND		YEAR([Date]) = @Year2
					AND		(Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
					AND		ChurchStatus_ID IN (1,2,3,4,5)),0)

UNION ALL

SELECT	[Name],
		ISNULL((	SELECT	SUM(Number)
					FROM	vw_Church_HistoryPartnership
					WHERE	MONTH([Date]) = @Month1
					AND		YEAR([Date]) = @Year1
					AND		(Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
					AND		ChurchStatus_ID = S.ChurchStatus_ID),0),
		ISNULL((	SELECT	SUM(Number)
					FROM	vw_Church_HistoryPartnership
					WHERE	MONTH([Date]) = @Month2
					AND		YEAR([Date]) = @Year2
					AND		(Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
					AND		ChurchStatus_ID = S.ChurchStatus_ID),0)
FROM	Church_ChurchStatus S
WHERE	ChurchStatus_ID IN (6)

UNION ALL

SELECT	'Total',
		ISNULL((	SELECT	SUM(Number)
					FROM	vw_Church_HistoryPartnership
					WHERE	MONTH([Date]) = @Month1
					AND		YEAR([Date]) = @Year1
					AND		(Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
					AND		ChurchStatus_ID IN (1,2,3,4,5,6)),0),
		ISNULL((	SELECT	SUM(Number)
					FROM	vw_Church_HistoryPartnership
					WHERE	MONTH([Date]) = @Month2
					AND		YEAR([Date]) = @Year2
					AND		(Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
					AND		ChurchStatus_ID IN (1,2,3,4,5,6)),0)


ALTER TABLE #Report ADD Diff int

UPDATE #Report SET Diff = Num2 - Num1

SELECT * FROM #Report
GO
