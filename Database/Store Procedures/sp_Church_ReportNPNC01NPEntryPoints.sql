SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ReportNPNC01NPEntryPoints]

@StartMonth			INT,
@StartYear			INT,
@EndMonth			INT,
@EndYear			INT,
@NPNCType_ID		INT,
@Campus_ID			INT,
@Ministry_ID		INT

AS

SELECT		ISNULL([Name],'No Entry Point') as Name,
			COUNT(*) as [Number],
			ISNULL(E.SortOrder, 999) as SortOrder
FROM		Church_NPNC N,
			Common_ContactInternal C
				LEFT JOIN Church_EntryPoint E ON C.EntryPoint_ID = E.EntryPoint_ID
WHERE		C.Contact_ID = N.Contact_ID
AND			(MONTH([FirstContactDate]) >= @StartMonth OR YEAR([FirstContactDate]) > @StartYear)
AND			YEAR([FirstContactDate]) >= @StartYear
AND			(MONTH([FirstContactDate]) <= @EndMonth OR YEAR([FirstContactDate]) < @EndYear)
AND			YEAR([FirstContactDate]) <= @EndYear
AND			N.NPNCType_ID = @NPNCType_ID
AND			(N.CampusDecision_ID = @Campus_ID OR @Campus_ID IS NULL)
AND			(N.Ministry_ID = @Ministry_ID OR @Ministry_ID IS NULL)
GROUP BY	E.[Name], E.SortOrder
ORDER BY	ISNULL(E.SortOrder, 999)
GO
