SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ReportNPNC02MinistryAllocation]

@StartMonth		INT,
@StartYear		INT,
@EndMonth		INT,
@EndYear		INT,
@NPNCType_ID	INT,
@Campus_ID		INT

AS

SELECT		[Name],
			COUNT(*) as [Number]
FROM		Church_Ministry M,
			Church_NPNC N,
			Common_ContactInternal C
WHERE		M.Ministry_ID = C.Ministry_ID
AND			C.Contact_ID = N.Contact_ID
AND			(MONTH([FirstContactDate]) >= @StartMonth OR YEAR([FirstContactDate]) > @StartYear)
AND			YEAR([FirstContactDate]) >= @StartYear
AND			(MONTH([FirstContactDate]) <= @EndMonth OR YEAR([FirstContactDate]) < @EndYear)
AND			YEAR([FirstContactDate]) <= @EndYear
AND			(N.CampusDecision_ID = @Campus_ID OR @Campus_ID IS NULL)
AND			N.NPNCType_ID = @NPNCType_ID
GROUP BY	[Name]
GO
