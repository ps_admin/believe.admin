SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ReportNPNC03InitialStatus]

@StartMonth		INT,
@StartYear		INT,
@EndMonth		INT,
@EndYear		INT,
@Campus_ID		INT

AS

DECLARE	@NPNCType_ID INT 
SET		@NPNCType_ID = 1 --New People

CREATE TABLE #Dates (
	[Date] NVARCHAR(20)
)

DECLARE @Date DATETIME
SET @Date = CONVERT(DATETIME, CONVERT(NVARCHAR,@StartYear) + '-' +CONVERT(NVARCHAR, @StartMonth) + '-1')
WHILE @Date <= CONVERT(DATETIME, CONVERT(NVARCHAR,@EndYear) + '-' +CONVERT(NVARCHAR, @EndMonth) + '-1') BEGIN
	INSERT #Dates (Date) VALUES (LEFT(DATENAME(m, @Date),3) + ' ' + CONVERT(NVARCHAR,YEAR(@Date)))
	SET @Date = DATEADD(mm, 1, @Date)
END


SELECT		N.InitialStatus_ID,
			LEFT(DATENAME(m, [Date]),3) + ' ' + CONVERT(NVARCHAR,YEAR([Date])) as [Date],
			SUM(Number) as Number
INTO		#t1
FROM		Church_NPNCInitialStatus S,
			(
			SELECT		InitialStatus_ID,
						Campus_ID,
						[Date],
						[Number]
			FROM		Church_NPSummaryStats
			UNION ALL
			SELECT		InitialStatus_ID,
						CampusDecision_ID,
						FirstContactDate,
						COUNT(*)
			FROM		Church_NPNC
			WHERE		NPNCType_ID = @NPNCType_ID
			GROUP BY	InitialStatus_ID,
						CampusDecision_ID,
						[FirstContactDate]) N
WHERE		N.InitialStatus_ID = S.InitialStatus_ID
AND			(MONTH([Date]) >= @StartMonth OR YEAR([Date]) > @StartYear)
AND			YEAR([Date]) >= @StartYear
AND			(MONTH([Date]) <= @EndMonth OR YEAR([Date]) < @EndYear)
AND			YEAR([Date]) <= @EndYear
AND			(N.Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
GROUP BY	LEFT(DATENAME(m, [Date]),3) + ' ' + CONVERT(NVARCHAR,YEAR([Date])),
			N.InitialStatus_ID

SELECT		D.[Date],
			S.[Name],
			ISNULL((SELECT [Number] FROM #t1 WHERE [Date] = D.[Date] AND InitialStatus_ID = S.InitialStatus_ID),0) as Number
INTO		#Report1
FROM		#Dates D,
			Church_NPNCInitialStatus S
WHERE		S.NPNCType_ID = @NPNCType_ID
ORDER BY	CONVERT(DATETIME, D.[Date])



/* Create a second Formatted table for results display */

DECLARE	@DateString	NVARCHAR(100)
DECLARE @SQL		NVARCHAR(500)


SELECT		[Name]
INTO		#Report
FROM		Church_NPNCInitialStatus
WHERE		NPNCType_ID = @NPNCType_ID


DECLARE DatesCursor CURSOR FOR
	SELECT	DISTINCT CONVERT(DATETIME,[Date])
	FROM	#Report1
	ORDER BY CONVERT(DATETIME,[Date]) ASC

OPEN DatesCursor

FETCH NEXT FROM DatesCursor INTO @Date
WHILE (@@FETCH_STATUS = 0) BEGIN

	SET @DateString = LEFT(DATENAME(m, @Date),3) + ' ' + CONVERT(NVARCHAR,YEAR(@Date)) 

	SET @SQL = 'ALTER TABLE #Report ADD [' + @DateString + '] int'
	EXEC(@SQL)

	SET @SQL = 'UPDATE #Report 
				SET [' + @DateString + '] = [Number]
				FROM	#Report1 R
				WHERE	R.[Name] = #Report.[Name] 
				AND		R.[Date] = ''' + @DateString  + ''''
	EXEC(@SQL)

	FETCH NEXT FROM DatesCursor INTO @Date
END

CLOSE DatesCursor
DEALLOCATE DatesCursor

SELECT	*
FROM	#Report

SELECT	*
FROM	#Report1
GO
