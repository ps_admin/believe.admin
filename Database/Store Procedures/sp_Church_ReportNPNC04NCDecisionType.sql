SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ReportNPNC04NCDecisionType]

@StartMonth		INT,
@StartYear		INT,
@EndMonth		INT,
@EndYear		INT,
@Campus_ID		INT

AS

CREATE TABLE #Dates (
	[Date] NVARCHAR(20)
)

DECLARE @Date DATETIME
SET @Date = CONVERT(DATETIME, CONVERT(NVARCHAR,@StartYear) + '-' +CONVERT(NVARCHAR, @StartMonth) + '-1')
WHILE @Date <= CONVERT(DATETIME, CONVERT(NVARCHAR,@EndYear) + '-' +CONVERT(NVARCHAR, @EndMonth) + '-1') BEGIN
	INSERT #Dates (Date) VALUES (LEFT(DATENAME(m, @Date),3) + ' ' + CONVERT(NVARCHAR,YEAR(@Date)))
	SET @Date = DATEADD(mm, 1, @Date)
END


SELECT		D.[Date],
			T.[Name],
			ISNULL((SELECT COUNT(*) FROM Church_NPNC N WHERE MONTH([FirstContactDate]) = MONTH(D.[Date]) AND YEAR([FirstContactDate]) = YEAR(D.[Date]) AND NCDecisionType_ID = T.NCDecisionType_ID AND (CampusDecision_ID = @Campus_ID OR @Campus_ID IS NULL)),0) as Number
INTO		#Report1
FROM		#Dates D,
			Church_NCDecisionType T
ORDER BY	CONVERT(DATETIME, D.[Date])



/* Create a second Formatted table for results display */

DECLARE	@DateString	NVARCHAR(100)
DECLARE @SQL		NVARCHAR(500)


SELECT		[Name]
INTO		#Report
FROM		Church_NCDecisionType


DECLARE DatesCursor CURSOR FOR
	SELECT	DISTINCT CONVERT(DATETIME,[Date])
	FROM	#Report1
	ORDER BY CONVERT(DATETIME,[Date]) ASC

OPEN DatesCursor

FETCH NEXT FROM DatesCursor INTO @Date
WHILE (@@FETCH_STATUS = 0) BEGIN

	SET @DateString = LEFT(DATENAME(m, @Date),3) + ' ' + CONVERT(NVARCHAR,YEAR(@Date)) 

	SET @SQL = 'ALTER TABLE #Report ADD [' + @DateString + '] int'
	EXEC(@SQL)

	SET @SQL = 'UPDATE #Report 
				SET [' + @DateString + '] = [Number]
				FROM	#Report1 R
				WHERE	R.[Name] = #Report.[Name] 
				AND		R.[Date] = ''' + @DateString  + ''''
	EXEC(@SQL)

	FETCH NEXT FROM DatesCursor INTO @Date
END

CLOSE DatesCursor
DEALLOCATE DatesCursor

SELECT	*
FROM	#Report

SELECT	*
FROM	#Report1
GO
