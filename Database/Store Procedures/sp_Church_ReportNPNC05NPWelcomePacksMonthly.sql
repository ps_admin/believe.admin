SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ReportNPNC05NPWelcomePacksMonthly]

@StartMonth		INT,
@StartYear		INT,
@EndMonth		INT,
@EndYear		INT,
@Campus_ID		INT

AS

CREATE TABLE #Dates (
	[Date] NVARCHAR(20)
)

DECLARE @Date DATETIME
SET @Date = CONVERT(DATETIME, CONVERT(NVARCHAR,@StartYear) + '-' +CONVERT(NVARCHAR, @StartMonth) + '-1')
WHILE @Date <= CONVERT(DATETIME, CONVERT(NVARCHAR,@EndYear) + '-' +CONVERT(NVARCHAR, @EndMonth) + '-1') BEGIN
	INSERT #Dates (Date) VALUES (LEFT(DATENAME(m, @Date),3) + ' ' + CONVERT(NVARCHAR,YEAR(@Date)))
	SET @Date = DATEADD(mm, 1, @Date)
END

CREATE TABLE #Report ([Name] NVARCHAR(50))
INSERT #Report ([Name]) VALUES ('Number Issued')
INSERT #Report ([Name]) VALUES ('Number Returned')
INSERT #Report ([Name]) VALUES ('Percent')


SELECT		D.[Date],
			R.[Name],
			CONVERT(DECIMAL(18,0),
				ISNULL((SELECT SUM(NumberIssued) FROM Church_NPNCWelcomePacksIssued WHERE MONTH([DateIssued]) = MONTH(D.[Date]) AND YEAR([DateIssued]) = YEAR(D.[Date]) AND	(Campus_ID = @Campus_ID OR @Campus_ID IS NULL) AND R.[Name] = 'Number Issued'),0) +
				ISNULL((SELECT SUM(NumberReturned) FROM Church_NPNCWelcomePacksIssued WHERE MONTH([DateIssued]) = MONTH(D.[Date]) AND YEAR([DateIssued]) = YEAR(D.[Date]) AND (Campus_ID = @Campus_ID OR @Campus_ID IS NULL) AND R.[Name] = 'Number Returned'),0) +
				ISNULL((SELECT SUM(NumberReturned) / CONVERT(DECIMAL(18,0), SUM(NumberIssued)) * 100 FROM Church_NPNCWelcomePacksIssued WHERE MONTH([DateIssued]) = MONTH(D.[Date]) AND YEAR([DateIssued]) = YEAR(D.[Date]) AND	(Campus_ID = @Campus_ID OR @Campus_ID IS NULL) AND R.[Name] = 'Percent'),0)
			) AS Number	
INTO		#Report1
FROM		#Dates D,
			#Report R
ORDER BY	CONVERT(DATETIME, D.[Date])


/* Create a second Formatted table for results display */

DECLARE	@DateString	NVARCHAR(100)
DECLARE @SQL		NVARCHAR(500)


DECLARE DatesCursor CURSOR FOR
	SELECT	DISTINCT CONVERT(DATETIME,[Date])
	FROM	#Report1
	ORDER BY CONVERT(DATETIME,[Date]) ASC

OPEN DatesCursor

FETCH NEXT FROM DatesCursor INTO @Date
WHILE (@@FETCH_STATUS = 0) BEGIN

	SET @DateString = LEFT(DATENAME(m, @Date),3) + ' ' + CONVERT(NVARCHAR,YEAR(@Date)) 

	SET @SQL = 'ALTER TABLE #Report ADD [' + @DateString + '] DECIMAL(18,0)'
	EXEC(@SQL)

	SET @SQL = 'UPDATE #Report 
				SET [' + @DateString + '] = [Number]
				FROM	#Report1 R
				WHERE	R.[Name] = #Report.[Name] 
				AND		R.[Date] = ''' + @DateString  + ''''
	EXEC(@SQL)

	FETCH NEXT FROM DatesCursor INTO @Date
END

CLOSE DatesCursor
DEALLOCATE DatesCursor

SELECT	*
FROM	#Report

SELECT	*
FROM	#Report1
WHERE	[Name] <> 'Percent'
GO
