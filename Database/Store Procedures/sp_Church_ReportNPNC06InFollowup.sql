SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ReportNPNC06InFollowup]

@StartYear		INT,
@EndYear		INT,
@NPNCType_ID	INT,
@Campus_ID		INT

AS

CREATE TABLE #Dates (
	[Date] NVARCHAR(20)
)

DECLARE @Date DATETIME
SET @Date = CONVERT(DATETIME, CONVERT(NVARCHAR,@StartYear) + '-1-1')
WHILE @Date <= CONVERT(DATETIME, CONVERT(NVARCHAR,@EndYear) + '-1-1') BEGIN
	INSERT #Dates (Date) VALUES (CONVERT(NVARCHAR,YEAR(@Date)))
	SET @Date = DATEADD(yy, 1, @Date)
END


SELECT	CONVERT(INT, Ministry_ID) AS ID,
		[Name],
		[SortOrder]
INTO	#Report
FROM	Church_Ministry

INSERT #Report SELECT 0, 'Total', 999


SELECT		R.[Name],
			D.[Date],
			ISNULL((SELECT COUNT(*) FROM Church_NPNC NPNC WHERE (Ministry_ID = R.ID OR R.ID = 0) AND YEAR([FirstContactDate]) = YEAR(D.[Date]) AND NPNCType_ID = @NPNCType_ID AND (CampusDecision_ID = @Campus_ID OR @Campus_ID IS NULL)),0) as [Number]
INTO		#Report1
FROM		#Dates D,
			#Report R
ORDER BY	CONVERT(DATETIME, D.[Date])



/* Create a second Formatted table for results display */

DECLARE	@DateString	NVARCHAR(100)
DECLARE @SQL		NVARCHAR(500)



DECLARE DatesCursor CURSOR FOR
	SELECT	DISTINCT CONVERT(DATETIME,[Date])
	FROM	#Report1
	ORDER BY CONVERT(DATETIME,[Date]) ASC

OPEN DatesCursor

FETCH NEXT FROM DatesCursor INTO @Date
WHILE (@@FETCH_STATUS = 0) BEGIN

	SET @DateString = CONVERT(NVARCHAR,YEAR(@Date)) 

	SET @SQL = 'ALTER TABLE #Report ADD [' + @DateString + '] int'
	EXEC(@SQL)

	SET @SQL = 'UPDATE #Report 
				SET [' + @DateString + '] = [Number]
				FROM	#Report1 R
				WHERE	R.[Name] = #Report.[Name] 
				AND		R.[Date] = ''' + @DateString  + ''''
	EXEC(@SQL)

	FETCH NEXT FROM DatesCursor INTO @Date
END

CLOSE DatesCursor
DEALLOCATE DatesCursor

ALTER TABLE #Report DROP COLUMN ID

SELECT		*
FROM		#Report
ORDER BY	[SortOrder]
GO
