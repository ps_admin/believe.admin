SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ReportNPNC07NPWelcomePacksYearly]

@StartYear		INT,
@EndYear		INT,
@Campus_ID		INT

AS

CREATE TABLE #Dates (
	[Date] NVARCHAR(20)
)

DECLARE @Date DATETIME
SET @Date = CONVERT(DATETIME, CONVERT(NVARCHAR,@StartYear) + '-1-1')
WHILE @Date <= CONVERT(DATETIME, CONVERT(NVARCHAR,@EndYear) + '-1-1') BEGIN
	INSERT #Dates (Date) VALUES (CONVERT(NVARCHAR,YEAR(@Date)))
	SET @Date = DATEADD(yy, 1, @Date)
END

CREATE TABLE #Report ([Name] NVARCHAR(50))
INSERT #Report ([Name]) VALUES ('Number Issued')
INSERT #Report ([Name]) VALUES ('Number Returned')
INSERT #Report ([Name]) VALUES ('% Returned')
INSERT #Report ([Name]) VALUES ('# of NP/NC outcomed to Parter status')


SELECT		D.[Date],
			R.[Name],
			CONVERT(INT, ISNULL((SELECT SUM(NumberIssued) FROM Church_NPNCWelcomePacksIssued WHERE YEAR([DateIssued]) = YEAR(D.[Date]) AND (Campus_ID = @Campus_ID OR @Campus_ID IS NULL) AND R.[Name] = 'Number Issued'),0) +
						 ISNULL((SELECT SUM(NumberReturned) FROM Church_NPNCWelcomePacksIssued WHERE YEAR([DateIssued]) = YEAR(D.[Date]) AND (Campus_ID = @Campus_ID OR @Campus_ID IS NULL) AND R.[Name] = 'Number Returned'),0) +
						 ISNULL((SELECT CASE WHEN SUM(NumberIssued) > 0 THEN ((SUM(NumberReturned) / CONVERT(DECIMAL(18,2),SUM(NumberIssued))) * 100) ELSE 0 END FROM Church_NPNCWelcomePacksIssued WHERE YEAR([DateIssued]) = YEAR(D.[Date]) AND (Campus_ID = @Campus_ID OR @Campus_ID IS NULL) AND R.[Name] = '% Returned'),0) +
						 ISNULL((SELECT COUNT(*) FROM Church_NPNC NPNC WHERE YEAR([FirstContactDate]) = YEAR(D.[Date]) AND (CampusDecision_ID = @Campus_ID OR @Campus_ID IS NULL) AND R.[Name] = '# of NP/NC outcomed to Parter status' AND NPNC.OutcomeStatus_ID IN (1,2,3)),0)) as Number
INTO		#Report1
FROM		#Dates D,
			#Report R
ORDER BY	CONVERT(DATETIME, D.[Date])


/* Create a second Formatted table for results display */

DECLARE	@DateString	NVARCHAR(100)
DECLARE @SQL		NVARCHAR(500)


DECLARE DatesCursor CURSOR FOR
	SELECT	DISTINCT CONVERT(DATETIME,[Date])
	FROM	#Report1
	ORDER BY CONVERT(DATETIME,[Date]) ASC

OPEN DatesCursor

FETCH NEXT FROM DatesCursor INTO @Date
WHILE (@@FETCH_STATUS = 0) BEGIN

	SET @DateString = CONVERT(NVARCHAR,YEAR(@Date))

	SET @SQL = 'ALTER TABLE #Report ADD [' + @DateString + '] int'
	EXEC(@SQL)

	SET @SQL = 'UPDATE #Report 
				SET [' + @DateString + '] = [Number]
				FROM	#Report1 R
				WHERE	R.[Name] = #Report.[Name] 
				AND		R.[Date] = ''' + @DateString  + ''''
	EXEC(@SQL)

	FETCH NEXT FROM DatesCursor INTO @Date
END

CLOSE DatesCursor
DEALLOCATE DatesCursor

SELECT	*
FROM	#Report
GO
