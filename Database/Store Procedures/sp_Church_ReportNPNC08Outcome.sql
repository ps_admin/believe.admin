SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ReportNPNC08Outcome]

@StartDate			DATETIME,
@EndDate			DATETIME,
@NPNCType_ID		INT,
@Campus_ID			INT,
@Ministry_ID		INT,
@Region_ID			INT,
@DateType			INT

AS



CREATE TABLE #Status (
	[ID]		INT,
	[Name]		NVARCHAR(50)
)

INSERT	#Status 
SELECT	ChurchStatus_ID, [Name] FROM Church_ChurchStatus 
WHERE	Deleted = 0
AND		((ChurchStatus_ID <> 4 AND @NPNCType_ID = 1) OR
		 (ChurchStatus_ID <> 5 AND @NPNCType_ID = 2))

SELECT		CASE WHEN @Ministry_ID IS NULL 
				THEN 'Total' 
				ELSE (SELECT [Name] FROM Church_Ministry WHERE Ministry_ID = @Ministry_ID) END 
			+ ISNULL((SELECT ' - ' + [Name] FROM Church_Region WHERE Region_ID = @Region_ID),'')
			as Ministry,
			S.[Name],
			ISNULL((	SELECT	COUNT(*) 
						FROM	Church_NPNC N,
								Common_ContactInternal CI
						WHERE	CI.Contact_ID = N.Contact_ID
						AND		N.OutcomeStatus_ID = S.ID
						AND		((@DateType = 1 AND	[OutcomeDate] >= @StartDate AND [OutcomeDate] <= DATEADD(dd,1,@EndDate))
								  OR 
								 (@DateType = 2 AND	[FirstContactDate] >= @StartDate AND [FirstContactDate] <= DATEADD(dd,1,@EndDate)))
						AND		N.NPNCType_ID = @NPNCType_ID
						AND		(N.CampusDecision_ID = @Campus_ID OR @Campus_ID IS NULL)
						AND		(N.Ministry_ID = @Ministry_ID OR @Ministry_ID IS NULL)
						AND		(N.Region_ID = @Region_ID OR @Region_ID IS NULL)), 0) as Number
INTO		#Report1
FROM		#Status S
			

/* Create a second Formatted table for results display */

DECLARE	@Name	NVARCHAR(100)
DECLARE @SQL	NVARCHAR(500)

SELECT	DISTINCT Ministry
INTO	#Report
FROM	#Report1

DECLARE StatusCursor CURSOR FOR
	SELECT	[Name]
	FROM	#Report1

OPEN StatusCursor

FETCH NEXT FROM StatusCursor INTO @Name
WHILE (@@FETCH_STATUS = 0) BEGIN

	SET @SQL = 'ALTER TABLE #Report ADD [' + @Name + '] int'
	EXEC(@SQL)

	SET @SQL = 'UPDATE #Report 
				SET [' + @Name + '] = [Number]
				FROM	#Report1 R
				WHERE	R.[Name] = ''' + @Name  + ''''
	EXEC(@SQL)

	FETCH NEXT FROM StatusCursor INTO @Name
END

CLOSE StatusCursor
DEALLOCATE StatusCursor

SELECT	[Name], Number
FROM	#Report1

SELECT	*
FROM	#Report
GO
