SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ReportNPNC09AgeDemographic]

@StartMonth		INT,
@StartYear		INT,
@EndMonth		INT,
@EndYear		INT,
@NPNCType_ID	INT,
@Campus_ID		INT,
@Ministry_ID	INT

AS

CREATE TABLE #Ages (
	AgeMin		INT,
	AgeMax		INT,
	[Age]		NVARCHAR(50)
)

--INSERT #Ages SELECT DATEADD(YY, -13, dbo.GetRelativeDate()), dbo.GetRelativeDate(), '0 - 12'

INSERT #Ages SELECT 0, 12, '0 - 12'
INSERT #Ages SELECT 13, 18, '13 - 18'
INSERT #Ages SELECT 19, 25, '19 - 25'
INSERT #Ages SELECT 26, 35, '26 - 35'
INSERT #Ages SELECT 36, 49, '36 - 49'
INSERT #Ages SELECT 50, 150, '50 +'

SELECT		CASE WHEN @Ministry_ID IS NULL THEN 'Total' ELSE (SELECT [Name] FROM Church_Ministry WHERE Ministry_ID = @Ministry_ID) END as Ministry,
			A.[Age],
			COUNT(*) as [Number]
INTO		#Report1
FROM		#Ages A,
			Church_NPNC N,
			Common_Contact C
WHERE		N.Contact_ID = C.Contact_ID
AND			(MONTH([FirstContactDate]) >= @StartMonth OR YEAR([FirstContactDate]) > @StartYear)
AND			YEAR([FirstContactDate]) >= @StartYear
AND			(MONTH([FirstContactDate]) <= @EndMonth OR YEAR([FirstContactDate]) < @EndYear)
AND			YEAR([FirstContactDate]) <= @EndYear
AND			(N.CampusDecision_ID = @Campus_ID OR @Campus_ID IS NULL)
AND			dbo.Common_GetAgeFromDate(C.DateOfBirth, N.FirstContactDate) BETWEEN A.AgeMin AND A.AgeMax
AND			N.NPNCType_ID = @NPNCType_ID
AND			(N.Ministry_ID = @Ministry_ID OR @Ministry_ID IS NULL)
GROUP BY	A.[Age]


/* Create a second Formatted table for results display */

DECLARE	@Age	NVARCHAR(100)
DECLARE @SQL	NVARCHAR(500)

SELECT	DISTINCT Ministry
INTO	#Report
FROM	#Report1

DECLARE AgeCursor CURSOR FOR
	SELECT		[Age]
	FROM		#Report1
	ORDER BY	[Age]

OPEN AgeCursor

FETCH NEXT FROM AgeCursor INTO @Age
WHILE (@@FETCH_STATUS = 0) BEGIN

	SET @SQL = 'ALTER TABLE #Report ADD [' + @Age + '] int'
	EXEC(@SQL)

	SET @SQL = 'UPDATE #Report 
				SET [' + @Age + '] = [Number]
				FROM	#Report1 R
				WHERE	R.[Age] = ''' + @Age  + ''''
	EXEC(@SQL)

	FETCH NEXT FROM AgeCursor INTO @Age
END

CLOSE AgeCursor
DEALLOCATE AgeCursor

SELECT	Age, Number
FROM	#Report1

SELECT	*
FROM	#Report
GO
