SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ReportNPNC10FirstContact]


@StartMonth		INT,
@StartYear		INT,
@EndMonth		INT,
@EndYear		INT,
@NPNCType_ID	INT,
@Campus_ID		INT,
@Ministry_ID	INT

AS

CREATE TABLE #Dates (
	StartHour	DATETIME,
	EndHour		DATETIME,
	[Date]		NVARCHAR(50)
)

INSERT #Dates SELECT 0, 24, '24 Hours'
INSERT #Dates SELECT 25, 48, '48 Hours'
INSERT #Dates SELECT 49, 168, '3 - 7 Days'
INSERT #Dates SELECT 169, 100000, '8 Days +'
INSERT #Dates SELECT -1,-1, 'Not Contacted'


SELECT		*,
			CASE WHEN FirstFollowupCallDate IS NOT NULL AND FirstFollowupVisitDate IS NOT NULL THEN 
					CASE WHEN FirstFollowupCallDate > FirstFollowupVisitDate THEN FirstFollowupCallDate ELSE FirstFollowupVisitDate END
				 ELSE COALESCE(FirstFollowupCallDate, FirstFollowupVisitDate) END AS FirstFollowUpDate
INTO		#NPNC
FROM		Church_NPNC



SELECT		'1st Contacted' as [Name],
			D.[Date],
			ISNULL((SELECT		COUNT(*)
					FROM		#NPNC N
					WHERE		(MONTH([FirstContactDate]) >= @StartMonth OR YEAR([FirstContactDate]) > @StartYear)
					AND			YEAR([FirstContactDate]) >= @StartYear
					AND			(MONTH([FirstContactDate]) <= @EndMonth OR YEAR([FirstContactDate]) < @EndYear)
					AND			YEAR([FirstContactDate]) <= @EndYear
					AND			N.NPNCType_ID = @NPNCType_ID
					AND			(N.CampusDecision_ID = @Campus_ID OR @Campus_ID IS NULL)
					AND			(N.Ministry_ID = @Ministry_ID OR @Ministry_ID IS NULL)
					AND			((FirstFollowUpDate IS NULL AND D.StartHour = -1)
					OR			(DATEDIFF(hh, FirstContactDate, FirstFollowUpDate) BETWEEN D.StartHour AND D.EndHour))
					AND			FirstContactDate IS NOT NULL),0) AS Number
INTO		#Report1
FROM		#Dates D



INSERT		#Report1
SELECT		'% Outcomed' as [Name],
			D.[Date],
			ISNULL((SELECT		CASE WHEN COUNT(*) > 0 THEN (SUM(CASE WHEN OutcomeStatus_ID IS NOT NULL THEN 1 ELSE 0 END) / CONVERT(DECIMAL(18,2),COUNT(*)) * 100)
								ELSE 0 END 
					FROM		#NPNC N
					WHERE		(MONTH([FirstContactDate]) >= @StartMonth OR YEAR([FirstContactDate]) > @StartYear)
					AND			YEAR([FirstContactDate]) >= @StartYear
					AND			(MONTH([FirstContactDate]) <= @EndMonth OR YEAR([FirstContactDate]) < @EndYear)
					AND			YEAR([FirstContactDate]) <= @EndYear
					AND			N.NPNCType_ID = @NPNCType_ID
					AND			(N.CampusDecision_ID = @Campus_ID OR @Campus_ID IS NULL)
					AND			(N.Ministry_ID = @Ministry_ID OR @Ministry_ID IS NULL)
					AND			((FirstFollowUpDate IS NULL AND D.StartHour = -1)
					OR			(DATEDIFF(hh, FirstContactDate, FirstFollowUpDate) BETWEEN D.StartHour AND D.EndHour))
					AND			FirstContactDate IS NOT NULL),0) AS Number
FROM		#Dates D


/* Create a second Formatted table for results display */

DECLARE	@Date		NVARCHAR(100)
DECLARE @SQL		NVARCHAR(500)

SELECT	DISTINCT [Name]
INTO	#Report
FROM	#Report1

DECLARE DateCursor CURSOR FOR
	SELECT	[Date]
	FROM	#Dates

OPEN DateCursor

FETCH NEXT FROM DateCursor INTO @Date
WHILE (@@FETCH_STATUS = 0) BEGIN

	SET @SQL = 'ALTER TABLE #Report ADD [' + @Date + '] int'
	EXEC(@SQL)

	SET @SQL = 'UPDATE #Report 
				SET [' + @Date + '] = [Number]
				FROM	#Report1 R
				WHERE	#Report.[Name] = R.[Name]
				AND		R.[Date] = ''' + @Date  + ''''
	EXEC(@SQL)

	FETCH NEXT FROM DateCursor INTO @Date
END

CLOSE DateCursor
DEALLOCATE DateCursor

SELECT	*
FROM	#Report1
WHERE	[Name] = '1st Contacted'

SELECT	*
FROM	#Report1
WHERE	[Name] = '% Outcomed'

SELECT	*
FROM	#Report
ORDER BY CASE WHEN [Name] = '% Outcomed' THEN 2 ELSE 1 END
GO
