SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ReportNPNC11SummaryStats]

@StartMonth		INT,
@StartYear		INT,
@EndMonth		INT,
@EndYear		INT,
@NPNCType_ID	INT,
@Campus_ID		INT

AS

CREATE TABLE #Dates (
	[Date] NVARCHAR(20)
)

CREATE TABLE #Report (
	[Name] NVARCHAR(50)
)

CREATE TABLE #Report1 (
	[Name] NVARCHAR(50),
	[Date] NVARCHAR(20),
	[Number] INT
)
	

DECLARE @Date DATETIME
SET @Date = CONVERT(DATETIME, CONVERT(NVARCHAR,@StartYear) + '-' +CONVERT(NVARCHAR, @StartMonth) + '-1')
WHILE @Date <= CONVERT(DATETIME, CONVERT(NVARCHAR,@EndYear) + '-' +CONVERT(NVARCHAR, @EndMonth) + '-1') BEGIN
	INSERT #Dates (Date) VALUES (LEFT(DATENAME(m, @Date),3) + ' ' + CONVERT(NVARCHAR,YEAR(@Date)))
	SET @Date = DATEADD(mm, 1, @Date)
END

IF @NPNCType_ID = 2 BEGIN

	SELECT		NCSummaryStatsMinistry_ID,
				NCDecisionType_ID,
				LEFT(DATENAME(m, [Date]),3) + ' ' + CONVERT(NVARCHAR,YEAR([Date])) as [Date],
				SUM(Number) as Number
	INTO		#t1
	FROM		Church_NCSummaryStats S
	WHERE		(MONTH([Date]) >= @StartMonth OR YEAR([Date]) > @StartYear)
	AND			YEAR([Date]) >= @StartYear
	AND			(MONTH([Date]) <= @EndMonth OR YEAR([Date]) < @EndYear)
	AND			YEAR([Date]) <= @EndYear
	AND			(Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
	GROUP BY	LEFT(DATENAME(m, [Date]),3) + ' ' + CONVERT(NVARCHAR,YEAR([Date])),
				NCSummaryStatsMinistry_ID,
				NCDecisionType_ID

	INSERT		#Report1
	SELECT		M.[Name] + ' - ' + T.[Name] as [Name],
				D.[Date],
				ISNULL((SELECT [Number] FROM #t1 WHERE [Date] = D.[Date] AND NCSummaryStatsMinistry_ID = M.NCSummaryStatsMinistry_ID AND NCDecisionType_ID = T.NCDecisionType_ID),0) as Number
	FROM		#Dates D,
				Church_NCSummaryStatsMinistry M,
				Church_NCDecisionType T
	ORDER BY	CONVERT(DATETIME, D.[Date]), M.[Name] + ' - ' + T.[Name]

	INSERT		#Report1
	SELECT		'Total - ' + T.[Name],
				D.[Date],
				ISNULL((SELECT SUM([Number]) FROM #t1 WHERE [Date] = D.[Date] AND NCDecisionType_ID = T.NCDecisionType_ID),0) as Number
	FROM		#Dates D,
				Church_NCDecisionType T
	ORDER BY	CONVERT(DATETIME, D.[Date]), 'Total - ' + T.[Name]

	INSERT 		#Report
	SELECT		'Total - ' + T.[Name] as [Name]
	FROM		Church_NCDecisionType T
	ORDER BY	T.SortOrder

	INSERT 		#Report
	SELECT		M.[Name] + ' - ' + T.[Name] as [Name]
	FROM		Church_NCSummaryStatsMinistry M,
				Church_NCDecisionType T
	ORDER BY	M.SortOrder, T.SortOrder
	
END
ELSE BEGIN

	SELECT		InitialStatus_ID,
				LEFT(DATENAME(m, [Date]),3) + ' ' + CONVERT(NVARCHAR,YEAR([Date])) as [Date],
				SUM(Number) as Number
	INTO		#t2
	FROM		Church_NPSummaryStats S
	WHERE		(MONTH([Date]) >= @StartMonth OR YEAR([Date]) > @StartYear)
	AND			YEAR([Date]) >= @StartYear
	AND			(MONTH([Date]) <= @EndMonth OR YEAR([Date]) < @EndYear)
	AND			YEAR([Date]) <= @EndYear
	AND			(Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
	GROUP BY	LEFT(DATENAME(m, [Date]),3) + ' ' + CONVERT(NVARCHAR,YEAR([Date])),
				InitialStatus_ID
			
	INSERT		#Report1
	SELECT		S.[Name] as [Name],
				D.[Date],
				ISNULL((SELECT [Number] FROM #t2 WHERE [Date] = D.[Date] AND InitialStatus_ID = S.InitialStatus_ID),0) as Number
	FROM		#Dates D,
				Church_NPNCInitialStatus S
	WHERE		S.NPNCType_ID = @NPNCType_ID
	ORDER BY	CONVERT(DATETIME, D.[Date]), S.[Name]

	INSERT		#Report1
	SELECT		'Total - ' + S.[Name],
				D.[Date],
				ISNULL((SELECT SUM([Number]) FROM #t2 WHERE [Date] = D.[Date] AND InitialStatus_ID = S.InitialStatus_ID),0) as Number
	FROM		#Dates D,
				Church_NPNCInitialStatus S
	WHERE		S.NPNCType_ID = @NPNCType_ID
	ORDER BY	CONVERT(DATETIME, D.[Date]), 'Total - ' + S.[Name]

	INSERT 		#Report
	SELECT		S.[Name] as [Name]
	FROM		Church_NPNCInitialStatus S
	WHERE		NPNCType_ID = @NPNCType_ID
	ORDER BY	S.SortOrder
END


/* Create a second Formatted table for results display */

DECLARE	@DateString	NVARCHAR(100)
DECLARE @SQL		NVARCHAR(500)


DECLARE DatesCursor CURSOR FOR
	SELECT	DISTINCT CONVERT(DATETIME,[Date])
	FROM	#Report1
	ORDER BY CONVERT(DATETIME,[Date]) ASC

OPEN DatesCursor

FETCH NEXT FROM DatesCursor INTO @Date
WHILE (@@FETCH_STATUS = 0) BEGIN

	SET @DateString = LEFT(DATENAME(m, @Date),3) + ' ' + CONVERT(NVARCHAR,YEAR(@Date)) 

	SET @SQL = 'ALTER TABLE #Report ADD [' + @DateString + '] int'
	EXEC(@SQL)

	SET @SQL = 'UPDATE #Report 
				SET [' + @DateString + '] = [Number]
				FROM	#Report1 R
				WHERE	R.[Name] = #Report.[Name] 
				AND		R.[Date] = ''' + @DateString  + ''''
	EXEC(@SQL)

	FETCH NEXT FROM DatesCursor INTO @Date
END

CLOSE DatesCursor
DEALLOCATE DatesCursor

SELECT	*
FROM	#Report

SELECT	*
FROM	#Report1

DROP TABLE #Report
DROP TABLE #Report1
DROP TABLE #Dates
GO
