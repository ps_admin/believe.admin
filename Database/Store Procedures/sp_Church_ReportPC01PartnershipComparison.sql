SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ReportPC01PartnershipComparison]

@StartMonth		INT,
@StartYear		INT,
@EndMonth		INT,
@EndYear		INT,
@Campus_ID		INT = null,
@Ministry_ID	INT

AS

SET NOCOUNT ON


/* Return a results set for use with charting */


SELECT		CONVERT(INT,Ministry_ID) as ID, [Name]
INTO		#Report
FROM		Church_Ministry
WHERE		(Ministry_ID = @Ministry_ID OR @Ministry_ID IS NULL)

IF @Ministry_ID IS NULL
	INSERT		#Report (ID, [Name]) VALUES (0, 'Total') 

SELECT		[Name],
			LEFT(DATENAME(m, [Date]),3) + ' ' + CONVERT(NVARCHAR,YEAR([Date])) as [Date],
			SUM(Number) as [Number]
INTO		#Report1
FROM		#Report R,
			vw_Church_HistoryPartnership P
WHERE		(R.ID = P.Ministry_ID OR (R.ID = 0 AND P.Ministry_ID IS NOT NULL))
AND			(P.Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
AND			(MONTH([Date]) >= @StartMonth OR YEAR([Date]) > @StartYear)
AND			YEAR([Date]) >= @StartYear
AND			(MONTH([Date]) <= @EndMonth OR YEAR([Date]) < @EndYear)
AND			YEAR([Date]) <= @EndYear
GROUP BY	[Name], [Date]

/* Create a second Formatted table for results display */

DECLARE @Date		DATETIME
DECLARE	@DateString	NVARCHAR(100)
DECLARE @SQL		NVARCHAR(500)

DECLARE DatesCursor CURSOR FOR
	SELECT	DISTINCT CONVERT(DATETIME,[Date])
	FROM	#Report1
	ORDER BY CONVERT(DATETIME,[Date]) ASC

OPEN DatesCursor

FETCH NEXT FROM DatesCursor INTO @Date
WHILE (@@FETCH_STATUS = 0) BEGIN

	SET @DateString = LEFT(DATENAME(m, @Date),3) + ' ' + CONVERT(NVARCHAR,YEAR(@Date)) 

	SET @SQL = 'ALTER TABLE #Report ADD [' + @DateString + '] int'
	EXEC(@SQL)

	SET @SQL = 'UPDATE #Report 
				SET [' + @DateString + '] = [Number]
				FROM	#Report1 R
				WHERE	R.[Name] = #Report.[Name] 
				AND		R.[Date] = ''' + @DateString  + ''''
	EXEC(@SQL)

	FETCH NEXT FROM DatesCursor INTO @Date
END

CLOSE DatesCursor
DEALLOCATE DatesCursor

ALTER TABLE #Report DROP COLUMN ID

SELECT		*
FROM		#Report
ORDER BY	CASE WHEN [Name] = 'Total' THEN 1 ELSE 0 END

SELECT	*
FROM	#Report1
GO
