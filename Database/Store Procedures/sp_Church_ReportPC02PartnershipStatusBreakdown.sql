SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ReportPC02PartnershipStatusBreakdown]

@StartMonth		INT,
@StartYear		INT,
@EndMonth		INT,
@EndYear		INT,
@Campus_ID		INT = null

AS

SET NOCOUNT ON


/* Create Results Table First */

DECLARE @Date	DATETIME
DECLARE @SQL	NVARCHAR(MAX)

SELECT	CONVERT(INT, ChurchStatus_ID) AS ChurchStatus_ID, [Name] as ChurchStatus
INTO	#Report
FROM	Church_ChurchStatus
WHERE	ChurchStatus_ID IN (1,2)

DECLARE DatesCursor CURSOR FOR
	SELECT	DISTINCT [Date] 
	FROM	vw_Church_HistoryPartnership 
	WHERE	(MONTH([Date]) >= @StartMonth OR YEAR([Date]) > @StartYear)
	AND		YEAR([Date]) >= @StartYear
	AND		(MONTH([Date]) <= @EndMonth OR YEAR([Date]) < @EndYear)
	AND		YEAR([Date]) <= @EndYear
	AND		(Campus_ID = @Campus_ID OR @Campus_ID IS NULL)

OPEN DatesCursor

FETCH NEXT FROM DatesCursor INTO @Date
WHILE (@@FETCH_STATUS = 0) BEGIN

	SET @SQL = 'ALTER TABLE #Report ADD [' + LEFT(DATENAME(m, @Date),3) + ' ' + CONVERT(NVARCHAR,YEAR(@Date)) +'] int'
	EXEC(@SQL)

	SET @SQL = 'UPDATE #Report SET [' + LEFT(DATENAME(m, @Date),3) + ' ' + CONVERT(NVARCHAR,YEAR(@Date)) +'] = 
							(	SELECT	SUM(Number)
								FROM	vw_Church_HistoryPartnership
								WHERE	MONTH([Date]) = ' + CONVERT(NVARCHAR, MONTH(@Date)) + '
								AND		YEAR([Date]) = ' + CONVERT(NVARCHAR, YEAR(@Date)) + 
								CASE WHEN @Campus_ID IS NOT NULL THEN ' AND Campus_ID =  ' + CONVERT(NVARCHAR, @Campus_ID) ELSE '' END + ' 
								AND		ChurchStatus_ID = #Report.ChurchStatus_ID) + 
							CASE WHEN #Report.ChurchStatus_ID = 2 THEN ISNULL((SELECT SUM(Number)
								FROM	vw_Church_HistoryPartnership
								WHERE	MONTH([Date]) = ' + CONVERT(NVARCHAR, MONTH(@Date)) + '
								AND		YEAR([Date]) = ' + CONVERT(NVARCHAR, YEAR(@Date)) + 
								CASE WHEN @Campus_ID IS NOT NULL THEN ' AND Campus_ID =  ' + CONVERT(NVARCHAR, @Campus_ID) ELSE '' END + '
								AND		ChurchStatus_ID = 3),0) ELSE 0 END'
								
	EXEC(@SQL)

	FETCH NEXT FROM DatesCursor INTO @Date
END

CLOSE DatesCursor
DEALLOCATE DatesCursor


ALTER TABLE #Report DROP COLUMN ChurchStatus_ID

SELECT	*
FROM	#Report



/* Secondly, return a results set for use with charting */

SELECT		S.ChurchStatus_ID,
			[Name] as ChurchStatus,
			LEFT(DATENAME(m, [Date]),3) + ' ' + CONVERT(NVARCHAR,YEAR([Date])) as [Date],
			SUM(Number) as [Number]
INTO		#t2
FROM		Church_ChurchStatus S,
			vw_Church_HistoryPartnership P
WHERE		S.ChurchStatus_ID = P.ChurchStatus_ID
AND			(MONTH([Date]) >= @StartMonth OR YEAR([Date]) > @StartYear)
AND			YEAR([Date]) >= @StartYear
AND			(MONTH([Date]) <= @EndMonth OR YEAR([Date]) < @EndYear)
AND			YEAR([Date]) <= @EndYear
AND			(Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
AND			S.ChurchStatus_ID IN (1,2)
GROUP BY	S.ChurchStatus_ID, [Name], [Date]

--Add Partner D figures into Contact
UPDATE		#t2
SET			Number = Number + ISNULL((	SELECT		SUM(Number)
								FROM		Church_ChurchStatus S,
											vw_Church_HistoryPartnership P
								WHERE		S.ChurchStatus_ID = P.ChurchStatus_ID
								AND			LEFT(DATENAME(m, [Date]),3) + ' ' + CONVERT(NVARCHAR,YEAR([Date])) = #t2.[Date]
								AND			(Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
								AND			S.ChurchStatus_ID IN (3)),0)
WHERE		ChurchStatus_ID = 2


SELECT * FROM #t2
GO
