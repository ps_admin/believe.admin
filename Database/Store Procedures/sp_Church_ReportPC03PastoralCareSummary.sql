SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ReportPC03PastoralCareSummary]

@StartMonth		INT,
@StartYear		INT,
@EndMonth		INT,
@EndYear		INT,
@Campus_ID		INT = null,
@Ministry_ID	INT

AS

SET NOCOUNT ON



/* Return a results set for use with charting */

CREATE TABLE #Report ( [Name] NVARCHAR(50) )
INSERT #Report ([Name]) VALUES ('Sunday Attendance *')
INSERT #Report ([Name]) VALUES ('UL Attendance *')
INSERT #Report ([Name]) VALUES ('Pastoral Contacts')
IF @Ministry_ID = 2 OR @Ministry_ID IS NULL INSERT #Report ([Name]) VALUES ('Boom Attendance *')

/* Get Total Number of ULG / Sundays in month, so we can work out the average */
SELECT		LEFT(DATENAME(m, [Date]),3) + ' ' + CONVERT(NVARCHAR,YEAR([Date])) as [Date],
			COUNT(*) AS Sunday,
			SUM(CONVERT(INT,ULG)) AS UL,
			SUM(CONVERT(INT,Boom)) AS Boom
INTO		#Totals
FROM		Church_PCRDate 
WHERE		(MONTH([Date]) >= @StartMonth OR YEAR([Date]) > @StartYear)
AND			YEAR([Date]) >= @StartYear
AND			(MONTH([Date]) <= @EndMonth OR YEAR([Date]) < @EndYear)
AND			YEAR([Date]) <= @EndYear
GROUP BY	LEFT(DATENAME(m, [Date]),3) + ' ' + CONVERT(NVARCHAR,YEAR([Date]))

SELECT		DISTINCT LEFT(DATENAME(m, [Date]),3) + ' ' + CONVERT(NVARCHAR,YEAR([Date])) as [Date]
INTO		#Dates
FROM		Church_PCRDate 
WHERE		(MONTH([Date]) >= @StartMonth OR YEAR([Date]) > @StartYear)
AND			YEAR([Date]) >= @StartYear
AND			(MONTH([Date]) <= @EndMonth OR YEAR([Date]) < @EndYear)
AND			YEAR([Date]) <= @EndYear

SELECT		R.[Name],
			D.[Date],
			SUM(CASE WHEN R.[Name] = 'Sunday Attendance *' THEN P.SundayAttendance
					 WHEN R.[Name] = 'UL Attendance *' THEN P.ULGAttendance
					 WHEN R.[Name] = 'Pastoral Contacts' THEN P.Call + P.Visit + P.Other
					 WHEN R.[Name] = 'Boom Attendance *' THEN P.BoomAttendance
					 ELSE 0 END) as [Number]
INTO		#Report1
FROM		#Report R,
			#Dates D,
			vw_Church_PCR P
WHERE		LEFT(DATENAME(m, P.[Date]),3) + ' ' + CONVERT(NVARCHAR,YEAR(P.[Date])) = D.[Date]
AND			(Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
AND			(P.Ministry_ID = @Ministry_ID OR @Ministry_ID IS NULL)
GROUP BY	R.[Name],
			D.[Date]
			
			
/* Add ULG Visitors to totals */
UPDATE		#Report1
SET			[Number] = [Number] + ISNULL((SELECT	SUM(ULGVisitors)
						FROM	Church_PCR P,
								Church_PCRDate D,
								Church_ULG U
						WHERE	P.PCRDate_ID = D.PCRDate_ID
						AND		P.ULG_ID = U.ULG_ID
						AND		(U.Ministry_ID = @Ministry_ID OR @Ministry_ID IS NULL)
						AND		(U.Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
						AND		LEFT(DATENAME(m, D.[Date]),3) + ' ' + CONVERT(NVARCHAR,YEAR(D.[Date])) = #Report1.[Date]),0)
WHERE		Name = 'UL Attendance *'
			
/* Divide total monthly attendance by the number of weeks, to give an average */			
UPDATE		#Report1
SET			[Number] = CASE WHEN [Name] = 'Sunday Attendance *' THEN CASE WHEN [Sunday] > 0 THEN [Number] / [Sunday] ELSE 0 END
							WHEN [Name] = 'UL Attendance *' THEN CASE WHEN [UL] > 0 THEN [Number] / [UL] ELSE 0 END
							WHEN [Name] = 'Boom Attendance *' THEN CASE WHEN [Boom] > 0 THEN [Number] / [Boom] ELSE 0 END
							ELSE [Number] END
FROM		#Totals
WHERE		#Totals.[Date] = #Report1.[Date]

			
/* Create a second Formatted table for results display */

DECLARE @Date		DATETIME
DECLARE	@DateString	NVARCHAR(100)
DECLARE @SQL		NVARCHAR(500)

DECLARE DatesCursor CURSOR FOR
	SELECT	DISTINCT CONVERT(DATETIME,[Date])
	FROM	#Report1
	ORDER BY CONVERT(DATETIME,[Date]) ASC

OPEN DatesCursor

FETCH NEXT FROM DatesCursor INTO @Date
WHILE (@@FETCH_STATUS = 0) BEGIN

	SET @DateString = LEFT(DATENAME(m, @Date),3) + ' ' + CONVERT(NVARCHAR,YEAR(@Date)) 

	SET @SQL = 'ALTER TABLE #Report ADD [' + @DateString + '] int'
	EXEC(@SQL)

	SET @SQL = 'UPDATE #Report 
				SET [' + @DateString + '] = [Number]
				FROM	#Report1 R
				WHERE	R.[Name] = #Report.[Name] 
				AND		R.[Date] = ''' + @DateString  + ''''
	EXEC(@SQL)

	FETCH NEXT FROM DatesCursor INTO @Date
END

CLOSE DatesCursor
DEALLOCATE DatesCursor

SELECT		*
FROM		#Report

SELECT		*
FROM		#Report1
ORDER BY	CONVERT(DATETIME, [Date])
GO
