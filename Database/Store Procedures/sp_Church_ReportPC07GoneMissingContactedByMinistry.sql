SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ReportPC07GoneMissingContactedByMinistry]

@StartMonth		INT,
@StartYear		INT,
@EndMonth		INT,
@EndYear		INT,
@Campus_ID		INT = null,
@Ministry_ID	INT

AS

SET NOCOUNT ON


/* Return a results set for use with charting */

SELECT		DISTINCT LEFT(DATENAME(m, [Date]),3) + ' ' + CONVERT(NVARCHAR,YEAR([Date])) as [Date]
INTO		#Dates
FROM		Church_PCRDate 
WHERE		(MONTH([Date]) >= @StartMonth OR YEAR([Date]) > @StartYear)
AND			YEAR([Date]) >= @StartYear
AND			(MONTH([Date]) <= @EndMonth OR YEAR([Date]) < @EndYear)
AND			YEAR([Date]) <= @EndYear

CREATE TABLE #Report (
	ID INT,
	[Name] NVARCHAR(100)
)
CREATE TABLE #Report1 (
	[Name] NVARCHAR(100),
	[Date] NVARCHAR(100),
	[Number] decimal (18,2)
)
CREATE TABLE #Attendance (
	ID INT,
	[Name] NVARCHAR(100),
	[Date] NVARCHAR(100),
	Contact_ID INT,
	Sunday INT,
	ULG INT,
	Contacted INT
)

INSERT		#Report
SELECT		Ministry_ID, [Name]
FROM		Church_Ministry
WHERE		(Ministry_ID = @Ministry_ID OR @Ministry_ID IS NULL)

INSERT		#Attendance
SELECT		R.ID,
			R.[Name],
			D.[Date],
			Contact_ID, 
			SUM(CONVERT(INT,SundayAttendance)) as Sunday,
			SUM(CONVERT(INT,ULGAttendance)) AS ULG,
			SUM(Call + Visit + Other) as Contacted
FROM		vw_Church_PCR P,
			#Report R,
			#Dates D
WHERE		LEFT(DATENAME(m, P.[Date]),3) + ' ' + CONVERT(NVARCHAR,YEAR(P.[Date])) = D.Date
AND			(P.Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
AND			P.Ministry_ID = R.ID
AND			P.ChurchStatus_ID <> 2 --Don't count contacts
GROUP BY	R.ID,
			R.[Name],
			D.[Date],
			Contact_ID

INSERT		#Report1
SELECT		R.[Name],
			D.[Date],
			CASE WHEN ISNULL((SELECT COUNT(*) FROM #Attendance WHERE Sunday = 0 AND ULG = 0 AND [Date] = D.[Date] AND ID = R.[ID]),0) > 0 
				THEN	(CONVERT(DECIMAL(18,2), ISNULL((SELECT COUNT(*) FROM #Attendance WHERE Sunday = 0 AND ULG = 0 AND Contacted > 0 AND [Date] = D.[Date] AND ID = R.[ID]),0)) / 
						CONVERT(DECIMAL(18,2), ISNULL((SELECT COUNT(*) FROM #Attendance WHERE Sunday = 0 AND ULG = 0 AND [Date] = D.[Date] AND ID = R.[ID]),0)))  * 100
				ELSE	0 END as [Number]
FROM		#Report R,
			#Dates D
ORDER BY	CONVERT(DATETIME,D.[Date]) ASC


SELECT	*
FROM	#Report1
GO
