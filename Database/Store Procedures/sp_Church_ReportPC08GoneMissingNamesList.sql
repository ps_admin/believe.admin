SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ReportPC08GoneMissingNamesList]

@Month				INT,
@Year				INT,
@Campus_ID			INT = null,
@Ministry_ID		INT,
@Region_ID	INT

AS

SET NOCOUNT ON

DECLARE		@Name		NVARCHAR(500),
			@Contact_ID	INT


SELECT		C.Contact_ID AS ID, 
			C.FirstName as [First Name],
			C.LastName as [Last Name],
			ISNULL((SELECT [Name] FROM Church_ChurchStatus WHERE ChurchStatus_ID = CI.ChurchStatus_ID), '') as [Church Status],
			ISNULL((SELECT [Name] FROM Church_Ministry WHERE Ministry_ID = P.Ministry_ID),'') as Ministry,
			ISNULL((SELECT [Name] FROM Church_Region WHERE Region_ID = P.Region_ID),'') as Region,
			CONVERT(NVARCHAR(1000), '') as [Urban Life],
			SUM(CONVERT(INT,SundayAttendance)) as Sunday,
			SUM(CONVERT(INT,ULGAttendance)) as ULG,
			SUM(Call + Visit + Other) as Contacted
INTO		#Attendance
FROM		vw_Church_PCR P,
			Common_Contact C,
			Common_ContactInternal CI
WHERE		P.Contact_ID = C.Contact_ID
AND			C.Contact_ID = CI.Contact_ID
AND			MONTH(P.[Date]) = @Month
AND			YEAR(P.[Date]) = @Year
AND			(P.Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
AND			(P.Ministry_ID = @Ministry_ID OR @Ministry_ID IS NULL)
AND			(P.Region_ID = @Region_ID OR @Region_ID IS NULL)
AND			CI.ChurchStatus_ID NOT IN (2,6,7) --Don't include Contacts, Mailinglist or Inactive 
GROUP BY	C.Contact_ID,
			C.FirstName,
			C.LastName,
			CI.ChurchStatus_ID,
			P.Ministry_ID,
			P.Region_ID
HAVING		SUM(CONVERT(INT, SundayAttendance)) = 0
AND			SUM(CONVERT(INT, ULGAttendance)) = 0
			
DECLARE ULGCursor CURSOR FOR
	SELECT DISTINCT C.Contact_ID, [Code] + ' ' + [Name]
	FROM Church_ULG U, Church_ULGContact C, #Attendance A
	WHERE U.ULG_ID = C.ULG_ID AND C.Contact_ID = A.ID AND C.Inactive=0

OPEN ULGCursor

FETCH NEXT FROM ULGCursor INTO @Contact_ID, @Name

WHILE @@FETCH_STATUS = 0 BEGIN
	UPDATE #Attendance SET [Urban Life] = [Urban Life] + @Name + ', ' WHERE ID = @Contact_ID
	FETCH NEXT FROM ULGCursor INTO @Contact_ID, @Name
END

CLOSE ULGCursor
DEALLOCATE ULGCursor

UPDATE		#Attendance
SET			[Urban Life] = LEFT([Urban Life], LEN([Urban Life]) - 1)
WHERE		LEN([Urban Life]) > 0


SELECT * FROM #Attendance
ORDER BY [Urban Life],[Last Name]
GO
