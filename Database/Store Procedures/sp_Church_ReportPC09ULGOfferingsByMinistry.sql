SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ReportPC09ULGOfferingsByMinistry]

@StartMonth		INT,
@StartYear		INT,
@EndMonth		INT,
@EndYear		INT,
@Campus_ID		INT = null,
@Ministry_ID	INT

AS

SET NOCOUNT ON


/* Return a results set for use with charting */

SELECT		DISTINCT LEFT(DATENAME(m, [Date]),3) + ' ' + CONVERT(NVARCHAR,YEAR([Date])) as [Date]
INTO		#Dates
FROM		Church_PCRDate 
WHERE		(MONTH([Date]) >= @StartMonth OR YEAR([Date]) > @StartYear)
AND			YEAR([Date]) >= @StartYear
AND			(MONTH([Date]) <= @EndMonth OR YEAR([Date]) < @EndYear)
AND			YEAR([Date]) <= @EndYear

CREATE TABLE #Report (
	ID INT,
	[Name] NVARCHAR(100)
)
CREATE TABLE #Report1 (
	[Name] NVARCHAR(100),
	[Date] NVARCHAR(100),
	[Number] int
)


INSERT		#Report
SELECT		Ministry_ID, [Name]
FROM		Church_Ministry
WHERE		(Ministry_ID = @Ministry_ID OR @Ministry_ID IS NULL)

INSERT		#Report1
SELECT		R.[Name],
			D.[Date],
			ISNULL((SELECT SUM(ULGOffering)
					FROM	Church_PCR P,
							Church_PCRDate PD,
							Church_ULG U
					WHERE	P.PCRDate_ID = PD.PCRDate_ID
					AND		P.ULG_ID = U.ULG_ID
					AND		LEFT(DATENAME(m, PD.[Date]),3) + ' ' + CONVERT(NVARCHAR,YEAR(PD.[Date])) = D.[Date]
					AND		(U.Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
					AND		U.Ministry_ID = R.ID),0) as [Number]
FROM		#Report R,
			#Dates D
ORDER BY	CONVERT(DATETIME,[Date]) ASC


/* Create a second Formatted table for results display */

DECLARE @Date		DATETIME
DECLARE	@DateString	NVARCHAR(100)
DECLARE @SQL		NVARCHAR(500)

DECLARE DatesCursor CURSOR FOR
	SELECT	DISTINCT CONVERT(DATETIME,[Date])
	FROM	#Report1
	ORDER BY CONVERT(DATETIME,[Date]) ASC

OPEN DatesCursor

FETCH NEXT FROM DatesCursor INTO @Date
WHILE (@@FETCH_STATUS = 0) BEGIN

	SET @DateString = LEFT(DATENAME(m, @Date),3) + ' ' + CONVERT(NVARCHAR,YEAR(@Date)) 

	SET @SQL = 'ALTER TABLE #Report ADD [' + @DateString + '] int'
	EXEC(@SQL)

	SET @SQL = 'UPDATE #Report 
				SET [' + @DateString + '] = [Number]
				FROM	#Report1 R
				WHERE	R.[Name] = #Report.[Name] 
				AND		R.[Date] = ''' + @DateString  + ''''
	EXEC(@SQL)

	FETCH NEXT FROM DatesCursor INTO @Date
END

CLOSE DatesCursor
DEALLOCATE DatesCursor

ALTER TABLE #Report DROP COLUMN ID

SELECT	*
FROM	#Report

SELECT	*
FROM	#Report1
GO
