SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ReportPC10ULGAttendanceByMinistryOrRegion]

@StartMonth		INT,
@StartYear		INT,
@EndMonth		INT,
@EndYear		INT,
@Campus_ID		INT = null,
@Ministry_ID	INT,
@ByRegion		BIT

AS

SET NOCOUNT ON


/* Return a results set for use with charting */

SELECT		DISTINCT LEFT(DATENAME(m, [Date]),3) + ' ' + CONVERT(NVARCHAR,YEAR([Date])) as [Date]
INTO		#Dates
FROM		Church_PCRDate 
WHERE		(MONTH([Date]) >= @StartMonth OR YEAR([Date]) > @StartYear)
AND			YEAR([Date]) >= @StartYear
AND			(MONTH([Date]) <= @EndMonth OR YEAR([Date]) < @EndYear)
AND			YEAR([Date]) <= @EndYear

CREATE TABLE #Report (
	ID			INT,
	[Name]		NVARCHAR(100),
	SortOrder	INT
)
CREATE TABLE #Report1 (
	[ID]		INT,
	[Name]		NVARCHAR(100),
	[Date]		NVARCHAR(100),
	[Number]	INT
)

/* Get Total Number of ULG / Sundays in month, so we can work out the average */
SELECT		LEFT(DATENAME(m, [Date]),3) + ' ' + CONVERT(NVARCHAR,YEAR([Date])) as [Date],
			COUNT(*) AS ULG
INTO		#Totals
FROM		Church_PCRDate D
WHERE		(MONTH([Date]) >= @StartMonth OR YEAR([Date]) > @StartYear)
AND			YEAR([Date]) >= @StartYear
AND			(MONTH([Date]) <= @EndMonth OR YEAR([Date]) < @EndYear)
AND			YEAR([Date]) <= @EndYear
AND			[Date] <= dbo.GetRelativeDate()
AND			D.ULG = 1
GROUP BY	LEFT(DATENAME(m, [Date]),3) + ' ' + CONVERT(NVARCHAR,YEAR([Date]))

IF @ByRegion = 0 BEGIN

	INSERT		#Report
	SELECT		Ministry_ID, [Name], SortOrder
	FROM		Church_Ministry
	WHERE		(Ministry_ID = @Ministry_ID OR @Ministry_ID IS NULL)

	INSERT		#Report1
	SELECT		R.ID,
				R.[Name],
				D.[Date],
				ISNULL((SELECT SUM(CONVERT(INT, ULGAttendance))
						FROM	vw_Church_PCR P
						WHERE	LEFT(DATENAME(m, P.[Date]),3) + ' ' + CONVERT(NVARCHAR,YEAR(P.[Date])) = D.[Date]
						AND		P.Ministry_ID = R.ID
						AND		(P.Campus_ID = @Campus_ID OR @Campus_ID IS NULL)),0) as [Number]
	FROM		#Report R,
				#Dates D
	ORDER BY	CONVERT(DATETIME,[Date]) ASC
		
	
	/* Add ULG Visitors to totals */
	UPDATE		#Report1
	SET			[Number] = [Number] + ISNULL((SELECT	SUM(ULGVisitors)
							FROM	Church_PCR P,
									Church_PCRDate D,
									Church_ULG U
							WHERE	P.PCRDate_ID = D.PCRDate_ID
							AND		P.ULG_ID = U.ULG_ID
							AND		U.Ministry_ID = #Report1.ID
							AND		(U.Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
							AND		LEFT(DATENAME(m, D.[Date]),3) + ' ' + CONVERT(NVARCHAR,YEAR(D.[Date])) = #Report1.[Date]),0)
	
END
ELSE BEGIN

	INSERT		#Report
	SELECT		DISTINCT R.Region_ID, [Name], SortOrder
	FROM		Church_Region R, Church_CampusMinistryRegion CMR
	WHERE		R.Region_ID = CMR.Region_ID
	AND			R.Deleted = 0
	AND			(Ministry_ID = @Ministry_ID OR @Ministry_ID IS NULL)
	AND			(Campus_ID = @Campus_ID OR @Campus_ID IS NULL)

	INSERT		#Report1
	SELECT		R.ID,
				R.[Name],
				D.[Date],
				ISNULL((SELECT SUM(CONVERT(INT,ULGAttendance))
						FROM	vw_Church_PCR P
						WHERE	LEFT(DATENAME(m, P.[Date]),3) + ' ' + CONVERT(NVARCHAR,YEAR(P.[Date])) = D.[Date]
						AND		(P.Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
						AND		P.Region_ID = R.ID),0) as [Number]
	FROM		#Report R,
				#Dates D
	ORDER BY	CONVERT(DATETIME,[Date]) ASC
		
	/* Add ULG Visitors to totals */
	UPDATE		#Report1
	SET			[Number] = [Number] + ISNULL((SELECT	SUM(ULGVisitors)
							FROM	Church_PCR P,
									Church_PCRDate D,
									Church_ULG U
							WHERE	P.PCRDate_ID = D.PCRDate_ID
							AND		P.ULG_ID = U.ULG_ID
							AND		U.Ministry_ID = #Report1.ID
							AND		(U.Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
							AND		LEFT(DATENAME(m, D.[Date]),3) + ' ' + CONVERT(NVARCHAR,YEAR(D.[Date])) = #Report1.[Date]),0)

END

UPDATE		#Report1
SET			[Number] = CASE WHEN [ULG] > 0 THEN [Number] / [ULG] ELSE 0 END
FROM		#Totals
WHERE		#Totals.[Date] = #Report1.[Date]



/* Create a second Formatted table for results display */

DECLARE @Date		DATETIME
DECLARE	@DateString	NVARCHAR(100)
DECLARE @SQL		NVARCHAR(500)

DECLARE DatesCursor CURSOR FOR
	SELECT	DISTINCT CONVERT(DATETIME,[Date])
	FROM	#Report1
	ORDER BY CONVERT(DATETIME,[Date]) ASC

OPEN DatesCursor

FETCH NEXT FROM DatesCursor INTO @Date
WHILE (@@FETCH_STATUS = 0) BEGIN

	SET @DateString = LEFT(DATENAME(m, @Date),3) + ' ' + CONVERT(NVARCHAR,YEAR(@Date)) 

	SET @SQL = 'ALTER TABLE #Report ADD [' + @DateString + '] int'
	EXEC(@SQL)

	SET @SQL = 'UPDATE #Report 
				SET [' + @DateString + '] = [Number]
				FROM	#Report1 R
				WHERE	R.[Name] = #Report.[Name] 
				AND		R.[Date] = ''' + @DateString  + ''''
	EXEC(@SQL)

	FETCH NEXT FROM DatesCursor INTO @Date
END

CLOSE DatesCursor
DEALLOCATE DatesCursor

ALTER TABLE #Report DROP COLUMN ID

SELECT		*
FROM		#Report
ORDER BY	SortOrder

SELECT		*
FROM		#Report1
GO
