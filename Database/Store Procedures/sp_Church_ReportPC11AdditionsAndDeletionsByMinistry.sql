SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ReportPC11AdditionsAndDeletionsByMinistry]

@StartDate		DATETIME,
@EndDate		DATETIME,
@Campus_ID		INT = null,
@Ministry_ID	INT

AS

SET NOCOUNT ON

SELECT		CONVERT(INT, M.Ministry_ID) as ID,
			[Name],
			ISNULL((SELECT COUNT(*) FROM Church_ChurchStatusChangeRequest CR, Common_ContactInternal CI WHERE CR.Contact_ID = CI.Contact_ID AND DateRequested >= @StartDate AND DateRequested <= DATEADD(dd, 1, @EndDate) AND (CR.Campus_ID = @Campus_ID OR @Campus_ID IS NULL) AND CR.Ministry_ID = M.Ministry_ID AND CR.ChurchStatus_ID in (1,2,3)),0) as Additions,
			ISNULL((SELECT COUNT(*) FROM Church_ChurchStatusChangeRequest CR, Common_ContactInternal CI WHERE CR.Contact_ID = CI.Contact_ID AND DateRequested >= @StartDate AND DateRequested <= DATEADD(dd, 1, @EndDate) AND (CR.Campus_ID = @Campus_ID OR @Campus_ID IS NULL) AND CurrentMinistry_ID = M.Ministry_ID AND CR.ChurchStatus_ID in (6,7)),0) as Deletions
INTO		#Report
FROM		Church_Ministry M
WHERE		(M.Ministry_ID = @Ministry_ID OR @Ministry_ID IS NULL)
UNION ALL
SELECT		0 as ID,
			'No Ministry',
			ISNULL((SELECT COUNT(*) FROM Church_ChurchStatusChangeRequest CR, Common_ContactInternal CI WHERE CR.Contact_ID = CI.Contact_ID AND DateRequested >= @StartDate AND DateRequested <= DATEADD(dd, 1, @EndDate) AND (CR.Campus_ID = @Campus_ID OR @Campus_ID IS NULL) AND CR.Ministry_ID is null AND CR.ChurchStatus_ID in (1,2,3)),0) as Additions,
			ISNULL((SELECT COUNT(*) FROM Church_ChurchStatusChangeRequest CR, Common_ContactInternal CI WHERE CR.Contact_ID = CI.Contact_ID AND DateRequested >= @StartDate AND DateRequested <= DATEADD(dd, 1, @EndDate) AND (CR.Campus_ID = @Campus_ID OR @Campus_ID IS NULL) AND CurrentMinistry_ID is null AND CR.ChurchStatus_ID in (6,7)),0) as Deletions
WHERE		@Ministry_ID IS NULL

DELETE FROM #Report
WHERE Additions = 0 AND Deletions = 0 AND ID = 0

INSERT		#Report
SELECT		0, 'Total', SUM(Additions), SUM(Deletions)
FROM		#Report

SELECT		[Name],
			Additions,
			Deletions
FROM		#Report
ORDER BY	CASE WHEN ID = 0 THEN 1 ELSE 0 END


SELECT		[Name],
			COUNT(*) as Number
FROM		Church_DeleteReason D,
			Church_ChurchStatusChangeRequest C,
			Common_ContactInternal CI
WHERE		D.DeleteReason_ID = C.DeleteReason_ID
AND			C.Contact_ID = CI.Contact_ID
AND			C.DateRequested >= @StartDate
AND 		C.DateRequested <= DATEADD(dd, 1, @EndDate)
AND			(CI.Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
GROUP BY	[Name]
GO
