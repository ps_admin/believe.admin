SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ReportPC12PartnershipComparisonChurchStatus]

@StartMonth			INT,
@StartYear			INT,
@EndMonth			INT,
@EndYear			INT,
@Campus_ID			INT,
@Ministry_ID		INT,
@Region_ID			INT

AS

SET NOCOUNT ON


/* Return a results set for use with charting */

SELECT		CONVERT(INT,CS.ChurchStatus_ID) as ChurchStatus_ID, 
			CS.Name as [ChurchStatus]
INTO		#Report
FROM		Church_ChurchStatus CS
WHERE		Deleted = 0
AND			ChurchStatus_ID <=5

SELECT		[ChurchStatus],
			LEFT(DATENAME(m, [Date]),3) + ' ' + CONVERT(NVARCHAR,YEAR([Date])) as [Date],
			SUM(Number) as [Number]
INTO		#Report1
FROM		#Report R,
			vw_Church_HistoryPartnership P
WHERE		(P.Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
AND			(P.Ministry_ID = @Ministry_ID OR @Ministry_ID IS NULL)
AND			(R.ChurchStatus_ID = P.ChurchStatus_ID OR (R.ChurchStatus_ID = 0 AND P.ChurchStatus_ID IS NULL))
AND			(MONTH([Date]) >= @StartMonth OR YEAR([Date]) > @StartYear)
AND			YEAR([Date]) >= @StartYear
AND			(MONTH([Date]) <= @EndMonth OR YEAR([Date]) < @EndYear)
AND			YEAR([Date]) <= @EndYear
GROUP BY	[ChurchStatus], [Date]

/* Create a second Formatted table for results display */

DECLARE @Date		DATETIME
DECLARE	@DateString	NVARCHAR(100)
DECLARE @SQL		NVARCHAR(500)

DECLARE DatesCursor CURSOR FOR
	SELECT	DISTINCT CONVERT(DATETIME,[Date])
	FROM	#Report1
	ORDER BY CONVERT(DATETIME,[Date]) ASC

OPEN DatesCursor

FETCH NEXT FROM DatesCursor INTO @Date
WHILE (@@FETCH_STATUS = 0) BEGIN

	SET @DateString = LEFT(DATENAME(m, @Date),3) + ' ' + CONVERT(NVARCHAR,YEAR(@Date)) 

	SET @SQL = 'ALTER TABLE #Report ADD [' + @DateString + '] int'
	EXEC(@SQL)

	SET @SQL = 'UPDATE #Report 
				SET [' + @DateString + '] = [Number]
				FROM	#Report1 R
				WHERE	R.[ChurchStatus] = #Report.[ChurchStatus] 
				AND		R.[Date] = ''' + @DateString  + ''''
	EXEC(@SQL)

	FETCH NEXT FROM DatesCursor INTO @Date
END

CLOSE DatesCursor
DEALLOCATE DatesCursor

ALTER TABLE #Report DROP COLUMN ChurchStatus_ID

SELECT		*
FROM		#Report
--ORDER BY	CASE WHEN [Name] = 'Total' THEN 1 ELSE 0 END

SELECT	*
FROM	#Report1
GO
