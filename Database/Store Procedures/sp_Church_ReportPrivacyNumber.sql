SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ReportPrivacyNumber]

@Campus_ID		INT = null

AS

SELECT	CI.PrivacyNumber as [Privacy #], 
		C.FirstName as [First Name],
		C.LastName as [Last Name],
		C.Address1 as [Address],
		C.Suburb,
		ISNULL((SELECT State FROM Common_GeneralState S WHERE State_ID = C.State_ID),'') as [State],
		C.Postcode,
		C.Phone,
		C.Mobile
FROM	Common_Contact C,
		Common_ContactInternal CI
WHERE	C.Contact_ID = CI.Contact_ID
AND		(CI.Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
AND		PrivacyNumber <> ''
GO
