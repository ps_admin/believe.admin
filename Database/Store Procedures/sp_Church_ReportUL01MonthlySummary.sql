SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ReportUL01MonthlySummary]

@StartMonth		INT,
@StartYear		INT,
@EndMonth		INT,
@EndYear		INT,
@ULG_ID			INT,
@Campus_ID		INT = null,
@Ministry_ID	INT,
@Region_ID		INT

AS

SET NOCOUNT ON



/* Return a results set for use with charting */

CREATE TABLE #Report ( [Name] NVARCHAR(50) )
INSERT #Report ([Name]) VALUES ('Sunday')
INSERT #Report ([Name]) VALUES ('UL')
IF (SELECT Ministry_ID FROM Church_ULG WHERE ULG_ID = @ULG_ID) = 2 OR @Ministry_ID = 2
	INSERT #Report ([Name]) VALUES ('Boom')
INSERT #Report ([Name]) VALUES ('Pastoral Contacts')
INSERT #Report ([Name]) VALUES ('Members')


/* Create a list of the ULG's that we're including */

SELECT ULG_ID INTO #ULG
FROM Church_ULG WHERE (ULG_ID = @ULG_ID OR @ULG_ID IS NULL) AND (Ministry_ID = @Ministry_ID OR @Ministry_ID IS NULL) AND (Region_ID = @Region_ID OR @Region_ID IS NULL)
	

/* Get Total Number of ULG / Sundays in month, so we can work out the average */
SELECT		LEFT(DATENAME(m, [Date]),3) + ' ' + CONVERT(NVARCHAR,YEAR([Date])) as [Date],
			CONVERT(INTEGER, ROUND(COUNT(*) / CONVERT(DECIMAL(18,0),(SELECT COUNT(*) FROM #ULG)), 0)) AS Sunday,
			CONVERT(INTEGER, ROUND(SUM(CONVERT(INT,ULG)) / CONVERT(DECIMAL(18,0),(SELECT COUNT(*) FROM #ULG)),0)) AS UL,
			CONVERT(INTEGER, ROUND(SUM(CONVERT(INT,Boom)) / CONVERT(DECIMAL(18,0),(SELECT COUNT(*) FROM #ULG)),0)) AS Boom
INTO		#Totals
FROM		Church_PCRDate D,
			Church_ULG U
WHERE		(MONTH([Date]) >= @StartMonth OR YEAR([Date]) > @StartYear)
AND			YEAR([Date]) >= @StartYear
AND			(MONTH([Date]) <= @EndMonth OR YEAR([Date]) < @EndYear)
AND			YEAR([Date]) <= @EndYear
AND			(Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
AND			D.[Date] >= U.ActiveDate
AND			(U.ULG_ID = @ULG_ID OR @ULG_ID IS NULL)
AND			(U.Ministry_ID = @Ministry_ID OR @Ministry_ID IS NULL)
AND			(U.Region_ID = @Region_ID OR @Region_ID IS NULL)
GROUP BY	LEFT(DATENAME(m, [Date]),3) + ' ' + CONVERT(NVARCHAR,YEAR([Date]))

SELECT		DISTINCT LEFT(DATENAME(m, [Date]),3) + ' ' + CONVERT(NVARCHAR,YEAR([Date])) as [Date]
INTO		#Dates
FROM		Church_PCRDate D,
			Church_ULG U
WHERE		(MONTH([Date]) >= @StartMonth OR YEAR([Date]) > @StartYear)
AND			YEAR([Date]) >= @StartYear
AND			(MONTH([Date]) <= @EndMonth OR YEAR([Date]) < @EndYear)
AND			YEAR([Date]) <= @EndYear
AND			(Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
AND			D.[Date] >= U.ActiveDate
AND			(U.ULG_ID = @ULG_ID OR @ULG_ID IS NULL)
AND			(U.Ministry_ID = @Ministry_ID OR @Ministry_ID IS NULL)
AND			(U.Region_ID = @Region_ID OR @Region_ID IS NULL)


SELECT		R.[Name],
			D.[Date],
			ISNULL(CASE	WHEN R.[Name] = 'Sunday' THEN (SELECT SUM(CONVERT(INT,P.SundayAttendance)) FROM vw_Church_PCR P WHERE	LEFT(DATENAME(m, P.[Date]),3) + ' ' + CONVERT(NVARCHAR,YEAR(P.[Date])) = D.[Date] AND (P.ULG_ID = @ULG_ID OR @ULG_ID IS NULL) AND (P.Ministry_ID = @Ministry_ID OR @Ministry_ID IS NULL) AND (P.Region_ID = @Region_ID OR @Region_ID IS NULL)) 
						WHEN R.[Name] = 'UL' THEN (SELECT SUM(CONVERT(INT,P.ULGAttendance)) FROM vw_Church_PCR P WHERE	LEFT(DATENAME(m, P.[Date]),3) + ' ' + CONVERT(NVARCHAR,YEAR(P.[Date])) = D.[Date] AND (P.ULG_ID = @ULG_ID OR @ULG_ID IS NULL) AND (P.Ministry_ID = @Ministry_ID OR @Ministry_ID IS NULL) AND (P.Region_ID = @Region_ID OR @Region_ID IS NULL)) 
						WHEN R.[Name] = 'Pastoral Contacts' THEN (SELECT SUM(P.[Call] + P.Visit + P.Other) FROM vw_Church_PCR P WHERE	LEFT(DATENAME(m, P.[Date]),3) + ' ' + CONVERT(NVARCHAR,YEAR(P.[Date])) = D.[Date] AND (P.ULG_ID = @ULG_ID OR @ULG_ID IS NULL) AND (P.Ministry_ID = @Ministry_ID OR @Ministry_ID IS NULL) AND (P.Region_ID = @Region_ID OR @Region_ID IS NULL)) 
						WHEN R.[Name] = 'Boom' THEN (SELECT SUM(CONVERT(INT,P.BoomAttendance)) FROM vw_Church_PCR P WHERE	LEFT(DATENAME(m, P.[Date]),3) + ' ' + CONVERT(NVARCHAR,YEAR(P.[Date])) = D.[Date] AND (P.ULG_ID = @ULG_ID OR @ULG_ID IS NULL) AND (P.Ministry_ID = @Ministry_ID OR @Ministry_ID IS NULL) AND (P.Region_ID = @Region_ID OR @Region_ID IS NULL)) 
						ELSE 0 END,0)
			as [Number]
INTO		#Report1
FROM		#Report R,
			#Dates D
			

UPDATE		#Report1
SET			[Number] = CASE WHEN [Name] = 'Sunday' THEN CASE WHEN [Sunday] > 0 THEN [Number] / [Sunday] ELSE 0 END
							WHEN [Name] = 'UL' THEN CASE WHEN [UL] > 0 THEN [Number] / [UL] ELSE 0 END
							WHEN [Name] = 'Boom' THEN CASE WHEN [Boom] > 0 THEN [Number] / [Boom] ELSE 0 END
							ELSE [Number] END
FROM		#Totals
WHERE		#Totals.[Date] = #Report1.[Date]
			

UPDATE		#Report1
SET			[Number] = ISNULL((SELECT SUM(Number) FROM vw_Church_HistoryULGMembership WHERE (ULG_ID = @ULG_ID OR @ULG_ID IS NULL) AND (Ministry_ID = @Ministry_ID OR @Ministry_ID IS NULL) AND (Region_ID = @Region_ID OR @Region_ID IS NULL) AND Date = #Report1.Date),0)
WHERE		[Name] = 'Members'

UPDATE		#Report1
SET			[Number] = [Number] + ISNULL((SELECT	SUM(ULGVisitors)
						FROM	Church_PCR P,
								Church_PCRDate D,
								Church_ULG U
						WHERE	P.PCRDate_ID = D.PCRDate_ID
						AND		P.ULG_ID = U.ULG_ID
						AND		(U.Ministry_ID = @Ministry_ID  OR @Ministry_ID IS NULL)
						AND		(U.Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
						AND		LEFT(DATENAME(m, D.[Date]),3) + ' ' + CONVERT(NVARCHAR,YEAR(D.[Date])) = #Report1.[Date]),0)
WHERE [Name] = 'UL'

/* Create a second Formatted table for results display */

DECLARE @Date		DATETIME
DECLARE	@DateString	NVARCHAR(100)
DECLARE @SQL		NVARCHAR(500)

DECLARE DatesCursor CURSOR FOR
	SELECT	DISTINCT CONVERT(DATETIME,[Date])
	FROM	#Report1
	ORDER BY CONVERT(DATETIME,[Date]) ASC

OPEN DatesCursor

FETCH NEXT FROM DatesCursor INTO @Date
WHILE (@@FETCH_STATUS = 0) BEGIN

	SET @DateString = LEFT(DATENAME(m, @Date),3) + ' ' + CONVERT(NVARCHAR,YEAR(@Date)) 

	SET @SQL = 'ALTER TABLE #Report ADD [' + @DateString + '] int'
	EXEC(@SQL)

	SET @SQL = 'UPDATE #Report 
				SET [' + @DateString + '] = [Number]
				FROM	#Report1 R
				WHERE	R.[Name] = #Report.[Name] 
				AND		R.[Date] = ''' + @DateString  + ''''
	EXEC(@SQL)

	FETCH NEXT FROM DatesCursor INTO @Date
END

CLOSE DatesCursor
DEALLOCATE DatesCursor

SELECT	*
FROM	#Report

SELECT		*
FROM		#Report1
ORDER BY	CONVERT(DATETIME, [Date])
GO
