SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ReportULGAttendanceRoll]

@ULG_ID		int,
@StartDate	datetime,
@EndDate	datetime

AS

SELECT		DISTINCT C.Contact_ID,
			C.FirstName + ' ' + C.LastName as Name, 
			C.LastName,
			CS.Name as [ChurchStatus]
INTO		#t1
FROM		Common_Contact C,
			Common_ContactInternal CI,
			Church_ChurchStatus CS,
			Church_PCRContact P,
			Church_PCR PCR,
			Church_PCRDate D
WHERE		C.Contact_ID = CI.Contact_ID
AND			CI.ChurchStatus_ID = CS.ChurchStatus_ID
AND			C.Contact_ID = P.Contact_ID
AND			PCR.ULG_ID = @ULG_ID
AND			P.PCR_ID = PCR.PCR_ID
AND			PCR.PCRDate_ID = D.PCRDate_ID
AND			D.[Date] >= @StartDate
AND			D.[Date] <= @EndDate
ORDER BY	C.LastName

DECLARE @PCRDate_ID		int,
		@SQL			nvarchar(max),
		@Column			nvarchar(100)

DECLARE PCRDateCursor Cursor FOR
	SELECT	DISTINCT D.PCRDate_ID 
	FROM	Church_PCRDate D, Church_PCR PCR, Church_PCRContact C
	WHERE	D.PCRDate_ID = PCR.PCRDate_ID AND PCR.PCR_ID = C.PCR_ID
	AND		D.[Date] >= @StartDate AND D.[Date] <= @EndDate AND	PCR.ULG_ID = @ULG_ID
	
OPEN PCRDateCursor

FETCH NEXT FROM PCRDateCursor INTO @PCRDate_ID
WHILE @@Fetch_Status = 0 BEGIN

	-- Sunday Attendance
	SET @Column = '[' + (SELECT LEFT(CONVERT(NVARCHAR, [Date], 106),6) FROM Church_PCRDate WHERE PCRDate_ID = @PCRDate_ID) + ' Sunday]'
	SET @SQL = 'ALTER TABLE #t1 ADD ' + @Column + ' bit '
	EXEC (@SQL)
	SET @SQL = 'UPDATE #t1 SET ' + @Column + ' = ISNULL((SELECT MAX(CONVERT(INT,C.SundayAttendance)) FROM Church_PCRDate D, Church_PCR P, Church_PCRContact C WHERE D.PCRDate_ID = ' + CONVERT(NVARCHAR, @PCRDate_ID) + ' AND D.PCRDate_ID = P.PCRDate_ID AND P.PCR_ID = C.PCR_ID AND P.ULG_ID = ' + CONVERT(NVARCHAR, @ULG_ID) + ' AND C.Contact_ID = #t1.Contact_ID),0)'
	EXEC (@SQL)
	
	-- ULG Attendance
	IF (SELECT ULG FROM Church_PCRDate WHERE PCRDate_ID = @PCRDate_ID) = 1 BEGIN
		SET @Column = '[' + (SELECT LEFT(CONVERT(NVARCHAR, [Date], 106),6) FROM Church_PCRDate WHERE PCRDate_ID = @PCRDate_ID) + ' ULG]'
		SET @SQL = 'ALTER TABLE #t1 ADD ' + @Column + ' bit '
		EXEC (@SQL)
		SET @SQL = 'UPDATE #t1 SET ' + @Column + ' = ISNULL((SELECT MAX(CONVERT(INT,C.ULGAttendance)) FROM Church_PCRDate D, Church_PCR P, Church_PCRContact C WHERE D.PCRDate_ID = ' + CONVERT(NVARCHAR, @PCRDate_ID) + ' AND D.PCRDate_ID = P.PCRDate_ID AND P.PCR_ID = C.PCR_ID AND P.ULG_ID = ' + CONVERT(NVARCHAR, @ULG_ID) + ' AND C.Contact_ID = #t1.Contact_ID),0)'
		EXEC (@SQL)
	END
	
	-- Boom Attendance
	IF (SELECT ULG FROM Church_PCRDate WHERE PCRDate_ID = @PCRDate_ID) = 1 AND (SELECT Ministry_ID FROM Church_ULG WHERE ULG_ID = @ULG_ID) = 2 BEGIN
		SET @Column = '[' + (SELECT LEFT(CONVERT(NVARCHAR, [Date], 106),6) FROM Church_PCRDate WHERE PCRDate_ID = @PCRDate_ID) + ' Boom]'
		SET @SQL = 'ALTER TABLE #t1 ADD ' + @Column + ' bit '
		EXEC (@SQL)
		SET @SQL = 'UPDATE #t1 SET ' + @Column + ' = ISNULL((SELECT MAX(CONVERT(INT,C.BoomAttendance)) FROM Church_PCRDate D, Church_PCR P, Church_PCRContact C WHERE D.PCRDate_ID = ' + CONVERT(NVARCHAR, @PCRDate_ID) + ' AND D.PCRDate_ID = P.PCRDate_ID AND P.PCR_ID = C.PCR_ID AND P.ULG_ID = ' + CONVERT(NVARCHAR, @ULG_ID) + ' AND C.Contact_ID = #t1.Contact_ID),0)'
		EXEC (@SQL)
	END
	
	-- FNL Attendance
	IF (SELECT FNL FROM Church_PCRDate WHERE PCRDate_ID = @PCRDate_ID) = 1 AND (SELECT Ministry_ID FROM Church_ULG WHERE ULG_ID = @ULG_ID) = 1 BEGIN
		SET @Column = '[' + (SELECT LEFT(CONVERT(NVARCHAR, [Date], 106),6) FROM Church_PCRDate WHERE PCRDate_ID = @PCRDate_ID) + ' FNL]'
		SET @SQL = 'ALTER TABLE #t1 ADD ' + @Column + ' bit '
		EXEC (@SQL)
		SET @SQL = 'UPDATE #t1 SET ' + @Column + ' = ISNULL((SELECT MAX(CONVERT(INT,C.FNLAttendance)) FROM Church_PCRDate D, Church_PCR P, Church_PCRContact C WHERE D.PCRDate_ID = ' + CONVERT(NVARCHAR, @PCRDate_ID) + ' AND D.PCRDate_ID = P.PCRDate_ID AND P.PCR_ID = C.PCR_ID AND P.ULG_ID = ' + CONVERT(NVARCHAR, @ULG_ID) + ' AND C.Contact_ID = #t1.Contact_ID),0)'
		EXEC (@SQL)
	END
		
	-- Calls
	SET @Column = '[' + (SELECT LEFT(CONVERT(NVARCHAR, [Date], 106),6) FROM Church_PCRDate WHERE PCRDate_ID = @PCRDate_ID) + ' Calls]'
	SET @SQL = 'ALTER TABLE #t1 ADD ' + @Column + ' int '
	EXEC (@SQL)
	SET @SQL = 'UPDATE #t1 SET ' + @Column + ' = ISNULL((SELECT MAX(C.Call) FROM Church_PCRDate D, Church_PCR P, Church_PCRContact C WHERE D.PCRDate_ID = ' + CONVERT(NVARCHAR, @PCRDate_ID) + ' AND D.PCRDate_ID = P.PCRDate_ID AND P.PCR_ID = C.PCR_ID AND P.ULG_ID = ' + CONVERT(NVARCHAR, @ULG_ID) + ' AND C.Contact_ID = #t1.Contact_ID),0)'
	EXEC (@SQL)
	
	-- Visits
	SET @Column = '[' + (SELECT LEFT(CONVERT(NVARCHAR, [Date], 106),6) FROM Church_PCRDate WHERE PCRDate_ID = @PCRDate_ID) + ' Visits]'
	SET @SQL = 'ALTER TABLE #t1 ADD ' + @Column + ' int '
	EXEC (@SQL)
	SET @SQL = 'UPDATE #t1 SET ' + @Column + ' = ISNULL((SELECT MAX(C.Visit) FROM Church_PCRDate D, Church_PCR P, Church_PCRContact C WHERE D.PCRDate_ID = ' + CONVERT(NVARCHAR, @PCRDate_ID) + ' AND D.PCRDate_ID = P.PCRDate_ID AND P.PCR_ID = C.PCR_ID AND P.ULG_ID = ' + CONVERT(NVARCHAR, @ULG_ID) + ' AND C.Contact_ID = #t1.Contact_ID),0)'
	EXEC (@SQL)

	FETCH NEXT FROM PCRDateCursor INTO @PCRDate_ID
END

CLOSE PCRDateCursor
DEALLOCATE PCRDateCursor

ALTER TABLE #t1 DROP COLUMN LastName

SELECT * FROM #t1
GO
