SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_RestrictedAccessRoleInsert]
(
	@ContactRole_ID int,
	@Role_ID int,
	@User_ID int = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin

	SET NOCOUNT ON

	INSERT INTO [Church_RestrictedAccessRole]
	(
		[ContactRole_ID],
		[Role_ID]
	)
	VALUES
	(
		@ContactRole_ID,
		@Role_ID
	)

	SELECT RestrictedAccessRole_ID = SCOPE_IDENTITY();
GO
