SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_RestrictedAccessRoleUpdate]
(
	@RestrictedAccessRole_ID int,
	@ContactRole_ID int,
	@Role_ID int,
	@User_ID int = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin

	SET NOCOUNT ON
	
	UPDATE [Church_RestrictedAccessRole]
	SET
		[ContactRole_ID] = @ContactRole_ID,
		[Role_ID] = @Role_ID
	WHERE 
		[RestrictedAccessRole_ID] = @RestrictedAccessRole_ID
GO
