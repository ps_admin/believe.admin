SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_RestrictedAccessULGDelete]
(
	@RestrictedAccessULG_ID int,
	@User_ID int = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin

	SET NOCOUNT ON

	--Update NPNC records to set carer to NULL where this person is the carer
	UPDATE  Church_NPNC
	SET		Carer_ID = NULL
	FROM	Church_NPNC NPNC
	WHERE	Carer_ID IN
	(
		SELECT	CR.Contact_ID
		FROM	Church_RestrictedAccessULG ULG,
				Church_ContactRole CR
		WHERE	ULG.ContactRole_ID = CR.ContactRole_ID
		AND		ULG.RestrictedAccessULG_ID = @RestrictedAccessULG_ID
		AND		(SELECT COUNT(*)
				FROM	Church_Role R2,
						Church_ContactRole CR2,
						Church_RestrictedAccessULG ULG2
				WHERE	R2.Role_ID = CR.Role_ID
				AND		CR2.ContactRole_ID = ULG2.ContactRole_ID
				AND		RoleTypeUL_ID IN (2,3)
				AND		CR2.Contact_ID = CR.Contact_ID) <= 1
		AND		ULG.ULG_ID = NPNC.ULG_ID
	)

	DELETE 
	FROM   [Church_RestrictedAccessULG]
	WHERE  [RestrictedAccessULG_ID] = @RestrictedAccessULG_ID
GO
