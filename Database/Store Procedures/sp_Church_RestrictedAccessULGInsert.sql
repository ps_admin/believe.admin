SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_RestrictedAccessULGInsert]
(
	@ContactRole_ID int,
	@ULG_ID int,
	@User_ID int = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin

	SET NOCOUNT ON

	INSERT INTO [Church_RestrictedAccessULG]
	(
		[ContactRole_ID],
		[ULG_ID]
	)
	VALUES
	(
		@ContactRole_ID,
		@ULG_ID
	)

	SELECT RestrictedAccessULG_ID = SCOPE_IDENTITY();
GO
