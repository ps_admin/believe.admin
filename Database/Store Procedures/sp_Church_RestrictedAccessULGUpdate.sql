SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_RestrictedAccessULGUpdate]
(
	@RestrictedAccessULG_ID int,
	@ContactRole_ID int,
	@ULG_ID int,
	@User_ID int = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin

	SET NOCOUNT ON
	
	UPDATE [Church_RestrictedAccessULG]
	SET
		[ContactRole_ID] = @ContactRole_ID,
		[ULG_ID] = @ULG_ID
	WHERE 
		[RestrictedAccessULG_ID] = @RestrictedAccessULG_ID
GO
