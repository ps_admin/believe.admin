SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_RoleDelete]
(
	@Role_ID int
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Church_Role]
	WHERE  [Role_ID] = @Role_ID
GO
