SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_RoleInsert]
(
	@Name nvarchar(50),
	@RoleType_ID int,
	@RoleTypeUL_ID int,
	@Deleted bit
)
AS
	SET NOCOUNT ON

	INSERT INTO [Church_Role]
	(
		[Name],
		[RoleType_ID],
		[RoleTypeUL_ID],
		[Deleted]
	)
	VALUES
	(
		@Name,
		@RoleType_ID,
		@RoleTypeUL_ID,
		@Deleted
	)

	SELECT Role_ID = SCOPE_IDENTITY();
GO
