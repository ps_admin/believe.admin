SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_RoleUpdate]
(
	@Role_ID int,
	@Name nvarchar(50),
	@RoleType_ID int,
	@RoleTypeUL_ID int,
	@Deleted bit
)
AS
	SET NOCOUNT ON
	
	UPDATE [Church_Role]
	SET
		[Name] = @Name,
		[RoleType_ID] = @RoleType_ID,
		[RoleTypeUL_ID] = @RoleTypeUL_ID,
		[Deleted] = @Deleted
	WHERE 
		[Role_ID] = @Role_ID
GO
