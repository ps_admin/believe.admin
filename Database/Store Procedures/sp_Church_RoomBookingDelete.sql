SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_RoomBookingDelete]
(
	@RoomBooking_ID int
)
AS
	SET NOCOUNT ON

	UPDATE	[Church_RoomBooking]
	SET		[Deleted] = 1
	WHERE	[RoomBooking_ID] = @RoomBooking_ID
GO
