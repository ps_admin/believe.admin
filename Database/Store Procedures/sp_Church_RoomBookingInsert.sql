SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_RoomBookingInsert]
(
	@Room_ID int,
	@BookingReason nvarchar(200),
	@BookedBy_ID int,
	@StartDate datetime,
	@EndDate datetime,
	@AllDayBooking bit,
	@RecurrenceInfo nvarchar(max),
	@BookingType int,
	@Deleted bit
)
AS
	SET NOCOUNT ON

	INSERT INTO [Church_RoomBooking]
	(
		[Room_ID],
		[BookingReason],
		[BookedBy_ID],
		[StartDate],
		[EndDate],
		[AllDayBooking],
		[RecurrenceInfo],
		[BookingType],
		[Deleted]
	)
	VALUES
	(
		@Room_ID,
		@BookingReason,
		@BookedBy_ID,
		@StartDate,
		@EndDate,
		@AllDayBooking,
		@RecurrenceInfo,
		@BookingType,
		@Deleted
	)

	SELECT RoomBooking_ID = SCOPE_IDENTITY();
GO
