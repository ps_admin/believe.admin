SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_RoomBookingSelect]
(
	@Room_ID int
)
AS
	SET NOCOUNT ON

	SELECT	*,
			U.Name as BookedBy
	FROM	[Church_RoomBooking] RB,
			vw_Users U
	WHERE	RB.BookedBy_ID = U.Contact_ID
	AND		[Room_ID] = @Room_ID
	AND		RB.[Deleted] = 0
GO
