SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_RoomBookingUpdate]
(
	@RoomBooking_ID int,
	@Room_ID int,
	@BookingReason nvarchar(200),
	@BookedBy_ID int,
	@StartDate datetime,
	@EndDate datetime,
	@AllDayBooking bit,
	@RecurrenceInfo nvarchar(max),
	@BookingType int,
	@Deleted bit
)
AS
	SET NOCOUNT ON
	
	UPDATE [Church_RoomBooking]
	SET
		[Room_ID] = @Room_ID,
		[BookingReason] = @BookingReason,
		[BookedBy_ID] = @BookedBy_ID,
		[StartDate] = @StartDate,
		[EndDate] = @EndDate,
		[AllDayBooking] = @AllDayBooking,
		[RecurrenceInfo] = @RecurrenceInfo,
		[BookingType] = @BookingType,
		[Deleted] = @Deleted
	WHERE 
		[RoomBooking_ID] = @RoomBooking_ID
GO
