SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_RoomDelete]
(
	@Room_ID int
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM	[Church_Room]
	WHERE	[Room_ID] = @Room_ID
GO
