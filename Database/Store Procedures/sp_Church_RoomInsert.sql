SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_RoomInsert]
(
	@Name nvarchar(50),
	@RestrictedRoom bit,
	@SortOrder int,
	@Deleted bit
)
AS
	SET NOCOUNT ON

	INSERT INTO [Church_Room]
	(
		[Name],
		[RestrictedRoom],
		[SortOrder],
		[Deleted]
	)
	VALUES
	(
		@Name,
		@RestrictedRoom,
		@SortOrder,
		@Deleted
	)

	SELECT Room_ID = SCOPE_IDENTITY();
GO
