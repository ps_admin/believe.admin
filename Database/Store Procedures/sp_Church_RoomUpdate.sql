SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_RoomUpdate]
(
	@Room_ID int,
	@Name nvarchar(50),
	@RestrictedRoom bit,
	@SortOrder int,
	@Deleted bit
)
AS
	SET NOCOUNT ON
	
	UPDATE [Church_Room]
	SET
		[Name] = @Name,
		[RestrictedRoom] = @RestrictedRoom,
		[SortOrder] = @SortOrder,
		[Deleted] = @Deleted
	WHERE 
		[Room_ID] = @Room_ID
GO
