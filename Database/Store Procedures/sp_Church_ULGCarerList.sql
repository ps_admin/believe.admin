SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ULGCarerList]

@ULG_ID		INT

AS

SELECT	DISTINCT CC.Contact_ID, 
		CC.FirstName + ' ' + CC.LastName + ' (' + CONVERT(VARCHAR,(SELECT COUNT(*) FROM Church_NPNC NPNC WHERE Carer_ID = CC.Contact_ID AND NPNC.OutcomeStatus_ID IS NULL)) + ')' as [Name]
FROM	Church_Role R,
		Church_ContactRole CR,
		Church_RestrictedAccessULG ULG,
		Common_Contact CC
WHERE	R.Role_ID = CR.Role_ID
AND		CR.ContactRole_ID = ULG.ContactRole_ID
AND		CR.Contact_ID = CC.Contact_ID
AND		RoleTypeUL_ID IN (2,3,4)
AND		ULG.ULG_ID = @ULG_ID
GO
