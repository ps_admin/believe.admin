SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ULGCheckPreviousPCRReportCompleted]

@ULG_ID	INT

AS

DECLARE @PreviousPCRDate_ID INT
DECLARE @PreviousPCRDate DATETIME

SELECT	TOP 1 @PreviousPCRDate_ID = PCRDate_ID FROM Church_PCRDate 
		WHERE Date < (SELECT Date FROM vw_Church_LatestPCRReportDate)
		ORDER BY DATE Desc

SELECT @PreviousPCRDate = Date FROM Church_PCRDate WHERE PCRDate_ID = @PreviousPCRDate_ID


SELECT	(SELECT Date FROM vw_Church_LatestPCRReportDate) as CurrentPCRDate,
		(SELECT PCRDate_ID FROM vw_Church_LatestPCRReportDate) as CurrentPCRDate_ID,
		@PreviousPCRDate as PreviousPCRDate,
		@PreviousPCRDate_ID as PreviousPCRDate_ID,
		COALESCE(
			(SELECT ReportCompleted FROM Church_PCR WHERE PCRDate_ID = @PreviousPCRDate_ID AND ULG_ID = @ULG_ID),
			(SELECT CASE WHEN ActiveDate > @PreviousPCRDate THEN 1 ELSE 0 END FROM Church_ULG WHERE ULG_ID = @ULG_ID))
		AS PreviousReportCompleted
GO
