SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ULGCoachList]

@ULG_ID		INT

AS

SELECT	DISTINCT CC.Contact_ID
FROM	Church_Role R,
		Church_ContactRole CR,
		Church_RestrictedAccessULG ULG,
		Common_Contact CC
WHERE	R.Role_ID = CR.Role_ID
AND		CR.ContactRole_ID = ULG.ContactRole_ID
AND		R.RoleTypeUL_ID = 2 --UL Coach
AND		CR.Contact_ID = CC.Contact_ID
AND		ULG.ULG_ID = @ULG_ID
GO
