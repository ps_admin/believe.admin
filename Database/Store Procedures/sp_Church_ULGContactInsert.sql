SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ULGContactInsert]
(
	@ULG_ID int,
	@Contact_ID int,
	@DateJoined datetime,
	@Inactive bit,
	@SortOrder int,
	@GUID uniqueidentifier,
	@User_ID int = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin

	SET NOCOUNT ON

	INSERT INTO [Church_ULGContact]
	(
		[ULG_ID],
		[Contact_ID],
		[DateJoined],
		[Inactive],
		[SortOrder],
		[GUID]
	)
	VALUES
	(
		@ULG_ID,
		@Contact_ID,
		@DateJoined,
		@Inactive,
		@SortOrder,
		@GUID
	)

	SELECT ULGContact_ID = SCOPE_IDENTITY();
GO
