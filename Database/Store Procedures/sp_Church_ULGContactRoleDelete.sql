SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ULGContactRoleDelete]
(
	@ULGContactRole_ID int
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Church_ULGContactRole]
	WHERE  [ULGContactRole_ID] = @ULGContactRole_ID
GO
