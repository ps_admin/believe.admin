SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ULGContactRoleInsert]
(
	@ULGContact_ID int,
	@ULGRole_ID int,
	@DateAdded datetime
)
AS
	SET NOCOUNT ON

	INSERT INTO [Church_ULGContactRole]
	(
		[ULGContact_ID],
		[ULGRole_ID],
		[DateAdded]
	)
	VALUES
	(
		@ULGContact_ID,
		@ULGRole_ID,
		@DateAdded
	)

	SELECT ULGContactRole_ID = SCOPE_IDENTITY();
GO
