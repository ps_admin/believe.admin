SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ULGContactRoleUpdate]
(
	@ULGContactRole_ID int,
	@ULGContact_ID int,
	@ULGRole_ID int,
	@DateAdded datetime
)
AS
	SET NOCOUNT ON
	
	UPDATE [Church_ULGContactRole]
	SET
		[ULGContact_ID] = @ULGContact_ID,
		[ULGRole_ID] = @ULGRole_ID,
		[DateAdded] = @DateAdded
	WHERE 
		[ULGContactRole_ID] = @ULGContactRole_ID
GO
