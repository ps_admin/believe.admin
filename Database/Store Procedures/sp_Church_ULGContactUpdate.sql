SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ULGContactUpdate]
(
	@ULGContact_ID int,
	@ULG_ID int,
	@Contact_ID int,
	@DateJoined datetime,
	@Inactive bit,
	@SortOrder int,
	@GUID uniqueidentifier,
	@User_ID int = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin

	SET NOCOUNT ON
	
	UPDATE [Church_ULGContact]
	SET
		[ULG_ID] = @ULG_ID,
		[Contact_ID] = @Contact_ID,
		[DateJoined] = @DateJoined,
		[Inactive] = @Inactive,
		[SortOrder] = @SortOrder,
		[GUID] = @GUID
	WHERE 
		[ULGContact_ID] = @ULGContact_ID
GO
