SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ULGGetNextReportDueDate]

@ULGDate	BIT

AS

SELECT	DATEADD(dd,-1, DATEADD(hh,(SELECT PCRDueHours FROM Church_PCROptions), MIN([Date])))
FROM	Church_PCRDate
WHERE	(ULG = @ULGDate OR @ULGDate = 0)
AND		[Date] >= dbo.GetRelativeDate()
GO
