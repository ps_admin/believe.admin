SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ULGInsert]
(
	@Code nvarchar(20),
	@Name nvarchar(50),
	@GroupType nvarchar(50),
	@Campus_ID int =1,
	@Ministry_ID int,
	@Region_ID int,
	@ActiveDate datetime,
	@ULGDateDay_ID int,
	@ULGDateTime_ID int,
	@Address1 nvarchar(80),
	@Address2 nvarchar(80),
	@Suburb nvarchar(50),
	@State_ID int,
	@Postcode nvarchar(20),
	@Country_ID int,
	@Inactive bit,
	@SortOrder int = 0
)
AS
	SET NOCOUNT ON

	INSERT INTO [Church_ULG]
	(
		[Code],
		[Name],
		[GroupType],
		[Campus_ID],
		[Ministry_ID],
		[Region_ID],
		[ActiveDate],
		[ULGDateDay_ID],
		[ULGDateTime_ID],
		[Address1],
		[Address2],
		[Suburb],
		[State_ID],
		[Postcode],
		[Country_ID],
		[Inactive],
		[SortOrder]
	)
	VALUES
	(
		@Code,
		@Name,
		@GroupType,
		@Campus_ID,
		@Ministry_ID,
		@Region_ID,
		@ActiveDate,
		@ULGDateDay_ID,
		@ULGDateTime_ID,
		@Address1,
		@Address2,
		@Suburb,
		@State_ID,
		@Postcode,
		@Country_ID,
		@Inactive,
		@SortOrder
	)

	SELECT ULG_ID = SCOPE_IDENTITY();
GO
