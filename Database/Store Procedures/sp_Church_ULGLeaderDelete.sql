SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ULGLeaderDelete]
(
	@ULGLeader_ID int
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Church_ULGLeader]
	WHERE  [ULGLeader_ID] = @ULGLeader_ID
GO
