SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ULGLeaderInsert]
(
	@ULG_ID int,
	@Contact_ID int,
	@ULGLeaderRole_ID int
)
AS
	SET NOCOUNT ON

	INSERT INTO [Church_ULGLeader]
	(
		[ULG_ID],
		[Contact_ID],
		[ULGLeaderRole_ID]
	)
	VALUES
	(
		@ULG_ID,
		@Contact_ID,
		@ULGLeaderRole_ID
	)

	SELECT ULGLeader_ID = SCOPE_IDENTITY();
GO
