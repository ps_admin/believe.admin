SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ULGLeaderUpdate]
(
	@ULGLeader_ID int,
	@ULG_ID int,
	@Contact_ID int,
	@ULGLeaderRole_ID int
)
AS
	SET NOCOUNT ON
	
	UPDATE [Church_ULGLeader]
	SET
		[ULG_ID] = @ULG_ID,
		[Contact_ID] = @Contact_ID,
		[ULGLeaderRole_ID] = @ULGLeaderRole_ID
	WHERE 
		[ULGLeader_ID] = @ULGLeader_ID
GO
