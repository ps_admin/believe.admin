SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ULGList]

@Campus_ID				INT = null,
@Ministry_ID			INT,
@Region_ID		INT,
@Inactive				INT

AS

SELECT		U.ULG_ID as _ULG_ID,
			U.Code,
			U.Name,
			U.Suburb,
			C.[Name] as Campus,
			D.[Name] as Ministry,
			R.[Name] as Region,
			ISNULL((SELECT [Name] FROM Church_ULGDateDay WHERE ULGDateDay_ID = U.ULGDateDay_ID),'') AS [Group Day],
			ISNULL((SELECT [Name] FROM Church_ULGDateTime WHERE ULGDateTime_ID = U.ULGDateTime_ID),'') AS [Group Time],
			(SELECT COUNT(*) FROM Church_ULGContact WHERE ULG_ID = U.ULG_ID AND Inactive = 0) as Members,
			CASE WHEN U.Inactive = 1 THEN 'No' ELSE 'Yes' END as Active
FROM		Church_ULG U 
			JOIN Church_Ministry D ON U.Ministry_ID = D.Ministry_ID 
			LEFT JOIN Church_Region R ON U.Region_ID = R.Region_ID
			JOIN Church_Campus C ON U.Campus_ID = C.Campus_ID
WHERE		(U.Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
AND			(U.Ministry_ID = @Ministry_ID OR @Ministry_ID IS NULL)
AND			(U.Region_ID = @Region_ID OR @Region_ID IS NULL)
AND			(U.Inactive = @Inactive OR @Inactive = -1)
ORDER BY	CASE WHEN LEFT(Code,2) = 'UL' THEN 1 ELSE 2 END,
			U.Code
GO
