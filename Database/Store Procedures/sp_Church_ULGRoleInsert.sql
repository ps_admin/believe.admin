SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ULGRoleInsert]
(
	@Name nvarchar(50),
	@IsNPCarer bit,
	@IsNCCarer bit,
	@SortOrder int
)
AS
	SET NOCOUNT ON

	INSERT INTO [Church_ULGRole]
	(
		[Name],
		[IsNPCarer],
		[IsNCCarer],
		[SortOrder]
	)
	VALUES
	(
		@Name,
		@IsNPCarer,
		@IsNCCarer,
		@SortOrder
	)

	SELECT ULGRole_ID = SCOPE_IDENTITY();
GO
