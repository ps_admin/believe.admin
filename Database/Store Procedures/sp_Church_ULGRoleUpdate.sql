SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ULGRoleUpdate]
(
	@ULGRole_ID int,
	@Name nvarchar(50),
	@IsNPCarer bit,
	@IsNCCarer bit,
	@SortOrder int
)
AS
	SET NOCOUNT ON
	
	UPDATE [Church_ULGRole]
	SET
		[Name] = @Name,
		[IsNPCarer] = @IsNPCarer,
		[IsNCCarer] = @IsNCCarer,
		[SortOrder] = @SortOrder
	WHERE 
		[ULGRole_ID] = @ULGRole_ID
GO
