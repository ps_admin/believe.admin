SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ULGSelectAll]

AS

SELECT		CONVERT(INT,ULG_ID) AS ULG_ID,
			Code,
			[Name],
			GroupType,
			Campus_ID,
			Ministry_ID,
			Region_ID,
			ULGDateDay_ID,
			ULGDateTime_ID,
			ActiveDate,
			Address1,
			Address2,
			Suburb,
			State_ID,
			Postcode,
			Country_ID,
			Inactive,
			IDENTITY(int,1,1) as SortOrder
INTO		#ULG
FROM		Church_ULG
ORDER BY	CASE WHEN LEFT(Code,2) = 'UL' THEN 1 ELSE 2 END,
			Code

SELECT		*
FROM		#ULG

SELECT		*
FROM		Church_ULGContact
WHERE		Inactive = 0
GO
