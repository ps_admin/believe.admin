SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Church_ULGUpdate]
(
	@ULG_ID int,
	@Code nvarchar(20),
	@Name nvarchar(50),
	@GroupType nvarchar(50),
	@Campus_ID int=1,
	@Ministry_ID int,
	@Region_ID int,
	@ActiveDate datetime,
	@ULGDateDay_ID int,
	@ULGDateTime_ID int,
	@Address1 nvarchar(80),
	@Address2 nvarchar(80),
	@Suburb nvarchar(50),
	@State_ID int,
	@Postcode nvarchar(20),
	@Country_ID int,
	@Inactive bit,
	@SortOrder int = 0
)
AS
	SET NOCOUNT ON
	
	UPDATE [Church_ULG]
	SET
		[Code] = @Code,
		[Name] = @Name,
		[GroupType] = @GroupType,
		[Campus_ID] = @Campus_ID,
		[Ministry_ID] = @Ministry_ID,
		[Region_ID] = @Region_ID,
		[ActiveDate] = @ActiveDate,
		[ULGDateDay_ID] = @ULGDateDay_ID,
		[ULGDateTime_ID] = @ULGDateTime_ID,
		[Address1] = @Address1,
		[Address2] = @Address2,
		[Suburb] = @Suburb,
		[State_ID] = @State_ID,
		[Postcode] = @Postcode,
		[Country_ID] = @Country_ID,
		[Inactive] = @Inactive,
		[SortOrder] = @SortOrder
	WHERE 
		[ULG_ID] = @ULG_ID
GO
