SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_ChurchListUsersByRole2]
@Role_ID		INT

AS

DECLARE @SQL VARCHAR(8000)

SET @SQL =
'SELECT *
FROM	vw_Users U, 
		Church_ContactRole R
WHERE	U.Contact_ID = R.Contact_ID'


IF @Role_ID = 19 
	SET @SQL = @SQL + ' AND	(R.Role_ID = 19 OR R.Role_ID = 84)'
ELSE
	SET @SQL = @SQL + ' AND	R.Role_ID = ' + @Role_ID
	
EXEC(@SQL)
GO
