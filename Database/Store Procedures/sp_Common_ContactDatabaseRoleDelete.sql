SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_ContactDatabaseRoleDelete]
(
	@ContactDatabaseRole_ID int,
	@User_ID int = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin

	SET NOCOUNT ON

	DELETE 
	FROM   [Common_ContactDatabaseRole]
	WHERE  [ContactDatabaseRole_ID] = @ContactDatabaseRole_ID
GO
