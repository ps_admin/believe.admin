SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_ContactDatabaseRoleInsert]
(
	@Contact_ID int,
	@DatabaseRole_ID int,
	@DateAdded datetime,
	@User_ID int = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin

	SET NOCOUNT ON

	INSERT INTO [Common_ContactDatabaseRole]
	(
		[Contact_ID],
		[DatabaseRole_ID],
		[DateAdded]
	)
	VALUES
	(
		@Contact_ID,
		@DatabaseRole_ID,
		@DateAdded
	)

	SELECT ContactDatabaseRole_ID = SCOPE_IDENTITY();
GO
