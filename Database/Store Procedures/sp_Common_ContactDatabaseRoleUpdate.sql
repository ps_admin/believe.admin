SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_ContactDatabaseRoleUpdate]
(
	@ContactDatabaseRole_ID int,
	@Contact_ID int,
	@DatabaseRole_ID int,
	@DateAdded datetime,
	@User_ID int = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin

	SET NOCOUNT ON
	
	UPDATE [Common_ContactDatabaseRole]
	SET
		[Contact_ID] = @Contact_ID,
		[DatabaseRole_ID] = @DatabaseRole_ID,
		[DateAdded] = @DateAdded
	WHERE 
		[ContactDatabaseRole_ID] = @ContactDatabaseRole_ID
GO
