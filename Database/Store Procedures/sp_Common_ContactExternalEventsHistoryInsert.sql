SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_ContactExternalEventsHistoryInsert]

@Contact_ID			INT,
@Conference_ID		INT,
@Venue_ID			INT,
@User_ID			INT,
@Action				NVARCHAR(20),
@Item				NVARCHAR(200),
@Table				NVARCHAR(50),
@FieldName			NVARCHAR(200),
@OldValue			NVARCHAR(MAX),
@OldText			NVARCHAR(MAX),
@NewValue			NVARCHAR(MAX),
@NewText			NVARCHAR(MAX)

AS

INSERT Common_ContactExternalEventsHistory 
(
	Contact_ID,
	Conference_ID,
	Venue_ID,
	User_ID,
	DateChanged,
	Action,
	Item,
	[Table],
	FieldName,
	OldValue,
	OldText,
	NewValue,
	NewText
)
VALUES
(
	@Contact_ID,
	@Conference_ID,
	@Venue_ID,
	@User_ID,
	dbo.GetRelativeDate(),
	@Action,
	@Item,
	@Table,
	@FieldName,
	@OldValue,
	@OldText,
	@NewValue,
	@NewText
)
GO
