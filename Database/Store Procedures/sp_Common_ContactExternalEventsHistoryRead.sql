SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_ContactExternalEventsHistoryRead]

@Contact_ID		INT

AS

SELECT 	CH.CH_ID,
		CH.Contact_ID,
		CH.DateChanged,
		CH.User_ID,
		CH.Venue_ID,
		CH.Conference_ID,
		CH.Action,
		CH.Item,
		CH.[Table],
		CH.FieldName,
		CH.OldValue,
		CH.OldText,
		CH.NewValue,
		CH.NewText,
		U.FirstName + ' ' + U.LastName as UserName,
		(SELECT VenueName FROM Events_Venue WHERE Venue_ID = CH.Venue_ID) as Venue,
		(SELECT ConferenceName FROM Events_Conference WHERE Conference_ID = CH.Conference_ID) as Conference
FROM	Common_ContactExternalEventsHistory CH,
		Common_ContactExternal CE,
		vw_Users U
WHERE 	CH.Contact_ID = CE.Contact_ID
AND		CH.User_ID = U.Contact_ID
AND		CE.Contact_ID = @Contact_ID
AND		CE.Deleted = 0

UNION ALL

SELECT 	CH.CH_ID,
		CH.Contact_ID,
		CH.DateChanged,
		CH.User_ID,
		CONVERT(INT, NULL) as Venue_ID,
		CONVERT(INT, NULL) as Conference_ID,
		CH.Action,
		CH.Item,
		CH.[Table],
		CH.FieldName,
		CH.OldValue,
		CH.OldText,
		CH.NewValue,
		CH.NewText,
		U.FirstName + ' ' + U.LastName as UserName,
		CONVERT(NVARCHAR,'') as Venue,
		CONVERT(NVARCHAR,'') as Conference
FROM	Common_ContactHistory CH,
		Common_ContactExternal CE,
		vw_Users U
WHERE 	CH.Contact_ID = CE.Contact_ID
AND		CH.User_ID = U.Contact_ID
AND		CE.Contact_ID = @Contact_ID
AND		CE.Deleted = 0
ORDER BY DateChanged DESC
GO
