SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_ContactExternalEventsRead]

@Contact_ID		INT

 AS

-- SELECT Contact Data
SELECT 	CEE.*
FROM 	Common_ContactExternalEvents CEE,
	 	Common_ContactExternal CE
WHERE 	CE.Contact_ID = @Contact_ID
AND		CE.Contact_ID = CEE.Contact_ID
AND		CE.Deleted = 0


-- SELECT Contact Payment Data
SELECT 	CP.* 
FROM 	Events_ContactPayment CP,
		Common_ContactExternal C
WHERE 	CP.Contact_ID = C.Contact_ID
AND		C.Contact_ID = @Contact_ID
AND		C.Deleted = 0

-- SELECT Registration Data
SELECT 	Re.*
FROM 	Events_Registration Re,
		Common_ContactExternal C
WHERE 	Re.Contact_ID = C.Contact_ID
AND		C.Contact_ID = @Contact_ID
AND		C.Deleted = 0

-- SELECT Registration Child Information Data
SELECT 	RC.*
FROM 	Events_RegistrationChildInformation RC,
		Events_Registration Re,
		Common_ContactExternal C
WHERE 	RC.Registration_ID = Re.Registration_ID
AND		Re.Contact_ID = C.Contact_ID
AND		C.Contact_ID = @Contact_ID
AND		C.Deleted = 0

-- SELECT Contact's Pre-Orders
SELECT	P.*
FROM	Events_PreOrder P,
		Events_Registration Re, 
		Common_ContactExternal C
WHERE 	P.Registration_ID = Re.Registration_ID
AND		Re.Contact_ID = C.Contact_ID
AND		C.Contact_ID = @Contact_ID
AND		C.Deleted = 0

-- SELECT Registration Option Data
SELECT 	RO.*
FROM 	Events_RegistrationOption RO,
		Events_Registration Re,
		Common_ContactExternal C
WHERE 	RO.Registration_ID = Re.Registration_ID
AND		Re.Contact_ID = C.Contact_ID
AND		C.Contact_ID = @Contact_ID
AND		C.Deleted = 0

-- SELECT Contact's Waiting List
SELECT 	WL.*
FROM	Events_WaitingList WL,
		Common_ContactExternal C
WHERE 	WL.Contact_ID = C.Contact_ID
AND		C.Contact_ID = @Contact_ID
AND		C.Deleted = 0
GO
