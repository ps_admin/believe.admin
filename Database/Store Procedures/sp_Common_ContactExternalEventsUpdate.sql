SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_ContactExternalEventsUpdate]
(
	@Contact_ID int,
	@EmergencyContactName nvarchar(50),
	@EmergencyContactPhone nvarchar(20),
	@EmergencyContactRelationship nvarchar(30),
	@MedicalInformation nvarchar(200),
	@MedicalAllergies nvarchar(200),
	@AccessibilityInformation nvarchar(max) = '',
	@EventComments nvarchar(500),
	@User_ID int = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin

	SET NOCOUNT ON

	IF ((SELECT COUNT(*) FROM Common_ContactExternalEvents WHERE Contact_ID = @Contact_ID) = 0) BEGIN

		INSERT INTO [Common_ContactExternalEvents]
		(
			[Contact_ID],
			[EmergencyContactName],
			[EmergencyContactPhone],
			[EmergencyContactRelationship],
			[MedicalInformation],
			[MedicalAllergies],
			[AccessibilityInformation],
			[EventComments]
		)
		VALUES
		(
			@Contact_ID,
			@EmergencyContactName,
			@EmergencyContactPhone,
			@EmergencyContactRelationship,
			@MedicalInformation,
			@MedicalAllergies,
			@AccessibilityInformation,
			@EventComments
		)

	END
	ELSE BEGIN

		UPDATE [Common_ContactExternalEvents]
		SET
			[Contact_ID] = @Contact_ID,
			[EmergencyContactName] = @EmergencyContactName,
			[EmergencyContactPhone] = @EmergencyContactPhone,
			[EmergencyContactRelationship] = @EmergencyContactRelationship,
			[MedicalInformation] = @MedicalInformation,
			[MedicalAllergies] = @MedicalAllergies,
			[AccessibilityInformation] = @AccessibilityInformation,
			[EventComments] = @EventComments
		WHERE 
			[Contact_ID] = @Contact_ID
	
	END
GO
