SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_ContactExternalRead]

@Contact_ID		INT

AS


SELECT	*
FROM	Common_ContactExternal
WHERE	Contact_ID = @Contact_ID

SELECT	*
FROM	Common_ContactExternalEvents
WHERE	Contact_ID = @Contact_ID

SELECT	*
FROM	Common_ContactExternalRevolution
WHERE	Contact_ID = @Contact_ID

SELECT	*
FROM	External_ContactChurchInvolvement
WHERE	Contact_ID = @Contact_ID
GO
