SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_ContactExternalRevolutionRead]

@Contact_ID		INT	

AS

SELECT	*
FROM	Common_ContactExternalRevolution
WHERE	Contact_ID = @Contact_ID

SELECT	* 
FROM	Revolution_ContactSubscription
WHERE	Contact_ID = @Contact_ID

SELECT	SP.*
FROM	Revolution_SubscriptionPayment SP,
		Revolution_ContactSubscription CS
WHERE	SP.ContactSubscription_ID = CS.ContactSubscription_ID
AND		CS.Contact_ID = @Contact_ID
GO
