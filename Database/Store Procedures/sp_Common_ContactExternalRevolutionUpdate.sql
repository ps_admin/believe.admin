SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_ContactExternalRevolutionUpdate]
(
	@Contact_ID int,
	@ReferredBy_ID int,
	@AcceptedTermsAndConditions bit,
	@User_ID int = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin

	SET NOCOUNT ON

	IF ((SELECT COUNT(*) FROM Common_ContactExternalRevolution WHERE Contact_ID = @Contact_ID) = 0) BEGIN

		INSERT INTO [Common_ContactExternalRevolution]
		(
			[Contact_ID],
			[ReferredBy_ID],
			[AcceptedTermsAndConditions]
		)
		VALUES
		(
			@Contact_ID,
			@ReferredBy_ID,
			@AcceptedTermsAndConditions
		)

	END
	ELSE BEGIN

		UPDATE [Common_ContactExternalRevolution]
		SET
			[Contact_ID] = @Contact_ID,
			[ReferredBy_ID] = @ReferredBy_ID,
			[AcceptedTermsAndConditions] = @AcceptedTermsAndConditions
		WHERE 
			[Contact_ID] = @Contact_ID
	
	END
GO
