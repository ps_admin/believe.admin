SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_ContactExternalUpdate]
(
	@Contact_ID int,
	@ChurchName nvarchar(50),
	@Denomination_ID int,
	@SeniorPastorName nvarchar(50),
	@ChurchAddress1 nvarchar(100),
	@ChurchAddress2 nvarchar(100),
	@ChurchSuburb nvarchar(20),
	@ChurchPostcode nvarchar(10),
	@ChurchState_ID int = NULL,
	@ChurchStateOther nvarchar(20),
	@ChurchCountry_ID int = NULL,
	@ChurchWebsite nvarchar(50),
	@LeaderName nvarchar(60),
	@ChurchNumberOfPeople nvarchar(10),
	@ChurchPhone nvarchar(20),
	@ChurchEmail nvarchar(100),
	@ContactDetailsVerified bit,
	@Deleted bit,
	@Inactive bit,
	@User_ID int = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin

	SET NOCOUNT ON

	IF ((SELECT COUNT(*) FROM Common_ContactExternal WHERE Contact_ID = @Contact_ID) = 0) BEGIN

		INSERT INTO [Common_ContactExternal]
		(
			[Contact_ID],
			[ChurchName],
			[Denomination_ID],
			[SeniorPastorName],
			[ChurchAddress1],
			[ChurchAddress2],
			[ChurchSuburb],
			[ChurchPostcode],
			[ChurchState_ID],
			[ChurchStateOther],
			[ChurchCountry_ID],
			[ChurchWebsite],
			[LeaderName],
			[ChurchNumberOfPeople],
			[ChurchPhone],
			[ChurchEmail],
			[ContactDetailsVerified],
			[Deleted],
			[Inactive]
		)
		VALUES
		(
			@Contact_ID,
			@ChurchName,
			@Denomination_ID,
			@SeniorPastorName,
			@ChurchAddress1,
			@ChurchAddress2,
			@ChurchSuburb,
			@ChurchPostcode,
			@ChurchState_ID,
			@ChurchStateOther,
			@ChurchCountry_ID,
			@ChurchWebsite,
			@LeaderName,
			@ChurchNumberOfPeople,
			@ChurchPhone,
			@ChurchEmail,
			@ContactDetailsVerified,
			@Deleted,
			@Inactive
		)

	END
	ELSE BEGIN

		UPDATE [Common_ContactExternal]
		SET
			[Contact_ID] = @Contact_ID,
			[ChurchName] = @ChurchName,
			[Denomination_ID] = @Denomination_ID,
			[SeniorPastorName] = @SeniorPastorName,
			[ChurchAddress1] = @ChurchAddress1,
			[ChurchAddress2] = @ChurchAddress2,
			[ChurchSuburb] = @ChurchSuburb,
			[ChurchPostcode] = @ChurchPostcode,
			[ChurchState_ID] = @ChurchState_ID,
			[ChurchStateOther] = @ChurchStateOther,
			[ChurchCountry_ID] = @ChurchCountry_ID,
			[ChurchWebsite] = @ChurchWebsite,
			[LeaderName] = @LeaderName,
			[ChurchNumberOfPeople] = @ChurchNumberOfPeople,
			[ChurchPhone] = @ChurchPhone,
			[ChurchEmail] = @ChurchEmail,
			[ContactDetailsVerified] = @ContactDetailsVerified,
			[Deleted] = @Deleted,
			[Inactive] = @Inactive
		WHERE 
			[Contact_ID] = @Contact_ID
	
	END
GO
