SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_ContactHistoryRead]

@Contact_ID		INT,
@Module			NVARCHAR(20)

AS

SELECT		CH.*,
			ISNULL((SELECT [Name] FROM vw_Users WHERE Contact_ID = CH.User_ID),'') as ChangedBy
FROM		Common_ContactHistory CH
WHERE		Contact_ID = @Contact_ID
AND			(Module = 'Common'
OR			((Module = 'Events' OR Module = 'Revolution') AND (@Module = 'External' OR @Module = 'Both'))
OR			((Module = 'Church') AND (@Module = 'Internal' OR @Module = 'Both')))
GO
