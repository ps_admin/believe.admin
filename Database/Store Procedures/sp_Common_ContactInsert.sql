SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_ContactInsert]
(
	@Salutation_ID int,
	@FirstName nvarchar(30),
	@LastName nvarchar(30),
	@Gender nvarchar(6),
	@DateOfBirth smalldatetime = NULL,
	@Address1 nvarchar(100),
	@Address2 nvarchar(100),
	@Suburb nvarchar(20),
	@Postcode nvarchar(10),
	@State_ID int,
	@StateOther nvarchar(20),
	@Country_ID int,
	@Phone nvarchar(20),
	@PhoneWork nvarchar(20),
	@Fax nvarchar(20),
	@Mobile nvarchar(20),
	@Email nvarchar(80),
	@Email2 nvarchar(80) = '',
	@DoNotIncludeEmail1InMailingList bit = 0,
	@DoNotIncludeEmail2InMailingList bit = 0,
	@Family_ID int = null,
	@FamilyMemberType_ID int = null,
	@Password nvarchar(20),
	@VolunteerPoliceCheck nvarchar(20),
	@VolunteerPoliceCheckDate datetime = NULL,
	@VolunteerWWCC nvarchar(20),
	@VolunteerWWCCDate datetime = NULL,
	@VolunteerWWCCType_ID int = NULL,
	@CreationDate datetime,
	@CreatedBy_ID int,
	@ModificationDate datetime = NULL,
	@ModifiedBy_ID int,
	@GUID uniqueidentifier,
	@User_ID int = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin

	SET NOCOUNT ON

	INSERT INTO [Common_Contact]
	(
		[Salutation_ID],
		[FirstName],
		[LastName],
		[Gender],
		[DateOfBirth],
		[Address1],
		[Address2],
		[Suburb],
		[Postcode],
		[State_ID],
		[StateOther],
		[Country_ID],
		[Phone],
		[PhoneWork],
		[Fax],
		[Mobile],
		[Email],
		[Email2],
		[DoNotIncludeEmail1InMailingList],
		[DoNotIncludeEmail2InMailingList],
		[Family_ID],
		[FamilyMemberType_ID],
		[Password],
		[VolunteerPoliceCheck],
		[VolunteerPoliceCheckDate],
		[VolunteerWWCC],
		[VolunteerWWCCDate],
		[VolunteerWWCCType_ID],
		[CreationDate],
		[CreatedBy_ID],
		[ModificationDate],
		[ModifiedBy_ID],
		[GUID]
	)
	VALUES
	(
		@Salutation_ID,
		@FirstName,
		@LastName,
		@Gender,
		@DateOfBirth,
		@Address1,
		@Address2,
		@Suburb,
		@Postcode,
		@State_ID,
		@StateOther,
		@Country_ID,
		@Phone,
		@PhoneWork,
		@Fax,
		@Mobile,
		@Email,
		@Email2,
		@DoNotIncludeEmail1InMailingList,
		@DoNotIncludeEmail2InMailingList,
		@Family_ID,
		@FamilyMemberType_ID,
		@Password,
		@VolunteerPoliceCheck,
		@VolunteerPoliceCheckDate,
		@VolunteerWWCC,
		@VolunteerWWCCDate,
		@VolunteerWWCCType_ID,
		dbo.GetRelativeDate(),
		@CreatedBy_ID,
		@ModificationDate,
		@ModifiedBy_ID,
		@GUID
	)

	SELECT SCOPE_IDENTITY() AS Contact_ID
GO
