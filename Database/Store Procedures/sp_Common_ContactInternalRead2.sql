SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_ContactInternalRead2]

@Contact_ID		INT

AS

SELECT	*
FROM	Common_ContactInternal
WHERE	Contact_ID = @Contact_ID

SELECT	*
FROM	Church_ChurchStatusChangeRequest
WHERE	Contact_ID = @Contact_ID
AND		Actioned = 0

SELECT	*
FROM	Church_NPNC
WHERE	Contact_ID = @Contact_ID

SELECT	*
FROM	Church_ULGContact
WHERE	Contact_ID = @Contact_ID

SELECT	*
FROM	Church_ContactAdditionalDecision
WHERE	Contact_ID = @Contact_ID

SELECT	CR.*
FROM	Church_ContactRole CR,
		Church_Role R
WHERE	CR.Role_ID = R.Role_ID
AND		Contact_ID = @Contact_ID

SELECT	RAR.*
FROM	Church_RestrictedAccessRole RAR,
		Church_ContactRole CR
WHERE	RAR.ContactRole_ID = CR.ContactRole_ID
AND		Contact_ID = @Contact_ID

SELECT	RAU.*
FROM	Church_RestrictedAccessULG RAU,
		Church_ContactRole CR
WHERE	RAU.ContactRole_ID = CR.ContactRole_ID
AND		Contact_ID = @Contact_ID

SELECT	*
FROM	Church_ContactCourseInstance
WHERE	Contact_ID = @Contact_ID

SELECT	*
FROM	Church_ContactCourseEnrolment
WHERE	Contact_ID = @Contact_ID

SELECT	CEA.*
FROM	Church_CourseEnrolmentAttendance CEA
WHERE	CEA.Contact_ID = @Contact_ID

SELECT	TC.*
FROM	Church_CourseEnrolmentAttendance CEA,
		Church_CourseEnrolmentAttendanceTopicsCovered TC
WHERE	CEA.CourseEnrolmentAttendance_ID = TC.CourseEnrolmentAttendance_ID
AND		CEA.Contact_ID = @Contact_ID

SELECT	*
FROM	Church_ContactComment
WHERE	Contact_ID = @Contact_ID

SELECT	*
FROM	Church_247Prayer
WHERE	Contact_ID = @Contact_ID

SELECT	*
FROM	Church_ContactKidsMinistryCode
WHERE	Contact_ID = @Contact_ID

SELECT	*
FROM	Church_KidsMinistryClearance
WHERE	Contact_ID = @Contact_ID
GO
