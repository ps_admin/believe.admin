SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_ContactInternalUpdate]
(
	@Contact_ID int,
	@ChurchStatus_ID int,
	@Campus_ID INT = 1,
	@Ministry_ID int = NULL,
	@Region_ID int = NULL,
	@PostalAddress1 nvarchar(100),
	@PostalAddress2 nvarchar(100),
	@PostalSuburb nvarchar(20),
	@PostalPostcode nvarchar(50),
	@PostalState_ID int = NULL,
	@PostalStateOther nvarchar(20),
	@PostalCountry_ID int = NULL,
	@Photo image = NULL,
	@EntryPoint_ID int = NULL,
	@DateJoinedChurch datetime = NULL,
	@CovenantFormDate datetime = NULL,
	@WaterBaptismDate datetime = NULL,
	@VolunteerFormDate datetime = NULL,
	@PrimaryCarer nvarchar(50) = '',
	@CountryOfOrigin_ID int = NULL,
	@PrivacyNumber nvarchar(50) = '',
	@PrimarySchool nvarchar(100) = '',
	@SchoolGrade_ID int = null,
	@HighSchool nvarchar(100),
	@University nvarchar(100),
	@InformationIsConfidential bit = 0,
	@FacebookAddress nvarchar(100) = '',
	@Education_ID int = NULL,
	@ModeOfTransport_ID int = NULL,
	@CarParkUsed_ID int = NULL,
	@ServiceAttending_ID int = NULL,
	@Occupation nvarchar(100),
	@Employer nvarchar(50),
	@EmploymentStatus_ID int = NULL,
	@EmploymentPosition_ID int = NULL,
	@EmploymentIndustry_ID int = NULL,
	@KidsMinistryComments NVARCHAR(MAX) = '',
	@KidsMinistryGroup NVARCHAR(50) = '',
	@Deleted bit,
	@DeleteReason_ID int = NULL,
	@User_ID int = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin

	SET NOCOUNT ON

	IF ((SELECT COUNT(*) FROM Common_ContactInternal WHERE Contact_ID = @Contact_ID) = 0) BEGIN

		INSERT INTO [Common_ContactInternal]
		(
			[Contact_ID],
			[ChurchStatus_ID],
			[Campus_ID],
			[Ministry_ID],
			[Region_ID],
			[PostalAddress1],
			[PostalAddress2],
			[PostalSuburb],
			[PostalPostcode],
			[PostalState_ID],
			[PostalStateOther],
			[PostalCountry_ID],
			[Photo],
			[EntryPoint_ID],
			[DateJoinedChurch],
			[CovenantFormDate],
			[WaterBaptismDate],
			[VolunteerFormDate],
			[PrimaryCarer],
			[CountryOfOrigin_ID],
			[PrivacyNumber],
			[PrimarySchool],
			[SchoolGrade_ID],
			[HighSchool],
			[University],
			[InformationIsConfidential],
			[FacebookAddress],
			[Education_ID],
			[ModeOfTransport_ID],
			[CarParkUsed_ID],
			[ServiceAttending_ID],
			[Occupation],
			[Employer],
			[EmploymentStatus_ID],
			[EmploymentPosition_ID],
			[EmploymentIndustry_ID],
			[KidsMinistryComments],
			[KidsMinistryGroup],
			[Deleted],
			[DeleteReason_ID]
		)
		VALUES
		(
			@Contact_ID,
			@ChurchStatus_ID,
			@Campus_ID,
			@Ministry_ID,
			@Region_ID,
			@PostalAddress1,
			@PostalAddress2,
			@PostalSuburb,
			@PostalPostcode,
			@PostalState_ID,
			@PostalStateOther,
			@PostalCountry_ID,
			@Photo,
			@EntryPoint_ID,
			@DateJoinedChurch,
			@CovenantFormDate,
			@WaterBaptismDate,
			@VolunteerFormDate,
			@PrimaryCarer,
			@CountryOfOrigin_ID,
			@PrivacyNumber,
			@PrimarySchool,
			@SchoolGrade_ID,
			@HighSchool,
			@University,
			@InformationIsConfidential,
			@FacebookAddress,
			@Education_ID,
			@ModeOfTransport_ID,
			@CarParkUsed_ID,
			@ServiceAttending_ID,
			@Occupation,
			@Employer,
			@EmploymentStatus_ID,
			@EmploymentPosition_ID,
			@EmploymentIndustry_ID,
			@KidsMinistryComments,
			@KidsMinistryGroup,
			@Deleted,
			@DeleteReason_ID
		)

	END
	ELSE BEGIN

		UPDATE [Common_ContactInternal]
		SET
			[ChurchStatus_ID] = @ChurchStatus_ID,
			[Campus_ID] = @Campus_ID,
			[Ministry_ID] = @Ministry_ID,
			[Region_ID] = @Region_ID,
			[PostalAddress1] = @PostalAddress1,
			[PostalAddress2] = @PostalAddress2,
			[PostalSuburb] = @PostalSuburb,
			[PostalPostcode] = @PostalPostcode,
			[PostalState_ID] = @PostalState_ID,
			[PostalStateOther] = @PostalStateOther,
			[PostalCountry_ID] = @PostalCountry_ID,
			[Photo] = @Photo,
			[EntryPoint_ID] = @EntryPoint_ID,
			[DateJoinedChurch] = @DateJoinedChurch,
			[CovenantFormDate] = @CovenantFormDate,
			[WaterBaptismDate] = @WaterBaptismDate,
			[VolunteerFormDate] = @VolunteerFormDate,
			[PrimaryCarer] = @PrimaryCarer,
			[CountryOfOrigin_ID] = @CountryOfOrigin_ID,
			[PrivacyNumber] = @PrivacyNumber,
			[PrimarySchool] = @PrimarySchool,
			[SchoolGrade_ID] = @SchoolGrade_ID,
			[HighSchool] = @HighSchool,
			[University] = @University,
			[InformationIsConfidential] = @InformationIsConfidential,
			[FacebookAddress] = @FacebookAddress,
			[Education_ID] = @Education_ID,
			[ModeOfTransport_ID] = @ModeOfTransport_ID,
			[CarParkUsed_ID] = @CarParkUsed_ID,
			[ServiceAttending_ID] = @ServiceAttending_ID,
			[Occupation] = @Occupation,
			[Employer] = @Employer,
			[EmploymentStatus_ID] = @EmploymentStatus_ID,
			[EmploymentPosition_ID] = @EmploymentPosition_ID,
			[EmploymentIndustry_ID] = @EmploymentIndustry_ID,
			[KidsMinistryComments] = @KidsMinistryComments,
			[KidsMinistryGroup] = @KidsMinistryGroup,
			[Deleted] = @Deleted,
			[DeleteReason_ID] = @DeleteReason_ID
		WHERE 
			[Contact_ID] = @Contact_ID

	END
GO
