SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_ContactInternalUpdatePostalAddress]
(
	@Contact_ID int,
	@PostalAddress1 nvarchar(100),
	@PostalAddress2 nvarchar(100),
	@PostalSuburb nvarchar(20),
	@PostalPostcode nvarchar(50),
	@PostalState_ID int = NULL,
	@PostalStateOther nvarchar(20),
	@PostalCountry_ID int = NULL,
	@ModifiedBy_ID int
)
AS
	SET NOCOUNT ON
	
	UPDATE [Common_ContactInternal]
	SET
		[PostalAddress1] = @PostalAddress1,
		[PostalAddress2] = @PostalAddress2,
		[PostalSuburb] = @PostalSuburb,
		[PostalPostcode] = @PostalPostcode,
		[PostalState_ID] = @PostalState_ID,
		[PostalStateOther] = @PostalStateOther,
		[PostalCountry_ID] = @PostalCountry_ID
	WHERE 
		[Contact_ID] = @Contact_ID
		
		
	UPDATE [Common_Contact]
	SET
		[ModificationDate] = dbo.GetRelativeDate(),
		[ModifiedBy_ID] = @ModifiedBy_ID
	WHERE 
		[Contact_ID] = @Contact_ID
GO
