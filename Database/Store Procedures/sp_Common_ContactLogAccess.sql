SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_ContactLogAccess]

@Contact_ID		INT

AS

-- Log in the history file that this contact has been read.
-- Don't log again if the last entry in the log is "record accessed."

DECLARE	@User_ID INT
SET @User_ID = COALESCE(CONVERT(INT, SUBSTRING(Context_Info(),1,4)), 100) -- Default to Administrator

IF @User_ID > 1000 
	IF @Contact_ID > 0
		IF COALESCE((SELECT TOP 1 DateChanged FROM Common_ContactHistory WHERE User_ID = @User_ID AND Desc2 = 'Record Accessed' AND Contact_ID = @Contact_ID ORDER BY DateChanged DESC), DATEADD(mi, -16, dbo.GetRelativeDate())) < DATEADD(mi, -15, dbo.GetRelativeDate())
			INSERT	Common_ContactHistory (Contact_ID, Module, [Desc], Desc2, DateChanged, User_ID, [Action], Item, [Table], FieldName, OldValue, OldText, NewValue, NewText)
			SELECT	@Contact_ID, 'Common', 'Contact Details', 'Record Accessed', dbo.GetRelativeDate(), @User_ID, 
					'', '', 'Common_Contact', '', '', '', '', ''
GO
