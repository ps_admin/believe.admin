SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_ContactMailingListInsert]
(
	@Contact_ID int,
	@MailingList_ID int,
	@SubscriptionActive bit,
	@User_ID int = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin

	SET NOCOUNT ON

	INSERT INTO [Common_ContactMailingList]
	(
		[Contact_ID],
		[MailingList_ID],
		[SubscriptionActive]
	)
	VALUES
	(
		@Contact_ID,
		@MailingList_ID,
		@SubscriptionActive
	)

	SELECT ContactMailingList_ID = SCOPE_IDENTITY();
GO
