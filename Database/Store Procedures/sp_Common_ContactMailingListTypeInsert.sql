SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_ContactMailingListTypeInsert]
(
	@Contact_ID int,
	@MailingListType_ID int,
	@SubscriptionActive bit,
	@User_ID int = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin

	SET NOCOUNT ON

	INSERT INTO [Common_ContactMailingListType]
	(
		[Contact_ID],
		[MailingListType_ID],
		[SubscriptionActive]
	)
	VALUES
	(
		@Contact_ID,
		@MailingListType_ID,
		@SubscriptionActive
	)

	SELECT ContactMailingListType_ID = SCOPE_IDENTITY();
GO
