SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_ContactMailingListTypeUpdate]
(
	@ContactMailingListType_ID int,
	@Contact_ID int,
	@MailingListType_ID int,
	@SubscriptionActive bit,
	@User_ID int = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin

	SET NOCOUNT ON
	
	UPDATE [Common_ContactMailingListType]
	SET
		[Contact_ID] = @Contact_ID,
		[MailingListType_ID] = @MailingListType_ID,
		[SubscriptionActive] = @SubscriptionActive
	WHERE 
		[ContactMailingListType_ID] = @ContactMailingListType_ID
GO
