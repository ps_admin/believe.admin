SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_ContactMailingListUpdate]
(
	@ContactMailingList_ID int = 0,
	@Contact_ID int,
	@MailingList_ID int,
	@SubscriptionActive bit,
	@User_ID int = null
)
AS
	DECLARE @Count INT = 0

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin

	SET NOCOUNT ON
	
	SELECT @Count = COUNT(*)
	FROM [Common_ContactMailingList]
	WHERE 
		[Contact_ID] = @Contact_ID AND
		[MailingList_ID] = @MailingList_ID
		
	IF @Count > 0 
		UPDATE [Common_ContactMailingList]
		SET
			[SubscriptionActive] = @SubscriptionActive
		WHERE 
			[Contact_ID] = @Contact_ID AND
			[MailingList_ID] = @MailingList_ID
	ELSE
		INSERT INTO [Common_ContactMailingList]([Contact_ID], [MailingList_ID], [SubscriptionActive])
		VALUES(@Contact_ID, @MailingList_ID, @SubscriptionActive)
GO
