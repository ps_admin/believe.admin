SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_ContactOldPasswordCheck]

@Contact_ID	int,
@Password	nvarchar(20),
@Number		int

AS

DECLARE @SQL nvarchar(max)

SET @SQL = 'SELECT		TOP ' + CONVERT(NVARCHAR, @Number) + ' *
			INTO		#t1
			FROM		Common_ContactOldPassword
			WHERE		Contact_ID = ' + CONVERT(NVARCHAR, @Contact_ID) + '
			ORDER BY	DateChanged DESC
			
			SELECT		COUNT(*)
			FROM		#t1
			WHERE		Password = ''' + @Password + ''''
			
EXEC(@SQL)
GO
