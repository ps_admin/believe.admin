SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_ContactOldPasswordInsert]

@Contact_ID	int,
@DateChanged datetime,
@Password nvarchar(20)

AS

INSERT Common_ContactOldPassword
(
	Contact_ID,
	DateChanged,
	Password
)
VALUES
(
	@Contact_ID,
	@DateChanged,
	@Password
)

SELECT SCOPE_IDENTITY() as OldPassword_ID
GO
