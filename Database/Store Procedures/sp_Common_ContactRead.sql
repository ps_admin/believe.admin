SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_ContactRead]

@Contact_ID		INT

AS

SELECT	*
FROM	Common_Contact
WHERE	Contact_ID = @Contact_ID


IF (@Contact_ID > 0 )
	INSERT Common_ContactMailingList(Contact_ID, MailingList_ID, SubscriptionActive)	
	SELECT	@Contact_ID, MailingList_ID, 0
	FROM	Common_MailingList ML
	WHERE NOT EXISTS (	SELECT	MailingList_ID
						FROM	Common_ContactMailingList 
						WHERE	Contact_ID = @Contact_ID
						AND		MailingList_ID = ML.MailingList_ID)

IF (@Contact_ID > 0 )
	INSERT Common_ContactMailingListType(Contact_ID, MailingListType_ID, SubscriptionActive)	
	SELECT	@Contact_ID, MailingListType_ID, 0
	FROM	Common_MailingListType MLT, Common_Contact CC
	WHERE	CC.Contact_ID = @Contact_ID
	AND		NOT EXISTS (	SELECT	MailingListType_ID
							FROM	Common_ContactMailingListType
							WHERE	Contact_ID = CC.Contact_ID 
							AND		MailingListType_ID = MLT.MailingListType_ID)

SELECT	*
FROM	Common_ContactMailingList
WHERE	Contact_ID = @Contact_ID

SELECT	*
FROM	Common_ContactMailingListType
WHERE	Contact_ID = @Contact_ID

SELECT	@Contact_ID as Contact_ID,
		C.Family_ID,
		C.Contact_ID as FamilyMember_ID, FamilyMemberType_ID,
		FirstName + ' ' + LastName as Name, 
		ISNULL((SELECT Name FROM Church_Campus Ca, Common_ContactInternal CI WHERE Ca.Campus_ID = CI.Campus_ID AND CI.Contact_ID = C.Contact_ID),'') as ChurchCampus,
		ISNULL((SELECT Name FROM Church_ChurchStatus CS, Common_ContactInternal CI WHERE CS.ChurchStatus_ID = CI.ChurchStatus_ID AND CI.Contact_ID = C.Contact_ID),'') as ChurchStatus,
		ISNULL(CONVERT(NVARCHAR, dbo.Common_GetAge(C.DateOfBirth)),'') as Age,
		A.[StreetAddress], A.PostalAddress, C.Phone, C.Mobile, C.Email, A.InternalExternal
FROM	Common_Contact C,
		vw_Common_ContactAddressView A
WHERE	C.Contact_ID = A.Contact_ID
AND		(Family_ID = @Contact_ID
OR		Family_ID IN (SELECT Family_ID FROM Common_Contact WHERE Contact_ID = @Contact_ID)
OR		C.Contact_ID = (SELECT Family_ID FROM Common_Contact WHERE Contact_ID = @Contact_ID))
AND		C.Contact_ID <> @Contact_ID

SELECT	*
FROM	vw_Users
WHERE	Contact_ID = @Contact_ID

SELECT	D.* 
FROM	Common_ContactDatabaseRole D
WHERE	D.Contact_ID = @Contact_ID
GO
