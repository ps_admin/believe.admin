SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_ContactReadByEmail] 

@Email		NVARCHAR(80)

AS

SELECT		TOP 1 Contact_ID
FROM		Common_Contact
WHERE		Email = @Email
GO
