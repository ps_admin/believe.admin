SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_ContactSearch2]

@SearchBase					INT = 0,
@Name						NVARCHAR(40) = '',
@QuickSearch				NVARCHAR(40) = '',
@FirstName					NVARCHAR(20) = '',
@LastName					NVARCHAR(20) = '',
@ChurchName					NVARCHAR(50) = '',
@ContactNum					NVARCHAR(10) = '',
@Address					NVARCHAR(50) = '',
@Suburb						NVARCHAR(30) = '',
@Postcode					NVARCHAR(20) = '',
@Country_ID					INT = 0,
@State_ID					INT = 0,
@Gender						NVARCHAR(10) = '',
@Email						NVARCHAR(80) = '',
@EmergencyContact			NVARCHAR(30) = '',
@Phone 						NVARCHAR(20) = '',
@Fax						NVARCHAR(20) = '',
@Mobile						NVARCHAR(20) = '',
@LeaderName					NVARCHAR(30) = '',
@Subscription_ID			INT = NULL,
@SignupDateStart			NVARCHAR(20) = '',
@SignupDateEnd				NVARCHAR(20) = '',
@User_ID					INT

AS

DECLARE @SQL			VARCHAR(8000)
DECLARE @SQLSelect		VARCHAR(8000)
DECLARE @SQLFrom		VARCHAR(8000)
DECLARE @SQLWhere		VARCHAR(8000)
DECLARE	@Campus_ID	INT

SELECT @Campus_ID = Campus_ID FROM Common_ContactInternal WHERE Contact_ID = @User_ID

SELECT		CONVERT(int,C.Contact_ID) as Contact_ID,
			Sa.Salutation as Title,
			C.FirstName, 
			C.LastName,
			C.Address1,
			C.Suburb,
			C.Postcode,
			S.State,
			C.Phone,
			C.Fax,
			C.Mobile,
			C.Email,
			C.Gender,
			C.DateOfBirth,
			C.CreationDate,
			C.ModificationDate,
			CONVERT(BIT, 0) as ActiveExternal,
			CONVERT(BIT, 0) as ActiveInternal,
			CONVERT(NVARCHAR(50), '') as Campus,
			CONVERT(NVARCHAR(50), '') as ChurchStatus,
			CONVERT(INT, 0) as ChurchStatus_ID,
			CONVERT(BIT, 0) as AccessExternal,
			CONVERT(BIT, 0) as AccessInternal,
			CONVERT(BIT, 0) as InformationIsConfidential
INTO		#t1
FROM 		Common_Contact C, Common_GeneralState S, Common_Salutation Sa
WHERE 		C.State_ID = S.State_ID
AND			C.Salutation_ID = Sa.Salutation_ID
AND			1=0

/********************************
*       SQL Select String		*
*********************************/
SET @SQLSelect = '	
INSERT INTO #t1
SELECT	DISTINCT 
		C.Contact_ID,
		Sa.Salutation as Title,
		C.FirstName, 
		C.LastName,
		C.Address1,
		C.Suburb,
		C.Postcode,
		ISNULL((SELECT State FROM Common_GeneralState WHERE State_ID = C.State_ID),'''') as State,
		C.Phone,
		C.Fax,
		C.Mobile,
		C.Email,
		C.Gender,
		C.DateOfBirth,
		C.CreationDate,
		C.ModificationDate,
		CONVERT(BIT, (SELECT COUNT(*) FROM Common_ContactExternal WHERE Contact_ID = C.Contact_ID)),
		CONVERT(BIT, (SELECT COUNT(*) FROM Common_ContactInternal WHERE Contact_ID = C.Contact_ID)),
		ISNULL((SELECT Ca.[Name] FROM Common_ContactInternal CI, Church_Campus Ca WHERE CI.Campus_ID = Ca.Campus_ID AND CI.Contact_ID = C.Contact_ID),''''),
		ISNULL((SELECT CS.[Name] FROM Common_ContactInternal CI, Church_ChurchStatus CS WHERE CI.ChurchStatus_ID = CS.ChurchStatus_ID AND CI.Contact_ID = C.Contact_ID),''''),
		ISNULL((SELECT ChurchStatus_ID FROM Common_ContactInternal WHERE Contact_ID = C.Contact_ID),0),
		0,
		0,
		0'
SET		@SQLFrom = 'FROM 	Common_Contact C, Common_Salutation Sa'
SET		@SQLWhere = 'WHERE	C.Salutation_ID = Sa.Salutation_ID' + CHAR(10)

/********************************
*       SQL From String			*
*********************************/

IF @SearchBase = 1 BEGIN
	SET @SQLFrom = @SQLFrom + ', Common_ContactInternal CI'
	SET @SQLWhere = @SQLWhere + ' AND C.Contact_ID = CI.Contact_ID'
END

IF @SearchBase = 2 OR @ChurchName <> '' OR @EmergencyContact <> '' OR @LeaderName <> '' BEGIN
	SET @SQLFrom = @SQLFrom + ', Common_ContactExternal CE'
	SET @SQLWhere = @SQLWhere + ' AND C.Contact_ID = CE.Contact_ID AND CE.Deleted = 0 '
END

IF @Subscription_ID IS NOT NULL OR @SignupDateStart <> '' OR @SignupDateEnd <> '' BEGIN
	SET @SQLFrom = @SQLFrom + ', Revolution_ContactSubscription CS'
	SET @SQLWhere = @SQLWhere + ' AND C.Contact_ID = CS.Contact_ID'
END

/********************************
*       SQL Where String		*
*********************************/

--Contact Options
IF (@Name <> '') BEGIN

	DECLARE @Text NVARCHAR(1000)

	DECLARE NameCursor CURSOR FOR 
		SELECT strval FROM dbo.SplitString(@Name,' ')

	OPEN NameCursor
		
	SET @SQLWhere = @SQLWhere + '	AND ('
	FETCH NEXT FROM NameCursor INTO @Text
	WHILE (@@Fetch_Status <> -1) BEGIN
			SET @SQLWhere = @SQLWhere + '(C.FirstName LIKE ''%' + @Text + '%'' OR C.LastName LIKE ''%' + @Text + '%'') AND'
		FETCH NEXT FROM NameCursor INTO @Text	
	END
	SET @SQLWhere = LEFT(@SQLWhere, LEN(@SQLWhere) - 4) + ')'

	Close NameCursor
	Deallocate NameCursor

END

IF (@QuickSearch <> '') BEGIN

	DECLARE @Text2 NVARCHAR(1000)

	DECLARE QuickSearchCursor CURSOR FOR 
		SELECT strval FROM dbo.SplitString(@QuickSearch,' ')

	OPEN QuickSearchCursor
		
	SET @SQLWhere = @SQLWhere + '	AND ('
	FETCH NEXT FROM QuickSearchCursor INTO @Text2
	WHILE (@@Fetch_Status <> -1) BEGIN
			SET @SQLWhere = @SQLWhere + '(C.FirstName LIKE ''%' + @Text2 + '%'' OR C.LastName LIKE ''%' + @Text2 + '%'' OR CONVERT(NVARCHAR,C.Contact_ID) LIKE ''%' + @Text2 + '%'' OR C.Mobile LIKE ''%' + @Text2 + '%'' OR C.Email LIKE ''%' + @Text2 + '%'' OR C.Email2 LIKE ''%' + @Text2 + '%'') AND'
		FETCH NEXT FROM QuickSearchCursor INTO @Text2
	END
	SET @SQLWhere = LEFT(@SQLWhere, LEN(@SQLWhere) - 4) + ')'

	Close QuickSearchCursor
	Deallocate QuickSearchCursor

END


IF @FirstName <> '' 						SET @SQLWhere = @SQLWhere + '	AND	(C.FirstName LIKE ''%' + @FirstName + '%'')' + CHAR(10)
IF @LastName <> ''  						SET @SQLWhere = @SQLWhere + '	AND	(C.LastName LIKE ''%' + @LastName +'%'')' + CHAR(10)
IF @ChurchName <> '' 						SET @SQLWhere = @SQLWhere + '	AND	(CE.ChurchName LIKE ''%' + @ChurchName +'%'')' + CHAR(10)
IF @ContactNum <> '' 						SET @SQLWhere = @SQLWhere + '	AND	C.Contact_ID = ' + @ContactNum + CHAR(10)
IF @Address <> '' 							SET @SQLWhere = @SQLWhere + '	AND	(C.Address1 LIKE ''%' + @Address + '%'' OR C.Address2 LIKE ''%' + @Address + '%'')' + CHAR(10)
IF @Suburb <> '' 							SET @SQLWhere = @SQLWhere + '	AND	(C.Suburb LIKE ''%' + @Suburb +'%'')' + CHAR(10)
IF @Postcode <> '' 							SET @SQLWhere = @SQLWhere + '	AND	(C.Postcode LIKE ''%' + @Postcode +'%'')' + CHAR(10)
IF @Country_ID > 0							SET @SQLWhere = @SQLWhere + '	AND	(C.Country_ID = ' + CONVERT(VARCHAR,@Country_ID)+')' + CHAR(10)
IF @State_ID > 0		 					SET @SQLWhere = @SQLWhere + '	AND	(C.State_ID = ' + CONVERT(VARCHAR,@State_ID) + ')' + CHAR(10)
IF @Gender <> '' 							SET @SQLWhere = @SQLWhere + '	AND	(C.Gender =''' + @Gender +''')' + CHAR(10)
IF @Email <> '' 							SET @SQLWhere = @SQLWhere + '	AND	(C.Email LIKE ''%' + replace(@Email,'_','[_]') + '%'' OR C.Email2 LIKE ''%' + replace(@Email,'_','[_]') + '%'')' + CHAR(10)
IF @EmergencyContact <> '' 					SET @SQLWhere = @SQLWhere + '	AND	(CE.EmergencyContactName LIKE ''' + @EmergencyContact +''')' + CHAR(10)
IF @Phone  <> '' 							SET @SQLWhere = @SQLWhere + '	AND	(C.Phone LIKE ''%' + @Phone +'%'')' + CHAR(10)
IF @Fax <> '' 								SET @SQLWhere = @SQLWhere + '	AND	(C.Fax LIKE ''%' + @Fax +'%'')' + CHAR(10)
IF @Mobile <> '' 							SET @SQLWhere = @SQLWhere + '	AND	(C.Mobile LIKE ''%' + @Mobile +'%'')' + CHAR(10)
IF @LeaderName <> '' 						SET @SQLWhere = @SQLWhere + '	AND	(CE.LeaderName LIKE ''%' + @LeaderName +'%'')' + CHAR(10)

-- Revolution Options
IF @Subscription_ID IS NOT NULL				SET @SQLWHERE = @SQLWhere + '	AND (CS.Subscription_ID = ' + CONVERT(NVARCHAR,@Subscription_ID) + ')'
IF @SignupDateStart <> ''					SET @SQLWHERE = @SQLWhere + '	AND (CS.SignupDate >= ''' + CONVERT(NVARCHAR,@SignupDateStart) + ''')'
IF @SignupDateEnd <> ''						SET @SQLWHERE = @SQLWhere + '	AND (CS.SignupDate < ''' + CONVERT(NVARCHAR,DATEADD(dd,1,@SignupDateEnd)) + ''')'

SET @SQL = @SQLSelect + CHAR(10) + @SQLFrom + CHAR(10) + @SQLWhere

EXEC(@SQL)


DECLARE	@PermissionContactDetailsExternal INT = dbo.Common_GetDatabasePermission('CONTACT_DETAILS_EXTERNAL', @User_ID),
		@PermissionContactDetailsInternalFull INT = dbo.Common_GetDatabasePermission('CONTACT_DETAILS_INTERNAL_FULL', @User_ID),
		@PermissionCampusViewAll INT = dbo.Common_GetDatabasePermission('CAMPUS_VIEW_ALL', @User_ID),
		@PermissionContactDetailsInternalRestricted INT = dbo.Common_GetDatabasePermission('CONTACT_DETAILS_INTERNAL_RESTRICTED', @User_ID),
		@PermissionContactDetailsInternalConfidential INT = dbo.Common_GetDatabasePermission('CONTACT_DETAILS_INTERNAL_CONFIDENTIAL', @User_ID)
		
UPDATE	#t1
SET		AccessExternal = 1
WHERE	Contact_ID IN (
				SELECT	Contact_ID FROM Common_ContactExternal
				WHERE	@PermissionContactDetailsExternal = 1)

UPDATE	#t1
SET		AccessInternal = 1
WHERE	Contact_ID IN (
				SELECT	Contact_ID FROM Common_ContactInternal
				WHERE	InformationIsConfidential = 0 
				AND		@PermissionContactDetailsInternalFull = 1
				AND		(Campus_ID = @Campus_ID OR @PermissionCampusViewAll = 1))
				
OR		Contact_ID IN (
				SELECT	Contact_ID FROM Common_ContactInternal
				WHERE	InformationIsConfidential = 0 
				AND		@PermissionContactDetailsInternalRestricted = 1
				AND		(Campus_ID = @Campus_ID OR @PermissionCampusViewAll = 1)
				AND		(Contact_ID IN (SELECT	CR.Contact_ID 
									FROM	Church_ContactRole CR,
											Church_Role R,
											Church_RestrictedAccessRole RAR,
											Church_ContactRole CR2
									WHERE	R.Role_ID = CR.Role_ID
									AND		RAR.Role_Id = R.Role_ID
									AND		CR2.ContactRole_ID = RAR.ContactRole_ID
									AND		CR2.Contact_ID = @User_ID)
				OR Contact_ID IN  (SELECT	UC.Contact_ID 
									FROM	Church_ContactRole CR,
											Church_RestrictedAccessULG RAU,
											Church_ULGContact UC
									WHERE	CR.ContactRole_ID = RAU.ContactRole_ID
									AND		RAU.ULG_ID = UC.ULG_ID
									AND		UC.InActive=0
									AND		CR.Contact_ID = @User_ID)))
OR		Contact_ID IN (	
				SELECT	Contact_ID FROM Common_ContactInternal
				WHERE	InformationIsConfidential = 1
				AND		@PermissionContactDetailsInternalConfidential = 1
				AND		(Campus_ID = @Campus_ID OR @PermissionCampusViewAll = 1))

--Only flag a record as 'Confidential' if the logged-in user doesn't have permission to see it.
UPDATE	#t1
SET		InformationIsConfidential = CI.InformationIsConfidential,
		AccessExternal = 0,
		AccessInternal = 0,
		Address1 = 'Confidential',
		Suburb = 'Confidential',
		Postcode = '',
		[State] = '',
		Phone = 'Confidential',
		Fax = '',
		Mobile = 'Confidential',
		Email = '',
		DateOfBirth = null
FROM	Common_ContactInternal CI
WHERE	CI.Contact_ID = #t1.Contact_ID
AND		CI.InformationIsConfidential = 1
AND		@PermissionContactDetailsInternalConfidential = 0

SELECT	*
FROM	#t1
GO
