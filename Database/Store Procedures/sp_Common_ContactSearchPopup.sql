SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_ContactSearchPopup]

@Contact_ID					INT,
@Name						NVARCHAR(40) = '',
@Ministry_ID				INT

AS

DECLARE @SQL			VARCHAR(8000)
DECLARE @SQLSelect		VARCHAR(8000)
DECLARE @SQLWhere		VARCHAR(8000)

SELECT	C.Contact_ID,  
		C.FirstName, 
		C.LastName, 
		FirstName + ' ' + LastName as Name, 
		ISNULL((SELECT Name FROM Church_ChurchStatus CS, Common_ContactInternal CI WHERE CS.ChurchStatus_ID = CI.ChurchStatus_ID AND CI.Contact_ID = C.Contact_ID),'') as ChurchStatus,
		A.StreetAddress,
		A.PostalAddress,
		C.Phone, 
		C.Mobile, 
		C.Email
INTO	#t1
FROM	Common_Contact C,
		vw_Common_ContactAddressView A
WHERE	C.Contact_ID = A.Contact_ID
AND		1=0


/********************************
*       SQL Select String		*
*********************************/
SET @SQLSelect = '	
INSERT INTO #t1
SELECT	C.Contact_ID, 
		C.FirstName, 
		C.LastName, 
		FirstName + '' '' + LastName as Name, 
		ISNULL((SELECT Name FROM Church_ChurchStatus CS, Common_ContactInternal CI WHERE CS.ChurchStatus_ID = CI.ChurchStatus_ID AND CI.Contact_ID = C.Contact_ID),'''') as ChurchStatus,
		A.StreetAddress,
		A.PostalAddress,
		C.Phone, 
		C.Mobile, 
		C.Email
FROM	Common_Contact C,
		vw_Common_ContactAddressView A
WHERE	C.Contact_ID = A.Contact_ID'

IF	@Contact_ID > 0 SET @SQLSelect = @SQLSelect + CHAR(10) + 'AND		C.Contact_ID <> ' + CONVERT(NVARCHAR, @Contact_ID) 
IF	@Ministry_ID > 0 SET @SQLSelect = @SQLSelect + CHAR(10) + 'AND		C.Contact_ID IN (SELECT Contact_ID FROM Common_ContactInternal WHERE Ministry_ID = ' + CONVERT(NVARCHAR,@Ministry_ID) + ')'

		
SET @SQLWhere = ''

IF (@Name <> '') BEGIN

	DECLARE @Text2 NVARCHAR(1000)

	DECLARE QuickSearchCursor CURSOR FOR 
		SELECT strval FROM dbo.SplitString(@Name,' ')

	OPEN QuickSearchCursor
		
	SET @SQLWhere = @SQLWhere + 'AND		('
	FETCH NEXT FROM QuickSearchCursor INTO @Text2
	WHILE (@@Fetch_Status <> -1) BEGIN
			SET @SQLWhere = @SQLWhere + '(C.FirstName LIKE ''%' + @Text2 + '%'' OR C.LastName LIKE ''%' + @Text2 + '%'' OR CONVERT(NVARCHAR,C.Contact_ID) LIKE ''%' + @Text2 + '%'' OR C.Mobile LIKE ''%' + @Text2 + '%'' OR C.Email LIKE ''%' + @Text2 + '%'' OR C.Email2 LIKE ''%' + @Text2 + '%'') AND'
		FETCH NEXT FROM QuickSearchCursor INTO @Text2
	END
	SET @SQLWhere = LEFT(@SQLWhere, LEN(@SQLWhere) - 4) + ')'

	Close QuickSearchCursor
	Deallocate QuickSearchCursor

END

SET @SQL = @SQLSelect + CHAR(10) + @SQLWhere
EXEC(@SQL)
print @SQL


SELECT		*
FROM		#t1
ORDER BY	LastName,
			FirstName
GO
