SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_ContactUpdateFamily]
(
	@Contact_ID int,
	@Family_ID int,
	@FamilyMemberType_ID int = -1,
	@ModifiedBy_ID int
)
AS
	SET NOCOUNT ON
	
	UPDATE [Common_Contact]
	SET
		[Family_ID] = @Family_ID,
		[FamilyMemberType_ID] = CASE WHEN @FamilyMemberType_ID <> -1 THEN @FamilyMemberType_ID ELSE FamilyMemberType_ID END,
		[ModificationDate] = dbo.GetRelativeDate(),
		[ModifiedBy_ID] = @ModifiedBy_ID
	WHERE 
		[Contact_ID] = @Contact_ID
GO
