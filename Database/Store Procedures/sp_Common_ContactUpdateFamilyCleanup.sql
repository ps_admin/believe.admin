SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_ContactUpdateFamilyCleanup]
(
	@Family_ID		int,
	@Contact_ID		int
)
AS
	SET NOCOUNT ON

	DECLARE @FamilyCount INT
	SET @FamilyCount = (SELECT COUNT(*) FROM [Common_Contact] WHERE Family_ID = @Family_ID AND Contact_ID <> @Contact_ID)
	
	
	IF (@FamilyCount = 0) BEGIN
		UPDATE Common_Contact SET Family_ID = NULL where Family_ID = @Family_ID
		UPDATE Common_Family SET Deleted = 1 WHERE Family_ID = @Family_ID
	END
		
		
	SELECT CASE WHEN @FamilyCount = 0 THEN 0 ELSE @Family_ID END
GO
