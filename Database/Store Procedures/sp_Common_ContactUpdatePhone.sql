SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_ContactUpdatePhone]
(
	@Contact_ID int,
	@HomePhone nvarchar(20)		= null,
	@MobilePhone nvarchar(20)	= null,
	@ModifiedBy_ID int
)
AS
	SET NOCOUNT ON
	
	UPDATE [Common_Contact]
	SET
		[Phone] = CASE WHEN @HomePhone IS NULL THEN Phone ELSE @HomePhone END,
		[Mobile] = CASE WHEN @MobilePhone IS NULL THEN Mobile ELSE @MobilePhone END,
		[ModificationDate] = dbo.GetRelativeDate(),
		[ModifiedBy_ID] = @ModifiedBy_ID
	WHERE 
		[Contact_ID] = @Contact_ID
GO
