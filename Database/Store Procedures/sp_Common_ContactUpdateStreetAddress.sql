SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_ContactUpdateStreetAddress]
(
	@Contact_ID int,
	@Address1 nvarchar(100),
	@Address2 nvarchar(100),
	@Suburb nvarchar(20),
	@Postcode nvarchar(10),
	@State_ID int,
	@StateOther nvarchar(20),
	@Country_ID int,
	@ModifiedBy_ID int
)
AS
	SET NOCOUNT ON
	
	UPDATE [Common_Contact]
	SET
		[Address1] = @Address1,
		[Address2] = @Address2,
		[Suburb] = @Suburb,
		[Postcode] = @Postcode,
		[State_ID] = @State_ID,
		[StateOther] = @StateOther,
		[Country_ID] = @Country_ID,
		[ModificationDate] = dbo.GetRelativeDate(),
		[ModifiedBy_ID] = @ModifiedBy_ID
	WHERE 
		[Contact_ID] = @Contact_ID
GO
