SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_CreditCardReconciliation]

@Conference_ID	INT

AS

SELECT		CCName, 
			SUM(PaymentAmount) as PaymentAmount
FROM		(
				select paymentamount, rtrim(ltrim(ccname)) as CCName from events_contactpayment where paymenttype_id = 3 and ccmanual = 0 and PaypalTransactionRef= '' and conference_id = @Conference_ID
				union all 				select paymentamount, rtrim(ltrim(ccname)) as CCName from events_grouppayment where paymenttype_id = 3 and ccmanual = 0 and PaypalTransactionRef= '' and conference_id =  @Conference_ID
			) P
GROUP BY	P.CCName
GO
