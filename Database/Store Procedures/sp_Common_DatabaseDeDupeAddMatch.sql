SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_DatabaseDeDupeAddMatch]

@Contact_ID				INT,
@ContactMatch_ID		INT

AS

DECLARE		@Match_ID	INT

BEGIN TRANSACTION
	
	IF (@ContactMatch_ID > 0) BEGIN

		SELECT  @Match_ID = Match_ID FROM Common_DatabaseDedupeMatch WHERE Contact_ID = @Contact_ID
		IF @Match_ID IS NULL BEGIN
			SELECT	@Match_ID = ISNULL(MAX(Match_ID),0) + 1 FROM Common_DatabaseDedupeMatch

			INSERT	Common_DatabaseDedupeMatch (Match_ID, Contact_ID)
			VALUES (@Match_ID, @Contact_ID)
		END

		INSERT	Common_DatabaseDedupeMatch (Match_ID, Contact_ID)
		VALUES (@Match_ID, @ContactMatch_ID)

	END

	UPDATE Common_Contact SET DeDupeVerified = 1 WHERE Contact_ID = @Contact_ID

COMMIT TRANSACTION
GO
