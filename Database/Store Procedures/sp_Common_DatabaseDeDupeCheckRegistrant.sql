SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_DatabaseDeDupeCheckRegistrant]

@Contact_ID		INT

AS

SELECT	Contact_ID 
FROM	Common_Contact C
WHERE	C.Contact_ID = @Contact_ID
AND		C.DeDupeVerified = 0
GO
