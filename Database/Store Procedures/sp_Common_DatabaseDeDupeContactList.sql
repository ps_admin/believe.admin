SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_DatabaseDeDupeContactList]

AS

SELECT		Contact_ID
FROM		Common_Contact
WHERE		DeDupeVerified = 0
ORDER BY	Contact_ID
GO
