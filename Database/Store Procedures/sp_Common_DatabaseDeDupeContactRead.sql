SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_DatabaseDeDupeContactRead]

@Contact_ID		INT

AS

SELECT		C.Contact_ID,
			C.FirstName,
			C.LastName,
			C.DateOfBirth,
			C.Address1,
			C.Address2,
			C.Suburb,
			C.Postcode,
			CASE WHEN C.State_ID = 9 THEN StateOther ELSE S.State END as State,
			GC.Country,
			C.Phone,
			C.Mobile,
			C.Email,
			C.CreationDate
FROM		Common_Contact C,
			Common_GeneralState S,
			Common_GeneralCountry GC
WHERE		C.State_ID = S.State_ID
AND			C.Country_ID = GC.Country_ID
AND			C.Contact_ID = @Contact_ID
GO
