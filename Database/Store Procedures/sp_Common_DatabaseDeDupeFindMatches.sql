SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_DatabaseDeDupeFindMatches]

@Contact_ID			INT
			
AS

DECLARE		@LastName			VARCHAR(50),
			@FirstName			VARCHAR(50),
			@DateOfBirth		VARCHAR(50),
			@Address1			VARCHAR(50),
			@Suburb				VARCHAR(50),
			@Postcode			VARCHAR(50),
			@State				VARCHAR(50),
			@Country			VARCHAR(50),
			@Phone				VARCHAR(50),
			@Mobile				VARCHAR(50),
			@Email				VARCHAR(100),		
			@ScoreLastName		DECIMAL(5,2),
			@ScoreFirstName		DECIMAL(5,2),
			@ScoreDateOfBirth	DECIMAL(5,2),
			@ScoreAddress1		DECIMAL(5,2),
			@ScoreSuburb		DECIMAL(5,2),
			@ScorePostcode		DECIMAL(5,2),
			@ScoreState			DECIMAL(5,2),
			@ScoreCountry		DECIMAL(5,2),
			@ScorePhone			DECIMAL(5,2),
			@ScoreMobile		DECIMAL(5,2),
			@ScoreEmail			DECIMAL(5,2)

SET			@ScoreLastName =		17
SET			@ScoreFirstName =		17
SET			@ScoreDateOfBirth =		17
SET			@ScoreAddress1 =		10		
SET			@ScoreSuburb =			5			
SET			@ScorePostcode =		5      		
SET			@ScoreState =			5			
SET			@ScoreCountry =			0		
SET			@ScorePhone =			4			
SET			@ScoreMobile =			15			
SET			@ScoreEmail =			5	


SELECT		C.Contact_ID,
			C.FirstName,
			C.LastName,
			C.DateOfBirth,
			C.Address1,
			C.Address2,
			C.Suburb,
			C.Postcode,
			CASE WHEN C.State_ID = 9 THEN StateOther ELSE S.State END as State,
			GC.Country,
			C.Phone,
			C.Mobile,
			C.Email,
			C.CreationDate,
			CONVERT(DECIMAL(19,2),0) AS PercentMatch
INTO		#t1
FROM		Common_Contact C,
			Common_GeneralState S,
			Common_GeneralCountry GC
WHERE		C.State_ID = S.State_ID
AND			C.Country_ID = GC.Country_ID


SELECT		@LastName = C.LastName, 
			@FirstName = C.FirstName,
			@DateOfBirth = ISNULL(CONVERT(VARCHAR,C.DateOfBirth,106),''),
			@Address1 = C.Address1,
			@Suburb = C.Suburb,
			@Postcode = C.Postcode,
			@State = CASE WHEN C.State_ID = 9 THEN StateOther ELSE S.State END,
			@Country = GC.Country,
			@Phone = REPLACE(REPLACE(REPLACE(C.Phone,' ',''),'(',''),'(',''),
			@Mobile = REPLACE(C.Mobile,' ',''),
			@Email = C.Email
FROM		Common_Contact C,
			Common_GeneralState S,
			Common_GeneralCountry GC
WHERE		C.State_ID = S.State_ID
AND			C.Country_ID = GC.Country_ID
AND			C.Contact_ID = @Contact_ID
				

UPDATE #t1 SET PercentMatch = (CASE WHEN LastName = @LastName OR LastName = @FirstName THEN @ScoreLastName ELSE 0 END) +
							  (CASE WHEN LastName <> @LastName AND DIFFERENCE(LastName, @LastName) = 4 THEN @ScoreLastName * 0.5 ELSE 0 END) +
							  (CASE WHEN FirstName = @FirstName OR FirstName = @LastName THEN @ScoreFirstName ELSE 0 END) +
							  (CASE WHEN FirstName <> @FirstName AND DIFFERENCE(FirstName, @FirstName) = 4 THEN @ScoreFirstName * 0.5 ELSE 0 END) +
							  (CASE WHEN ISNULL(CONVERT(VARCHAR,DateOfBirth,106),'') = @DateOfBirth THEN @ScoreDateOfBirth ELSE	
									(CASE WHEN ABS(DateDiff(dd, DateOfBirth, @DateOfBirth)) < 5 THEN @ScoreDateOfBirth / 2 ELSE 0 END) END) + -- If DOB is close, give a 1/2 score
							  (CASE WHEN Address1 = @Address1 THEN @ScoreAddress1 ELSE 0 END) +
							  (CASE WHEN Address1 <> @Address1 AND DIFFERENCE(Address1, @Address1) = 4 THEN @ScoreAddress1 * 0.5 ELSE 0 END) +
							  (CASE WHEN Suburb = @Suburb THEN @ScoreSuburb ELSE 0 END) +
							  (CASE WHEN Suburb <> @Suburb AND DIFFERENCE(Suburb, @Suburb) = 4 THEN @ScoreSuburb * 0.5 ELSE 0 END) +
							  (CASE WHEN Postcode = @Postcode THEN @ScorePostcode ELSE 0 END) +
							  (CASE WHEN State = @State THEN @ScoreState ELSE 0 END) +
							  (CASE WHEN Country = @Country THEN @ScoreCountry ELSE 0 END) +
							  (CASE WHEN REPLACE(REPLACE(REPLACE(Phone,' ',''),'(',''),'(','') = @Phone THEN @ScorePhone ELSE 0 END) +
							  (CASE WHEN REPLACE(Mobile,' ','') = @Mobile THEN @ScoreMobile ELSE 0 END) +
							  (CASE WHEN Email = @Email THEN @ScoreEmail ELSE 0 END)

SELECT		TOP 10 
			IDENTITY(int, 1, 1) as MatchNumber,
			Contact_ID,
			FirstName,
			LastName,
			DateOfBirth,
			Address1,
			Address2,
			Suburb,
			Postcode,
			State,
			Country,
			Phone,
			Mobile,
			Email,
			CreationDate,
			PercentMatch
INTO		#t2
FROM		#t1 
WHERE		Contact_ID <> @Contact_ID
AND			PercentMatch > 60
ORDER BY	PercentMatch DESC

SELECT		*
FROM		#t2
GO
