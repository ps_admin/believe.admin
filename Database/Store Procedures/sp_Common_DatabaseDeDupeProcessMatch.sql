SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_DatabaseDeDupeProcessMatch]

AS

BEGIN TRANSACTION

BEGIN

	SET NOCOUNT ON
	SET XACT_ABORT ON 

	DECLARE	@MasterContact_ID INT,
			@Contact_ID	INT,
			@Match_ID INT,
			@Email NVARCHAR(MAX)
			
	--First, Ensure that no two matched people have two church records. Remove these manually.
	SELECT DISTINCT Match_ID FROM Common_DatabaseDedupeMatch D WHERE 
	(SELECT COUNT(*) FROM Common_ContactInternal I, Common_DatabaseDedupeMatch M WHERE I.Contact_ID = M.Contact_ID AND M.Match_ID = D.Match_ID)> 1
	
	/*
	select distinct M.match_ID, c.contact_ID, ci.churchstatus_Id, c.modificationdate, c.* from common_contact c, common_COntactinternal ci, Common_DatabaseDedupeMatch M
	where m.contact_id = c.contact_ID and c.contact_Id = ci.contact_ID
	and Match_ID in (5)
	ORDER BY M.Match_ID
	
	delete from Common_DatabaseDedupeMatch where match_id in (25, 503)
	
	declare @old int; set @old = 93456
	declare @new int; set @new = 111399
	update church_contactcomment set contact_Id = @new where contact_ID = @old
	update church_npnc set contact_Id = @new where contact_ID = @old
	update church_ulgcontact set contact_Id = @new where contact_ID = @old
	update church_pcrcontact set contact_Id = @new where contact_ID = @old
	update church_churchstatuschangerequest set contact_Id = @new where contact_ID = @old
	update Church_ContactAdditionalDecision set contact_Id = @new where contact_ID = @old
	update Church_ContactCourseInstance set contact_Id = @new where contact_ID = @old
	update Church_247Prayer set contact_Id = @new where contact_ID = @old
	update Church_ContactRole set contact_Id = @new where contact_ID = @old
	update Church_KidsMinistryAttendance set contact_Id = @new where contact_ID = @old
	update Church_KidsMinistryAttendance set CheckedInByParent_ID = @new where CheckedInByParent_ID = @old
	update Church_KidsMinistryAttendance set CheckedOutByParent_ID = @new where CheckedOutByParent_ID = @old
	update Church_KidsMinistryClearance set contact_Id = @new where contact_ID = @old
	update Church_KidsMinistryClearance set clearancecontact_Id = @new where clearancecontact_Id = @old
	update Church_ContactKidsMinistryCode set contact_Id = @new where contact_ID = @old
	delete from common_contactinternal where Contact_ID = @old

	*/
	
	
	--Second, Ensure that no two matched people have two group records. Remove these manually.
	SELECT DISTINCT Match_ID FROM Common_DatabaseDedupeMatch D WHERE 
	(SELECT COUNT(*) FROM Events_Group G, Common_DatabaseDedupeMatch M WHERE G.GroupLeader_ID = M.Contact_ID AND M.Match_ID = D.Match_ID)> 1
	
	/*
	
	select M.match_ID, c.contact_Id, g.groupname, g.youthleadername, g.creationdate, g.modificationdate, c.* from common_contact c, events_group g, Common_DatabaseDedupeMatch M
	where m.contact_id = c.contact_ID and c.contact_ID = g.groupleader_ID
	and Match_ID in (27,151,571,607)
	ORDER BY M.Match_ID
	
	select * from events_group where groupleader_Id in (95967,107001)
	select * from events_registration where groupleader_Id in (95967,107001)
	
	select match_Id, contact_ID from Common_DatabaseDedupeMatch group by match_Id, contact_ID having count(*) >1
	delete from Common_DatabaseDedupeMatch where match_Id = 532
	
	declare @old int; set @old = 108930
	declare @new int; set @new = 110416
	update events_groupBulkPurchase set GroupLeader_ID = @new where GroupLeader_ID = @old
	update events_groupBulkRegistration set GroupLeader_ID = @new where GroupLeader_ID = @old
	update events_groupContact set GroupLeader_ID = @new where GroupLeader_ID = @old
	update events_groupHistory set GroupLeader_ID = @new where GroupLeader_ID = @old
	update events_groupNotes set GroupLeader_ID = @new where GroupLeader_ID = @old
	update events_groupPayment set GroupLeader_ID = @new where GroupLeader_ID = @old
	update events_groupPromoRequest set GroupLeader_ID = @new where GroupLeader_ID = @old
	update events_Registration set GroupLeader_ID = @new where GroupLeader_ID = @old
	DELETE FROM Events_Group WHERE GroupLeader_ID = @old

	*/

	-- Optional Step 3
	-- Check the matches manually, for incorrect matches (human error)
	
	/*
	select distinct M.match_ID, c.contact_ID, c.modificationdate, c.* from common_contact c, common_COntactinternal ci, Common_DatabaseDedupeMatch M
	where m.contact_id = c.contact_ID
	and Processed = 0  
	ORDER BY M.Match_ID
	*/
	
	select *
	from Common_DatabaseDedupeMatch m, common_userdetail u
	where m.contact_Id = u.contact_ID
	order by match_Id
	
	
	DECLARE MatchCursor CURSOR FOR
		SELECT DISTINCT Match_ID FROM Common_DatabaseDeDupeMatch WHERE Processed = 0 ORDER BY Match_ID

	OPEN MatchCursor 

	FETCH NEXT FROM MatchCursor INTO @Match_ID

	WHILE @@FETCH_STATUS = 0 BEGIN

		--Find Master record
		SET @MasterContact_ID = (SELECT	TOP 1 C.Contact_ID
					FROM	Common_DatabaseDeDupeMatch D,
							Common_Contact C
					WHERE	D.Contact_ID = C.Contact_ID
					AND		Match_ID = @Match_ID 
					ORDER BY	(SELECT COUNT(*) FROM Common_ContactInternal WHERE Contact_ID = C.Contact_ID) DESC, 
								(SELECT COUNT(*) FROM Revolution_ContactSubscription WHERE Contact_ID = C.Contact_ID) DESC, 
								(SELECT COUNT(*) FROM Events_Group WHERE GroupLeader_ID = C.Contact_ID) DESC, 
								ModificationDate DESC)
		
		SELECT D.Match_ID, C.* FROM Common_Contact C, Common_DatabaseDeDupeMatch D WHERE C.Contact_ID = D.Contact_ID AND D.Match_ID = @Match_ID

		DECLARE ContactCursor CURSOR FOR 
			SELECT Contact_ID FROM Common_DatabaseDeDupeMatch WHERE Match_ID = @Match_ID  AND Contact_ID <> @MasterContact_ID
		OPEN ContactCursor

		FETCH NEXT FROM ContactCursor INTO @Contact_ID
		WHILE @@FETCH_STATUS = 0 BEGIN

			--UPDATE Common_Contact
			UPDATE	Common_Contact
			SET		DateOfBirth = COALESCE(Common_Contact.DateOfBirth,S.DateOfBirth),
					Gender = CASE WHEN Common_Contact.Gender = '' AND S.Gender <> '' THEN S.Gender ELSE Common_Contact.Gender END,
					Phone = CASE WHEN Common_Contact.Phone = '' AND S.Phone <> '' THEN S.Phone ELSE Common_Contact.Phone END,
					PhoneWork = CASE WHEN Common_Contact.PhoneWork = '' AND S.PhoneWork <> '' THEN S.PhoneWork ELSE Common_Contact.PhoneWork END,
					Mobile = CASE WHEN Common_Contact.Mobile = '' AND S.Mobile <> '' THEN S.Mobile ELSE Common_Contact.Mobile END
			FROM	Common_Contact,
					Common_Contact S
			WHERE	Common_Contact.Contact_ID = @MasterContact_ID
			AND		S.Contact_ID = @Contact_ID
		
		
			SELECT @MasterContact_ID
			--SELECT * FROM Common_ContactInternal WHERE Contact_ID IN (@MasterContact_ID, @Contact_ID)

			IF (SELECT COUNT(*) FROM Common_ContactExternal WHERE Contact_ID = @MasterContact_ID) = 0
				UPDATE Common_ContactExternal	SET Contact_ID = @MasterContact_ID WHERE Contact_ID = @Contact_ID

			IF (SELECT COUNT(*) FROM Common_ContactExternalEvents WHERE Contact_ID = @MasterContact_ID) = 0
				UPDATE Common_ContactExternalEvents	SET Contact_ID = @MasterContact_ID WHERE Contact_ID = @Contact_ID
			ELSE BEGIN
				UPDATE Events_GroupContact		SET Contact_ID = @MasterContact_ID WHERE Contact_ID = @Contact_ID
				UPDATE Events_Registration		SET Contact_ID = @MasterContact_ID WHERE Contact_ID = @Contact_ID
				UPDATE Events_ContactPayment	SET Contact_ID = @MasterContact_ID WHERE Contact_ID = @Contact_ID
				UPDATE Events_WaitingList		SET Contact_ID = @MasterContact_ID WHERE Contact_ID = @Contact_ID
			END
			
			SELECT @MasterContact_ID, COUNT(*) FROM Common_UserDetail WHERE Contact_ID = @MasterContact_ID
			IF (SELECT COUNT(*) FROM Common_UserDetail WHERE Contact_ID = @MasterContact_ID) = 0 BEGIN
				UPDATE Common_Contact SET [ModifiedBy_ID] = @MasterContact_ID WHERE [ModifiedBy_ID] = @Contact_ID
				UPDATE Common_Contact SET [CreatedBy_ID] = @MasterContact_ID WHERE [CreatedBy_ID] = @Contact_ID
				UPDATE Events_Group SET [ModifiedBy_ID] = @MasterContact_ID WHERE [ModifiedBy_ID] = @Contact_ID
				UPDATE Events_Group SET [CreatedBy_ID] = @MasterContact_ID WHERE [CreatedBy_ID] = @Contact_ID
				UPDATE Common_UserDetail SET Contact_ID = @MasterContact_ID WHERE Contact_ID = @Contact_ID
			END
			ELSE BEGIN
				UPDATE Common_ContactDatabaseRole SET Contact_ID = @MasterContact_ID WHERE Contact_ID = @Contact_ID
				UPDATE Events_POSTransaction SET [User_ID] = @MasterContact_ID WHERE [User_ID] = @Contact_ID
				UPDATE Events_ContactPayment SET [PaymentBy_ID] = @MasterContact_ID WHERE [PaymentBy_ID] = @Contact_ID
				UPDATE Events_GroupHistory SET [User_ID] = @MasterContact_ID WHERE [User_ID] = @Contact_ID
				UPDATE Events_Registration SET [RegisteredBy_ID] = @MasterContact_ID WHERE [RegisteredBy_ID] = @Contact_ID
				UPDATE Events_GroupPayment SET [PaymentBy_ID] = @MasterContact_ID WHERE [PaymentBy_ID] = @Contact_ID
				UPDATE Common_ContactHistory SET [User_ID] = @MasterContact_ID WHERE [User_ID] = @Contact_ID
				UPDATE Common_Contact SET [ModifiedBy_ID] = @MasterContact_ID WHERE [ModifiedBy_ID] = @Contact_ID
				UPDATE Common_Contact SET [CreatedBy_ID] = @MasterContact_ID WHERE [CreatedBy_ID] = @Contact_ID
				DELETE FROM Common_UserDetail WHERE Contact_ID = @Contact_ID
			END
			
			UPDATE Events_POSTransaction SET Contact_ID = @MasterContact_ID WHERE Contact_ID = @Contact_ID
			UPDATE Common_ContactHistory SET Contact_ID = @MasterContact_ID WHERE Contact_ID = @Contact_ID
			UPDATE Revolution_SignInLog SET Contact_ID = @MasterContact_ID WHERE Contact_ID = @Contact_ID
			UPDATE Revolution_ContentDownload SET Contact_ID = @MasterContact_ID WHERE Contact_ID = @Contact_ID
			UPDATE Events_Group SET GroupLeader_ID = @MasterContact_ID WHERE GroupLeader_ID = @Contact_ID

			--Move Rev Subscription if it exists
			IF (SELECT COUNT(*) FROM Revolution_ContactSubscription WHERE Contact_ID = @Contact_ID) > 0 BEGIN

				SELECT 'Moved Revolution'
				--Move the email address
				SELECT @Email = Email FROM Common_Contact WHERE Contact_ID = @Contact_ID
				UPDATE Common_Contact SET Email = '' where contact_ID = @Contact_ID
				UPDATE Common_Contact SET Email = @Email where contact_ID = @MasterContact_ID
			
				IF (SELECT COUNT(*) FROM Common_ContactExternalRevolution WHERE Contact_ID = @MasterContact_ID) = 0
					UPDATE Common_ContactExternalRevolution SET Contact_ID = @MasterContact_ID WHERE Contact_ID = @Contact_ID
				ELSE BEGIN
					UPDATE Revolution_ActivationCode SET Contact_ID = @MasterContact_ID WHERE Contact_ID = @Contact_ID
					UPDATE Revolution_ContactSubscription SET Contact_ID = @MasterContact_ID WHERE Contact_ID = @Contact_ID
				END
			END

			DELETE FROM Common_WebSession WHERE Contact_ID = @Contact_ID
			DELETE FROM Common_ContactExternalRevolution WHERE Contact_ID = @Contact_ID
			DELETE FROM Common_ContactExternalEvents WHERE Contact_ID = @Contact_ID
			DELETE FROM Common_ContactExternal WHERE Contact_ID = @Contact_ID
			DELETE FROM Common_ContactMailingList WHERE Contact_ID = @Contact_ID
			DELETE FROM Common_ContactMailingListType WHERE Contact_ID = @Contact_ID
			DELETE FROM Common_Contact WHERE Contact_ID = @Contact_ID

			FETCH NEXT FROM ContactCursor INTO @Contact_ID
		END

		CLOSE ContactCursor
		DEALLOCATE ContactCursor

		SELECT C.* FROM Common_Contact C, Common_DatabaseDeDupeMatch D WHERE C.Contact_ID = D.Contact_ID AND D.Match_ID = @Match_ID

		UPDATE Common_DatabaseDeDupeMatch SET Processed = 1 WHERE Match_ID = @Match_ID


		FETCH NEXT FROM MatchCursor INTO @Match_ID
	END

	CLOSE MatchCursor
	DEALLOCATE MatchCursor
END



COMMIT TRANSACTION
GO
