SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_DatabaseDeDupeReadStats]

AS

SELECT		SUM(CASE WHEN DeDupeVerified = 1 THEN 1 ELSE 0 END) AS Processed, 
			COUNT(*) AS Total 
FROM		Common_Contact C
GO
