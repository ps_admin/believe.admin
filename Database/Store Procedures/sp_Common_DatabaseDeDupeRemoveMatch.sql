SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_DatabaseDeDupeRemoveMatch]

@Contact_ID		INT

AS

DELETE
FROM	Common_DatabaseDedupeMatch
WHERE	Match_ID = (SELECT Match_ID FROM Common_DatabaseDedupeMatch WHERE Contact_ID = @Contact_ID)

UPDATE	Common_Contact 
SET		DeDupeVerified = 0
WHERE	Contact_ID = @Contact_ID
GO
