SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_DatabaseDeDupeRestartProcess]

AS

TRUNCATE TABLE Common_DatabaseDeDupeMatch

UPDATE		Common_Contact
SET			DeDupeVerified = 0
WHERE		DeDupeVerified = 1

UPDATE		Common_DatabaseDedupeOptions
SET			DateLastStarted = dbo.GetRelativeDate()
GO
