SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_DatabaseOptionsUpdate]

@DefaultEmailAddress	NVARCHAR(100),
@TimeDifference			INT

AS

UPDATE	Common_DatabaseInstance
SET		DefaultEmailAddress = @DefaultEmailAddress,
		TimeDifference = @TimeDifference
GO
