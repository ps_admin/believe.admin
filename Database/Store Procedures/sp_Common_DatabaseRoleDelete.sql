SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_DatabaseRoleDelete]
(
	@DatabaseRole_ID int
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Common_DatabaseRole]
	WHERE  [DatabaseRole_ID] = @DatabaseRole_ID
GO
