SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_DatabaseRoleInsert]
(
	@Module nvarchar(10),
	@Name nvarchar(50),
	@SortOrder int,
	@Deleted bit = 0
)
AS
	SET NOCOUNT ON

	INSERT INTO [Common_DatabaseRole]
	(
		[Module],
		[Name],
		[SortOrder],
		[Deleted]
	)
	VALUES
	(
		@Module,
		@Name,
		@SortOrder,
		@Deleted
	)

	SELECT DatabaseRole_ID = SCOPE_IDENTITY();
GO
