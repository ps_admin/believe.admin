SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_DatabaseRolePermissionDelete]
(
	@DatabaseRolePermission_ID int
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Common_DatabaseRolePermission]
	WHERE  [DatabaseRolePermission_ID] = @DatabaseRolePermission_ID
GO
