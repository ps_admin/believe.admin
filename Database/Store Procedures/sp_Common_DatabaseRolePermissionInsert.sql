SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_DatabaseRolePermissionInsert]
(
	@DatabaseRole_ID int,
	@DatabaseObject_ID int,
	@Read bit,
	@Write bit,
	@ViewState bit,
	@ViewCountry bit
)
AS
	SET NOCOUNT ON

	INSERT INTO [Common_DatabaseRolePermission]
	(
		[DatabaseRole_ID],
		[DatabaseObject_ID],
		[Read],
		[Write],
		[ViewState],
		[ViewCountry]
	)
	VALUES
	(
		@DatabaseRole_ID,
		@DatabaseObject_ID,
		@Read,
		@Write,
		@ViewState,
		@ViewCountry
	)

	SELECT DatabaseRolePermission_ID = SCOPE_IDENTITY();
GO
