SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_DatabaseRolePermissionUpdate]
(
	@DatabaseRolePermission_ID int,
	@DatabaseRole_ID int,
	@DatabaseObject_ID int,
	@Read bit,
	@Write bit,
	@ViewState bit,
	@ViewCountry bit
)
AS
	SET NOCOUNT ON
	
	UPDATE [Common_DatabaseRolePermission]
	SET
		[DatabaseRole_ID] = @DatabaseRole_ID,
		[DatabaseObject_ID] = @DatabaseObject_ID,
		[Read] = @Read,
		[Write] = @Write,
		[ViewState] = @ViewState,
		[ViewCountry] = @ViewCountry
	WHERE 
		[DatabaseRolePermission_ID] = @DatabaseRolePermission_ID
GO
