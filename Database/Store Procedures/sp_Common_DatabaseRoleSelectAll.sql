SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_DatabaseRoleSelectAll]

AS

SET NOCOUNT ON

SELECT * FROM Common_DatabaseRole

-- Add permissions to permissions table if they do not exist
INSERT 	Common_DatabaseRolePermission (DatabaseRole_ID, DatabaseObject_ID, [Read], Write, ViewState, ViewCountry)
SELECT 	DatabaseRole_ID, DatabaseObject_ID, 0, 0, 0, 0
FROM 	Common_DatabaseRole DR, 
		Common_DatabaseObject O
WHERE 	NOT EXISTS (
			SELECT * FROM Common_DatabaseRolePermission
			WHERE DatabaseObject_ID = O.DatabaseObject_ID
			AND	DatabaseRole_ID = DR.DatabaseRole_ID)
AND		DR.Module = O.Module

SELECT * FROM Common_DatabaseRolePermission
GO
