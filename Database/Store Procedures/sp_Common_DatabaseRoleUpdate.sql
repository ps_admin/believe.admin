SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_DatabaseRoleUpdate]
(
	@DatabaseRole_ID int,
	@Module nvarchar(10),
	@Name nvarchar(50),
	@SortOrder int,
	@Deleted bit = 0
)
AS
	SET NOCOUNT ON
	
	UPDATE [Common_DatabaseRole]
	SET
		[Module] = @Module,
		[Name] = @Name,
		[SortOrder] = @SortOrder,
		[Deleted] = @Deleted
	WHERE 
		[DatabaseRole_ID] = @DatabaseRole_ID
GO
