SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_DedupeContacts]

@MasterContact_ID		INT,
@DuplicateContact_ID	INT

AS

BEGIN TRANSACTION

BEGIN

	SET NOCOUNT ON
	SET XACT_ABORT ON 

	
	/*****************************************************************************
	 *****************************************************************************
	 **																			**
	 **						     COMMON RECORDS									**
	 **																			**
	 *****************************************************************************
	 *****************************************************************************/	
	
	--UPDATE Common_Contact
	UPDATE	Common_Contact
	SET		DateOfBirth = COALESCE(Common_Contact.DateOfBirth,S.DateOfBirth),
			Gender = CASE WHEN Common_Contact.Gender = '' AND S.Gender <> '' THEN S.Gender ELSE Common_Contact.Gender END,
			Phone = CASE WHEN Common_Contact.Phone = '' AND S.Phone <> '' THEN S.Phone ELSE Common_Contact.Phone END,
			Fax = CASE WHEN Common_Contact.Fax = '' AND S.Fax <> '' THEN S.Fax ELSE Common_Contact.Fax END,
			PhoneWork = CASE WHEN Common_Contact.PhoneWork = '' AND S.PhoneWork <> '' THEN S.PhoneWork ELSE Common_Contact.PhoneWork END,
			Mobile = CASE WHEN Common_Contact.Mobile = '' AND S.Mobile <> '' THEN S.Mobile ELSE Common_Contact.Mobile END
	FROM	Common_Contact,
			Common_Contact S
	WHERE	Common_Contact.Contact_ID = @MasterContact_ID
	AND		S.Contact_ID = @DuplicateContact_ID
	
		
	--Transfer their email address
	DECLARE @Email1UserMaster BIT
	DECLARE @Email1UserDuplicate BIT
	DECLARE @Email1Master NVARCHAR(100), @Email2Master NVARCHAR(100), @Email1Duplicate NVARCHAR(100), @Email2Duplicate NVARCHAR(100)
	DECLARE @DoNotIncludeEmail1Master BIT, @DoNotIncludeEmail2Master BIT, @DoNotIncludeEmail1Duplicate BIT, @DoNotIncludeEmail2Duplicate BIT
		
	SET @Email1UserMaster = CASE WHEN (SELECT COUNT(*) FROM Common_UserDetail U, Common_Contact C WHERE U.Contact_ID = C.Contact_ID AND C.Email <> '' AND C.Contact_ID = @MasterContact_ID) > 0 THEN 1 ELSE 0 END
	SET @Email1UserDuplicate = CASE WHEN (SELECT COUNT(*) FROM Common_UserDetail U, Common_Contact C WHERE U.Contact_ID = C.Contact_ID AND C.Email <> '' AND C.Contact_ID = @DuplicateContact_ID) > 0 THEN 1 ELSE 0 END
	SELECT @Email1Master = Email, @Email2Master = Email2, @DoNotIncludeEmail1Master = DoNotIncludeEmail1InMailingList, @DoNotIncludeEmail2Master = DoNotIncludeEmail2InMailingList FROM Common_Contact WHERE Contact_ID = @MasterContact_ID
	SELECT @Email1Duplicate = Email, @Email2Duplicate = Email2, @DoNotIncludeEmail1Duplicate = DoNotIncludeEmail1InMailingList, @DoNotIncludeEmail2Duplicate = DoNotIncludeEmail2InMailingList FROM Common_Contact WHERE Contact_ID = @DuplicateContact_ID

	UPDATE Common_Contact SET Email = '', Email2 = '', DoNotIncludeEmail1InMailingList = 0, DoNotIncludeEmail2InMailingList = 0 WHERE Contact_ID IN (@MasterContact_ID, @DuplicateContact_ID)
	
	--Email 1
	IF @Email1UserMaster = 1 AND @Email1Master <> '' BEGIN
		UPDATE Common_Contact SET Email = @Email1Master, DoNotIncludeEmail1InMailingList = @DoNotIncludeEmail1Master WHERE Contact_ID = @MasterContact_ID
		SET @Email1Master = ''
	END
	ELSE IF @Email1UserDuplicate = 1 AND @Email1Duplicate <> '' BEGIN
		UPDATE Common_Contact SET Email = @Email1Duplicate, DoNotIncludeEmail1InMailingList = @DoNotIncludeEmail1Duplicate WHERE Contact_ID = @MasterContact_ID
		SET @Email1Duplicate = ''
	END
	ELSE IF @Email1Master <> '' BEGIN
		UPDATE Common_Contact SET Email = @Email1Master, DoNotIncludeEmail1InMailingList = @DoNotIncludeEmail1Master WHERE Contact_ID = @MasterContact_ID
		SET @Email1Master = ''
	END
	ELSE IF @Email2Master <> '' AND (SELECT COUNT(*) FROM Common_Contact WHERE Email = @Email2Master) = 0 BEGIN
		UPDATE Common_Contact SET Email = @Email2Master, DoNotIncludeEmail1InMailingList = @DoNotIncludeEmail2Master WHERE Contact_ID = @MasterContact_ID
		SET @Email2Master = ''
	END 
	ELSE IF @Email1Duplicate <> '' BEGIN
		UPDATE Common_Contact SET Email = @Email1Duplicate, DoNotIncludeEmail1InMailingList = @DoNotIncludeEmail1Duplicate WHERE Contact_ID = @MasterContact_ID
		SET @Email1Duplicate = ''
	END
	ELSE IF @Email2Duplicate <> '' AND (SELECT COUNT(*) FROM Common_Contact WHERE Email = @Email2Duplicate) = 0 BEGIN
		UPDATE Common_Contact SET Email = @Email2Duplicate, DoNotIncludeEmail1InMailingList = @DoNotIncludeEmail2Duplicate WHERE Contact_ID = @MasterContact_ID
		SET @Email2Duplicate = ''
	END 
	
	--Email 2
	IF @Email1UserMaster = 1 AND @Email1Master <> ''
		UPDATE Common_Contact SET Email2 = @Email1Master, DoNotIncludeEmail2InMailingList = @DoNotIncludeEmail1Master WHERE Contact_ID = @MasterContact_ID
	ELSE IF @Email1UserDuplicate = 1 AND @Email1Duplicate <> ''
		UPDATE Common_Contact SET Email2 = @Email1Duplicate, DoNotIncludeEmail2InMailingList = @DoNotIncludeEmail1Duplicate WHERE Contact_ID = @MasterContact_ID
	ELSE IF @Email1Master <> ''
		UPDATE Common_Contact SET Email2 = @Email1Master, DoNotIncludeEmail2InMailingList = @DoNotIncludeEmail1Master WHERE Contact_ID = @MasterContact_ID
	ELSE IF @Email2Master <> ''
		UPDATE Common_Contact SET Email2 = @Email2Master, DoNotIncludeEmail2InMailingList = @DoNotIncludeEmail2Master WHERE Contact_ID = @MasterContact_ID
	ELSE IF @Email1Duplicate <> ''
		UPDATE Common_Contact SET Email2 = @Email1Duplicate, DoNotIncludeEmail2InMailingList = @DoNotIncludeEmail1Duplicate WHERE Contact_ID = @MasterContact_ID
	ELSE IF @Email2Duplicate <> ''
		UPDATE Common_Contact SET Email2 = @Email2Duplicate, DoNotIncludeEmail2InMailingList = @DoNotIncludeEmail2Duplicate WHERE Contact_ID = @MasterContact_ID
	
	
	UPDATE Events_POSTransaction SET Contact_ID = @MasterContact_ID WHERE Contact_ID = @DuplicateContact_ID
	UPDATE Common_ContactHistory SET Contact_ID = @MasterContact_ID WHERE Contact_ID = @DuplicateContact_ID
	UPDATE Common_ContactOldPassword SET Contact_ID = @MasterContact_ID WHERE Contact_ID = @DuplicateContact_ID
	UPDATE Revolution_SignInLog SET Contact_ID = @MasterContact_ID WHERE Contact_ID = @DuplicateContact_ID
	UPDATE Revolution_ContentDownload SET Contact_ID = @MasterContact_ID WHERE Contact_ID = @DuplicateContact_ID

	
	/*****************************************************************************
	 *****************************************************************************
	 **																			**
	 **						 INTERNAL CHURCH RECORD								**
	 **																			**
	 *****************************************************************************
	 *****************************************************************************/
	
	--Combine the Internal Church Record (if needed)
	DECLARE @InternalMaster BIT,
			@InternalDuplicate BIT
			
	SET		@InternalMaster = CASE WHEN (SELECT COUNT(*) FROM Common_ContactInternal WHERE Contact_ID = @MasterContact_ID) > 0 THEN 1 ELSE 0 END
	SET		@InternalDuplicate = CASE WHEN (SELECT COUNT(*) FROM Common_ContactInternal WHERE Contact_ID = @DuplicateContact_ID) > 0 THEN 1 ELSE 0 END
	
	IF		@InternalMaster = 1 AND @InternalDuplicate = 1 BEGIN 
		UPDATE	Common_ContactInternal
		SET		PostalAddress1 = CASE WHEN Common_ContactInternal.PostalAddress1 = '' AND D.PostalAddress1 <> '' THEN D.PostalAddress1 ELSE Common_ContactInternal.PostalAddress1 END,
				PostalAddress2 = CASE WHEN Common_ContactInternal.PostalAddress2 = '' AND D.PostalAddress2 <> '' THEN D.PostalAddress2 ELSE Common_ContactInternal.PostalAddress2 END,
				PostalSuburb = CASE WHEN Common_ContactInternal.PostalSuburb = '' AND D.PostalSuburb <> '' THEN D.PostalSuburb ELSE Common_ContactInternal.PostalSuburb END,
				PostalPostcode = CASE WHEN Common_ContactInternal.PostalPostcode = '' AND D.PostalPostcode <> '' THEN D.PostalPostcode ELSE Common_ContactInternal.PostalPostcode END,
				PostalState_ID = CASE WHEN Common_ContactInternal.PostalState_ID is null AND D.PostalState_ID is not null THEN D.PostalState_ID ELSE Common_ContactInternal.PostalState_ID END,
				PostalStateOther = CASE WHEN Common_ContactInternal.PostalStateOther = '' AND D.PostalStateOther <> '' THEN D.PostalStateOther ELSE Common_ContactInternal.PostalStateOther END,
				PostalCountry_ID = CASE WHEN Common_ContactInternal.PostalCountry_ID is null AND D.PostalCountry_ID is not null THEN D.PostalCountry_ID ELSE Common_ContactInternal.PostalCountry_ID END,
				Photo = CASE WHEN Common_ContactInternal.Photo IS NULL AND D.Photo IS NOT NULL THEN D.Photo ELSE Common_ContactInternal.Photo END,
				EntryPoint_ID = CASE WHEN Common_ContactInternal.EntryPoint_ID IS NULL AND D.EntryPoint_ID IS NOT NULL THEN D.EntryPoint_ID ELSE Common_ContactInternal.EntryPoint_ID END,
				DateJoinedChurch = CASE WHEN Common_ContactInternal.DateJoinedChurch IS NULL AND D.DateJoinedChurch IS NOT NULL THEN D.DateJoinedChurch ELSE Common_ContactInternal.DateJoinedChurch END,
				CovenantFormDate = CASE WHEN Common_ContactInternal.CovenantFormDate IS NULL AND D.CovenantFormDate IS NOT NULL THEN D.CovenantFormDate ELSE Common_ContactInternal.CovenantFormDate END,
				SalvationDate = CASE WHEN Common_ContactInternal.SalvationDate IS NULL AND D.SalvationDate IS NOT NULL THEN D.SalvationDate ELSE Common_ContactInternal.SalvationDate END,
				WaterBaptismDate = CASE WHEN Common_ContactInternal.WaterBaptismDate IS NULL AND D.WaterBaptismDate IS NOT NULL THEN D.WaterBaptismDate ELSE Common_ContactInternal.WaterBaptismDate END,
				VolunteerFormDate = CASE WHEN Common_ContactInternal.VolunteerFormDate IS NULL AND D.VolunteerFormDate IS NOT NULL THEN D.CovenantFormDate ELSE Common_ContactInternal.VolunteerFormDate END,
				PrimaryCarer = CASE WHEN Common_ContactInternal.PrimaryCarer = '' AND D.PrimaryCarer <> '' THEN D.PrimaryCarer ELSE Common_ContactInternal.PrimaryCarer END,
				CountryOfOrigin_ID = CASE WHEN Common_ContactInternal.CountryOfOrigin_ID IS NULL AND D.CountryOfOrigin_ID IS NOT NULL THEN D.CountryOfOrigin_ID ELSE Common_ContactInternal.CountryOfOrigin_ID END,
				PrivacyNumber = CASE WHEN Common_ContactInternal.PrivacyNumber = '' AND D.PrivacyNumber <> '' THEN D.PrivacyNumber ELSE Common_ContactInternal.PrivacyNumber END,
				PrimarySchool = CASE WHEN Common_ContactInternal.PrimarySchool = '' AND D.PrimarySchool <> '' THEN D.PrimarySchool ELSE Common_ContactInternal.PrimarySchool END,
				SchoolGrade_ID = CASE WHEN Common_ContactInternal.SchoolGrade_ID IS NULL AND D.SchoolGrade_ID IS NOT NULL THEN D.SchoolGrade_ID ELSE Common_ContactInternal.SchoolGrade_ID END,
				HighSchool = CASE WHEN Common_ContactInternal.HighSchool = '' AND D.HighSchool <> '' THEN D.HighSchool ELSE Common_ContactInternal.HighSchool END,
				University = CASE WHEN Common_ContactInternal.University = '' AND D.University <> '' THEN D.University ELSE Common_ContactInternal.University END,
				InformationIsConfidential = CASE WHEN Common_ContactInternal.InformationIsConfidential = 0 AND D.InformationIsConfidential = 1 THEN D.InformationIsConfidential ELSE Common_ContactInternal.InformationIsConfidential END,
				FacebookAddress = CASE WHEN Common_ContactInternal.FacebookAddress = '' AND D.FacebookAddress <> '' THEN D.FacebookAddress ELSE Common_ContactInternal.FacebookAddress END,
				Education_ID = CASE WHEN Common_ContactInternal.Education_ID IS NULL AND D.Education_ID IS NOT NULL THEN D.Education_ID ELSE Common_ContactInternal.Education_ID END,
				ModeOfTransport_ID = CASE WHEN Common_ContactInternal.ModeOfTransport_ID IS NULL AND D.ModeOfTransport_ID IS NOT NULL THEN D.ModeOfTransport_ID ELSE Common_ContactInternal.ModeOfTransport_ID END,
				CarParkUsed_ID = CASE WHEN Common_ContactInternal.CarParkUsed_ID IS NULL AND D.CarParkUsed_ID IS NOT NULL THEN D.CarParkUsed_ID ELSE Common_ContactInternal.CarParkUsed_ID END,
				ServiceAttending_ID = CASE WHEN Common_ContactInternal.ServiceAttending_ID IS NULL AND D.ServiceAttending_ID IS NOT NULL THEN D.ServiceAttending_ID ELSE Common_ContactInternal.ServiceAttending_ID END,
				Occupation = CASE WHEN Common_ContactInternal.Occupation IS NULL AND D.Occupation IS NOT NULL THEN D.Occupation ELSE Common_ContactInternal.Occupation END,
				Employer = CASE WHEN Common_ContactInternal.Employer IS NULL AND D.Employer IS NOT NULL THEN D.Employer ELSE Common_ContactInternal.Employer END,
				EmploymentStatus_ID = CASE WHEN Common_ContactInternal.EmploymentStatus_ID IS NULL AND D.EmploymentStatus_ID IS NOT NULL THEN D.EmploymentStatus_ID ELSE Common_ContactInternal.EmploymentStatus_ID END,
				EmploymentPosition_ID = CASE WHEN Common_ContactInternal.EmploymentPosition_ID IS NULL AND D.EmploymentPosition_ID IS NOT NULL THEN D.EmploymentPosition_ID ELSE Common_ContactInternal.EmploymentPosition_ID END,
				EmploymentIndustry_ID = CASE WHEN Common_ContactInternal.EmploymentIndustry_ID IS NULL AND D.EmploymentIndustry_ID IS NOT NULL THEN D.EmploymentIndustry_ID ELSE Common_ContactInternal.EmploymentIndustry_ID END,
				KidsMinistryComments = CASE WHEN Common_ContactInternal.KidsMinistryComments = '' AND D.KidsMinistryComments <> '' THEN D.KidsMinistryComments ELSE Common_ContactInternal.KidsMinistryComments END,
				KidsMinistryGroup = CASE WHEN Common_ContactInternal.KidsMinistryGroup = '' AND D.KidsMinistryGroup <> '' THEN D.KidsMinistryGroup ELSE Common_ContactInternal.KidsMinistryGroup END
		FROM	Common_ContactInternal, 
				Common_ContactInternal D
		WHERE	Common_ContactInternal.Contact_ID = @MasterContact_ID
		AND		D.Contact_ID = @DuplicateContact_ID
	END
	
	-- If the duplicate has a Church record, but master doesn't, temporarily copy the duplicate's record to the master
	-- so we can transfer the dependant records.
	IF @InternalDuplicate = 1 AND @InternalMaster = 0
		EXEC sp_Common_DedupeCopyRecord @DuplicateContact_ID, @MasterContact_ID, 'Common_ContactInternal'
	
	
	-- Transfer the dependant records
	IF @InternalDuplicate = 1 BEGIN
		UPDATE Church_247Prayer SET Contact_ID = @MasterContact_ID WHERE Contact_ID = @DuplicateContact_ID
		UPDATE Church_ChurchStatusChangeRequest SET Contact_ID = @MasterContact_ID WHERE Contact_ID = @DuplicateContact_ID
		UPDATE Church_ContactAdditionalDecision SET Contact_ID = @MasterContact_ID WHERE Contact_ID = @DuplicateContact_ID
		UPDATE Church_ContactComment SET Contact_ID = @MasterContact_ID WHERE Contact_ID = @DuplicateContact_ID
		UPDATE Church_ContactCourseEnrolment SET Contact_ID = @MasterContact_ID WHERE Contact_ID = @DuplicateContact_ID
		UPDATE Church_ContactCourseInstance SET Contact_ID = @MasterContact_ID WHERE Contact_ID = @DuplicateContact_ID
		UPDATE Church_ContactKidsMinistryCode SET Contact_ID = @MasterContact_ID WHERE Contact_ID = @DuplicateContact_ID
		UPDATE Church_ContactRole SET Contact_ID = @MasterContact_ID WHERE Contact_ID = @DuplicateContact_ID
		UPDATE Church_CourseEnrolmentAttendance SET Contact_ID = @MasterContact_ID WHERE Contact_ID = @DuplicateContact_ID
		UPDATE Church_KidsMinistryAttendance SET Contact_ID = @MasterContact_ID WHERE Contact_ID = @DuplicateContact_ID
		UPDATE Church_KidsMinistryClearance SET Contact_ID = @MasterContact_ID WHERE Contact_ID = @DuplicateContact_ID
		UPDATE Church_KidsMinistryClearance SET ClearanceContact_ID = @MasterContact_ID WHERE ClearanceContact_ID = @DuplicateContact_ID
		UPDATE Church_KidsMinistryToiletBreak SET Contact_ID = @MasterContact_ID WHERE Contact_ID = @DuplicateContact_ID
		UPDATE Church_NPNC SET Contact_ID = @MasterContact_ID WHERE Contact_ID = @DuplicateContact_ID
		UPDATE Church_RegionalPastor SET Contact_ID = @MasterContact_ID WHERE Contact_ID = @DuplicateContact_ID
		UPDATE Church_ULGContact SET Contact_ID = @MasterContact_ID WHERE Contact_ID = @DuplicateContact_ID
		
		
		--PCR Contact is a special case. If there are duplicate PCR_ID/Contact_ID records, we must combine them.
		UPDATE	Church_PCRContact
		SET		SundayAttendance = CASE WHEN Church_PCRContact.SundayAttendance = 1 OR D.SundayAttendance=1 then 1 ELSE 0 END,
				ULGAttendance = CASE WHEN Church_PCRContact.ULGAttendance = 1 OR D.ULGAttendance=1 then 1 ELSE 0 END,
				BoomAttendance = CASE WHEN Church_PCRContact.BoomAttendance = 1 OR D.BoomAttendance=1 then 1 ELSE 0 END,
				FNLAttendance = CASE WHEN Church_PCRContact.FNLAttendance = 1 OR D.FNLAttendance=1 then 1 ELSE 0 END,
				[Call] = dbo.MaximumInt(Church_PCRContact.[Call], D.[Call]),
				Visit = dbo.MaximumInt(Church_PCRContact.Visit, D.Visit),
				Other = dbo.MaximumInt(Church_PCRContact.Other, D.Other)
		FROM	Church_PCRContact D,
				Church_PCRContact
		WHERE	D.PCR_ID = Church_PCRContact.PCR_ID
		AND		D.Contact_ID = @DuplicateContact_ID
		AND		Church_PCRContact.Contact_ID = @MasterContact_ID
		AND		D.PCRContact_ID IN (SELECT PCRContact_ID FROM Church_PCRContact WHERE Contact_ID = @DuplicateContact_ID)
		AND		Church_PCRContact.PCRContact_ID IN (SELECT PCRContact_ID FROM Church_PCRContact WHERE Contact_ID = @MasterContact_ID)
		
		DELETE FROM Church_PCRContact WHERE Contact_ID = @DuplicateContact_ID AND PCR_ID IN (SELECT PCR_ID FROM Church_PCRContact WHERE Contact_ID = @MasterContact_ID)
		UPDATE Church_PCRContact SET Contact_ID = @MasterContact_ID WHERE Contact_ID = @DuplicateContact_ID
		
	END
	
	/*****************************************************************************
	 *****************************************************************************
	 **																			**
	 **								  EXTERNAL 									**
	 **																			**
	 *****************************************************************************
	 *****************************************************************************/
	 
	DECLARE @ExternalMaster BIT,
			@ExternalDuplicate BIT,
			@ExternalEventsMaster BIT,
			@ExternalEventsDuplicate BIT,
			@ExternalRevolutionMaster BIT,
			@ExternalRevolutionDuplicate BIT
			
	SET		@ExternalMaster = CASE WHEN (SELECT COUNT(*) FROM Common_ContactExternal WHERE Contact_ID = @MasterContact_ID) > 0 THEN 1 ELSE 0 END
	SET		@ExternalDuplicate = CASE WHEN (SELECT COUNT(*) FROM Common_ContactExternal WHERE Contact_ID = @DuplicateContact_ID) > 0 THEN 1 ELSE 0 END
	SET		@ExternalEventsMaster = CASE WHEN (SELECT COUNT(*) FROM Common_ContactExternalEvents WHERE Contact_ID = @MasterContact_ID) > 0 THEN 1 ELSE 0 END
	SET		@ExternalEventsDuplicate = CASE WHEN (SELECT COUNT(*) FROM Common_ContactExternalEvents WHERE Contact_ID = @DuplicateContact_ID) > 0 THEN 1 ELSE 0 END
	SET		@ExternalRevolutionMaster = CASE WHEN (SELECT COUNT(*) FROM Common_ContactExternalRevolution WHERE Contact_ID = @MasterContact_ID) > 0 THEN 1 ELSE 0 END
	SET		@ExternalRevolutionDuplicate = CASE WHEN (SELECT COUNT(*) FROM Common_ContactExternalRevolution WHERE Contact_ID = @DuplicateContact_ID) > 0 THEN 1 ELSE 0 END


	-- If the duplicate has a External record, but master doesn't, temporarily copy the duplicate's record to the master
	-- so we can transfer the dependant records.
	IF @ExternalDuplicate = 1 AND @ExternalMaster = 0
		EXEC sp_Common_DedupeCopyRecord @DuplicateContact_ID, @MasterContact_ID, 'Common_ContactExternal'
	IF @ExternalEventsDuplicate = 1 AND @ExternalEventsMaster = 0
		EXEC sp_Common_DedupeCopyRecord @DuplicateContact_ID, @MasterContact_ID, 'Common_ContactExternalEvents'
	IF @ExternalRevolutionDuplicate = 1 AND @ExternalRevolutionMaster = 0
		EXEC sp_Common_DedupeCopyRecord @DuplicateContact_ID, @MasterContact_ID, 'Common_ContactExternalRevolution'
	



	/*****************************************************************************
	 *****************************************************************************
	 **																			**
	 **								EXTERNAL RECORD								**
	 **																			**
	 *****************************************************************************
	 *****************************************************************************/
	 	
	 IF		@ExternalMaster = 1 AND @ExternalDuplicate = 1 BEGIN 
		UPDATE	Common_ContactExternal
		SET		ChurchName = CASE WHEN Common_ContactExternal.ChurchName = '' AND D.ChurchName <> '' THEN D.ChurchName ELSE Common_ContactExternal.ChurchName END,
				Denomination_ID = CASE WHEN Common_ContactExternal.Denomination_ID is null AND D.Denomination_ID is not null THEN D.Denomination_ID ELSE Common_ContactExternal.Denomination_ID END,
				SeniorPastorName = CASE WHEN Common_ContactExternal.SeniorPastorName = '' AND D.SeniorPastorName <> '' THEN D.SeniorPastorName ELSE Common_ContactExternal.SeniorPastorName END,
				ChurchAddress1 = CASE WHEN Common_ContactExternal.ChurchAddress1 = '' AND D.ChurchAddress1 <> '' THEN D.ChurchAddress1 ELSE Common_ContactExternal.ChurchAddress1 END,
				ChurchAddress2 = CASE WHEN Common_ContactExternal.ChurchAddress2 = '' AND D.ChurchAddress2 <> '' THEN D.ChurchAddress2 ELSE Common_ContactExternal.ChurchAddress2 END,
				ChurchSuburb = CASE WHEN Common_ContactExternal.ChurchSuburb = '' AND D.ChurchSuburb <> '' THEN D.ChurchSuburb ELSE Common_ContactExternal.ChurchSuburb END,
				ChurchPostcode = CASE WHEN Common_ContactExternal.ChurchPostcode = '' AND D.ChurchPostcode <> '' THEN D.ChurchPostcode ELSE Common_ContactExternal.ChurchPostcode END,
				ChurchState_ID = CASE WHEN Common_ContactExternal.ChurchState_ID is null AND D.ChurchState_ID is not null THEN D.ChurchState_ID ELSE Common_ContactExternal.ChurchState_ID END,
				ChurchStateOther = CASE WHEN Common_ContactExternal.ChurchStateOther = '' AND D.ChurchStateOther <> '' THEN D.ChurchStateOther ELSE Common_ContactExternal.ChurchStateOther END,
				ChurchCountry_ID = CASE WHEN Common_ContactExternal.ChurchCountry_ID is null AND D.ChurchCountry_ID is not null THEN D.ChurchCountry_ID ELSE Common_ContactExternal.ChurchCountry_ID END,
				ChurchWebsite = CASE WHEN Common_ContactExternal.ChurchWebsite = '' AND D.ChurchWebsite <> '' THEN D.ChurchWebsite ELSE Common_ContactExternal.ChurchWebsite END,
				LeaderName = CASE WHEN Common_ContactExternal.LeaderName = '' AND D.ChurchWebsite <> '' THEN D.LeaderName ELSE Common_ContactExternal.LeaderName END,
				ChurchNumberOfPeople = CASE WHEN Common_ContactExternal.ChurchNumberOfPeople = '' AND D.ChurchNumberOfPeople <> '' THEN D.ChurchNumberOfPeople ELSE Common_ContactExternal.ChurchNumberOfPeople END,
				ChurchPhone = CASE WHEN Common_ContactExternal.ChurchPhone = '' AND D.ChurchPhone <> '' THEN D.ChurchPhone ELSE Common_ContactExternal.ChurchPhone END,
				ChurchEmail = CASE WHEN Common_ContactExternal.ChurchEmail = '' AND D.ChurchEmail <> '' THEN D.ChurchEmail ELSE Common_ContactExternal.ChurchEmail END
		FROM	Common_ContactExternal, 
				Common_ContactExternal D
		WHERE	Common_ContactExternal.Contact_ID = @MasterContact_ID
		AND		D.Contact_ID = @DuplicateContact_ID
	END
	
	-- Transfer the dependant records
	IF @ExternalDuplicate = 1 BEGIN
		UPDATE External_ContactChurchInvolvement SET Contact_ID = @MasterContact_ID WHERE Contact_ID = @DuplicateContact_ID
	END
	
	

	/*****************************************************************************
	 *****************************************************************************
	 **																			**
	 **						  EXTERNAL EVENTS RECORD							**
	 **																			**
	 *****************************************************************************
	 *****************************************************************************/	
		
	IF @ExternalEventsMaster = 1 AND @ExternalEventsDuplicate = 1 BEGIN 
		UPDATE	Common_ContactExternalEvents
		SET		EmergencyContactName = CASE WHEN Common_ContactExternalEvents.EmergencyContactName = '' AND D.EmergencyContactName <> '' THEN D.EmergencyContactName ELSE Common_ContactExternalEvents.EmergencyContactName END,
				EmergencyContactPhone = CASE WHEN Common_ContactExternalEvents.EmergencyContactPhone = '' AND D.EmergencyContactPhone <> '' THEN D.EmergencyContactPhone ELSE Common_ContactExternalEvents.EmergencyContactPhone END,
				EmergencyContactRelationship = CASE WHEN Common_ContactExternalEvents.EmergencyContactRelationship = '' AND D.EmergencyContactRelationship <> '' THEN D.EmergencyContactRelationship ELSE Common_ContactExternalEvents.EmergencyContactRelationship END,
				MedicalInformation = CASE WHEN Common_ContactExternalEvents.MedicalInformation = '' AND D.MedicalInformation <> '' THEN D.MedicalInformation ELSE Common_ContactExternalEvents.MedicalInformation END,
				MedicalAllergies = CASE WHEN Common_ContactExternalEvents.MedicalAllergies = '' AND D.MedicalAllergies <> '' THEN D.MedicalAllergies ELSE Common_ContactExternalEvents.MedicalAllergies END,
				AccessibilityInformation = CASE WHEN Common_ContactExternalEvents.AccessibilityInformation = '' AND D.AccessibilityInformation <> '' THEN D.AccessibilityInformation ELSE Common_ContactExternalEvents.AccessibilityInformation END
		FROM	Common_ContactExternalEvents, 
				Common_ContactExternalEvents D
		WHERE	Common_ContactExternalEvents.Contact_ID = @MasterContact_ID
		AND		D.Contact_ID = @DuplicateContact_ID
	END
	
	
	-- Transfer the dependant records
	IF @ExternalEventsDuplicate = 1 BEGIN
		UPDATE Events_ContactPayment SET Contact_ID = @MasterContact_ID WHERE Contact_ID = @DuplicateContact_ID
		UPDATE Events_GroupContact SET Contact_ID = @MasterContact_ID WHERE Contact_ID = @DuplicateContact_ID
		UPDATE Events_Registration SET Contact_ID = @MasterContact_ID WHERE Contact_ID = @DuplicateContact_ID
		UPDATE Events_WaitingList SET Contact_ID = @MasterContact_ID WHERE Contact_ID = @DuplicateContact_ID
	END




	/*****************************************************************************
	 *****************************************************************************
	 **																			**
	 **						 EXTERNAL REVOLUTION RECORD							**
	 **																			**
	 *****************************************************************************
	 *****************************************************************************/	
	
	IF @ExternalRevolutionMaster = 1 AND @ExternalRevolutionDuplicate = 1 BEGIN 
		UPDATE	Common_ContactExternalRevolution
		SET		ReferredBy_ID = CASE WHEN Common_ContactExternalRevolution.ReferredBy_ID IS NULL AND D.ReferredBy_ID IS NOT NULL THEN D.ReferredBy_ID ELSE Common_ContactExternalRevolution.ReferredBy_ID END,
				AcceptedTermsAndConditions = CASE WHEN Common_ContactExternalRevolution.AcceptedTermsAndConditions = 0 AND D.AcceptedTermsAndConditions = 1 THEN D.AcceptedTermsAndConditions ELSE Common_ContactExternalRevolution.AcceptedTermsAndConditions END
		FROM	Common_ContactExternalRevolution, 
				Common_ContactExternalRevolution D
		WHERE	Common_ContactExternalRevolution.Contact_ID = @MasterContact_ID
		AND		D.Contact_ID = @DuplicateContact_ID
	END

	IF @ExternalRevolutionDuplicate = 1 BEGIN
		UPDATE Revolution_ActivationCode SET Contact_ID = @MasterContact_ID WHERE Contact_ID = @DuplicateContact_ID
		UPDATE Revolution_ContactSubscription SET Contact_ID = @MasterContact_ID WHERE Contact_ID = @DuplicateContact_ID
	END




	/*****************************************************************************
	 *****************************************************************************
	 **																			**
	 **						       GROUP RECORD									**
	 **																			**
	 *****************************************************************************
	 *****************************************************************************/	

	DECLARE @GroupMaster BIT,
			@GroupDuplicate BIT
			
	SET		@GroupMaster = CASE WHEN (SELECT COUNT(*) FROM Events_Group WHERE GroupLeader_ID = @MasterContact_ID) > 0 THEN 1 ELSE 0 END
	SET		@GroupDuplicate = CASE WHEN (SELECT COUNT(*) FROM Events_Group WHERE GroupLeader_ID = @DuplicateContact_ID) > 0 THEN 1 ELSE 0 END

	IF	@GroupDuplicate = 1 BEGIN
		IF @GroupMaster = 0
			UPDATE Events_Group SET GroupLeader_ID = @MasterContact_ID WHERE GroupLeader_ID = @DuplicateContact_ID
		ELSE BEGIN
			--Combine the two groups
			UPDATE Events_GroupBulkPurchase SET GroupLeader_ID = @MasterContact_ID WHERE GroupLeader_ID = @DuplicateContact_ID
			UPDATE Events_GroupBulkRegistration SET GroupLeader_ID = @MasterContact_ID WHERE GroupLeader_ID = @DuplicateContact_ID
			UPDATE Events_GroupContact SET GroupLeader_ID = @MasterContact_ID WHERE GroupLeader_ID = @DuplicateContact_ID
			UPDATE Events_GroupHistory SET GroupLeader_ID = @MasterContact_ID WHERE GroupLeader_ID = @DuplicateContact_ID
			UPDATE Events_GroupNotes SET GroupLeader_ID = @MasterContact_ID WHERE GroupLeader_ID = @DuplicateContact_ID
			UPDATE Events_GroupPayment SET GroupLeader_ID = @MasterContact_ID WHERE GroupLeader_ID = @DuplicateContact_ID
			UPDATE Events_GroupPromoRequest SET GroupLeader_ID = @MasterContact_ID WHERE GroupLeader_ID = @DuplicateContact_ID
			UPDATE Events_Registration SET GroupLeader_ID = @MasterContact_ID WHERE GroupLeader_ID = @DuplicateContact_ID
			DELETE FROM Events_Group WHERE GroupLeader_ID = @DuplicateContact_ID
		END
	END
	
	
	
	

	/*****************************************************************************
	 *****************************************************************************
	 **																			**
	 **						   ADD/UPDATE USER RECORDS							**
	 **																			**
	 *****************************************************************************
	 *****************************************************************************/	
	

	--Temporarily duplicate the User Record if the master does not have one, but duplicate does....
	IF	(SELECT COUNT(*) FROM Common_UserDetail WHERE Contact_ID = @DuplicateContact_ID) = 1 AND
		(SELECT COUNT(*) FROM Common_UserDetail WHERE Contact_ID = @MasterContact_ID) = 0 BEGIN
		EXEC sp_Common_DedupeCopyRecord @DuplicateContact_ID, @MasterContact_ID, 'Common_UserDetail'
	END
	
	--Common
	UPDATE Common_Contact SET [ModifiedBy_ID] = @MasterContact_ID WHERE [ModifiedBy_ID] = @DuplicateContact_ID
	UPDATE Common_Contact SET [CreatedBy_ID] = @MasterContact_ID WHERE [CreatedBy_ID] = @DuplicateContact_ID
	UPDATE Common_ContactHistory SET [User_ID] = @MasterContact_ID WHERE [User_ID] = @DuplicateContact_ID
	UPDATE Common_ContactDatabaseRole SET Contact_ID = @MasterContact_ID WHERE Contact_ID = @DuplicateContact_ID
	
	--Church
	UPDATE Church_ChurchStatusChangeRequest SET RequestedBy_ID = @MasterContact_ID WHERE RequestedBy_ID = @DuplicateContact_ID
	UPDATE Church_ContactAdditionalDecision SET EnteredBy_ID = @MasterContact_ID WHERE EnteredBy_ID = @DuplicateContact_ID
	UPDATE Church_ContactComment SET CommentBy_ID = @MasterContact_ID WHERE CommentBy_ID = @DuplicateContact_ID
	UPDATE Church_CourseEnrolmentAttendance SET MarkedOffBy_ID = @MasterContact_ID WHERE MarkedOffBy_ID = @DuplicateContact_ID
	UPDATE Church_KidsMinistryAttendance SET CheckedInByParent_ID = @MasterContact_ID WHERE CheckedInByParent_ID = @DuplicateContact_ID
	UPDATE Church_KidsMinistryAttendance SET CheckedOutByParent_ID = @MasterContact_ID WHERE CheckedOutByParent_ID = @DuplicateContact_ID
	UPDATE Church_KidsMinistryAttendance SET CheckedInByLeader_ID = @MasterContact_ID WHERE CheckedInByLeader_ID = @DuplicateContact_ID
	UPDATE Church_KidsMinistryAttendance SET CheckedOutByLeader_ID = @MasterContact_ID WHERE CheckedOutByLeader_ID = @DuplicateContact_ID
	UPDATE Church_KidsMinistryClearance SET ClearanceContact_ID = @MasterContact_ID WHERE ClearanceContact_ID = @DuplicateContact_ID
	UPDATE Church_KidsMinistryToiletBreak SET Leader1_ID = @MasterContact_ID WHERE Leader1_ID = @DuplicateContact_ID
	UPDATE Church_KidsMinistryToiletBreak SET Leader2_ID = @MasterContact_ID WHERE Leader2_ID = @DuplicateContact_ID
	UPDATE Church_NCSummaryStats SET EnteredBy_ID = @MasterContact_ID WHERE EnteredBy_ID = @DuplicateContact_ID
	UPDATE Church_NPNC SET NCTeamMember_ID = @MasterContact_ID WHERE NCTeamMember_ID = @DuplicateContact_ID
	UPDATE Church_NPNC SET Carer_ID = @MasterContact_ID WHERE Carer_ID = @DuplicateContact_ID
	UPDATE Church_NPNCWelcomePacksIssued SET EnteredBy_ID = @MasterContact_ID WHERE EnteredBy_ID = @DuplicateContact_ID
	UPDATE Church_NPSummaryStats SET EnteredBy_ID = @MasterContact_ID WHERE EnteredBy_ID = @DuplicateContact_ID

	--Events
	UPDATE Events_Group SET [ModifiedBy_ID] = @MasterContact_ID WHERE [ModifiedBy_ID] = @DuplicateContact_ID
	UPDATE Events_Group SET [CreatedBy_ID] = @MasterContact_ID WHERE [CreatedBy_ID] = @DuplicateContact_ID
	UPDATE Events_GroupNotes SET [User_ID]= @MasterContact_ID WHERE [User_ID] = @DuplicateContact_ID
	UPDATE Events_POSTransaction SET [User_ID] = @MasterContact_ID WHERE [User_ID] = @DuplicateContact_ID
	UPDATE Events_ContactPayment SET [PaymentBy_ID] = @MasterContact_ID WHERE [PaymentBy_ID] = @DuplicateContact_ID
	UPDATE Events_GroupHistory SET [User_ID] = @MasterContact_ID WHERE [User_ID] = @DuplicateContact_ID
	UPDATE Events_Registration SET [RegisteredBy_ID] = @MasterContact_ID WHERE [RegisteredBy_ID] = @DuplicateContact_ID
	UPDATE Events_GroupPayment SET [PaymentBy_ID] = @MasterContact_ID WHERE [PaymentBy_ID] = @DuplicateContact_ID
	
	--Revolution
	UPDATE Revolution_ActivationCode SET ActivatedBy_ID = @MasterContact_ID WHERE ActivatedBy_ID = @DuplicateContact_ID
	UPDATE Revolution_ContactSubscription SET SubscribedBy_ID = @MasterContact_ID WHERE SubscribedBy_ID = @DuplicateContact_ID
	UPDATE Revolution_SubscriptionPayment SET PaymentBy_ID = @MasterContact_ID WHERE PaymentBy_ID = @DuplicateContact_ID


	DELETE FROM Common_UserDetail WHERE Contact_ID = @DuplicateContact_ID	
		
		
		
		

	--Final Cleanup
	DELETE FROM Common_WebSession WHERE Contact_ID = @DuplicateContact_ID
	
	DELETE FROM Common_ContactExternalRevolution WHERE Contact_ID = @DuplicateContact_ID
	DELETE FROM Common_ContactExternalEvents WHERE Contact_ID = @DuplicateContact_ID
	DELETE FROM Common_ContactExternal WHERE Contact_ID = @DuplicateContact_ID
	
	DELETE FROM Common_ContactInternal WHERE Contact_ID = @DuplicateContact_ID
	
	DELETE FROM Common_ContactMailingList WHERE Contact_ID = @DuplicateContact_ID
	DELETE FROM Common_ContactMailingListType WHERE Contact_ID = @DuplicateContact_ID
	
	DELETE FROM Common_Contact WHERE Contact_ID = @DuplicateContact_ID

END

COMMIT TRANSACTION
GO
