SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_DedupeCopyRecord]

@Old_ID		INT,
@New_ID		INT,
@Table		NVARCHAR(50)

AS

DECLARE @SQL	NVARCHAR(MAX),
		@SQL2	NVARCHAR(MAX),
		@Column	NVARCHAR(100),
		@IDName	NVARCHAR(100),
		@Index	INT

DECLARE ColumnCursor CURSOR FOR
	SELECT Name, ColID FROM syscolumns WHERE ID in (SELECT ID FROM sysobjects where name = @Table)
	
OPEN ColumnCursor

SET @SQL = 'INSERT INTO ' + @Table + '('

FETCH NEXT FROM ColumnCursor INTO @Column, @Index
WHILE (@@FETCH_STATUS = 0) BEGIN
	SET @SQL = @SQL + @Column + ','
	IF @Index=1 BEGIN
		SET @SQL2 = CONVERT(NVARCHAR,@New_ID) + ','
		SET @IDName = @Column
	END
	ELSE
		SET @SQL2 = @SQL2 + @Column + ','
	FETCH NEXT FROM ColumnCursor INTO @Column, @Index
END

CLOSE ColumnCursor
DEALLOCATE ColumnCursor

SET @SQL2 = LEFT(@SQL2,LEN(@SQL2)-1)
SET @SQL = LEFT(@SQL,LEN(@SQL)-1) + ')' + CHAR(13) + 'SELECT ' + @SQL2 + ' FROM ' + @Table + ' WHERE ' + @IDName + ' = ' + CONVERT(NVARCHAR,@Old_ID)

IF (SELECT COUNT(*) FROM syscolumns WHERE ID=(SELECT ID FROM sysobjects WHERE name = @Table) AND status=128) > 0
	SET @SQL = 'SET IDENTITY_INSERT ' + @Table + ' ON' + CHAR(13) + CHAR(13) + @SQL + CHAR(13) + 'SET IDENTITY_INSERT ' + @Table + ' OFF' + CHAR(13)

EXEC(@SQL)
GO
