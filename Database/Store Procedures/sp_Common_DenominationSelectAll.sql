SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_DenominationSelectAll]

AS

SELECT 		Denomination_ID, 
			Denomination 
FROM 		Common_Denomination
ORDER BY 	CASE WHEN Denomination = 'Other' THEN 2 ELSE 1 END, 
			Denomination
GO
