SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_MailingListSubscriptionChange]

@Contact_ID							INT,
@MailingList_ID						INT,
@MailingListContactMethod_ID		INT,
@Date								DATETIME,
@SubscriptionActive					BIT,
@MailingListUnsubscribeReason_ID	INT,
@User_ID							INT

AS

INSERT Common_MailingListSubscriptionChange
(
	MailingList_ID,
	MailingListContactMethod_ID,
	Contact_ID,
	[Date],
	SubscriptionActive,
	MailingListUnsubscribeReason_ID,
	User_ID
)
VALUES
(
	@MailingList_ID,
	@MailingListContactMethod_ID,
	@Contact_ID,
	@Date,
	@SubscriptionActive,
	@MailingListUnsubscribeReason_ID,
	@User_ID
)
GO
