SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_ReportDatabaseRole]

@Campus_ID			INT = null,
@Module				NVARCHAR(50),
@DatabaseRole_ID	INT

AS

SELECT		Contact_ID,
			[Name],
			LastName,
			ISNULL((SELECT M.[Name] FROM Church_Ministry M, Common_ContactInternal CI WHERE M.Ministry_ID = CI.Ministry_ID AND CI.Contact_ID = U.Contact_ID),'') as Ministry,
			CONVERT(NVARCHAR(500),'') as Roles
INTO		#Users
FROM		vw_Users U
WHERE		(Campus_ID = @Campus_ID OR @Campus_ID IS NULL)
AND			Inactive = 0



DECLARE	@Contact_ID		INT
DECLARE	@Name			NVARCHAR(100)
DECLARE	@CursorModule	NVARCHAR(100)

DECLARE RoleCursor CURSOR FOR
	SELECT Contact_ID, [Name], Module 
	FROM Common_ContactDatabaseRole DR, Common_DatabaseRole R 
	WHERE DR.DatabaseRole_ID = R.DatabaseRole_ID AND (Module = @Module OR @Module = '')  and (R.DatabaseRole_ID = @DatabaseRole_ID OR @DatabaseRole_ID IS NULL)
	ORDER BY [Name] 

OPEN RoleCursor

FETCH NEXT FROM RoleCursor INTO @Contact_ID, @Name, @CursorModule

WHILE (@@Fetch_Status = 0) BEGIN

	UPDATE #Users SET Roles = Roles + @Name + ' (' + @CursorModule + '), ' WHERE Contact_ID = @Contact_ID

	FETCH NEXT FROM RoleCursor INTO @Contact_ID, @Name, @CursorModule
END

CLOSE RoleCursor
DEALLOCATE RoleCursor

UPDATE		#Users
SET			Roles = LEFT(Roles, LEN(Roles) - 1)
WHERE		LEN(Roles) > 0

SELECT		Contact_ID,
			[Name],
			[Ministry],
			[Roles]
FROM		#Users
WHERE		Roles <> ''
ORDER BY	LastName
GO
