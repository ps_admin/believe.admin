SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_UserAccountReset]

@Contact_ID		INT

AS

UPDATE	Common_UserDetail
SET		InvalidPasswordAttempts = 0
WHERE	Contact_ID = @Contact_ID
GO
