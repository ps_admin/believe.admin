SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_UserDetailDelete]

@Contact_ID		INT

AS

DELETE
FROM	Common_UserDetail
WHERE	Contact_ID = @Contact_ID
GO
