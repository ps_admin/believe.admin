SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_UserDetailUpdate]

@Contact_ID					INT,
@LastPasswordChangeDate		DATETIME,
@InvalidPasswordAttempts	INT,
@PasswordResetRequired		BIT,
@Inactive					BIT

AS


	SET NOCOUNT ON

	IF ((SELECT COUNT(*) FROM Common_UserDetail WHERE Contact_ID = @Contact_ID) = 0) BEGIN

		INSERT INTO [Common_UserDetail]
		(
			[Contact_ID],
			[LastPasswordChangeDate],
			[InvalidPasswordAttempts],
			[PasswordResetRequired],
			[Inactive]
		)
		VALUES
		(
			@Contact_ID,
			@LastPasswordChangeDate,
			@InvalidPasswordAttempts,
			@PasswordResetRequired,
			@Inactive
		)

	END
	ELSE BEGIN

		UPDATE [Common_UserDetail]
		SET
			[LastPasswordChangeDate] = @LastPasswordChangeDate,
			[InvalidPasswordAttempts] = @InvalidPasswordAttempts,
			[PasswordResetRequired] = @PasswordResetRequired,
			[Inactive] = @Inactive
		WHERE 
			[Contact_ID] = @Contact_ID
	
	END
GO
