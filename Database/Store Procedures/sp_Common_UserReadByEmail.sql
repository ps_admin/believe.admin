SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_UserReadByEmail]

@Email		NVARCHAR(80)

AS

SELECT	* 
FROM	vw_Users 
WHERE	Email = @Email

SELECT	D.* 
FROM	vw_Users U,
		Common_ContactDatabaseRole D
WHERE	U.Contact_ID = D.Contact_ID
AND		U.Email = @Email
GO
