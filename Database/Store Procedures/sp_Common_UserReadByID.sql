SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_UserReadByID]

@Contact_ID		INT

AS

SELECT	* 
FROM	vw_Users 
WHERE	Contact_ID = @Contact_ID

SELECT	* 
FROM	Common_ContactDatabaseRole
WHERE	Contact_ID = @Contact_ID
GO
