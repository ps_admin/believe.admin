SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_UsersSelectAll]

AS

SELECT	* 
FROM	vw_Users

SELECT	D.* 
FROM	vw_Users U,
		Common_ContactDatabaseRole D
WHERE	U.Contact_ID = D.Contact_ID
GO
