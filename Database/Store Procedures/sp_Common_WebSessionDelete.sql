SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_WebSessionDelete]

	@WebSession_ID		INT

AS

DELETE
FROM	Common_WebSession
WHERE	WebSession_ID = @WebSession_ID
GO
