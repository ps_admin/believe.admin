SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_WebSessionInsert]
(
	@Contact_ID int,
	@SessionStartDate datetime,
	@SessionLastActivity datetime,
	@ClientIP nvarchar(100),
	@SessionID nvarchar(100),
	@SessionTimeout int
)
AS
	SET NOCOUNT ON

	INSERT INTO [Common_WebSession]
	(
		[Contact_ID],
		[SessionStartDate],
		[SessionLastActivity],
		[ClientIP],
		[SessionID],
		[SessionTimeout]
	)
	VALUES
	(
		@Contact_ID,
		@SessionStartDate,
		@SessionLastActivity,
		@ClientIP,
		@SessionID,
		@SessionTimeout
	)

	SELECT WebSession_ID = SCOPE_IDENTITY();
GO
