SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_WebSessionRead]

@SessionID	NVARCHAR(100)

AS

SELECT	* 
FROM	Common_WebSession 
WHERE	SessionID = @SessionID

--Expire any old sessions:
DELETE	
FROM	Common_WebSession
WHERE	SessionLastActivity < DATEADD(mm,-2,dbo.GetRelativeDate())
GO
