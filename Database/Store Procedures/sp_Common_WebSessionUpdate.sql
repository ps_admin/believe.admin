SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Common_WebSessionUpdate]
(
	@WebSession_ID int,
	@Contact_ID int,
	@SessionStartDate datetime,
	@SessionLastActivity datetime,
	@ClientIP nvarchar(100),
	@SessionID nvarchar(100),
	@SessionTimeout int
)
AS
	SET NOCOUNT ON

	UPDATE	[Common_WebSession]
	SET		[Contact_ID] = @Contact_ID,
			[SessionStartDate] = @SessionStartDate,
			[SessionLastActivity] = @SessionLastActivity,
			[ClientIP] = @ClientIP,
			[SessionID] = @SessionID,
			[SessionTimeout] = @SessionTimeout
	WHERE	[WebSession_ID] = @WebSession_ID
GO
