SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_AccommodationDelete]

@Accommodation_ID		INT

AS

UPDATE	Events_Registration
SET		Accommodation_ID = NULL
WHERE	Accommodation_ID = @Accommodation_ID

DELETE 
FROM 	Events_Accommodation
WHERE	Accommodation_ID = @Accommodation_ID
GO
