SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_AccommodationInsert]

@AccommodationVenueName			VARCHAR(30),
@AccommodationVenueLocation		VARCHAR(50),
@Venue_ID						INT,
@MaxPeople						INT

AS

INSERT INTO Events_Accommodation
(
	AccommodationVenueName,
	AccommodationVenueLocation,
	Venue_ID,
	MaxPeople
)
VALUES
(
	@AccommodationVenueName,
	@AccommodationVenueLocation,
	@Venue_ID,
	@MaxPeople
)

SELECT SCOPE_IDENTITY() AS Accommodation_ID
GO
