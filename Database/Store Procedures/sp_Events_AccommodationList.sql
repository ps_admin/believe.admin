SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_AccommodationList]

@Conference_ID		INT

AS

SELECT 		Re.Registration_ID,
			C.FirstName + ' ' + C.LastName as RegistrantName,
			C.Contact_ID,
			Re.Venue_ID,
			Re.Accommodation_ID,
			C.Gender,
			CE.ChurchName,
			S.State
FROM 		Common_Contact C,
			Common_ContactExternal CE,
			Events_Registration Re,
			Events_Venue V,
			Common_GeneralState S
WHERE		C.Contact_ID = Re.Contact_ID
AND			C.Contact_ID = CE.Contact_ID
AND			Re.Accommodation =1
AND			Re.Venue_ID = V.Venue_ID
AND			C.State_ID = S.State_ID
AND			V.Conference_ID = @Conference_ID
AND			CE.Deleted = 0
AND			Re.GroupLeader_ID IS NULL
ORDER BY	CE.ChurchName,
			C.LastName
GO
