SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_AccommodationListGroups]

@Conference_ID		INT

AS

SELECT 		G.GroupLeader_ID, 
			G.GroupName, 
			S.State,
			Re.Venue_ID,
			Re.Accommodation_ID,
			SUM(CASE WHEN C.Gender = 'Male' then 1 ELSE 0 END) as Male,
			SUM(CASE WHEN C.Gender = 'Female' then 1 ELSE 0 END) as Female					
FROM		Events_Group G,
 			Common_Contact C,
 			Common_Contact CG,
 			Common_ContactExternal CE,
			Events_Registration Re,
			Events_Venue V,
			Common_GeneralState S
WHERE		Re.GroupLeader_ID = G.GroupLeader_ID
AND			C.Contact_ID = Re.Contact_ID
AND			C.Contact_ID = CE.Contact_ID
AND			G.GroupLeader_ID = CG.Contact_ID
AND			Re.Accommodation = 1 
AND			Re.Venue_ID = V.Venue_ID
AND			CG.State_ID = S.State_ID
AND			CE.Deleted = 0
AND			V.Conference_ID = @Conference_ID
GROUP BY	G.GroupLeader_ID, 
			G.GroupName, 
			S.State,
			Re.Venue_ID,
			Re.Accommodation_ID
ORDER BY 	GroupName
GO
