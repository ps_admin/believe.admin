SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_AccommodationReport]

@Venue_ID 				INT,
@Accommodation_ID 		INT

AS

SELECT 		G.GroupLeader_ID, 
			G.GroupName, 
			GC.FirstName + ' ' + GC.LastName AS GroupLeader,
			GC.Mobile,
			S.State,
			SUM(CASE WHEN C.Gender = 'Male' then 1 ELSE 0 END) as Male,
			SUM(CASE WHEN C.Gender = 'Female' then 1 ELSE 0 END) as Female,
			COUNT(*) AS Total				
FROM		Events_Group G,
			Common_GeneralState S,
 			Common_Contact C,
 			Common_Contact GC,
 			Common_ContactExternal CE,
			Events_Registration Re
WHERE		G.GroupLeader_ID = Re.GroupLeader_ID
AND			G.GroupLeader_ID = GC.Contact_ID
AND			GC.State_ID = S.State_ID
AND			C.Contact_ID = Re.Contact_ID
AND			C.Contact_ID = CE.Contact_ID
AND			CE.Deleted = 0
AND			Re.Venue_Id = @Venue_ID
AND			Re.Accommodation = 1
GROUP BY	G.GroupLeader_ID, 
			G.GroupName, 
			S.State,
			GC.FirstName + ' ' + GC.LastName,
			GC.Mobile
ORDER BY 	GroupName

SELECT 		C.Contact_ID,
			C.FirstName + ' ' + C.LastName as RegistrantName,
			C.Gender,
			CE.ChurchName,
			S.State,
			C.Mobile as Phone
FROM 		Common_Contact C,
			Common_ContactExternal CE,
			Events_Registration Re,
			Common_GeneralState S
WHERE		C.Contact_ID = CE.Contact_ID
AND			C.Contact_ID = Re.Contact_ID
AND			C.State_ID = S.State_ID
AND			Re.Accommodation = 1
AND			CE.Deleted = 0
AND			Re.GroupLeader_ID IS NULL
AND			Re.Venue_ID = @Venue_ID
AND			Re.Accommodation_ID = @Accommodation_ID
ORDER BY	CE.ChurchName,
			C.LastName
GO
