SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_AccommodationUpdate]

@Accommodation_ID				INT,
@AccommodationVenueName			VARCHAR(30),
@AccommodationVenueLocation		VARCHAR(50),
@Venue_ID						INT,
@MaxPeople						INT

AS

UPDATE 	Events_Accommodation
SET		AccommodationVenueName = @AccommodationVenueName,
		AccommodationVenueLocation = @AccommodationVenueLocation,
		Venue_ID = @Venue_ID,
		MaxPeople = @MaxPeople
WHERE	Accommodation_ID = @Accommodation_ID
GO
