SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_ContactPaymentDelete]
(
	@ContactPayment_ID		INT,
	@User_ID int = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin

	DELETE 
	FROM 	Events_ContactPayment
	WHERE	ContactPayment_ID = @ContactPayment_ID
GO
