SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_ContactPaymentUpdate]
(
	@ContactPayment_ID		INT,
	@Contact_ID				INT,
	@Conference_ID			INT,
	@PaymentType_ID			INT,
	@PaymentAmount			MONEY,
	@CCNumber				VARCHAR(20),
	@CCExpiry				VARCHAR(5),
	@CCName					VARCHAR(50),
	@CCPhone				VARCHAR(50),
	@CCManual				BIT,
	@CCTransactionRef		VARCHAR(20),
	@CCRefund				BIT,
	@ChequeDrawer			VARCHAR(50),
	@ChequeBank				VARCHAR(30),
	@ChequeBranch			VARCHAR(30),
	@PaypalTransactionRef	VARCHAR(20) = '',
	@Comment				VARCHAR(200),
	@PaymentDate			DATETIME,
	@PaymentBy_ID			INT,
	@BankAccount_ID			INT,
	@User_ID				INT = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin
		
	UPDATE 	Events_ContactPayment
	SET		Contact_ID = @Contact_ID,
			Conference_ID = @Conference_ID,
			PaymentType_ID = @PaymentType_ID,
			PaymentAmount = @PaymentAmount,
			CCNumber = @CCNumber,
			CCExpiry = @CCExpiry,
			CCName = @CCName,
			CCPhone = @CCPhone,
			CCManual = @CCManual,
			CCTransactionRef = @CCTransactionRef,
			CCRefund = @CCRefund,
			ChequeDrawer = @ChequeDrawer,
			ChequeBank = @ChequeBank,
			ChequeBranch = @ChequeBranch,
			PaypalTransactionRef = @PaypalTransactionRef,
			Comment = @Comment,
			PaymentDate = @PaymentDate,
			PaymentBy_ID = @PaymentBy_ID,
			BankAccount_ID = @BankAccount_ID
	WHERE	ContactPayment_ID = @ContactPayment_ID
GO
