SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_ContactSessionOrderRead]

@Contact_ID		INT

AS

SELECT		PT.POSTransaction_ID,
			PT.TransactionDate,
			REPLACE(PT.ShipTo, CHAR(10), ', ') as ShipTo,
			P.Title,
			PTL.Quantity as QuantityOrdered,
			PTL.QuantitySupplied,
			PTL.Price,
			PTL.Discount,
			PTL.Net
FROM		Events_POSTransaction PT,
			Events_POSTransactionLine PTL,
			Events_Product P
WHERE		PT.POSTransaction_ID = PTL.POSTransaction_ID
AND			PTL.Product_ID = P.Product_ID
AND			PT.TransactionType = 2
AND			PT.Contact_ID = @Contact_ID
GO
