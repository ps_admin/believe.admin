SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_CrecheChildInsert]
(
	@Registration_ID int,
	@CrecheChildRegistrationType_ID int,
	@ChildFirstName varchar(20),
	@ChildLastName varchar(20),
	@ChildDateOfBirth datetime = NULL,
	@ChildAge varchar(2),
	@CareRequired varchar(20),
	@ChildMedicalInformation varchar(200),
	@ChildAllergies varchar(200),
	@CrecheChildSchoolGrade_ID int = NULL,
	@ChildrensChurchPastor nvarchar(50) = '',
	@DropOffPickUpName nvarchar(50) = '',
	@DropOffPickUpRelationship nvarchar(50) = '',
	@DropOffPickUpContactNumber nvarchar(50) = '',
	@ConsentFormReturned bit = 0,
	@DateAdded datetime,
	@User_ID INT = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin
		
	SET NOCOUNT ON

	INSERT INTO [Events_CrecheChild]
	(
		[Registration_ID],
		[CrecheChildRegistrationType_ID],
		[ChildFirstName],
		[ChildLastName],
		[ChildDateOfBirth],
		[ChildAge],
		[CareRequired],
		[ChildMedicalInformation],
		[ChildAllergies],
		[CrecheChildSchoolGrade_ID],
		[ChildrensChurchPastor],
		[DropOffPickUpName],
		[DropOffPickUpRelationship],
		[DropOffPickUpContactNumber],
		[ConsentFormReturned],
		[DateAdded]
	)
	VALUES
	(
		@Registration_ID,
		@CrecheChildRegistrationType_ID,
		@ChildFirstName,
		@ChildLastName,
		@ChildDateOfBirth,
		@ChildAge,
		@CareRequired,
		@ChildMedicalInformation,
		@ChildAllergies,
		@CrecheChildSchoolGrade_ID,
		@ChildrensChurchPastor,
		@DropOffPickUpName,
		@DropOffPickUpRelationship,
		@DropOffPickUpContactNumber,
		@ConsentFormReturned,
		@DateAdded
	)

	SELECT CrecheChild_ID = SCOPE_IDENTITY();
GO
