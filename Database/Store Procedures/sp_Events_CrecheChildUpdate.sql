SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_CrecheChildUpdate]
(
	@CrecheChild_ID int,
	@Registration_ID int,
	@CrecheChildRegistrationType_ID int,
	@ChildFirstName varchar(20),
	@ChildLastName varchar(20),
	@ChildDateOfBirth datetime = NULL,
	@ChildAge varchar(2),
	@CareRequired varchar(20),
	@ChildMedicalInformation varchar(200),
	@ChildAllergies varchar(200),
	@CrecheChildSchoolGrade_ID int = NULL,
	@ChildrensChurchPastor nvarchar(50) = '',
	@DropOffPickUpName nvarchar(50) = '',
	@DropOffPickUpRelationship nvarchar(50) = '',
	@DropOffPickUpContactNumber nvarchar(50) = '',
	@ConsentFormReturned bit = 0,
	@DateAdded datetime,
	@User_ID INT = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin
		
	SET NOCOUNT ON
	
	UPDATE [Events_CrecheChild]
	SET
		[Registration_ID] = @Registration_ID,
		[CrecheChildRegistrationType_ID] = @CrecheChildRegistrationType_ID,
		[ChildFirstName] = @ChildFirstName,
		[ChildLastName] = @ChildLastName,
		[ChildDateOfBirth] = @ChildDateOfBirth,
		[ChildAge] = @ChildAge,
		[CareRequired] = @CareRequired,
		[ChildMedicalInformation] = @ChildMedicalInformation,
		[ChildAllergies] = @ChildAllergies,
		[CrecheChildSchoolGrade_ID] = @CrecheChildSchoolGrade_ID,
		[ChildrensChurchPastor] = @ChildrensChurchPastor,
		[DropOffPickUpName] = @DropOffPickUpName,
		[DropOffPickUpRelationship] = @DropOffPickUpRelationship,
		[DropOffPickUpContactNumber] = @DropOffPickUpContactNumber,
		[ConsentFormReturned] = @ConsentFormReturned
		--Don't Update the DateAdded
	WHERE 
		[CrecheChild_ID] = @CrecheChild_ID
GO
