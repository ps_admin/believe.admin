SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_GroupBulkPurchaseInsert]
(
	@GroupLeader_ID int,
	@Venue_ID int,
	@AccommodationQuantity int,
	@CateringQuantity int,
	@LeadershipBreakfastQuantity int,
	@PlanetkidsQuantity int = 0,
	@PlanetkidsRegistrationType_ID int = null,
	@DateAdded datetime,
	@User_ID INT
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin
		
	SET NOCOUNT ON

	INSERT INTO [Events_GroupBulkPurchase]
	(
		[GroupLeader_ID],
		[Venue_ID],
		[AccommodationQuantity],
		[CateringQuantity],
		[LeadershipBreakfastQuantity],
		[PlanetkidsQuantity],
		[PlanetkidsRegistrationType_ID],
		[DateAdded]
	)
	VALUES
	(
		@GroupLeader_ID,
		@Venue_ID,
		@AccommodationQuantity,
		@CateringQuantity,
		@LeadershipBreakfastQuantity,
		@PlanetkidsQuantity,
		@PlanetkidsRegistrationType_ID,
		@DateAdded
	)

	SELECT GroupBulkPurchase_ID = SCOPE_IDENTITY();
GO
