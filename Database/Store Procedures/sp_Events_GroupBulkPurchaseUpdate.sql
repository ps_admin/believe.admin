SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_GroupBulkPurchaseUpdate]
(
	@GroupBulkPurchase_ID int,
	@GroupLeader_ID int,
	@Venue_ID int,
	@AccommodationQuantity int,
	@CateringQuantity int,
	@LeadershipBreakfastQuantity int,
	@PlanetkidsQuantity int,
	@PlanetkidsRegistrationType_ID int = NULL,
	@DateAdded datetime,
	@User_ID INT
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin
		
	SET NOCOUNT ON
	
	UPDATE [Events_GroupBulkPurchase]
	SET
		[GroupLeader_ID] = @GroupLeader_ID,
		[Venue_ID] = @Venue_ID,
		[AccommodationQuantity] = @AccommodationQuantity,
		[CateringQuantity] = @CateringQuantity,
		[LeadershipBreakfastQuantity] = @LeadershipBreakfastQuantity,
		[PlanetkidsQuantity] = @PlanetkidsQuantity,
		[PlanetkidsRegistrationType_ID] = @PlanetkidsRegistrationType_ID,
		[DateAdded] = @DateAdded
	WHERE 
		[GroupBulkPurchase_ID] = @GroupBulkPurchase_ID
GO
