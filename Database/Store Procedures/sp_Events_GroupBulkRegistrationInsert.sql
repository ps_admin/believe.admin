SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_GroupBulkRegistrationInsert]
(
	@GroupLeader_ID int,
	@Venue_ID int,
	@RegistrationType_ID int,
	@Quantity int,
	@DateAdded datetime,
	@User_ID INT = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin
		
	SET NOCOUNT ON

	INSERT INTO [Events_GroupBulkRegistration]
	(
		[GroupLeader_ID],
		[Venue_ID],
		[RegistrationType_ID],
		[Quantity],
		[DateAdded]
	)
	VALUES
	(
		@GroupLeader_ID,
		@Venue_ID,
		@RegistrationType_ID,
		@Quantity,
		@DateAdded
	)

	SELECT GroupBulkRegistration_ID = SCOPE_IDENTITY();
GO
