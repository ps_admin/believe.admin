SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_GroupBulkRegistrationUpdate]
(
	@GroupBulkRegistration_ID int,
	@GroupLeader_ID int,
	@Venue_ID int,
	@RegistrationType_ID int,
	@Quantity int,
	@DateAdded datetime,
	@User_ID INT = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin
		
	SET NOCOUNT ON
	
	UPDATE [Events_GroupBulkRegistration]
	SET
		[GroupLeader_ID] = @GroupLeader_ID,
		[Venue_ID] = @Venue_ID,
		[RegistrationType_ID] = @RegistrationType_ID,
		[Quantity] = @Quantity,
		[DateAdded] = @DateAdded
	WHERE 
		[GroupBulkRegistration_ID] = @GroupBulkRegistration_ID
GO
