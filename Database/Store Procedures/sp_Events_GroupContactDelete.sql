SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_GroupContactDelete]
(
	@GroupContact_ID int
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Events_GroupContact]
	WHERE  [GroupContact_ID] = @GroupContact_ID
GO
