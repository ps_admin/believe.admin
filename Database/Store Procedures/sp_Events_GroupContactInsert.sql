SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_GroupContactInsert]
(
	@GroupLeader_ID int,
	@Contact_ID int
)
AS
	SET NOCOUNT ON

	INSERT INTO [Events_GroupContact]
	(
		[GroupLeader_ID],
		[Contact_ID]
	)
	VALUES
	(
		@GroupLeader_ID,
		@Contact_ID
	)

	SELECT GroupContact_ID = SCOPE_IDENTITY();
GO
