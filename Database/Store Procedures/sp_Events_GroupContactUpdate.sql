SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_GroupContactUpdate]
(
	@GroupContact_ID int,
	@GroupLeader_ID int,
	@Contact_ID int
)
AS
	SET NOCOUNT ON
	
	UPDATE [Events_GroupContact]
	SET
		[GroupLeader_ID] = @GroupLeader_ID,
		[Contact_ID] = @Contact_ID
	WHERE 
		[GroupContact_ID] = @GroupContact_ID
GO
