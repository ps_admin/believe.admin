SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_GroupHistoryInsert]

@GroupLeader_ID		INT,
@Conference_ID		INT,
@Venue_ID			INT,
@DateChanged		DATETIME,
@User_ID			INT,
@Action				NVARCHAR(20),
@Item				NVARCHAR(200),
@Table				NVARCHAR(20),
@FieldName			NVARCHAR(50),
@OldValue			NVARCHAR(MAX),
@OldText			NVARCHAR(MAX),
@NewValue			NVARCHAR(MAX),
@NewText			NVARCHAR(MAX)

AS

INSERT Events_GroupHistory 
(
	GroupLeader_ID,
	Conference_ID,
	Venue_ID,
	DateChanged,
	[User_ID],
	[Action],
	Item,	
	[Table],
	FieldName,
	OldValue,
	OldText,
	NewValue,
	NewText
)
VALUES
(
	@GroupLeader_ID,
	@Conference_ID,
	@Venue_ID,
	dbo.GetRelativeDate(),
	@User_ID,
	@Action,
	@Item,	
	@Table,
	@FieldName,
	@OldValue,
	@OldText,
	@NewValue,
	@NewText
)
GO
