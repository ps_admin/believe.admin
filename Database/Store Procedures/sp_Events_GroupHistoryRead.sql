SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_GroupHistoryRead]

@GroupLeader_ID		INT

AS

SELECT 	GH.*,
		U.FirstName + ' ' + U.LastName as UserName,
		(SELECT VenueName FROM Events_Venue WHERE Venue_ID = GH.Venue_ID) as Venue,
		(SELECT ConferenceName FROM Events_Conference WHERE Conference_ID = GH.Conference_ID) as Conference
FROM	Events_GroupHistory GH,
		Events_Group G,
		vw_Users U
WHERE 	GH.GroupLeader_ID = G.GroupLeader_ID
AND		GH.User_ID = U.Contact_ID
AND		G.GroupLeader_ID = @GroupLeader_ID
AND		G.Deleted = 0
GO
