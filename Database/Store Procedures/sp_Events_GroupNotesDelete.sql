SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_GroupNotesDelete]
(
	@GroupNotes_ID int
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Events_GroupNotes]
	WHERE  [GroupNotes_ID] = @GroupNotes_ID
GO
