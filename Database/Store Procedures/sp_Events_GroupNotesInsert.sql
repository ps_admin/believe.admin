SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_GroupNotesInsert]
(
	@GroupLeader_ID int,
	@DateAdded datetime,
	@User_ID int,
	@Notes ntext
)
AS
	SET NOCOUNT ON

	INSERT INTO [Events_GroupNotes]
	(
		[GroupLeader_ID],
		[DateAdded],
		[User_ID],
		[Notes]
	)
	VALUES
	(
		@GroupLeader_ID,
		@DateAdded,
		@User_ID,
		@Notes
	)

	SELECT GroupNotes_ID = SCOPE_IDENTITY();
GO
