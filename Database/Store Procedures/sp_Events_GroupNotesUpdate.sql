SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_GroupNotesUpdate]
(
	@GroupNotes_ID int,
	@GroupLeader_ID int,
	@DateAdded datetime,
	@User_ID int,
	@Notes ntext
)
AS
	SET NOCOUNT ON
	
	UPDATE [Events_GroupNotes]
	SET
		[GroupLeader_ID] = @GroupLeader_ID,
		[DateAdded] = @DateAdded,
		[User_ID] = @User_ID,
		[Notes] = @Notes
	WHERE 
		[GroupNotes_ID] = @GroupNotes_ID
GO
