SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_GroupPaymentDelete]
(
	@GroupPayment_ID int,
	@User_ID INT = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin
		
	SET NOCOUNT ON

	DELETE 
	FROM   [Events_GroupPayment]
	WHERE  [GroupPayment_ID] = @GroupPayment_ID
GO
