SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_GroupPaymentInsert]
(
	@GroupLeader_ID int,
	@Conference_ID int,
	@PaymentType_ID int,
	@PaymentAmount money,
	@CCNumber varchar(20),
	@CCExpiry varchar(5),
	@CCName varchar(50),
	@CCPhone varchar(20),
	@CCManual bit,
	@CCTransactionRef varchar(20),
	@CCRefund bit,
	@ChequeDrawer varchar(50),
	@ChequeBank varchar(30),
	@ChequeBranch varchar(30),
	@PaypalTransactionRef	VARCHAR(20) = '',
	@Comment varchar(200),
	@PaymentDate datetime,
	@PaymentBy_ID int,
	@BankAccount_ID int,
	@User_ID INT = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin
		
	SET NOCOUNT ON

	INSERT INTO [Events_GroupPayment]
	(
		[GroupLeader_ID],
		[Conference_ID],
		[PaymentType_ID],
		[PaymentAmount],
		[CCNumber],
		[CCExpiry],
		[CCName],
		[CCPhone],
		[CCManual],
		[CCTransactionRef],
		[CCRefund],
		[ChequeDrawer],
		[ChequeBank],
		[ChequeBranch],
		[PaypalTransactionRef],
		[Comment],
		[PaymentDate],
		[PaymentBy_ID],
		[BankAccount_ID]
	)
	VALUES
	(
		@GroupLeader_ID,
		@Conference_ID,
		@PaymentType_ID,
		@PaymentAmount,
		@CCNumber,
		@CCExpiry,
		@CCName,
		@CCPhone,
		@CCManual,
		@CCTransactionRef,
		@CCRefund,
		@ChequeDrawer,
		@ChequeBank,
		@ChequeBranch,
		@PaypalTransactionRef,
		@Comment,
		@PaymentDate,
		@PaymentBy_ID,
		@BankAccount_ID
	)

	SELECT GroupPayment_ID = SCOPE_IDENTITY();
GO
