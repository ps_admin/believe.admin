SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_GroupPaymentUpdate]
(
	@GroupPayment_ID int,
	@GroupLeader_ID int,
	@Conference_ID int,
	@PaymentType_ID int,
	@PaymentAmount money,
	@CCNumber varchar(20),
	@CCExpiry varchar(5),
	@CCName varchar(50),
	@CCPhone varchar(20),
	@CCManual bit,
	@CCTransactionRef varchar(20),
	@CCRefund bit,
	@ChequeDrawer varchar(50),
	@ChequeBank varchar(30),
	@ChequeBranch varchar(30),
	@PaypalTransactionRef	VARCHAR(20) = '',
	@Comment varchar(200),
	@PaymentDate datetime,
	@PaymentBy_ID int,
	@BankAccount_ID int,
	@User_ID INT = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin
		
	SET NOCOUNT ON
	
	UPDATE [Events_GroupPayment]
	SET
		[GroupLeader_ID] = @GroupLeader_ID,
		[Conference_ID] = @Conference_ID,
		[PaymentType_ID] = @PaymentType_ID,
		[PaymentAmount] = @PaymentAmount,
		[CCNumber] = @CCNumber,
		[CCExpiry] = @CCExpiry,
		[CCName] = @CCName,
		[CCPhone] = @CCPhone,
		[CCManual] = @CCManual,
		[CCTransactionRef] = @CCTransactionRef,
		[CCRefund] = @CCRefund,
		[ChequeDrawer] = @ChequeDrawer,
		[ChequeBank] = @ChequeBank,
		[ChequeBranch] = @ChequeBranch,
		[PaypalTransactionRef] = @PaypalTransactionRef,
		[Comment] = @Comment,
		[PaymentDate] = @PaymentDate,
		[PaymentBy_ID] = @PaymentBy_ID,
		[BankAccount_ID] = @BankAccount_ID
	WHERE 
		[GroupPayment_ID] = @GroupPayment_ID
GO
