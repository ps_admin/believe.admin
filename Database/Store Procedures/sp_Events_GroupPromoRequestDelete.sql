SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_GroupPromoRequestDelete]
(
	@GroupPromoRequest_ID int
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Events_GroupPromoRequest]
	WHERE  [GroupPromoRequest_ID] = @GroupPromoRequest_ID
GO
