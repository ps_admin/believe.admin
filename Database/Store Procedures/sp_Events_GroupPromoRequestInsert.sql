SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_GroupPromoRequestInsert]
(
	@GroupLeader_ID int,
	@Conference_ID int,
	@GroupPromo_ID int,
	@DateAdded datetime,
	@User_ID int
)
AS
	SET NOCOUNT ON

	INSERT INTO [Events_GroupPromoRequest]
	(
		[GroupLeader_ID],
		[Conference_ID],
		[GroupPromo_ID],
		[DateAdded],
		[User_ID]
	)
	VALUES
	(
		@GroupLeader_ID,
		@Conference_ID,
		@GroupPromo_ID,
		@DateAdded,
		@User_ID
	)

	SELECT GroupPromoRequest_ID = SCOPE_IDENTITY();
GO
