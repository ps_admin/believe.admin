SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_GroupPromoRequestUpdate]
(
	@GroupPromoRequest_ID int,
	@GroupLeader_ID int,
	@Conference_ID int,
	@GroupPromo_ID int,
	@DateAdded datetime,
	@User_ID int
)
AS
	SET NOCOUNT ON
	
	UPDATE [Events_GroupPromoRequest]
	SET
		[GroupLeader_ID] = @GroupLeader_ID,
		[Conference_ID] = @Conference_ID,
		[GroupPromo_ID] = @GroupPromo_ID,
		[DateAdded] = @DateAdded,
		[User_ID] = @User_ID
	WHERE 
		[GroupPromoRequest_ID] = @GroupPromoRequest_ID
GO
