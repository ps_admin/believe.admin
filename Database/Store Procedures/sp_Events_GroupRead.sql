SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_GroupRead]

@GroupLeader_ID		INT

AS

-- SELECT Group Data
SELECT 		*
FROM 		Events_Group
WHERE 		GroupLeader_ID = @GroupLeader_ID

-- SELECT Group Notes
SELECT 		*
FROM 		Events_GroupNotes
WHERE 		GroupLeader_ID = @GroupLeader_ID

-- SELECT Group Payment Data
SELECT 		GP.* 
FROM 		Events_GroupPayment GP
WHERE 		GroupLeader_ID = @GroupLeader_ID

-- SELECT Group Promo Request Data
SELECT 		GP.* 
FROM 		Events_GroupPromoRequest GP
WHERE 		GroupLeader_ID = @GroupLeader_ID

-- SELECT Group Bulk Registration Data
SELECT		GBR.*
FROM		Events_GroupBulkRegistration GBR
WHERE		GroupLeader_ID = @GroupLeader_ID


-- SELECT Group Bulk Registration Option Data
SELECT		GBP.*
FROM		Events_GroupBulkPurchase GBP
WHERE		GroupLeader_ID = @GroupLeader_ID

-- SELECT Contact List into Temporary Table
SELECT	DISTINCT CE.Contact_ID 
INTO	#t1 
FROM	Events_GroupContact GC, 
		Common_ContactExternal CE 
WHERE	GC.Contact_ID = CE.Contact_ID 
AND		CE.Deleted = 0 
AND		GC.GroupLeader_ID = @GroupLeader_ID

INSERT	#t1 
SELECT	DISTINCT CE.Contact_ID 
FROM	Events_Registration Re, 
		Common_ContactExternal CE 
WHERE	Re.Contact_ID = CE.Contact_ID 
AND		CE.Deleted = 0 AND Re.GroupLeader_ID = @GroupLeader_ID
AND		CE.Contact_ID NOT IN (SELECT Contact_ID FROM #t1)

INSERT	#t1 
SELECT	GroupLeader_ID 
FROM	Events_Group 
WHERE	GroupLeader_ID = @GroupLeader_ID
AND		GroupLeader_ID NOT IN (SELECT Contact_ID FROM #t1)


-- SELECT Contact Data
SELECT 		@GroupLeader_ID as GroupLeader_ID, C.*
FROM 		Common_Contact C
WHERE 		C.Contact_ID IN (SELECT Contact_ID FROM #t1)


-- UPDATE Contact Mailing List Records
INSERT Common_ContactMailingList(Contact_ID, MailingList_ID, SubscriptionActive)	
SELECT	Contact_ID, MailingList_ID, 0
FROM	Common_MailingList ML, #t1
WHERE NOT EXISTS (	SELECT	MailingList_ID
					FROM	Common_ContactMailingList 
					WHERE	Contact_ID = #t1.Contact_ID
					AND		MailingList_ID = ML.MailingList_ID)


-- UPDATE Contact Mailing List Records
INSERT Common_ContactMailingListType(Contact_ID, MailingListType_ID, SubscriptionActive)	
SELECT	Contact_ID, MailingListType_ID, 0
FROM	Common_MailingListType MLT, #t1
WHERE NOT EXISTS (	SELECT	MailingListType_ID
					FROM	Common_ContactMailingListType
					WHERE	Contact_ID = #t1.Contact_ID
					AND		MailingListType_ID = MLT.MailingListType_ID)


-- SELECT Contact Mailing List Data
SELECT		@GroupLeader_ID, CML.*
FROM		Common_ContactMailingList CML
WHERE		CML.Contact_ID IN (SELECT Contact_ID FROM #t1)

-- SELECT Contact Mailing List Type Data
SELECT		@GroupLeader_ID, CMLT.*
FROM		Common_ContactMailingListType CMLT
WHERE		CMLT.Contact_ID IN (SELECT Contact_ID FROM #t1)

-- SELECT Contact Database Role Data
SELECT		@GroupLeader_ID as Group_ID, CDR.*
FROM		Common_ContactDatabaseRole CDR
WHERE		CDR.Contact_ID IN (SELECT Contact_ID FROM #t1)

-- SELECT ContactExternal Data
SELECT 		@GroupLeader_ID as Group_ID, CE.*
FROM 		Common_ContactExternal CE
WHERE 		CE.Contact_ID IN (SELECT Contact_ID FROM #t1)

-- SELECT ContactExternalEvents Data
SELECT 		@GroupLeader_ID as Group_ID, CE.*
FROM 		Common_ContactExternalEvents CE
WHERE 		CE.Contact_ID IN (SELECT Contact_ID FROM #t1)

-- SELECT Contact Payment Data
SELECT 		CP.* 
FROM 		Events_ContactPayment CP
WHERE		CP.Contact_ID IN (SELECT Contact_ID FROM #t1)

-- SELECT Registration Data
SELECT 		Re.*
FROM 		Events_Registration Re
WHERE 		Re.Contact_ID IN (SELECT Contact_ID FROM #t1)

-- SELECT CrecheChild Data for Contact.
SELECT 		CC.*
FROM		Events_CrecheChild CC, 
			Events_Registration Re
WHERE 		CC.Registration_ID = Re.Registration_ID 
AND	 		Re.Contact_ID IN (SELECT Contact_ID FROM #t1)

-- SELECT Contact's Pre-orders
SELECT		P.*
FROM		Events_PreOrder P,
			Events_Registration Re
WHERE 		P.Registration_ID = Re.Registration_ID
AND			Re.Contact_ID IN (SELECT Contact_ID FROM #t1)

-- SELECT Registration Option Data
SELECT 		RO.*
FROM 		Events_RegistrationOption RO,
			Events_Registration Re
WHERE 		RO.Registration_ID = Re.Registration_ID
AND			Re.Contact_ID IN (SELECT Contact_ID FROM #t1)

-- SELECT Contact's Waiting List
SELECT		WL.*
FROM		Events_WaitingList WL
WHERE 		WL.Contact_ID IN (SELECT Contact_ID FROM #t1)

DROP TABLE #t1
GO
