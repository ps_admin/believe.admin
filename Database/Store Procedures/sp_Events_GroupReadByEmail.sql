SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_GroupReadByEmail]

@Email		NVARCHAR(80)

AS

SELECT		GroupLeader_ID
FROM		Events_Group G,
			Common_Contact C
WHERE		G.GroupLeader_ID = C.Contact_ID
AND			C.Email = @Email
GO
