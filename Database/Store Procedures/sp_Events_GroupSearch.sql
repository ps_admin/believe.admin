SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_GroupSearch]

@GroupLeader_ID			INT = 0,
@GroupName				VARCHAR(30) = '',
@Church					VARCHAR(50) = '',
@YouthLeaderName		VARCHAR(50) = '',
@GroupLeaderFirstName	VARCHAR(20) = '',
@GroupLeaderLastName	VARCHAR(20) = '',
@GroupLeaderPhone		VARCHAR(20) = '',
@GroupLeaderSuburb		VARCHAR(30) = '',
@State_ID				INT = null,
@GroupLeaderEmail		VARCHAR(20) = '',
@GroupLeaderMobile		VARCHAR(20) = '',
@GroupSize_ID			INT = null,
@GroupCreditStatus_ID	INT = null,
@Conference_ID			INT = null,
@Venue_ID				INT = null,
@Registered				INT = 0,
@CreditDebit			INT = 0,
@UnallocatedRegos		INT = 0,
@GroupPromo_ID			INT = 0,
@UserState_ID			INT = null

AS

DECLARE @SQL NVARCHAR(MAX)

SELECT 	CONVERT(INT,G.GroupLeader_ID) as GroupLeader_ID,
		G.GroupName,
		ISNULL((SELECT ChurchName FROM Common_ContactExternal WHERE Contact_ID = ''),'''') as Church,
		G.YouthLeaderName,
		C.FirstName,
		C.LastName,
		C.Address1 + C.Address2 as Address,
		C.Suburb,
		CASE WHEN S.State = 'Other' THEN StateOther ELSE S.State END as State,
		C.Postcode,
		C.Email,
		C.Phone,
		C.Mobile,
		Co.Country,
		G.CreationDate,
		G.Comments,
		ISNULL((SELECT [Name] FROM Events_GroupSize WHERE GroupSize_ID = G.GroupSize_ID),'') as GroupSize,
		CONVERT(INT,0) AS NumRegistered,
		Co.Currency,
		CONVERT(MONEY,0) as Amount,
		CONVERT(MONEY,0) as Payments,
		CONVERT(MONEY,0) as Balance,
		CONVERT(INT,0) as CreditDebit,
		CONVERT(INT,0) as UnAllocatedRegos
INTO	#t1
FROM	Events_Group G, 
		Common_Contact C
			LEFT JOIN Common_GeneralState S ON C.State_ID = S.State_ID
			LEFT JOIN Common_GeneralCountry Co ON C.Country_ID = Co.Country_ID
WHERE 	G.GroupLeader_ID = C.Contact_ID
AND		1=0

SET @SQL = '
	INSERT #t1
	SELECT	CONVERT(INT,G.GroupLeader_ID) as GroupLeader_ID,
			G.GroupName,
			ISNULL((SELECT ChurchName FROM Common_ContactExternal WHERE Contact_ID = ' + CONVERT(NVARCHAR,@GroupLeader_ID) + '),'''') as Church,
			G.YouthLeaderName,
			C.FirstName,
			C.LastName,
			C.Address1 + C.Address2 as Address,
			C.Suburb,
			CASE WHEN S.State = ''Other'' THEN StateOther ELSE S.State END as State,
			C.Postcode,
			C.Email,
			C.Phone,
			C.Mobile,
			Co.Country,
			G.CreationDate,
			G.Comments,
			ISNULL((SELECT [Name] FROM Events_GroupSize WHERE GroupSize_ID = G.GroupSize_ID),'''') as GroupSize,
			CONVERT(INT,0) AS NumRegistered,
			' + CASE WHEN @Conference_ID IS NULL THEN '''''' ELSE 'ISNULL((SELECT Currency FROM Common_GeneralCountry GC, Events_Conference C WHERE GC.Country_ID = C.Country_ID AND C.Conference_ID = ' + CONVERT(VARCHAR, @Conference_ID) + '),'''')' END + ' AS Currency,
			CONVERT(MONEY,0) as Amount,
			CONVERT(MONEY,0) as Payments,
			CONVERT(MONEY,0) as Balance,
			CONVERT(INT,0) as CreditDebit,
			' + CASE WHEN @Conference_ID IS NULL THEN '0' ELSE '(SELECT SUM(UnAllocated) FROM vw_Events_GroupSummaryRegistrationView GRV WHERE GRV.GroupLeader_ID = G.GroupLeader_ID AND GRV.Conference_ID = ' + CONVERT(VARCHAR,@Conference_ID) + ')' END + ' as UnAllocatedRegos
	FROM	Events_Group G, 
			Common_Contact C
				LEFT JOIN Common_GeneralState S ON C.State_ID = S.State_ID
				LEFT JOIN Common_GeneralCountry Co ON C.Country_ID = Co.Country_ID
	WHERE 	G.GroupLeader_ID = C.Contact_ID
	AND		G.Deleted = 0'

IF @GroupLeader_ID > 0			SET @SQL = @SQL + '	AND	(G.GroupLeader_ID = ' + CONVERT(VARCHAR,@GroupLeader_ID) +')'
IF @GroupName <> '' 			SET @SQL = @SQL + '	AND	(G.GroupName LIKE ''%' + REPLACE(@GroupName,'''','''''') +'%'')'
IF @Church <> '' 				SET @SQL = @SQL + '	AND	(ISNULL((SELECT ChurchName FROM Common_ContactExternal WHERE Contact_ID = C.Contact_ID),'''') LIKE ''%' + REPLACE(@Church,'''','''''') +'%'')'
IF @YouthLeaderName <> '' 		SET @SQL = @SQL + '	AND	(G.YouthLeaderName LIKE ''%' + REPLACE(@YouthLeaderName,'''','''''') +'%'')'
IF @GroupLeaderFirstName <> '' 	SET @SQL = @SQL + '	AND	(C.FirstName LIKE ''%' + REPLACE(@GroupLeaderFirstName,'''','''''') +'%'' )'
IF @GroupLeaderLastName <> '' 	SET @SQL = @SQL + '	AND	(C.LastName LIKE ''%' + REPLACE(@GroupLeaderLastName,'''','''''') +'%'' )'
IF @GroupLeaderPhone <> '' 		SET @SQL = @SQL + '	AND	(C.PhoneHome LIKE ''%' + REPLACE(@GroupLeaderPhone,'''','''''') +'%'')'
IF @GroupLeaderSuburb <> '' 	SET @SQL = @SQL + '	AND	(C.Suburb LIKE ''%' + REPLACE(@GroupLeaderSuburb,'''','''''') +'%'')'
IF @GroupLeaderEmail <> '' 		SET @SQL = @SQL + '	AND	(C.Email LIKE ''%' + REPLACE(@GroupLeaderEmail,'''','''''') +'%'')'
IF @GroupLeaderMobile <> '' 	SET @SQL = @SQL + '	AND	(C.Mobile LIKE ''%' + REPLACE(@GroupLeaderMobile,'''','''''') +'%'')'


IF @State_ID IS NOT NULL
	SET @SQL = @SQL + '	AND	C.State_ID = ' + CONVERT(VARCHAR,@State_ID)
IF @UserState_ID IS NOT NULL
	SET @SQL = @SQL + '	AND	(C.State_ID = ' + CONVERT(VARCHAR,@UserState_ID) + ' OR G.GroupLeader_ID IN (SELECT DISTINCT Re.GroupLeader_ID FROM Events_Registration Re, Events_Venue V WHERE Re.Venue_Id = V.Venue_ID AND V.State_ID = ' + CONVERT(VARCHAR,@UserState_ID) + '))'
IF @GroupSize_ID IS NOT NULL
	SET @SQL = @SQL + '	AND	G.GroupSize_ID = ' + CONVERT(VARCHAR, @GroupSize_ID)
IF @GroupCreditStatus_ID IS NOT NULL
	SET @SQL = @SQL + '	AND	G.GroupType_ID = ' + CONVERT(VARCHAR, @GroupCreditStatus_ID)
IF @GroupPromo_ID > 0		 	
	SET @SQL = @SQL + '	AND (G.GroupLeader_ID IN (SELECT DISTINCT GroupLeader_ID FROM Events_GroupPromoRequest PR WHERE PR.GroupPromo_ID = ' + CONVERT(VARCHAR,@GroupPromo_ID) + ' AND PR.Conference_ID = ' + CONVERT(NVARCHAR, @Conference_ID) + '))'
print @sql
EXEC(@SQL)

UPDATE #t1 SET Amount = ISNULL((SELECT SUM(TotalCost) FROM vw_Events_GroupSummaryView RCV WHERE RCV.GroupLeader_ID = #t1.GroupLeader_ID and Conference_ID = @Conference_ID),0),
			   Payments =  ISNULL((SELECT SUM(PaymentAmount) FROM Events_GroupPayment GP where GP.GroupLeader_ID = #t1.GroupLeader_ID and Conference_ID = @Conference_ID),0)

UPDATE #t1 SET CreditDebit = CASE WHEN Amount > Payments THEN 1 WHEN Amount < Payments THEN 2 ELSE 0 END,
			   Balance = Amount - Payments

DELETE FROM #t1 WHERE CreditDebit <> 0 AND @CreditDebit = 1 AND @Conference_ID IS NOT NULL
DELETE FROM #t1 WHERE CreditDebit <> 1 AND @CreditDebit = 2 AND @Conference_ID IS NOT NULL
DELETE FROM #t1 WHERE CreditDebit <> 2 AND @CreditDebit = 3 AND @Conference_ID IS NOT NULL

DELETE FROM #t1 WHERE UnAllocatedRegos = 0 AND @UnallocatedRegos = 1 AND @Conference_ID IS NOT NULL
DELETE FROM #t1 WHERE UnAllocatedRegos <> 0 AND @UnallocatedRegos = 2 AND @Conference_ID IS NOT NULL

DELETE FROM #t1 WHERE @Registered = 1 AND ISNULL((SELECT SUM(TotalRegistrations) FROM vw_Events_GroupsummaryRegistrationView WHERE GroupLeader_ID = #t1.GroupLeader_ID AND Conference_ID = @Conference_ID),0) = 0 
DELETE FROM #t1 WHERE @Registered = 2 AND ISNULL((SELECT SUM(TotalRegistrations) FROM vw_Events_GroupsummaryRegistrationView WHERE GroupLeader_ID = #t1.GroupLeader_ID AND Conference_ID = @Conference_ID),0) <> 0

IF @Venue_ID IS NOT NULL
	UPDATE #t1 SET NumRegistered = 
		(SELECT SUM(TotalRegistrations) FROM vw_Events_GroupsummaryRegistrationView
		WHERE GroupLeader_ID = #t1.GroupLeader_ID AND Venue_ID = @Venue_ID)


SELECT * FROM #t1
GO
