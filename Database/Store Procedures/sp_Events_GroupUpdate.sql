SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_GroupUpdate]
(
	@GroupLeader_ID int,
	@GroupName nvarchar(50),
	@GroupColeaders nvarchar(100),
	@YouthLeaderName nvarchar(50),
	@GroupSize_ID int = null,
	@GroupCreditStatus_ID int = 1,
	@GroupType_ID int = 1,
	@Comments nvarchar(MAX),
	@DetailsVerified bit = 0,
	@Deleted bit,
	@CreationDate datetime,
	@CreatedBy_ID int,
	@ModificationDate datetime = NULL,
	@ModifiedBy_ID int = NULL,
	@GUID uniqueidentifier,
	@User_ID INT = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin
		
	SET NOCOUNT ON

	IF ((SELECT COUNT(*) FROM Events_Group WHERE GroupLeader_ID = @GroupLeader_ID) = 0) BEGIN
	
		INSERT INTO [Events_Group]
		(
			[GroupLeader_ID],
			[GroupName],
			[GroupColeaders],
			[YouthLeaderName],
			[GroupSize_ID],
			[GroupCreditStatus_ID],
			[GroupType_ID],
			[Comments],
			[DetailsVerified],
			[Deleted],
			[CreationDate],
			[CreatedBy_ID],
			[ModificationDate],
			[ModifiedBy_ID],
			[GUID]
		)
		VALUES
		(
			@GroupLeader_ID,
			@GroupName,
			@GroupColeaders,
			@YouthLeaderName,
			@GroupSize_ID,
			@GroupCreditStatus_ID,
			@GroupType_ID,
			@Comments,
			@DetailsVerified,
			@Deleted,
			@CreationDate,
			@CreatedBy_ID,
			@ModificationDate,
			@ModifiedBy_ID,
			@GUID
		)

	END
	ELSE BEGIN

		UPDATE [Events_Group]
		SET
			[GroupLeader_ID] = @GroupLeader_ID,
			[GroupName] = @GroupName,
			[GroupColeaders] = @GroupColeaders,
			[YouthLeaderName] = @YouthLeaderName,
			[GroupSize_ID] = @GroupSize_ID,
			[GroupCreditStatus_ID] = @GroupCreditStatus_ID,
			[GroupType_ID] = @GroupType_ID,
			[Comments] = @Comments,
			[DetailsVerified] = @DetailsVerified,
			[Deleted] = @Deleted,
			[CreationDate] = @CreationDate,
			[CreatedBy_ID] = @CreatedBy_ID,
			[ModificationDate] = @ModificationDate,
			[ModifiedBy_ID] = @ModifiedBy_ID,
			[GUID] = @GUID
		WHERE 
			[GroupLeader_ID] = @GroupLeader_ID

	END
GO
