SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_GroupUpdateContactList]

@GroupLeader_ID			INT,
@ContactList			NVARCHAR(MAX)

AS

DECLARE @SQL	VARCHAR(5500)

BEGIN TRANSACTION

DELETE FROM Events_GroupContact WHERE GroupLeader_ID = @GroupLeader_ID

SET @SQL = '	INSERT Events_GroupContact (GroupLeader_ID, Contact_ID)
				SELECT ' + CONVERT(VARCHAR, @GroupLeader_ID) + ', Contact_ID
				FROM Common_Contact WHERE Contact_ID IN (' + @ContactList + ')'

EXEC(@SQL)

COMMIT TRANSACTION
GO
