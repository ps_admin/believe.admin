SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_NightlySaveRegistrationTotals]

AS


INSERT		Events_RegistrationTotals (Venue_ID, [Date], RegistrationType_ID, Quantity)
SELECT		Venue_ID, dbo.GetRelativeDate(), RegistrationType_ID, SUM(Qty) as Qty
FROM		(
				SELECT		Venue_ID, RegistrationType_ID, count(*) as Qty
				FROM		Events_Registration R
				WHERE		GroupLeader_ID IS NULL
				GROUP BY	Venue_ID, RegistrationType_ID
				UNION ALL
				SELECT		Venue_ID, RegistrationType_ID, SUM(TotalRegistrations)
				FROM		vw_Events_GroupSummaryRegistrationView V
				GROUP BY	Venue_ID, RegistrationType_ID
			) Z
GROUP BY	Venue_ID, RegistrationType_ID
GO
