SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_POSRegisterInsert]

@POSRegister		VARCHAR(50)

AS

INSERT Events_POSRegister
(
	POSRegister
)
VALUES
(
	@POSRegister
)

SELECT SCOPE_IDENTITY() AS POSRegister_ID
GO
