SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_POSRegisterUpdate]

@POSRegister_ID		INT,
@POSRegister		VARCHAR(50)

AS

UPDATE 	Events_POSRegister
SET		POSRegister = @POSRegister
WHERE	POSRegister_ID = @POSRegister_ID
GO
