SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_POSTransactionInsert]

@SaleType			INT,
@TransactionType	INT,
@Contact_ID			INT,
@ShipTo				VARCHAR(500),
@OriginalDocket		INT,
@TransactionDate	DATETIME,
@User_ID			INT,
@POSRegister_ID		INT,
@Venue_ID			INT

AS

INSERT Events_POSTransaction
(
	SaleType,
	TransactionType,
	Contact_ID,
	ShipTo,
	OriginalDocket,
	TransactionDate,
	[User_ID],
	POSRegister_ID,
	Venue_ID
)
VALUES
(
	@SaleType,
	@TransactionType,
	@Contact_ID,
	@ShipTo,
	@OriginalDocket,
	dbo.GetRelativeDate(),
	@User_ID,
	@POSRegister_ID,
	@Venue_ID
)

SELECT SCOPE_IDENTITY() AS POSTranasction_ID
GO
