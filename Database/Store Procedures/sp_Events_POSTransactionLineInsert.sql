SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_POSTransactionLineInsert]

@POSTransaction_ID					INT,
@Product_ID							INT,
@Price								MONEY,
@Discount							DECIMAL(18,2),
@Quantity							INT,
@QuantitySupplied					INT,
@QuantityReturned					INT,
@OriginalPOSTransactionLine_ID		INT,
@Title								VARCHAR(50),
@Net								MONEY


AS

INSERT Events_POSTransactionLine
(
	POSTransaction_ID,
	Product_ID,
	Price,
	Discount,
	Quantity,
	QuantitySupplied,
	QuantityReturned,
	OriginalPOSTransactionLine_ID,
	Title,
	Net
)
VALUES
(
	@POSTransaction_ID,
	@Product_ID,
	@Price,
	@Discount,
	@Quantity,
	@QuantitySupplied,
	@QuantityReturned,
	@OriginalPOSTransactionLine_ID,
	@Title,
	@Net	
)

SELECT SCOPE_IDENTITY() AS POSTransactionLine_ID
GO
