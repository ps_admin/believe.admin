SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_POSTransactionLineUpdate]

@POSTransactionLine_ID				INT,
@POSTransaction_ID					INT,
@Product_ID							INT,
@Price								MONEY,
@Discount							DECIMAL(18,2),
@Quantity							INT,
@QuantitySupplied					INT,
@QuantityReturned					INT,
@OriginalPOSTransactionLine_ID		INT,
@Title								VARCHAR(50),
@Net								MONEY


AS

UPDATE 	Events_POSTransactionLine
SET		POSTransaction_ID = @POSTransaction_ID,
		Product_ID = @Product_ID,
		Price = @Price,
		Discount = @Discount,
		Quantity = @Quantity,
		QuantitySupplied = @QuantitySupplied,
		QuantityReturned = @QuantityReturned,
		OriginalPOSTransactionLine_ID = @OriginalPOSTransactionLine_ID,
		Title = @Title,
		Net = @Net
WHERE	POSTransactionLine_ID = @POSTransactionLine_ID
GO
