SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_POSTransactionPaymentInsert]

@POSTransaction_ID		INT,
@PaymentType			VARCHAR(20),
@PaymentAmount			MONEY,
@PaymentTendered		MONEY,
@ChequeDrawer			VARCHAR(50),
@ChequeBank				VARCHAR(30),
@ChequeBranch			VARCHAR(30),
@CreditCardType			INT

AS

INSERT Events_POSTransactionPayment
(
	POSTransaction_ID,
	PaymentType,
	PaymentAmount,
	PaymentTendered,
	ChequeDrawer,
	ChequeBank,
	ChequeBranch,
	CreditCardType
)
VALUES
(
	@POSTransaction_ID,
	@PaymentType,
	@PaymentAmount,
	@PaymentTendered,
	@ChequeDrawer,
	@ChequeBank,
	@ChequeBranch,
	@CreditCardType
)

SELECT SCOPE_IDENTITY() AS POSTransactionPayment_ID
GO
