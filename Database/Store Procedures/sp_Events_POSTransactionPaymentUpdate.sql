SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_POSTransactionPaymentUpdate]

@POSTransactionPayment_ID	INT,
@POSTransaction_ID			INT,
@PaymentType				VARCHAR(20),
@PaymentAmount				MONEY,
@PaymentTendered			MONEY,
@ChequeDrawer				VARCHAR(50),
@ChequeBank					VARCHAR(30),
@ChequeBranch				VARCHAR(30),
@CreditCardType				INT

AS

UPDATE 	Events_POSTransactionPayment
SET		POSTransaction_ID = @POSTransaction_ID,
		PaymentType = @PaymentType,
		PaymentAmount = @PaymentAmount,
		PaymentTendered = @PaymentTendered,
		ChequeDrawer = @ChequeDrawer,
		ChequeBank = @ChequeBank,
		ChequeBranch = @ChequeBranch,
		CreditCardType = @CreditCardType
WHERE	POSTransactionPayment_ID = @POSTransactionPayment_ID
GO
