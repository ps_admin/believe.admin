SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_POSTransactionRead]

@POSTransaction_ID		INT

AS

SELECT		*
FROM 		Events_POSTransaction PT
WHERE 		POSTransaction_ID = @POSTransaction_ID

SELECT		* 
FROM 		Events_POSTransactionLine
WHERE 		POSTransaction_ID = @POSTransaction_ID

SELECT		* 
FROM 		Events_POSTransactionPayment
WHERE 		POSTransaction_ID = @POSTransaction_ID
GO
