SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_POSTransactionUpdate]

@POSTransaction_ID	INT,
@SaleType			INT,
@TransactionType	INT,
@Contact_ID			INT,
@ShipTo				VARCHAR(500),
@OriginalDocket		INT,
@TransactionDate	DATETIME,
@User_ID			INT,
@POSRegister_ID		INT,
@Venue_ID			INT

AS

UPDATE	Events_POSTransaction
SET		SaleType = @SaleType,
		TransactionType = @TransactionType,
		Contact_ID = @Contact_ID,
		ShipTo = @ShipTo,
		OriginalDocket = @OriginalDocket,
		--Don't update the transactiondate. Leave it as the original.
		--TransactionDate = @TransactionDate,
		[User_ID] = @User_ID,
		POSRegister_ID = @POSRegister_ID,
		Venue_ID = @Venue_ID
WHERE	POSTransaction_ID = @POSTransaction_ID
GO
