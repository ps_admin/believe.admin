SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_PreOrderDelete]
(
	@PreOrder_ID int,
	@User_ID				INT = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin
		
	SET NOCOUNT ON

	DELETE 
	FROM   [Events_PreOrder]
	WHERE  
		[PreOrder_ID] = @PreOrder_ID
GO
