SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_PreOrderInsert]
(
	@Registration_ID int = NULL,
	@Product_ID int,
	@Discount decimal(18,2),
	@PickedUp bit = NULL,
	@User_ID INT = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin
		
	SET NOCOUNT ON

	INSERT INTO [Events_PreOrder]
	(
		[Registration_ID],
		[Product_ID],
		[Discount],
		[PickedUp]
	)
	VALUES
	(
		@Registration_ID,
		@Product_ID,
		@Discount,
		@PickedUp
	)

	SELECT PreOrder_ID = SCOPE_IDENTITY();
GO
