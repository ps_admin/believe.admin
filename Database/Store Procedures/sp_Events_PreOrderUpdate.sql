SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_PreOrderUpdate]
(
	@PreOrder_ID int,
	@Registration_ID int = NULL,
	@Product_ID int,
	@Discount decimal(18,2),
	@PickedUp bit = NULL,
	@User_ID INT = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin
		
	SET NOCOUNT ON
	
	UPDATE [Events_PreOrder]
	SET
		[Registration_ID] = @Registration_ID,
		[Product_ID] = @Product_ID,
		[Discount] = @Discount,
		[PickedUp] = @PickedUp
	WHERE 
		[PreOrder_ID] = @PreOrder_ID
GO
