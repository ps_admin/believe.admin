SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_PriorityTagCheckIn]
	@PriorityTag_ID VARCHAR(10)
AS
BEGIN
	UPDATE [Events_PriorityTagLoan]
	SET [PriorityTagLoan_CheckIn] = CURRENT_TIMESTAMP
	WHERE [PriorityTag_ID] = @PriorityTag_ID
END
GO
