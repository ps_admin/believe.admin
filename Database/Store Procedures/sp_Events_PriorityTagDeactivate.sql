SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_PriorityTagDeactivate] 
	@PriorityTag_ID VARCHAR(10)
AS
BEGIN
	UPDATE [Events_PriorityTag]
	SET [PriorityTag_Active] = 0,
		[PriorityTag_DeactivationDate] = CURRENT_TIMESTAMP
	WHERE [PriorityTag_ID] = @PriorityTag_ID
END
GO
