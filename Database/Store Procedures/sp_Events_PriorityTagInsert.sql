SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_PriorityTagInsert]
	@PriorityTag_ID VARCHAR(10),
	@AccessLevel_ID INT
AS
BEGIN
	DECLARE @ActiveTagCount INT = 0
	
	SELECT @ActiveTagCount = COUNT(*)
	FROM [Events_PriorityTag]
	WHERE [PriorityTag_ID] = @PriorityTag_ID
	AND [PriorityTag_Active] = 1
	
	IF @ActiveTagCount > 0
		RAISERROR('A volunteer tag with the given ID is still active.', 16, 1)
		
	SELECT @ActiveTagCount = COUNT(*)
	FROM [Events_PriorityTag]
	WHERE [PriorityTag_ID] = @PriorityTag_ID
	AND [AccessLevel_ID] = @AccessLevel_ID
	
	IF @ActiveTagCount > 0
		UPDATE [Events_PriorityTag]
		SET [PriorityTag_Active] = 1,
			[PriorityTag_DeactivationDate] = NULL
		WHERE [PriorityTag_ID] = @PriorityTag_ID
	ELSE
		INSERT INTO [Events_PriorityTag]([PriorityTag_ID], [AccessLevel_ID], [PriorityTag_Active], [PriorityTag_IssueDate], [PriorityTag_DeactivationDate])
		VALUES(@PriorityTag_ID, @AccessLevel_ID, 1, CURRENT_TIMESTAMP, NULL)
END
GO
