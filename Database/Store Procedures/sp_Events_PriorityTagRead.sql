SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_PriorityTagRead]
	@AccessLevel_ID INT
AS
BEGIN
	SELECT [PriorityTag_ID], [AccessLevel_ID], [PriorityTag_Active], [PriorityTag_IssueDate], [PriorityTag_DeactivationDate]
	FROM [Events_PriorityTag]
	WHERE [AccessLevel_ID] = @AccessLevel_ID
	ORDER BY [PriorityTag_IssueDate]
END
GO
