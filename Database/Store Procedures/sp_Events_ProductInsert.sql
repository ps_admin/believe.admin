SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_ProductInsert]

@Code					VARCHAR(20),
@Code2					VARCHAR(20),
@Title					VARCHAR(50),
@AuthorArtist			VARCHAR(50),
@Deleted				BIT

AS

INSERT INTO Events_Product
(
	Code,
	Code2,
	Title,
	AuthorArtist,
	Deleted
)
VALUES
(
	@Code,
	@Code2,
	@Title,
	@AuthorArtist,
	@Deleted
)

SELECT SCOPE_IDENTITY() AS Product_ID
GO
