SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_ProductPriceInsert]

@Product_ID		INT,
@Country_ID		INT,
@Price			MONEY

AS

INSERT Events_ProductPrice
(
	Product_ID,
	Country_ID,
	Price
)
VALUES
(
	@Product_ID,
	@Country_ID,
	@Price
)

SELECT SCOPE_IDENTITY() AS ProductPrice_ID
GO
