SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_ProductPriceUpdate]

@ProductPrice_ID	INT,
@Product_ID			INT,
@Country_ID			INT,
@Price				MONEY

AS

UPDATE	Events_ProductPrice
SET		Product_ID = @Product_ID,
		Country_ID = @Country_ID,
		Price = @Price
WHERE	ProductPrice_ID = @ProductPrice_ID
GO
