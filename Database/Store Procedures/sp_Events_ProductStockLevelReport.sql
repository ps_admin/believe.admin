SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_ProductStockLevelReport]

@Venue_ID			INT,
@StartDate			DATETIME,
@EndDate			DATETIME,
@Product_Id			INT

AS

SELECT		P.Product_ID,
			P.Code,
			P.Title,
			CONVERT(INT,0) as OpeningBalance,
			CONVERT(INT,0) as Adjustment,
			CONVERT(INT,0) as Sales,
			CONVERT(INT,0) as SubTotal,
			CONVERT(INT,0) as ClosingBalance,
			CONVERT(INT,0) as [Difference],
			CONVERT(INT,0) as Returned,
			CONVERT(INT,0) as OutstandingAmount,
			CONVERT(VARCHAR(2000),'') as Comments
INTO		#t1
FROM		Events_Product P

SELECT		SL.Product_ID,
			SUM(CASE WHEN AdjustmentType = 1 THEN Quantity ELSE 0 END) AS OpeningBalance,
			SUM(CASE WHEN AdjustmentType = 2 THEN Quantity ELSE 0 END) AS Adjustment,
			SUM(CASE WHEN AdjustmentType = 3 THEN Quantity ELSE 0 END) AS ClosingBalance,
			SUM(CASE WHEN AdjustmentType = 4 THEN Quantity ELSE 0 END) AS Returned
INTO		#t2
FROM		Events_StockLevel SL
WHERE		(SL.Venue_ID = @Venue_ID OR (SL.Venue_ID IS NULL AND @Venue_ID IS NULL))
AND			(SL.Product_ID = @Product_ID OR @Product_ID IS NULL)
AND			SL.DateAdded >= @StartDate
AND			SL.DateAdded < DATEADD(dd, 1, @EndDate)
GROUP BY	SL.Product_ID

SELECT		PTL.Product_ID,
			SUM(Quantity) as Sales
INTO		#t3
FROM		Events_POSTransaction PT, 
			Events_POSTransactionLine PTL
WHERE		PT.POSTransaction_ID = PTL.POSTransaction_ID
AND			PT.TransactionDate >= @StartDate
AND			PT.TransactionDate < DATEADD(dd,1,@EndDate)
AND			(PT.Venue_ID = @Venue_ID OR (PT.Venue_ID IS NULL AND @Venue_ID IS NULL))
GROUP BY	PTL.Product_ID

UPDATE		#t1
SET			OpeningBalance = #t2.OpeningBalance,
			Adjustment = #t2.Adjustment,
			ClosingBalance = #t2.ClosingBalance,
			Returned = #t2.Returned
FROM		#t2
WHERE		#t1.Product_ID = #t2.Product_ID

UPDATE		#t1
SET			Sales = #t3.Sales
FROM		#t3
WHERE		#t1.Product_ID = #t3.Product_ID

UPDATE		#t1
SET			SubTotal = OpeningBalance + Adjustment - Sales

UPDATE		#t1
SET			[Difference] = ClosingBalance - SubTotal

UPDATE		#t1
SET			OutstandingAmount = [Difference] - Returned

DECLARE		@Prod_ID			INT,
			@AdjustmentType		INT,
			@Comments			VARCHAR(2000)

DECLARE	CommentsCursor CURSOR FOR
	SELECT Product_ID, AdjustmentType, Comments 
	FROM Events_StockLevel 
	WHERE (Venue_ID = @Venue_ID OR @Venue_ID IS NULL)
	AND (Product_ID = @Product_ID OR @Product_ID IS NULL) 
	AND DateAdded >= @StartDate 
	AND DateAdded < DATEADD(dd, 1, @EndDate) 
	AND Comments <> ''

OPEN CommentsCursor

FETCH NEXT FROM CommentsCursor INTO @Prod_ID, @AdjustmentType, @Comments
WHILE @@FETCH_STATUS = 0 BEGIN

	UPDATE		#t1 
	SET			Comments =	Comments + 
							CASE	WHEN @AdjustmentType = 1 THEN 'Opening Balance: '
									WHEN @AdjustmentType = 2 THEN 'Adjustment: '
									WHEN @AdjustmentType = 3 THEN 'Closing Balance: '
									WHEN @AdjustmentType = 4 THEN 'Returned Stock: '
							ELSE '' END +
							@Comments + CHAR(10)
	WHERE		Product_ID = @Prod_ID

	FETCH NEXT FROM CommentsCursor INTO @Prod_ID, @AdjustmentType, @Comments

END

CLOSE CommentsCursor
DEALLOCATE CommentsCursor

SELECT		*
FROM		#t1
WHERE		OpeningBalance <> 0 
OR			Adjustment <> 0 
OR			Sales <> 0 
OR			SubTotal <> 0
OR			ClosingBalance <> 0
OR			[Difference] <> 0
OR			Returned <> 0
OR			OutstandingAmount <> 0
GO
