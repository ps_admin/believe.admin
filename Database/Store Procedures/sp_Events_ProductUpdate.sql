SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_ProductUpdate]

@Product_ID				INT,
@Code					VARCHAR(20),
@Code2					VARCHAR(20),
@Title					VARCHAR(50),
@AuthorArtist			VARCHAR(50),
@Deleted				BIT

AS

UPDATE	Events_Product
SET		Code = @Code,
		Code2 = @Code2,
		Title = @Title,
		AuthorArtist = @AuthorArtist,
		Deleted = @Deleted
WHERE	Product_ID = @Product_ID
GO
