SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_RegistrantSearch]

@FirstName					NVARCHAR(20) = '',
@LastName					NVARCHAR(20) = '',
@ChurchName					NVARCHAR(50) = '',
@RegNum						NVARCHAR(10) = '',
@Address					NVARCHAR(50) = '',
@Suburb						NVARCHAR(30) = '',
@Postcode					NVARCHAR(20) = '',
@Country_ID					INT = 0,
@State_ID					INT = 0,
@Gender						NVARCHAR(10) = '',
@Email						NVARCHAR(80) = '',
@EmergencyContact			NVARCHAR(30) = '',
@Phone 						NVARCHAR(20) = '',
@Fax						NVARCHAR(20) = '',
@Mobile						NVARCHAR(20) = '',
@LeaderName					NVARCHAR(30) = '',
@Conference_ID				INT = 0,
@Registered					INT = 0,
@CreditDebit				INT = 0,
@DateFrom					NVARCHAR(20) = '',
@DateTo						NVARCHAR(20) = '',
@GroupName					NVARCHAR(30) = '',
@Attended					NVARCHAR(10) = '',
@Venue_ID					INT = 0,
@RegistrationType_ID		NVARCHAR(30) = '',
@PaymentType_ID				INT = 0,
@WaitingList				INT = 0,
@PreOrder					INT = 0,
@SessionOrderProduct_ID		INT = 0,
@SessionOrderVenue_ID		INT = -1,
@RegistrationOptions		NVARCHAR(MAX) = '',
@UserState_ID				INT = 0,
@UserCountry_ID				INT = 0,
@ShowInactiveRegistrants	INT = 1


AS

DECLARE @SQL			VARCHAR(8000)
DECLARE @SQLSelect		VARCHAR(8000)
DECLARE @SQLFrom		VARCHAR(8000)
DECLARE @SQLWhere		VARCHAR(8000)

SELECT		CONVERT(int,C.Contact_ID) as Contact_ID,
			Sa.Salutation as Title,
			C.FirstName, 
			C.LastName,
			C.Address1,
			C.Suburb,
			C.Postcode,
			ISNULL(S.State,'''') as State,
			ISNULL(Co.Country,'''') as Country,
			C.Phone,
			C.Fax,
			C.Mobile,
			C.Email,
			C.Gender,
			C.DateOfBirth,
			CONVERT(NVARCHAR(1000),'''') as GroupName,
			CE.ChurchName,
			CE.LeaderName,
			CEE.EmergencyContactName,
			CEE.EmergencyContactPhone,
			CEE.EventComments as Comments,
			Registered = CONVERT(BIT,0),
			Amount = Convert(MONEY,0),
			Payments = Convert(MONEY,0),
			CreditDebit= Convert(BIT,0),
			CE.Inactive,
			C.CreationDate,
			C.ModificationDate
INTO		#t1
FROM 		Common_Contact C 
				LEFT JOIN Common_ContactExternal CE ON C.Contact_ID = CE.Contact_ID
				LEFT JOIN Common_ContactExternalEvents CEE ON CE.Contact_ID = CEE.Contact_ID 
				LEFT JOIN Common_GeneralState S ON C.State_ID = S.State_ID
				LEFT JOIN Common_GeneralCountry Co ON C.Country_ID = Co.Country_ID, 
			Common_Salutation Sa
WHERE 		C.Contact_ID = CE.Contact_ID
AND			C.Salutation_ID = Sa.Salutation_ID
AND			1=0

/********************************
*       SQL Select String		*
*********************************/
SET @SQLSelect = '	
INSERT INTO #t1
SELECT	DISTINCT 
		C.Contact_ID,
		Sa.Salutation as Title,
		C.FirstName, 
		C.LastName,
		C.Address1,
		C.Suburb,
		C.Postcode,
		ISNULL(S.State,'''') as State,
		ISNULL(Co.Country,'''') as Country,
		C.Phone,
		C.Fax,
		C.Mobile,
		C.Email,
		C.Gender,
		C.DateOfBirth,
		CONVERT(VARCHAR(1000),'''') as GroupName,
		CE.ChurchName,
		CE.LeaderName,
		CEE.EmergencyContactName,
		CEE.EmergencyContactPhone,
		CEE.EventComments,
		Registered = (SELECT CASE WHEN ' + CONVERT(VARCHAR,@Conference_ID) + ' = 0 THEN 0 ELSE (SELECT COUNT(*) FROM Events_Registration Re, Events_Venue V WHERE CE.Contact_ID = Re.Contact_ID AND Re.Venue_ID = V.Venue_ID AND V.Conference_ID = ' + CONVERT(VARCHAR,@Conference_ID) +') END),
		Convert(MONEY,0) as Amount,
		Convert(MONEY,0) as Payments,
		Convert(BIT,0) AS CreditDebit,
		CE.Inactive,
		C.CreationDate,
		C.ModificationDate'

SET		@SQLFrom = '
FROM 	Common_Contact C 
			LEFT JOIN Common_ContactExternal CE ON C.Contact_ID = CE.Contact_ID
			LEFT JOIN Common_ContactExternalEvents CEE ON CE.Contact_ID = CEE.Contact_ID   
			LEFT JOIN Common_GeneralState S ON C.State_ID = S.State_ID
			LEFT JOIN Common_GeneralCountry Co ON C.Country_ID = Co.Country_ID,  
		Common_Salutation Sa'

SET		@SQLWhere = '
WHERE	C.Salutation_ID = Sa.Salutation_ID 
AND		ISNULL(CE.Deleted,0) = 0 ' + CHAR(10)

/********************************
*       SQL From String			*
*********************************/
IF @Registered = 1 OR @Attended <> '' OR @Venue_ID > 0 OR @RegistrationType_ID > 0 OR @PaymentType_ID > 0 OR @WaitingList > 0 OR @PreOrder > 0 OR @SessionOrderProduct_ID > 0 BEGIN
	SET @SQLFrom = @SQLFrom + ', Events_Registration Re, Events_Venue V'
	SET @SQLWhere = @SQLWhere + ' AND CE.Contact_ID = Re.Contact_ID AND Re.Venue_ID = V.Venue_ID'
END
IF @GroupName <> '' BEGIN
	SET @SQLFrom = @SQLFrom + ', Events_Group G' + CHAR(10)
	SET @SQLWhere = @SQLWhere + ' AND Re.GroupLeader_ID = G.GroupLeader_ID'
END
IF @PaymentType_ID > 0 BEGIN
	SET @SQLFrom = @SQLFrom + ', Events_ContactPayment CP'
	SET @SQLWhere = @SQLWhere + ' AND C.Contact_ID = CP.Contact_ID'
END 
IF @WaitingList > 0 BEGIN
	SET @SQLFrom = @SQLFrom + ', Events_WaitingList WL'
	SET @SQLWhere = @SQLWhere + ' AND C.Contact_ID = WL.Contact_ID'
END
IF @PreOrder > 0 BEGIN
	SET @SQLFrom = @SQLFrom + ', Events_Preorder PO'
	SET @SQLWhere = @SQLWhere + ' AND Re.Registration_ID = PO.Registration_ID'
END
IF @SessionOrderProduct_ID > 0 BEGIN
	SET @SQLFrom = @SQLFrom + ', Events_POSTransaction PT, Events_POSTransactionLine PTL'
	SET @SQLWhere = @SQLWhere + ' AND C.Contact_ID = PT.Contact_ID AND PT.POSTransaction_ID = PTL.POSTransaction_ID'
END


/********************************
*       SQL Where String		*
*********************************/

--Registrant Options
IF @UserState_ID > 0						SET @SQLWhere = @SQLWhere + '	AND	(C.State_ID = ' + CONVERT(VARCHAR,@UserState_ID) + ' OR C.Contact_ID IN (SELECT Contact_ID FROM Events_Registration Re, Events_Venue V WHERE Re.Venue_Id = V.Venue_ID AND V.State_ID = ' + CONVERT(VARCHAR,@UserState_ID)+ ' AND V.Conference_ID = ' + CONVERT(VARCHAR,@Conference_ID) +'))' + CHAR(10)
IF @UserCountry_ID > 0						SET @SQLWhere = @SQLWhere + '	AND	(C.Country_ID = ' + CONVERT(VARCHAR,@UserCountry_ID) + ' OR C.Contact_ID IN (SELECT Contact_ID FROM Events_Registration Re, Events_Venue V, Events_Conference C WHERE Re.Venue_Id = V.Venue_ID AND V.Conference_ID = C.Conference_ID AND C.Country_ID = ' + CONVERT(VARCHAR,@UserCountry_ID)+ ' AND C.Conference_ID = ' + CONVERT(VARCHAR,@Conference_ID) +'))' + CHAR(10)
IF @FirstName <> '' 						SET @SQLWhere = @SQLWhere + '	AND	(C.FirstName LIKE ''%' + @FirstName + '%'')' + CHAR(10)
IF @LastName <> ''  						SET @SQLWhere = @SQLWhere + '	AND	(C.LastName LIKE ''%' + @LastName +'%'')' + CHAR(10)
IF @ChurchName <> '' 						SET @SQLWhere = @SQLWhere + '	AND	(CE.ChurchName LIKE ''%' + @ChurchName +'%'')' + CHAR(10)
IF @RegNum <> '' 							SET @SQLWhere = @SQLWhere + '	AND	C.Contact_ID = ' + @RegNum + CHAR(10)
IF @Address <> '' 							SET @SQLWhere = @SQLWhere + '	AND	(C.Address1 LIKE ''%' + @Address + '%'' OR C.Address2 LIKE ''%' + @Address + '%'')' + CHAR(10)
IF @Suburb <> '' 							SET @SQLWhere = @SQLWhere + '	AND	(C.Suburb LIKE ''%' + @Suburb +'%'')' + CHAR(10)
IF @Postcode <> '' 							SET @SQLWhere = @SQLWhere + '	AND	(C.Postcode LIKE ''%' + @Postcode +'%'')' + CHAR(10)
IF @Country_ID > 0							SET @SQLWhere = @SQLWhere + '	AND	(C.Country_ID = ' + CONVERT(VARCHAR,@Country_ID)+')' + CHAR(10)
IF @State_ID > 0		 					SET @SQLWhere = @SQLWhere + '	AND	(C.State_ID = ' + CONVERT(VARCHAR,@State_ID) + ')' + CHAR(10)
IF @Gender <> '' 							SET @SQLWhere = @SQLWhere + '	AND	(C.Gender =''' + @Gender +''')' + CHAR(10)
IF @Email <> '' 							SET @SQLWhere = @SQLWhere + '	AND	(C.Email LIKE ''%' + @Email +'%'')' + CHAR(10)
IF @EmergencyContact <> '' 					SET @SQLWhere = @SQLWhere + '	AND	(CE.EmergencyContactName LIKE ''' + @EmergencyContact +''')' + CHAR(10)
IF @Phone  <> '' 							SET @SQLWhere = @SQLWhere + '	AND	(C.Phone LIKE ''%' + @Phone +'%'')' + CHAR(10)
IF @Fax <> '' 								SET @SQLWhere = @SQLWhere + '	AND	(C.Fax LIKE ''%' + @Fax +'%'')' + CHAR(10)
IF @Mobile <> '' 							SET @SQLWhere = @SQLWhere + '	AND	(C.Mobile LIKE ''%' + @Mobile +'%'')' + CHAR(10)
IF @LeaderName <> '' 						SET @SQLWhere = @SQLWhere + '	AND	(CE.LeaderName LIKE ''%' + @LeaderName +'%'')' + CHAR(10)
IF @ShowInactiveRegistrants = 0				SET	@SQLWhere = @SQLWhere + '	AND (CE.Inactive = 0)' + CHAR(10)

-- Registration Options
IF @SessionOrderProduct_ID > 0				SET @SQLWhere = @SQLWhere + '	AND	PTL.Product_ID = ' + CONVERT(VARCHAR, @SessionOrderProduct_ID) + CHAR(10)
IF @SessionOrderVenue_ID = 0				SET @SQLWhere = @SQLWhere + '	AND	PT.Venue_ID IS NULL'
IF @SessionOrderVenue_ID > 0				SET @SQLWhere = @SQLWhere + '	AND PT.Venue_ID = ' + CONVERT(VARCHAR, @SessionOrderVenue_ID) + CHAR(10)

IF @Registered = 1 BEGIN
											SET @SQLWhere = @SQLWhere + '	AND V.Conference_ID = ' + CONVERT(VARCHAR, @Conference_ID)
	IF @DateFrom <> '' 						SET @SQLWhere = @SQLWhere + '	AND	Re.RegistrationDate >= ''' + @DateFrom + '''' + CHAR(10)
	IF @DateTo <> ''						SET @SQLWhere = @SQLWhere + '	AND	Re.RegistrationDate < ''' + Convert(VARCHAR, DATEADD(dd,1,CONVERT(SMALLDATETIME,@DateTo)),20) + '''' + CHAR(10)
	IF @GroupName <> ''						SET @SQLWhere = @SQLWhere + '	AND	G.GroupName LIKE ''%' + @GroupName + '%''' + CHAR(10)
	IF @Attended <> ''						SET @SQLWhere = @SQLWhere + ' 	AND	Re.Attended = ' + CONVERT(VARCHAR, CASE WHEN @Attended = 'YES' THEN 1 ELSE 0 END,0) + CHAR(10)
	IF @Venue_ID > 0						SET @SQLWhere = @SQLWhere + '	AND	Re.Venue_ID = ' + CONVERT(VARCHAR, @Venue_ID) + CHAR(10)
	IF @RegistrationType_ID > 0				SET @SQLWhere = @SQLWhere + '	AND	Re.RegistrationType_ID = ' + CONVERT(VARCHAR, @RegistrationType_ID) + CHAR(10)
	IF @PaymentType_ID > 0					SET @SQLWhere = @SQLWhere + '	AND	CP.PaymentType_ID = ' + CONVERT(VARCHAR, @PaymentType_ID) + CHAR(10)
	IF @WaitingList > 0 					SET @SQLWhere = @SQLWhere + '	AND	WL.Venue_ID = ' + CONVERT(VARCHAR, @WaitingList) + CHAR(10)
	IF @PreOrder > 0						SET @SQLWhere = @SQLWhere + '	AND	PO.Product_ID = ' + CONVERT(VARCHAR, @PreOrder) + CHAR(10)

	IF @RegistrationOptions <> '' BEGIN
		WHILE @RegistrationOptions <> '' BEGIN

			DECLARE @RegOption VARCHAR(MAX)
			DECLARE @RegOptionValue VARCHAR(MAX)
			IF CHARINDEX('|',@RegistrationOptions) > 0 BEGIN
				SET @RegOption = LEFT(@RegistrationOptions, CHARINDEX('|',@RegistrationOptions)-1)
				SET @RegistrationOptions = RIGHT(@RegistrationOptions, LEN(@RegistrationOptions) - CHARINDEX('|',@RegistrationOptions))
			END
			IF CHARINDEX('|',@RegistrationOptions) > 0 BEGIN
				SET @RegOptionValue = LEFT(@RegistrationOptions, CHARINDEX('|',@RegistrationOptions)-1)
				SET @RegistrationOptions = RIGHT(@RegistrationOptions, LEN(@RegistrationOptions) - CHARINDEX('|',@RegistrationOptions))
			END
			ELSE BEGIN
				SET @RegOptionValue = @RegistrationOptions
				SET @RegistrationOptions = ''
			END
			IF @RegOptionValue <> '' BEGIN
				SET @SQLWhere = @SQLWHERE + ' AND Re.Registration_ID IN (SELECT Registration_ID FROM vw_Events_RegistrationOptionView ROV WHERE ROV.RegistrationOptionName_ID = ' + @RegOption + ' AND ((ROV.FieldType<>''TXT'' AND ROV.[Value] = ''' + @RegOptionValue + ''') OR (ROV.FieldType=''TXT'' AND ROV.[Value] LIKE ''%' + @RegOptionValue + '%'')))'
			END
		END 
	END
END

IF	@Registered = 2 AND @Conference_ID > 0 
 	SET @SQLWhere = @SQLWhere + ' AND	C.Contact_ID NOT IN (SELECT Contact_ID FROM Events_Registration Re, Events_Venue V WHERE Re.Venue_ID = V.Venue_ID AND V.Conference_ID = ' + CONVERT(VARCHAR,@Conference_ID) + ')'

SET @SQL = @SQLSelect + CHAR(10) + @SQLFrom + CHAR(10) + @SQLWhere

EXEC(@SQL)

IF @Conference_ID > 0 BEGIN

	DECLARE @Group VARCHAR(1000)
	DECLARE @Contact_ID INT
	
	DECLARE GroupCursor CURSOR FOR
		SELECT DISTINCT Re.Contact_ID, G.GroupName FROM Events_Group G, Events_Registration Re, Events_Venue V, #t1 WHERE #t1.Contact_ID = Re.Contact_ID AND Re.Venue_ID = V.Venue_ID AND G.GroupLeader_ID = Re.GroupLeader_ID AND (V.Conference_ID = @Conference_ID OR @Conference_ID = 0) ORDER BY Contact_ID, GroupName
	
		OPEN GroupCursor
	
		FETCH NEXT FROM GroupCursor INTO @Contact_ID, @Group
		WHILE(@@fetch_status=0) BEGIN
			UPDATE #t1 SET GroupName = ISNULL(#t1.GroupName,'') + @Group + '; ' where Contact_ID = @Contact_ID
			FETCH NEXT FROM GroupCursor INTO @Contact_ID, @Group
		END
	
	CLOSE GroupCursor
	DEALLOCATE GroupCursor
	
	UPDATE #t1 SET GroupName = left(GroupName,len(GroupName)-1) WHERE GroupName <> ''

END

UPDATE #t1 SET Amount = (SELECT SUM(TotalCost) FROM vw_Events_RegistrantCostView RCV WHERE RCV.Contact_ID = #t1.Contact_ID AND RCV.GroupLeader_ID IS NULL AND (RCV.Conference_ID = @Conference_ID OR @Conference_ID = 0))
UPDATE #t1 SET Payments =  ISNULL((SELECT SUM(paymentamount) FROM Events_ContactPayment CP WHERE CP.Contact_ID = #t1.Contact_ID AND (CP.Conference_ID = @Conference_ID OR @Conference_ID = 0)),0)

UPDATE #t1 SET CreditDebit = 1 WHERE Amount <> Payments AND GroupName = ''

DELETE FROM #t1 WHERE CreditDebit = 0 AND @CreditDebit = 1
DELETE FROM #t1 WHERE CreditDebit = 1 AND @CreditDebit = 2

SELECT * FROM #t1
GO
