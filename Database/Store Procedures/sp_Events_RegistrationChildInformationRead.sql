SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_RegistrationChildInformationRead]

@Registration_ID	INT

AS

SELECT	*
FROM	Events_RegistrationChildInformation
WHERE	Registration_ID = @Registration_ID
GO
