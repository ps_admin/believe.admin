SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_RegistrationChildInformationUpdate]
(
	@Registration_ID int,
	@CrecheChildSchoolGrade_ID int,
	@ChildsFriend nvarchar(50),
	@DropOffPickUpName nvarchar(50),
	@DropOffPickUpRelationship nvarchar(50),
	@DropOffPickUpContactNumber nvarchar(50),
	@ChildrensChurchPastor nvarchar(50),
	@ConsentFormSent bit = 0,
	@ConsentFormSentDate datetime = null,
	@ConsentFormReturned bit = 0,
	@ConsentFormReturnedDate datetime = null,
	@User_ID int = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin

	SET NOCOUNT ON

	IF ((SELECT COUNT(*) FROM Events_RegistrationChildInformation WHERE Registration_ID = @Registration_ID) = 0) BEGIN

		INSERT INTO [Events_RegistrationChildInformation]
		(
			[Registration_ID],
			[CrecheChildSchoolGrade_ID],
			[ChildsFriend],
			[DropOffPickUpName],
			[DropOffPickUpRelationship],
			[DropOffPickUpContactNumber],
			[ChildrensChurchPastor],
			[ConsentFormSent],
			[ConsentFormSentDate],
			[ConsentFormReturned],
			[ConsentFormReturnedDate]
			
		)
		VALUES
		(
			@Registration_ID,
			@CrecheChildSchoolGrade_ID,
			@ChildsFriend,
			@DropOffPickUpName,
			@DropOffPickUpRelationship,
			@DropOffPickUpContactNumber,
			@ChildrensChurchPastor,
			@ConsentFormSent,
			@ConsentFormSentDate,
			@ConsentFormReturned,
			@ConsentFormReturnedDate
		)

	END
	ELSE BEGIN

		UPDATE [Events_RegistrationChildInformation]
		SET
			[CrecheChildSchoolGrade_ID] = @CrecheChildSchoolGrade_ID,
			[ChildsFriend] = @ChildsFriend,
			[DropOffPickUpName] = @DropOffPickUpName,
			[DropOffPickUpRelationship] = @DropOffPickUpRelationship,
			[DropOffPickUpContactNumber] = @DropOffPickUpContactNumber,
			[ChildrensChurchPastor] = @ChildrensChurchPastor,
			[ConsentFormSent] = @ConsentFormSent,
			[ConsentFormSentDate] = @ConsentFormSentDate,
			[ConsentFormReturned] = @ConsentFormReturned,
			[ConsentFormReturnedDate] = @ConsentFormReturnedDate
		WHERE 
			[Registration_ID] = @Registration_ID
	
	END
GO
