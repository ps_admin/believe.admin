SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_RegistrationFriendList]

@Contact_ID		INT,
@Conference_ID	INT

AS

SELECT	DISTINCT Contact_ID
FROM	Events_Registration R,
		Events_Venue V
WHERE	R.Venue_ID = V.Venue_ID
AND		V.Conference_ID = @Conference_ID
AND		R.RegisteredByFriend_ID = @Contact_ID
GO
