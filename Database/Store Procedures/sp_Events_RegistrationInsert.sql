SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_RegistrationInsert]
(
	@Contact_ID int,
	@GroupLeader_ID int = NULL,
	@FamilyRegistration bit,
	@RegisteredByFriend_ID int = null,
	@Venue_ID int,
	@RegistrationType_ID int,
	@RegistrationDiscount decimal(18,2),
	@Elective_ID int = NULL,
	@Accommodation bit,
	@Accommodation_ID int = NULL,
	@Catering bit,
	@CateringNeeds nvarchar(100),
	@LeadershipBreakfast bit,
	@LeadershipSummit bit = 0, 
	@ULG_ID	int = null,
	@ParentGuardian varchar(50),
	@ParentGuardianPhone varchar(50),
	@AcceptTermsRego bit,
	@AcceptTermsParent bit,
	@AcceptTermsCreche bit,
	@RegistrationDate datetime,
	@RegisteredBy_ID int,
	@Attended bit,
	@Volunteer bit,
	@VolunteerDepartment_ID int = NULL,
	@ComboRegistration_ID int = NULL,
	@User_ID INT = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin
		
	SET NOCOUNT ON

	INSERT INTO [Events_Registration]
	(
		[Contact_ID],
		[GroupLeader_ID],
		[FamilyRegistration],
		[RegisteredByFriend_ID],
		[Venue_ID],
		[RegistrationType_ID],
		[RegistrationDiscount],
		[Elective_ID],
		[Accommodation],
		[Accommodation_ID],
		[Catering],
		[CateringNeeds],
		[LeadershipBreakfast],
		[LeadershipSummit],
		[ULG_ID],
		[ParentGuardian],
		[ParentGuardianPhone],
		[AcceptTermsRego],
		[AcceptTermsParent],
		[AcceptTermsCreche],
		[RegistrationDate],
		[RegisteredBy_ID],
		[Attended],
		[Volunteer],
		[VolunteerDepartment_ID],
		[ComboRegistration_ID]
	)
	VALUES
	(
		@Contact_ID,
		@GroupLeader_ID,
		@FamilyRegistration,
		@RegisteredByFriend_ID,
		@Venue_ID,
		@RegistrationType_ID,
		@RegistrationDiscount,
		@Elective_ID,
		@Accommodation,
		@Accommodation_ID,
		@Catering,
		@CateringNeeds,
		@LeadershipBreakfast,
		@LeadershipSummit,
		@ULG_ID,
		@ParentGuardian,
		@ParentGuardianPhone,
		@AcceptTermsRego,
		@AcceptTermsParent,
		@AcceptTermsCreche,
		@RegistrationDate,
		@RegisteredBy_ID,
		@Attended,
		@Volunteer,
		@VolunteerDepartment_ID,
		@ComboRegistration_ID
	)

	SELECT Registration_ID = SCOPE_IDENTITY();
GO
