SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_RegistrationOptionInsert]
(
	@Registration_ID int,
	@RegistrationOptionName_ID int,
	@RegistrationOptionValue_ID int = NULL,
	@RegistrationOptionText nvarchar(100),
	@User_ID INT = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin
		
	SET NOCOUNT ON

	INSERT INTO [Events_RegistrationOption]
	(
		[Registration_ID],
		[RegistrationOptionName_ID],
		[RegistrationOptionValue_ID],
		[RegistrationOptionText]
	)
	VALUES
	(
		@Registration_ID,
		@RegistrationOptionName_ID,
		@RegistrationOptionValue_ID,
		@RegistrationOptionText
	)

	SELECT RegistrationOption_ID = SCOPE_IDENTITY();
GO
