SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_RegistrationOptionUpdate]
(
	@RegistrationOption_ID int,
	@Registration_ID int,
	@RegistrationOptionName_ID int,
	@RegistrationOptionValue_ID int = NULL,
	@RegistrationOptionText nvarchar(100),
	@User_ID INT = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin
		
	SET NOCOUNT ON
	
	UPDATE [Events_RegistrationOption]
	SET
		[Registration_ID] = @Registration_ID,
		[RegistrationOptionName_ID] = @RegistrationOptionName_ID,
		[RegistrationOptionValue_ID] = @RegistrationOptionValue_ID,
		[RegistrationOptionText] = @RegistrationOptionText
	WHERE 
		[RegistrationOption_ID] = @RegistrationOption_ID
GO
