SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_RegistrationTypeInsert]

@Conference_ID				INT,
@RegistrationType			VARCHAR(50),
@RegistrationCost			MONEY,
@StartDate					DATETIME,
@EndDate					DATETIME,
@AvailableOnline			BIT,
@OnlineRegistrationType		INT,
@IsBulkRegistration			BIT

AS

INSERT INTO RegistrationType
(
	Conference_ID,
	RegistrationType,
	RegistrationCost,
	StartDate,
	EndDate,
	AvailableOnline,
	OnlineRegistrationType,
	IsBulkRegistration
)
VALUES
(
	@Conference_ID,
	@RegistrationType,
	@RegistrationCost,
	@StartDate,
	@EndDate,
	@AvailableOnline,
	@OnlineRegistrationType,
	@IsBulkRegistration
)

SELECT SCOPE_IDENTITY() AS RegistrationType_ID
GO
