SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_RegistrationTypeUpdate]

@RegistrationType_ID		INT,
@Conference_ID				INT,
@RegistrationType			VARCHAR(50),
@RegistrationCost			MONEY,
@StartDate					DATETIME,
@EndDate					DATETIME,
@AvailableOnline			BIT,
@OnlineRegistrationType		INT,
@IsBulkRegistration			BIT

AS

UPDATE 	RegistrationType
SET		Conference_ID = @Conference_ID,
		RegistrationType = @RegistrationType,
		RegistrationCost = @RegistrationCost,
		StartDate = @StartDate,
		EndDate = @EndDate,
		AvailableOnline = @AvailableOnline,
		OnlineRegistrationType = @OnlineRegistrationType,
		IsBulkRegistration = @IsBulkRegistration
WHERE	RegistrationType_ID = @RegistrationType_ID
GO
