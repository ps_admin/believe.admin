SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_RegistrationUpdate]
(
	@Registration_ID int,
	@Contact_ID int,
	@GroupLeader_ID int = NULL,
	@FamilyRegistration bit,
	@RegisteredByFriend_ID int = null,
	@Venue_ID int,
	@RegistrationType_ID int,
	@RegistrationDiscount decimal(18,2),
	@Elective_ID int = NULL,
	@Accommodation bit,
	@Accommodation_ID int = NULL,
	@Catering bit,
	@CateringNeeds nvarchar(100),
	@LeadershipBreakfast bit,
	@LeadershipSummit bit = 0,
	@ULG_ID	int = null,
	@ParentGuardian varchar(50),
	@ParentGuardianPhone varchar(50),
	@AcceptTermsRego bit,
	@AcceptTermsParent bit,
	@AcceptTermsCreche bit,
	@RegistrationDate datetime,
	@RegisteredBy_ID int,
	@Attended bit,
	@Volunteer bit,
	@VolunteerDepartment_ID int = NULL,
	@ComboRegistration_ID int = NULL,
	@User_ID INT = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin
		
	SET NOCOUNT ON
	
	UPDATE [Events_Registration]
	SET
		[Contact_ID] = @Contact_ID,
		[GroupLeader_ID] = @GroupLeader_ID,
		[FamilyRegistration] = @FamilyRegistration,
		[RegisteredByFriend_ID] = @RegisteredByFriend_ID,
		[Venue_ID] = @Venue_ID,
		[RegistrationType_ID] = @RegistrationType_ID,
		[RegistrationDiscount] = @RegistrationDiscount,
		[Elective_ID] = @Elective_ID,
		[Accommodation] = @Accommodation,
		[Accommodation_ID] = @Accommodation_ID,
		[Catering] = @Catering,
		[CateringNeeds] = @CateringNeeds,
		[LeadershipBreakfast] = @LeadershipBreakfast,
		[LeadershipSummit] = @LeadershipSummit,
		[ULG_ID] = @ULG_ID,
		[ParentGuardian] = @ParentGuardian,
		[ParentGuardianPhone] = @ParentGuardianPhone,
		[AcceptTermsRego] = @AcceptTermsRego,
		[AcceptTermsParent] = @AcceptTermsParent,
		[AcceptTermsCreche] = @AcceptTermsCreche,
		[RegistrationDate] = @RegistrationDate,
		[RegisteredBy_ID] = @RegisteredBy_ID,
		[Attended] = @Attended,
		[Volunteer] = @Volunteer,
		[VolunteerDepartment_ID] = @VolunteerDepartment_ID,
		[ComboRegistration_ID] = @ComboRegistration_ID
	WHERE 
		[Registration_ID] = @Registration_ID
GO
