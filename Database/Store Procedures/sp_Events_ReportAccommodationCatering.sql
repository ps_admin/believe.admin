SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_ReportAccommodationCatering]

@Venue_ID	INT

AS

SELECT	G.GroupLeader_ID, G.GroupName,
		C.FirstName, C.LastName, C.Email, C.Phone, C.Mobile,
		AccommodationQuantity as Purchased, 
		(SELECT COUNT(*) FROM Events_Registration WHERE Venue_ID = V.Venue_ID and GroupLeader_ID = V.GroupLeader_ID and Accommodation=1) as Allocated
FROM	vw_Events_GroupSummaryBulkPurchaseView V ,
		Events_Group G,
		Common_Contact C
WHERE	V.GroupLeader_ID = G.GroupLeader_ID
AND		G.GroupLeader_ID = C.Contact_ID	
AND		V.Venue_ID = @Venue_ID and V.AccommodationQuantity > 0

SELECT	C.Contact_ID, C.FirstName, C.LastName, C.Gender, 
		dbo.Common_GetAge(C.DateOfBirth) as Age, C.Email, C.Phone, C.Mobile,
		ISNULL((SELECT GroupName FROM Events_Group WHERE GroupLeader_ID = R.GroupLeader_ID),'') as GroupName
FROM	Events_Registration R, Common_Contact C 
WHERE	Venue_ID = @Venue_ID and Accommodation = 1
AND		R.Contact_ID = C.Contact_ID


SELECT	G.GroupLeader_ID, G.GroupName,
		C.FirstName, C.LastName, C.Email, C.Phone, C.Mobile,
		CateringQuantity as Purchased, 
		(SELECT COUNT(*) FROM Events_Registration WHERE Venue_ID = V.Venue_ID and GroupLeader_ID = V.GroupLeader_ID and Catering=1) as Allocated
FROM	vw_Events_GroupSummaryBulkPurchaseView V ,
		Events_Group G,
		Common_Contact C
WHERE	V.GroupLeader_ID = G.GroupLeader_ID
AND		G.GroupLeader_ID = C.Contact_ID	
AND		V.Venue_ID = @Venue_ID and V.CateringQuantity>0

SELECT	C.Contact_ID, C.FirstName, C.LastName, C.Gender, 
		dbo.Common_GetAge(C.DateOfBirth) as Age, C.Email, C.Phone, C.Mobile,
		ISNULL((SELECT GroupName FROM Events_Group WHERE GroupLeader_ID = R.GroupLeader_ID),'') as GroupName,
		R.CateringNeeds
FROM	Events_Registration R, Common_Contact C 
WHERE	Venue_ID = @Venue_ID and Catering = 1
AND		R.Contact_ID = C.Contact_ID
GO
