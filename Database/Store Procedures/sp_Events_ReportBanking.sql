SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_ReportBanking]

@Conference_ID				INT,
@StartDate					DATETIME,
@EndDate					DATETIME,
@Users						VARCHAR(1000),
@IncludeTransactionReport	BIT

AS
DECLARE @SQL		VARCHAR(7000)


/*********************************************
**										    **
**		1. Retreive Payments			    **
**										    **
**********************************************/
SET @SQL = '
	SELECT		Conference_ID, 
				PT.PaymentType, 
				PT.PaymentType_ID,
				CP.PaymentAmount, 
				CP.CCNumber, 
				CP.PaymentDate, 
				CP.PaymentBy_ID
	INTO		#Payment
	FROM		Events_ContactPayment CP,
				Common_PaymentType PT
	WHERE		CP.PaymentType_ID = PT.PaymentType_ID
	AND			CP.Conference_ID = ' + CONVERT(VARCHAR,@Conference_ID) + '
	UNION ALL
	SELECT		Conference_ID,
				PT.PaymentType,
				PT.PaymentType_ID,
				GP.PaymentAmount,
				GP.CCNumber,
				GP.PaymentDate,
				GP.PaymentBy_ID 
	FROM		Events_GroupPayment GP,
				Common_PaymentType PT 
	WHERE		GP.PaymentType_ID = PT.PaymentType_ID
	AND			GP.Conference_ID = ' + CONVERT(VARCHAR,@Conference_ID) + '

	SELECT		CASE WHEN PaymentType = ''Credit Card'' THEN
						CASE	WHEN left(CCNumber,3) like ''30[0-5]'' or left(ccnumber,2) = ''36'' THEN ''Diners''
								WHEN left(CCNumber,2) = ''34'' or left(ccnumber,2) = ''37'' then ''American Express''
								WHEN left(CCNumber,2) like ''5[1-5]'' then ''Mastercard''
								WHEN left(CCNumber,1) = ''4'' then ''Visa''
								WHEN left(CCNumber,4) = ''5610'' then ''Bankcard''
								ELSE ''Unknown Credit Card Type'' END
						Else PaymentType END
				AS PaymentType,
				PaymentType_ID,
				SUM(PaymentAmount) as PaymentAmount
	FROM		#Payment
	WHERE		PaymentDate >= ''' + CONVERT(VARCHAR,@StartDate) + '''
	AND			PaymentDate < DateAdd(dd,1,''' + CONVERT(VARCHAR,@EndDate) + ''')'

IF @Users <> ''
	SET @SQL = @SQL + '	AND			PaymentBy_ID IN (' + @Users + ')'

SET @SQL = @SQL + '
	GROUP BY	CASE WHEN PaymentType = ''Credit Card'' THEN
						CASE	WHEN left(CCNumber,3) like ''30[0-5]'' or left(ccnumber,2) = ''36'' THEN ''Diners''
								WHEN left(CCNumber,2) = ''34'' or left(ccnumber,2) = ''37'' then ''American Express''
								WHEN left(CCNumber,2) like ''5[1-5]'' then ''Mastercard''
								WHEN left(CCNumber,1) = ''4'' then ''Visa''
								WHEN left(CCNumber,4) = ''5610'' then ''Bankcard''
								ELSE ''Unknown Credit Card Type'' END
						Else PaymentType END,
				PaymentType_ID
	ORDER BY	CASE WHEN PaymentType_ID = 3 then 0 ELSE PaymentType_ID END'

EXEC(@SQL)


/**************************************************************
**			 												 **
**  2. Retreive Total Registration Amounts					 **
**															 **
**	Amount are total for Conference, not for the date range  **
**														     **
***************************************************************/

SELECT		V.Venue_ID,
			V.VenueName,
			Registrations = SUM(RegistrationCost),
			Catering = SUM(CateringCost),
			Accommodation = SUM(AccommodationCost),
			PreOrders = SUM(PreOrderCost),
			OtherCost = SUM(OtherCost),
			Total = SUM(TotalCost)
INTO		#t1
FROM		vw_Events_RegistrantCostView RCV,
			Events_Venue V
WHERE		RCV.Venue_ID = V.Venue_ID
AND			V.Conference_ID = @Conference_ID
AND			RCV.GroupLeader_ID IS NULL
GROUP BY	V.Venue_ID, V.VenueName

INSERT		#t1
SELECT		V.Venue_ID,
			V.VenueName,
			Registrations = SUM(Registrations),
			Catering = SUM(Catering),
			Accommodation = SUM(Accommodation),
			PreOrders = 0,
			OtherCost = SUM(Other),
			Total = SUM(TotalCost)
FROM		[vw_Events_GroupSummaryView] GSV,
			Events_Venue V
WHERE		GSV.Venue_ID = V.Venue_ID
AND			V.Conference_ID = @Conference_ID
GROUP BY	V.Venue_ID, V.VenueName
			
SELECT		Venue_ID,
			VenueName,
			SUM(Registrations) as Registrations,
			SUM(Catering) as Catering,
			SUM(Accommodation) as Accommodation,
			SUM(PreOrders) as PreOrders,
			CONVERT(decimal(18,2),0) as Concert,
			SUM(OtherCost) as Other,
			SUM(Total) as Total
FROM #t1
GROUP BY	Venue_ID,
			VenueName
--EXEC(@SQL)

/*********************************************
**			 							    **
**   3. Retreive Outstanding Balances	    **
**										    **
**********************************************/

SELECT		DISTINCT 'Single' as GroupSingle,
			CC.Contact_ID as GroupRegistrant_ID,
			CC.FirstName + ' ' + CC.LastName as GroupRegistrantName,
			ISNULL((SELECT SUM(TotalCost) FROM vw_Events_RegistrantCostView RCV WHERE RCV.Contact_ID = CC.Contact_ID AND RCV.GroupLeader_ID IS NULL AND RCV.Conference_ID = @Conference_ID),0) as TotalCost,
			ISNULL((SELECT SUM(PaymentAmount) FROM Events_ContactPayment CP WHERE CP.Contact_ID = CC.Contact_ID AND CP.Conference_ID = @Conference_ID),0) as Payments,
			CONVERT(MONEY, 0) as Balance
INTO		#Outstanding
FROM		Common_Contact CC

UNION ALL

SELECT		DISTINCT 'Group',
			G.GroupLeader_Id,
			G.GroupName,
			ISNULL((SELECT SUM(TotalCost) FROM vw_Events_GroupSummaryView GSV WHERE GSV.GroupLeader_Id = G.GroupLeader_Id and Conference_ID = @Conference_ID),0),
			ISNULL((SELECT SUM(PaymentAmount) FROM Events_GroupPayment GP WHERE GP.GroupLeader_Id = G.GroupLeader_Id and Conference_ID = @Conference_ID),0),
			CONVERT(MONEY, 0) as Balance
FROM		Events_Group G

DELETE FROM #Outstanding where TotalCost = Payments or (TotalCost=0 and Payments=0)
UPDATE #Outstanding SET Balance = TotalCost - Payments

SELECt	GroupSingle,
		GroupRegistrant_ID,
		GroupRegistrantName,
		Balance
FROM	#Outstanding
ORDER BY CASE WHEN GroupSingle = 'Single' THEN 1 ELSE 2 END, GroupRegistrant_ID

/*********************************************
**			 							    **
**   4. Retreive Registration Breakdowns    **
**										    **
**********************************************/
/*
IF (@IncludeTransactionReport = 1) BEGIN
	SET @SQL = '
	SELECT		(SELECT C.FirstName + '' '' + C.LastName FROM Common_Contact C WHERE Contact_ID = TL.Contact_ID) as Registrant,
				(SELECT G.GroupName FROM Events_Group G WHERE Group_ID = TL.Group_ID) as [Group],
				VenueName,
				Registrations = sum(CASE WHEN Item = ''Registration'' THEN Amount ELSE 0 END),
				Accommodation = SUM(CASE WHEN Item = ''Accommodation'' THEN Amount ELSE 0 END),
				Catering = SUM(CASE WHEN Item = ''Catering'' THEN Amount ELSE 0 END),
				Creche = SUM(CASE WHEN Item = ''Creche'' THEN Amount ELSE 0 END),
				Concert = SUM(CASE WHEN Item = ''Concert'' THEN Amount ELSE 0 END),
				PreOrders = SUM(CASE WHEN Item = ''PreOrder'' THEN Amount ELSE 0 END)
	INTO		#t1
	FROM		Events_TransactionLog TL, 
				Events_Venue V
	WHERE		[Date] >= ''' + CONVERT(VARCHAR,@StartDate) + '''
	AND 		[Date] < DateAdd(dd,1,''' + CONVERT(VARCHAR,@EndDate) + ''')'
	
	IF @Users <> ''
		SET @SQL = @SQL + '	AND			User_ID IN (' + @Users + ')'
	
	SET @SQL = @SQL + '
	AND			TL.Venue_ID = V.Venue_ID
	AND			V.Conference_ID = ' + CONVERT(VARCHAR,@Conference_ID) + '
	GROUP BY	TL.Contact_ID, TL.Group_ID, VenueName
	
	SELECT		Registrant,
				[Group],
				VenueName,
				SUM(Registrations) as Registrations,
				SUM(Accommodation) as Accommodation,
				SUM(Catering) as Catering,
				SUM(Creche) as Creche,
				SUM(Concert) as Concert,
				SUM(PreOrders) as PreOrders
	FROM		#t1
	GROUP BY	Registrant,
				[Group],
				VenueName
	HAVING		SUM(Registrations) <> 0 OR
				SUM(Accommodation) <> 0 OR
				SUM(Catering) <> 0 OR
				SUM(Creche) <> 0 OR
				SUM(Concert) <> 0 OR
				SUM(PreOrders) <> 0'
	
	EXEC(@SQL)

END


*/
GO
