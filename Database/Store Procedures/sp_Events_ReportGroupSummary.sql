SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_ReportGroupSummary]

@Conference_ID		INT,
@Venue_ID			INT,
@GroupLeader_ID		INT

AS

DECLARE @Registration_ID			INT,
		@Title						VARCHAR(300),
		@RegistrationOptionName		NVARCHAR(50),
		@RegistrationOptionValue	NVARCHAR(50)

SELECT		CONVERT(VARCHAR,G.GroupLeader_ID) as GroupNum,
			G.GroupLeader_ID,
			G.GroupName,
			C.FirstName + ' ' + C.LastName as GroupLeader,
			C.Mobile,
			C.Email,
			ISNULL(SUM(GRV.Purchased),0) AS PurchasedRegistrations,
			ISNULL(SUM(GRV.Allocated),0) AS AllocatedRegistrations,
			ISNULL(SUM(GRV.UnAllocated),0) AS UnAllocatedRegistrations,
			CONVERT(VARCHAR(3),'') as Currency,
			CONVERT(Money,0) as TotalCost,
			CONVERT(Money,0) as TotalPayments,
			CONVERT(Money,0) as Outstanding,
			1 as [Order]
INTO		#t1
FROM		Events_Group G
				LEFT JOIN vw_Events_GroupRegistrationView GRV ON G.GroupLeader_ID = GRV.GroupLeader_ID
											AND GRV.Conference_ID = COALESCE(@Conference_ID, GRV.Conference_ID)
											AND GRV.Venue_ID = COALESCE(@Venue_ID, GRV.Venue_ID),
			Common_Contact C
WHERE		G.GroupLeader_ID = C.Contact_ID
AND			G.GroupLeader_ID = COALESCE(@GroupLeader_ID, G.GroupLeader_ID)
GROUP BY	G.GroupLeader_ID,
			G.GroupName,
			C.FirstName + ' ' + C.LastName,
			C.Mobile,
			C.Email,
			GRV.Conference_ID, GRV.Venue_ID
HAVING		SUM(GRV.Purchased) > 0 
OR			SUM(GRV.Allocated) > 0 
OR			SUM(GRV.UnAllocated) > 0 
OR			@GroupLeader_ID IS NOT NULL

UPDATE #t1 SET Currency = ISNULL((SELECT Currency FROM Events_Conference C, Common_GeneralCountry Co WHERE C.Country_ID = Co.Country_ID AND C.Conference_ID = @Conference_ID),'')
UPDATE #t1 SET TotalCost =	ISNULL((SELECT TotalCost FROM vw_Events_GroupSummaryView GSV WHERE GSV.GroupLeader_ID = #t1.GroupLeader_ID AND GSV.Conference_ID = @Conference_ID AND GSV.Venue_ID = @Venue_ID),0) 
UPDATE #t1 SET TotalPayments =  ISNULL((select sum(paymentamount) from Events_GroupPayment gp where gp.GroupLeader_ID = #t1.GroupLeader_ID and Conference_ID = @Conference_ID),0)
UPDATE #t1 SET Outstanding = TotalCost - TotalPayments


IF @GroupLeader_ID IS NULL BEGIN
	INSERT #t1 (GroupNum, GroupLeader_ID, GroupName, GroupLeader, Mobile, Email, PurchasedRegistrations, AllocatedRegistrations, UnAllocatedRegistrations, TotalCost, TotalPayments, Outstanding, [Order])
	SELECT '',0,'TOTAL','','','', ISNULL(SUM(PurchasedRegistrations),0), ISNULL(SUM(AllocatedRegistrations),0), ISNULL(SUM(UnAllocatedRegistrations),0), ISNULL(SUM(TotalCost),0), ISNULL(SUM(TotalPayments),0), ISNULL(SUM(Outstanding),0), 2 FROM #t1
END

SELECT	Re.Registration_ID,
		Re.GroupLeader_ID,
		C.Contact_ID,
		C.FirstName,
		C.LastName,
		dbo.Common_GetAge(C.DateOfBirth) as Age,
		(SELECT Currency FROM Common_GeneralCountry GC, Events_Conference C WHERE GC.Country_ID = C.Country_ID AND C.Conference_ID = @Conference_ID) AS Currency,
		ISNULL((SELECT SUM(TotalCost) FROM vw_Events_RegistrantCostView RCV WHERE RCV.Contact_ID = C.Contact_ID AND RCV.Venue_ID = COALESCE(@Venue_ID, RCV.Venue_ID) AND Conference_ID = @Conference_ID AND GroupLeader_ID = G.GroupLeader_ID),0) as TotalCost,
		G.GroupName,
		V.VenueName,
		V.Venue_ID,
		RT.RegistrationType,
		CONVERT(VARCHAR,Re.RegistrationDate,105) as RegistrationDate,
		CONVERT(VARCHAR(1000),'') as OtherInformation,
		ISNULL((SELECT Name FROM Church_ULG WHERE ULG_ID = Re.ULG_ID),'') AS ULG,
		CEE.MedicalAllergies as MedicalAllergies,
		CONVERT(BIT, CASE WHEN ISNULL((SELECT TOP 1 RegistrationOptionText FROM Events_RegistrationOption RO, Events_RegistrationOptionName RON WHERE RO.RegistrationOptionName_ID = RON.RegistrationOptionName_ID AND RON.Name LIKE '%Transport%' AND RO.Registration_ID = Re.Registration_ID),0) IN ('True','1') THEN 1 ELSE 0 END) as Transport
INTO 	#t2
FROM	Common_Contact C
			LEFT JOIN Common_GeneralState S ON C.State_ID = S.State_ID
			LEFT JOIN Common_GeneralCountry Co ON C.Country_ID = Co.Country_ID,
		Common_ContactExternal CE,
		Common_ContactExternalEvents CEE,
		Events_Registration Re,
		Events_Group G,
		Events_Venue V,
		Events_RegistrationType RT
WHERE	C.Contact_ID = CE.Contact_ID
AND		CE.Contact_ID = CEE.Contact_ID
AND		CE.Contact_ID = Re.Contact_ID
AND		Re.GroupLeader_ID = G.GroupLeader_ID
AND		Re.Venue_ID = V.Venue_ID
AND		Re.RegistrationType_ID = RT.RegistrationType_ID
AND		CE.Deleted = 0
AND		V.Conference_ID = @Conference_ID
AND		Re.Venue_ID = COALESCE(@Venue_ID, Re.Venue_ID)
AND		Re.GroupLeader_ID = COALESCE(@GroupLeader_ID, Re.GroupLeader_ID)

CREATE INDEX IX_Registration_ID
on #t2 (Registration_ID)


/****************************
   Process Registrations
*****************************/
DECLARE RegistrationCursor CURSOR FOR
SELECT		RO.Registration_ID,
			RON.Description,
			CASE WHEN RON.FieldType = 'DDL' THEN (SELECT [Name] FROM Events_RegistrationOptionValue WHERE RegistrationOptionValue_ID = RO.RegistrationOptionValue_ID)
				 WHEN RON.FieldType = 'BIT' THEN CASE WHEN RO.RegistrationOptionText = 'True' THEN 'Yes' ELSE 'NO' END
				 ELSE RO.RegistrationOptionText END
	FROM	#t2,
			Events_RegistrationOption RO, 
			Events_RegistrationOptionName RON 
	WHERE	#t2.Registration_ID = RO.Registration_ID
	AND		RO.RegistrationOptionName_ID = RON.RegistrationOptionName_ID
	AND		RON.IncludeFieldInReport = 1
	AND		CASE WHEN RON.FieldType = 'DDL' THEN CASE WHEN (SELECT [IsDefault] FROM Events_RegistrationOptionValue WHERE RegistrationOptionValue_ID = RO.RegistrationOptionValue_ID) = 0 THEN 1 ELSE 0 END
				 WHEN RON.FieldType = 'BIT' THEN CASE WHEN RegistrationOptionText = 'True' THEN 1 ELSE 0 END
				 WHEN RON.FieldType = 'TXT' THEN CASE WHEN RegistrationOptionText <> '' THEN 1 ELSE 0 END
				 ELSE 1 END = 1

	OPEN RegistrationCursor


	FETCH NEXT FROM RegistrationCursor INTO @Registration_ID, @RegistrationOptionName, @RegistrationOptionValue
	WHILE @@Fetch_Status = 0 BEGIN
		UPDATE #t2 SET OtherInformation = OtherInformation + ', ' + @RegistrationOptionName + ': ' + @RegistrationOptionValue WHERE Registration_ID = @Registration_ID
		
		FETCH NEXT FROM RegistrationCursor INTO @Registration_ID, @RegistrationOptionName, @RegistrationOptionValue
	END

CLOSE RegistrationCursor
DEALLOCATE RegistrationCursor


/****************************
   Process PreOrders
*****************************/
DECLARE PreOrderCursor CURSOR FOR
	SELECT #t2.Registration_ID, P.Title FROM #t2, Events_PreOrder PO, Events_Product P WHERE #t2.Registration_ID = PO.Registration_ID AND PO.Product_ID = P.Product_ID

	OPEN PreOrderCursor

	FETCH NEXT FROM PreOrderCursor INTO @Registration_ID, @Title
	WHILE @@Fetch_Status = 0 BEGIN
		UPDATE #t2 SET OtherInformation = OtherInformation + ', Preorder: ' + @Title WHERE Registration_ID = @Registration_ID
		FETCH NEXT FROM PreOrderCursor INTO @Registration_ID, @Title
	END

CLOSE PreOrderCursor
DEALLOCATE PreOrderCursor

UPDATE #t2 SET OtherInformation = RIGHT(OtherInformation, (LEN(OtherInformation)-2)) WHERE LEN(OtherInformation) > 2


SELECT * FROM #t1 ORDER BY [Order], GroupName

SELECT DISTINCT Contact_ID, FirstName, LastName, GroupLeader_ID, GroupName, Age, Currency, TotalCost FROM #t2 ORDER BY GroupName, LastName, FirstName
SELECT Contact_ID, GroupLeader_ID, VenueName, RegistrationType, RegistrationDate, OtherInformation, ULG, MedicalAllergies, Transport FROM #t2

DROP TABLE #t1
DROP TABLE #t2
GO
