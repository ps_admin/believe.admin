SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_ReportMarketingGroupList]

@Events		VARCHAR(255),
@NumEvents	INT

AS

DECLARE @Event		INT
DECLARE @Count		INT
DECLARE @Counter	INT

SET @Count = 0
SET @Counter = 0

SELECT GroupLeader_ID, CONVERT(INT,0) as NumEvents INTO #Groups FROM Events_Group WHERE Deleted = 0

WHILE (@Events <> '') BEGIN

	IF CHARINDEX(',', @Events) > 0 BEGIN
		SET @Event = CONVERT(INT,LEFT(@Events, CHARINDEX(',', @Events)-1))
		SET @Events = RIGHT(@Events, LEN(@Events) - CHARINDEX(',', @Events))
	END
	ELSE BEGIN
		SET @Event = CONVERT(INT,@Events)
		SET @Events = ''
	END

	UPDATE #Groups 
	SET NumEvents = NumEvents + 1
	FROM Events_Registration Re, Events_Venue V
	WHERE Re.GroupLeader_ID = #Groups.GroupLeader_ID
	AND	Re.Venue_ID = V.Venue_ID
	AND V.Conference_ID = @Event

	SET @Count = @Count + 1

END

SELECT	G.GroupLeader_ID,
		G.GroupName,
		G.Church,
		G.GroupLeaderFirstName,
		G.GroupLeaderLastName,
		G.GroupLeaderAddress1,
		G.GroupLeaderAddress2,
		G.GroupLeaderSuburb,
		G.GroupLeaderPostcode,
		CASE WHEN S.State = 'Other' THEN GroupLeaderStateOther ELSE S.State END as GroupLeaderState,
		C.Country,
		G.GroupLeaderEmail,
		G.GroupLeaderMobile
FROM	#Groups,
		Events_Group G,
		Common_GeneralState S,
		Common_GeneralCountry C
WHERE	#Groups.GroupLeader_ID = G.GroupLeader_ID
AND		G.GroupLeaderState_ID = S.State_ID
AND		G.GroupLeaderCountry_ID = C.Country_ID
AND		NumEvents = @NumEvents
GO
