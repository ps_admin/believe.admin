SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_ReportMarketingGroupSummary]

@Events		VARCHAR(255)

AS


DECLARE @Event		INT
DECLARE @Count		INT
DECLARE @Counter	INT

SET @Count = 0
SET @Counter = 0

CREATE TABLE #Results
(
	NumEvents			int,
	PercentAttended		decimal(18,2),
	NumberAttended		int
)

SELECT GroupLeader_ID, CONVERT(INT,0) as NumEvents INTO #Groups FROM Events_Group WHERE Deleted = 0

WHILE (@Events <> '') BEGIN

	IF CHARINDEX(',', @Events) > 0 BEGIN
		SET @Event = CONVERT(INT,LEFT(@Events, CHARINDEX(',', @Events)-1))
		SET @Events = RIGHT(@Events, LEN(@Events) - CHARINDEX(',', @Events))
	END
	ELSE BEGIN
		SET @Event = CONVERT(INT,@Events)
		SET @Events = ''
	END

	UPDATE #Groups 
	SET NumEvents = NumEvents + 1
	FROM Events_Registration Re, Events_Venue V
	WHERE Re.GroupLeader_ID = #Groups.GroupLeader_ID
	AND	Re.Venue_ID = V.Venue_ID
	AND V.Conference_ID = @Event

	SET @Count = @Count + 1

END

WHILE @Counter <= @Count BEGIN
	
	INSERT #Results (NumEvents, PercentAttended, NumberAttended)
	SELECT @Counter, 0, COUNT(*) FROM #Groups WHERE NumEvents = @Counter

	SET @Counter = @Counter + 1

END

UPDATE #Results
SET PercentAttended = (100 * NumberAttended / CONVERT(DECIMAL(18,2),(SELECT SUM(NumberAttended) FROM #Results)))
WHERE (SELECT SUM(NumberAttended) FROM #Results) > 0

SELECT * FROM #Results

select * from #Groups

DROP TABLE #Results
DROP TABLE #Groups
GO
