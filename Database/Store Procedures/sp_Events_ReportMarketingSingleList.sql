SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_ReportMarketingSingleList]

@Events		VARCHAR(255),
@NumEvents	INT

AS

DECLARE @Event				INT,
		@Count				INT,
		@Counter			INT,
		@MailingListType	NVARCHAR(50),
		@SQL				NVARCHAR(1000)

SET @Count = 0
SET @Counter = 0

SELECT Contact_ID, CONVERT(INT,0) as NumEvents INTO #Contacts 
FROM Common_ContactExternal CE WHERE Deleted = 0

WHILE (@Events <> '') BEGIN

	IF CHARINDEX(',', @Events) > 0 BEGIN
		SET @Event = CONVERT(INT,LEFT(@Events, CHARINDEX(',', @Events)-1))
		SET @Events = RIGHT(@Events, LEN(@Events) - CHARINDEX(',', @Events))
	END
	ELSE BEGIN
		SET @Event = CONVERT(INT,@Events)
		SET @Events = ''
	END

	UPDATE #Contacts 
	SET NumEvents = NumEvents + 1
	FROM Events_Registration Re, Events_Venue V
	WHERE Re.Contact_ID = #Contacts.Contact_ID
	AND	Re.Venue_ID = V.Venue_ID
	AND V.Conference_ID = @Event

	SET @Count = @Count + 1

END

SELECT	CC.Contact_ID,
		CC.FirstName,
		CC.LastName,
		CC.Address1,
		CC.Address2,
		CC.Suburb,
		CC.PostCode,
		CASE WHEN S.State = 'Other' THEN CC.StateOther ELSE S.State END as State,
		C.Country,
		CC.Email,
		CC.Mobile
INTO	#Results
FROM	#Contacts,
		Common_Contact CC,
		Common_GeneralState S,
		Common_GeneralCountry C
WHERE	#Contacts.Contact_ID = CC.Contact_ID
AND		CC.State_ID = S.State_ID
AND		CC.Country_ID = C.Country_ID
AND		NumEvents = @NumEvents


DECLARE MailingCursor CURSOR FOR
	SELECT Name FROM Common_MailingListType

OPEN MailingCursor

FETCH NEXT FROM MailingCursor INTO @MailingListType
WHILE (@@FETCH_STATUS = 0) BEGIN
	SET @SQL = 'ALTER Table #Results ADD [' + @MailingListType + 'List] BIT'
	EXEC(@SQL)
	SET @SQL = 'UPDATE #Results SET [' + @MailingListType + 'List] = 
				(SELECT SubscriptionActive FROM Common_ContactMailingListType CMLT, Common_MailingListType MLT
				WHERE CMLT.MailingListType_ID = MLT.MailingListType_Id 
				AND Contact_ID = #Results.Contact_ID AND MLT.Name = ''' + @MailingListType + ''')'
	EXEC(@SQL)
	SET @SQL = 'UPDATE #Results SET [' + @MailingListType + 'List] = CASE WHEN [' + @MailingListType + 'List] = 1 THEN 
				(SELECT SubscriptionActive FROM Common_ContactMailingList CMLT
				WHERE Contact_ID = #Results.Contact_ID AND CMLT.MailingList_ID = 1)' + -- Events Mailing List
			   ' ELSE 0 END'

	EXEC(@SQL)
	
	FETCH NEXT FROM MailingCursor INTO @MailingListType
END

CLOSE MailingCursor
DEALLOCATE MailingCursor

SELECT * FROM #Results
		
DROP TABLE #Contacts
GO
