SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_ReportPKBus]

@Conference_ID			INT,
@Venue_ID				INT, 
@Bus					VARCHAR(20)

AS

SELECT		CC.Contact_ID,
			CC.FirstName,	
			CC.LastName,
			CONVERT(BIT, CASE WHEN @Bus = (SELECT V.[Name] FROM Events_RegistrationOptionValue V, Events_RegistrationOptionName N, Events_RegistrationOption O WHERE V.RegistrationOptionName_ID = N.RegistrationOptionName_ID AND N.[Name] = 'BUSWED' AND N.RegistrationOptionName_ID = O.RegistrationOptionName_ID AND V.RegistrationOptionValue_ID = O.RegistrationOptionValue_ID AND O.Registration_ID = Re.Registration_ID) THEN 1 ELSE 0 END) AS BusWednesday,
			CONVERT(BIT, CASE WHEN @Bus = (SELECT V.[Name] FROM Events_RegistrationOptionValue V, Events_RegistrationOptionName N, Events_RegistrationOption O WHERE V.RegistrationOptionName_ID = N.RegistrationOptionName_ID AND N.[Name] = 'BUSTHU' AND N.RegistrationOptionName_ID = O.RegistrationOptionName_ID AND V.RegistrationOptionValue_ID = O.RegistrationOptionValue_ID AND O.Registration_ID = Re.Registration_ID) THEN 1 ELSE 0 END) AS BusThursday
INTO		#t1
FROM		Common_Contact CC,
			Events_Registration Re,
			Events_Venue V
WHERE		CC.Contact_ID = Re.Contact_ID
AND			Re.Venue_ID = V.Venue_ID
AND			(V.Venue_ID = @Venue_ID OR @Venue_ID IS NULL)
AND			V.Conference_ID = @Conference_ID

SELECT		*
FROM		#t1
WHERE		BusWednesday = 1 OR
			BusThursday = 1
GO
