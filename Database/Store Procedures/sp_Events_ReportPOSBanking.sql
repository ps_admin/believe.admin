SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[sp_Events_ReportPOSBanking]

@StartDate				DATETIME,
@EndDate				DATETIME,
@Venue_ID				INT,
@POSRegister_ID			VARCHAR(50),
@IncludePOSSales		BIT,
@IncludeSessionOrders	BIT

AS

SELECT		PT.PaymentType,
			SUM(PaymentAmount) as PaymentAmount
FROM 		Events_POSTransactionPayment PTP,
			Events_POSTransaction P,
			Common_PaymentType PT
WHERE		PTP.POSTransaction_ID = P.POSTransaction_ID
AND			PTP.PaymentType = PT.PaymentType_ID
AND			P.TransactionDate >= @StartDate
AND			P.TransactionDate < @EndDate
AND			(P.Venue_ID = @Venue_ID or (P.Venue_ID IS NULL AND @Venue_ID IS NULL))
AND			(P.POSRegister_ID = @POSRegister_ID OR @POSRegister_ID IS NULL)
AND			((P.TransactionType = 1 AND @IncludePOSSales = 1)
OR			(P.TransactionType = 2 AND @IncludeSessionOrders = 1))
GROUP BY	PT.PaymentType
GO
