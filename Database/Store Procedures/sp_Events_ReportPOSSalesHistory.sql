SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_ReportPOSSalesHistory] 

@Conference_ID			INT,
@Venue_ID				INT,
@StartDate				DATETIME,
@EndDate				DATETIME,
@Product_ID				INT, 
@User_ID				INT,
@IncludePOSSales		BIT,
@IncludeSessionOrders	BIT

AS


SELECT		PT.POSTransaction_ID,
			ISNULL((SELECT VenueName FROM Events_Venue WHERE Venue_ID = PT.Venue_ID),'General POS Sales') as VenueName,
			PT.TransactionDate,
			SUM((Quantity) * ROUND(Price * ((100-Discount)/100),2)) as SaleTotal,
			U.FirstName + ' ' + U.LastName as SalesAssistant
INTO		#t1
FROM		Events_POSTransaction PT,
			Events_POSTransactionLine PTL,
			vw_Users U
WHERE		PT.POSTransaction_ID = PTL.POSTransaction_ID
AND			PT.User_ID = U.Contact_ID
AND			(PT.Venue_ID = @Venue_ID or (PT.Venue_ID IS NULL AND @Venue_ID IS NULL) OR @Venue_ID = 0)
AND			(PT.Venue_ID IN (SELECT Venue_ID FROM Events_Venue WHERE Conference_ID = @Conference_ID) OR @Conference_ID IS NULL)
AND			(PT.TransactionDate > @StartDate OR @StartDate IS NULL)
AND			(PT.TransactionDate < @EndDate OR @EndDate IS NULL)
AND			(PTL.Product_ID = @Product_ID OR @Product_ID IS NULL)
AND			(PT.User_ID = @User_ID OR @User_ID IS NULL)
AND			((PT.TransactionType = 1 AND @IncludePOSSales = 1)
OR			(PT.TransactionType = 2 AND @IncludeSessionOrders = 1))
GROUP BY	PT.POSTransaction_ID,
			PT.Venue_ID,
			PT.TransactionDate,
			U.FirstName + ' ' + U.LastName

SELECT * FROM #t1

SELECT		PTL.POSTransaction_ID,
			P.Code,
			P.Title,
			PTL.Price,
			(PTL.Quantity - QuantityReturned) as Quantity,
			(Quantity - QuantityReturned) * ROUND(PTL.Price * ((100-PTL.Discount)/100),2) as Total
FROM		Events_POSTransactionLine PTL,
			Events_Product P
WHERE		PTL.Product_ID = P.Product_ID
AND			PTL.POSTransaction_ID IN (SELECT POSTransaction_ID FROM #t1)

SELECT		PTP.POSTransaction_ID,
			PT.PaymentType,
			PTP.PaymentAmount
FROM		Events_POSTransactionPayment PTP,
			Common_PaymentType PT
WHERE		PTP.PaymentType = PT.PaymentType_ID
AND			PTP.POSTransaction_ID IN (SELECT POSTransaction_ID FROM #t1)

DROP TABLE #t1
GO
