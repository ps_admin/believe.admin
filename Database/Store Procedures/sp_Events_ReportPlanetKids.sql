SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_ReportPlanetKids]

@Conference_ID	INT,
@Venue_ID		INT

AS

SELECT		C.Contact_ID,
			C.FirstName,
			C.LastName,
			[Age] = dbo.Common_GetAge(C.DateOfBirth),
			[Name] = C.FirstName + ' ' + C.LastName,
			CEE.MedicalAllergies,
			CEE.MedicalInformation,
			RCI.DropOffPickUpName,
			RCI.DropOffPickUpRelationship,
			RCI.DropOffPickUpContactNumber,
			RCI.ConsentFormReturned,
			RCI.ConsentFormReturnedDate,
			RCI.ChildrensChurchPastor,
			RCI.ChildsFriend,
			[SchoolGrade] = ISNULL((SELECT Grade FROM Common_SchoolGrade WHERE SchoolGrade_ID = RCI.CrecheChildSchoolGrade_ID),'')
FROM		Common_Contact C,
			Common_ContactExternal CE,
			Common_ContactExternalEvents CEE,
			Events_Registration Re
				LEFT JOIN Events_RegistrationChildInformation RCI ON Re.Registration_ID = RCI.Registration_ID,
			Events_RegistrationType RT,
			Events_Venue V
WHERE		C.Contact_ID = CE.Contact_ID
AND			CE.Contact_ID = Re.Contact_ID
AND			CE.Contact_ID = CEE.Contact_ID
AND			Re.Venue_ID = V.Venue_ID
AND			Re.RegistrationType_ID = RT.RegistrationType_ID
AND			RT.IsChildRegistrationType = 1
AND			(Re.Venue_ID = @Venue_ID OR @Venue_ID IS NULL)
AND			V.Conference_ID = @Conference_ID
AND			CE.Deleted = 0
ORDER BY	C.LastName, C.FirstName


SELECT		COUNT(*) as Total,
			ISNULL(SUM(CASE WHEN CHARINDEX('early childhood', RT.RegistrationType) > 0 THEN 1 ELSE 0 END),0) as EarlyChildhood,
			ISNULL(SUM(CASE WHEN CHARINDEX('early childhood', RT.RegistrationType) = 0 THEN 1 ELSE 0 END),0) as PrimaryAge
FROM		Common_Contact CC,
			Common_ContactExternal CE,
			Events_Registration Re,
			Events_RegistrationType RT,
			Events_Venue V
WHERE		CC.Contact_ID = CE.Contact_ID
AND			CE.Contact_ID = RE.Contact_ID
AND			Re.RegistrationType_ID = RT.RegistrationType_ID
AND			RT.IsChildRegistrationType = 1
AND			Re.Venue_ID = V.Venue_ID
AND			(Re.Venue_ID = @Venue_ID OR @Venue_ID IS NULL)
AND			V.Conference_ID = @Conference_ID
AND			CE.Deleted = 0
GO
