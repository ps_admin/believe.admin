SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_ReportRegistrantDetails]

@Registration_ID	INT

AS

SELECT	C.Contact_ID,
		C.FirstName + ' ' + C.LastName as ContactName,
		CAV.StreetAddressMultiLine as Address,
		C.Phone,
		C.Fax,
		C.Gender,
		C.Email,
		C.PhoneWork,
		C.Mobile,
		C.DateOfBirth,
		CE.ChurchName,
		ISNULL((SELECT Denomination FROM Common_Denomination WHERE Denomination_ID = CE.Denomination_ID),'') as Denomination,
		CEV.EmergencyContactName,
		CEV.EmergencyContactPhone,
		CEV.EmergencyContactRelationship,
		CE.LeaderName,
		CEV.MedicalAllergies,
		CEV.MedicalInformation,
		CEV.EventComments,
		(SELECT FirstName + ' ' + LastName FROM Common_Contact WHERE Contact_ID = C.CreatedBy_ID) as CreatedBy,
		(SELECT FirstName + ' ' + LastName FROM Common_Contact WHERE Contact_ID = C.ModifiedBy_ID) as ModifiedBy,
		C.CreationDate,
		C.ModificationDate,
		Co.ConferenceName,
		V.VenueName,
		RT.RegistrationType,
		R.RegistrationDiscount,
		ISNULL((SELECT GroupName FROM Events_Group WHERE GroupLeader_ID = R.GroupLeader_ID), '') as [GroupName],
		ISNULL((SELECT Name FROM Events_Elective WHERE Elective_ID = R.Elective_ID), '') as Elective,
		R.Accommodation,
		R.Catering,
		R.CateringNeeds,
		R.LeadershipBreakfast,
		R.Volunteer,
		ISNULL((SELECT VolunteerDepartment FROM Events_VolunteerDepartment WHERE VolunteerDepartment_ID = R.VolunteerDepartment_ID), '') as VolunteerDepartment,
		R.ParentGuardian,
		R.ParentGuardianPhone,
		(SELECT FirstName + ' ' + LastName FROM Common_Contact WHERE Contact_ID = R.RegisteredBy_ID) as RegisteredBy,
		R.RegistrationDate,
		R.Attended,
		Co.Conference_ID
FROM	Common_Contact C,
		vw_Common_ContactAddressView CAV,
		Common_ContactExternal CE,
		Common_ContactExternalEvents CEV,
		Events_Registration R,
		Events_Venue V,
		Events_Conference Co,
		Events_RegistrationType RT
WHERE	C.Contact_ID = CAV.Contact_ID
AND		C.Contact_ID = CE.Contact_ID
AND		C.Contact_ID = CEV.Contact_ID
AND		C.Contact_ID = R.Contact_ID
AND		R.Venue_ID = V.Venue_ID
AND		V.Conference_ID = Co.Conference_ID
AND		R.RegistrationType_ID = RT.RegistrationType_ID
AND		R.Registration_ID = @Registration_ID

SELECT	RON.Description as Name,
		CASE WHEN ROV.FieldType = 'DDL' 
				THEN (SELECT Name FROM Events_RegistrationOptionValue WHERE RegistrationOptionName_ID = ROV.RegistrationOptionName_ID AND Value = ROV.Value)
			 WHEN ROV.FieldType = 'BIT'
				THEN CASE WHEN Value = 0 THEN 'No' ELSE 'Yes' END	
			 ELSE ROV.Value 
		END as Value
FROM	vw_Events_RegistrationOptionView ROV,
		Events_RegistrationOptionName RON
WHERE	ROV.RegistrationOptionName_ID = RON.RegistrationOptionName_ID
AND		Registration_ID = @Registration_ID



SELECT	P.Code,
		P.Title,
		P.AuthorArtist,
		ISNULL((SELECT Price FROM Events_ProductPrice WHERE Product_ID = P.Product_ID AND Country_ID = C.Country_ID), '') as Price,
		PO.Discount,
		ROUND(ISNULL((SELECT Price FROM Events_ProductPrice WHERE Product_ID = P.Product_ID), '') * (100 - PO.Discount)/100,2) as Value
FROM	Events_PreOrder PO,
		Events_Product P,
		Events_Registration R, 
		Events_Venue V,
		Events_Conference C
WHERE	PO.Product_ID = P.Product_ID
AND		PO.Registration_ID = R.Registration_ID
AND		R.Venue_ID = V.Venue_ID
AND		V.Conference_ID = C.Conference_ID
AND		PO.Registration_ID = @Registration_ID
GO
