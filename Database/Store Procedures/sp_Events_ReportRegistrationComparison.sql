SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_ReportRegistrationComparison]

@Conference_ID		INT,
@Days				INT

AS

SET NOCOUNT ON

DECLARE		@ReportStartDate	DATETIME,
			@ConferenceEndDate	DATETIME,
			@Count				INT

SELECT		@ConferenceEndDate = MAX(ConferenceEndDate) FROM Events_Venue WHERE Conference_ID = @Conference_ID
SET			@ReportStartDate =	DATEADD(DD, -@Days, @ConferenceEndDate)

CREATE TABLE #Report (
	[Day]			INT,
	[DayInverse]	INT,
	[Date]			NVARCHAR(20),
	[Regos]			INT
)

SET @Count = 1
WHILE (@Count <= @Days) BEGIN
	INSERT #Report([Day], [DayInverse], [Date], Regos) VALUES (@Count, @Days - @Count + 1, CONVERT(NVARCHAR, DATEADD(dd, @Count, @ReportStartDate), 102), 0)
	SET @Count = @Count + 1
END

SELECT		CONVERT(NVARCHAR, T.[Date], 102) as [Date],
			SUM(Quantity) as Regos
INTO		#Regos
FROM		Events_RegistrationTotals T,
			Events_Venue V
WHERE		T.Venue_ID = V.Venue_ID
AND			V.Conference_ID = @Conference_ID
GROUP BY	CONVERT(NVARCHAR, T.[Date], 102)

UPDATE	#Report
SET		Regos = (SELECT SUM(Regos)
				 FROM	#Regos R
				 WHERE	#Report.[Date] = R.[Date])
				 
UPDATE	#Report
SET		Regos = 0
WHERE	Regos IS NULL
AND		[Date] < (SELECT MIN([Date]) FROM #Regos WHERE Regos > 0)
				 
UPDATE	#Report
SET		Regos = (SELECT MAX(Regos)
				 FROM	#Regos)
WHERE	Regos IS NULL
			
SELECT	@Conference_ID, 
		* 
FROM	#Report

DROP TABLE	#Report, #Regos
GO
