SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_ReportRegistrationComparison2]

@Conference_ID		INT,
@Days				INT

AS

DECLARE		@ReportStartDate	DATETIME,
			@ConferenceEndDate	DATETIME,
			@Count				INT

SELECT		@ConferenceEndDate = MAX(ConferenceEndDate) FROM Events_Venue WHERE Conference_ID = @Conference_ID
SET			@ReportStartDate =	DATEADD(DD, -@Days, @ConferenceEndDate)


SELECT		CONVERT(INT, 0) AS [Day],
			CONVERT(INT, 0) AS [DayInverse],
			CONVERT(NVARCHAR,'') AS [Date],
			CONVERT(INT, 0) AS [Regos]
INTO		#t2 
WHERE		1=0

SET @Count = 1
WHILE (@Count <= @Days) BEGIN
	INSERT #t2([Day], [DayInverse], [Date], Regos) VALUES (@Count, @Days - @Count + 1, CONVERT(NVARCHAR, DATEADD(dd, @Count, @ReportStartDate), 102), 0)
	SET @Count = @Count + 1
END

-- Calculate Registrations
SELECT		CONVERT(NVARCHAR, DateAdded, 102) AS [Date], SUM(Quantity) as Quantity
INTO		#Registrations
FROM		Events_GroupBulkRegistration B, Events_Venue V 
WHERE		B.Venue_ID = V.Venue_ID AND	V.Conference_ID = @Conference_ID
GROUP BY	CONVERT(NVARCHAR, DateAdded, 102)

INSERT		#Registrations
SELECT		CONVERT(NVARCHAR, RegistrationDate, 102) AS [Date], COUNT(*) as Quantity
FROM		Events_Registration R, Events_Venue V
WHERE		R.Venue_ID = V.Venue_ID AND	V.Conference_ID = @Conference_ID AND GroupLeader_ID IS NULL
GROUP BY	CONVERT(NVARCHAR, RegistrationDate, 102)


SELECT		GroupLeader_ID, RegistrationType_ID, SUM(Quantity) as Quantity
INTO		#BulkSummary
FROM		Events_GroupBulkRegistration B, Events_Venue V 
WHERE		B.Venue_ID = V.Venue_ID AND	V.Conference_ID = @Conference_ID
GROUP BY	GroupLeader_ID, RegistrationType_ID

SELECT		GroupLeader_ID, RegistrationType_ID, COUNT(*) as Quantity
INTO		#RegoSummary
FROM		Events_Registration R, Events_Venue V
WHERE		R.Venue_ID = V.Venue_ID AND	V.Conference_ID = @Conference_ID AND GroupLeader_ID IS NOT NULL
GROUP BY	GroupLeader_ID, RegistrationType_ID

			
UPDATE #RegoSummary SET Quantity = B2.Quantity - B.Quantity
FROM #BulkSummary B, #RegoSummary B2 
WHERE B.GroupLeader_ID = B2.GroupLeader_ID AND B.RegistrationType_ID = B2.RegistrationType_ID


DELETE FROM #RegoSummary WHERE Quantity <= 0


DECLARE @GroupLeader_ID INT, @RegistrationType_ID INT, @Quantity INT, @SQL NVARCHAR(MAX)
DECLARE RegoCursor CURSOR FOR
	SELECT * FROM #RegoSummary
OPEN RegoCursor


FETCH NEXT FROM RegoCursor INTO @GroupLeader_ID, @RegistrationType_ID, @Quantity
WHILE (@@FETCH_STATUS = 0) BEGIN

	SET @SQL = 'INSERT #Registrations
				SELECT TOP ' + CONVERT(NVARCHAR, @Quantity) + ' CONVERT(NVARCHAR, RegistrationDate, 102) , 1
				FROM Events_Registration 
				WHERE	GroupLeader_ID = ' + CONVERT(NVARCHAR, @GroupLeader_ID) + '
				AND		RegistrationType_ID = ' + CONVERT(NVARCHAR, @RegistrationType_ID) + '
				ORDER BY RegistrationDate DESC'
	
	EXEC(@SQL)
	
	FETCH NEXT FROM RegoCursor INTO @GroupLeader_ID, @RegistrationType_ID, @Quantity
END

CLOSE RegoCursor
DEALLOCATE RegoCursor

SELECT [Date], SUM(Quantity) as Quantity INTO #Registrations2 FROM #Registrations GROUP BY [Date]

UPDATE #t2 SET Regos = Quantity FROM #Registrations2 R WHERE R.[Date] = #t2.[Date] -- OR CONVERT(DATETIME,R.[Date]) < @ReportStartDate
UPDATE #t2 SET Regos = ISNULL((SELECT SUM(Quantity) FROM #Registrations2 R WHERE CONVERT(DateTime, R.[Date]) < (SELECT MIN([Date]) FROM #t2)),0)
WHERE #t2.[Date] = (SELECT MIN([Date]) FROM #t2) 

--Accumulate Progressive Regos
UPDATE #t2 SET Regos = (SELECT SUM(Regos) FROM #t2 t WHERE t.[Day] <= #t2.[Day])

SELECT @Conference_ID, * FROM #t2 ORDER BY [day]

DROP TABLE #t2

DROP TABLE #Registrations, #BulkSummary, #RegoSummary, #Registrations2
GO
