SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_ReportSalesPOS]

@Conference_ID			INT,
@Venue_ID				INT,
@StartDate				SMALLDATETIME,
@EndDate				SMALLDATETIME,
@POSRegister_ID			INT,
@IncludePOSSales		BIT,
@IncludeSessionOrders	BIT

AS

SELECT 		0 as SortKey,
			P.Product_ID,
			P.Code,
			P.Title,
			SUM(CASE WHEN PTL.Discount<>100 THEN (PTL.Quantity) ELSE 0 END) as Quantity,
			SUM(CASE WHEN PTL.Discount=100 THEN (PTL.Quantity) ELSE 0 END) as Comp,
			SUM(PTL.Quantity) as TotalQuantity,
			SUM(CASE WHEN PT.TransactionType = 2 THEN PTL.QuantitySupplied ELSE (PTL.Quantity) END) AS QuantitySupplied,
			COUNT(PT.POSTransaction_ID) as Trans,
			CASE WHEN SUM(PTL.Quantity) <> 0 THEN SUM(PTL.Net) / SUM(PTL.Quantity) ELSE 0 END as AvgPrice,
			SUM(PTL.Net) as Total
INTO		#t1
FROM		Events_Product P,
			Events_POSTransaction PT,
			Events_POSTransactionLine PTL
WHERE		P.Product_ID = PTL.Product_ID
AND			PTL.POSTransaction_ID = PT.POSTransaction_ID
AND			PT.TransactionDate >= @StartDate
AND			PT.TransactionDate < @EndDate
AND			(PT.Venue_ID = @Venue_ID or (PT.Venue_ID IS NULL AND @Venue_ID IS NULL)  or @Venue_ID = 0) -- VenueID Zero is for All venues for current conference
AND			(PT.Venue_ID IN (SELECT Venue_ID FROM Events_Venue WHERE Conference_ID = @Conference_ID) OR @Conference_ID IS NULL)
AND			(PT.POSRegister_ID = @POSRegister_ID OR @POSRegister_ID IS NULL)
AND			((PT.TransactionType = 1 AND @IncludePOSSales = 1)
OR			(PT.TransactionType = 2 AND @IncludeSessionOrders = 1))
GROUP BY	P.Product_ID,
			P.Code,
			P.Title

INSERT #t1
SELECT 	1,
		0,
		'', 
		'TOTAL', 
		ISNULL(SUM(Quantity),''),
		ISNULL(SUM(Comp),''),
		ISNULL(SUM(TotalQuantity),''),
		ISNULL(SUM(QuantitySupplied),''),
		ISNULL(SUM(Trans),''), 
		ISNULL(CASE WHEN SUM(Quantity) <> 0 THEN SUM(Total) / SUM(Quantity) ELSE '' END,''), 
		ISNULL(SUM(Total),'')
FROM #t1

SELECT		* 
FROM		#t1 
WHERE		ISNULL(Quantity,0) <> 0
OR			ISNULL(Comp,0) <> 0
OR			ISNULL(TotalQuantity,0) <> 0
OR			ISNULL(QuantitySupplied,0) <> 0
OR			ISNULL(Total,0) <> 0		
ORDER BY	SortKey, 
			Title
GO
