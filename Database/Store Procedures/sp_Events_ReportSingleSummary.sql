SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_ReportSingleSummary]

@Conference_ID	INT,
@Venue_ID		INT

AS

DECLARE @Registration_ID			INT,
		@Title						VARCHAR(300),
		@RegistrationOptionName		NVARCHAR(50),
		@RegistrationOptionValue	NVARCHAR(50)

SELECT	Re.Registration_ID,
		CC.Contact_ID,
		CC.FirstName,
		CC.LastName,
		CASE WHEN CC.Address1 <> '' THEN CC.Address1 ELSE '' END +
		CASE WHEN CC.Address2 <> '' THEN ', ' + CC.Address1 ELSE '' END +
		CASE WHEN CC.Suburb <> '' THEN ', ' + CC.Suburb ELSE '' END +
		CASE WHEN (CASE WHEN CHARINDEX('Other', S.State) > 0 THEN CC.StateOther ELSE S.State END) <> '' THEN ', ' + (CASE WHEN CHARINDEX('Other',S.State) > 0  THEN CC.StateOther ELSE S.State END) ELSE '' END +
		CASE WHEN CC.PostCode <> '' THEN ', ' + CC.PostCode ELSE '' END +
		CASE WHEN C.Country <> 'Australia' THEN ', ' + C.Country ELSE '' END AS Address,
		dbo.Common_GetAge(CC.DateOfBirth) as Age,
		(SELECT GC.Currency FROM Common_GeneralCountry GC, Events_Conference C WHERE C.Country_ID = GC.Country_ID AND C.Conference_ID = @Conference_ID) as Currency,
		ISNULL((SELECT SUM(TotalCost) FROM vw_Events_RegistrantCostView WHERE Contact_ID = Re.Contact_ID AND Conference_ID = V.Conference_ID AND GroupLeader_ID IS NULL),0) as TotalCost,
		ISNULL((SELECT SUM(PaymentAmount) FROM Events_ContactPayment CP WHERE CP.Contact_ID = CC.Contact_ID AND CP.Conference_ID = V.Conference_ID AND GroupLeader_ID IS NULL),0) AS TotalPayments,
		CONVERT(Money,0) as Outstanding,
		V.Venue_ID,
		V.VenueName,
		RT.RegistrationType,
		CONVERT(VARCHAR,Re.RegistrationDate,105) as RegistrationDate,
		CONVERT(NVARCHAR(MAX),'') as OtherInformation,
		ISNULL((SELECT Name FROM Church_ULG WHERE ULG_ID = Re.ULG_ID),'') AS ULG,
		CEE.MedicalAllergies as MedicalAllergies,
		CONVERT(BIT, CASE WHEN ISNULL((SELECT TOP 1 RegistrationOptionText FROM Events_RegistrationOption RO, Events_RegistrationOptionName RON WHERE RO.RegistrationOptionName_ID = RON.RegistrationOptionName_ID AND RON.Name LIKE '%Transport%' AND RO.Registration_ID = Re.Registration_ID),0) IN ('True','1') THEN 1 ELSE 0 END) as Transport
INTO 	#t1
FROM	Common_Contact CC
			LEFT JOIN Common_GeneralState S ON CC.State_ID = S.State_ID
			LEFT JOIN Common_GeneralCountry C ON CC.State_ID = C.Country_ID,
		Common_ContactExternal CE,
		Common_ContactExternalEvents CEE,
		Events_Registration Re,
		Events_Venue V,
		Events_RegistrationType RT
WHERE	CC.Contact_ID = CE.Contact_ID
AND		CE.Contact_ID = CEE.Contact_ID
AND		CE.Contact_ID = Re.Contact_ID
AND		Re.Venue_ID = V.Venue_ID
AND		Re.RegistrationType_ID = RT.RegistrationType_ID
AND		Ce.Deleted = 0
AND		Re.GroupLeader_ID IS NULL
AND		(Re.Venue_ID = @Venue_ID OR @Venue_ID IS NULL)
AND		V.Conference_ID = @Conference_ID

CREATE INDEX IX_Registration_ID
on #t1 (Registration_ID)

UPDATE #t1 SET Outstanding = TotalCost - TotalPayments


/****************************
   Process Registrations
*****************************/
DECLARE RegistrationCursor CURSOR FOR
	SELECT	RO.Registration_ID,
			RON.Description,
			CASE WHEN RON.FieldType = 'DDL' THEN (SELECT [Name] FROM Events_RegistrationOptionValue WHERE RegistrationOptionValue_ID = RO.RegistrationOptionValue_ID)
				 WHEN RON.FieldType = 'BIT' THEN CASE WHEN RegistrationOptionText = 'True' THEN 'Yes' ELSE 'No' END
		    ELSE RO.RegistrationOptionText END
	FROM	#t1,
			Events_RegistrationOption RO, 
			Events_RegistrationOptionName RON 
	WHERE	#t1.Registration_ID = RO.Registration_ID
	AND		RO.RegistrationOptionName_ID = RON.RegistrationOptionName_ID
	AND		RON.IncludeFieldInReport = 1
	AND		CASE WHEN RON.FieldType = 'DDL' THEN CASE WHEN (SELECT [IsDefault] FROM Events_RegistrationOptionValue WHERE RegistrationOptionValue_ID = RO.RegistrationOptionValue_ID) = 0 THEN 1 ELSE 0 END
				 WHEN RON.FieldType = 'BIT' THEN CASE WHEN RegistrationOptionText = 'True' THEN 1 ELSE 0 END
				 WHEN RON.FieldType = 'TXT' THEN CASE WHEN RegistrationOptionText <> '' THEN 1 ELSE 0 END
				 ELSE 1 END = 1
		
	OPEN RegistrationCursor

	FETCH NEXT FROM RegistrationCursor INTO @Registration_ID, @RegistrationOptionName, @RegistrationOptionValue
	WHILE @@Fetch_Status = 0 BEGIN
		UPDATE #t1 SET OtherInformation = OtherInformation + ', ' + @RegistrationOptionName + ': ' + @RegistrationOptionValue WHERE Registration_ID = @Registration_ID
		
		FETCH NEXT FROM RegistrationCursor INTO @Registration_ID, @RegistrationOptionName, @RegistrationOptionValue
	END

CLOSE RegistrationCursor
DEALLOCATE RegistrationCursor


/****************************
   Process PreOrders
*****************************/
DECLARE PreOrderCursor CURSOR FOR
	SELECT #t1.Registration_ID, P.Title FROM #t1, Events_PreOrder PO, Events_Product P WHERE #t1.Registration_ID = PO.Registration_ID AND PO.Product_ID = P.Product_ID

	OPEN PreOrderCursor

	FETCH NEXT FROM PreOrderCursor INTO @Registration_ID, @Title
	WHILE @@Fetch_Status = 0 BEGIN
		UPDATE #t1 SET OtherInformation = OtherInformation + ', Preorder: ' + @Title  WHERE Registration_ID = @Registration_ID
		FETCH NEXT FROM PreOrderCursor INTO @Registration_ID, @Title
	END

CLOSE PreOrderCursor
DEALLOCATE PreOrderCursor

UPDATE #t1 SET OtherInformation = RIGHT(OtherInformation, (LEN(OtherInformation)-2)) WHERE LEN(OtherInformation) > 2

SELECT DISTINCT Contact_ID, FirstName, LastName, Address, Age, Currency, TotalCost, TotalPayments, Outstanding FROM #t1 ORDER BY LastName, FirstName

SELECT Contact_ID, VenueName, RegistrationType, RegistrationDate, OtherInformation, ULG, MedicalAllergies, Transport FROM #t1

DROP TABLE #t1
GO
