SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_ReportSummaryAge]

@Conference_ID	INT,
@State_ID		INT

AS

CREATE TABLE #Ages
(
	StartDate	DATETIME,
	EndDate		DATETIME,
	Age			VARCHAR(20),
	SortOrder	INT
)

INSERT #Ages VALUES (DATEADD(yy,-5,dbo.GetRelativeDate()), dbo.GetRelativeDate(), 'Under 5', 1)
INSERT #Ages VALUES (DATEADD(yy,-13,dbo.GetRelativeDate()), DATEADD(yy,-5,dbo.GetRelativeDate()), '5 - 12', 2)
INSERT #Ages VALUES (DATEADD(yy,-16,dbo.GetRelativeDate()), DATEADD(yy,-13,dbo.GetRelativeDate()), '13 - 15', 3)
INSERT #Ages VALUES (DATEADD(yy,-18,dbo.GetRelativeDate()), DATEADD(yy,-16,dbo.GetRelativeDate()), '16 - 17', 4)
INSERT #Ages VALUES (DATEADD(yy,-21,dbo.GetRelativeDate()), DATEADD(yy,-18,dbo.GetRelativeDate()), '18 - 20', 5)
INSERT #Ages VALUES (DATEADD(yy,-25,dbo.GetRelativeDate()), DATEADD(yy,-21,dbo.GetRelativeDate()), '21 - 24', 6)
INSERT #Ages VALUES (DATEADD(yy,-30,dbo.GetRelativeDate()), DATEADD(yy,-25,dbo.GetRelativeDate()), '25 - 29', 7)
INSERT #Ages VALUES (DATEADD(yy,-36,dbo.GetRelativeDate()), DATEADD(yy,-30,dbo.GetRelativeDate()), '30 - 35', 8)
INSERT #Ages VALUES (DATEADD(yy,-46,dbo.GetRelativeDate()), DATEADD(yy,-36,dbo.GetRelativeDate()), '36 - 45', 9)
INSERT #Ages VALUES (DATEADD(yy,-56,dbo.GetRelativeDate()), DATEADD(yy,-46,dbo.GetRelativeDate()), '46 - 55', 10)
INSERT #Ages VALUES (DATEADD(yy,-200,dbo.GetRelativeDate()), DATEADD(yy,-56,dbo.GetRelativeDate()), '55+', 11)
INSERT #Ages VALUES (DATEADD(yy,-200,dbo.GetRelativeDate()), dbo.GetRelativeDate(), 'TOTAL', 12)


SELECT DISTINCT C.Contact_ID,
			C.DateOfBirth,
			C.Gender
INTO		#t1
FROM		Common_Contact C,
			Common_ContactExternal CE, 
			Events_Registration Re,
			Events_Venue V
WHERE		C.Contact_ID = CE.Contact_ID
AND			CE.Contact_ID = Re.Contact_ID
AND			Re.Venue_ID = V.Venue_ID
AND			CE.Deleted = 0
AND			V.Conference_ID = @Conference_ID
AND			(V.State_ID = @State_ID OR @State_ID IS NULL)

SELECT 		#Ages.Age, #Ages.StartDate, #Ages.EndDate, 
			SUM(CASE WHEN #t1.DateOfBirth > #Ages.StartDate AND #t1.DateOfBirth <= #Ages.EndDate  AND #t1.Gender = 'Male' THEN 1 ELSE 0 END) as Male,
			SUM(CASE WHEN #t1.DateOfBirth > #Ages.StartDate AND #t1.DateOfBirth <= #Ages.EndDate  AND #t1.Gender = 'Female' THEN 1 ELSE 0 END) as Female,
			SUM(CASE WHEN #t1.DateOfBirth > #Ages.StartDate AND #t1.DateOfBirth <= #Ages.EndDate THEN 1 ELSE 0 END) as Total
FROM		#t1, 
			#Ages
GROUP BY	#Ages.Age, #Ages.StartDate, #Ages.EndDate, #Ages.SortOrder
ORDER BY	SortOrder
GO
