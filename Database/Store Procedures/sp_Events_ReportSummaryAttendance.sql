SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_ReportSummaryAttendance]

@Conference_ID	INT,
@State_ID		INT

AS

DECLARE		@SQL							NVARCHAR(MAX),
			@RegistrationTypeReportGroup_ID	INT

create table #t1 (
	Venue_ID			INT,
	VenueName			VARCHAR(50),
	TotalRegos			INT,
	TotalRegistrants	INT,
	AllocatedRegos		INT,
	Accommodation		INT,
	Catering			INT,
	LeadershipBreakfast	INT,
	Attended			INT
)

SELECT 		Re.Registration_ID,
			CE.Contact_ID,
			V.Venue_ID,
			Re.GroupLeader_ID,
			RT.RegistrationTypeReportGroup_ID,
			COUNT(*) as qty, 
			SUM(convert(int,Re.Attended,0)) as Attended
INTO		#t2
FROM 		Common_ContactExternal CE,
			Events_Registration Re,
			Events_RegistrationType RT, 
			Events_Venue V
WHERE 		CE.Contact_ID = Re.Contact_ID
AND 		Re.RegistrationType_ID = RT.RegistrationType_ID
AND 		Re.Venue_ID = V.Venue_ID
AND 		(V.State_ID = @State_ID OR @State_ID IS NULL)
AND 		V.Conference_ID = @Conference_ID
AND 		CE.Deleted = 0
GROUP BY	Re.Registration_ID,
			CE.Contact_ID,
			RT.RegistrationTypeReportGroup_ID, 
			V.Venue_ID,
			Re.GroupLeader_ID
			
--Select Pre-registrations and add them to the totals
SELECT Venue_ID, RegistrationTypeReportGroup_ID, SUM(TotalRegistrations) as Qty INTO #t3 FROM vw_Events_GroupSummaryRegistrationView V, Events_RegistrationType RT
WHERE V.RegistrationType_ID = RT.RegistrationType_ID AND V.Conference_ID = @Conference_ID GROUP BY Venue_ID, RegistrationTypeReportGroup_ID

INSERT #t1 (Venue_ID, VenueName)
SELECT DISTINCT Venue_ID, VenueName FROM Events_Venue WHERE Conference_ID = @Conference_ID

UPDATE #t1 SET TotalRegos =				ISNULL((SELECT sum(#t2.qty) from #t2 where #t2.Venue_ID = #t1.Venue_ID and GroupLeader_ID IS NULL),0) + ISNULL((SELECT SUM(Qty) FROM #t3 WHERE Venue_ID = #t1.Venue_ID),0)
UPDATE #t1 SET TotalRegistrants =		ISNULL((SELECT COUNT(DISTINCT Contact_ID) FROM #t2 WHERE Venue_ID = #t1.Venue_ID),0)
UPDATE #t1 SET AllocatedRegos =			ISNULL((select sum(#t2.qty) from #t2 where #t2.Venue_ID = #t1.Venue_ID),0) 
UPDATE #t1 SET Accommodation =			ISNULL((SELECT COUNT(*) FROM Events_Registration R WHERE R.Accommodation = 1 AND GroupLeader_ID IS NULL AND R.Venue_ID = #t1.Venue_ID), 0) +
										ISNULL((SELECT SUM(AccommodationQuantity) FROM vw_Events_GroupSummaryBulkPurchaseView GBP WHERE GBP.Venue_ID = #t1.Venue_ID ),0)
UPDATE #t1 SET Catering =				ISNULL((SELECT COUNT(*) FROM Events_Registration R WHERE R.Catering = 1 AND GroupLeader_ID IS NULL AND R.Venue_ID = #t1.Venue_ID), 0) +
										ISNULL((SELECT SUM(CateringQuantity) FROM vw_Events_GroupSummaryBulkPurchaseView GBP WHERE GBP.Venue_ID = #t1.Venue_ID ),0)
UPDATE #t1 SET LeadershipBreakfast =	ISNULL((SELECT COUNT(*) FROM Events_Registration R WHERE R.LeadershipBreakfast = 1 AND GroupLeader_ID IS NULL AND R.Venue_ID = #t1.Venue_ID), 0) +
										ISNULL((SELECT SUM(LeadershipBreakfastQuantity) FROM vw_Events_GroupSummaryBulkPurchaseView GBP WHERE GBP.Venue_ID = #t1.Venue_ID ),0)
UPDATE #t1 SET Attended =				ISNULL((SELECT SUM(Attended) FROM #t2 WHERE #t2.Venue_ID = #t1.Venue_ID),0)


INSERT #t1 (VenueName, TotalRegos, TotalRegistrants, AllocatedRegos, Accommodation, Catering, LeadershipBreakfast, Attended)
SELECT 	'TOTAL',
		SUM(TotalRegos),
		SUM(TotalRegistrants),
		SUM(AllocatedRegos),
		SUM(Accommodation),
		SUM(Catering),
		SUM(LeadershipBreakfast),
		SUM(Attended)
FROM #t1


DECLARE RTGCursor CURSOR FOR
	SELECT RegistrationTypeReportGroup_ID FROM Events_RegistrationTypeReportGroup WHERE Conference_ID = @Conference_ID
OPEN RTGCursor

FETCH NEXT FROM RTGCursor INTO @RegistrationTypeReportGroup_ID
WHILE (@@FETCH_STATUS = 0) BEGIN

	SET @SQL = 'ALTER TABLE #t1	ADD [' + (SELECT Name FROM Events_RegistrationTypeReportGroup WHERE RegistrationTypeReportGroup_ID = @RegistrationTypeReportGroup_ID) + '] INT'
	EXEC(@SQL)

	SET @SQL = 'UPDATE #t1 SET [' + (SELECT Name FROM Events_RegistrationTypeReportGroup WHERE RegistrationTypeReportGroup_ID = @RegistrationTypeReportGroup_ID) + '] = ' +
			   'ISNULL((SELECT SUM(Qty) FROM #t2 WHERE (Venue_ID = #t1.Venue_ID OR #t1.Venue_ID IS NULL) AND RegistrationTypeReportGroup_ID = ' + CONVERT(VARCHAR,@RegistrationTypeReportGroup_ID) + ' AND GroupLeader_ID IS NULL),0) + 
				ISNULL((SELECT SUM(Qty) FROM #t3 WHERE (Venue_ID = #t1.Venue_ID OR #t1.Venue_ID IS NULL) AND RegistrationTypeReportGroup_ID = ' + CONVERT(VARCHAR,@RegistrationTypeReportGroup_ID) + '),0)'
	EXEC(@SQL)

	FETCH NEXT FROM RTGCursor INTO @RegistrationTypeReportGroup_ID
END

CLOSE RTGCursor
DEALLOCATE RTGCursor


SELECT * FROM #t1

drop table #t3, #t1, #t2
GO
