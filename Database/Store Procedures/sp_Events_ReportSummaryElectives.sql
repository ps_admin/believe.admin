SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_ReportSummaryElectives] 

@Conference_ID	INT,
@State_ID		INT

AS

DECLARE		@SQL								NVARCHAR(MAX),
			@RegistrationTypeReportGroup_ID		INT

SELECT		DISTINCT E.Elective_ID,
			E.[Name] as Elective
INTO		#t1
FROM		Events_Elective E,
			Events_RegistrationTypeElective RTE,
			Events_RegistrationType R
WHERE		RTE.Elective_ID = E.Elective_ID
AND			RTE.RegistrationType_ID = R.RegistrationType_ID
AND			R.Conference_ID = @Conference_ID


DECLARE RTGCursor CURSOR FOR
	SELECT RegistrationTypeReportGroup_ID FROM Events_RegistrationTypeReportGroup WHERE Conference_ID = @Conference_ID
OPEN RTGCursor

FETCH NEXT FROM RTGCursor INTO @RegistrationTypeReportGroup_ID
WHILE (@@FETCH_STATUS = 0) BEGIN

	SET @SQL = 'ALTER TABLE #t1	ADD [' + (SELECT Name FROM Events_RegistrationTypeReportGroup WHERE RegistrationTypeReportGroup_ID = @RegistrationTypeReportGroup_ID) + '] INT'
	EXEC(@SQL)

	SET @SQL = 'UPDATE #t1 SET [' + (SELECT Name FROM Events_RegistrationTypeReportGroup WHERE RegistrationTypeReportGroup_ID = @RegistrationTypeReportGroup_ID) + '] = ' +
			   'ISNULL((SELECT COUNT(*) FROM Events_Registration R, Events_RegistrationType RT WHERE R.RegistrationType_ID = RT.RegistrationType_ID AND RT.RegistrationTypeReportGroup_ID = ' + CONVERT(NVARCHAR, @RegistrationTypeReportGroup_ID) + ' AND Elective_ID = #t1.Elective_ID),0)'
	EXEC(@SQL)

	FETCH NEXT FROM RTGCursor INTO @RegistrationTypeReportGroup_ID
END

CLOSE RTGCursor
DEALLOCATE RTGCursor


SELECT * FROM #t1

DROP TABLE #t1
GO
