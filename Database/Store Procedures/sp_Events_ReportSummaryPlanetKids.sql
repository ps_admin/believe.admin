SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_ReportSummaryPlanetKids]

@Conference_ID	INT,
@State_ID		INT

AS

SELECT		PK.PKPersonalStatus_ID, PK.PersonalStatus as Status, PKSchoolGrade as SchoolGrade,
			COUNT(*) as Registrants,
			SUM(CASE WHEN Re.PKParentAttendingPlanetshakers = 1 THEN 1 ELSE 0 END) as Parent
INTO		#t1
FROM		PKPersonalStatus PK, 
			Registration Re,
			Registrant R,
			Venue V
WHERE		PK.PKPersonalStatus_ID = Re.PKPersonalStatus_ID
AND			R.Registrant_ID = Re.Registrant_ID
AND			Re.Venue_ID = V.Venue_ID
AND			R.Deleted = 0
AND			V.Conference_ID = @Conference_ID
AND			(V.State_ID = @State_ID OR @State_ID IS NULL)
GROUP BY	PK.PKPersonalStatus_ID, Re.PKPersonalStatus_ID, PK.PersonalStatus, PKSchoolGrade

INSERT		#t1 (PKPersonalStatus_ID, Status, SchoolGrade, Registrants, Parent)
SELECT		100, 'Child Total', null, SUM(Registrants), SUM(Parent) FROM #t1 WHERE PKPersonalStatus_ID = 3

INSERT		#t1 (PKPersonalStatus_ID, Status, SchoolGrade, Registrants, Parent)
SELECT		101, 'Total', null, SUM(Registrants), SUM(Parent) FROM #t1 WHERE PKPersonalStatus_ID IN (SELECT PKPersonalStatus_ID FROM PKPersonalStatus)

SELECT		* 
FROM		#t1
ORDER BY	PKPersonalStatus_ID
GO
