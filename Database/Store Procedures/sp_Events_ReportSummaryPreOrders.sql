SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_ReportSummaryPreOrders]

@Conference_ID	INT,
@State_ID		INT

AS

CREATE TABLE #t1
(
	Venue_ID	INT,
	Venue		VARCHAR(30)
)

INSERT INTO #t1 (Venue_ID, Venue)
SELECT Venue_ID, VenueName FROM Events_Venue V WHERE (State_ID = @State_ID or @State_ID IS NULL) AND V.Conference_ID = @Conference_ID

SELECT 		Re.Registration_ID,
			V.Venue_ID,
			CE.Deleted
INTO		#t2
FROM 		Common_ContactExternal CE,
			Events_Registration Re, 
			Events_Venue V
WHERE 		CE.Contact_ID = Re.Contact_ID
AND			Re.Venue_ID = V.Venue_ID 
AND 		V.Conference_ID = @Conference_ID


DECLARE @Product_ID INT
DECLARE @Product VARCHAR(50)

DECLARE ProductCursor Cursor FOR
	SELECT P.Product_ID, P.Title FROM Events_ConferencePreOrder CP, Events_Product P WHERE CP.Product_ID = P.Product_ID AND CP.Conference_ID = @Conference_ID

OPEN ProductCursor

WHILE (0=0) BEGIN

	FETCH NEXT FROM ProductCursor INTO @Product_ID, @Product

	IF (@@FETCH_STATUS <> 0) BREAK

	EXEC('ALTER TABLE #t1 ADD [' + @Product + '] INT DEFAULT 0')
	EXEC ('UPDATE #t1 SET [' + @Product + '] = (SELECT COUNT(*) FROM Events_PreOrder PO, #t2 WHERE PO.Registration_ID = #t2.Registration_ID AND #t2.Venue_ID = #t1.Venue_ID AND PO.Product_ID = ''' + @Product_ID + '''  AND #t2.Deleted = 0)')
END

CLOSE ProductCursor
DEALLOCATE ProductCursor

ALTER TABLE #t1 DROP COLUMN Venue_ID

SELECT * FROM #t1

drop table #t1, #t2
GO
