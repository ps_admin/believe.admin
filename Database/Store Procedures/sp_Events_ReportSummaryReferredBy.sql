SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_ReportSummaryReferredBy]

@Conference_ID	INT,
@State_ID		VARCHAR(50)

AS

	SELECT 		ROV.[Name] as ReferredBy, 
				COUNT(*) as Num
	FROM 		Common_ContactExternal CE, 
				Events_Registration Re,
				Events_Venue V,
				Events_RegistrationOption RO,
				Events_RegistrationOptionName RON, 
				Events_RegistrationOptionValue ROV
	WHERE		CE.Contact_ID = Re.Contact_ID
	AND			Re.Venue_Id = V.Venue_ID
	AND			Re.Registration_ID = RO.Registration_ID
	AND			RO.RegistrationOptionName_ID = RON.RegistrationOptionName_ID
	AND			RON.[Name] = 'REFERREDBY'
	AND			RO.RegistrationOptionValue_ID = ROV.RegistrationOptionValue_ID
	AND			(V.State_ID = @State_ID or @State_ID IS NULL)
	AND			V.Conference_ID = @Conference_ID
	AND 		CE.Deleted = 0
	GROUP BY	ROV.[Name]
GO
