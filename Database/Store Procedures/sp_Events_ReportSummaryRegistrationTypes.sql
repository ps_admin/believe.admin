SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_ReportSummaryRegistrationTypes]

@Conference_ID	INT,
@State_ID		INT

AS

CREATE TABLE #t1
(
	Venue_ID	int,
	Venue	varchar(30)
)

INSERT INTO #t1 (Venue_ID, Venue)
SELECT Venue_ID, VenueName FROM Events_Venue V WHERE (State_ID = @State_ID or @State_ID IS NULL) AND V.Conference_ID = @Conference_ID

INSERT INTO #t1 (Venue_ID, Venue) VALUES (0,'TOTAL')

DECLARE @RegistrationType_ID INT
DECLARE @RegistrationType VARCHAR(100)
DECLARE @SQL VARCHAR(1000)


SELECT		RT.RegistrationType_ID,
			RT.RegistrationType,
			V.Venue_ID,
			COUNT(*) as Total
INTO		#t2
FROM		Common_Contact C,
			Common_ContactExternal CE,
			Events_Registration Re,
			Events_RegistrationType RT,
			Events_Venue V
WHERE		C.Contact_ID = CE.Contact_ID
AND			CE.Contact_ID = Re.Contact_ID
AND			Re.Venue_ID = V.Venue_ID
AND			Re.RegistrationType_ID = RT.RegistrationType_ID
AND			RT.Conference_ID = @Conference_ID
AND			(V.State_ID = @State_ID  OR @State_ID is null)
AND			CE.Deleted = 0
AND			Re.GroupLeader_Id IS NULL
GROUP BY	RT.RegistrationType_ID,
			RT.RegistrationType,
			V.Venue_ID
UNION ALL
SELECT		RT.RegistrationType_ID,
			RT.RegistrationType,
			GB.Venue_ID,
			SUM(CASE WHEN TotalRegistrations > 0 THEN TotalRegistrations ELSE 0 END) as Total
FROM		vw_Events_GroupSummaryRegistrationView GB,
			Events_RegistrationType RT
WHERE		GB.RegistrationType_ID = RT.RegistrationType_ID
AND			RT.Conference_ID = @Conference_ID
GROUP BY 	RT.RegistrationType_ID,
			RT.RegistrationType,
			GB.Venue_ID

DECLARE RegistrationTypeCursor Cursor FOR
	SELECT RegistrationType_ID, RegistrationType FROM Events_RegistrationType WHERE Conference_ID = @Conference_ID

OPEN RegistrationTypeCursor

WHILE (0=0) BEGIN

	FETCH NEXT FROM RegistrationTypeCursor into @RegistrationType_ID, @RegistrationType

	IF (@@FETCH_STATUS <> 0) BREAK

	EXEC('ALTER TABLE #t1 ADD [' + @RegistrationType + '] INT DEFAULT 0')
	SET @SQL = 'UPDATE #t1 SET [' + @RegistrationType + '] = ISNULL((SELECT SUM(Total) FROM #t2 WHERE #t2.RegistrationType =  ''' + @RegistrationType + ''' AND #t2.Venue_ID = #t1.Venue_ID),0)'
	EXEC(@SQL)

	SET @SQL = 'UPDATE #t1 SET [' + @RegistrationType + '] = (SELECT SUM([' + @RegistrationType + ']) FROM #t1 WHERE Venue_ID <> 0) WHERE Venue_ID = 0'
	PRINT @SQL
	EXEC(@SQL)
END

SELECT * FROM #t1

CLOSE RegistrationTypeCursor
DEALLOCATE RegistrationTypeCursor
GO
