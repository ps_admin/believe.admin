SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_ReportSummaryState]

@Conference_ID	INT,
@State_ID		INT

AS

CREATE TABLE #t1
(
	State_ID	INT,
	State		VARCHAR(100),
	Num			INT,
	NumReg		INT DEFAULT 0
)

INSERT #t1 SELECT null, 'Australia', null, null

INSERT		#t1
SELECT 		S.State_ID,
			'  -- ' + S.State, 
			COUNT(*) as Qty, 
			NULL 
FROM 		Common_Contact C,
			Common_ContactExternal CE, 
			Common_GeneralState S 
WHERE 		C.Contact_ID = CE.Contact_ID
AND			C.State_ID = S.State_ID
AND 		CE.Deleted = 0
AND			S.State <> 'Other' 
GROUP BY 	S.State_ID,
			S.State 
ORDER BY	S.State

SELECT S.State_ID, '  -- ' + S.State as State, COUNT(*) as Qty 
INTO #t2 FROM Common_Contact C, Common_ContactExternal CE, Common_GeneralState S where C.Contact_ID = CE.Contact_ID AND C.State_ID = S.State_ID AND CE.Contact_ID in (SELECT DISTINCT CE.Contact_ID FROM Events_Registration Re, Common_ContactExternal CE, Events_Venue V WHERE CE.Contact_ID = Re.Contact_ID AND Re.Venue_Id = V.Venue_ID AND V.Conference_ID = @Conference_ID AND CE.Deleted = 0)
and S.State <> 'Other' GROUP BY S.State_ID, S.State

UPDATE #t1 SET NumReg = #t2.Qty FROM #t2 WHERE #t1.State = #t2.State

INSERT #t1 SELECT null, 'Australia Total', (SELECT COUNT(*) FROM Common_Contact C, Common_ContactExternal CE WHERE C.Contact_ID = CE.Contact_ID AND C.State_ID <> 9 and CE.Deleted = 0), (SELECT COUNT(*) FROM Common_Contact C, Common_ContactExternal CE WHERE C.Contact_ID = CE.Contact_ID AND C.State_ID <> 9 AND CE.Contact_ID IN (SELECT DISTINCT Contact_ID FROM Events_Registration Re, Events_Venue V WHERE Re.Venue_ID = V.Venue_Id AND V.Conference_ID = @Conference_ID) AND CE.Deleted = 0)
INSERT #t1 SELECT null, '', NULL, NULL

INSERT #t1 SELECT null,  'Overseas', (SELECT COUNT(*) FROM Common_Contact C, Common_ContactExternal CE WHERE C.Contact_ID = CE.Contact_ID AND C.State_ID = 9 and CE.Deleted = 0), (SELECT COUNT(*) FROM Common_Contact C, Common_ContactExternal CE WHERE C.Contact_ID = CE.Contact_ID AND C.State_ID = 9 AND CE.Contact_ID IN (SELECT DISTINCT Contact_ID FROM Events_Registration Re, Events_Venue V WHERE Re.Venue_ID = V.Venue_Id AND V.Conference_ID = @Conference_ID) AND CE.Deleted = 0)

DELETE FROM #t1 WHERE State_ID <> @State_ID AND @State_ID IS NOT NULL

SELECT * FROM #t1

DROP TABLE #t1
GO
