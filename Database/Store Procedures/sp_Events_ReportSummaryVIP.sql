SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_ReportSummaryVIP]

@Conference_ID	INT,
@State_ID		INT

AS

	DECLARE @RegistrationOptionName_ID INT
	DECLARE @VIP VARCHAR(50)
	DECLARE @SQL VARCHAR(5000)


	SELECT	V.Venue_ID, V.VenueName as [Venue]
	INTO	#t1
	FROM	Events_Venue V
	WHERE	V.Conference_ID = @Conference_ID
	AND		V.State_ID = COALESCE(@State_ID, V.State_ID)


	DECLARE VIPCursor CURSOR FOR
		SELECT		RON.RegistrationOptionName_ID,
					RON.Description
		FROM		Events_RegistrationOptionName RON
		WHERE		OptionType = 'VIP'
		AND			Conference_ID = @Conference_ID

	OPEN VIPCursor

	FETCH NEXT FROM VIPCursor into @RegistrationOptionName_ID, @VIP
	WHILE (@@FETCH_STATUS = 0) BEGIN

		EXEC('ALTER TABLE #t1 ADD [' + @VIP + '] INT DEFAULT 0')
		SET @SQL = 'UPDATE #t1 SET [' + @VIP + '] = ISNULL((SELECT SUM(CONVERT(INT, ROV.Value)) FROM Events_Registration Re, vw_Events_RegistrationOptionView ROV, Events_RegistrationOptionName RON  WHERE Re.Registration_ID = ROV.Registration_ID AND ROV.RegistrationOptionName_ID = RON.RegistrationOptionName_ID AND RON.RegistrationOptionName_ID = ' + CONVERT(NVARCHAR, @RegistrationOptionName_ID) + ' AND RON.Conference_ID = ' + CONVERT(NVARCHAR, @Conference_ID) + ' AND Re.Venue_ID = #t1.Venue_ID),0)'
		--SELECT @SQL
		EXEC(@SQL)

		FETCH NEXT FROM VIPCursor into @RegistrationOptionName_ID, @VIP
	END

CLOSE VIPCursor
DEALLOCATE VIPCursor

ALTER TABLE #t1 DROP COLUMN Venue_ID

SELECT * FROM #t1
GO
