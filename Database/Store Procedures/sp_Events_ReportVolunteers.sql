SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_ReportVolunteers]

@Venue_ID	int

AS

SELECT		[Name] = CC.FirstName + ' ' + CC.LastName,
			VolunteerDepartment
FROM		Common_Contact CC,
			Common_ContactExternal CE,
			Events_Registration Re,
			Events_VolunteerDepartment Vd
WHERE		CC.Contact_ID = CE.Contact_ID
AND			CE.Contact_ID = Re.Contact_ID
AND			Re.Venue_ID = @Venue_ID
AND			Re.VolunteerDepartment_ID = Vd.VolunteerDepartment_ID
AND			CE.Deleted = 0
ORDER BY	VolunteerDepartment, LastName, FirstName
GO
