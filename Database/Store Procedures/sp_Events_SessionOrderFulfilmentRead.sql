SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_SessionOrderFulfilmentRead]

@Venue_ID				INT

AS

SELECT		POSTransaction_ID, 
			TransactionDate
INTO		#SessionOrders
FROM		Events_POSTransaction PT
WHERE		PT.TransactionType = 2
AND			(PT.Venue_ID = @Venue_ID or (PT.Venue_ID IS NULL AND @Venue_ID IS NULL))

SELECT		P.Product_ID,
			P.Code,
			P.Title,
			SUM(CASE WHEN PTL.Quantity - PTL.QuantitySupplied - PTL.QuantityReturned < 0 THEN 0 ELSE PTL.Quantity - PTL.QuantitySupplied - PTL.QuantityReturned END) AS Quantity,
			SUM(CASE WHEN PTL.Quantity - PTL.QuantitySupplied - PTL.QuantityReturned < 0 THEN 0 ELSE PTL.Quantity - PTL.QuantitySupplied - PTL.QuantityReturned END) AS AvailableQuantity,
			COUNT(DISTINCT PT.POSTransaction_ID) as Orders,
			SUM(CASE WHEN PTL.Quantity - PTL.QuantitySupplied - PTL.QuantityReturned < 0 THEN 0 ELSE (PTL.Quantity - PTL.QuantitySupplied - PTL.QuantityReturned) * ROUND(PTL.Price * ((100-Discount)/100),2) END) AS [Value],
			CONVERT(BIT,1) as Fulfil
INTO		#SessionOrderList
FROM		Events_POSTransaction PT,
			Events_POSTransactionLine PTL,
			Events_Product P,
			#SessionOrders S
WHERE		PT.POSTransaction_ID = S.POSTransaction_ID
AND			PT.POSTransaction_ID = PTL.POSTransaction_ID
AND			PTL.Product_ID = P.Product_ID
GROUP BY	P.Product_ID,
			P.Code,
			P.Title

SELECT		*
FROM		#SessionOrderList
WHERE		Quantity > 0
ORDER BY	Title

SELECT		*
FROM		#SessionOrders
ORDER BY	TransactionDate
GO
