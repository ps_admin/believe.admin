SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_StockLevelInsert]

@Product_ID			INT,
@Venue_ID			INT,
@Quantity			INT,
@AdjustmentType		INT,
@DateAdded			DATETIME,
@Comments			NVARCHAR(500)

AS

INSERT Events_StockLevel
(
	Product_ID,
	Venue_ID,
	Quantity,
	AdjustmentType,
	DateAdded,
	Comments
)
VALUES
(
	@Product_ID,
	@Venue_ID,
	@Quantity,
	@AdjustmentType,
	@DateAdded,
	@Comments
)

SELECT SCOPE_IDENTITY() as StockLevel_ID
GO
