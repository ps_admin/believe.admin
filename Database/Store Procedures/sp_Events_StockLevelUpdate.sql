SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_StockLevelUpdate]

@StockLevel_ID		INT,
@Product_ID			INT,
@Venue_ID			INT,
@Quantity			INT,
@AdjustmentType		INT,
@DateAdded			DATETIME,
@Comments			NVARCHAR(500)

AS

UPDATE	Events_StockLevel
SET		Product_ID = @Product_ID,
		Venue_ID = @Venue_ID,
		Quantity = @Quantity,
		AdjustmentType = @AdjustmentType,
		DateAdded = @DateAdded,
		Comments = @Comments
WHERE	StockLevel_ID = @StockLevel_ID
GO
