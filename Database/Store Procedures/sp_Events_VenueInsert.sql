SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_VenueInsert]

@Conference_ID					INT,
@VenueName						VARCHAR(30),
@VenueLocation					VARCHAR(100),
@State_ID						INT,
@ConferenceStartDate			DATETIME,
@ConferenceEndDate				DATETIME,
@MaxRegistrants					INT,
@AllowMultipleRegistrations		BIT,
@IsClosed						BIT,
@AccommodationClosed			BIT,
@CateringClosed					BIT,
@PKEarlyChildhoodClosed			BIT,
@PKPrimaryClosed				BIT,
@LeadershipBreakfastClosed		BIT


AS

	INSERT INTO Events_Venue
	(
		Conference_ID,
		VenueName,
		VenueLocation,
		State_ID,
		ConferenceStartDate,
		ConferenceEndDate,
		MaxRegistrants,
		AllowMultipleRegistrations,
		IsClosed,
		AccommodationClosed,
		CateringClosed,
		PlanetkidsEarlyChildhoodClosed,
		PlanetkidsPrimaryClosed,
		LeadershipBreakfastClosed
	)
	VALUES
	(
		@Conference_ID,
		@VenueName,
		@VenueLocation,
		@State_ID,
		@ConferenceStartDate,
		@ConferenceEndDate,
		@MaxRegistrants,
		@AllowMultipleRegistrations,
		@IsClosed,
		@AccommodationClosed,
		@CateringClosed,
		@PKEarlyChildhoodClosed,
		@PKPrimaryClosed,
		@LeadershipBreakfastClosed
	)

SELECT SCOPE_IDENTITY() AS Venue_ID
GO
