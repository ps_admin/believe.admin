SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_VenueSelectAll]

AS

SELECT 		C.Conference_ID, 
			C.ConferenceName, 
			V.Venue_ID, 
			V.VenueName, 
			V.VenueLocation, 
			V.State_ID, 
			V.ConferenceStartDate, 
			V.ConferenceEndDate,
			V.MaxRegistrants, 
			V.AllowMultipleRegistrations,
			V.IsClosed,
			V.AccommodationClosed,
			V.CateringClosed,
			V.CrecheClosed,
			V.PlanetkidsEarlyChildhoodClosed,
			V.PlanetkidsPrimaryClosed,
			V.LeadershipBreakfastClosed,
			ISNULL(R.Registrants,0) as CurrentRegistrants
FROM 		Events_Conference C,
			Events_Venue V LEFT JOIN
			vw_Events_TotalRegistrants R ON
			V.Venue_ID = R.Venue_ID
WHERE		C.Conference_ID = V.Conference_ID
ORDER BY	C.ConferenceDate,
			V.ConferenceStartDate
GO
