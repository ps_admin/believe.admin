SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_VenueUpdate]

@Venue_ID							INT,
@Conference_ID						INT,
@VenueName							VARCHAR(30),
@VenueLocation						VARCHAR(100),
@State_ID							INT,
@ConferenceStartDate				DATETIME,
@ConferenceEndDate					DATETIME,
@MaxRegistrants						INT,
@AllowMultipleRegistrations			BIT,
@IsClosed							BIT,
@AccommodationClosed				BIT,
@CateringClosed						BIT,
@PlanetkidsEarlyChildhoodClosed		BIT,
@PlanetkidsPrimaryClosed			BIT,
@LeadershipBreakfastClosed			BIT

AS

UPDATE 	Events_Venue
SET		Conference_ID = @Conference_ID,
		VenueName = @VenueName,
		VenueLocation = @VenueLocation,
		State_ID = @State_ID,
		ConferenceStartDate = @ConferenceStartDate,
		ConferenceEndDate = @ConferenceEndDate,
		MaxRegistrants = @MaxRegistrants,
		AllowMultipleRegistrations = @AllowMultipleRegistrations,
		IsClosed = @IsClosed,
		AccommodationClosed = @AccommodationClosed,
		CateringClosed = @CateringClosed,
		PlanetkidsEarlyChildhoodClosed = @PlanetKidsEarlyChildhoodClosed,
		PlanetkidsPrimaryClosed = @PlanetKidsPrimaryClosed,
		LeadershipBreakfastClosed = @LeadershipBreakfastClosed
WHERE	Venue_ID = @Venue_ID
GO
