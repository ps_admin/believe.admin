SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_VolunteerInsert]
	@Conference_ID INT,
	@VolunteerDepartment_ID INT,
	@AccessLevel_ID INT = 0,
	@Contact_ID INT
AS
BEGIN

	INSERT INTO Events_Volunteer([Contact_ID], [ConferenceDepartmentMapping_ID])
	VALUES(
		@Contact_ID,
		(SELECT TOP 1 ConferenceDepartmentMapping_ID
		 FROM Events_ConferenceDepartmentMapping
		 WHERE [Conference_ID] = @Conference_ID AND
			   [VolunteerDepartment_ID] = @VolunteerDepartment_ID AND
			   (@AccessLevel_ID = 0 OR [AccessLevel_ID] = @AccessLevel_ID))
	)
	
END
GO
