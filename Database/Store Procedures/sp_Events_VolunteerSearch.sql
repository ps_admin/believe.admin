SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_VolunteerSearch]
	@Conference_ID INT ,
	@VolunteerDepartment_ID INT ,
	@AccessLevel_ID INT = 0
AS
BEGIN
	DECLARE @ConferenceDepartmentMapping_ID INT
	
	IF @AccessLevel_ID <> 0
		SELECT @ConferenceDepartmentMapping_ID = ConferenceDepartmentMapping_ID
		FROM [dbo].[Events_ConferenceDepartmentMapping]
		WHERE [Conference_ID] = @Conference_ID
			AND [VolunteerDepartment_ID] = @VolunteerDepartment_ID
			AND [AccessLevel_ID] = @AccessLevel_ID
	ELSE
		SET @ConferenceDepartmentMapping_ID = 
		(SELECT TOP 1 ConferenceDepartmentMapping_ID
		FROM [dbo].[Events_ConferenceDepartmentMapping]
		WHERE [Conference_ID] = @Conference_ID
			AND [VolunteerDepartment_ID] = @VolunteerDepartment_ID)
	
	SELECT c.[Contact_ID], c.[FirstName], c.[LastName], c.[Mobile], c.[Email], c.[VolunteerWWCCDate] AS WWCC, c.[VolunteerPoliceCheckDate] AS PoliceCheck, v.[Volunteer_FormReceived] AS VolunteerForm, a.[AccessLevel]
	INTO #t1
	FROM [dbo].[Events_Volunteer] v
		LEFT JOIN [dbo].[Common_Contact] c ON v.[Contact_ID] = c.[Contact_ID]
		LEFT JOIN [dbo].[Events_ConferenceDepartmentMapping] m ON v.[ConferenceDepartmentMapping_ID] = m.[ConferenceDepartmentMapping_ID]
		LEFT JOIN [dbo].[Events_AccessLevel] a ON m.[AccessLevel_ID] = a.[AccessLevel_ID]
	WHERE v.[ConferenceDepartmentMapping_ID] = @ConferenceDepartmentMapping_ID
	
	ALTER TABLE #t1 ADD VolunteerTag BIT
	
	UPDATE #t1
	SET [VolunteerTag] = 0
	WHERE (SELECT COUNT(*) FROM [dbo].[Events_VolunteerTag] t WHERE #t1.[Contact_ID] = t.[Contact_ID] AND t.[VolunteerTag_Active] = 1) < 1
	
	UPDATE #t1
	SET [VolunteerTag] = 1
	WHERE (SELECT COUNT(*) FROM [dbo].[Events_VolunteerTag] t WHERE #t1.[Contact_ID] = t.[Contact_ID] AND t.[VolunteerTag_Active] = 1) > 0
	
	SELECT *
	FROM #t1
	
	DROP TABLE #t1
END
GO
