SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Events_VolunteerSearchAvailable]

@QuickSearch				NVARCHAR(40) = '',
@Conference_ID				INT = 0

AS

SELECT c.[Contact_ID], c.[FirstName], c.[LastName]
FROM [dbo].[Common_Contact] c, [dbo].[Events_Registration] r
WHERE c.[Contact_ID] = r.[Contact_ID] AND
		r.Venue_ID IN (SELECT [Venue_ID]
					   FROM [dbo].[Events_Venue]
					   WHERE [Conference_ID] = @Conference_ID) AND
	  (c.[FirstName] LIKE ('%' + @QuickSearch + '%') OR
		c.[LastName] LIKE ('%' + @QuickSearch + '%') OR
		CONVERT(NVARCHAR, c.[Contact_ID]) LIKE ('%' + @QuickSearch + '%')) AND
		(r.VolunteerDepartment_ID IS NULL)
GO
