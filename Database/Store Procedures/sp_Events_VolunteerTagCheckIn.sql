SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_VolunteerTagCheckIn]
	@VolunteerTag_ID INT
AS
BEGIN
	UPDATE [Events_VolunteerTagLoan]
	SET [VolunteerTagLoan_CheckIn] = CURRENT_TIMESTAMP
	WHERE [VolunteerTag_ID] = @VolunteerTag_ID
END
GO
