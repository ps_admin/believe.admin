SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_VolunteerTagDeactivate] 
	@VolunteerTag_ID INT
AS
BEGIN
	UPDATE [Events_VolunteerTag]
	SET [VolunteerTag_Active] = 0,
		[VolunteerTag_DeactivationDate] = CURRENT_TIMESTAMP
	WHERE [VolunteerTag_ID] = @VolunteerTag_ID
END
GO
