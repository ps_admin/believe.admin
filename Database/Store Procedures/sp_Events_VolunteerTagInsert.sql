SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_VolunteerTagInsert]
	@VolunteerTag_ID VARCHAR(10),
	@Contact_ID INT
AS
BEGIN
	DECLARE @ActiveTagCount INT = 0
	
	SELECT @ActiveTagCount = COUNT(*)
	FROM [Events_VolunteerTag]
	WHERE [VolunteerTag_ID] = @VolunteerTag_ID
	AND [VolunteerTag_Active] = 1
	
	IF @ActiveTagCount > 0
		RAISERROR('A volunteer tag with the given ID is still active.', 16, 1)
		
	SELECT @ActiveTagCount = COUNT(*)
	FROM [Events_VolunteerTag]
	WHERE [VolunteerTag_ID] = @VolunteerTag_ID
	AND [Contact_ID] = @Contact_ID
	
	IF @ActiveTagCount > 0
		UPDATE [Events_VolunteerTag]
		SET [VolunteerTag_Active] = 1, [VolunteerTag_DeactivationDate] = NULL
		WHERE [VolunteerTag_ID] = @VolunteerTag_ID
	ELSE
		INSERT INTO [Events_VolunteerTag]([VolunteerTag_ID], [Contact_ID], [VolunteerTag_Active], [VolunteerTag_IssueDate], [VolunteerTag_DeactivationDate])
		VALUES(@VolunteerTag_ID, @Contact_ID, 1, CURRENT_TIMESTAMP, NULL)
END
GO
