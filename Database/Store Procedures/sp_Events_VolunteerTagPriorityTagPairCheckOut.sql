SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_VolunteerTagPriorityTagPairCheckOut]
	@Conference_ID INT,
	@VolunteerDepartment_ID INT,
	@AccessLevel_ID INT,
	@PriorityTag_ID VARCHAR(10),
	@VolunteerTag_ID INT,
	@Contact_ID INT
AS
BEGIN
	DECLARE @VolunteerTagLoanCount INT = 0
	DECLARE @PriorityTagLoanCount INT = 0
	DECLARE @ConferenceDepartmentMappingID INT = 0

	SELECT TOP 1 @ConferenceDepartmentMappingID = ConferenceDepartmentMapping_ID
	FROM [Events_ConferenceDepartmentMapping]
	WHERE [Conference_ID] = @Conference_ID
		AND [VolunteerDepartment_ID] = @VolunteerDepartment_ID
		AND [AccessLevel_ID] = @AccessLevel_ID
		
	SELECT @VolunteerTagLoanCount = COUNT(*)
	FROM [Events_VolunteerTagLoan]
	WHERE [VolunteerTag_ID] = @VolunteerTag_ID
		AND [ConferenceDepartmentMapping_ID] = @ConferenceDepartmentMappingID 
	
	SELECT @PriorityTagLoanCount = COUNT(*)
	FROM [Events_PriorityTagLoan] 
	WHERE [PriorityTag_ID] = @PriorityTag_ID
		AND [ConferenceDepartmentMapping_ID] = @ConferenceDepartmentMappingID 
		
	BEGIN TRANSACTION TagsPairCheckOut
	
	IF @VolunteerTagLoanCount = 0
		INSERT INTO [Events_VolunteerTagLoan]([VolunteerTag_ID], [ConferenceDepartmentMapping_ID], [Contact_ID], [VolunteerTagLoan_CheckOut], [VolunteerTagLoan_CheckIn])
		VALUES(@VolunteerTag_ID, @ConferenceDepartmentMappingID, @Contact_ID, CURRENT_TIMESTAMP, NULL)
	
	IF @PriorityTagLoanCount = 0
		INSERT INTO [Events_PriorityTagLoan]([PriorityTag_ID], [ConferenceDepartmentMapping_ID], [Contact_ID], [PriorityTagLoan_CheckOut], [PriorityTagLoan_CheckIn])
		VALUES(@PriorityTag_ID, @ConferenceDepartmentMappingID, @Contact_ID, CURRENT_TIMESTAMP, NULL)
	
	COMMIT TRANSACTION TagsPairCheckOut
END
GO
