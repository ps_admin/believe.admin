SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_Events_VolunteerTagRead]
	@ContactID INT
AS
BEGIN
	SELECT [VolunteerTag_ID], [Contact_ID], [VolunteerTag_Active], [VolunteerTag_IssueDate], [VolunteerTag_DeactivationDate]
	FROM [Events_VolunteerTag]
	WHERE [Contact_ID] = @ContactID
	ORDER BY VolunteerTag_IssueDate
END
GO
