SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_VolunteerTagReadActive]
	@ContactID INT
AS
BEGIN
	SELECT [VolunteerTag_ID], [Contact_ID], [VolunteerTag_Active], [VolunteerTag_IssueDate], [VolunteerTag_DeactivationDate]
	FROM [Events_VolunteerTag]
	WHERE [Contact_ID] = @ContactID
		AND [VolunteerTag_Active] = 1
	ORDER BY VolunteerTag_IssueDate
END
GO
