SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_VolunteersRead]
	@Conference_ID INT ,
	@VolunteerDepartment_ID INT = 0
AS
BEGIN
	DECLARE @SQL VARCHAR(8000)
	
	SELECT c.[Contact_ID], c.[FirstName], c.[LastName], c.[Mobile], c.[Email], c.[VolunteerWWCCDate] AS WWCC, c.[VolunteerPoliceCheckDate] AS PoliceCheck, v.[Volunteer_FormReceived] AS VolunteerForm, a.[AccessLevel]
	INTO #t1
	FROM [dbo].[Events_Volunteer] v
		LEFT JOIN [dbo].[Common_Contact] c ON v.[Contact_ID] = c.[Contact_ID]
		LEFT JOIN [dbo].[Events_ConferenceDepartmentMapping] m ON v.[ConferenceDepartmentMapping_ID] = m.[ConferenceDepartmentMapping_ID]
		LEFT JOIN [dbo].[Events_AccessLevel] a ON m.[AccessLevel_ID] = a.[AccessLevel_ID]
	WHERE 1 = 0
	
	SET @SQL =
	'INSERT INTO #t1
	SELECT c.[Contact_ID], c.[FirstName], c.[LastName], c.[Mobile], c.[Email], c.[VolunteerWWCCDate] AS WWCC, c.[VolunteerPoliceCheckDate] AS PoliceCheck, v.[Volunteer_FormReceived] AS VolunteerForm, a.[AccessLevel]
	FROM [dbo].[Events_Volunteer] v
		LEFT JOIN [dbo].[Common_Contact] c ON v.[Contact_ID] = c.[Contact_ID]
		LEFT JOIN [dbo].[Events_ConferenceDepartmentMapping] m ON v.[ConferenceDepartmentMapping_ID] = m.[ConferenceDepartmentMapping_ID]
		LEFT JOIN [dbo].[Events_AccessLevel] a ON m.[AccessLevel_ID] = a.[AccessLevel_ID]
	WHERE m.[Conference_ID] = ' + CONVERT(VARCHAR, @Conference_ID)
	
	IF @VolunteerDepartment_ID <> 0
		SET @SQL = @SQL + ' AND m.[VolunteerDepartment_ID] = ' + CONVERT(VARCHAR, @VolunteerDepartment_ID)
	
	EXEC(@SQL)
	
	-- Add a column to indicate whether a volunteer has been issued with any volunteer tag
	ALTER TABLE #t1 ADD VolunteerTag BIT
	
	UPDATE #t1
	SET [VolunteerTag] = 0
	WHERE (SELECT COUNT(*) FROM [dbo].[Events_VolunteerTag] t WHERE #t1.[Contact_ID] = t.[Contact_ID] AND t.[VolunteerTag_Active] = 1) < 1
	
	UPDATE #t1
	SET [VolunteerTag] = 1
	WHERE (SELECT COUNT(*) FROM [dbo].[Events_VolunteerTag] t WHERE #t1.[Contact_ID] = t.[Contact_ID] AND t.[VolunteerTag_Active] = 1) > 0
	
	-- Add a column to indicate whether a volunteer is registered for conference
	ALTER TABLE #t1 ADD Registered BIT
	
	UPDATE #t1
	SET [Registered] = 0
	WHERE (SELECT COUNT(*) FROM [dbo].[Events_Registration] r WHERE r.[Venue_ID] IN (SELECT [Venue_ID] FROM [dbo].[Events_Venue] v WHERE v.[Conference_ID] = @Conference_ID) AND r.[Contact_ID] = #t1.[Contact_ID]) < 1
	
	UPDATE #t1
	SET [Registered] = 1
	WHERE (SELECT COUNT(*) FROM [dbo].[Events_Registration] r WHERE r.[Venue_ID] IN (SELECT [Venue_ID] FROM [dbo].[Events_Venue] v WHERE v.[Conference_ID] = @Conference_ID) AND r.[Contact_ID] = #t1.[Contact_ID]) > 0
	
	SELECT *
	FROM #t1
	
	DROP TABLE #t1
END
GO
