SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_WaitingListInsert]
(
	@Contact_ID int,
	@Venue_ID int,
	@DateAdded smalldatetime,
	@User_ID INT = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin
		
	SET NOCOUNT ON

	INSERT INTO [Events_WaitingList]
	(
		[Contact_ID],
		[Venue_ID],
		[DateAdded]
	)
	VALUES
	(
		@Contact_ID,
		@Venue_ID,
		@DateAdded
	)

	SELECT WaitingList_ID = SCOPE_IDENTITY();
GO
