SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Events_WaitingListUpdate]
(
	@WaitingList_ID int,
	@Contact_ID int,
	@Venue_ID int,
	@DateAdded smalldatetime,
	@User_ID INT = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin
		
	SET NOCOUNT ON
	
	UPDATE [Events_WaitingList]
	SET
		[Contact_ID] = @Contact_ID,
		[Venue_ID] = @Venue_ID,
		[DateAdded] = @DateAdded
	WHERE 
		[WaitingList_ID] = @WaitingList_ID
GO
