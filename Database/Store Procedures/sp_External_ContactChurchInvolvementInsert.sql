SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_External_ContactChurchInvolvementInsert]
(
	@Contact_ID int,
	@ChurchInvolvement_ID int,
	@DateAdded datetime,
	@User_ID int
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin

	SET NOCOUNT ON

	INSERT INTO [External_ContactChurchInvolvement]
	(
		[Contact_ID],
		[ChurchInvolvement_ID],
		[DateAdded]
	)
	VALUES
	(
		@Contact_ID,
		@ChurchInvolvement_ID,
		@DateAdded
	)

	SELECT ContactChurchInvolvement_ID = SCOPE_IDENTITY();
GO
