SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_External_ContactChurchInvolvementUpdate]
(
	@ContactChurchInvolvement_ID int,
	@Contact_ID int,
	@ChurchInvolvement_ID int,
	@DateAdded datetime,
	@User_ID int
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin

	SET NOCOUNT ON
	
	UPDATE [External_ContactChurchInvolvement]
	SET
		[Contact_ID] = @Contact_ID,
		[ChurchInvolvement_ID] = @ChurchInvolvement_ID,
		[DateAdded] = @DateAdded
	WHERE 
		[ContactChurchInvolvement_ID] = @ContactChurchInvolvement_ID
GO
