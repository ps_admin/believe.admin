SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_PCR_AddContactComments]
(
	@PCRContact_ID INT,
	@Activity NVARCHAR(40),
	@Contact_ID INT,
	@CommentBy_ID INT,	
	@Comment NTEXT,
	@CommentType INT = 1, -- Administrative
	@CommentSource INT = 2 -- PCR	
)
AS
BEGIN
	SET NOCOUNT ON

	INSERT INTO [Church_ContactComment]
	(
		[Contact_ID],
		[CommentBy_ID],
		[CommentDate],
		[Comment],
		[CommentType_ID],
		[CommentSource_ID]
	)
	VALUES
	(
		@Contact_ID,
		@CommentBy_ID,
		GETDATE(),
		@Comment,
		@CommentType, 
		@CommentSource	
	)
	
	DECLARE 
		@Call INT,
		@Visit INT,
		@Others INT
		
	SET @Call = 0
	SET @Visit = 0
	SET @Others = 0
	
	IF (@Activity = 'Call')
		SET @Call = 1
	ELSE IF (@Activity = 'Visit')
		SET @Visit = 1
	ELSE
		SET @Others = 1
	
	UPDATE 
		[Church_PCRContact]
	SET
		[Call] = [Call] + @Call,
		[Visit] = Visit + @Visit,
		[Other] = Other + @Others
	WHERE 
		[PCRContact_ID] = @PCRContact_ID	
END


--select top 10 * from [Church_ContactComment] order by Comment_ID desc
--select * from Church_PCRContact where PCRContact_ID = 775096
GO
