SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_PCR_GetContactDetails]		
	@ContactID INT
AS
BEGIN		
	
	-- Get the latest active PCR
	DECLARE @PCRDate_ID INT
	
	SELECT 
		@PCRDate_ID = PCRDate_ID
	FROM 
		Church_PCRDate 	WITH(NOLOCK)	
	WHERE 
		DATEDIFF(d, [Date], GETDATE()) >= 0 
		AND DATEDIFF(d, [Date], GETDATE()) < 7


	-- Get Summary of the contact in past 31 days
	SELECT	
		C.Contact_ID,
		SUM(CONVERT(INT,SundayAttendance)) as MonthSundayTotal,
		SUM(CONVERT(INT,ULGAttendance)) as MonthULGTotal,
		SUM(CONVERT(INT,BoomAttendance)) as MonthBoomTotal,
		SUM(Call) as MonthCallTotal,
		SUM(Visit) as MonthVisitTotal		
	INTO 
		#Summary
	FROM	
		Church_PCRContact C WITH(NOLOCK)	
	INNER JOIN
		Church_PCR PCR WITH(NOLOCK) ON PCR.PCR_ID = C.PCR_ID 
		AND PCR.PCRDate_ID 
			IN (
				SELECT	
					PCRDate_ID
				FROM 
					Church_PCRDate
				WHERE 
					DATEDIFF(d, [Date], GETDATE()) <= 31
					AND Deleted = 0
					AND GETDATE() > [Date]
				)
	WHERE
		C.Contact_ID = @ContactID
	GROUP BY 
		C.Contact_ID	

	-- Now, returns all details
	SELECT	
			CC.Contact_ID AS ID,
			C.PCRContact_ID AS PCRContactID,
			CC.FirstName as FirstName,
			CC.LastName as LastName,
			Email,
			Mobile,
			Address1,
			Address2, 
			Suburb,
			Postcode,							
			CS.Name AS Status,
			ISNULL(S.MonthSundayTotal,0) as MonthSundayTotal,
			ISNULL(S.MonthULGTotal,0) as MonthULGTotal,
			ISNULL(S.MonthBoomTotal,0) as MonthBoomTotal,
			ISNULL(S.MonthCallTotal,0) as MonthCallTotal,
			ISNULL(S.MonthVisitTotal,0) as MonthVisitTotal,			
			CC.[GUID] as ContactGUID
	FROM 
		Church_PCR PCR WITH(NOLOCK)
	INNER JOIN 
		Church_PCRContact C  WITH(NOLOCK) ON C.PCR_ID = PCR.PCR_ID
	INNER JOIN 
		Common_Contact CC WITH(NOLOCK) ON CC.Contact_ID = C.Contact_ID
	INNER JOIN 
		Common_ContactInternal CI WITH(NOLOCK) ON CI.Contact_ID = CC.Contact_ID 			
	INNER JOIN 
		Church_ChurchStatus CS WITH(NOLOCK) ON CS.ChurchStatus_ID = CI.ChurchStatus_ID	
	INNER JOIN 
		#Summary S ON C.Contact_ID = S.Contact_ID
	WHERE 
		PCR.PCRDate_ID = @PCRDate_ID
	ORDER BY 
		CC.FirstName, CC.LastName

	DROP TABLE #Summary	

END

-- exec sp_PCR_GetContactDetails @ContactID = 117307
--, @PCRDate_ID = 229
GO
