SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_PCR_GetUrbanLifeContacts]	
	@ULG_ID INT,
	@PCRDate_ID INT = NULL
AS	
BEGIN
	
	-- If PCRDate is not given, assume current PCR week is requested	
	IF @PCRDate_ID IS NULL
	BEGIN
		SELECT 
			@PCRDate_ID = PCRDate_ID
		FROM 
			Church_PCRDate 	WITH(NOLOCK)	
		WHERE 
			DATEDIFF(d, [Date], GETDATE()) >= 0 
			AND DATEDIFF(d, [Date], GETDATE()) < 7	
	END	
	
	-- If PCR has not yet been setup, create one	
	DECLARE @CurrentPCR INT
	SELECT @CurrentPCR = COUNT(*) FROM Church_PCR WHERE PCRDate_ID = @PCRDate_ID AND ULG_ID = @ULG_ID
	IF (@CurrentPCR = 0) 
	BEGIN
		INSERT	
			Church_PCR 
			(PCRDate_ID, ULG_ID, ULGChildren, ULGVisitors, ULGOffering, ReportCompleted)
		VALUES
			(@PCRDate_ID, @ULG_ID, 0, 0, 0, 0)
			
		PRINT 'Insert new entry to table Church_PCR'
			
		-- Also Import PCRContact to be added into the new created PCR
		INSERT	
			Church_PCRContact 
			(PCR_ID, Contact_ID, SundayAttendance, ULGAttendance, BoomAttendance, Call, Visit)
		SELECT DISTINCT 
			PCR_ID, Contact_ID, 0, 0, 0, 0, 0
		FROM	
			Church_PCR P WITH(NOLOCK)
		INNER JOIN 			
			Church_PCRDate PD WITH(NOLOCK) ON PD.PCRDate_ID = P.PCRDate_ID
				AND PD.PCRDate_ID = @PCRDate_ID
				AND PD.Deleted = 0
		INNER JOIN			
			Church_ULGContact U WITH(NOLOCK) ON U.ULG_ID = P.ULG_ID
				AND	U.ULG_ID = @ULG_ID
				AND	U.Inactive = 0
		WHERE NOT EXISTS (
			SELECT PC.Contact_ID
			FROM Church_PCRContact PC WITH(NOLOCK)
			WHERE PC.PCR_ID = P.PCR_ID
				AND	PC.Contact_ID = U.Contact_ID
			)			
			
		PRINT 'Importing contact data into Church_PCRContact table'
	END
			
	-- Now output the results	
	SELECT	
		DISTINCT
		CC.Contact_ID AS ID,
		FirstName,
		LastName,
		Email,
		Mobile,
		Address1,
		Address2, 
		Suburb,
		Postcode,
		CS.Name AS Status
	FROM	
		Common_Contact CC WITH(NOLOCK)
	INNER JOIN
		Church_PCRContact C WITH(NOLOCK) ON C.Contact_ID = CC.Contact_ID
	INNER JOIN
		Church_PCR PCR WITH(NOLOCK) ON PCR.PCR_ID = C.PCR_ID 
			AND PCR.PCRDate_ID = @PCRDate_ID
	INNER JOIN 
		Common_ContactInternal CI WITH(NOLOCK) ON CI.Contact_ID = CC.Contact_ID 
			AND	CI.ChurchStatus_ID NOT IN (6, 7)		
	INNER JOIN 
		Church_ChurchStatus CS WITH(NOLOCK) ON CS.ChurchStatus_ID = CI.ChurchStatus_ID		
	ORDER BY
		FirstName, LastName
END

-- sp_PCR_GetUrbanLifeContacts @ULG_ID = 43
GO
