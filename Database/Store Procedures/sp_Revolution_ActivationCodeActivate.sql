SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Revolution_ActivationCodeActivate]

@SerialNumberStart	INT,
@SerialNumberEnd	INT,
@ActivatedBy_ID		INT

AS


UPDATE	Revolution_ActivationCode
SET		CardActivated = 1,
		ActivationDate = dbo.GetRelativeDate(),
		ActivatedBy_ID = @ActivatedBy_ID
WHERE	SerialNumber >= @SerialNumberStart
AND		SerialNumber <= @SerialNumberEnd
AND		CardActivated = 0
GO
