SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Revolution_ActivationCodeGenerate]

@NumOfCodes			INT,
@Subscription_ID	INT

AS

SET NOCOUNT ON

DECLARE @Code				NVARCHAR(15)
DECLARE @CodeLength			INT
DECLARE @CodeCount			INT
DECLARE @CodeLengthCount	INT
DECLARE @Rand				INT
DECLARE @SerialNumber		INT

SET @CodeLength = 10
SET @CodeCount = 0
SET @SerialNumber = ISNULL((SELECT MAX(SerialNumber) FROM Revolution_ActivationCode),0) 

WHILE @CodeCount < @NumOfCodes BEGIN

	SET @CodeLengthCount = 0
	SET @Code = ''

	WHILE LEN(@Code) < @CodeLength BEGIN
		
		SET @Rand = round((rand(cast(newid() as binary(4)) ^ cast(substring(cast(newid() as binary(4)), 7,4) as int)) * 46),0)

		IF @Rand >=2 AND @Rand <= 9 -- No number 1
			SET @Code = @Code + RIGHT(CONVERT(NVARCHAR, @Rand),1)
		IF @Rand >=11 AND @Rand <= 18 -- No number 1
			SET @Code = @Code + RIGHT(CONVERT(NVARCHAR, @Rand - 9),1)
		IF @Rand >=19 AND @Rand <= 44 AND @Rand <> 33 and @Rand <> 27 -- no letter I or O
			IF LEN(@Code) <= 1 
				SET @Code = @Code + CHAR(46 + @Rand)
			ELSE IF RIGHT(@Code, 2) NOT LIKE '[A-Z][A-Z]' --Maximum of 2 letters together, to prevent swear words.
				SET @Code = @Code + CHAR(46 + @Rand)

		IF LEN(@Code) >=2	-- No duplicate characters
			IF RIGHT(@Code,1) = LEFT(RIGHT(@Code,2),1)
				SET @Code = LEFT(@Code, LEN(@Code) - 1)

	END
		
	SET @CodeCount = @CodeCount + 1

	INSERT Revolution_ActivationCode (SerialNumber,ActivationCode, CardActivated, SubscriptionActivated, Subscription_ID)	
	VALUES (@SerialNumber + @CodeCount, @Code, 0, 0, @Subscription_ID)

END

SELECT SerialNumber, ActivationCode FROM Revolution_ActivationCode
WHERE SerialNumber > @SerialNumber
GO
