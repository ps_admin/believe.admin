SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Revolution_ActivationCodeReadByActivationCode]

@ActivationCode		NVARCHAR(15)

AS

SELECT	* 
FROM	Revolution_ActivationCode
WHERE	ActivationCode = @ActivationCode
GO
