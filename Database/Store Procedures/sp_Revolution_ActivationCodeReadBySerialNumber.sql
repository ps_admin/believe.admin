SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Revolution_ActivationCodeReadBySerialNumber]

@SerialNumber		INT

AS

SELECT	* 
FROM	Revolution_ActivationCode
WHERE	SerialNumber = @SerialNumber
GO
