SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Revolution_ActivationCodeUpdate]
(
	@ActivationCode_ID int,
	@SerialNumber int,
	@ActivationCode nvarchar(15),
	@CardActivated bit,
	@SubscriptionActivated bit,
	@Subscription_ID int,
	@Contact_ID int = NULL,
	@ActivationDate datetime = NULL,
	@ActivatedBy_ID int
)
AS
	SET NOCOUNT ON
	
	UPDATE [Revolution_ActivationCode]
	SET
		[SerialNumber] = @SerialNumber,
		[ActivationCode] = @ActivationCode,
		[CardActivated] = @CardActivated,
		[SubscriptionActivated] = @SubscriptionActivated,
		[Subscription_ID] = @Subscription_ID,
		[Contact_ID] = @Contact_ID,
		[ActivationDate] = @ActivationDate,
		[ActivatedBy_ID] = @ActivatedBy_ID
	WHERE 
		[ActivationCode_ID] = @ActivationCode_ID
GO
