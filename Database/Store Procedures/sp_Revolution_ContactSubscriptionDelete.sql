SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Revolution_ContactSubscriptionDelete]
(
	@ContactSubscription_ID int,
	@User_ID INT = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin
		
	SET NOCOUNT ON

	DELETE 
	FROM   [Revolution_ContactSubscription]
	WHERE  
		[ContactSubscription_ID] = @ContactSubscription_ID
GO
