SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Revolution_ContactSubscriptionInsert]
(
	@Contact_ID int,
	@Subscription_ID int,
	@SubscriptionDiscount decimal(18,2),
	@SignupDate datetime,
	@SubscriptionStartDate datetime,
	@SubscriptionCancelled bit,
	@EndOfTrialEmailSent bit = 0,
	@SubscribedBy_ID int,
	@User_ID INT = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin
		
	SET NOCOUNT ON

	INSERT INTO [Revolution_ContactSubscription]
	(
		[Contact_ID],
		[Subscription_ID],
		[SubscriptionDiscount],
		[SignupDate],
		[SubscriptionStartDate],
		[SubscriptionCancelled],
		[EndOfTrialEmailSent],
		[SubscribedBy_ID]
	)
	VALUES
	(
		@Contact_ID,
		@Subscription_ID,
		@SubscriptionDiscount,
		@SignupDate,
		@SubscriptionStartDate,
		@SubscriptionCancelled,
		@EndOfTrialEmailSent,
		@SubscribedBy_ID
	)

	SELECT ContactSubscription_ID = SCOPE_IDENTITY();
GO
