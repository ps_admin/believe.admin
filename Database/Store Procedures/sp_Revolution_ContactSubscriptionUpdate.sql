SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Revolution_ContactSubscriptionUpdate]
(
	@ContactSubscription_ID int,
	@Contact_ID int,
	@Subscription_ID int,
	@SubscriptionDiscount decimal(18,2),
	@SignupDate datetime,
	@SubscriptionStartDate datetime,
	@SubscriptionCancelled bit,
	@EndOfTrialEmailSent bit = 0,
	@SubscribedBy_ID int,
	@User_ID INT = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin
		
	SET NOCOUNT ON
	
	UPDATE [Revolution_ContactSubscription]
	SET
		[Contact_ID] = @Contact_ID,
		[Subscription_ID] = @Subscription_ID,
		[SubscriptionDiscount] = @SubscriptionDiscount,
		[SignupDate] = @SignupDate,
		[SubscriptionStartDate] = @SubscriptionStartDate,
		[SubscriptionCancelled] = @SubscriptionCancelled,
		[EndOfTrialEmailSent] = @EndOfTrialEmailSent,
		[SubscribedBy_ID] = @SubscribedBy_ID
	WHERE 
		[ContactSubscription_ID] = @ContactSubscription_ID
GO
