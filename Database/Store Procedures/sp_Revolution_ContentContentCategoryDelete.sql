SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Revolution_ContentContentCategoryDelete]
(
	@ContentContentCategory_ID int
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Revolution_ContentContentCategory]
	WHERE  [ContentContentCategory_ID] = @ContentContentCategory_ID
GO
