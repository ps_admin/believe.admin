SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Revolution_ContentContentCategoryInsert]
(
	@Content_ID int,
	@ContentSubCategory_ID int,
	@SortOrder int
)
AS
	SET NOCOUNT ON

	INSERT INTO [Revolution_ContentContentCategory]
	(
		[Content_ID],
		[ContentSubCategory_ID],
		[SortOrder]
	)
	VALUES
	(
		@Content_ID,
		@ContentSubCategory_ID,
		@SortOrder
	)

	SELECT ContentContentCategory_ID = SCOPE_IDENTITY();
GO
