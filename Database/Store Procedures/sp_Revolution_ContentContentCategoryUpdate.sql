SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Revolution_ContentContentCategoryUpdate]
(
	@ContentContentCategory_ID int,
	@Content_ID int,
	@ContentSubCategory_ID int,
	@SortOrder int
)
AS
	SET NOCOUNT ON
	
	UPDATE [Revolution_ContentContentCategory]
	SET
		[Content_ID] = @Content_ID,
		[ContentSubCategory_ID] = @ContentSubCategory_ID,
		[SortOrder] = @SortOrder
	WHERE 
		[ContentContentCategory_ID] = @ContentContentCategory_ID
GO
