SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Revolution_ContentDelete]
(
	@Content_ID int
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Revolution_Content]
	WHERE  
		[Content_ID] = @Content_ID
GO
