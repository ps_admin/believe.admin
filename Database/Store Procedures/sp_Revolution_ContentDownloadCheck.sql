SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Revolution_ContentDownloadCheck]

@Contact_ID			INT,
@ContentFile_ID		INT

AS

SELECT	COUNT(*) 
FROM	Revolution_ContentDownload
WHERE	Contact_ID = @Contact_ID
AND		ContentFile_ID = @ContentFile_ID
AND		MediaPlayerDownload = 0
AND		ResetDownload = 0
GO
