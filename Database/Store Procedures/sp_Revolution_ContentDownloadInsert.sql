SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Revolution_ContentDownloadInsert]
(
	@Contact_ID int,
	@ContentFile_ID int,
	@ClientIP nvarchar(15),
	@MediaPlayerDownload bit
)
AS
	SET NOCOUNT ON

	INSERT INTO [Revolution_ContentDownload]
	(
		[Contact_ID],
		[ContentFile_ID],
		[ClientIP],
		[DownloadDate],
		[MediaPlayerDownload],
		[ResetDownload]
	)
	VALUES
	(
		@Contact_ID,
		@ContentFile_ID,
		@ClientIP,
		dbo.GetRelativeDate(),
		@MediaPlayerDownload,
		0
	)

	SELECT ContentDownload_ID = SCOPE_IDENTITY();
GO
