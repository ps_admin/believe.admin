SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Revolution_ContentDownloadLimitReadByContact] 

@Contact_ID		INT

AS

DECLARE		@MaxDownloads INT
SELECT		@MaxDownloads = PremiumDownloadLimit FROM Revolution_Options

SELECT		CP.Name as [ContentPack],
			CF.FileName as [Content],
			CF.ContentFile_ID,
			SUM(CASE WHEN CD.ResetDownload=0 THEN 1 ELSE 0 END) as [DownloadsSinceReset],
			COUNT(*) as [TotalDownloads],
			MAX(CD.DownloadDate) as LastDownloadDate
FROM		Revolution_ContentPack CP,
			Revolution_Content C,
			Revolution_ContentFile CF,
			Revolution_ContentFileType CFT,
			Revolution_ContentDownload CD
WHERE		CP.ContentPack_ID = C.ContentPack_ID
AND			C.Content_ID = CF.Content_ID
AND			CF.ContentFile_ID = CD.ContentFile_ID
AND			CF.ContentFileType_ID = CFT.ContentFileType_ID
AND			C.RestrictedDownload = 1
AND			CFT.RestrictedDownload = 1
AND			CD.Contact_ID = @Contact_ID
AND			CD.MediaPlayerDownload = 0
GROUP BY	CP.Name,
			CF.FileName,
			CF.ContentFile_ID
HAVING		SUM(CASE WHEN CD.ResetDownload=0 THEN 1 ELSE 0 END) >= @MaxDownloads
ORDER BY	LastDownloadDate DESC
GO
