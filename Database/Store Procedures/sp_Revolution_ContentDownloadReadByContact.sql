SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Revolution_ContentDownloadReadByContact]

@Contact_ID		INT

AS

SELECT		CP.Name as [ContentPack],
			C.Name as [Content],
			CF.FileName as [File],
			C.RestrictedDownload, 
			CD.DownloadDate
FROM		Revolution_ContentPack CP,
			Revolution_Content C,
			Revolution_ContentFile CF,
			Revolution_ContentDownload CD
WHERE		CP.ContentPack_ID = C.ContentPack_ID
AND			C.Content_ID = CF.Content_ID
AND			CF.ContentFile_ID = CD.ContentFile_ID
AND			CD.Contact_ID = @Contact_ID
AND			CD.MediaPlayerDownload = 0
ORDER BY	DownloadDate DESC
GO
