SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Revolution_ContentFileDelete]
(
	@ContentFile_ID int
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Revolution_ContentFile]
	WHERE  
		[ContentFile_ID] = @ContentFile_ID
GO
