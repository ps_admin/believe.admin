SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Revolution_ContentFileInsert]
(
	@Content_ID int,
	@FileName nvarchar(100),
	@AllowDownload bit,
	@ExternalDownloadLocation nvarchar(80),
	@ContentFileType_ID int,
	@ContentFileCategory_ID int,
	@Visible bit,
	@SortOrder int
)
AS
	SET NOCOUNT ON

	INSERT INTO [Revolution_ContentFile]
	(
		[Content_ID],
		[FileName],
		[AllowDownload],
		[ExternalDownloadLocation],
		[ContentFileType_ID],
		[ContentFileCategory_ID],
		[Visible],
		[SortOrder]
	)
	VALUES
	(
		@Content_ID,
		@FileName,
		@AllowDownload,
		@ExternalDownloadLocation,
		@ContentFileType_ID,
		@ContentFileCategory_ID,
		@Visible,
		@SortOrder
	)

	SELECT ContentFile_ID = SCOPE_IDENTITY();
GO
