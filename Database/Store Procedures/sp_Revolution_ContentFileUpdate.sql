SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Revolution_ContentFileUpdate]
(
	@ContentFile_ID int,
	@Content_ID int,
	@FileName nvarchar(100),
	@AllowDownload bit,
	@ExternalDownloadLocation nvarchar(80),
	@ContentFileType_ID int,
	@ContentFileCategory_ID int,
	@Visible bit,
	@SortOrder int
)
AS
	SET NOCOUNT ON
	
	UPDATE [Revolution_ContentFile]
	SET
		[Content_ID] = @Content_ID,
		[FileName] = @FileName,
		[AllowDownload] = @AllowDownload,
		[ExternalDownloadLocation] = @ExternalDownloadLocation,
		[ContentFileType_ID] = @ContentFileType_ID,
		[ContentFileCategory_ID] = @ContentFileCategory_ID,
		[Visible] = @Visible,
		[SortOrder] = @SortOrder
	WHERE 
		[ContentFile_ID] = @ContentFile_ID
GO
