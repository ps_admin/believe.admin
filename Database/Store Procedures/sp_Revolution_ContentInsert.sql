SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Revolution_ContentInsert]
(
	@ContentPack_ID int,
	@Name nvarchar(50),
	@AuthenticationRequired	bit,
	@RestrictedDownload bit,
	@MediaPlayerBackgroundImage image = null,
	@ThumbnailImage image = null,
	@Description nvarchar(200),
	@MoreDetailsLink nvarchar(50),
	@Visible bit,
	@SortOrder int
)
AS
	SET NOCOUNT ON

	INSERT INTO [Revolution_Content]
	(
		[ContentPack_ID],
		[Name],
		[AuthenticationRequired],
		[RestrictedDownload],
		[MediaPlayerBackgroundImage],
		[ThumbnailImage],
		[Description],
		[MoreDetailsLink],
		[Visible],
		[SortOrder]
	)
	VALUES
	(
		@ContentPack_ID,
		@Name,
		@AuthenticationRequired,
		@RestrictedDownload,
		@MediaPlayerBackgroundImage,
		@ThumbnailImage,
		@Description,
		@MoreDetailsLink,
		@Visible,
		@SortOrder
	)

	SELECT Content_ID = SCOPE_IDENTITY();
GO
