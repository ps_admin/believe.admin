SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Revolution_ContentPackDelete]
(
	@ContentPack_ID int
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Revolution_ContentPack]
	WHERE  
		[ContentPack_ID] = @ContentPack_ID
GO
