SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Revolution_ContentPackInsert]
(
	@Name nvarchar(50),
	@StartDate datetime,
	@EndDate datetime = NULL,
	@BonusContentPack bit = 0
)
AS
	SET NOCOUNT ON

	INSERT INTO [Revolution_ContentPack]
	(
		[Name],
		[StartDate],
		[EndDate],
		[BonusContentPack]
	)
	VALUES
	(
		@Name,
		@StartDate,
		@EndDate,
		@BonusContentPack
	)

	SELECT ContentPack_ID = SCOPE_IDENTITY();
GO
