SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Revolution_ContentPackUpdate]
(
	@ContentPack_ID int,
	@Name nvarchar(50),
	@StartDate datetime,
	@EndDate datetime = NULL,
	@BonusContentPack bit = 0
)
AS
	SET NOCOUNT ON
	
	UPDATE [Revolution_ContentPack]
	SET
		[Name] = @Name,
		[StartDate] = @StartDate,
		[EndDate] = @EndDate,
		[BonusContentPack] = @BonusContentPack
	WHERE 
		[ContentPack_ID] = @ContentPack_ID
GO
