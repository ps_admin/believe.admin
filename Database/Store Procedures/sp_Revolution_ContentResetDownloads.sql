SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Revolution_ContentResetDownloads]

@Contact_ID			INT,
@ContentFile_ID		INT

AS

UPDATE	Revolution_ContentDownload
SET		ResetDownload = 1 
FROM	Revolution_ContentDownload CD,
		Revolution_ContentFile CF
WHERE	CD.ContentFile_ID = CF.ContentFile_ID
AND		CD.Contact_ID = @Contact_ID
AND		CF.ContentFile_ID = @ContentFile_ID
GO
