SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Revolution_ContentSelectAll]

AS

--Don't select the Image fields directly! They're too big to return, especially
--when developing remotely.
SELECT		Content_ID,
			ContentPack_ID,
			[Name],
			AuthenticationRequired,
			RestrictedDownload,
			--MediaPlayerBackgroundImage,
			--ThumbnailImage,
			Description,
			MoreDetailsLink,
			Visible,
			SortOrder,
			GUID
FROM		Revolution_Content

SELECT		*
FROM		Revolution_ContentContentCategory
ORDER BY	Content_ID, SortOrder
GO
