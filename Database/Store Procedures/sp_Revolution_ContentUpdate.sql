SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Revolution_ContentUpdate]
(
	@Content_ID int,
	@ContentPack_ID int,
	@Name nvarchar(50),
	@AuthenticationRequired	bit,
	@RestrictedDownload bit,
	@MediaPlayerBackgroundImage image = null,
	@ThumbnailImage image = null,
	@Description nvarchar(200),
	@MoreDetailsLink nvarchar(50),
	@Visible bit,
	@SortOrder int
)
AS
	SET NOCOUNT ON
	
	UPDATE [Revolution_Content]
	SET
		[ContentPack_ID] = @ContentPack_ID,
		[Name] = @Name,
		[AuthenticationRequired] = @AuthenticationRequired,
		[RestrictedDownload] = @RestrictedDownload,
		[Description] = @Description,
		[MoreDetailsLink] = @MoreDetailsLink,
		[Visible] = @Visible,
		[SortOrder] = @SortOrder
	WHERE 
		[Content_ID] = @Content_ID
	

	UPDATE [Revolution_Content]
	SET
		[MediaPlayerBackgroundImage] = @MediaPlayerBackgroundImage
	WHERE 
		[Content_ID] = @Content_ID
	AND	@MediaPlayerBackgroundImage IS NOT NULL

	
	UPDATE [Revolution_Content]
	SET
		[ThumbnailImage] = @ThumbnailImage
	WHERE 
		[Content_ID] = @Content_ID
	AND	@ThumbnailImage IS NOT NULL
GO
