SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Revolution_NightlyEndOfTrialEmail]

AS

SELECT	Contact_ID, 
		ContactSubscription_ID
FROM	Revolution_ContactSubscription
WHERE	EndOfTrialEmailSent = 0
AND		SubscriptionCancelled = 0
AND		Subscription_ID in (3)
AND		DATEADD(dd, 24, SignupDate) < dbo.GetRelativeDate()
AND		Contact_ID NOT IN (SELECT Contact_ID FROM Revolution_ContactSubscription WHERE Subscription_ID IN (1,2))
GO
