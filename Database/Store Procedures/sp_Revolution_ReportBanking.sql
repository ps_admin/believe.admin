SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Revolution_ReportBanking]

@StartDate					DATETIME,
@EndDate					DATETIME

AS
DECLARE @SQL		VARCHAR(7000)


/*********************************************
**										    **
**		1. Retreive Payments			    **
**										    **
**********************************************/
SET @SQL = '
	SELECT		PT.PaymentType, 
				PT.PaymentType_ID,
				SP.PaymentAmount, 
				SP.CCNumber, 
				SP.PaymentDate, 
				SP.PaymentBy_ID,
				CC.Country_ID
	INTO		#Payment
	FROM		Revolution_SubscriptionPayment SP,
				Revolution_ContactSubscription CS,
				Common_Contact CC,
				Common_PaymentType PT
	WHERE		SP.ContactSubscription_ID = CS.ContactSubscription_ID
	AND			CS.Contact_ID = CC.Contact_ID
	AND			CS.SubscriptionCancelled = 0
	AND			SP.PaymentType_ID = PT.PaymentType_ID
	AND			CS.SignupDate >= ''' + CONVERT(VARCHAR,@StartDate) + '''
	AND			CS.SignupDate < DateAdd(dd,1,''' + CONVERT(VARCHAR,@EndDate) + ''')

	SELECT		CASE WHEN PaymentType = ''Credit Card'' THEN
						CASE	WHEN left(CCNumber,3) like ''30[0-5]'' or left(ccnumber,2) = ''36'' THEN ''Diners''
								WHEN left(CCNumber,2) = ''34'' or left(ccnumber,2) = ''37'' then ''American Express''
								WHEN left(CCNumber,2) like ''5[1-5]'' then ''Mastercard''
								WHEN left(CCNumber,1) = ''4'' then ''Visa''
								WHEN left(CCNumber,4) = ''5610'' then ''Bankcard''
								ELSE ''Unknown Credit Card Type'' END
						Else PaymentType END AS PaymentType,
				SUM(CASE WHEN Country_ID = 11 THEN PaymentAmount ELSE 0 END) as PaymentAmountAus,
				SUM(CASE WHEN Country_ID <> 11 THEN PaymentAmount ELSE 0 END) as PaymentAmountOS,
				COUNT(*) AS Subscriptions
	FROM		#Payment
	GROUP BY	CASE WHEN PaymentType = ''Credit Card'' THEN
						CASE	WHEN left(CCNumber,3) like ''30[0-5]'' or left(ccnumber,2) = ''36'' THEN ''Diners''
								WHEN left(CCNumber,2) = ''34'' or left(ccnumber,2) = ''37'' then ''American Express''
								WHEN left(CCNumber,2) like ''5[1-5]'' then ''Mastercard''
								WHEN left(CCNumber,1) = ''4'' then ''Visa''
								WHEN left(CCNumber,4) = ''5610'' then ''Bankcard''
								ELSE ''Unknown Credit Card Type'' END
						Else PaymentType END,
				PaymentType_ID
	ORDER BY	CASE WHEN PaymentType_ID = 3 then 0 ELSE PaymentType_ID END'

EXEC(@SQL)


/*********************************************
**			 							    **
**  2. Retreive Total Subscription Amounts  **
**										    **
**********************************************/


SET @SQL = '
SELECT		S.Name,
			Round(S.Cost * ((100 - CS.SubscriptionDiscount) / 100), 2) as Cost,
			Subscriptions = Count(*),
			TotalAmount = SUM(Round(S.Cost * ((100 - CS.SubscriptionDiscount) / 100), 2))
FROM		Revolution_Subscription S,
			Revolution_ContactSubscription CS
WHERE		S.Subscription_ID = CS.Subscription_ID
AND			[SignupDate] >= ''' + CONVERT(VARCHAR,@StartDate) + '''
AND			[SignupDate] < DateAdd(dd,1,''' + CONVERT(VARCHAR,@EndDate) + ''')
AND			CS.SubscriptionCancelled = 0
GROUP BY	S.Name, 
			Round(S.Cost * ((100 - CS.SubscriptionDiscount) / 100), 2)'

EXEC(@SQL)
GO
