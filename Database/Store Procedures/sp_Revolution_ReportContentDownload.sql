SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Revolution_ReportContentDownload]

@StartDate						DATETIME,
@EndDate						DATETIME,
@ContentCategory_ID				INT,
@ContentFileType_ID				INT,
@ShowRestrictedContent			INT = -1,
@ShowMediaPlayerDownloads		INT = -1

AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = '
	SELECT		CC.[Name] as ContentCategory,
				CFC.[Name] as ContentFileCategory,
				CFT.[Name] as ContentFileType,
				C.[Name] as ContentName,
				COUNT(*) as Downloads
	FROM		Revolution_Content C LEFT JOIN
				vw_Revolution_ContentCategory RCC ON C.Content_ID = RCC.Content_ID JOIN
				Revolution_ContentCategory CC ON RCC.ContentCategory_ID = CC.ContentCategory_ID,
				Revolution_ContentFile CF,
				Revolution_ContentDownload CD,
				Revolution_ContentFileType CFT,
				Revolution_ContentFileCategory CFC
	WHERE		C.Content_ID = CF.Content_ID
	AND			CF.ContentFile_ID = CD.ContentFile_ID
	AND			CF.ContentFileType_ID = CFT.ContentFileType_ID
	AND			CF.ContentFileCategory_ID = CFC.ContentFileCategory_ID
	AND			CD.DownloadDate >= ''' + CONVERT(NVARCHAR, @StartDate) + '''
	AND			CD.DownloadDate < ''' + CONVERT(NVARCHAR, DATEADD(DD,1,@EndDate)) + '''' + 
CASE WHEN @ContentCategory_ID IS NOT NULL THEN '
	AND			C.Content_ID IN (SELECT Content_ID FROM Revolution_ContentSubCategory CSC, Revolution_ContentContentCategory CCC WHERE CSC.ContentSubCategory_ID = CCC.ContentSubCategory_ID AND CSC.ContentCategory_ID = ' + CONVERT(NVARCHAR, @ContentCategory_ID) + ')' ELSE '' END +
CASE WHEN @ContentFileType_ID IS NOT NULL THEN '
	AND			CFT.ContentFileType_ID = ' + CONVERT(NVARCHAR, @ContentFileType_ID) ELSE '' END + 
CASE WHEN @ShowRestrictedContent >= 0 THEN '
	AND			C.RestrictedDownload = ' + CONVERT(NVARCHAR, @ShowRestrictedContent) ELSE '' END +  
CASE WHEN @ShowMediaPlayerDownloads >= 0 THEN '
	AND			CD.MediaPlayerDownload = ' + CONVERT(NVARCHAR, @ShowMediaPlayerDownloads) ELSE '' END + '
	GROUP BY	CC.Name,
				CFC.Name,
				CFT.Name,
				C.Name
	ORDER BY	COUNT(*) DESC'

EXEC(@SQL)
GO
