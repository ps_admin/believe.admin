SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Revolution_ReportSubscription]

@ReportType						INT,
@StartMonth						INT,
@StartYear						INT,
@Months							INT,
@Subscriptions					NVARCHAR(100)

AS

DECLARE @SQL			NVARCHAR(MAX)
DECLARE @StartDate		DATETIME
DECLARE @Counter		INT

IF @ReportType = 1 BEGIN

	CREATE TABLE #t1 (
		Country NVARCHAR(100),
		[State]	NVARCHAR(100),
		Subscriptions INT
	)
	
	SET @SQL = '
		INSERT		#t1
		SELECT		GC.Country, 
					CASE WHEN GS.State <> ''Other'' THEN GS.State ELSE '''' END,
					COUNT(*)
		FROM		Common_Contact CC,
					Common_GeneralState GS,
					Common_GeneralCountry GC,
					vw_Revolution_CurrentSubscriptions CS
		WHERE		CC.State_ID = GS.State_ID
		AND			CC.Country_ID = GC.Country_ID
		AND			CC.Contact_ID = CS.Contact_ID
		AND			CS.Subscription_ID IN (' + @Subscriptions + ')
		GROUP BY	GC.Country, 
					GS.State
		ORDER BY	GC.Country, 
					GS.State

		SELECT	1, * FROM #t1
		UNION ALL	
		SELECT		2, ''Total'',
					'''', 
					Count(*)
		FROM		Common_Contact CC,
					vw_Revolution_CurrentSubscriptions CS
		WHERE		CC.Contact_ID = CS.Contact_ID
		AND			CS.Subscription_ID IN (' + @Subscriptions + ')
		ORDER BY	1, Country'
		
	EXEC(@SQL)

	DROP TABLE	#t1

END

IF @ReportType = 2 BEGIN

	CREATE TABLE #t2 (
		Country_ID int,
		State_ID int
	)
	
	SET @SQL =  '
		INSERT		#t2
		SELECT		DISTINCT CC.Country_ID,
					CC.State_ID
		FROM		Common_Contact CC,
					Revolution_ContactSubscription CS
		WHERE		CC.Contact_ID = CS.Contact_ID
		AND			CS.SubscriptionCancelled = 0
		AND			CS.Subscription_ID IN (' + @Subscriptions + ')
		UNION ALL	
		SELECT		0, 0'
		
	EXEC(@SQL)

	SET @Counter = 1

	WHILE @Counter <= @Months BEGIN
		SET @SQL = 'ALTER TABLE #t2 ADD Column' + CONVERT(NVARCHAR,@counter) + ' INT'
		EXEC (@SQL)

		SET @StartDate = DATEADD(mm, @Counter-1, CONVERT(DATETIME, CONVERT(NVARCHAR, @StartYear) + '-' + CONVERT(NVARCHAR, @StartMonth) + '-1'))
		SET @SQL = 'UPDATE #t2 SET Column' + CONVERT(NVARCHAR,@counter) + ' = 
					(	SELECT	COUNT(*) 
						FROM	Revolution_ContactSubscription CS,
								Common_Contact CC
						WHERE	CS.Contact_ID = CC.Contact_ID
						AND		CS.SignupDate >=  ''' + CONVERT(NVARCHAR, @StartDate) + '''
						AND		CS.SignupDate < ''' + CONVERT(NVARCHAR, DATEADD(mm, 1, @StartDate)) + '''
						AND		CS.SubscriptionCancelled = 0
						AND		CS.Subscription_ID IN (' + @Subscriptions + ')
						AND		(CC.Country_ID = #t2.Country_ID OR #t2.Country_ID = 0)
						AND		(CC.State_ID = #t2.State_ID OR #t2.State_ID = 0))'

		EXEC (@SQL)

		SET @Counter = @Counter + 1
	END

	SET @SQL = 'ALTER TABLE #t2 ADD Total INT'
	EXEC (@SQL)
	SET @StartDate = CONVERT(DATETIME, CONVERT(NVARCHAR, @StartYear) + '-' + CONVERT(NVARCHAR, @StartMonth) + '-1')
		SET @SQL = 'UPDATE #t2 SET Total = 
					(	SELECT	COUNT(*) 
						FROM	Revolution_ContactSubscription CS,
								Common_Contact CC
						WHERE	CS.Contact_ID = CC.Contact_ID
						AND		CS.SignupDate >=  ''' + CONVERT(NVARCHAR, @StartDate) + '''
						AND		CS.SignupDate < ''' + CONVERT(NVARCHAR, DATEADD(mm, @Months, @StartDate)) + '''
						AND		CS.SubscriptionCancelled = 0
						AND		CS.Subscription_ID IN (' + @Subscriptions + ')
						AND		(CC.Country_ID = #t2.Country_ID OR #t2.Country_ID = 0)
						AND		(CC.State_ID = #t2.State_ID OR #t2.State_ID = 0))'

	EXEC (@SQL)

	SELECT		1, GC.Country, 
				CASE WHEN GS.State <> 'Other' THEN GS.State ELSE '' END AS State,
				#t2.*
	FROM		#t2,
				Common_GeneralState GS,
				Common_GeneralCountry GC
	WHERE		#t2.State_ID = GS.State_ID
	AND			#t2.Country_ID = GC.Country_ID
	UNION ALL	
	SELECT		2, 'Total',
				'', 
				#t2.*
	FROM		#t2
	WHERE		Country_ID = 0 AND State_ID = 0
	ORDER BY	1, Country

	DROP TABLE #t2

END


IF @ReportType = 3 BEGIN

	CREATE TABLE #t3 (
		Country_ID int,
		State_ID int
	)
	
	SET @SQL =  '
		INSERT		#t3
		SELECT		DISTINCT CC.Country_ID,
					CC.State_ID
		FROM		Common_Contact CC,
					Revolution_ContactSubscription CS
		WHERE		CC.Contact_ID = CS.Contact_ID
		AND			CS.SubscriptionCancelled = 0
		AND			CS.Subscription_ID IN (' + @Subscriptions + ')
		UNION ALL	
		SELECT		0, 0'
		
	EXEC(@SQL)

	SET @Counter = 1
	
	WHILE @Counter <= @Months BEGIN
		SET @SQL = 'ALTER TABLE #t3 ADD Column' + CONVERT(NVARCHAR,@counter) + ' INT'
		EXEC (@SQL)

		SET @StartDate = DATEADD(mm, @Counter-1, CONVERT(DATETIME, CONVERT(NVARCHAR, @StartYear) + '-' + CONVERT(NVARCHAR, @StartMonth) + '-1'))
		SET @SQL = 'UPDATE #t3 SET Column' + CONVERT(NVARCHAR,@counter) + ' = 
					(	SELECT	COUNT(*) 
						FROM	Revolution_ContactSubscription CS,
								Revolution_Subscription S,
								Common_Contact CC
						WHERE	CS.Contact_ID = CC.Contact_ID
						AND		CS.Subscription_ID = S.Subscription_ID
						AND		DATEADD(mm, S.Length-1, CS.SubscriptionStartDate) >=  ''' + CONVERT(NVARCHAR, @StartDate) + '''
						AND		CS.SubscriptionStartDate <= ''' + CONVERT(NVARCHAR, @StartDate) + '''
						AND		CS.SubscriptionCancelled = 0
						AND		CS.Subscription_ID IN (' + @Subscriptions + ')
						AND		(CC.Country_ID = #t3.Country_ID OR #t3.Country_ID = 0)
						AND		(CC.State_ID = #t3.State_ID OR #t3.State_ID = 0))'

		EXEC (@SQL)

		SET @Counter = @Counter + 1
	END

	SET @SQL = 'ALTER TABLE #t3 ADD Total INT'
	EXEC (@SQL)
	SET @StartDate = CONVERT(DATETIME, CONVERT(NVARCHAR, @StartYear) + '-' + CONVERT(NVARCHAR, @StartMonth) + '-1')
	SET @SQL = 'UPDATE #t3 SET Total = 
					(	SELECT	COUNT(*) 
						FROM	Revolution_ContactSubscription CS,
								Revolution_Subscription S,
								Common_Contact CC
						WHERE	CS.Contact_ID = CC.Contact_ID
						AND		CS.Subscription_ID = S.Subscription_ID
						AND		DATEADD(mm, S.Length-1, CS.SubscriptionStartDate) >=  ''' + CONVERT(NVARCHAR, @StartDate) + '''
						AND		CS.SubscriptionStartDate <= ''' + CONVERT(NVARCHAR, DATEADD(mm, @Months, @StartDate)) + '''
						AND		CS.SubscriptionCancelled = 0
						AND		CS.Subscription_ID IN (' + @Subscriptions + ')
						AND		(CC.Country_ID = #t3.Country_ID OR #t3.Country_ID = 0)
						AND		(CC.State_ID = #t3.State_ID OR #t3.State_ID = 0))'

	EXEC (@SQL)

	SELECT		1, GC.Country, 
				CASE WHEN GS.State <> 'Other' THEN GS.State ELSE '' END AS State,
				#t3.*
	FROM		#t3,
				Common_GeneralState GS,
				Common_GeneralCountry GC
	WHERE		#t3.State_ID = GS.State_ID
	AND			#t3.Country_ID = GC.Country_ID
	UNION ALL	
	SELECT		2, 'Total',
				'', 
				#t3.*
	FROM		#t3
	WHERE		Country_ID = 0 AND State_ID = 0
	ORDER BY	1, Country

	DROP TABLE #t3

END
GO
