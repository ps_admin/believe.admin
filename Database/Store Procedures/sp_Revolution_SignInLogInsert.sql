SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Revolution_SignInLogInsert]
(
	@ClientIP nvarchar(15),
	@Email nvarchar(100),
	@Password nvarchar(100),
	@Success bit,
	@Contact_ID int = NULL,
	@CurrentMember bit
)
AS
	SET NOCOUNT ON

	INSERT INTO [Revolution_SignInLog]
	(
		[Date],
		[ClientIP],
		[Email],
		[Password],
		[Success],
		[Contact_ID],
		[CurrentMember]
	)
	VALUES
	(
		dbo.GetRelativeDate(),
		@ClientIP,
		@Email,
		@Password,
		@Success,
		@Contact_ID,
		@CurrentMember
	)

	SELECT SigninLog_ID = SCOPE_IDENTITY();
GO
