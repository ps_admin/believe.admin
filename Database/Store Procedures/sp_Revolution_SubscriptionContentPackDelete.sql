SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Revolution_SubscriptionContentPackDelete]
(
	@Subscription_ID int,
	@ContentPack_ID int
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM	[Revolution_SubscriptionContentPack]
	WHERE 	[Subscription_ID] = @Subscription_ID
	AND		[ContentPack_ID] = @ContentPack_ID
GO
