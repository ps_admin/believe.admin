SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Revolution_SubscriptionContentPackInsert]
(
	@Subscription_ID int,
	@ContentPack_ID int
)
AS
	SET NOCOUNT ON

	INSERT INTO [Revolution_SubscriptionContentPack]
	(
		[Subscription_ID],
		[ContentPack_ID]
	)
	VALUES
	(
		@Subscription_ID,
		@ContentPack_ID
	)
GO
