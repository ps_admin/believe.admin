SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Revolution_SubscriptionDelete]
(
	@Subscription_ID int
)
AS
	SET NOCOUNT ON

	DELETE 
	FROM   [Revolution_Subscription]
	WHERE  
		[Subscription_ID] = @Subscription_ID
GO
