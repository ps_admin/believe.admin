SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Revolution_SubscriptionInsert]
(
	@Name nvarchar(50),
	@Cost money,
	@Length int,
	@BankAccount_ID int
)
AS
	SET NOCOUNT ON

	INSERT INTO [Revolution_Subscription]
	(
		[Name],
		[Cost],
		[Length],
		[BankAccount_ID]
	)
	VALUES
	(
		@Name,
		@Cost,
		@Length,
		@BankAccount_ID
	)

	SELECT Subscription_ID = SCOPE_IDENTITY();
GO
