SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Revolution_SubscriptionPaymentInsert]
(
	@ContactSubscription_ID int,
	@PaymentType_ID int,
	@PaymentAmount money,
	@CCNumber nvarchar(20),
	@CCExpiry nvarchar(20),
	@CCName nvarchar(20),
	@CCPhone nvarchar(20),
	@CCManual bit,
	@CCTransactionRef nvarchar(20),
	@CCRefund bit,
	@ChequeDrawer nvarchar(50),
	@ChequeBank nvarchar(50),
	@ChequeBranch nvarchar(30),
	@Comment nvarchar(200),
	@PaymentDate datetime,
	@PaymentBy_ID int,
	@BankAccount_ID int,
	@User_ID INT = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin
		
	SET NOCOUNT ON

	INSERT INTO [Revolution_SubscriptionPayment]
	(
		[ContactSubscription_ID],
		[PaymentType_ID],
		[PaymentAmount],
		[CCNumber],
		[CCExpiry],
		[CCName],
		[CCPhone],
		[CCManual],
		[CCTransactionRef],
		[CCRefund],
		[ChequeDrawer],
		[ChequeBank],
		[ChequeBranch],
		[Comment],
		[PaymentDate],
		[PaymentBy_ID],
		[BankAccount_ID]
	)
	VALUES
	(
		@ContactSubscription_ID,
		@PaymentType_ID,
		@PaymentAmount,
		@CCNumber,
		@CCExpiry,
		@CCName,
		@CCPhone,
		@CCManual,
		@CCTransactionRef,
		@CCRefund,
		@ChequeDrawer,
		@ChequeBank,
		@ChequeBranch,
		@Comment,
		@PaymentDate,
		@PaymentBy_ID,
		@BankAccount_ID
	)

	SELECT SubscriptionPayment_ID = SCOPE_IDENTITY();

	RETURN @@Error
GO
