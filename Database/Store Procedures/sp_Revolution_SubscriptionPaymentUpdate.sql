SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Revolution_SubscriptionPaymentUpdate]
(
	@SubscriptionPayment_ID int,
	@ContactSubscription_ID int,
	@PaymentType_ID int,
	@PaymentAmount money,
	@CCNumber nvarchar(20),
	@CCExpiry nvarchar(20),
	@CCName nvarchar(20),
	@CCPhone nvarchar(20),
	@CCManual bit,
	@CCTransactionRef nvarchar(20),
	@CCRefund bit,
	@ChequeDrawer nvarchar(50),
	@ChequeBank nvarchar(50),
	@ChequeBranch nvarchar(30),
	@Comment nvarchar(200),
	@PaymentDate datetime,
	@PaymentBy_ID int,
	@BankAccount_ID int,
	@User_ID INT = null
)
AS

	DECLARE @bin varbinary(128); SELECT @bin = convert(varbinary(128), @User_ID); SET CONTEXT_INFO @bin
		
	SET NOCOUNT ON
	
	UPDATE [Revolution_SubscriptionPayment]
	SET
		[ContactSubscription_ID] = @ContactSubscription_ID,
		[PaymentType_ID] = @PaymentType_ID,
		[PaymentAmount] = @PaymentAmount,
		[CCNumber] = @CCNumber,
		[CCExpiry] = @CCExpiry,
		[CCName] = @CCName,
		[CCPhone] = @CCPhone,
		[CCManual] = @CCManual,
		[CCTransactionRef] = @CCTransactionRef,
		[CCRefund] = @CCRefund,
		[ChequeDrawer] = @ChequeDrawer,
		[ChequeBank] = @ChequeBank,
		[ChequeBranch] = @ChequeBranch,
		[Comment] = @Comment,
		[PaymentDate] = @PaymentDate,
		[PaymentBy_ID] = @PaymentBy_ID,
		[BankAccount_ID] = @BankAccount_ID
	WHERE 
		[SubscriptionPayment_ID] = @SubscriptionPayment_ID

	RETURN @@Error
GO
