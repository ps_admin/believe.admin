SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_Revolution_SubscriptionUpdate]
(
	@Subscription_ID int,
	@Name nvarchar(50),
	@Cost money,
	@Length int,
	@BankAccount_ID int
)
AS
	SET NOCOUNT ON
	
	UPDATE [Revolution_Subscription]
	SET
		[Name] = @Name,
		[Cost] = @Cost,
		[Length] = @Length,
		[BankAccount_ID] = @BankAccount_ID
	WHERE 
		[Subscription_ID] = @Subscription_ID
GO
