SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Church_247Prayer](
	[247Prayer_ID] [int] IDENTITY(1,1) NOT NULL,
	[Contact_ID] [int] NOT NULL,
	[Day_ID] [int] NOT NULL,
	[Time_ID] [int] NOT NULL,
	[Duration_ID] [int] NOT NULL,
	[ContactMethod_ID] [int] NOT NULL,
	[DateAdded] [datetime] NOT NULL,
 CONSTRAINT [PK_Church_247Prayer] PRIMARY KEY CLUSTERED 
(
	[247Prayer_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Church_247Prayer]  WITH CHECK ADD  CONSTRAINT [FK_Church_247Prayer_Church_247PrayerContactMethod] FOREIGN KEY([ContactMethod_ID])
REFERENCES [dbo].[Church_247PrayerContactMethod] ([ContactMethod_ID])
GO
ALTER TABLE [dbo].[Church_247Prayer] CHECK CONSTRAINT [FK_Church_247Prayer_Church_247PrayerContactMethod]
GO
ALTER TABLE [dbo].[Church_247Prayer]  WITH CHECK ADD  CONSTRAINT [FK_Church_247Prayer_Church_247PrayerDay] FOREIGN KEY([Day_ID])
REFERENCES [dbo].[Church_247PrayerDay] ([Day_ID])
GO
ALTER TABLE [dbo].[Church_247Prayer] CHECK CONSTRAINT [FK_Church_247Prayer_Church_247PrayerDay]
GO
ALTER TABLE [dbo].[Church_247Prayer]  WITH CHECK ADD  CONSTRAINT [FK_Church_247Prayer_Church_247PrayerDuration] FOREIGN KEY([Duration_ID])
REFERENCES [dbo].[Church_247PrayerDuration] ([Duration_ID])
GO
ALTER TABLE [dbo].[Church_247Prayer] CHECK CONSTRAINT [FK_Church_247Prayer_Church_247PrayerDuration]
GO
ALTER TABLE [dbo].[Church_247Prayer]  WITH CHECK ADD  CONSTRAINT [FK_Church_247Prayer_Church_247PrayerTime] FOREIGN KEY([Time_ID])
REFERENCES [dbo].[Church_247PrayerTime] ([Time_ID])
GO
ALTER TABLE [dbo].[Church_247Prayer] CHECK CONSTRAINT [FK_Church_247Prayer_Church_247PrayerTime]
GO
ALTER TABLE [dbo].[Church_247Prayer]  WITH CHECK ADD  CONSTRAINT [FK_Church_247Prayer_Common_ContactInternal] FOREIGN KEY([Contact_ID])
REFERENCES [dbo].[Common_ContactInternal] ([Contact_ID])
GO
ALTER TABLE [dbo].[Church_247Prayer] CHECK CONSTRAINT [FK_Church_247Prayer_Common_ContactInternal]
GO
