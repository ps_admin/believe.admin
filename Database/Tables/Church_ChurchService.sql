SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Church_ChurchService](
	[ChurchService_ID] [int] IDENTITY(1,1) NOT NULL,
	[Campus_ID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[DisplayInPCRReport] [bit] NOT NULL,
	[DisplayInKidsMinistryModule] [bit] NOT NULL,
	[SortOrder] [int] NOT NULL,
 CONSTRAINT [PK_Church_ChurchService] PRIMARY KEY CLUSTERED 
(
	[ChurchService_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Church_ChurchService] ADD  CONSTRAINT [DF_Church_ChurchService_Campus_ID]  DEFAULT ((1)) FOR [Campus_ID]
GO
