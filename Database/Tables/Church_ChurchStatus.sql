SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Church_ChurchStatus](
	[ChurchStatus_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[RequestReasonWhenChangingTo] [bit] NOT NULL,
	[SortOrder] [int] NOT NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_Church_ChurchStatus] PRIMARY KEY CLUSTERED 
(
	[ChurchStatus_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Church_ChurchStatus] ADD  CONSTRAINT [DF_Church_ChurchStatus_Deleted]  DEFAULT ((0)) FOR [Deleted]
GO
