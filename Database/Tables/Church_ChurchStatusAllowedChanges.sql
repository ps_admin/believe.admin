SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Church_ChurchStatusAllowedChanges](
	[ChurchStatusChange_ID] [int] IDENTITY(1,1) NOT NULL,
	[ChurchStatusFrom_ID] [int] NULL,
	[ChurchStatusTo_ID] [int] NOT NULL,
	[ChangeMustBeApproved] [bit] NOT NULL,
 CONSTRAINT [PK_Church_ChurchStatusChange] PRIMARY KEY CLUSTERED 
(
	[ChurchStatusChange_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Church_ChurchStatusAllowedChanges]  WITH CHECK ADD  CONSTRAINT [FK_Church_ChurchStatusAllowedChanges_Church_ChurchStatus] FOREIGN KEY([ChurchStatusFrom_ID])
REFERENCES [dbo].[Church_ChurchStatus] ([ChurchStatus_ID])
GO
ALTER TABLE [dbo].[Church_ChurchStatusAllowedChanges] CHECK CONSTRAINT [FK_Church_ChurchStatusAllowedChanges_Church_ChurchStatus]
GO
ALTER TABLE [dbo].[Church_ChurchStatusAllowedChanges]  WITH CHECK ADD  CONSTRAINT [FK_Church_ChurchStatusAllowedChanges_Church_ChurchStatus1] FOREIGN KEY([ChurchStatusTo_ID])
REFERENCES [dbo].[Church_ChurchStatus] ([ChurchStatus_ID])
GO
ALTER TABLE [dbo].[Church_ChurchStatusAllowedChanges] CHECK CONSTRAINT [FK_Church_ChurchStatusAllowedChanges_Church_ChurchStatus1]
GO
