SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Church_ChurchStatusChangeRequest](
	[StatusChangeRequest_ID] [int] IDENTITY(1,1) NOT NULL,
	[Contact_ID] [int] NOT NULL,
	[ChurchStatus_ID] [int] NOT NULL,
	[DeleteReason_ID] [int] NULL,
	[CurrentCampus_ID] [int] NULL,
	[CurrentMinistry_ID] [int] NULL,
	[CurrentRegion_ID] [int] NULL,
	[NPNC_ID] [int] NULL,
	[Campus_ID] [int] NULL,
	[Ministry_ID] [int] NULL,
	[Region_ID] [int] NULL,
	[ULG_ID] [int] NULL,
	[Comments] [nvarchar](max) NOT NULL,
	[DateRequested] [datetime] NOT NULL,
	[RequestedBy_ID] [int] NOT NULL,
	[Actioned] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_Church_ContactDeletePending] PRIMARY KEY CLUSTERED 
(
	[StatusChangeRequest_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Church_ChurchStatusChangeRequest]  WITH CHECK ADD  CONSTRAINT [FK_Church_ChurchStatusChangeRequest_Church_ChurchStatus] FOREIGN KEY([ChurchStatus_ID])
REFERENCES [dbo].[Church_ChurchStatus] ([ChurchStatus_ID])
GO
ALTER TABLE [dbo].[Church_ChurchStatusChangeRequest] CHECK CONSTRAINT [FK_Church_ChurchStatusChangeRequest_Church_ChurchStatus]
GO
ALTER TABLE [dbo].[Church_ChurchStatusChangeRequest]  WITH CHECK ADD  CONSTRAINT [FK_Church_ChurchStatusChangeRequest_Church_Ministry] FOREIGN KEY([CurrentMinistry_ID])
REFERENCES [dbo].[Church_Ministry] ([Ministry_ID])
GO
ALTER TABLE [dbo].[Church_ChurchStatusChangeRequest] CHECK CONSTRAINT [FK_Church_ChurchStatusChangeRequest_Church_Ministry]
GO
ALTER TABLE [dbo].[Church_ChurchStatusChangeRequest]  WITH CHECK ADD  CONSTRAINT [FK_Church_ChurchStatusChangeRequest_Church_Ministry1] FOREIGN KEY([Ministry_ID])
REFERENCES [dbo].[Church_Ministry] ([Ministry_ID])
GO
ALTER TABLE [dbo].[Church_ChurchStatusChangeRequest] CHECK CONSTRAINT [FK_Church_ChurchStatusChangeRequest_Church_Ministry1]
GO
ALTER TABLE [dbo].[Church_ChurchStatusChangeRequest]  WITH CHECK ADD  CONSTRAINT [FK_Church_ChurchStatusChangeRequest_Church_Region] FOREIGN KEY([CurrentRegion_ID])
REFERENCES [dbo].[Church_Region] ([Region_ID])
GO
ALTER TABLE [dbo].[Church_ChurchStatusChangeRequest] CHECK CONSTRAINT [FK_Church_ChurchStatusChangeRequest_Church_Region]
GO
ALTER TABLE [dbo].[Church_ChurchStatusChangeRequest]  WITH CHECK ADD  CONSTRAINT [FK_Church_ChurchStatusChangeRequest_Church_Region1] FOREIGN KEY([Region_ID])
REFERENCES [dbo].[Church_Region] ([Region_ID])
GO
ALTER TABLE [dbo].[Church_ChurchStatusChangeRequest] CHECK CONSTRAINT [FK_Church_ChurchStatusChangeRequest_Church_Region1]
GO
ALTER TABLE [dbo].[Church_ChurchStatusChangeRequest]  WITH CHECK ADD  CONSTRAINT [FK_Church_ChurchStatusChangeRequest_Church_ULG] FOREIGN KEY([ULG_ID])
REFERENCES [dbo].[Church_ULG] ([ULG_ID])
GO
ALTER TABLE [dbo].[Church_ChurchStatusChangeRequest] CHECK CONSTRAINT [FK_Church_ChurchStatusChangeRequest_Church_ULG]
GO
ALTER TABLE [dbo].[Church_ChurchStatusChangeRequest]  WITH CHECK ADD  CONSTRAINT [FK_Church_ChurchStatusChangeRequest_Common_ContactInternal] FOREIGN KEY([Contact_ID])
REFERENCES [dbo].[Common_ContactInternal] ([Contact_ID])
GO
ALTER TABLE [dbo].[Church_ChurchStatusChangeRequest] CHECK CONSTRAINT [FK_Church_ChurchStatusChangeRequest_Common_ContactInternal]
GO
ALTER TABLE [dbo].[Church_ChurchStatusChangeRequest]  WITH CHECK ADD  CONSTRAINT [FK_Church_ChurchStatusChangeRequest_Common_UserDetail] FOREIGN KEY([RequestedBy_ID])
REFERENCES [dbo].[Common_UserDetail] ([Contact_ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Church_ChurchStatusChangeRequest] CHECK CONSTRAINT [FK_Church_ChurchStatusChangeRequest_Common_UserDetail]
GO
