SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Church_ContactAdditionalDecision](
	[AdditionalDecision_ID] [int] IDENTITY(1,1) NOT NULL,
	[Contact_ID] [int] NOT NULL,
	[DecisionDate] [datetime] NOT NULL,
	[DecisionType_ID] [int] NOT NULL,
	[CampusDecision_ID] [int] NOT NULL,
	[Comments] [nvarchar](max) NOT NULL,
	[EnteredDate] [datetime] NOT NULL,
	[EnteredBy_ID] [int] NOT NULL,
 CONSTRAINT [PK_Church_ContactAdditionalDecision] PRIMARY KEY CLUSTERED 
(
	[AdditionalDecision_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Church_ContactAdditionalDecision]  WITH CHECK ADD  CONSTRAINT [FK_Church_ContactAdditionalDecision_Common_ContactInternal] FOREIGN KEY([Contact_ID])
REFERENCES [dbo].[Common_ContactInternal] ([Contact_ID])
GO
ALTER TABLE [dbo].[Church_ContactAdditionalDecision] CHECK CONSTRAINT [FK_Church_ContactAdditionalDecision_Common_ContactInternal]
GO
