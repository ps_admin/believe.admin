SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Church_ContactComment](
	[Comment_ID] [int] IDENTITY(1,1) NOT NULL,
	[Contact_ID] [int] NOT NULL,
	[CommentBy_ID] [int] NOT NULL,
	[CommentDate] [datetime] NOT NULL,
	[Comment] [nvarchar](max) NOT NULL,
	[CommentType_ID] [int] NOT NULL,
	[CommentSource_ID] [int] NOT NULL,
	[NPNC_ID] [int] NULL,
	[InReplyToComment_ID] [int] NULL,
	[Actioned] [bit] NOT NULL,
	[ActionedBy_ID] [int] NULL,
	[ActionedDate] [datetime] NULL,
 CONSTRAINT [PK_Church_Comment] PRIMARY KEY CLUSTERED 
(
	[Comment_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Church_ContactComment]  WITH CHECK ADD  CONSTRAINT [FK_Church_ContactComment_Church_CommentSource] FOREIGN KEY([CommentSource_ID])
REFERENCES [dbo].[Church_CommentSource] ([CommentSource_ID])
GO
ALTER TABLE [dbo].[Church_ContactComment] CHECK CONSTRAINT [FK_Church_ContactComment_Church_CommentSource]
GO
ALTER TABLE [dbo].[Church_ContactComment]  WITH CHECK ADD  CONSTRAINT [FK_Church_ContactComment_Church_CommentType] FOREIGN KEY([CommentType_ID])
REFERENCES [dbo].[Church_CommentType] ([CommentType_ID])
GO
ALTER TABLE [dbo].[Church_ContactComment] CHECK CONSTRAINT [FK_Church_ContactComment_Church_CommentType]
GO
ALTER TABLE [dbo].[Church_ContactComment]  WITH CHECK ADD  CONSTRAINT [FK_Church_ContactComment_Common_ContactInternal] FOREIGN KEY([Contact_ID])
REFERENCES [dbo].[Common_ContactInternal] ([Contact_ID])
GO
ALTER TABLE [dbo].[Church_ContactComment] CHECK CONSTRAINT [FK_Church_ContactComment_Common_ContactInternal]
GO
ALTER TABLE [dbo].[Church_ContactComment]  WITH CHECK ADD  CONSTRAINT [FK_Church_ContactComment_Common_UserDetail] FOREIGN KEY([CommentBy_ID])
REFERENCES [dbo].[Common_UserDetail] ([Contact_ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Church_ContactComment] CHECK CONSTRAINT [FK_Church_ContactComment_Common_UserDetail]
GO
ALTER TABLE [dbo].[Church_ContactComment] ADD  CONSTRAINT [DF_Church_ContactComment_CommentSource_ID]  DEFAULT ((1)) FOR [CommentSource_ID]
GO
ALTER TABLE [dbo].[Church_ContactComment] ADD  CONSTRAINT [DF_Church_ContactComment_Actioned]  DEFAULT ((0)) FOR [Actioned]
GO
