SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Church_ContactCourseEnrolment](
	[ContactCourseEnrolment_ID] [int] IDENTITY(1,1) NOT NULL,
	[Contact_ID] [int] NOT NULL,
	[CourseInstance_ID] [int] NOT NULL,
	[EnrolmentDate] [datetime] NOT NULL,
	[Comments] [nvarchar](max) NOT NULL,
	[IntroductoryEmailSentDate] [datetime] NULL,
	[Completed] [bit] NOT NULL,
 CONSTRAINT [PK_Church_CourseEnrollment] PRIMARY KEY CLUSTERED 
(
	[ContactCourseEnrolment_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Church_ContactCourseEnrolment]  WITH CHECK ADD  CONSTRAINT [FK_Church_ContactCourseEnrollment_Church_CourseInstance] FOREIGN KEY([CourseInstance_ID])
REFERENCES [dbo].[Church_CourseInstance] ([CourseInstance_ID])
GO
ALTER TABLE [dbo].[Church_ContactCourseEnrolment] CHECK CONSTRAINT [FK_Church_ContactCourseEnrollment_Church_CourseInstance]
GO
ALTER TABLE [dbo].[Church_ContactCourseEnrolment]  WITH CHECK ADD  CONSTRAINT [FK_Church_ContactCourseEnrollment_Common_ContactInternal] FOREIGN KEY([Contact_ID])
REFERENCES [dbo].[Common_ContactInternal] ([Contact_ID])
GO
ALTER TABLE [dbo].[Church_ContactCourseEnrolment] CHECK CONSTRAINT [FK_Church_ContactCourseEnrollment_Common_ContactInternal]
GO
ALTER TABLE [dbo].[Church_ContactCourseEnrolment] ADD  CONSTRAINT [DF_Church_ContactCourseEnrolment_Comments]  DEFAULT ('') FOR [Comments]
GO
ALTER TABLE [dbo].[Church_ContactCourseEnrolment] ADD  CONSTRAINT [DF_Church_ContactCourseEnrolment_IntroductoryEmailSent]  DEFAULT ((0)) FOR [IntroductoryEmailSentDate]
GO
ALTER TABLE [dbo].[Church_ContactCourseEnrolment] ADD  CONSTRAINT [DF_Church_ContactCourseEnrollment_Completed]  DEFAULT ((0)) FOR [Completed]
GO
