SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Church_ContactCourseInstance](
	[ContactCourseInstance_ID] [int] IDENTITY(1,1) NOT NULL,
	[Contact_ID] [int] NOT NULL,
	[CourseInstance_ID] [int] NOT NULL,
	[DateAdded] [datetime] NOT NULL,
 CONSTRAINT [PK_Church_ContactCourseInstance] PRIMARY KEY CLUSTERED 
(
	[ContactCourseInstance_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Church_ContactCourseInstance]  WITH CHECK ADD  CONSTRAINT [FK_Church_ContactCourseInstance_Church_CourseInstance] FOREIGN KEY([CourseInstance_ID])
REFERENCES [dbo].[Church_CourseInstance] ([CourseInstance_ID])
GO
ALTER TABLE [dbo].[Church_ContactCourseInstance] CHECK CONSTRAINT [FK_Church_ContactCourseInstance_Church_CourseInstance]
GO
ALTER TABLE [dbo].[Church_ContactCourseInstance]  WITH CHECK ADD  CONSTRAINT [FK_Church_ContactCourseInstance_Common_ContactInternal] FOREIGN KEY([Contact_ID])
REFERENCES [dbo].[Common_ContactInternal] ([Contact_ID])
GO
ALTER TABLE [dbo].[Church_ContactCourseInstance] CHECK CONSTRAINT [FK_Church_ContactCourseInstance_Common_ContactInternal]
GO
