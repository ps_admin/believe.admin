SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Church_ContactKidsMinistryCode](
	[ContactKidsMinistryCode_ID] [int] IDENTITY(1,1) NOT NULL,
	[Contact_ID] [int] NOT NULL,
	[KidsMinistryCode_ID] [int] NOT NULL,
	[DateAdded] [datetime] NOT NULL,
 CONSTRAINT [PK_Church_ContactPlanetKidsChildCode] PRIMARY KEY CLUSTERED 
(
	[ContactKidsMinistryCode_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Church_ContactKidsMinistryCode]  WITH CHECK ADD  CONSTRAINT [FK_Church_ContactKidsMinistryCode_Church_KidsMinistryCode] FOREIGN KEY([KidsMinistryCode_ID])
REFERENCES [dbo].[Church_KidsMinistryCode] ([KidsMinistryCode_ID])
GO
ALTER TABLE [dbo].[Church_ContactKidsMinistryCode] CHECK CONSTRAINT [FK_Church_ContactKidsMinistryCode_Church_KidsMinistryCode]
GO
ALTER TABLE [dbo].[Church_ContactKidsMinistryCode]  WITH CHECK ADD  CONSTRAINT [FK_Church_ContactKidsMinistryCode_Common_ContactInternal] FOREIGN KEY([Contact_ID])
REFERENCES [dbo].[Common_ContactInternal] ([Contact_ID])
GO
ALTER TABLE [dbo].[Church_ContactKidsMinistryCode] CHECK CONSTRAINT [FK_Church_ContactKidsMinistryCode_Common_ContactInternal]
GO
