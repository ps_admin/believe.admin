SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Church_ContactRole](
	[ContactRole_ID] [int] IDENTITY(1,1) NOT NULL,
	[Contact_ID] [int] NOT NULL,
	[Role_ID] [int] NOT NULL,
	[Campus_ID] [int] NOT NULL,
	[DateAdded] [datetime] NOT NULL,
 CONSTRAINT [PK_Church_ContactRole] PRIMARY KEY CLUSTERED 
(
	[ContactRole_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Church_ContactRole]  WITH CHECK ADD  CONSTRAINT [FK_Church_ContactRole_Church_Role] FOREIGN KEY([Role_ID])
REFERENCES [dbo].[Church_Role] ([Role_ID])
GO
ALTER TABLE [dbo].[Church_ContactRole] CHECK CONSTRAINT [FK_Church_ContactRole_Church_Role]
GO
ALTER TABLE [dbo].[Church_ContactRole]  WITH CHECK ADD  CONSTRAINT [FK_Church_ContactRole_Common_ContactInternal] FOREIGN KEY([Contact_ID])
REFERENCES [dbo].[Common_ContactInternal] ([Contact_ID])
GO
ALTER TABLE [dbo].[Church_ContactRole] CHECK CONSTRAINT [FK_Church_ContactRole_Common_ContactInternal]
GO
