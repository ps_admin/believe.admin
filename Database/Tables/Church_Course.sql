SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Church_Course](
	[Course_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[PointsRequiredForCompletion] [int] NOT NULL,
	[ShowInCustomReport] [bit] NOT NULL,
	[LogCompletedSession] [bit] NOT NULL,
	[Deleted] [bit] NOT NULL,
	[SortOrder] [int] NOT NULL,
 CONSTRAINT [PK_Church_Course] PRIMARY KEY CLUSTERED 
(
	[Course_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Church_Course] ADD  CONSTRAINT [DF_Church_Course_PointsRequiredForCompletion]  DEFAULT ((0)) FOR [PointsRequiredForCompletion]
GO
ALTER TABLE [dbo].[Church_Course] ADD  CONSTRAINT [DF_Church_Course_ShowInCustomReport]  DEFAULT ((0)) FOR [ShowInCustomReport]
GO
ALTER TABLE [dbo].[Church_Course] ADD  CONSTRAINT [DF_Church_Course_LogCompletedSession]  DEFAULT ((0)) FOR [LogCompletedSession]
GO
ALTER TABLE [dbo].[Church_Course] ADD  CONSTRAINT [DF_Church_Course_Deleted]  DEFAULT ((0)) FOR [Deleted]
GO
