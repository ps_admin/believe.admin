SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Church_CourseEnrolmentAttendance](
	[CourseEnrolmentAttendance_ID] [int] IDENTITY(1,1) NOT NULL,
	[Contact_ID] [int] NOT NULL,
	[CourseSession_ID] [int] NOT NULL,
	[MarkedOffBy_ID] [int] NOT NULL,
	[MarkedOffDateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_Church_ContactCourseEnrollmentAttendance] PRIMARY KEY CLUSTERED 
(
	[CourseEnrolmentAttendance_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Church_CourseEnrolmentAttendance]  WITH CHECK ADD  CONSTRAINT [FK_Church_ContactCourseEnrollmentAttendance_Church_CourseSession] FOREIGN KEY([CourseSession_ID])
REFERENCES [dbo].[Church_CourseSession] ([CourseSession_ID])
GO
ALTER TABLE [dbo].[Church_CourseEnrolmentAttendance] CHECK CONSTRAINT [FK_Church_ContactCourseEnrollmentAttendance_Church_CourseSession]
GO
ALTER TABLE [dbo].[Church_CourseEnrolmentAttendance]  WITH CHECK ADD  CONSTRAINT [FK_Church_CourseEnrollmentAttendance_Common_ContactInternal] FOREIGN KEY([Contact_ID])
REFERENCES [dbo].[Common_ContactInternal] ([Contact_ID])
GO
ALTER TABLE [dbo].[Church_CourseEnrolmentAttendance] CHECK CONSTRAINT [FK_Church_CourseEnrollmentAttendance_Common_ContactInternal]
GO
