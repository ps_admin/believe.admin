SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Church_CourseEnrolmentAttendanceTopicsCovered](
	[CourseEnrolmentAttendanceTopicsCovered_ID] [int] IDENTITY(1,1) NOT NULL,
	[CourseEnrolmentAttendance_ID] [int] NOT NULL,
	[CourseTopic_ID] [int] NOT NULL,
 CONSTRAINT [PK_Church_CourseEnrolmentAttendanceTopicsCovered] PRIMARY KEY CLUSTERED 
(
	[CourseEnrolmentAttendanceTopicsCovered_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Church_CourseEnrolmentAttendanceTopicsCovered]  WITH CHECK ADD  CONSTRAINT [FK_Church_CourseEnrolmentAttendanceTopicsCovered_Church_CourseEnrolmentAttendance] FOREIGN KEY([CourseEnrolmentAttendance_ID])
REFERENCES [dbo].[Church_CourseEnrolmentAttendance] ([CourseEnrolmentAttendance_ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Church_CourseEnrolmentAttendanceTopicsCovered] CHECK CONSTRAINT [FK_Church_CourseEnrolmentAttendanceTopicsCovered_Church_CourseEnrolmentAttendance]
GO
ALTER TABLE [dbo].[Church_CourseEnrolmentAttendanceTopicsCovered]  WITH CHECK ADD  CONSTRAINT [FK_Church_CourseEnrolmentAttendanceTopicsCovered_Church_CourseTopic] FOREIGN KEY([CourseTopic_ID])
REFERENCES [dbo].[Church_CourseTopic] ([CourseTopic_ID])
GO
ALTER TABLE [dbo].[Church_CourseEnrolmentAttendanceTopicsCovered] CHECK CONSTRAINT [FK_Church_CourseEnrolmentAttendanceTopicsCovered_Church_CourseTopic]
GO
