SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Church_CourseInstance](
	[CourseInstance_ID] [int] IDENTITY(1,1) NOT NULL,
	[Course_ID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[StartDate] [datetime] NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_Church_CourseInstance] PRIMARY KEY CLUSTERED 
(
	[CourseInstance_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Church_CourseInstance]  WITH CHECK ADD  CONSTRAINT [FK_Church_CourseInstance_Church_Course] FOREIGN KEY([Course_ID])
REFERENCES [dbo].[Church_Course] ([Course_ID])
GO
ALTER TABLE [dbo].[Church_CourseInstance] CHECK CONSTRAINT [FK_Church_CourseInstance_Church_Course]
GO
ALTER TABLE [dbo].[Church_CourseInstance] ADD  CONSTRAINT [DF_Church_CourseInstance_Deleted]  DEFAULT ((0)) FOR [Deleted]
GO
