SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Church_CourseInstanceCampus](
	[CourseInstanceCampus_ID] [int] IDENTITY(1,1) NOT NULL,
	[CourseInstance_ID] [int] NOT NULL,
	[Campus_ID] [int] NOT NULL,
 CONSTRAINT [PK_Church_CourseInstanceCampus] PRIMARY KEY CLUSTERED 
(
	[CourseInstanceCampus_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Church_CourseInstanceCampus]  WITH CHECK ADD  CONSTRAINT [FK_Church_CourseInstanceCampus_Church_Campus] FOREIGN KEY([Campus_ID])
REFERENCES [dbo].[Church_Campus] ([Campus_ID])
GO
ALTER TABLE [dbo].[Church_CourseInstanceCampus] CHECK CONSTRAINT [FK_Church_CourseInstanceCampus_Church_Campus]
GO
ALTER TABLE [dbo].[Church_CourseInstanceCampus]  WITH CHECK ADD  CONSTRAINT [FK_Church_CourseInstanceCampus_Church_CourseInstance] FOREIGN KEY([CourseInstance_ID])
REFERENCES [dbo].[Church_CourseInstance] ([CourseInstance_ID])
GO
ALTER TABLE [dbo].[Church_CourseInstanceCampus] CHECK CONSTRAINT [FK_Church_CourseInstanceCampus_Church_CourseInstance]
GO
