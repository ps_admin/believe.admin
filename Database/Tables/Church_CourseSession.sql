SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Church_CourseSession](
	[CourseSession_ID] [int] IDENTITY(1,1) NOT NULL,
	[CourseInstance_ID] [int] NOT NULL,
	[Date] [datetime] NOT NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_Church_CourseSession] PRIMARY KEY CLUSTERED 
(
	[CourseSession_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Church_CourseSession]  WITH CHECK ADD  CONSTRAINT [FK_Church_CourseSession_Church_CourseInstance] FOREIGN KEY([CourseInstance_ID])
REFERENCES [dbo].[Church_CourseInstance] ([CourseInstance_ID])
GO
ALTER TABLE [dbo].[Church_CourseSession] CHECK CONSTRAINT [FK_Church_CourseSession_Church_CourseInstance]
GO
