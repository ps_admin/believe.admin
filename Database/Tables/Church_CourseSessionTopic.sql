SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Church_CourseSessionTopic](
	[CourseSessionTopic_ID] [int] IDENTITY(1,1) NOT NULL,
	[CourseInstance_ID] [int] NULL,
	[CourseSession_ID] [int] NOT NULL,
	[CourseTopic_ID] [int] NOT NULL,
 CONSTRAINT [PK_Church_CourseTopicSession] PRIMARY KEY CLUSTERED 
(
	[CourseSessionTopic_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Church_CourseSessionTopic]  WITH CHECK ADD  CONSTRAINT [FK_Church_CourseSessionTopic_Church_CourseInstance] FOREIGN KEY([CourseInstance_ID])
REFERENCES [dbo].[Church_CourseInstance] ([CourseInstance_ID])
GO
ALTER TABLE [dbo].[Church_CourseSessionTopic] CHECK CONSTRAINT [FK_Church_CourseSessionTopic_Church_CourseInstance]
GO
ALTER TABLE [dbo].[Church_CourseSessionTopic]  WITH CHECK ADD  CONSTRAINT [FK_Church_CourseTopicSession_Church_CourseSession] FOREIGN KEY([CourseSession_ID])
REFERENCES [dbo].[Church_CourseSession] ([CourseSession_ID])
GO
ALTER TABLE [dbo].[Church_CourseSessionTopic] CHECK CONSTRAINT [FK_Church_CourseTopicSession_Church_CourseSession]
GO
ALTER TABLE [dbo].[Church_CourseSessionTopic]  WITH CHECK ADD  CONSTRAINT [FK_Church_CourseTopicSession_Church_CourseTopic] FOREIGN KEY([CourseTopic_ID])
REFERENCES [dbo].[Church_CourseTopic] ([CourseTopic_ID])
GO
ALTER TABLE [dbo].[Church_CourseSessionTopic] CHECK CONSTRAINT [FK_Church_CourseTopicSession_Church_CourseTopic]
GO
