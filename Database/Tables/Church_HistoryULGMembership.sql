SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Church_HistoryULGMembership](
	[HistoryULGMembership_ID] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NOT NULL,
	[ULG_ID] [int] NOT NULL,
	[ChurchStatus_ID] [int] NOT NULL,
	[Campus_ID] [int] NULL,
	[Ministry_ID] [int] NULL,
	[Region_ID] [int] NULL,
	[Number] [int] NOT NULL,
 CONSTRAINT [PK_Church_HistoryULGMembership] PRIMARY KEY CLUSTERED 
(
	[HistoryULGMembership_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Church_HistoryULGMembership]  WITH CHECK ADD  CONSTRAINT [FK_Church_HistoryULGMembership_Church_ChurchStatus] FOREIGN KEY([ChurchStatus_ID])
REFERENCES [dbo].[Church_ChurchStatus] ([ChurchStatus_ID])
GO
ALTER TABLE [dbo].[Church_HistoryULGMembership] CHECK CONSTRAINT [FK_Church_HistoryULGMembership_Church_ChurchStatus]
GO
ALTER TABLE [dbo].[Church_HistoryULGMembership]  WITH CHECK ADD  CONSTRAINT [FK_Church_HistoryULGMembership_Church_Ministry] FOREIGN KEY([Ministry_ID])
REFERENCES [dbo].[Church_Ministry] ([Ministry_ID])
GO
ALTER TABLE [dbo].[Church_HistoryULGMembership] CHECK CONSTRAINT [FK_Church_HistoryULGMembership_Church_Ministry]
GO
ALTER TABLE [dbo].[Church_HistoryULGMembership]  WITH CHECK ADD  CONSTRAINT [FK_Church_HistoryULGMembership_Church_Region] FOREIGN KEY([Region_ID])
REFERENCES [dbo].[Church_Region] ([Region_ID])
GO
ALTER TABLE [dbo].[Church_HistoryULGMembership] CHECK CONSTRAINT [FK_Church_HistoryULGMembership_Church_Region]
GO
ALTER TABLE [dbo].[Church_HistoryULGMembership]  WITH CHECK ADD  CONSTRAINT [FK_Church_HistoryULGMembership_Church_ULG] FOREIGN KEY([ULG_ID])
REFERENCES [dbo].[Church_ULG] ([ULG_ID])
GO
ALTER TABLE [dbo].[Church_HistoryULGMembership] CHECK CONSTRAINT [FK_Church_HistoryULGMembership_Church_ULG]
GO
