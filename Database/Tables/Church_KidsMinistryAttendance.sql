SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Church_KidsMinistryAttendance](
	[KidsMinistryAttendance_ID] [int] IDENTITY(1,1) NOT NULL,
	[Contact_ID] [int] NOT NULL,
	[PCRDate_ID] [int] NULL,
	[Date] [datetime] NOT NULL,
	[ChurchService_ID] [int] NOT NULL,
	[CheckInTime] [datetime] NOT NULL,
	[CheckedInByParent_ID] [int] NOT NULL,
	[CheckedInByLeader_ID] [int] NOT NULL,
	[CheckOutTime] [datetime] NULL,
	[CheckedOutByParent_ID] [int] NULL,
	[CheckedOutByLeader_ID] [int] NULL,
 CONSTRAINT [PK_Church_KidsMinistryAttendance] PRIMARY KEY CLUSTERED 
(
	[KidsMinistryAttendance_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Church_KidsMinistryAttendance] ON [dbo].[Church_KidsMinistryAttendance] 
(
	[Contact_ID] ASC,
	[Date] ASC,
	[ChurchService_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Church_KidsMinistryAttendance]  WITH CHECK ADD  CONSTRAINT [FK_Church_KidsMinistryAttendance_Church_ChurchService1] FOREIGN KEY([ChurchService_ID])
REFERENCES [dbo].[Church_ChurchService] ([ChurchService_ID])
GO
ALTER TABLE [dbo].[Church_KidsMinistryAttendance] CHECK CONSTRAINT [FK_Church_KidsMinistryAttendance_Church_ChurchService1]
GO
ALTER TABLE [dbo].[Church_KidsMinistryAttendance]  WITH CHECK ADD  CONSTRAINT [FK_Church_KidsMinistryAttendance_Church_PCRDate] FOREIGN KEY([PCRDate_ID])
REFERENCES [dbo].[Church_PCRDate] ([PCRDate_ID])
GO
ALTER TABLE [dbo].[Church_KidsMinistryAttendance] CHECK CONSTRAINT [FK_Church_KidsMinistryAttendance_Church_PCRDate]
GO
ALTER TABLE [dbo].[Church_KidsMinistryAttendance]  WITH CHECK ADD  CONSTRAINT [FK_Church_KidsMinistryAttendance_Common_ContactInternal] FOREIGN KEY([Contact_ID])
REFERENCES [dbo].[Common_ContactInternal] ([Contact_ID])
GO
ALTER TABLE [dbo].[Church_KidsMinistryAttendance] CHECK CONSTRAINT [FK_Church_KidsMinistryAttendance_Common_ContactInternal]
GO
ALTER TABLE [dbo].[Church_KidsMinistryAttendance]  WITH CHECK ADD  CONSTRAINT [FK_Church_KidsMinistryAttendance_Common_ContactInternal1] FOREIGN KEY([CheckedInByParent_ID])
REFERENCES [dbo].[Common_ContactInternal] ([Contact_ID])
GO
ALTER TABLE [dbo].[Church_KidsMinistryAttendance] CHECK CONSTRAINT [FK_Church_KidsMinistryAttendance_Common_ContactInternal1]
GO
ALTER TABLE [dbo].[Church_KidsMinistryAttendance]  WITH CHECK ADD  CONSTRAINT [FK_Church_KidsMinistryAttendance_Common_ContactInternal2] FOREIGN KEY([CheckedOutByParent_ID])
REFERENCES [dbo].[Common_ContactInternal] ([Contact_ID])
GO
ALTER TABLE [dbo].[Church_KidsMinistryAttendance] CHECK CONSTRAINT [FK_Church_KidsMinistryAttendance_Common_ContactInternal2]
GO
ALTER TABLE [dbo].[Church_KidsMinistryAttendance] ADD  CONSTRAINT [DF_Church_KidsMinistryAttendance_CheckedInByLeader_ID]  DEFAULT ((12782)) FOR [CheckedInByLeader_ID]
GO
