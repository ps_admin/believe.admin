SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Church_KidsMinistryAttendanceLeader](
	[KidsMinistryAttendanceLeader_ID] [int] IDENTITY(1,1) NOT NULL,
	[Contact_ID] [int] NOT NULL,
	[Date] [datetime] NOT NULL,
	[ChurchService_ID] [int] NOT NULL,
	[CheckInTime] [datetime] NOT NULL,
	[CheckOutTime] [datetime] NULL,
 CONSTRAINT [PK_Church_KidsMinistryAttendanceLeader] PRIMARY KEY CLUSTERED 
(
	[KidsMinistryAttendanceLeader_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Church_KidsMinistryAttendanceLeader]  WITH CHECK ADD  CONSTRAINT [FK_Church_KidsMinistryAttendance_Church_ChurchService] FOREIGN KEY([ChurchService_ID])
REFERENCES [dbo].[Church_ChurchService] ([ChurchService_ID])
GO
ALTER TABLE [dbo].[Church_KidsMinistryAttendanceLeader] CHECK CONSTRAINT [FK_Church_KidsMinistryAttendance_Church_ChurchService]
GO
ALTER TABLE [dbo].[Church_KidsMinistryAttendanceLeader]  WITH CHECK ADD  CONSTRAINT [FK_Church_KidsMinistryAttendanceLeader_Common_ContactInternal] FOREIGN KEY([Contact_ID])
REFERENCES [dbo].[Common_ContactInternal] ([Contact_ID])
GO
ALTER TABLE [dbo].[Church_KidsMinistryAttendanceLeader] CHECK CONSTRAINT [FK_Church_KidsMinistryAttendanceLeader_Common_ContactInternal]
GO
