SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Church_KidsMinistryClearance](
	[KidsMinistryClearance_ID] [int] IDENTITY(1,1) NOT NULL,
	[Contact_ID] [int] NOT NULL,
	[ClearanceContact_ID] [int] NOT NULL,
	[DateAdded] [datetime] NOT NULL,
 CONSTRAINT [PK_Church_KidsMinistryClearance] PRIMARY KEY CLUSTERED 
(
	[KidsMinistryClearance_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Church_KidsMinistryClearance]  WITH CHECK ADD  CONSTRAINT [FK_Church_KidsMinistryClearance_Common_ContactInternal] FOREIGN KEY([Contact_ID])
REFERENCES [dbo].[Common_ContactInternal] ([Contact_ID])
GO
ALTER TABLE [dbo].[Church_KidsMinistryClearance] CHECK CONSTRAINT [FK_Church_KidsMinistryClearance_Common_ContactInternal]
GO
ALTER TABLE [dbo].[Church_KidsMinistryClearance]  WITH CHECK ADD  CONSTRAINT [FK_Church_KidsMinistryClearance_Common_ContactInternal1] FOREIGN KEY([ClearanceContact_ID])
REFERENCES [dbo].[Common_ContactInternal] ([Contact_ID])
GO
ALTER TABLE [dbo].[Church_KidsMinistryClearance] CHECK CONSTRAINT [FK_Church_KidsMinistryClearance_Common_ContactInternal1]
GO
