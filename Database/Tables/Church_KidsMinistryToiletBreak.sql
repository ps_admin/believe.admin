SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Church_KidsMinistryToiletBreak](
	[KidsMinistryToiletBreak_ID] [int] IDENTITY(1,1) NOT NULL,
	[Contact_ID] [int] NOT NULL,
	[Leader1_ID] [int] NOT NULL,
	[Leader2_ID] [int] NOT NULL,
	[ChurchService_ID] [int] NOT NULL,
	[TimeTaken] [datetime] NOT NULL,
	[TimeReturned] [datetime] NULL,
	[Comments] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Church_KidsMinistryToiletBreak] PRIMARY KEY CLUSTERED 
(
	[KidsMinistryToiletBreak_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Church_KidsMinistryToiletBreak]  WITH CHECK ADD  CONSTRAINT [FK_Church_KidsMinistryToiletBreak_Church_ChurchService] FOREIGN KEY([ChurchService_ID])
REFERENCES [dbo].[Church_ChurchService] ([ChurchService_ID])
GO
ALTER TABLE [dbo].[Church_KidsMinistryToiletBreak] CHECK CONSTRAINT [FK_Church_KidsMinistryToiletBreak_Church_ChurchService]
GO
ALTER TABLE [dbo].[Church_KidsMinistryToiletBreak]  WITH CHECK ADD  CONSTRAINT [FK_Church_KidsMinistryToiletBreak_Common_ContactInternal] FOREIGN KEY([Contact_ID])
REFERENCES [dbo].[Common_ContactInternal] ([Contact_ID])
GO
ALTER TABLE [dbo].[Church_KidsMinistryToiletBreak] CHECK CONSTRAINT [FK_Church_KidsMinistryToiletBreak_Common_ContactInternal]
GO
ALTER TABLE [dbo].[Church_KidsMinistryToiletBreak]  WITH CHECK ADD  CONSTRAINT [FK_Church_KidsMinistryToiletBreak_Common_ContactInternal1] FOREIGN KEY([Leader1_ID])
REFERENCES [dbo].[Common_ContactInternal] ([Contact_ID])
GO
ALTER TABLE [dbo].[Church_KidsMinistryToiletBreak] CHECK CONSTRAINT [FK_Church_KidsMinistryToiletBreak_Common_ContactInternal1]
GO
ALTER TABLE [dbo].[Church_KidsMinistryToiletBreak]  WITH CHECK ADD  CONSTRAINT [FK_Church_KidsMinistryToiletBreak_Common_ContactInternal2] FOREIGN KEY([Leader2_ID])
REFERENCES [dbo].[Common_ContactInternal] ([Contact_ID])
GO
ALTER TABLE [dbo].[Church_KidsMinistryToiletBreak] CHECK CONSTRAINT [FK_Church_KidsMinistryToiletBreak_Common_ContactInternal2]
GO
