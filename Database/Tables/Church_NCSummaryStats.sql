SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Church_NCSummaryStats](
	[SummaryStats_ID] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NOT NULL,
	[Campus_ID] [int] NOT NULL,
	[NCSummaryStatsMinistry_ID] [int] NOT NULL,
	[NCDecisionType_ID] [int] NOT NULL,
	[Number] [int] NOT NULL,
	[EnteredBy_ID] [int] NOT NULL,
	[DateEntered] [datetime] NOT NULL,
 CONSTRAINT [PK_Church_NCSummaryStats] PRIMARY KEY CLUSTERED 
(
	[SummaryStats_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Church_NCSummaryStats]  WITH CHECK ADD  CONSTRAINT [FK_Church_NCSummaryStats_Church_NCDecisionType] FOREIGN KEY([NCDecisionType_ID])
REFERENCES [dbo].[Church_NCDecisionType] ([NCDecisionType_ID])
GO
ALTER TABLE [dbo].[Church_NCSummaryStats] CHECK CONSTRAINT [FK_Church_NCSummaryStats_Church_NCDecisionType]
GO
ALTER TABLE [dbo].[Church_NCSummaryStats]  WITH CHECK ADD  CONSTRAINT [FK_Church_NCSummaryStats_Church_NCSummaryStatsMinistry] FOREIGN KEY([NCSummaryStatsMinistry_ID])
REFERENCES [dbo].[Church_NCSummaryStatsMinistry] ([NCSummaryStatsMinistry_ID])
GO
ALTER TABLE [dbo].[Church_NCSummaryStats] CHECK CONSTRAINT [FK_Church_NCSummaryStats_Church_NCSummaryStatsMinistry]
GO
ALTER TABLE [dbo].[Church_NCSummaryStats]  WITH CHECK ADD  CONSTRAINT [FK_Church_NCSummaryStats_Common_UserDetail] FOREIGN KEY([EnteredBy_ID])
REFERENCES [dbo].[Common_UserDetail] ([Contact_ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Church_NCSummaryStats] CHECK CONSTRAINT [FK_Church_NCSummaryStats_Common_UserDetail]
GO
