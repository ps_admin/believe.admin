SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Church_NPNC](
	[NPNC_ID] [int] IDENTITY(1,1) NOT NULL,
	[Contact_ID] [int] NOT NULL,
	[NPNCType_ID] [int] NOT NULL,
	[FirstContactDate] [datetime] NOT NULL,
	[FirstFollowupCallDate] [datetime] NULL,
	[FirstFollowupVisitDate] [datetime] NULL,
	[InitialStatus_ID] [int] NOT NULL,
	[NCDecisionType_ID] [int] NULL,
	[NCTeamMember_ID] [int] NULL,
	[CampusDecision_ID] [int] NULL,
	[Campus_ID] [int] NULL,
	[Ministry_ID] [int] NULL,
	[Region_ID] [int] NULL,
	[ULG_ID] [int] NULL,
	[ULGDateOfIssue] [datetime] NULL,
	[Carer_ID] [int] NULL,
	[OutcomeStatus_ID] [int] NULL,
	[OutcomeDate] [datetime] NULL,
	[OutcomeInactive_ID] [int] NULL,
	[GUID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_Church_NPNC] PRIMARY KEY CLUSTERED 
(
	[NPNC_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Holds the contact''s current ministry, until they are outcomed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Church_NPNC', @level2type=N'COLUMN',@level2name=N'Ministry_ID'
GO
ALTER TABLE [dbo].[Church_NPNC]  WITH CHECK ADD  CONSTRAINT [FK_Church_NPNC_Church_NCDecisionType] FOREIGN KEY([NCDecisionType_ID])
REFERENCES [dbo].[Church_NCDecisionType] ([NCDecisionType_ID])
GO
ALTER TABLE [dbo].[Church_NPNC] CHECK CONSTRAINT [FK_Church_NPNC_Church_NCDecisionType]
GO
ALTER TABLE [dbo].[Church_NPNC]  WITH CHECK ADD  CONSTRAINT [FK_Church_NPNC_Church_NPNCInitialStatus] FOREIGN KEY([InitialStatus_ID])
REFERENCES [dbo].[Church_NPNCInitialStatus] ([InitialStatus_ID])
GO
ALTER TABLE [dbo].[Church_NPNC] CHECK CONSTRAINT [FK_Church_NPNC_Church_NPNCInitialStatus]
GO
ALTER TABLE [dbo].[Church_NPNC]  WITH CHECK ADD  CONSTRAINT [FK_Church_NPNC_Church_NPNCOutcome] FOREIGN KEY([OutcomeInactive_ID])
REFERENCES [dbo].[Church_NPNCOutcomeInactive] ([OutcomeInactive_ID])
GO
ALTER TABLE [dbo].[Church_NPNC] CHECK CONSTRAINT [FK_Church_NPNC_Church_NPNCOutcome]
GO
ALTER TABLE [dbo].[Church_NPNC]  WITH CHECK ADD  CONSTRAINT [FK_Church_NPNC_Church_NPNCType] FOREIGN KEY([NPNCType_ID])
REFERENCES [dbo].[Church_NPNCType] ([NPNCType_ID])
GO
ALTER TABLE [dbo].[Church_NPNC] CHECK CONSTRAINT [FK_Church_NPNC_Church_NPNCType]
GO
ALTER TABLE [dbo].[Church_NPNC]  WITH CHECK ADD  CONSTRAINT [FK_Church_NPNC_Common_ContactInternal] FOREIGN KEY([Contact_ID])
REFERENCES [dbo].[Common_ContactInternal] ([Contact_ID])
GO
ALTER TABLE [dbo].[Church_NPNC] CHECK CONSTRAINT [FK_Church_NPNC_Common_ContactInternal]
GO
ALTER TABLE [dbo].[Church_NPNC]  WITH CHECK ADD  CONSTRAINT [FK_Church_NPNC_Common_UserDetail] FOREIGN KEY([Carer_ID])
REFERENCES [dbo].[Common_UserDetail] ([Contact_ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Church_NPNC] CHECK CONSTRAINT [FK_Church_NPNC_Common_UserDetail]
GO
