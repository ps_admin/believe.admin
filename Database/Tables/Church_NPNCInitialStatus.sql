SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Church_NPNCInitialStatus](
	[InitialStatus_ID] [int] IDENTITY(1,1) NOT NULL,
	[NPNCType_ID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[ShowInEntryForm] [bit] NOT NULL,
	[ShowInSummaryStatsEntry] [bit] NOT NULL,
	[SortOrder] [int] NOT NULL,
 CONSTRAINT [PK_Church_NPNCInitialStatus] PRIMARY KEY CLUSTERED 
(
	[InitialStatus_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
