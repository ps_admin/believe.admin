SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Church_NPNCWelcomePacksIssued](
	[WelcomePacksIssued_ID] [int] IDENTITY(1,1) NOT NULL,
	[Campus_ID] [int] NOT NULL,
	[DateIssued] [datetime] NOT NULL,
	[NumberIssued] [int] NOT NULL,
	[NumberReturned] [int] NOT NULL,
	[EnteredBy_ID] [int] NOT NULL,
	[DateEntered] [datetime] NOT NULL,
 CONSTRAINT [PK_Church_NPNCWelcomePacksIssued] PRIMARY KEY CLUSTERED 
(
	[WelcomePacksIssued_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Church_NPNCWelcomePacksIssued]  WITH CHECK ADD  CONSTRAINT [FK_Church_NPNCWelcomePacksIssued_Common_UserDetail] FOREIGN KEY([EnteredBy_ID])
REFERENCES [dbo].[Common_UserDetail] ([Contact_ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Church_NPNCWelcomePacksIssued] CHECK CONSTRAINT [FK_Church_NPNCWelcomePacksIssued_Common_UserDetail]
GO
