SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Church_NPSummaryStats](
	[SummaryStats_ID] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NOT NULL,
	[Campus_ID] [money] NOT NULL,
	[InitialStatus_ID] [int] NOT NULL,
	[Number] [int] NOT NULL,
	[EnteredBy_ID] [int] NOT NULL,
	[DateEntered] [datetime] NOT NULL,
 CONSTRAINT [PK_Church_NPNCSummaryStats] PRIMARY KEY CLUSTERED 
(
	[SummaryStats_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Church_NPSummaryStats]  WITH CHECK ADD  CONSTRAINT [FK_Church_NPNCSummaryStats_Church_NPNCInitialStatus] FOREIGN KEY([InitialStatus_ID])
REFERENCES [dbo].[Church_NPNCInitialStatus] ([InitialStatus_ID])
GO
ALTER TABLE [dbo].[Church_NPSummaryStats] CHECK CONSTRAINT [FK_Church_NPNCSummaryStats_Church_NPNCInitialStatus]
GO
ALTER TABLE [dbo].[Church_NPSummaryStats]  WITH CHECK ADD  CONSTRAINT [FK_Church_NPNCSummaryStats_Common_UserDetail] FOREIGN KEY([EnteredBy_ID])
REFERENCES [dbo].[Common_UserDetail] ([Contact_ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Church_NPSummaryStats] CHECK CONSTRAINT [FK_Church_NPNCSummaryStats_Common_UserDetail]
GO
