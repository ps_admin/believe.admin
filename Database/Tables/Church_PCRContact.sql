SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Church_PCRContact](
	[PCRContact_ID] [int] IDENTITY(1,1) NOT NULL,
	[PCR_ID] [int] NOT NULL,
	[Contact_ID] [int] NOT NULL,
	[SundayAttendance] [bit] NOT NULL,
	[ServiceAttended_ID] [int] NULL,
	[ULGAttendance] [bit] NOT NULL,
	[BoomAttendance] [bit] NOT NULL,
	[FNLAttendance] [bit] NOT NULL,
	[Call] [int] NOT NULL,
	[Visit] [int] NOT NULL,
	[Other] [int] NOT NULL,
 CONSTRAINT [PK_Church_PCReportContact] PRIMARY KEY CLUSTERED 
(
	[PCRContact_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Church_PCRContact] ON [dbo].[Church_PCRContact] 
(
	[PCR_ID] ASC,
	[Contact_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Church_PCRContact]  WITH CHECK ADD  CONSTRAINT [FK_Church_PCRContact_Church_PCR] FOREIGN KEY([PCR_ID])
REFERENCES [dbo].[Church_PCR] ([PCR_ID])
GO
ALTER TABLE [dbo].[Church_PCRContact] CHECK CONSTRAINT [FK_Church_PCRContact_Church_PCR]
GO
ALTER TABLE [dbo].[Church_PCRContact]  WITH CHECK ADD  CONSTRAINT [FK_Church_PCRContact_Common_ContactInternal] FOREIGN KEY([Contact_ID])
REFERENCES [dbo].[Common_ContactInternal] ([Contact_ID])
GO
ALTER TABLE [dbo].[Church_PCRContact] CHECK CONSTRAINT [FK_Church_PCRContact_Common_ContactInternal]
GO
ALTER TABLE [dbo].[Church_PCRContact] ADD  CONSTRAINT [DF_Church_PCRContact_FNLAttendance]  DEFAULT ((0)) FOR [FNLAttendance]
GO
ALTER TABLE [dbo].[Church_PCRContact] ADD  CONSTRAINT [DF_Church_PCRContact_Other]  DEFAULT ((0)) FOR [Other]
GO
