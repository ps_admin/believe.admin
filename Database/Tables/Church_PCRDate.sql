SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Church_PCRDate](
	[PCRDate_ID] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NOT NULL,
	[ULG] [bit] NOT NULL,
	[Boom] [bit] NOT NULL,
	[FNL] [bit] NOT NULL,
	[ReportIsDue] [bit] NOT NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_Church_NPNCWeeklyReport] PRIMARY KEY CLUSTERED 
(
	[PCRDate_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Church_PCRDate] ADD  CONSTRAINT [DF_Church_PCRDate_FNL]  DEFAULT ((0)) FOR [FNL]
GO
ALTER TABLE [dbo].[Church_PCRDate] ADD  CONSTRAINT [DF_Church_PCRDate_SendLateReminderEmails]  DEFAULT ((1)) FOR [ReportIsDue]
GO
ALTER TABLE [dbo].[Church_PCRDate] ADD  CONSTRAINT [DF_Church_PCRDate_Deleted]  DEFAULT ((0)) FOR [Deleted]
GO
