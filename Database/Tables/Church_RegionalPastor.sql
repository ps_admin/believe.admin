SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Church_RegionalPastor](
	[RegionalPastor_ID] [int] IDENTITY(1,1) NOT NULL,
	[Campus_ID] [int] NOT NULL,
	[Ministry_ID] [int] NOT NULL,
	[Region_ID] [int] NULL,
	[Contact_ID] [int] NOT NULL,
 CONSTRAINT [PK_Church_RegionalPastors] PRIMARY KEY CLUSTERED 
(
	[RegionalPastor_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Church_RegionalPastor]  WITH CHECK ADD  CONSTRAINT [FK_Church_RegionalPastor_Church_Campus] FOREIGN KEY([Campus_ID])
REFERENCES [dbo].[Church_Campus] ([Campus_ID])
GO
ALTER TABLE [dbo].[Church_RegionalPastor] CHECK CONSTRAINT [FK_Church_RegionalPastor_Church_Campus]
GO
ALTER TABLE [dbo].[Church_RegionalPastor]  WITH CHECK ADD  CONSTRAINT [FK_Church_RegionalPastor_Church_Region] FOREIGN KEY([Region_ID])
REFERENCES [dbo].[Church_Region] ([Region_ID])
GO
ALTER TABLE [dbo].[Church_RegionalPastor] CHECK CONSTRAINT [FK_Church_RegionalPastor_Church_Region]
GO
