SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Church_RestrictedAccessRole](
	[RestrictedAccessRole_ID] [int] IDENTITY(1,1) NOT NULL,
	[ContactRole_ID] [int] NOT NULL,
	[Role_ID] [int] NOT NULL,
 CONSTRAINT [PK_Church_RestrictedAccessRole] PRIMARY KEY CLUSTERED 
(
	[RestrictedAccessRole_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Church_RestrictedAccessRole]  WITH CHECK ADD  CONSTRAINT [FK_Church_RestrictedAccessRole_Church_ContactRole] FOREIGN KEY([ContactRole_ID])
REFERENCES [dbo].[Church_ContactRole] ([ContactRole_ID])
GO
ALTER TABLE [dbo].[Church_RestrictedAccessRole] CHECK CONSTRAINT [FK_Church_RestrictedAccessRole_Church_ContactRole]
GO
ALTER TABLE [dbo].[Church_RestrictedAccessRole]  WITH CHECK ADD  CONSTRAINT [FK_Church_RestrictedAccessRole_Church_Role] FOREIGN KEY([Role_ID])
REFERENCES [dbo].[Church_Role] ([Role_ID])
GO
ALTER TABLE [dbo].[Church_RestrictedAccessRole] CHECK CONSTRAINT [FK_Church_RestrictedAccessRole_Church_Role]
GO
