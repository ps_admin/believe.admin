SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Church_RestrictedAccessULG](
	[RestrictedAccessULG_ID] [int] IDENTITY(1,1) NOT NULL,
	[ContactRole_ID] [int] NOT NULL,
	[ULG_ID] [int] NOT NULL,
 CONSTRAINT [PK_Church_RestrictedAccessULG] PRIMARY KEY CLUSTERED 
(
	[RestrictedAccessULG_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Church_RestrictedAccessULG]  WITH CHECK ADD  CONSTRAINT [FK_Church_RestrictedAccessULG_Church_ContactRole] FOREIGN KEY([ContactRole_ID])
REFERENCES [dbo].[Church_ContactRole] ([ContactRole_ID])
GO
ALTER TABLE [dbo].[Church_RestrictedAccessULG] CHECK CONSTRAINT [FK_Church_RestrictedAccessULG_Church_ContactRole]
GO
ALTER TABLE [dbo].[Church_RestrictedAccessULG]  WITH CHECK ADD  CONSTRAINT [FK_Church_RestrictedAccessULG_Church_ULG] FOREIGN KEY([ULG_ID])
REFERENCES [dbo].[Church_ULG] ([ULG_ID])
GO
ALTER TABLE [dbo].[Church_RestrictedAccessULG] CHECK CONSTRAINT [FK_Church_RestrictedAccessULG_Church_ULG]
GO
