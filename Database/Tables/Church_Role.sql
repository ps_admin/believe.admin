SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Church_Role](
	[Role_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[RoleType_ID] [int] NOT NULL,
	[RoleTypeUL_ID] [int] NOT NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_Church_Role] PRIMARY KEY CLUSTERED 
(
	[Role_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Church_Role]  WITH CHECK ADD  CONSTRAINT [FK_Church_Role_Church_RoleType] FOREIGN KEY([RoleType_ID])
REFERENCES [dbo].[Church_RoleType] ([RoleType_ID])
GO
ALTER TABLE [dbo].[Church_Role] CHECK CONSTRAINT [FK_Church_Role_Church_RoleType]
GO
ALTER TABLE [dbo].[Church_Role]  WITH CHECK ADD  CONSTRAINT [FK_Church_Role_Church_RoleTypeUL] FOREIGN KEY([RoleTypeUL_ID])
REFERENCES [dbo].[Church_RoleTypeUL] ([RoleTypeUL_ID])
GO
ALTER TABLE [dbo].[Church_Role] CHECK CONSTRAINT [FK_Church_Role_Church_RoleTypeUL]
GO
ALTER TABLE [dbo].[Church_Role] ADD  CONSTRAINT [DF_Church_Role_Deleted]  DEFAULT ((0)) FOR [Deleted]
GO
