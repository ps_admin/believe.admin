SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Church_RoomBookingOptions](
	[LimitedBookingsStart] [datetime] NOT NULL,
	[LimitedBookingsEnd] [datetime] NOT NULL
) ON [PRIMARY]
GO
