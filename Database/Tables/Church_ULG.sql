SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Church_ULG](
	[ULG_ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](20) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[GroupType] [nvarchar](50) NOT NULL,
	[Campus_ID] [int] NOT NULL,
	[Ministry_ID] [int] NOT NULL,
	[Region_ID] [int] NULL,
	[ActiveDate] [datetime] NOT NULL,
	[ULGDateTime_ID] [int] NULL,
	[ULGDateDay_ID] [int] NULL,
	[Address1] [nvarchar](80) NOT NULL,
	[Address2] [nvarchar](80) NOT NULL,
	[Suburb] [nvarchar](50) NOT NULL,
	[State_ID] [int] NOT NULL,
	[Postcode] [nvarchar](20) NOT NULL,
	[Country_ID] [int] NOT NULL,
	[Inactive] [bit] NOT NULL,
	[SortOrder] [int] NOT NULL,
 CONSTRAINT [PK_Church_ULG] PRIMARY KEY CLUSTERED 
(
	[ULG_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Church_ULG]  WITH CHECK ADD  CONSTRAINT [FK_Church_ULG_Church_ULGDateDay] FOREIGN KEY([ULGDateDay_ID])
REFERENCES [dbo].[Church_ULGDateDay] ([ULGDateDay_ID])
GO
ALTER TABLE [dbo].[Church_ULG] CHECK CONSTRAINT [FK_Church_ULG_Church_ULGDateDay]
GO
ALTER TABLE [dbo].[Church_ULG]  WITH CHECK ADD  CONSTRAINT [FK_Church_ULG_Church_ULGDateTime] FOREIGN KEY([ULGDateTime_ID])
REFERENCES [dbo].[Church_ULGDateTime] ([ULGDateTime_ID])
GO
ALTER TABLE [dbo].[Church_ULG] CHECK CONSTRAINT [FK_Church_ULG_Church_ULGDateTime]
GO
