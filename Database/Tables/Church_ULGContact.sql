SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Church_ULGContact](
	[ULGContact_ID] [int] IDENTITY(1,1) NOT NULL,
	[ULG_ID] [int] NOT NULL,
	[Contact_ID] [int] NOT NULL,
	[DateJoined] [datetime] NOT NULL,
	[Inactive] [bit] NOT NULL,
	[SortOrder] [int] NOT NULL,
	[GUID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_Church_ContactULG] PRIMARY KEY CLUSTERED 
(
	[ULGContact_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ULGContact_Inactive] ON [dbo].[Church_ULGContact] 
(
	[Inactive] ASC
)
INCLUDE ( [ULG_ID],
[Contact_ID]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ULGContact_ULGID_Inactive] ON [dbo].[Church_ULGContact] 
(
	[ULG_ID] ASC,
	[Inactive] ASC
)
INCLUDE ( [Contact_ID]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Church_ULGContact]  WITH CHECK ADD  CONSTRAINT [FK_Church_ContactULG_Common_ContactInternal] FOREIGN KEY([Contact_ID])
REFERENCES [dbo].[Common_ContactInternal] ([Contact_ID])
GO
ALTER TABLE [dbo].[Church_ULGContact] CHECK CONSTRAINT [FK_Church_ContactULG_Common_ContactInternal]
GO
ALTER TABLE [dbo].[Church_ULGContact]  WITH CHECK ADD  CONSTRAINT [FK_Church_ULGContact_Church_ULG] FOREIGN KEY([ULG_ID])
REFERENCES [dbo].[Church_ULG] ([ULG_ID])
GO
ALTER TABLE [dbo].[Church_ULGContact] CHECK CONSTRAINT [FK_Church_ULGContact_Church_ULG]
GO
