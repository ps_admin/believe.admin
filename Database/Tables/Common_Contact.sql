SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Common_Contact](
	[Contact_ID] [int] IDENTITY(1,1) NOT NULL,
	[Salutation_ID] [int] NOT NULL,
	[FirstName] [nvarchar](30) NOT NULL,
	[LastName] [nvarchar](30) NOT NULL,
	[Gender] [nvarchar](6) NOT NULL,
	[DateOfBirth] [datetime] NULL,
	[Address1] [nvarchar](100) NOT NULL,
	[Address2] [nvarchar](100) NOT NULL,
	[Suburb] [nvarchar](20) NOT NULL,
	[Postcode] [nvarchar](10) NOT NULL,
	[State_ID] [int] NULL,
	[StateOther] [nvarchar](20) NOT NULL,
	[Country_ID] [int] NULL,
	[Phone] [nvarchar](20) NOT NULL,
	[PhoneWork] [nvarchar](20) NOT NULL,
	[Fax] [nvarchar](20) NOT NULL,
	[Mobile] [nvarchar](20) NOT NULL,
	[Email] [nvarchar](80) NOT NULL,
	[Email2] [nvarchar](80) NOT NULL,
	[DoNotIncludeEmail1InMailingList] [bit] NOT NULL,
	[DoNotIncludeEmail2InMailingList] [bit] NOT NULL,
	[Family_ID] [int] NULL,
	[FamilyMemberType_ID] [int] NULL,
	[Password] [nvarchar](20) NOT NULL,
	[SecretQuestion_ID] [int] NULL,
	[SecretAnswer] [nvarchar](100) NULL,
	[VolunteerPoliceCheck] [nvarchar](20) NOT NULL,
	[VolunteerPoliceCheckBit] [bit] NOT NULL,
	[VolunteerPoliceCheckDate] [datetime] NULL,
	[VolunteerWWCC] [nvarchar](20) NOT NULL,
	[VolunteerWWCCDate] [datetime] NULL,
	[VolunteerWWCCType_ID] [int] NULL,
	[CreatedBy_ID] [int] NULL,
	[CreationDate] [datetime] NOT NULL,
	[ModifiedBy_ID] [int] NULL,
	[ModificationDate] [datetime] NULL,
	[DeDupeVerified] [bit] NOT NULL,
	[GUID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_Contact] PRIMARY KEY CLUSTERED 
(
	[Contact_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Common_Contact]  WITH CHECK ADD  CONSTRAINT [FK_Common_Contact_Common_FamilyMemberType] FOREIGN KEY([FamilyMemberType_ID])
REFERENCES [dbo].[Common_FamilyMemberType] ([FamilyMemberType_ID])
GO
ALTER TABLE [dbo].[Common_Contact] CHECK CONSTRAINT [FK_Common_Contact_Common_FamilyMemberType]
GO
ALTER TABLE [dbo].[Common_Contact]  WITH CHECK ADD  CONSTRAINT [FK_Common_Contact_Common_Salutation] FOREIGN KEY([Salutation_ID])
REFERENCES [dbo].[Common_Salutation] ([Salutation_ID])
GO
ALTER TABLE [dbo].[Common_Contact] CHECK CONSTRAINT [FK_Common_Contact_Common_Salutation]
GO
ALTER TABLE [dbo].[Common_Contact]  WITH CHECK ADD  CONSTRAINT [FK_Common_Contact_Common_SecretQuestion] FOREIGN KEY([SecretQuestion_ID])
REFERENCES [dbo].[Common_SecretQuestion] ([SecretQuestion_ID])
GO
ALTER TABLE [dbo].[Common_Contact] CHECK CONSTRAINT [FK_Common_Contact_Common_SecretQuestion]
GO
ALTER TABLE [dbo].[Common_Contact]  WITH CHECK ADD  CONSTRAINT [FK_Common_Contact_Common_UserDetail] FOREIGN KEY([CreatedBy_ID])
REFERENCES [dbo].[Common_UserDetail] ([Contact_ID])
GO
ALTER TABLE [dbo].[Common_Contact] CHECK CONSTRAINT [FK_Common_Contact_Common_UserDetail]
GO
ALTER TABLE [dbo].[Common_Contact]  WITH CHECK ADD  CONSTRAINT [FK_Common_Contact_Common_WWCCType] FOREIGN KEY([VolunteerWWCCType_ID])
REFERENCES [dbo].[Common_WWCCType] ([WWCCType_ID])
GO
ALTER TABLE [dbo].[Common_Contact] CHECK CONSTRAINT [FK_Common_Contact_Common_WWCCType]
GO
ALTER TABLE [dbo].[Common_Contact]  WITH NOCHECK ADD  CONSTRAINT [CK_Common_Contact_Email] CHECK  (([dbo].[Common_CheckDuplicateEmail]([Email])<=(1)))
GO
ALTER TABLE [dbo].[Common_Contact] CHECK CONSTRAINT [CK_Common_Contact_Email]
GO
ALTER TABLE [dbo].[Common_Contact] ADD  CONSTRAINT [DF_Common_Contact_Email2]  DEFAULT ('') FOR [Email2]
GO
ALTER TABLE [dbo].[Common_Contact] ADD  CONSTRAINT [DF_Common_Contact_DoNotIncludeEmail1InMailingList]  DEFAULT ((0)) FOR [DoNotIncludeEmail1InMailingList]
GO
ALTER TABLE [dbo].[Common_Contact] ADD  CONSTRAINT [DF_Common_Contact_DoNotIncludeEmail2InMailingList]  DEFAULT ((0)) FOR [DoNotIncludeEmail2InMailingList]
GO
ALTER TABLE [dbo].[Common_Contact] ADD  CONSTRAINT [DF_Common_Contact_VolunteerPoliceCheck]  DEFAULT ('') FOR [VolunteerPoliceCheck]
GO
ALTER TABLE [dbo].[Common_Contact] ADD  CONSTRAINT [DF_Contact_VolunteerPoliceCheck]  DEFAULT ((0)) FOR [VolunteerPoliceCheckBit]
GO
ALTER TABLE [dbo].[Common_Contact] ADD  CONSTRAINT [DF_Contact_VolunteerWWCC]  DEFAULT ((0)) FOR [VolunteerWWCC]
GO
ALTER TABLE [dbo].[Common_Contact] ADD  CONSTRAINT [DF_Common_Contact_DeDupeVerified]  DEFAULT ((0)) FOR [DeDupeVerified]
GO
ALTER TABLE [dbo].[Common_Contact] ADD  CONSTRAINT [DF_Contact_GUID]  DEFAULT (newid()) FOR [GUID]
GO
