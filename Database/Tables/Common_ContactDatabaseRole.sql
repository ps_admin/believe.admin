SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Common_ContactDatabaseRole](
	[ContactDatabaseRole_ID] [int] IDENTITY(1,1) NOT NULL,
	[Contact_ID] [int] NOT NULL,
	[DatabaseRole_ID] [int] NOT NULL,
	[DateAdded] [datetime] NOT NULL,
 CONSTRAINT [PK_Common_ContactDatabaseRole] PRIMARY KEY CLUSTERED 
(
	[ContactDatabaseRole_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Common_ContactDatabaseRole]  WITH CHECK ADD  CONSTRAINT [FK_Common_ContactDatabaseRole_Common_DatabaseRole] FOREIGN KEY([DatabaseRole_ID])
REFERENCES [dbo].[Common_DatabaseRole] ([DatabaseRole_ID])
GO
ALTER TABLE [dbo].[Common_ContactDatabaseRole] CHECK CONSTRAINT [FK_Common_ContactDatabaseRole_Common_DatabaseRole]
GO
ALTER TABLE [dbo].[Common_ContactDatabaseRole]  WITH CHECK ADD  CONSTRAINT [FK_Common_ContactDatabaseRole_Common_UserDetail] FOREIGN KEY([Contact_ID])
REFERENCES [dbo].[Common_UserDetail] ([Contact_ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Common_ContactDatabaseRole] CHECK CONSTRAINT [FK_Common_ContactDatabaseRole_Common_UserDetail]
GO
