SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Common_ContactExternal](
	[Contact_ID] [int] NOT NULL,
	[ChurchName] [nvarchar](50) NOT NULL,
	[Denomination_ID] [int] NOT NULL,
	[SeniorPastorName] [nvarchar](50) NOT NULL,
	[ChurchAddress1] [nvarchar](100) NOT NULL,
	[ChurchAddress2] [nvarchar](100) NOT NULL,
	[ChurchSuburb] [nvarchar](20) NOT NULL,
	[ChurchPostcode] [nvarchar](10) NOT NULL,
	[ChurchState_ID] [int] NULL,
	[ChurchStateOther] [nvarchar](20) NOT NULL,
	[ChurchCountry_ID] [int] NULL,
	[ChurchWebsite] [nvarchar](50) NOT NULL,
	[LeaderName] [nvarchar](60) NOT NULL,
	[ChurchNumberOfPeople] [nvarchar](10) NOT NULL,
	[ChurchPhone] [nvarchar](20) NOT NULL,
	[ChurchEmail] [nvarchar](100) NOT NULL,
	[ContactDetailsVerified] [bit] NOT NULL,
	[Deleted] [bit] NOT NULL,
	[Inactive] [bit] NOT NULL,
 CONSTRAINT [PK_Common_ContactExternal] PRIMARY KEY CLUSTERED 
(
	[Contact_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Common_ContactExternal]  WITH CHECK ADD  CONSTRAINT [FK_Common_ContactExternal_Common_Denomination] FOREIGN KEY([Denomination_ID])
REFERENCES [dbo].[Common_Denomination] ([Denomination_ID])
GO
ALTER TABLE [dbo].[Common_ContactExternal] CHECK CONSTRAINT [FK_Common_ContactExternal_Common_Denomination]
GO
ALTER TABLE [dbo].[Common_ContactExternal]  WITH CHECK ADD  CONSTRAINT [FK_Common_ContactExternal_Common_GeneralCountry] FOREIGN KEY([ChurchCountry_ID])
REFERENCES [dbo].[Common_GeneralCountry] ([Country_ID])
GO
ALTER TABLE [dbo].[Common_ContactExternal] CHECK CONSTRAINT [FK_Common_ContactExternal_Common_GeneralCountry]
GO
ALTER TABLE [dbo].[Common_ContactExternal]  WITH CHECK ADD  CONSTRAINT [FK_Common_ContactExternal_Common_GeneralState] FOREIGN KEY([ChurchState_ID])
REFERENCES [dbo].[Common_GeneralState] ([State_ID])
GO
ALTER TABLE [dbo].[Common_ContactExternal] CHECK CONSTRAINT [FK_Common_ContactExternal_Common_GeneralState]
GO
ALTER TABLE [dbo].[Common_ContactExternal]  WITH CHECK ADD  CONSTRAINT [FK_ContactExternal_Contact] FOREIGN KEY([Contact_ID])
REFERENCES [dbo].[Common_Contact] ([Contact_ID])
GO
ALTER TABLE [dbo].[Common_ContactExternal] CHECK CONSTRAINT [FK_ContactExternal_Contact]
GO
ALTER TABLE [dbo].[Common_ContactExternal] ADD  CONSTRAINT [DF_Common_ContactExternal_ChurchName]  DEFAULT ('') FOR [ChurchName]
GO
ALTER TABLE [dbo].[Common_ContactExternal] ADD  CONSTRAINT [DF_ContactExternal_SeniorPastorName]  DEFAULT ('') FOR [SeniorPastorName]
GO
ALTER TABLE [dbo].[Common_ContactExternal] ADD  CONSTRAINT [DF_ContactExternal_ChurchAddress1]  DEFAULT ('') FOR [ChurchAddress1]
GO
ALTER TABLE [dbo].[Common_ContactExternal] ADD  CONSTRAINT [DF_ContactExternal_ChurchAddress2]  DEFAULT ('') FOR [ChurchAddress2]
GO
ALTER TABLE [dbo].[Common_ContactExternal] ADD  CONSTRAINT [DF_ContactExternal_ChurchSuburb]  DEFAULT ('') FOR [ChurchSuburb]
GO
ALTER TABLE [dbo].[Common_ContactExternal] ADD  CONSTRAINT [DF_ContactExternal_ChurchPostcode]  DEFAULT ('') FOR [ChurchPostcode]
GO
ALTER TABLE [dbo].[Common_ContactExternal] ADD  CONSTRAINT [DF_ContactExternal_ChurchStateOther]  DEFAULT ('') FOR [ChurchStateOther]
GO
ALTER TABLE [dbo].[Common_ContactExternal] ADD  CONSTRAINT [DF_ContactExternal_ChurchWebsite]  DEFAULT ('') FOR [ChurchWebsite]
GO
ALTER TABLE [dbo].[Common_ContactExternal] ADD  CONSTRAINT [DF_Common_ContactExternal_LeaderName]  DEFAULT ('') FOR [LeaderName]
GO
ALTER TABLE [dbo].[Common_ContactExternal] ADD  CONSTRAINT [DF_ContactExternal_ChurchNumberOfPeople]  DEFAULT ('') FOR [ChurchNumberOfPeople]
GO
ALTER TABLE [dbo].[Common_ContactExternal] ADD  CONSTRAINT [DF_ContactExternal_ChurchPhone]  DEFAULT ('') FOR [ChurchPhone]
GO
ALTER TABLE [dbo].[Common_ContactExternal] ADD  CONSTRAINT [DF_ContactExternal_ChurchEmail]  DEFAULT ('') FOR [ChurchEmail]
GO
ALTER TABLE [dbo].[Common_ContactExternal] ADD  CONSTRAINT [DF_Common_ContactExternal_ContactDetailsVerified]  DEFAULT ((0)) FOR [ContactDetailsVerified]
GO
ALTER TABLE [dbo].[Common_ContactExternal] ADD  CONSTRAINT [DF_Common_ContactExternal_Deleted]  DEFAULT ((0)) FOR [Deleted]
GO
ALTER TABLE [dbo].[Common_ContactExternal] ADD  CONSTRAINT [DF_ContactExternal_Inactive]  DEFAULT ((0)) FOR [Inactive]
GO
