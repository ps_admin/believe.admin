SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Common_ContactExternalEvents](
	[Contact_ID] [int] NOT NULL,
	[EmergencyContactName] [nvarchar](50) NOT NULL,
	[EmergencyContactPhone] [nvarchar](20) NOT NULL,
	[EmergencyContactRelationship] [nvarchar](30) NOT NULL,
	[MedicalInformation] [nvarchar](200) NOT NULL,
	[MedicalAllergies] [nvarchar](200) NOT NULL,
	[AccessibilityInformation] [nvarchar](max) NOT NULL,
	[EventComments] [nvarchar](500) NOT NULL,
 CONSTRAINT [PK_Common_ContactExternalEvents] PRIMARY KEY CLUSTERED 
(
	[Contact_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Common_ContactExternalEvents]  WITH CHECK ADD  CONSTRAINT [FK_Common_ContactExternalEvents_Common_ContactExternal] FOREIGN KEY([Contact_ID])
REFERENCES [dbo].[Common_ContactExternal] ([Contact_ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Common_ContactExternalEvents] CHECK CONSTRAINT [FK_Common_ContactExternalEvents_Common_ContactExternal]
GO
ALTER TABLE [dbo].[Common_ContactExternalEvents] ADD  CONSTRAINT [DF_Common_ContactExternalEvents_EmergencyContactName]  DEFAULT ('') FOR [EmergencyContactName]
GO
ALTER TABLE [dbo].[Common_ContactExternalEvents] ADD  CONSTRAINT [DF_Common_ContactExternalEvents_EmergencyContactPhone]  DEFAULT ('') FOR [EmergencyContactPhone]
GO
ALTER TABLE [dbo].[Common_ContactExternalEvents] ADD  CONSTRAINT [DF_Common_ContactExternalEvents_EmergencyContactRelationship]  DEFAULT ('') FOR [EmergencyContactRelationship]
GO
ALTER TABLE [dbo].[Common_ContactExternalEvents] ADD  CONSTRAINT [DF_Common_ContactExternalEvents_MedicalInformation]  DEFAULT ('') FOR [MedicalInformation]
GO
ALTER TABLE [dbo].[Common_ContactExternalEvents] ADD  CONSTRAINT [DF_Common_ContactExternalEvents_MedicalAllergies]  DEFAULT ('') FOR [MedicalAllergies]
GO
ALTER TABLE [dbo].[Common_ContactExternalEvents] ADD  CONSTRAINT [DF_Common_ContactExternalEvents_AccessibilityInformation]  DEFAULT ('') FOR [AccessibilityInformation]
GO
ALTER TABLE [dbo].[Common_ContactExternalEvents] ADD  CONSTRAINT [DF_Common_ContactExternalEvents_EventComments]  DEFAULT ('') FOR [EventComments]
GO
