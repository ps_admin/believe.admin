SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Common_ContactExternalRevolution](
	[Contact_ID] [int] NOT NULL,
	[ReferredBy_ID] [int] NOT NULL,
	[AcceptedTermsAndConditions] [bit] NOT NULL,
 CONSTRAINT [PK_Common_ContactExternalRevolution] PRIMARY KEY CLUSTERED 
(
	[Contact_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Common_ContactExternalRevolution]  WITH CHECK ADD  CONSTRAINT [FK_Common_ContactExternalRevolution_Common_Contact] FOREIGN KEY([Contact_ID])
REFERENCES [dbo].[Common_Contact] ([Contact_ID])
GO
ALTER TABLE [dbo].[Common_ContactExternalRevolution] CHECK CONSTRAINT [FK_Common_ContactExternalRevolution_Common_Contact]
GO
ALTER TABLE [dbo].[Common_ContactExternalRevolution]  WITH CHECK ADD  CONSTRAINT [FK_Common_ContactExternalRevolution_Revolution_ReferredBy] FOREIGN KEY([ReferredBy_ID])
REFERENCES [dbo].[Revolution_ReferredBy] ([ReferredBy_ID])
GO
ALTER TABLE [dbo].[Common_ContactExternalRevolution] CHECK CONSTRAINT [FK_Common_ContactExternalRevolution_Revolution_ReferredBy]
GO
