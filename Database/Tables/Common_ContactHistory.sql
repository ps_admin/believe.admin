SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Common_ContactHistory](
	[CH_ID] [int] IDENTITY(1,1) NOT NULL,
	[Contact_ID] [int] NOT NULL,
	[Module] [nvarchar](50) NOT NULL,
	[Desc] [nvarchar](50) NOT NULL,
	[Desc2] [nvarchar](50) NULL,
	[DateChanged] [datetime] NOT NULL,
	[User_ID] [int] NOT NULL,
	[Action] [nvarchar](20) NOT NULL,
	[Item] [nvarchar](200) NOT NULL,
	[Table] [nvarchar](50) NOT NULL,
	[FieldName] [nvarchar](200) NOT NULL,
	[OldValue] [nvarchar](max) NULL,
	[OldText] [nvarchar](max) NOT NULL,
	[NewValue] [nvarchar](max) NULL,
	[NewText] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_ContactHistory] PRIMARY KEY CLUSTERED 
(
	[CH_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Common_ContactHistory]  WITH CHECK ADD  CONSTRAINT [FK_Common_ContactHistory_Common_Contact] FOREIGN KEY([Contact_ID])
REFERENCES [dbo].[Common_Contact] ([Contact_ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Common_ContactHistory] CHECK CONSTRAINT [FK_Common_ContactHistory_Common_Contact]
GO
ALTER TABLE [dbo].[Common_ContactHistory]  WITH CHECK ADD  CONSTRAINT [FK_Common_ContactHistory_Common_UserDetail] FOREIGN KEY([User_ID])
REFERENCES [dbo].[Common_UserDetail] ([Contact_ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Common_ContactHistory] CHECK CONSTRAINT [FK_Common_ContactHistory_Common_UserDetail]
GO
