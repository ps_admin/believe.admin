SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Common_ContactInternal](
	[Contact_ID] [int] NOT NULL,
	[ChurchStatus_ID] [int] NOT NULL,
	[Campus_ID] [int] NULL,
	[Ministry_ID] [int] NULL,
	[Region_ID] [int] NULL,
	[PostalAddress1] [nvarchar](100) NOT NULL,
	[PostalAddress2] [nvarchar](100) NOT NULL,
	[PostalSuburb] [nvarchar](20) NOT NULL,
	[PostalPostcode] [nvarchar](50) NOT NULL,
	[PostalState_ID] [int] NULL,
	[PostalStateOther] [nvarchar](20) NOT NULL,
	[PostalCountry_ID] [int] NULL,
	[Photo] [image] NULL,
	[EntryPoint_ID] [int] NULL,
	[DateJoinedChurch] [datetime] NULL,
	[CovenantFormDate] [datetime] NULL,
	[SalvationDate] [datetime] NULL,
	[WaterBaptismDate] [datetime] NULL,
	[VolunteerFormDate] [datetime] NULL,
	[PrimaryCarer] [nvarchar](50) NOT NULL,
	[CountryOfOrigin_ID] [int] NULL,
	[PrivacyNumber] [nvarchar](50) NOT NULL,
	[PrimarySchool] [nvarchar](100) NOT NULL,
	[SchoolGrade_ID] [int] NULL,
	[HighSchool] [nvarchar](100) NOT NULL,
	[University] [nvarchar](100) NOT NULL,
	[InformationIsConfidential] [bit] NOT NULL,
	[FacebookAddress] [nvarchar](100) NOT NULL,
	[Education_ID] [int] NULL,
	[ModeOfTransport_ID] [int] NULL,
	[CarParkUsed_ID] [int] NULL,
	[ServiceAttending_ID] [int] NULL,
	[Occupation] [nvarchar](100) NOT NULL,
	[Employer] [nvarchar](50) NOT NULL,
	[EmploymentStatus_ID] [int] NULL,
	[EmploymentPosition_ID] [int] NULL,
	[EmploymentIndustry_ID] [int] NULL,
	[KidsMinistryComments] [nvarchar](max) NOT NULL,
	[KidsMinistryGroup] [nvarchar](50) NOT NULL,
	[Deleted] [bit] NOT NULL,
	[DeleteReason_ID] [int] NULL,
 CONSTRAINT [PK_Common_ContactInternal] PRIMARY KEY CLUSTERED 
(
	[Contact_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_ContactInternal_InformationIsConfidential] ON [dbo].[Common_ContactInternal] 
(
	[InformationIsConfidential] ASC
)
INCLUDE ( [Contact_ID],
[Campus_ID]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Common_ContactInternal]  WITH CHECK ADD  CONSTRAINT [FK_Common_ContactInternal_Church_Campus] FOREIGN KEY([Campus_ID])
REFERENCES [dbo].[Church_Campus] ([Campus_ID])
GO
ALTER TABLE [dbo].[Common_ContactInternal] CHECK CONSTRAINT [FK_Common_ContactInternal_Church_Campus]
GO
ALTER TABLE [dbo].[Common_ContactInternal]  WITH CHECK ADD  CONSTRAINT [FK_Common_ContactInternal_Church_CarParkUsed] FOREIGN KEY([CarParkUsed_ID])
REFERENCES [dbo].[Church_CarParkUsed] ([CarParkUsed_ID])
GO
ALTER TABLE [dbo].[Common_ContactInternal] CHECK CONSTRAINT [FK_Common_ContactInternal_Church_CarParkUsed]
GO
ALTER TABLE [dbo].[Common_ContactInternal]  WITH CHECK ADD  CONSTRAINT [FK_Common_ContactInternal_Church_DeleteReason] FOREIGN KEY([DeleteReason_ID])
REFERENCES [dbo].[Church_DeleteReason] ([DeleteReason_ID])
GO
ALTER TABLE [dbo].[Common_ContactInternal] CHECK CONSTRAINT [FK_Common_ContactInternal_Church_DeleteReason]
GO
ALTER TABLE [dbo].[Common_ContactInternal]  WITH CHECK ADD  CONSTRAINT [FK_Common_ContactInternal_Church_Education] FOREIGN KEY([Education_ID])
REFERENCES [dbo].[Church_Education] ([Education_ID])
GO
ALTER TABLE [dbo].[Common_ContactInternal] CHECK CONSTRAINT [FK_Common_ContactInternal_Church_Education]
GO
ALTER TABLE [dbo].[Common_ContactInternal]  WITH CHECK ADD  CONSTRAINT [FK_Common_ContactInternal_Church_EmploymentIndustry] FOREIGN KEY([EmploymentIndustry_ID])
REFERENCES [dbo].[Church_EmploymentIndustry] ([EmploymentIndustry_ID])
GO
ALTER TABLE [dbo].[Common_ContactInternal] CHECK CONSTRAINT [FK_Common_ContactInternal_Church_EmploymentIndustry]
GO
ALTER TABLE [dbo].[Common_ContactInternal]  WITH CHECK ADD  CONSTRAINT [FK_Common_ContactInternal_Church_EmploymentPosition] FOREIGN KEY([EmploymentPosition_ID])
REFERENCES [dbo].[Church_EmploymentPosition] ([EmploymentPosition_ID])
GO
ALTER TABLE [dbo].[Common_ContactInternal] CHECK CONSTRAINT [FK_Common_ContactInternal_Church_EmploymentPosition]
GO
ALTER TABLE [dbo].[Common_ContactInternal]  WITH CHECK ADD  CONSTRAINT [FK_Common_ContactInternal_Church_EmploymentStatus] FOREIGN KEY([EmploymentStatus_ID])
REFERENCES [dbo].[Church_EmploymentStatus] ([EmploymentStatus_ID])
GO
ALTER TABLE [dbo].[Common_ContactInternal] CHECK CONSTRAINT [FK_Common_ContactInternal_Church_EmploymentStatus]
GO
ALTER TABLE [dbo].[Common_ContactInternal]  WITH CHECK ADD  CONSTRAINT [FK_Common_ContactInternal_Church_EntryPoint] FOREIGN KEY([EntryPoint_ID])
REFERENCES [dbo].[Church_EntryPoint] ([EntryPoint_ID])
GO
ALTER TABLE [dbo].[Common_ContactInternal] CHECK CONSTRAINT [FK_Common_ContactInternal_Church_EntryPoint]
GO
ALTER TABLE [dbo].[Common_ContactInternal]  WITH CHECK ADD  CONSTRAINT [FK_Common_ContactInternal_Church_Ministry] FOREIGN KEY([Ministry_ID])
REFERENCES [dbo].[Church_Ministry] ([Ministry_ID])
GO
ALTER TABLE [dbo].[Common_ContactInternal] CHECK CONSTRAINT [FK_Common_ContactInternal_Church_Ministry]
GO
ALTER TABLE [dbo].[Common_ContactInternal]  WITH CHECK ADD  CONSTRAINT [FK_Common_ContactInternal_Church_ModeOfTransport] FOREIGN KEY([ModeOfTransport_ID])
REFERENCES [dbo].[Church_ModeOfTransport] ([ModeOfTransport_ID])
GO
ALTER TABLE [dbo].[Common_ContactInternal] CHECK CONSTRAINT [FK_Common_ContactInternal_Church_ModeOfTransport]
GO
ALTER TABLE [dbo].[Common_ContactInternal]  WITH CHECK ADD  CONSTRAINT [FK_Common_ContactInternal_Church_Region] FOREIGN KEY([Region_ID])
REFERENCES [dbo].[Church_Region] ([Region_ID])
GO
ALTER TABLE [dbo].[Common_ContactInternal] CHECK CONSTRAINT [FK_Common_ContactInternal_Church_Region]
GO
ALTER TABLE [dbo].[Common_ContactInternal]  WITH CHECK ADD  CONSTRAINT [FK_Common_ContactInternal_Common_Contact] FOREIGN KEY([Contact_ID])
REFERENCES [dbo].[Common_Contact] ([Contact_ID])
GO
ALTER TABLE [dbo].[Common_ContactInternal] CHECK CONSTRAINT [FK_Common_ContactInternal_Common_Contact]
GO
ALTER TABLE [dbo].[Common_ContactInternal] ADD  CONSTRAINT [DF_Common_ContactInternal_Campus_ID]  DEFAULT ((1)) FOR [Campus_ID]
GO
ALTER TABLE [dbo].[Common_ContactInternal] ADD  CONSTRAINT [DF_Common_ContactInternal_PrimaryCarer]  DEFAULT ('') FOR [PrimaryCarer]
GO
ALTER TABLE [dbo].[Common_ContactInternal] ADD  CONSTRAINT [DF_Common_ContactInternal_PrivacyNumber]  DEFAULT ('') FOR [PrivacyNumber]
GO
ALTER TABLE [dbo].[Common_ContactInternal] ADD  CONSTRAINT [DF_Common_ContactInternal_PrimarySchool]  DEFAULT ('') FOR [PrimarySchool]
GO
ALTER TABLE [dbo].[Common_ContactInternal] ADD  CONSTRAINT [DF_Common_ContactInternal_FacebookAddress]  DEFAULT ('') FOR [FacebookAddress]
GO
ALTER TABLE [dbo].[Common_ContactInternal] ADD  CONSTRAINT [DF_Common_ContactInternal_PlanetKidsComments]  DEFAULT ('') FOR [KidsMinistryComments]
GO
ALTER TABLE [dbo].[Common_ContactInternal] ADD  CONSTRAINT [DF_Common_ContactInternal_KidsMinistryGroup]  DEFAULT ('') FOR [KidsMinistryGroup]
GO
