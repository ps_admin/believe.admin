SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Common_ContactMailingList](
	[ContactMailingList_ID] [int] IDENTITY(1,1) NOT NULL,
	[Contact_ID] [int] NOT NULL,
	[MailingList_ID] [int] NOT NULL,
	[SubscriptionActive] [bit] NOT NULL,
 CONSTRAINT [PK_Common_ContactMailingList] PRIMARY KEY CLUSTERED 
(
	[ContactMailingList_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [KEY_Common_ContactMailingList] ON [dbo].[Common_ContactMailingList] 
(
	[Contact_ID] ASC,
	[MailingList_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Common_ContactMailingList]  WITH CHECK ADD  CONSTRAINT [FK_Common_ContactMailingList_Common_Contact] FOREIGN KEY([Contact_ID])
REFERENCES [dbo].[Common_Contact] ([Contact_ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Common_ContactMailingList] CHECK CONSTRAINT [FK_Common_ContactMailingList_Common_Contact]
GO
ALTER TABLE [dbo].[Common_ContactMailingList]  WITH CHECK ADD  CONSTRAINT [FK_Common_ContactMailingList_Common_MailingList] FOREIGN KEY([MailingList_ID])
REFERENCES [dbo].[Common_MailingList] ([MailingList_ID])
GO
ALTER TABLE [dbo].[Common_ContactMailingList] CHECK CONSTRAINT [FK_Common_ContactMailingList_Common_MailingList]
GO
