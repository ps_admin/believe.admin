SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Common_ContactMailingListType](
	[ContactMailingListType_ID] [int] IDENTITY(1,1) NOT NULL,
	[Contact_ID] [int] NOT NULL,
	[MailingListType_ID] [int] NOT NULL,
	[SubscriptionActive] [bit] NOT NULL,
 CONSTRAINT [PK_Common_ContactMailingListType] PRIMARY KEY CLUSTERED 
(
	[ContactMailingListType_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [KEY_Common_ContactMailingListType] ON [dbo].[Common_ContactMailingListType] 
(
	[Contact_ID] ASC,
	[MailingListType_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Common_ContactMailingListType]  WITH CHECK ADD  CONSTRAINT [FK_Common_ContactMailingListType_Common_Contact] FOREIGN KEY([Contact_ID])
REFERENCES [dbo].[Common_Contact] ([Contact_ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Common_ContactMailingListType] CHECK CONSTRAINT [FK_Common_ContactMailingListType_Common_Contact]
GO
ALTER TABLE [dbo].[Common_ContactMailingListType]  WITH CHECK ADD  CONSTRAINT [FK_Common_ContactMailingListType_Common_MailingListType] FOREIGN KEY([MailingListType_ID])
REFERENCES [dbo].[Common_MailingListType] ([MailingListType_ID])
GO
ALTER TABLE [dbo].[Common_ContactMailingListType] CHECK CONSTRAINT [FK_Common_ContactMailingListType_Common_MailingListType]
GO
