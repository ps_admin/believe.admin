SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Common_ContactOldPassword](
	[OldPassword_ID] [int] IDENTITY(1,1) NOT NULL,
	[Contact_ID] [int] NOT NULL,
	[DateChanged] [datetime] NOT NULL,
	[Password] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK_Common_ContactOldPassword] PRIMARY KEY CLUSTERED 
(
	[OldPassword_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Common_ContactOldPassword]  WITH CHECK ADD  CONSTRAINT [FK_Common_ContactOldPassword_Common_Contact] FOREIGN KEY([Contact_ID])
REFERENCES [dbo].[Common_Contact] ([Contact_ID])
GO
ALTER TABLE [dbo].[Common_ContactOldPassword] CHECK CONSTRAINT [FK_Common_ContactOldPassword_Common_Contact]
GO
