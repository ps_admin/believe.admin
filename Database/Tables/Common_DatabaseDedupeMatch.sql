SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Common_DatabaseDedupeMatch](
	[DatabaseDedupeMatch_ID] [int] IDENTITY(1,1) NOT NULL,
	[Match_ID] [int] NOT NULL,
	[Contact_ID] [int] NOT NULL,
	[Processed] [bit] NOT NULL,
 CONSTRAINT [PK_DatabaseDedupeMatch] PRIMARY KEY CLUSTERED 
(
	[DatabaseDedupeMatch_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Common_DatabaseDedupeMatch] ADD  CONSTRAINT [DF_Common_DatabaseDedupeMatch_Processed]  DEFAULT ((0)) FOR [Processed]
GO
