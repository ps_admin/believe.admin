SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Common_DatabaseInstance](
	[DatabaseInstance] [nvarchar](10) NOT NULL,
	[DefaultEmailAddress] [nvarchar](50) NOT NULL,
	[TimeDifference] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Common_DatabaseInstance] ADD  CONSTRAINT [DF_Common_DatabaseInstance_DefaultEmailAddress]  DEFAULT ('') FOR [DefaultEmailAddress]
GO
ALTER TABLE [dbo].[Common_DatabaseInstance] ADD  CONSTRAINT [DF_Common_DatabaseInstance_TimeDifference]  DEFAULT ((0)) FOR [TimeDifference]
GO
