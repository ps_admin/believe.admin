SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Common_DatabaseObject](
	[DatabaseObject_ID] [int] IDENTITY(37,1) NOT NULL,
	[Module] [nvarchar](10) NOT NULL,
	[ObjectType] [int] NOT NULL,
	[DatabaseObjectName] [nvarchar](50) NOT NULL,
	[DatabaseObjectText] [nvarchar](100) NOT NULL,
	[DatabaseObjectDesc] [nvarchar](100) NOT NULL,
	[ParentObject_ID] [int] NULL,
	[Order] [int] NOT NULL,
 CONSTRAINT [PK_Object] PRIMARY KEY NONCLUSTERED 
(
	[DatabaseObject_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [IX_Common_DatabaseObject] ON [dbo].[Common_DatabaseObject] 
(
	[Module] ASC,
	[Order] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Common_DatabaseObject] ADD  CONSTRAINT [DF_Common_DatabaseObject_M]  DEFAULT ((1)) FOR [Module]
GO
ALTER TABLE [dbo].[Common_DatabaseObject] ADD  CONSTRAINT [DF_Common_DatabaseObject_ObjectType]  DEFAULT ((1)) FOR [ObjectType]
GO
