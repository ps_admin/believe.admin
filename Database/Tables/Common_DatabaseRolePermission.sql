SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Common_DatabaseRolePermission](
	[DatabaseRolePermission_ID] [int] IDENTITY(350,1) NOT NULL,
	[DatabaseRole_ID] [int] NOT NULL,
	[DatabaseObject_ID] [int] NOT NULL,
	[Read] [bit] NOT NULL,
	[Write] [bit] NOT NULL,
	[ViewState] [bit] NOT NULL,
	[ViewCountry] [bit] NOT NULL,
 CONSTRAINT [PK_DatabaseRolePermission] PRIMARY KEY CLUSTERED 
(
	[DatabaseRolePermission_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Common_DatabaseRolePermission]  WITH CHECK ADD  CONSTRAINT [FK_DatabaseRolePermission_DatabaseObject] FOREIGN KEY([DatabaseObject_ID])
REFERENCES [dbo].[Common_DatabaseObject] ([DatabaseObject_ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Common_DatabaseRolePermission] CHECK CONSTRAINT [FK_DatabaseRolePermission_DatabaseObject]
GO
ALTER TABLE [dbo].[Common_DatabaseRolePermission]  WITH CHECK ADD  CONSTRAINT [FK_DatabaseRolePermission_DatabaseRole] FOREIGN KEY([DatabaseRole_ID])
REFERENCES [dbo].[Common_DatabaseRole] ([DatabaseRole_ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Common_DatabaseRolePermission] CHECK CONSTRAINT [FK_DatabaseRolePermission_DatabaseRole]
GO
