SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Common_GeneralCountry](
	[Country_ID] [int] IDENTITY(241,1) NOT NULL,
	[Country] [varchar](50) NOT NULL,
	[Currency] [varchar](3) NOT NULL,
	[Numeric] [char](3) NOT NULL,
	[Alpha-3] [char](3) NOT NULL,
	[Alpha-2] [char](2) NOT NULL,
	[SortOrder] [int] NOT NULL,
 CONSTRAINT [PK_GeneralCountry] PRIMARY KEY CLUSTERED 
(
	[Country_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Common_GeneralCountry] ADD  CONSTRAINT [DF_GeneralCountry_Currency]  DEFAULT ('') FOR [Currency]
GO
