SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Common_GeneralState](
	[State_ID] [int] IDENTITY(10,1) NOT NULL,
	[State] [varchar](10) NOT NULL,
	[FullName] [varchar](50) NOT NULL,
	[Country_ID] [int] NOT NULL,
	[SortOrder] [int] NOT NULL,
 CONSTRAINT [PK_GeneralState] PRIMARY KEY CLUSTERED 
(
	[State_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Common_GeneralState]  WITH CHECK ADD  CONSTRAINT [FK_GeneralState_GeneralCountry] FOREIGN KEY([Country_ID])
REFERENCES [dbo].[Common_GeneralCountry] ([Country_ID])
GO
ALTER TABLE [dbo].[Common_GeneralState] CHECK CONSTRAINT [FK_GeneralState_GeneralCountry]
GO
