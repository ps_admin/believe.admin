SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Common_MailingList](
	[MailingList_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](30) NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[Database] [nvarchar](50) NOT NULL,
	[TickedByDefault] [bit] NOT NULL,
	[TickedForCampus_ID] [int] NULL,
	[SortOrder] [int] NOT NULL,
 CONSTRAINT [PK_MailingList] PRIMARY KEY CLUSTERED 
(
	[MailingList_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Common_MailingList] ADD  CONSTRAINT [DF_Common_MailingList_Module]  DEFAULT (N'''') FOR [Database]
GO
ALTER TABLE [dbo].[Common_MailingList] ADD  CONSTRAINT [DF_Common_MailingList_SortOrder]  DEFAULT ((1)) FOR [SortOrder]
GO
