SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Common_SchoolGrade](
	[SchoolGrade_ID] [int] IDENTITY(1,1) NOT NULL,
	[Grade] [nvarchar](50) NOT NULL,
	[ShortGrade] [nvarchar](10) NOT NULL,
	[SortOrder] [int] NOT NULL,
 CONSTRAINT [PK_SchoolGrade] PRIMARY KEY CLUSTERED 
(
	[SchoolGrade_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Common_SchoolGrade] ADD  CONSTRAINT [DF_Common_SchoolGrade_ShortGrade]  DEFAULT ('') FOR [ShortGrade]
GO
