SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Common_UserDetail](
	[Contact_ID] [int] NOT NULL,
	[LastPasswordChangeDate] [datetime] NULL,
	[InvalidPasswordAttempts] [int] NOT NULL,
	[PasswordResetRequired] [bit] NOT NULL,
	[Inactive] [bit] NOT NULL,
 CONSTRAINT [PK_Common_UserDetail] PRIMARY KEY CLUSTERED 
(
	[Contact_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Common_UserDetail]  WITH CHECK ADD  CONSTRAINT [FK_Common_UserDetail_Common_Contact] FOREIGN KEY([Contact_ID])
REFERENCES [dbo].[Common_Contact] ([Contact_ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Common_UserDetail] CHECK CONSTRAINT [FK_Common_UserDetail_Common_Contact]
GO
ALTER TABLE [dbo].[Common_UserDetail] ADD  CONSTRAINT [DF_Common_UserDetail_Inactive]  DEFAULT ((0)) FOR [Inactive]
GO
