SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Common_WebSession](
	[WebSession_ID] [int] IDENTITY(1,1) NOT NULL,
	[Contact_ID] [int] NOT NULL,
	[SessionStartDate] [datetime] NULL,
	[SessionLastActivity] [datetime] NULL,
	[ClientIP] [nvarchar](100) NOT NULL,
	[SessionID] [nvarchar](100) NOT NULL,
	[SessionTimeout] [int] NOT NULL,
 CONSTRAINT [PK_Common_WebSession] PRIMARY KEY CLUSTERED 
(
	[WebSession_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Common_WebSession]  WITH CHECK ADD  CONSTRAINT [FK_Common_WebSession_Common_Contact] FOREIGN KEY([Contact_ID])
REFERENCES [dbo].[Common_Contact] ([Contact_ID])
GO
ALTER TABLE [dbo].[Common_WebSession] CHECK CONSTRAINT [FK_Common_WebSession_Common_Contact]
GO
