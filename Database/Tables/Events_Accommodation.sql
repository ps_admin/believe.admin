SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Events_Accommodation](
	[Accommodation_ID] [int] IDENTITY(26,1) NOT NULL,
	[AccommodationVenueName] [varchar](30) NOT NULL,
	[AccommodationVenueLocation] [varchar](50) NOT NULL,
	[Venue_ID] [int] NOT NULL,
	[MaxPeople] [int] NOT NULL,
 CONSTRAINT [PK_PSAccommodation] PRIMARY KEY CLUSTERED 
(
	[Accommodation_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Events_Accommodation]  WITH CHECK ADD  CONSTRAINT [FK_PSAccommodation_PSVenue] FOREIGN KEY([Venue_ID])
REFERENCES [dbo].[Events_Venue] ([Venue_ID])
GO
ALTER TABLE [dbo].[Events_Accommodation] CHECK CONSTRAINT [FK_PSAccommodation_PSVenue]
GO
