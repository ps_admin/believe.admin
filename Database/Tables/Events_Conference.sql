SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Events_Conference](
	[Conference_ID] [int] IDENTITY(1,1) NOT NULL,
	[ConferenceName] [nvarchar](50) NOT NULL,
	[ConferenceNameShort] [nvarchar](50) NOT NULL,
	[ConferenceDate] [nvarchar](50) NOT NULL,
	[ConferenceLocation] [nvarchar](50) NOT NULL,
	[ConferenceTagline] [nvarchar](50) NOT NULL,
	[ConferenceType] [int] NOT NULL,
	[Country_ID] [int] NOT NULL,
	[AllowOnlineRegistrationSingle] [bit] NOT NULL,
	[AllowOnlineRegistrationGroup] [bit] NOT NULL,
	[ShowConferenceOnWeb] [bit] NOT NULL,
	[AllowGroupCreditApproval] [bit] NOT NULL,
	[AllowGroupOnlineBulkRegistrationPurchase] [bit] NOT NULL,
	[RequireEmergencyContactInfo] [bit] NOT NULL,
	[RequireMedicalInfo] [bit] NOT NULL,
	[IncludeULGOption] [bit] NOT NULL,
	[CrecheAvailable] [bit] NOT NULL,
	[PreOrderAvailable] [bit] NOT NULL,
	[ElectivesAvailable] [bit] NOT NULL,
	[AccommodationAvailable] [bit] NOT NULL,
	[CateringAvailable] [bit] NOT NULL,
	[LeadershipBreakfastAvailable] [bit] NOT NULL,
	[AccommodationCost] [money] NOT NULL,
	[CateringCost] [money] NOT NULL,
	[LeadershipBreakfastCost] [money] NOT NULL,
	[PlanetKidsMinAge] [decimal](18, 1) NULL,
	[PlanetKidsMaxAge] [decimal](18, 1) NULL,
	[BankAccount_ID] [int] NOT NULL,
	[SortOrder] [int] NOT NULL,
 CONSTRAINT [PK_Conference] PRIMARY KEY CLUSTERED 
(
	[Conference_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Events_Conference]  WITH CHECK ADD  CONSTRAINT [FK_Conference_GeneralCountry] FOREIGN KEY([Country_ID])
REFERENCES [dbo].[Common_GeneralCountry] ([Country_ID])
GO
ALTER TABLE [dbo].[Events_Conference] CHECK CONSTRAINT [FK_Conference_GeneralCountry]
GO
ALTER TABLE [dbo].[Events_Conference]  WITH CHECK ADD  CONSTRAINT [FK_Event_Conference_Common_BankAccount] FOREIGN KEY([BankAccount_ID])
REFERENCES [dbo].[Common_BankAccount] ([BankAccount_ID])
GO
ALTER TABLE [dbo].[Events_Conference] CHECK CONSTRAINT [FK_Event_Conference_Common_BankAccount]
GO
ALTER TABLE [dbo].[Events_Conference] ADD  CONSTRAINT [DF_Conference_ConferenceNameShort]  DEFAULT ('') FOR [ConferenceNameShort]
GO
ALTER TABLE [dbo].[Events_Conference] ADD  CONSTRAINT [DF_Conference_AllowOnlineRegistrationSingle]  DEFAULT ((1)) FOR [AllowOnlineRegistrationSingle]
GO
ALTER TABLE [dbo].[Events_Conference] ADD  CONSTRAINT [DF_Conference_AllowOnlineRegistrationGroup]  DEFAULT ((1)) FOR [AllowOnlineRegistrationGroup]
GO
ALTER TABLE [dbo].[Events_Conference] ADD  CONSTRAINT [DF_Events_Conference_AllowGroupCreditApproval]  DEFAULT ((1)) FOR [AllowGroupCreditApproval]
GO
ALTER TABLE [dbo].[Events_Conference] ADD  CONSTRAINT [DF_Events_Conference_AllowGroupOnlineBulkRegistrationPurchase]  DEFAULT ((0)) FOR [AllowGroupOnlineBulkRegistrationPurchase]
GO
ALTER TABLE [dbo].[Events_Conference] ADD  CONSTRAINT [DF_Events_Conference_RequireEmergencyContactInfo]  DEFAULT ((0)) FOR [RequireEmergencyContactInfo]
GO
ALTER TABLE [dbo].[Events_Conference] ADD  CONSTRAINT [DF_Events_Conference_RequireMedicalInfo]  DEFAULT ((0)) FOR [RequireMedicalInfo]
GO
ALTER TABLE [dbo].[Events_Conference] ADD  CONSTRAINT [DF_Events_Conference_IncludeULGOption]  DEFAULT ((0)) FOR [IncludeULGOption]
GO
ALTER TABLE [dbo].[Events_Conference] ADD  CONSTRAINT [DF_Conference_PreOrderAvailable]  DEFAULT ((0)) FOR [PreOrderAvailable]
GO
ALTER TABLE [dbo].[Events_Conference] ADD  CONSTRAINT [DF_Events_Conference_ElectivesAvailable]  DEFAULT ((0)) FOR [ElectivesAvailable]
GO
ALTER TABLE [dbo].[Events_Conference] ADD  CONSTRAINT [DF_Conference_AccommodationAvailable]  DEFAULT ((0)) FOR [AccommodationAvailable]
GO
ALTER TABLE [dbo].[Events_Conference] ADD  CONSTRAINT [DF_Conference_CateringAvailable]  DEFAULT ((0)) FOR [CateringAvailable]
GO
ALTER TABLE [dbo].[Events_Conference] ADD  CONSTRAINT [DF_Events_Conference_LeadershipBreakfastAvailable]  DEFAULT ((0)) FOR [LeadershipBreakfastAvailable]
GO
ALTER TABLE [dbo].[Events_Conference] ADD  CONSTRAINT [DF_Events_Conference_AccommodationCost]  DEFAULT ((0)) FOR [AccommodationCost]
GO
ALTER TABLE [dbo].[Events_Conference] ADD  CONSTRAINT [DF_Events_Conference_CateringCost]  DEFAULT ((0)) FOR [CateringCost]
GO
ALTER TABLE [dbo].[Events_Conference] ADD  CONSTRAINT [DF_Events_Conference_LeadershipBreakfastCost]  DEFAULT ((0)) FOR [LeadershipBreakfastCost]
GO
ALTER TABLE [dbo].[Events_Conference] ADD  CONSTRAINT [DF_Conference_BankAccount_ID]  DEFAULT ((1)) FOR [BankAccount_ID]
GO
