SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Events_ConferenceDepartmentMapping](
	[ConferenceDepartmentMapping_ID] [int] IDENTITY(1,1) NOT NULL,
	[VolunteerDepartment_ID] [int] NULL,
	[AccessLevel_ID] [int] NULL,
	[Conference_ID] [int] NULL,
	[DepartmentHead_ID] [int] NULL,
	[ConferenceDepartmentMapping_VolunteersRequired] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ConferenceDepartmentMapping_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Events_ConferenceDepartmentMapping]  WITH CHECK ADD  CONSTRAINT [Events_ConferenceDepartmentMapping_AccessLevel_ID_FK] FOREIGN KEY([AccessLevel_ID])
REFERENCES [dbo].[Events_AccessLevel] ([AccessLevel_ID])
GO
ALTER TABLE [dbo].[Events_ConferenceDepartmentMapping] CHECK CONSTRAINT [Events_ConferenceDepartmentMapping_AccessLevel_ID_FK]
GO
ALTER TABLE [dbo].[Events_ConferenceDepartmentMapping]  WITH CHECK ADD  CONSTRAINT [Events_ConferenceDepartmentMapping_Conference_ID_FK] FOREIGN KEY([Conference_ID])
REFERENCES [dbo].[Events_Conference] ([Conference_ID])
GO
ALTER TABLE [dbo].[Events_ConferenceDepartmentMapping] CHECK CONSTRAINT [Events_ConferenceDepartmentMapping_Conference_ID_FK]
GO
ALTER TABLE [dbo].[Events_ConferenceDepartmentMapping]  WITH CHECK ADD  CONSTRAINT [Events_ConferenceDepartmentMapping_DepartmentHead_ID_FK] FOREIGN KEY([DepartmentHead_ID])
REFERENCES [dbo].[Common_Contact] ([Contact_ID])
GO
ALTER TABLE [dbo].[Events_ConferenceDepartmentMapping] CHECK CONSTRAINT [Events_ConferenceDepartmentMapping_DepartmentHead_ID_FK]
GO
ALTER TABLE [dbo].[Events_ConferenceDepartmentMapping]  WITH CHECK ADD  CONSTRAINT [Events_ConferenceDepartmentMapping_VolunteerDepartment_ID_FK] FOREIGN KEY([VolunteerDepartment_ID])
REFERENCES [dbo].[Events_VolunteerDepartment] ([VolunteerDepartment_ID])
GO
ALTER TABLE [dbo].[Events_ConferenceDepartmentMapping] CHECK CONSTRAINT [Events_ConferenceDepartmentMapping_VolunteerDepartment_ID_FK]
GO
