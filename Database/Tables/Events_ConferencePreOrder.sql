SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Events_ConferencePreOrder](
	[CP_ID] [int] IDENTITY(1,1) NOT NULL,
	[Conference_ID] [int] NOT NULL,
	[Product_ID] [int] NOT NULL,
	[ShowPreOrderOnWeb] [bit] NOT NULL,
 CONSTRAINT [PK_ConferencePreOrder] PRIMARY KEY CLUSTERED 
(
	[CP_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Events_ConferencePreOrder]  WITH CHECK ADD  CONSTRAINT [FK_ConferencePreOrder_Conference] FOREIGN KEY([Conference_ID])
REFERENCES [dbo].[Events_Conference] ([Conference_ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Events_ConferencePreOrder] CHECK CONSTRAINT [FK_ConferencePreOrder_Conference]
GO
ALTER TABLE [dbo].[Events_ConferencePreOrder]  WITH CHECK ADD  CONSTRAINT [FK_ConferencePreOrder_Product] FOREIGN KEY([Product_ID])
REFERENCES [dbo].[Events_Product] ([Product_ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Events_ConferencePreOrder] CHECK CONSTRAINT [FK_ConferencePreOrder_Product]
GO
ALTER TABLE [dbo].[Events_ConferencePreOrder] ADD  CONSTRAINT [DF_ConferencePreOrder_ShowPreOrderOnWeb]  DEFAULT ((1)) FOR [ShowPreOrderOnWeb]
GO
