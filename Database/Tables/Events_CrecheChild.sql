SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Events_CrecheChild](
	[CrecheChild_ID] [int] IDENTITY(303,1) NOT NULL,
	[Registration_ID] [int] NOT NULL,
	[CrecheChildRegistrationType_ID] [int] NULL,
	[ChildFirstName] [varchar](20) NOT NULL,
	[ChildLastName] [varchar](20) NOT NULL,
	[ChildDateOfBirth] [datetime] NULL,
	[ChildAge] [varchar](2) NOT NULL,
	[CareRequired] [varchar](20) NOT NULL,
	[ChildMedicalInformation] [varchar](200) NOT NULL,
	[ChildAllergies] [varchar](200) NOT NULL,
	[CrecheChildSchoolGrade_ID] [int] NULL,
	[ChildrensChurchPastor] [nvarchar](50) NOT NULL,
	[DropOffPickUpName] [nvarchar](50) NOT NULL,
	[DropOffPickUpRelationship] [nvarchar](50) NOT NULL,
	[DropOffPickUpContactNumber] [nvarchar](50) NOT NULL,
	[ConsentFormReturned] [bit] NOT NULL,
	[DateAdded] [datetime] NULL,
	[Gender] [nvarchar](6) NULL,
 CONSTRAINT [PK_CrecheChild] PRIMARY KEY CLUSTERED 
(
	[CrecheChild_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Events_CrecheChild]  WITH CHECK ADD  CONSTRAINT [FK_CrecheChild_CrecheChildSchoolGrade] FOREIGN KEY([CrecheChildSchoolGrade_ID])
REFERENCES [dbo].[Events_CrecheChildSchoolGrade] ([CrecheChildSchoolGrade_ID])
GO
ALTER TABLE [dbo].[Events_CrecheChild] CHECK CONSTRAINT [FK_CrecheChild_CrecheChildSchoolGrade]
GO
ALTER TABLE [dbo].[Events_CrecheChild]  WITH CHECK ADD  CONSTRAINT [FK_CrecheChild_Registration] FOREIGN KEY([Registration_ID])
REFERENCES [dbo].[Events_Registration] ([Registration_ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Events_CrecheChild] CHECK CONSTRAINT [FK_CrecheChild_Registration]
GO
ALTER TABLE [dbo].[Events_CrecheChild]  WITH CHECK ADD  CONSTRAINT [FK_Events_CrecheChild_Events_CrecheChildRegistrationType] FOREIGN KEY([CrecheChildRegistrationType_ID])
REFERENCES [dbo].[Events_CrecheChildRegistrationType] ([CrecheChildRegistrationType_ID])
GO
ALTER TABLE [dbo].[Events_CrecheChild] CHECK CONSTRAINT [FK_Events_CrecheChild_Events_CrecheChildRegistrationType]
GO
ALTER TABLE [dbo].[Events_CrecheChild] ADD  CONSTRAINT [DF_Events_CrecheChild_ChildrensChurchPastor]  DEFAULT ('') FOR [ChildrensChurchPastor]
GO
ALTER TABLE [dbo].[Events_CrecheChild] ADD  CONSTRAINT [DF_Events_CrecheChild_DropOffPickUpName]  DEFAULT ('') FOR [DropOffPickUpName]
GO
ALTER TABLE [dbo].[Events_CrecheChild] ADD  CONSTRAINT [DF_Events_CrecheChild_DropOffPickUpRelationship]  DEFAULT ('') FOR [DropOffPickUpRelationship]
GO
ALTER TABLE [dbo].[Events_CrecheChild] ADD  CONSTRAINT [DF_Events_CrecheChild_DropOffPickUpContactNumber]  DEFAULT ('') FOR [DropOffPickUpContactNumber]
GO
ALTER TABLE [dbo].[Events_CrecheChild] ADD  CONSTRAINT [DF_Events_CrecheChild_ConsentFormReturned]  DEFAULT ((0)) FOR [ConsentFormReturned]
GO
