SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Events_CrecheChildCost](
	[CrecheChildCost_ID] [int] IDENTITY(1,1) NOT NULL,
	[Conference_ID] [int] NOT NULL,
	[MinChild] [int] NOT NULL,
	[MaxChild] [int] NOT NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[Cost] [money] NOT NULL,
 CONSTRAINT [PK_CrecheChildCost] PRIMARY KEY CLUSTERED 
(
	[CrecheChildCost_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Events_CrecheChildCost]  WITH CHECK ADD  CONSTRAINT [FK_Event_CrecheChildCost_Event_Conference] FOREIGN KEY([Conference_ID])
REFERENCES [dbo].[Events_Conference] ([Conference_ID])
GO
ALTER TABLE [dbo].[Events_CrecheChildCost] CHECK CONSTRAINT [FK_Event_CrecheChildCost_Event_Conference]
GO
