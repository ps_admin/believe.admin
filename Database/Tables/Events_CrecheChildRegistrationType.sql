SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Events_CrecheChildRegistrationType](
	[CrecheChildRegistrationType_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Conference_ID] [int] NOT NULL,
	[RegistrationCost] [decimal](18, 2) NOT NULL,
	[MinimumQty] [int] NOT NULL,
	[MaximumQty] [int] NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[SortOrder] [int] NOT NULL,
 CONSTRAINT [PK_Events_CrecheChildRegistrationType] PRIMARY KEY CLUSTERED 
(
	[CrecheChildRegistrationType_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Events_CrecheChildRegistrationType]  WITH CHECK ADD  CONSTRAINT [FK_Events_CrecheChildRegistrationType_Events_Conference] FOREIGN KEY([Conference_ID])
REFERENCES [dbo].[Events_Conference] ([Conference_ID])
GO
ALTER TABLE [dbo].[Events_CrecheChildRegistrationType] CHECK CONSTRAINT [FK_Events_CrecheChildRegistrationType_Events_Conference]
GO
