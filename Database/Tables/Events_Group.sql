SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Events_Group](
	[GroupLeader_ID] [int] NOT NULL,
	[GroupName] [nvarchar](50) NOT NULL,
	[GroupColeaders] [nvarchar](100) NOT NULL,
	[YouthLeaderName] [nvarchar](50) NOT NULL,
	[GroupSize_ID] [int] NULL,
	[GroupCreditStatus_ID] [int] NOT NULL,
	[GroupType_ID] [int] NOT NULL,
	[Comments] [nvarchar](max) NOT NULL,
	[DetailsVerified] [bit] NOT NULL,
	[Deleted] [bit] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[CreatedBy_ID] [int] NOT NULL,
	[ModificationDate] [datetime] NULL,
	[ModifiedBy_ID] [int] NULL,
	[GUID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_Events_Group] PRIMARY KEY CLUSTERED 
(
	[GroupLeader_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Events_Group]  WITH CHECK ADD  CONSTRAINT [FK_Events_Group_Common_Contact] FOREIGN KEY([GroupLeader_ID])
REFERENCES [dbo].[Common_Contact] ([Contact_ID])
GO
ALTER TABLE [dbo].[Events_Group] CHECK CONSTRAINT [FK_Events_Group_Common_Contact]
GO
ALTER TABLE [dbo].[Events_Group]  WITH CHECK ADD  CONSTRAINT [FK_Events_Group_Common_UserDetail] FOREIGN KEY([CreatedBy_ID])
REFERENCES [dbo].[Common_UserDetail] ([Contact_ID])
GO
ALTER TABLE [dbo].[Events_Group] CHECK CONSTRAINT [FK_Events_Group_Common_UserDetail]
GO
ALTER TABLE [dbo].[Events_Group]  WITH CHECK ADD  CONSTRAINT [FK_Events_Group_Events_GroupCreditStatus] FOREIGN KEY([GroupCreditStatus_ID])
REFERENCES [dbo].[Events_GroupCreditStatus] ([GroupCreditStatus_ID])
GO
ALTER TABLE [dbo].[Events_Group] CHECK CONSTRAINT [FK_Events_Group_Events_GroupCreditStatus]
GO
ALTER TABLE [dbo].[Events_Group]  WITH CHECK ADD  CONSTRAINT [FK_Events_Group_Events_GroupSize] FOREIGN KEY([GroupSize_ID])
REFERENCES [dbo].[Events_GroupSize] ([GroupSize_ID])
GO
ALTER TABLE [dbo].[Events_Group] CHECK CONSTRAINT [FK_Events_Group_Events_GroupSize]
GO
ALTER TABLE [dbo].[Events_Group]  WITH CHECK ADD  CONSTRAINT [FK_Events_Group_Events_GroupType] FOREIGN KEY([GroupType_ID])
REFERENCES [dbo].[Events_GroupType] ([GroupType_ID])
GO
ALTER TABLE [dbo].[Events_Group] CHECK CONSTRAINT [FK_Events_Group_Events_GroupType]
GO
ALTER TABLE [dbo].[Events_Group] ADD  CONSTRAINT [DF_Events_Group_GroupColeaders]  DEFAULT ('') FOR [GroupColeaders]
GO
ALTER TABLE [dbo].[Events_Group] ADD  CONSTRAINT [DF_Events_Group_CreditApproved]  DEFAULT ((1)) FOR [GroupCreditStatus_ID]
GO
ALTER TABLE [dbo].[Events_Group] ADD  CONSTRAINT [DF_Events_Group_GroupType2_ID]  DEFAULT ((1)) FOR [GroupType_ID]
GO
ALTER TABLE [dbo].[Events_Group] ADD  CONSTRAINT [DF_Events_Group_DetailsVerified]  DEFAULT ((0)) FOR [DetailsVerified]
GO
ALTER TABLE [dbo].[Events_Group] ADD  CONSTRAINT [DF_Events_Group_GUID]  DEFAULT (newid()) FOR [GUID]
GO
