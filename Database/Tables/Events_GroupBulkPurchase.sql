SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Events_GroupBulkPurchase](
	[GroupBulkPurchase_ID] [int] IDENTITY(1,1) NOT NULL,
	[GroupLeader_ID] [int] NOT NULL,
	[Venue_ID] [int] NOT NULL,
	[AccommodationQuantity] [int] NOT NULL,
	[CateringQuantity] [int] NOT NULL,
	[LeadershipBreakfastQuantity] [int] NOT NULL,
	[PlanetkidsQuantity] [int] NOT NULL,
	[PlanetKidsRegistrationType_ID] [int] NULL,
	[DateAdded] [datetime] NOT NULL,
 CONSTRAINT [PK_Events_GroupBulkPurchase] PRIMARY KEY CLUSTERED 
(
	[GroupBulkPurchase_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Events_GroupBulkPurchase]  WITH CHECK ADD  CONSTRAINT [FK_Events_GroupBulkPurchase_Events_CrecheChildRegistrationType] FOREIGN KEY([PlanetKidsRegistrationType_ID])
REFERENCES [dbo].[Events_CrecheChildRegistrationType] ([CrecheChildRegistrationType_ID])
GO
ALTER TABLE [dbo].[Events_GroupBulkPurchase] CHECK CONSTRAINT [FK_Events_GroupBulkPurchase_Events_CrecheChildRegistrationType]
GO
ALTER TABLE [dbo].[Events_GroupBulkPurchase]  WITH CHECK ADD  CONSTRAINT [FK_Events_GroupBulkPurchase_Events_Group] FOREIGN KEY([GroupLeader_ID])
REFERENCES [dbo].[Events_Group] ([GroupLeader_ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Events_GroupBulkPurchase] CHECK CONSTRAINT [FK_Events_GroupBulkPurchase_Events_Group]
GO
ALTER TABLE [dbo].[Events_GroupBulkPurchase]  WITH CHECK ADD  CONSTRAINT [FK_Events_GroupBulkPurchase_Events_Venue] FOREIGN KEY([Venue_ID])
REFERENCES [dbo].[Events_Venue] ([Venue_ID])
GO
ALTER TABLE [dbo].[Events_GroupBulkPurchase] CHECK CONSTRAINT [FK_Events_GroupBulkPurchase_Events_Venue]
GO
