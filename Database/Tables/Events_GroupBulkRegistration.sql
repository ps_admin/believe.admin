SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Events_GroupBulkRegistration](
	[GroupBulkRegistration_ID] [int] IDENTITY(184,1) NOT NULL,
	[GroupLeader_ID] [int] NOT NULL,
	[Venue_ID] [int] NOT NULL,
	[RegistrationType_ID] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
	[DateAdded] [datetime] NOT NULL,
 CONSTRAINT [PK_GroupBulkRegistration] PRIMARY KEY CLUSTERED 
(
	[GroupBulkRegistration_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Events_GroupBulkRegistration]  WITH CHECK ADD  CONSTRAINT [FK_Events_GroupBulkRegistration_Events_RegistrationType] FOREIGN KEY([RegistrationType_ID])
REFERENCES [dbo].[Events_RegistrationType] ([RegistrationType_ID])
GO
ALTER TABLE [dbo].[Events_GroupBulkRegistration] CHECK CONSTRAINT [FK_Events_GroupBulkRegistration_Events_RegistrationType]
GO
ALTER TABLE [dbo].[Events_GroupBulkRegistration]  WITH CHECK ADD  CONSTRAINT [FK_Events_GroupBulkRegistration_Events_Venue] FOREIGN KEY([Venue_ID])
REFERENCES [dbo].[Events_Venue] ([Venue_ID])
GO
ALTER TABLE [dbo].[Events_GroupBulkRegistration] CHECK CONSTRAINT [FK_Events_GroupBulkRegistration_Events_Venue]
GO
ALTER TABLE [dbo].[Events_GroupBulkRegistration]  WITH NOCHECK ADD  CONSTRAINT [FK_GroupBulkRegistration_Group] FOREIGN KEY([GroupLeader_ID])
REFERENCES [dbo].[Events_Group] ([GroupLeader_ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Events_GroupBulkRegistration] CHECK CONSTRAINT [FK_GroupBulkRegistration_Group]
GO
