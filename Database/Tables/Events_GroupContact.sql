SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Events_GroupContact](
	[GroupContact_ID] [int] IDENTITY(167039,1) NOT NULL,
	[GroupLeader_ID] [int] NOT NULL,
	[Contact_ID] [int] NOT NULL,
 CONSTRAINT [PK_GroupRegistrant] PRIMARY KEY CLUSTERED 
(
	[GroupContact_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Events_GroupContact]  WITH CHECK ADD  CONSTRAINT [FK_Events_GroupContact_Common_ContactExternalEvents] FOREIGN KEY([Contact_ID])
REFERENCES [dbo].[Common_ContactExternalEvents] ([Contact_ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Events_GroupContact] CHECK CONSTRAINT [FK_Events_GroupContact_Common_ContactExternalEvents]
GO
ALTER TABLE [dbo].[Events_GroupContact]  WITH NOCHECK ADD  CONSTRAINT [FK_GroupContact_Group] FOREIGN KEY([GroupLeader_ID])
REFERENCES [dbo].[Events_Group] ([GroupLeader_ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Events_GroupContact] CHECK CONSTRAINT [FK_GroupContact_Group]
GO
