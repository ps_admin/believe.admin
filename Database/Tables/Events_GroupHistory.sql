SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Events_GroupHistory](
	[GH_ID] [int] IDENTITY(1,1) NOT NULL,
	[GroupLeader_ID] [int] NOT NULL,
	[Conference_ID] [int] NULL,
	[Venue_ID] [int] NULL,
	[DateChanged] [datetime] NOT NULL,
	[User_ID] [int] NOT NULL,
	[Action] [varchar](20) NOT NULL,
	[Item] [varchar](200) NOT NULL,
	[Table] [nvarchar](20) NOT NULL,
	[FieldName] [nvarchar](50) NOT NULL,
	[OldValue] [nvarchar](max) NULL,
	[OldText] [nvarchar](max) NOT NULL,
	[NewValue] [nvarchar](max) NULL,
	[NewText] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_GroupHistory] PRIMARY KEY CLUSTERED 
(
	[GH_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Events_GroupHistory]  WITH CHECK ADD  CONSTRAINT [FK_Events_GroupHistory_Common_UserDetail] FOREIGN KEY([User_ID])
REFERENCES [dbo].[Common_UserDetail] ([Contact_ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Events_GroupHistory] CHECK CONSTRAINT [FK_Events_GroupHistory_Common_UserDetail]
GO
ALTER TABLE [dbo].[Events_GroupHistory]  WITH CHECK ADD  CONSTRAINT [FK_GroupHistory_Conference] FOREIGN KEY([Conference_ID])
REFERENCES [dbo].[Events_Conference] ([Conference_ID])
GO
ALTER TABLE [dbo].[Events_GroupHistory] CHECK CONSTRAINT [FK_GroupHistory_Conference]
GO
ALTER TABLE [dbo].[Events_GroupHistory]  WITH NOCHECK ADD  CONSTRAINT [FK_GroupHistory_Group] FOREIGN KEY([GroupLeader_ID])
REFERENCES [dbo].[Events_Group] ([GroupLeader_ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Events_GroupHistory] CHECK CONSTRAINT [FK_GroupHistory_Group]
GO
ALTER TABLE [dbo].[Events_GroupHistory]  WITH CHECK ADD  CONSTRAINT [FK_GroupHistory_Venue] FOREIGN KEY([Venue_ID])
REFERENCES [dbo].[Events_Venue] ([Venue_ID])
GO
ALTER TABLE [dbo].[Events_GroupHistory] CHECK CONSTRAINT [FK_GroupHistory_Venue]
GO
