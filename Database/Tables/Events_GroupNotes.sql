SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Events_GroupNotes](
	[GroupNotes_ID] [int] IDENTITY(1,1) NOT NULL,
	[GroupLeader_ID] [int] NOT NULL,
	[DateAdded] [datetime] NOT NULL,
	[User_ID] [int] NOT NULL,
	[Notes] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Events_GroupNotes] PRIMARY KEY CLUSTERED 
(
	[GroupNotes_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Events_GroupNotes]  WITH CHECK ADD  CONSTRAINT [FK_Events_GroupNotes_Common_UserDetail] FOREIGN KEY([User_ID])
REFERENCES [dbo].[Common_UserDetail] ([Contact_ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Events_GroupNotes] CHECK CONSTRAINT [FK_Events_GroupNotes_Common_UserDetail]
GO
ALTER TABLE [dbo].[Events_GroupNotes]  WITH NOCHECK ADD  CONSTRAINT [FK_Events_GroupNotes_Events_Group] FOREIGN KEY([GroupLeader_ID])
REFERENCES [dbo].[Events_Group] ([GroupLeader_ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Events_GroupNotes] CHECK CONSTRAINT [FK_Events_GroupNotes_Events_Group]
GO
