SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Events_GroupPayment](
	[GroupPayment_ID] [int] IDENTITY(1,1) NOT NULL,
	[GroupLeader_ID] [int] NOT NULL,
	[Conference_ID] [int] NOT NULL,
	[PaymentType_ID] [int] NOT NULL,
	[PaymentAmount] [money] NOT NULL,
	[CCNumber] [varchar](20) NOT NULL,
	[CCExpiry] [varchar](5) NOT NULL,
	[CCName] [varchar](50) NOT NULL,
	[CCPhone] [varchar](20) NOT NULL,
	[CCManual] [bit] NOT NULL,
	[CCTransactionRef] [varchar](20) NOT NULL,
	[CCRefund] [bit] NOT NULL,
	[ChequeDrawer] [varchar](50) NOT NULL,
	[ChequeBank] [varchar](30) NOT NULL,
	[ChequeBranch] [varchar](30) NOT NULL,
	[PaypalTransactionRef] [varchar](20) NOT NULL,
	[Comment] [varchar](200) NOT NULL,
	[PaymentDate] [datetime] NOT NULL,
	[PaymentBy_ID] [int] NOT NULL,
	[BankAccount_ID] [int] NOT NULL,
 CONSTRAINT [PK_GroupPayment] PRIMARY KEY CLUSTERED 
(
	[GroupPayment_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Events_GroupPayment]  WITH CHECK ADD  CONSTRAINT [FK_Events_GroupPayment_Common_UserDetail] FOREIGN KEY([PaymentBy_ID])
REFERENCES [dbo].[Common_UserDetail] ([Contact_ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Events_GroupPayment] CHECK CONSTRAINT [FK_Events_GroupPayment_Common_UserDetail]
GO
ALTER TABLE [dbo].[Events_GroupPayment]  WITH CHECK ADD  CONSTRAINT [FK_GroupPayment_Conference] FOREIGN KEY([Conference_ID])
REFERENCES [dbo].[Events_Conference] ([Conference_ID])
GO
ALTER TABLE [dbo].[Events_GroupPayment] CHECK CONSTRAINT [FK_GroupPayment_Conference]
GO
ALTER TABLE [dbo].[Events_GroupPayment]  WITH NOCHECK ADD  CONSTRAINT [FK_GroupPayment_Group] FOREIGN KEY([GroupLeader_ID])
REFERENCES [dbo].[Events_Group] ([GroupLeader_ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Events_GroupPayment] CHECK CONSTRAINT [FK_GroupPayment_Group]
GO
ALTER TABLE [dbo].[Events_GroupPayment]  WITH CHECK ADD  CONSTRAINT [FK_GroupPayment_PaymentType] FOREIGN KEY([PaymentType_ID])
REFERENCES [dbo].[Common_PaymentType] ([PaymentType_ID])
GO
ALTER TABLE [dbo].[Events_GroupPayment] CHECK CONSTRAINT [FK_GroupPayment_PaymentType]
GO
ALTER TABLE [dbo].[Events_GroupPayment] ADD  CONSTRAINT [DF_Events_GroupPayment_PaypalTransactionRef]  DEFAULT ('') FOR [PaypalTransactionRef]
GO
