SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Events_GroupPromoRequest](
	[GroupPromoRequest_ID] [int] IDENTITY(1,1) NOT NULL,
	[GroupLeader_ID] [int] NOT NULL,
	[Conference_ID] [int] NOT NULL,
	[GroupPromo_ID] [int] NOT NULL,
	[DateAdded] [datetime] NOT NULL,
	[User_ID] [int] NOT NULL,
 CONSTRAINT [PK_Events_GroupPromoRequest] PRIMARY KEY CLUSTERED 
(
	[GroupPromoRequest_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Events_GroupPromoRequest]  WITH CHECK ADD  CONSTRAINT [FK_Events_GroupPromoRequest_Common_UserDetail] FOREIGN KEY([User_ID])
REFERENCES [dbo].[Common_UserDetail] ([Contact_ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Events_GroupPromoRequest] CHECK CONSTRAINT [FK_Events_GroupPromoRequest_Common_UserDetail]
GO
ALTER TABLE [dbo].[Events_GroupPromoRequest]  WITH CHECK ADD  CONSTRAINT [FK_Events_GroupPromoRequest_Events_Conference] FOREIGN KEY([Conference_ID])
REFERENCES [dbo].[Events_Conference] ([Conference_ID])
GO
ALTER TABLE [dbo].[Events_GroupPromoRequest] CHECK CONSTRAINT [FK_Events_GroupPromoRequest_Events_Conference]
GO
ALTER TABLE [dbo].[Events_GroupPromoRequest]  WITH CHECK ADD  CONSTRAINT [FK_Events_GroupPromoRequest_Events_Group] FOREIGN KEY([GroupLeader_ID])
REFERENCES [dbo].[Events_Group] ([GroupLeader_ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Events_GroupPromoRequest] CHECK CONSTRAINT [FK_Events_GroupPromoRequest_Events_Group]
GO
ALTER TABLE [dbo].[Events_GroupPromoRequest]  WITH CHECK ADD  CONSTRAINT [FK_Events_GroupPromoRequest_Events_GroupPromo] FOREIGN KEY([GroupPromo_ID])
REFERENCES [dbo].[Events_GroupPromo] ([GroupPromo_ID])
GO
ALTER TABLE [dbo].[Events_GroupPromoRequest] CHECK CONSTRAINT [FK_Events_GroupPromoRequest_Events_GroupPromo]
GO
