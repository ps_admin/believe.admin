SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Events_POSTransaction](
	[POSTransaction_ID] [int] IDENTITY(13631,1) NOT NULL,
	[SaleType] [int] NOT NULL,
	[TransactionType] [int] NOT NULL,
	[Contact_ID] [int] NULL,
	[ShipTo] [varchar](500) NOT NULL,
	[OriginalDocket] [int] NOT NULL,
	[TransactionDate] [datetime] NOT NULL,
	[User_ID] [int] NOT NULL,
	[POSRegister_ID] [int] NOT NULL,
	[Venue_ID] [int] NULL,
 CONSTRAINT [PK_POSTransaction] PRIMARY KEY CLUSTERED 
(
	[POSTransaction_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Events_POSTransaction]  WITH CHECK ADD  CONSTRAINT [FK_Events_POSTransaction_Common_Contact] FOREIGN KEY([Contact_ID])
REFERENCES [dbo].[Common_Contact] ([Contact_ID])
GO
ALTER TABLE [dbo].[Events_POSTransaction] CHECK CONSTRAINT [FK_Events_POSTransaction_Common_Contact]
GO
ALTER TABLE [dbo].[Events_POSTransaction]  WITH CHECK ADD  CONSTRAINT [FK_Events_POSTransaction_Common_UserDetail] FOREIGN KEY([User_ID])
REFERENCES [dbo].[Common_UserDetail] ([Contact_ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Events_POSTransaction] CHECK CONSTRAINT [FK_Events_POSTransaction_Common_UserDetail]
GO
ALTER TABLE [dbo].[Events_POSTransaction]  WITH CHECK ADD  CONSTRAINT [FK_Events_POSTransaction_Events_Venue] FOREIGN KEY([Venue_ID])
REFERENCES [dbo].[Events_Venue] ([Venue_ID])
GO
ALTER TABLE [dbo].[Events_POSTransaction] CHECK CONSTRAINT [FK_Events_POSTransaction_Events_Venue]
GO
ALTER TABLE [dbo].[Events_POSTransaction]  WITH CHECK ADD  CONSTRAINT [FK_POSTransaction_POSRegister] FOREIGN KEY([POSRegister_ID])
REFERENCES [dbo].[Events_POSRegister] ([POSRegister_ID])
GO
ALTER TABLE [dbo].[Events_POSTransaction] CHECK CONSTRAINT [FK_POSTransaction_POSRegister]
GO
ALTER TABLE [dbo].[Events_POSTransaction] ADD  CONSTRAINT [DF_POSTransaction_TransactionType]  DEFAULT ((1)) FOR [TransactionType]
GO
ALTER TABLE [dbo].[Events_POSTransaction] ADD  CONSTRAINT [DF_POSTransaction_ShipTo]  DEFAULT ('') FOR [ShipTo]
GO
