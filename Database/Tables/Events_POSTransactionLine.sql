SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Events_POSTransactionLine](
	[POSTransactionLine_ID] [int] IDENTITY(15002,1) NOT NULL,
	[POSTransaction_ID] [int] NOT NULL,
	[Product_ID] [int] NOT NULL,
	[Price] [money] NOT NULL,
	[Discount] [decimal](18, 2) NOT NULL,
	[Quantity] [int] NOT NULL,
	[QuantitySupplied] [int] NOT NULL,
	[QuantityReturned] [int] NOT NULL,
	[OriginalPOSTransactionLine_ID] [int] NULL,
	[Title] [varchar](50) NOT NULL,
	[Net] [money] NOT NULL,
 CONSTRAINT [PK_POSTransactionLine] PRIMARY KEY CLUSTERED 
(
	[POSTransactionLine_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Events_POSTransactionLine]  WITH CHECK ADD  CONSTRAINT [FK_POSTransactionLine_POSTransaction] FOREIGN KEY([POSTransaction_ID])
REFERENCES [dbo].[Events_POSTransaction] ([POSTransaction_ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Events_POSTransactionLine] CHECK CONSTRAINT [FK_POSTransactionLine_POSTransaction]
GO
ALTER TABLE [dbo].[Events_POSTransactionLine]  WITH CHECK ADD  CONSTRAINT [FK_POSTransactionLine_Product] FOREIGN KEY([Product_ID])
REFERENCES [dbo].[Events_Product] ([Product_ID])
GO
ALTER TABLE [dbo].[Events_POSTransactionLine] CHECK CONSTRAINT [FK_POSTransactionLine_Product]
GO
ALTER TABLE [dbo].[Events_POSTransactionLine] ADD  CONSTRAINT [DF_POSTransactionLine_HasBeenReturned]  DEFAULT ((0)) FOR [QuantityReturned]
GO
