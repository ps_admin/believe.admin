SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Events_POSTransactionPayment](
	[POSTransactionPayment_ID] [int] IDENTITY(8967,1) NOT NULL,
	[POSTransaction_ID] [int] NOT NULL,
	[PaymentType] [varchar](20) NOT NULL,
	[PaymentAmount] [money] NOT NULL,
	[PaymentTendered] [money] NOT NULL,
	[ChequeDrawer] [varchar](50) NOT NULL,
	[ChequeBank] [varchar](30) NOT NULL,
	[ChequeBranch] [varchar](30) NOT NULL,
	[CreditCardType] [int] NOT NULL,
 CONSTRAINT [PK_POSTransactionPayment] PRIMARY KEY CLUSTERED 
(
	[POSTransactionPayment_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Events_POSTransactionPayment]  WITH CHECK ADD  CONSTRAINT [FK_POSTransactionPayment_POSTransaction] FOREIGN KEY([POSTransaction_ID])
REFERENCES [dbo].[Events_POSTransaction] ([POSTransaction_ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Events_POSTransactionPayment] CHECK CONSTRAINT [FK_POSTransactionPayment_POSTransaction]
GO
ALTER TABLE [dbo].[Events_POSTransactionPayment] ADD  CONSTRAINT [DF_POSTransactionPayment_PaymentTendered]  DEFAULT ((0)) FOR [PaymentTendered]
GO
ALTER TABLE [dbo].[Events_POSTransactionPayment] ADD  CONSTRAINT [DF_POSTransactionPayment_CreditCardType]  DEFAULT ((1)) FOR [CreditCardType]
GO
