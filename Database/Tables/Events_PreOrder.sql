SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Events_PreOrder](
	[PreOrder_ID] [int] IDENTITY(3515,1) NOT NULL,
	[Registration_ID] [int] NULL,
	[Product_ID] [int] NOT NULL,
	[Discount] [decimal](18, 2) NOT NULL,
	[PickedUp] [bit] NULL,
 CONSTRAINT [PK_PreOrder] PRIMARY KEY CLUSTERED 
(
	[PreOrder_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Events_PreOrder]  WITH CHECK ADD  CONSTRAINT [FK_PreOrder_Product] FOREIGN KEY([Product_ID])
REFERENCES [dbo].[Events_Product] ([Product_ID])
GO
ALTER TABLE [dbo].[Events_PreOrder] CHECK CONSTRAINT [FK_PreOrder_Product]
GO
ALTER TABLE [dbo].[Events_PreOrder]  WITH CHECK ADD  CONSTRAINT [FK_PreOrder_Registration] FOREIGN KEY([Registration_ID])
REFERENCES [dbo].[Events_Registration] ([Registration_ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Events_PreOrder] CHECK CONSTRAINT [FK_PreOrder_Registration]
GO
