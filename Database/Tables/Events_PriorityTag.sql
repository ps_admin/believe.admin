SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Events_PriorityTag](
	[PriorityTag_ID] [varchar](10) NOT NULL,
	[AccessLevel_ID] [int] NOT NULL,
	[PriorityTag_Active] [bit] NULL,
	[PriorityTag_IssueDate] [date] NOT NULL,
	[PriorityTag_DeactivationDate] [date] NULL,
PRIMARY KEY CLUSTERED 
(
	[PriorityTag_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Events_PriorityTag]  WITH CHECK ADD  CONSTRAINT [Events_PriorityTag_AccessLevel_ID_FK] FOREIGN KEY([AccessLevel_ID])
REFERENCES [dbo].[Events_AccessLevel] ([AccessLevel_ID])
GO
ALTER TABLE [dbo].[Events_PriorityTag] CHECK CONSTRAINT [Events_PriorityTag_AccessLevel_ID_FK]
GO
