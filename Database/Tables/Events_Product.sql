SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Events_Product](
	[Product_ID] [int] IDENTITY(141,1) NOT NULL,
	[Code] [varchar](20) NOT NULL,
	[Code2] [varchar](20) NOT NULL,
	[Title] [varchar](50) NOT NULL,
	[AuthorArtist] [varchar](50) NOT NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[Product_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Events_Product] ADD  CONSTRAINT [DF_Product_SecondaryCode]  DEFAULT ('') FOR [Code2]
GO
ALTER TABLE [dbo].[Events_Product] ADD  CONSTRAINT [DF_Product_Deleted]  DEFAULT ((0)) FOR [Deleted]
GO
