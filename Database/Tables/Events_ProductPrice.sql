SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Events_ProductPrice](
	[ProductPrice_ID] [int] IDENTITY(1,1) NOT NULL,
	[Product_ID] [int] NOT NULL,
	[Country_ID] [int] NOT NULL,
	[Price] [money] NOT NULL,
 CONSTRAINT [PK_ProductPrice] PRIMARY KEY CLUSTERED 
(
	[ProductPrice_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Events_ProductPrice]  WITH CHECK ADD  CONSTRAINT [FK_ProductPrice_GeneralCountry] FOREIGN KEY([Country_ID])
REFERENCES [dbo].[Common_GeneralCountry] ([Country_ID])
GO
ALTER TABLE [dbo].[Events_ProductPrice] CHECK CONSTRAINT [FK_ProductPrice_GeneralCountry]
GO
ALTER TABLE [dbo].[Events_ProductPrice]  WITH CHECK ADD  CONSTRAINT [FK_ProductPrice_Product] FOREIGN KEY([Product_ID])
REFERENCES [dbo].[Events_Product] ([Product_ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Events_ProductPrice] CHECK CONSTRAINT [FK_ProductPrice_Product]
GO
