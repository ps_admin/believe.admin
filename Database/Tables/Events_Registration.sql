SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Events_Registration](
	[Registration_ID] [int] IDENTITY(35341,1) NOT NULL,
	[Contact_ID] [int] NOT NULL,
	[GroupLeader_ID] [int] NULL,
	[FamilyRegistration] [bit] NOT NULL,
	[RegisteredByFriend_ID] [int] NULL,
	[Venue_ID] [int] NOT NULL,
	[RegistrationType_ID] [int] NOT NULL,
	[RegistrationDiscount] [decimal](18, 2) NOT NULL,
	[Elective_ID] [int] NULL,
	[Accommodation] [bit] NOT NULL,
	[Accommodation_ID] [int] NULL,
	[Catering] [bit] NOT NULL,
	[CateringNeeds] [nvarchar](100) NOT NULL,
	[LeadershipBreakfast] [bit] NOT NULL,
	[LeadershipSummit] [bit] NOT NULL,
	[ULG_ID] [int] NULL,
	[ParentGuardian] [varchar](50) NOT NULL,
	[ParentGuardianPhone] [varchar](50) NOT NULL,
	[AcceptTermsRego] [bit] NOT NULL,
	[AcceptTermsParent] [bit] NOT NULL,
	[AcceptTermsCreche] [bit] NOT NULL,
	[RegistrationDate] [datetime] NOT NULL,
	[RegisteredBy_ID] [int] NOT NULL,
	[Attended] [bit] NOT NULL,
	[Volunteer] [bit] NOT NULL,
	[VolunteerDepartment_ID] [int] NULL,
	[ComboRegistration_ID] [int] NULL,
 CONSTRAINT [PK_Registration] PRIMARY KEY CLUSTERED 
(
	[Registration_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Events_Registration]  WITH CHECK ADD  CONSTRAINT [FK_Events_Registration_Common_ContactExternalEvents] FOREIGN KEY([Contact_ID])
REFERENCES [dbo].[Common_ContactExternalEvents] ([Contact_ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Events_Registration] CHECK CONSTRAINT [FK_Events_Registration_Common_ContactExternalEvents]
GO
ALTER TABLE [dbo].[Events_Registration]  WITH CHECK ADD  CONSTRAINT [FK_Events_Registration_Common_UserDetail] FOREIGN KEY([RegisteredBy_ID])
REFERENCES [dbo].[Common_UserDetail] ([Contact_ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Events_Registration] CHECK CONSTRAINT [FK_Events_Registration_Common_UserDetail]
GO
ALTER TABLE [dbo].[Events_Registration]  WITH CHECK ADD  CONSTRAINT [FK_Events_Registration_Events_Elective] FOREIGN KEY([Elective_ID])
REFERENCES [dbo].[Events_Elective] ([Elective_ID])
GO
ALTER TABLE [dbo].[Events_Registration] CHECK CONSTRAINT [FK_Events_Registration_Events_Elective]
GO
ALTER TABLE [dbo].[Events_Registration]  WITH CHECK ADD  CONSTRAINT [FK_Events_Registration_Events_Group] FOREIGN KEY([GroupLeader_ID])
REFERENCES [dbo].[Events_Group] ([GroupLeader_ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Events_Registration] CHECK CONSTRAINT [FK_Events_Registration_Events_Group]
GO
ALTER TABLE [dbo].[Events_Registration]  WITH CHECK ADD  CONSTRAINT [FK_Events_Registration_Events_RegistrationType] FOREIGN KEY([RegistrationType_ID])
REFERENCES [dbo].[Events_RegistrationType] ([RegistrationType_ID])
GO
ALTER TABLE [dbo].[Events_Registration] CHECK CONSTRAINT [FK_Events_Registration_Events_RegistrationType]
GO
ALTER TABLE [dbo].[Events_Registration]  WITH CHECK ADD  CONSTRAINT [FK_Registration_Accommodation] FOREIGN KEY([Accommodation_ID])
REFERENCES [dbo].[Events_Accommodation] ([Accommodation_ID])
GO
ALTER TABLE [dbo].[Events_Registration] CHECK CONSTRAINT [FK_Registration_Accommodation]
GO
ALTER TABLE [dbo].[Events_Registration]  WITH CHECK ADD  CONSTRAINT [FK_Registration_Venue] FOREIGN KEY([Venue_ID])
REFERENCES [dbo].[Events_Venue] ([Venue_ID])
GO
ALTER TABLE [dbo].[Events_Registration] CHECK CONSTRAINT [FK_Registration_Venue]
GO
ALTER TABLE [dbo].[Events_Registration]  WITH CHECK ADD  CONSTRAINT [FK_Registration_VolunteerDepartment] FOREIGN KEY([VolunteerDepartment_ID])
REFERENCES [dbo].[Events_VolunteerDepartment] ([VolunteerDepartment_ID])
GO
ALTER TABLE [dbo].[Events_Registration] CHECK CONSTRAINT [FK_Registration_VolunteerDepartment]
GO
ALTER TABLE [dbo].[Events_Registration] ADD  CONSTRAINT [DF_Events_Registration_FamilyRegistration]  DEFAULT ((0)) FOR [FamilyRegistration]
GO
ALTER TABLE [dbo].[Events_Registration] ADD  CONSTRAINT [DF_Events_Registration_Accommodation]  DEFAULT ((0)) FOR [Accommodation]
GO
ALTER TABLE [dbo].[Events_Registration] ADD  CONSTRAINT [DF_Events_Registration_Catering]  DEFAULT ((0)) FOR [Catering]
GO
ALTER TABLE [dbo].[Events_Registration] ADD  CONSTRAINT [DF_Events_Registration_CateringNeeds]  DEFAULT ('') FOR [CateringNeeds]
GO
ALTER TABLE [dbo].[Events_Registration] ADD  CONSTRAINT [DF_Events_Registration_LeadershipBreakfast]  DEFAULT ((0)) FOR [LeadershipBreakfast]
GO
ALTER TABLE [dbo].[Events_Registration] ADD  CONSTRAINT [DF_Events_Registration_LeadershipSummit]  DEFAULT ((0)) FOR [LeadershipSummit]
GO
