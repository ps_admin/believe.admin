SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Events_RegistrationChildInformation](
	[Registration_ID] [int] NOT NULL,
	[CrecheChildSchoolGrade_ID] [int] NULL,
	[ChildsFriend] [nvarchar](50) NOT NULL,
	[DropOffPickUpName] [nvarchar](50) NOT NULL,
	[DropOffPickUpRelationship] [nvarchar](50) NOT NULL,
	[DropOffPickUpContactNumber] [nvarchar](50) NOT NULL,
	[ChildrensChurchPastor] [nvarchar](50) NOT NULL,
	[ConsentFormSent] [bit] NOT NULL,
	[ConsentFormSentDate] [datetime] NULL,
	[ConsentFormReturned] [bit] NOT NULL,
	[ConsentFormReturnedDate] [datetime] NULL,
 CONSTRAINT [PK_Events_RegistrationChildInformation] PRIMARY KEY CLUSTERED 
(
	[Registration_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Events_RegistrationChildInformation]  WITH CHECK ADD  CONSTRAINT [FK_Events_RegistrationChildInformation_Events_CrecheChildSchoolGrade] FOREIGN KEY([CrecheChildSchoolGrade_ID])
REFERENCES [dbo].[Events_CrecheChildSchoolGrade] ([CrecheChildSchoolGrade_ID])
GO
ALTER TABLE [dbo].[Events_RegistrationChildInformation] CHECK CONSTRAINT [FK_Events_RegistrationChildInformation_Events_CrecheChildSchoolGrade]
GO
ALTER TABLE [dbo].[Events_RegistrationChildInformation]  WITH CHECK ADD  CONSTRAINT [FK_Events_RegistrationChildInformation_Events_Registration] FOREIGN KEY([Registration_ID])
REFERENCES [dbo].[Events_Registration] ([Registration_ID])
GO
ALTER TABLE [dbo].[Events_RegistrationChildInformation] CHECK CONSTRAINT [FK_Events_RegistrationChildInformation_Events_Registration]
GO
ALTER TABLE [dbo].[Events_RegistrationChildInformation] ADD  CONSTRAINT [DF_Events_RegistrationChildInformation_ConsentFormSent]  DEFAULT ((0)) FOR [ConsentFormSent]
GO
ALTER TABLE [dbo].[Events_RegistrationChildInformation] ADD  CONSTRAINT [DF_Events_RegistrationChildInformation_ConsentFormReturned]  DEFAULT ((0)) FOR [ConsentFormReturned]
GO
