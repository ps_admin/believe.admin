SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Events_RegistrationOption](
	[RegistrationOption_ID] [int] IDENTITY(1,1) NOT NULL,
	[Registration_ID] [int] NOT NULL,
	[RegistrationOptionName_ID] [int] NOT NULL,
	[RegistrationOptionValue_ID] [int] NULL,
	[RegistrationOptionText] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_Events_RegistrationOption] PRIMARY KEY CLUSTERED 
(
	[RegistrationOption_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_Events_RegistrationOption] UNIQUE NONCLUSTERED 
(
	[Registration_ID] ASC,
	[RegistrationOptionName_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Events_RegistrationOption]  WITH CHECK ADD  CONSTRAINT [FK_RegistrationOption_Registration] FOREIGN KEY([Registration_ID])
REFERENCES [dbo].[Events_Registration] ([Registration_ID])
GO
ALTER TABLE [dbo].[Events_RegistrationOption] CHECK CONSTRAINT [FK_RegistrationOption_Registration]
GO
ALTER TABLE [dbo].[Events_RegistrationOption]  WITH CHECK ADD  CONSTRAINT [FK_RegistrationOption_RegistrationOptionName] FOREIGN KEY([RegistrationOptionName_ID])
REFERENCES [dbo].[Events_RegistrationOptionName] ([RegistrationOptionName_ID])
GO
ALTER TABLE [dbo].[Events_RegistrationOption] CHECK CONSTRAINT [FK_RegistrationOption_RegistrationOptionName]
GO
ALTER TABLE [dbo].[Events_RegistrationOption]  WITH CHECK ADD  CONSTRAINT [FK_RegistrationOption_RegistrationOptionValue] FOREIGN KEY([RegistrationOptionValue_ID])
REFERENCES [dbo].[Events_RegistrationOptionValue] ([RegistrationOptionValue_ID])
GO
ALTER TABLE [dbo].[Events_RegistrationOption] CHECK CONSTRAINT [FK_RegistrationOption_RegistrationOptionValue]
GO
