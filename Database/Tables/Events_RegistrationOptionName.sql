SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Events_RegistrationOptionName](
	[RegistrationOptionName_ID] [int] IDENTITY(1,1) NOT NULL,
	[Conference_ID] [int] NOT NULL,
	[OptionType] [nvarchar](10) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[FieldType] [nvarchar](10) NOT NULL,
	[TransactionLogDescription] [nvarchar](50) NOT NULL,
	[SortOrder] [int] NOT NULL,
	[IncludeFieldInReport] [bit] NOT NULL,
 CONSTRAINT [PK_RegistrationOptionValue] PRIMARY KEY CLUSTERED 
(
	[RegistrationOptionName_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Events_RegistrationOptionName]  WITH CHECK ADD  CONSTRAINT [FK_Events_RegistrationOptionName_Events_Conference] FOREIGN KEY([Conference_ID])
REFERENCES [dbo].[Events_Conference] ([Conference_ID])
GO
ALTER TABLE [dbo].[Events_RegistrationOptionName] CHECK CONSTRAINT [FK_Events_RegistrationOptionName_Events_Conference]
GO
ALTER TABLE [dbo].[Events_RegistrationOptionName] ADD  CONSTRAINT [DF_RegistrationOptionName_SortOrder]  DEFAULT ((0)) FOR [SortOrder]
GO
