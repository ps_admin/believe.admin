SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Events_RegistrationOptionValue](
	[RegistrationOptionValue_ID] [int] IDENTITY(1,1) NOT NULL,
	[RegistrationOptionName_ID] [int] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Description] [nvarchar](100) NOT NULL,
	[Value] [int] NOT NULL,
	[OtherInformation_RegistrationOptionName_ID] [int] NULL,
	[IsDefault] [bit] NOT NULL,
	[AvailableOnline] [bit] NOT NULL,
	[SortOrder] [int] NULL,
 CONSTRAINT [PK_RegoOptionValue] PRIMARY KEY CLUSTERED 
(
	[RegistrationOptionValue_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Events_RegistrationOptionValue]  WITH CHECK ADD  CONSTRAINT [FK_RegistrationOptionValue_RegistrationOptionName] FOREIGN KEY([RegistrationOptionName_ID])
REFERENCES [dbo].[Events_RegistrationOptionName] ([RegistrationOptionName_ID])
GO
ALTER TABLE [dbo].[Events_RegistrationOptionValue] CHECK CONSTRAINT [FK_RegistrationOptionValue_RegistrationOptionName]
GO
ALTER TABLE [dbo].[Events_RegistrationOptionValue] ADD  CONSTRAINT [DF_Events_RegistrationOptionValue_Description]  DEFAULT ('') FOR [Description]
GO
ALTER TABLE [dbo].[Events_RegistrationOptionValue] ADD  CONSTRAINT [DF_RegistrationOptionValue_IsDefault]  DEFAULT ((0)) FOR [IsDefault]
GO
ALTER TABLE [dbo].[Events_RegistrationOptionValue] ADD  CONSTRAINT [DF_Events_RegistrationOptionValue_AvailableOnline]  DEFAULT ((1)) FOR [AvailableOnline]
GO
