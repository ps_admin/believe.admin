SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Events_RegistrationType](
	[RegistrationType_ID] [int] IDENTITY(1,1) NOT NULL,
	[Conference_ID] [int] NOT NULL,
	[RegistrationType] [varchar](80) NOT NULL,
	[RegistrationTypeQuantityGroup_ID] [int] NULL,
	[RegistrationTypeReportGroup_ID] [int] NULL,
	[RegistrationTypeSaleGroup_ID] [int] NULL,
	[RegistrationCost] [money] NOT NULL,
	[AvailableOnlineSingle] [bit] NOT NULL,
	[AvailableOnlineGroup] [bit] NOT NULL,
	[GroupMinimumQuantity] [int] NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[IsChildRegistrationType] [bit] NOT NULL,
	[IsYouthRegistrationType] [bit] NOT NULL,
	[RegistrationTypeCombo_ID] [int] NULL,
	[ComboRegistrationType_ID] [int] NULL,
	[ComboVenue_ID] [int] NULL,
	[SortOrder] [int] NOT NULL,
 CONSTRAINT [PK_RegistrationType] PRIMARY KEY NONCLUSTERED 
(
	[RegistrationType_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE CLUSTERED INDEX [IX_RegistrationType] ON [dbo].[Events_RegistrationType] 
(
	[Conference_ID] ASC,
	[SortOrder] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Events_RegistrationType]  WITH CHECK ADD  CONSTRAINT [FK_Events_RegistrationType_Events_RegistrationTypeQuantityGroup] FOREIGN KEY([RegistrationTypeQuantityGroup_ID])
REFERENCES [dbo].[Events_RegistrationTypeQuantityGroup] ([RegistrationTypeQuantityGroup_ID])
GO
ALTER TABLE [dbo].[Events_RegistrationType] CHECK CONSTRAINT [FK_Events_RegistrationType_Events_RegistrationTypeQuantityGroup]
GO
ALTER TABLE [dbo].[Events_RegistrationType]  WITH CHECK ADD  CONSTRAINT [FK_Events_RegistrationType_Events_RegistrationTypeReportGroup] FOREIGN KEY([RegistrationTypeReportGroup_ID])
REFERENCES [dbo].[Events_RegistrationTypeReportGroup] ([RegistrationTypeReportGroup_ID])
GO
ALTER TABLE [dbo].[Events_RegistrationType] CHECK CONSTRAINT [FK_Events_RegistrationType_Events_RegistrationTypeReportGroup]
GO
ALTER TABLE [dbo].[Events_RegistrationType]  WITH CHECK ADD  CONSTRAINT [FK_Events_RegistrationType_Events_RegistrationTypeSaleGroup] FOREIGN KEY([RegistrationTypeSaleGroup_ID])
REFERENCES [dbo].[Events_RegistrationTypeSaleGroup] ([RegistrationTypeSaleGroup_ID])
GO
ALTER TABLE [dbo].[Events_RegistrationType] CHECK CONSTRAINT [FK_Events_RegistrationType_Events_RegistrationTypeSaleGroup]
GO
ALTER TABLE [dbo].[Events_RegistrationType]  WITH CHECK ADD  CONSTRAINT [FK_RegistrationType_Conference] FOREIGN KEY([Conference_ID])
REFERENCES [dbo].[Events_Conference] ([Conference_ID])
GO
ALTER TABLE [dbo].[Events_RegistrationType] CHECK CONSTRAINT [FK_RegistrationType_Conference]
GO
ALTER TABLE [dbo].[Events_RegistrationType] ADD  CONSTRAINT [DF_Events_RegistrationType_AvailableOnlineSingle]  DEFAULT ((0)) FOR [AvailableOnlineSingle]
GO
ALTER TABLE [dbo].[Events_RegistrationType] ADD  CONSTRAINT [DF_Events_RegistrationType_AvailableOnlineGroup]  DEFAULT ((0)) FOR [AvailableOnlineGroup]
GO
ALTER TABLE [dbo].[Events_RegistrationType] ADD  CONSTRAINT [DF_Events_RegistrationType_GroupMinimumQuantity]  DEFAULT ((1)) FOR [GroupMinimumQuantity]
GO
ALTER TABLE [dbo].[Events_RegistrationType] ADD  CONSTRAINT [DF_Events_RegistrationType_IsChildRegistrationType]  DEFAULT ((0)) FOR [IsChildRegistrationType]
GO
ALTER TABLE [dbo].[Events_RegistrationType] ADD  CONSTRAINT [DF_Events_RegistrationType_IsYouthRegistrationType]  DEFAULT ((0)) FOR [IsYouthRegistrationType]
GO
ALTER TABLE [dbo].[Events_RegistrationType] ADD  CONSTRAINT [DF_RegistrationType_SortOrder]  DEFAULT ((0)) FOR [SortOrder]
GO
