SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Events_RegistrationTypeCombo](
	[RegistrationTypeCombo_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Conference_ID] [int] NOT NULL,
	[Venue_ID1] [int] NOT NULL,
	[Venue_ID2] [int] NOT NULL,
 CONSTRAINT [PK_Events_RegistrationTypeComboGroup] PRIMARY KEY CLUSTERED 
(
	[RegistrationTypeCombo_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
