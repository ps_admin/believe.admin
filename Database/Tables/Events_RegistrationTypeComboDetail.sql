SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Events_RegistrationTypeComboDetail](
	[RegistrationTypeComboDetail_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[RegistrationTypeCombo_ID] [int] NOT NULL,
	[RegistrationType_ID1] [int] NOT NULL,
	[RegistrationType_ID2] [int] NOT NULL,
 CONSTRAINT [PK_Events_RegistrationTypeComboDetail] PRIMARY KEY CLUSTERED 
(
	[RegistrationTypeComboDetail_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
