SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Events_RegistrationTypeElective](
	[RegistrationTypeElective_ID] [int] IDENTITY(1,1) NOT NULL,
	[RegistrationType_ID] [int] NOT NULL,
	[Elective_ID] [int] NOT NULL,
 CONSTRAINT [PK_Events_RegistrationTypeElective] PRIMARY KEY CLUSTERED 
(
	[RegistrationTypeElective_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Events_RegistrationTypeElective]  WITH CHECK ADD  CONSTRAINT [FK_Events_RegistrationTypeElective_Events_Elective] FOREIGN KEY([Elective_ID])
REFERENCES [dbo].[Events_Elective] ([Elective_ID])
GO
ALTER TABLE [dbo].[Events_RegistrationTypeElective] CHECK CONSTRAINT [FK_Events_RegistrationTypeElective_Events_Elective]
GO
ALTER TABLE [dbo].[Events_RegistrationTypeElective]  WITH CHECK ADD  CONSTRAINT [FK_Events_RegistrationTypeElective_Events_RegistrationType] FOREIGN KEY([RegistrationType_ID])
REFERENCES [dbo].[Events_RegistrationType] ([RegistrationType_ID])
GO
ALTER TABLE [dbo].[Events_RegistrationTypeElective] CHECK CONSTRAINT [FK_Events_RegistrationTypeElective_Events_RegistrationType]
GO
