SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Events_RegistrationTypeGroup](
	[RegistrationTypeGroup_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Conference_ID] [int] NOT NULL,
	[SortOrder] [int] NOT NULL,
 CONSTRAINT [PK_Events_RegistrationTypeGroup] PRIMARY KEY CLUSTERED 
(
	[RegistrationTypeGroup_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Events_RegistrationTypeGroup]  WITH CHECK ADD  CONSTRAINT [FK_Events_RegistrationTypeGroup_Events_Conference] FOREIGN KEY([Conference_ID])
REFERENCES [dbo].[Events_Conference] ([Conference_ID])
GO
ALTER TABLE [dbo].[Events_RegistrationTypeGroup] CHECK CONSTRAINT [FK_Events_RegistrationTypeGroup_Events_Conference]
GO
