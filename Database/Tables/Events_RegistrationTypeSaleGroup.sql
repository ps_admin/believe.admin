SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Events_RegistrationTypeSaleGroup](
	[RegistrationTypeSaleGroup_ID] [int] IDENTITY(1,1) NOT NULL,
	[Conference_ID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[MinimumAge] [decimal](18, 1) NOT NULL,
	[MaximumAge] [decimal](18, 1) NOT NULL,
	[SortOrder] [int] NOT NULL,
 CONSTRAINT [PK_Events_RegistrationTypeSaleGroup] PRIMARY KEY CLUSTERED 
(
	[RegistrationTypeSaleGroup_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Events_RegistrationTypeSaleGroup]  WITH CHECK ADD  CONSTRAINT [FK_Events_RegistrationTypeSaleGroup_Events_Conference] FOREIGN KEY([Conference_ID])
REFERENCES [dbo].[Events_Conference] ([Conference_ID])
GO
ALTER TABLE [dbo].[Events_RegistrationTypeSaleGroup] CHECK CONSTRAINT [FK_Events_RegistrationTypeSaleGroup_Events_Conference]
GO
