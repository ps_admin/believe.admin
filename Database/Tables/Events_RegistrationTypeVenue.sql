SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Events_RegistrationTypeVenue](
	[RegistrationTypeVenue_ID] [int] IDENTITY(1,1) NOT NULL,
	[RegistrationType_ID] [int] NOT NULL,
	[Venue_ID] [int] NOT NULL,
 CONSTRAINT [PK_RegistrationTypeVenue] PRIMARY KEY CLUSTERED 
(
	[RegistrationTypeVenue_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Events_RegistrationTypeVenue]  WITH CHECK ADD  CONSTRAINT [FK_Events_RegistrationTypeVenue_Events_RegistrationType] FOREIGN KEY([RegistrationType_ID])
REFERENCES [dbo].[Events_RegistrationType] ([RegistrationType_ID])
GO
ALTER TABLE [dbo].[Events_RegistrationTypeVenue] CHECK CONSTRAINT [FK_Events_RegistrationTypeVenue_Events_RegistrationType]
GO
ALTER TABLE [dbo].[Events_RegistrationTypeVenue]  WITH CHECK ADD  CONSTRAINT [FK_Venue] FOREIGN KEY([Venue_ID])
REFERENCES [dbo].[Events_Venue] ([Venue_ID])
GO
ALTER TABLE [dbo].[Events_RegistrationTypeVenue] CHECK CONSTRAINT [FK_Venue]
GO
