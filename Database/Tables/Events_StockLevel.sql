SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Events_StockLevel](
	[StockLevel_ID] [int] IDENTITY(1,1) NOT NULL,
	[Product_ID] [int] NOT NULL,
	[Venue_ID] [int] NULL,
	[Quantity] [int] NOT NULL,
	[AdjustmentType] [int] NOT NULL,
	[DateAdded] [datetime] NOT NULL,
	[Comments] [varchar](500) NOT NULL,
 CONSTRAINT [PK_StockLevel] PRIMARY KEY CLUSTERED 
(
	[StockLevel_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Events_StockLevel]  WITH CHECK ADD  CONSTRAINT [FK_StockLevel_Product] FOREIGN KEY([Product_ID])
REFERENCES [dbo].[Events_Product] ([Product_ID])
GO
ALTER TABLE [dbo].[Events_StockLevel] CHECK CONSTRAINT [FK_StockLevel_Product]
GO
ALTER TABLE [dbo].[Events_StockLevel]  WITH CHECK ADD  CONSTRAINT [FK_StockLevel_Venue] FOREIGN KEY([Venue_ID])
REFERENCES [dbo].[Events_Venue] ([Venue_ID])
GO
ALTER TABLE [dbo].[Events_StockLevel] CHECK CONSTRAINT [FK_StockLevel_Venue]
GO
ALTER TABLE [dbo].[Events_StockLevel] ADD  CONSTRAINT [DF_StockJournal_AdjustmentType]  DEFAULT ((1)) FOR [AdjustmentType]
GO
