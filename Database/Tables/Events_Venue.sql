SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Events_Venue](
	[Venue_ID] [int] IDENTITY(18,1) NOT NULL,
	[Conference_ID] [int] NOT NULL,
	[VenueName] [varchar](30) NOT NULL,
	[VenueLocation] [varchar](100) NOT NULL,
	[State_ID] [int] NULL,
	[ConferenceStartDate] [datetime] NOT NULL,
	[ConferenceEndDate] [datetime] NOT NULL,
	[MaxRegistrants] [int] NOT NULL,
	[AllowMultipleRegistrations] [bit] NOT NULL,
	[IsClosed] [bit] NOT NULL,
	[AccommodationClosed] [bit] NOT NULL,
	[CateringClosed] [bit] NOT NULL,
	[CrecheClosed] [bit] NOT NULL,
	[PlanetkidsEarlyChildhoodClosed] [bit] NOT NULL,
	[PlanetkidsPrimaryClosed] [bit] NOT NULL,
	[LeadershipBreakfastClosed] [bit] NOT NULL,
 CONSTRAINT [PK_PSVenue] PRIMARY KEY CLUSTERED 
(
	[Venue_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Events_Venue]  WITH CHECK ADD  CONSTRAINT [FK_Venue_Conference] FOREIGN KEY([Conference_ID])
REFERENCES [dbo].[Events_Conference] ([Conference_ID])
GO
ALTER TABLE [dbo].[Events_Venue] CHECK CONSTRAINT [FK_Venue_Conference]
GO
ALTER TABLE [dbo].[Events_Venue] ADD  CONSTRAINT [DF_Venue_AllowMultipleRegistrations]  DEFAULT ((0)) FOR [AllowMultipleRegistrations]
GO
ALTER TABLE [dbo].[Events_Venue] ADD  CONSTRAINT [DF_PSVenue_AccommotdationClosed]  DEFAULT ((0)) FOR [AccommodationClosed]
GO
ALTER TABLE [dbo].[Events_Venue] ADD  CONSTRAINT [DF_PSVenue_CateringClosed]  DEFAULT ((0)) FOR [CateringClosed]
GO
ALTER TABLE [dbo].[Events_Venue] ADD  CONSTRAINT [DF_PSVenue_CrecheClosed]  DEFAULT ((0)) FOR [CrecheClosed]
GO
ALTER TABLE [dbo].[Events_Venue] ADD  CONSTRAINT [DF_Events_Venue_PlanetkidsEarlyChildhoodClosed]  DEFAULT ((0)) FOR [PlanetkidsEarlyChildhoodClosed]
GO
ALTER TABLE [dbo].[Events_Venue] ADD  CONSTRAINT [DF_Events_Venue_PlanetkidsPrimaryClosed]  DEFAULT ((0)) FOR [PlanetkidsPrimaryClosed]
GO
ALTER TABLE [dbo].[Events_Venue] ADD  CONSTRAINT [DF_Events_Venue_LeadershipBreakfastClosed]  DEFAULT ((0)) FOR [LeadershipBreakfastClosed]
GO
