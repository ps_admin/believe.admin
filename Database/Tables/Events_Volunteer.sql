SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Events_Volunteer](
	[Contact_ID] [int] NOT NULL,
	[ConferenceDepartmentMapping_ID] [int] NOT NULL,
	[Volunteer_FormReceived] [date] NULL,
 CONSTRAINT [Events_Volunteer_PK] PRIMARY KEY CLUSTERED 
(
	[Contact_ID] ASC,
	[ConferenceDepartmentMapping_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Events_Volunteer]  WITH CHECK ADD  CONSTRAINT [Events_Volunteer_ConferenceDepartmentMapping_ID_FK] FOREIGN KEY([ConferenceDepartmentMapping_ID])
REFERENCES [dbo].[Events_ConferenceDepartmentMapping] ([ConferenceDepartmentMapping_ID])
GO
ALTER TABLE [dbo].[Events_Volunteer] CHECK CONSTRAINT [Events_Volunteer_ConferenceDepartmentMapping_ID_FK]
GO
ALTER TABLE [dbo].[Events_Volunteer]  WITH CHECK ADD  CONSTRAINT [Events_Volunteer_Contact_ID_FK] FOREIGN KEY([Contact_ID])
REFERENCES [dbo].[Common_Contact] ([Contact_ID])
GO
ALTER TABLE [dbo].[Events_Volunteer] CHECK CONSTRAINT [Events_Volunteer_Contact_ID_FK]
GO
