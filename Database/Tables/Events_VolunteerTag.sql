SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Events_VolunteerTag](
	[VolunteerTag_ID] [varchar](10) NOT NULL,
	[Contact_ID] [int] NOT NULL,
	[VolunteerTag_Active] [bit] NULL,
	[VolunteerTag_IssueDate] [date] NOT NULL,
	[VolunteerTag_DeactivationDate] [date] NULL,
 CONSTRAINT [PK_Events_VolunteerTag] PRIMARY KEY CLUSTERED 
(
	[VolunteerTag_ID] ASC,
	[Contact_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Events_VolunteerTag]  WITH CHECK ADD  CONSTRAINT [Events_VolunteerTag_Contact_ID_FK] FOREIGN KEY([Contact_ID])
REFERENCES [dbo].[Common_Contact] ([Contact_ID])
GO
ALTER TABLE [dbo].[Events_VolunteerTag] CHECK CONSTRAINT [Events_VolunteerTag_Contact_ID_FK]
GO
