SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Events_VolunteerTagLoan](
	[VolunteerTag_ID] [varchar](10) NOT NULL,
	[ConferenceDepartmentMapping_ID] [int] NOT NULL,
	[Contact_ID] [int] NULL,
	[VolunteerTagLoan_CheckOut] [date] NOT NULL,
	[VolunteerTagLoan_CheckIn] [date] NULL,
PRIMARY KEY CLUSTERED 
(
	[VolunteerTag_ID] ASC,
	[ConferenceDepartmentMapping_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Events_VolunteerTagLoan]  WITH CHECK ADD  CONSTRAINT [Events_VolunteerTagLoan_ConferenceDepartmentMapping_ID_FK] FOREIGN KEY([ConferenceDepartmentMapping_ID])
REFERENCES [dbo].[Events_ConferenceDepartmentMapping] ([ConferenceDepartmentMapping_ID])
GO
ALTER TABLE [dbo].[Events_VolunteerTagLoan] CHECK CONSTRAINT [Events_VolunteerTagLoan_ConferenceDepartmentMapping_ID_FK]
GO
ALTER TABLE [dbo].[Events_VolunteerTagLoan]  WITH CHECK ADD  CONSTRAINT [Events_VolunteerTagLoan_Contact_ID_FK] FOREIGN KEY([Contact_ID])
REFERENCES [dbo].[Common_Contact] ([Contact_ID])
GO
ALTER TABLE [dbo].[Events_VolunteerTagLoan] CHECK CONSTRAINT [Events_VolunteerTagLoan_Contact_ID_FK]
GO
ALTER TABLE [dbo].[Events_VolunteerTagLoan]  WITH CHECK ADD  CONSTRAINT [Events_VolunteerTagLoan_VolunteerTag_ID_FK] FOREIGN KEY([VolunteerTag_ID], [Contact_ID])
REFERENCES [dbo].[Events_VolunteerTag] ([VolunteerTag_ID], [Contact_ID])
GO
ALTER TABLE [dbo].[Events_VolunteerTagLoan] CHECK CONSTRAINT [Events_VolunteerTagLoan_VolunteerTag_ID_FK]
GO
