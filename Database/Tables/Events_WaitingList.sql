SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Events_WaitingList](
	[WaitingList_ID] [int] IDENTITY(237,1) NOT NULL,
	[Contact_ID] [int] NOT NULL,
	[Venue_ID] [int] NOT NULL,
	[DateAdded] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_WaitingList] PRIMARY KEY CLUSTERED 
(
	[WaitingList_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Events_WaitingList]  WITH CHECK ADD  CONSTRAINT [FK_Events_WaitingList_Common_ContactExternalEvents] FOREIGN KEY([Contact_ID])
REFERENCES [dbo].[Common_ContactExternalEvents] ([Contact_ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Events_WaitingList] CHECK CONSTRAINT [FK_Events_WaitingList_Common_ContactExternalEvents]
GO
ALTER TABLE [dbo].[Events_WaitingList]  WITH CHECK ADD  CONSTRAINT [FK_WaitingList_PSVenue] FOREIGN KEY([Venue_ID])
REFERENCES [dbo].[Events_Venue] ([Venue_ID])
GO
ALTER TABLE [dbo].[Events_WaitingList] CHECK CONSTRAINT [FK_WaitingList_PSVenue]
GO
