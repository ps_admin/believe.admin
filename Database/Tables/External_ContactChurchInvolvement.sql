SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[External_ContactChurchInvolvement](
	[ContactChurchInvolvement_ID] [int] IDENTITY(1,1) NOT NULL,
	[Contact_ID] [int] NOT NULL,
	[ChurchInvolvement_ID] [int] NOT NULL,
	[DateAdded] [datetime] NOT NULL,
 CONSTRAINT [PK_External_ContactChurchInvolvement] PRIMARY KEY CLUSTERED 
(
	[ContactChurchInvolvement_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[External_ContactChurchInvolvement]  WITH CHECK ADD  CONSTRAINT [FK_External_ContactChurchInvolvement_Common_ContactExternal] FOREIGN KEY([Contact_ID])
REFERENCES [dbo].[Common_ContactExternal] ([Contact_ID])
GO
ALTER TABLE [dbo].[External_ContactChurchInvolvement] CHECK CONSTRAINT [FK_External_ContactChurchInvolvement_Common_ContactExternal]
GO
ALTER TABLE [dbo].[External_ContactChurchInvolvement]  WITH CHECK ADD  CONSTRAINT [FK_External_ContactChurchInvolvement_External_ChurchInvolvement] FOREIGN KEY([ChurchInvolvement_ID])
REFERENCES [dbo].[External_ChurchInvolvement] ([ChurchInvolvement_ID])
GO
ALTER TABLE [dbo].[External_ContactChurchInvolvement] CHECK CONSTRAINT [FK_External_ContactChurchInvolvement_External_ChurchInvolvement]
GO
