SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Revolution_ActivationCode](
	[ActivationCode_ID] [int] IDENTITY(1,1) NOT NULL,
	[SerialNumber] [int] NOT NULL,
	[ActivationCode] [nvarchar](15) NOT NULL,
	[CardActivated] [bit] NOT NULL,
	[SubscriptionActivated] [bit] NOT NULL,
	[Subscription_ID] [int] NOT NULL,
	[Contact_ID] [int] NULL,
	[ActivationDate] [datetime] NULL,
	[ActivatedBy_ID] [int] NULL,
 CONSTRAINT [PK_Revolution_ActivationCode] PRIMARY KEY CLUSTERED 
(
	[ActivationCode_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Unique_Revolution_ActivationCode_ActivationCode] ON [dbo].[Revolution_ActivationCode] 
(
	[ActivationCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Unique_Revolution_ActivationCode_SerialNumber] ON [dbo].[Revolution_ActivationCode] 
(
	[SerialNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Revolution_ActivationCode]  WITH CHECK ADD  CONSTRAINT [FK_Revolution_ActivationCode_Common_ContactExternalRevolution] FOREIGN KEY([Contact_ID])
REFERENCES [dbo].[Common_ContactExternalRevolution] ([Contact_ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Revolution_ActivationCode] CHECK CONSTRAINT [FK_Revolution_ActivationCode_Common_ContactExternalRevolution]
GO
ALTER TABLE [dbo].[Revolution_ActivationCode]  WITH CHECK ADD  CONSTRAINT [FK_Revolution_ActivationCode_Common_UserDetail] FOREIGN KEY([ActivatedBy_ID])
REFERENCES [dbo].[Common_UserDetail] ([Contact_ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Revolution_ActivationCode] CHECK CONSTRAINT [FK_Revolution_ActivationCode_Common_UserDetail]
GO
ALTER TABLE [dbo].[Revolution_ActivationCode]  WITH CHECK ADD  CONSTRAINT [FK_Revolution_ActivationCode_Revolution_Subscription] FOREIGN KEY([Subscription_ID])
REFERENCES [dbo].[Revolution_Subscription] ([Subscription_ID])
GO
ALTER TABLE [dbo].[Revolution_ActivationCode] CHECK CONSTRAINT [FK_Revolution_ActivationCode_Revolution_Subscription]
GO
