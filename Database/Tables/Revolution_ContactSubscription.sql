SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Revolution_ContactSubscription](
	[ContactSubscription_ID] [int] IDENTITY(1,1) NOT NULL,
	[Contact_ID] [int] NOT NULL,
	[Subscription_ID] [int] NOT NULL,
	[SignupDate] [datetime] NOT NULL,
	[SubscriptionDiscount] [decimal](18, 2) NOT NULL,
	[SubscriptionStartDate] [datetime] NOT NULL,
	[SubscriptionCancelled] [bit] NOT NULL,
	[EndOfTrialEmailSent] [bit] NOT NULL,
	[SubscribedBy_ID] [int] NOT NULL,
 CONSTRAINT [PK_Revolution_ContactSubscription] PRIMARY KEY CLUSTERED 
(
	[ContactSubscription_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Revolution_ContactSubscription]  WITH CHECK ADD  CONSTRAINT [FK_Revolution_ContactSubscription_Common_ContactExternalRevolution] FOREIGN KEY([Contact_ID])
REFERENCES [dbo].[Common_ContactExternalRevolution] ([Contact_ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Revolution_ContactSubscription] CHECK CONSTRAINT [FK_Revolution_ContactSubscription_Common_ContactExternalRevolution]
GO
ALTER TABLE [dbo].[Revolution_ContactSubscription]  WITH CHECK ADD  CONSTRAINT [FK_Revolution_ContactSubscription_Common_UserDetail] FOREIGN KEY([SubscribedBy_ID])
REFERENCES [dbo].[Common_UserDetail] ([Contact_ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Revolution_ContactSubscription] CHECK CONSTRAINT [FK_Revolution_ContactSubscription_Common_UserDetail]
GO
ALTER TABLE [dbo].[Revolution_ContactSubscription]  WITH CHECK ADD  CONSTRAINT [FK_Revolution_ContactSubscription_Revolution_Subscription] FOREIGN KEY([Subscription_ID])
REFERENCES [dbo].[Revolution_Subscription] ([Subscription_ID])
GO
ALTER TABLE [dbo].[Revolution_ContactSubscription] CHECK CONSTRAINT [FK_Revolution_ContactSubscription_Revolution_Subscription]
GO
ALTER TABLE [dbo].[Revolution_ContactSubscription] ADD  CONSTRAINT [DF_Revolution_ContactSubscription_EndOfTrialEmailSent]  DEFAULT ((0)) FOR [EndOfTrialEmailSent]
GO
