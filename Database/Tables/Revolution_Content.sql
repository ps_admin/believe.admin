SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Revolution_Content](
	[Content_ID] [int] IDENTITY(1,1) NOT NULL,
	[ContentPack_ID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[AuthenticationRequired] [bit] NOT NULL,
	[RestrictedDownload] [bit] NOT NULL,
	[MediaPlayerBackgroundImage] [image] NULL,
	[ThumbnailImage] [image] NULL,
	[Description] [nvarchar](200) NOT NULL,
	[MoreDetailsLink] [nvarchar](100) NOT NULL,
	[Visible] [bit] NOT NULL,
	[SortOrder] [int] NOT NULL,
	[GUID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_Revolution_Content] PRIMARY KEY CLUSTERED 
(
	[Content_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Revolution_Content]  WITH CHECK ADD  CONSTRAINT [FK_Revolution_Content_Revolution_ContentPack] FOREIGN KEY([ContentPack_ID])
REFERENCES [dbo].[Revolution_ContentPack] ([ContentPack_ID])
GO
ALTER TABLE [dbo].[Revolution_Content] CHECK CONSTRAINT [FK_Revolution_Content_Revolution_ContentPack]
GO
ALTER TABLE [dbo].[Revolution_Content] ADD  CONSTRAINT [DF_Revolution_Content_Visible]  DEFAULT ((1)) FOR [Visible]
GO
ALTER TABLE [dbo].[Revolution_Content] ADD  CONSTRAINT [DF_Revolution_Content_GUID]  DEFAULT (newid()) FOR [GUID]
GO
