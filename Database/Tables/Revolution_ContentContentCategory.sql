SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Revolution_ContentContentCategory](
	[ContentContentCategory_ID] [int] IDENTITY(1,1) NOT NULL,
	[Content_ID] [int] NOT NULL,
	[ContentSubCategory_ID] [int] NOT NULL,
	[SortOrder] [int] NOT NULL,
 CONSTRAINT [PK_Revolution_ContentContentCategory] PRIMARY KEY CLUSTERED 
(
	[ContentContentCategory_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Revolution_ContentContentCategory]  WITH CHECK ADD  CONSTRAINT [FK_Revolution_ContentContentCategory_Revolution_Content] FOREIGN KEY([Content_ID])
REFERENCES [dbo].[Revolution_Content] ([Content_ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Revolution_ContentContentCategory] CHECK CONSTRAINT [FK_Revolution_ContentContentCategory_Revolution_Content]
GO
ALTER TABLE [dbo].[Revolution_ContentContentCategory]  WITH CHECK ADD  CONSTRAINT [FK_Revolution_ContentContentCategory_Revolution_ContentSubCategory] FOREIGN KEY([ContentSubCategory_ID])
REFERENCES [dbo].[Revolution_ContentSubCategory] ([ContentSubCategory_ID])
GO
ALTER TABLE [dbo].[Revolution_ContentContentCategory] CHECK CONSTRAINT [FK_Revolution_ContentContentCategory_Revolution_ContentSubCategory]
GO
