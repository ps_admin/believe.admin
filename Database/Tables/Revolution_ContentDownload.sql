SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Revolution_ContentDownload](
	[ContentDownload_ID] [int] IDENTITY(1,1) NOT NULL,
	[Contact_ID] [int] NOT NULL,
	[ContentFile_ID] [int] NOT NULL,
	[ClientIP] [nvarchar](15) NOT NULL,
	[DownloadDate] [datetime] NOT NULL,
	[MediaPlayerDownload] [bit] NOT NULL,
	[ResetDownload] [bit] NOT NULL,
 CONSTRAINT [PK_Revolution_ContentDownload] PRIMARY KEY CLUSTERED 
(
	[ContentDownload_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Revolution_ContentDownload]  WITH CHECK ADD  CONSTRAINT [FK_Revolution_ContentDownload_Common_Contact] FOREIGN KEY([Contact_ID])
REFERENCES [dbo].[Common_Contact] ([Contact_ID])
GO
ALTER TABLE [dbo].[Revolution_ContentDownload] CHECK CONSTRAINT [FK_Revolution_ContentDownload_Common_Contact]
GO
ALTER TABLE [dbo].[Revolution_ContentDownload]  WITH CHECK ADD  CONSTRAINT [FK_Revolution_ContentDownload_Revolution_ContentFile] FOREIGN KEY([ContentFile_ID])
REFERENCES [dbo].[Revolution_ContentFile] ([ContentFile_ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Revolution_ContentDownload] CHECK CONSTRAINT [FK_Revolution_ContentDownload_Revolution_ContentFile]
GO
