SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Revolution_ContentFile](
	[ContentFile_ID] [int] IDENTITY(1,1) NOT NULL,
	[Content_ID] [int] NOT NULL,
	[FileName] [nvarchar](100) NOT NULL,
	[AllowDownload] [bit] NOT NULL,
	[ExternalDownloadLocation] [nvarchar](80) NOT NULL,
	[ContentFileType_ID] [int] NOT NULL,
	[ContentFileCategory_ID] [int] NOT NULL,
	[Visible] [bit] NOT NULL,
	[SortOrder] [int] NOT NULL,
	[GUID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_Revolution_ContentFile] PRIMARY KEY CLUSTERED 
(
	[ContentFile_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Revolution_ContentFile]  WITH CHECK ADD  CONSTRAINT [FK_Revolution_ContentFile_Revolution_Content] FOREIGN KEY([Content_ID])
REFERENCES [dbo].[Revolution_Content] ([Content_ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Revolution_ContentFile] CHECK CONSTRAINT [FK_Revolution_ContentFile_Revolution_Content]
GO
ALTER TABLE [dbo].[Revolution_ContentFile]  WITH CHECK ADD  CONSTRAINT [FK_Revolution_ContentFile_Revolution_ContentFileCategory] FOREIGN KEY([ContentFileCategory_ID])
REFERENCES [dbo].[Revolution_ContentFileCategory] ([ContentFileCategory_ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Revolution_ContentFile] CHECK CONSTRAINT [FK_Revolution_ContentFile_Revolution_ContentFileCategory]
GO
ALTER TABLE [dbo].[Revolution_ContentFile]  WITH CHECK ADD  CONSTRAINT [FK_Revolution_ContentFile_Revolution_ContentFileType] FOREIGN KEY([ContentFileType_ID])
REFERENCES [dbo].[Revolution_ContentFileType] ([ContentFileType_ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Revolution_ContentFile] CHECK CONSTRAINT [FK_Revolution_ContentFile_Revolution_ContentFileType]
GO
ALTER TABLE [dbo].[Revolution_ContentFile] ADD  CONSTRAINT [DF_Revolution_ContentFile_Visible]  DEFAULT ((1)) FOR [Visible]
GO
ALTER TABLE [dbo].[Revolution_ContentFile] ADD  CONSTRAINT [DF_Revolution_ContentFile_GUID]  DEFAULT (newid()) FOR [GUID]
GO
