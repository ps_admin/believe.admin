SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Revolution_ContentSubCategory](
	[ContentSubCategory_ID] [int] IDENTITY(1,1) NOT NULL,
	[ContentCategory_ID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[SortOrder] [int] NOT NULL,
 CONSTRAINT [PK_Revolution_ContentSubCategory] PRIMARY KEY CLUSTERED 
(
	[ContentSubCategory_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Revolution_ContentSubCategory]  WITH CHECK ADD  CONSTRAINT [FK_Revolution_ContentSubCategory_Revolution_ContentCategory] FOREIGN KEY([ContentCategory_ID])
REFERENCES [dbo].[Revolution_ContentCategory] ([ContentCategory_ID])
GO
ALTER TABLE [dbo].[Revolution_ContentSubCategory] CHECK CONSTRAINT [FK_Revolution_ContentSubCategory_Revolution_ContentCategory]
GO
