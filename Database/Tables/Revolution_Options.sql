SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Revolution_Options](
	[PremiumDownloadLimit] [int] NOT NULL,
	[DownloadWindow] [int] NOT NULL,
	[ContentFolder] [nvarchar](100) NULL,
	[BankAccount_ID] [int] NOT NULL
) ON [PRIMARY]
GO
