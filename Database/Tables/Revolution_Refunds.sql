SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Revolution_Refunds](
	[ContactSubscription_ID] [int] NOT NULL,
	[Contact_ID] [int] NOT NULL,
	[Firstname] [nvarchar](30) NOT NULL,
	[LastName] [nvarchar](30) NOT NULL,
	[Email] [nvarchar](80) NOT NULL,
	[SubscriptionStartDate] [datetime] NOT NULL,
	[subscriptionlength] [int] NULL,
	[SubscriptionEndDate] [datetime] NULL,
	[ExcessMonths] [int] NULL,
	[CCTransactionRef] [nvarchar](20) NULL,
	[CCNumber] [nvarchar](20) NULL,
	[CCExpiry] [nvarchar](20) NULL,
	[CCName] [nvarchar](20) NULL,
	[PaymentAmount] [money] NULL,
	[Refund] [decimal](18, 2) NULL,
	[RefundCompleted] [bit] NULL,
	[TXRef] [nvarchar](50) NULL,
	[Response] [nvarchar](100) NULL
) ON [PRIMARY]
GO
