SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Revolution_SignInLog](
	[SigninLog_ID] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NOT NULL,
	[ClientIP] [nvarchar](15) NOT NULL,
	[Email] [nvarchar](100) NOT NULL,
	[Password] [nvarchar](100) NOT NULL,
	[Success] [bit] NOT NULL,
	[Contact_ID] [int] NULL,
	[CurrentMember] [bit] NOT NULL,
 CONSTRAINT [PK_Revolution_SigninLog] PRIMARY KEY CLUSTERED 
(
	[SigninLog_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Revolution_SignInLog]  WITH CHECK ADD  CONSTRAINT [FK_Revolution_SignInLog_Common_Contact] FOREIGN KEY([Contact_ID])
REFERENCES [dbo].[Common_Contact] ([Contact_ID])
GO
ALTER TABLE [dbo].[Revolution_SignInLog] CHECK CONSTRAINT [FK_Revolution_SignInLog_Common_Contact]
GO
