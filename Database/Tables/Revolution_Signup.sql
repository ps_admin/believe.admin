SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Revolution_Signup](
	[Subscription_ID] [int] IDENTITY(1,1) NOT NULL,
	[Salutation_ID] [int] NOT NULL,
	[FirstName] [nvarchar](30) NOT NULL,
	[LastName] [nvarchar](30) NOT NULL,
	[DateOfBirth] [smalldatetime] NOT NULL,
	[Gender] [nvarchar](6) NOT NULL,
	[Address1] [nvarchar](100) NOT NULL,
	[Address2] [nvarchar](100) NOT NULL,
	[Suburb] [nvarchar](20) NOT NULL,
	[Postcode] [nvarchar](50) NOT NULL,
	[State_ID] [int] NOT NULL,
	[StateOther] [nvarchar](20) NOT NULL,
	[Country_ID] [int] NOT NULL,
	[Phone] [nvarchar](20) NOT NULL,
	[Mobile] [nvarchar](20) NOT NULL,
	[Email] [nvarchar](80) NOT NULL,
	[Password] [nvarchar](20) NOT NULL,
	[ChurchName] [nvarchar](50) NOT NULL,
	[Denomination_ID] [int] NOT NULL,
	[ChurchSuburb] [nvarchar](50) NOT NULL,
	[ChurchState_ID] [int] NULL,
	[ChurchStateOther] [nvarchar](20) NOT NULL,
	[ChurchCountry_ID] [int] NULL,
	[ChurchWebsite] [nvarchar](50) NOT NULL,
	[ReferredBy_ID] [int] NOT NULL,
	[ReferredByOther] [nvarchar](50) NOT NULL,
	[CCNumber] [nvarchar](20) NOT NULL,
	[CCExpiry] [nvarchar](5) NOT NULL,
	[CCName] [nvarchar](50) NOT NULL,
	[CCPhone] [nvarchar](50) NOT NULL,
	[CCTransactionRef] [nvarchar](20) NOT NULL,
	[SignupDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Revolution_Signup] PRIMARY KEY CLUSTERED 
(
	[Subscription_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Revolution_Signup] ADD  CONSTRAINT [DF_Revolution_Signup_SignupDate]  DEFAULT (getdate()) FOR [SignupDate]
GO
