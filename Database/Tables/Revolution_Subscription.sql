SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Revolution_Subscription](
	[Subscription_ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Cost] [money] NOT NULL,
	[Length] [int] NOT NULL,
	[SubscriptionLength] [int] NULL,
	[SubscriptionActive] [int] NULL,
	[BankAccount_ID] [int] NOT NULL,
	[OneTimeSubscriptionOnly] [bit] NOT NULL,
 CONSTRAINT [PK_Revolution_Subscription] PRIMARY KEY CLUSTERED 
(
	[Subscription_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Revolution_Subscription]  WITH CHECK ADD  CONSTRAINT [FK_Revolution_Subscription_Common_BankAccount] FOREIGN KEY([BankAccount_ID])
REFERENCES [dbo].[Common_BankAccount] ([BankAccount_ID])
GO
ALTER TABLE [dbo].[Revolution_Subscription] CHECK CONSTRAINT [FK_Revolution_Subscription_Common_BankAccount]
GO
