SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Revolution_SubscriptionContentPack](
	[SubscriptionContentPack_ID] [int] IDENTITY(1,1) NOT NULL,
	[Subscription_ID] [int] NOT NULL,
	[ContentPack_ID] [int] NOT NULL,
 CONSTRAINT [PK_Revolution_SubscriptionContentPack] PRIMARY KEY CLUSTERED 
(
	[SubscriptionContentPack_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Revolution_SubscriptionContentPack]  WITH CHECK ADD  CONSTRAINT [FK_Revolution_SubscriptionContentPack_Revolution_ContentPack] FOREIGN KEY([ContentPack_ID])
REFERENCES [dbo].[Revolution_ContentPack] ([ContentPack_ID])
GO
ALTER TABLE [dbo].[Revolution_SubscriptionContentPack] CHECK CONSTRAINT [FK_Revolution_SubscriptionContentPack_Revolution_ContentPack]
GO
ALTER TABLE [dbo].[Revolution_SubscriptionContentPack]  WITH CHECK ADD  CONSTRAINT [FK_Revolution_SubscriptionContentPack_Revolution_Subscription] FOREIGN KEY([Subscription_ID])
REFERENCES [dbo].[Revolution_Subscription] ([Subscription_ID])
GO
ALTER TABLE [dbo].[Revolution_SubscriptionContentPack] CHECK CONSTRAINT [FK_Revolution_SubscriptionContentPack_Revolution_Subscription]
GO
