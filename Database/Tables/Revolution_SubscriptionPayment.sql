SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Revolution_SubscriptionPayment](
	[SubscriptionPayment_ID] [int] IDENTITY(1,1) NOT NULL,
	[ContactSubscription_ID] [int] NOT NULL,
	[PaymentType_ID] [int] NOT NULL,
	[PaymentAmount] [money] NOT NULL,
	[CCNumber] [nvarchar](20) NOT NULL,
	[CCExpiry] [nvarchar](20) NOT NULL,
	[CCName] [nvarchar](20) NOT NULL,
	[CCPhone] [nvarchar](20) NOT NULL,
	[CCManual] [bit] NOT NULL,
	[CCTransactionRef] [nvarchar](20) NOT NULL,
	[CCRefund] [bit] NOT NULL,
	[ChequeDrawer] [nvarchar](50) NOT NULL,
	[ChequeBank] [nvarchar](50) NOT NULL,
	[ChequeBranch] [nvarchar](30) NOT NULL,
	[Comment] [nvarchar](200) NOT NULL,
	[PaymentDate] [datetime] NOT NULL,
	[PaymentBy_ID] [int] NOT NULL,
	[BankAccount_ID] [int] NOT NULL,
 CONSTRAINT [PK_Revolution_SubscriptionPayment] PRIMARY KEY CLUSTERED 
(
	[SubscriptionPayment_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Revolution_SubscriptionPayment]  WITH CHECK ADD  CONSTRAINT [FK_Revolution_SubscriptionPayment_Common_BankAccount] FOREIGN KEY([BankAccount_ID])
REFERENCES [dbo].[Common_BankAccount] ([BankAccount_ID])
GO
ALTER TABLE [dbo].[Revolution_SubscriptionPayment] CHECK CONSTRAINT [FK_Revolution_SubscriptionPayment_Common_BankAccount]
GO
ALTER TABLE [dbo].[Revolution_SubscriptionPayment]  WITH CHECK ADD  CONSTRAINT [FK_Revolution_SubscriptionPayment_Common_PaymentType] FOREIGN KEY([PaymentType_ID])
REFERENCES [dbo].[Common_PaymentType] ([PaymentType_ID])
GO
ALTER TABLE [dbo].[Revolution_SubscriptionPayment] CHECK CONSTRAINT [FK_Revolution_SubscriptionPayment_Common_PaymentType]
GO
ALTER TABLE [dbo].[Revolution_SubscriptionPayment]  WITH CHECK ADD  CONSTRAINT [FK_Revolution_SubscriptionPayment_Common_UserDetail] FOREIGN KEY([PaymentBy_ID])
REFERENCES [dbo].[Common_UserDetail] ([Contact_ID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Revolution_SubscriptionPayment] CHECK CONSTRAINT [FK_Revolution_SubscriptionPayment_Common_UserDetail]
GO
ALTER TABLE [dbo].[Revolution_SubscriptionPayment]  WITH CHECK ADD  CONSTRAINT [FK_Revolution_SubscriptionPayment_Revolution_ContactSubscription] FOREIGN KEY([ContactSubscription_ID])
REFERENCES [dbo].[Revolution_ContactSubscription] ([ContactSubscription_ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Revolution_SubscriptionPayment] CHECK CONSTRAINT [FK_Revolution_SubscriptionPayment_Revolution_ContactSubscription]
GO
