SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_Church_CurrentPCRReportDate]

AS
SELECT	TOP 1 * 
FROM	Church_PCRDate
WHERE	Date <= GetDate()
AND		Deleted = 0
ORDER BY [Date] DESC
GO
