SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_Church_HistoryPartnership]

AS
SELECT	[Date],
		ChurchStatus_ID, 
		Campus_ID, 
		Ministry_ID, 
		Region_ID,
		Number
FROM	Church_HistoryPartnership
WHERE	ChurchStatus_ID in (1,2,3,4,5,6)
AND		[Date] < '1-' + LEFT(DATENAME(m, GetDate()),3) + ' ' + CONVERT(NVARCHAR,YEAR(GetDate()))

UNION ALL

SELECT		LEFT(DATENAME(m, GetDate()),3) + ' ' + CONVERT(NVARCHAR,YEAR(GetDate())) as [Date],
			ChurchStatus_ID,
			Campus_ID,
			Ministry_ID,
			Region_ID,
			COUNT(*)
FROM		Common_ContactInternal
WHERE		ChurchStatus_ID in (1,2,3,4,5,6)
GROUP BY	ChurchStatus_ID,
			Campus_ID,
			Ministry_ID,
			Region_ID
GO
