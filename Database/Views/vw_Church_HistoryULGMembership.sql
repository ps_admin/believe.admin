SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_Church_HistoryULGMembership]

AS

SELECT		[Date],
			ULG_ID,
			ChurchStatus_ID, 
			Ministry_ID, 
			Region_ID,
			Number
FROM		Church_HistoryULGMembership
WHERE		[Date] < '1-' + LEFT(DATENAME(m, GetDate()),3) + ' ' + CONVERT(NVARCHAR,YEAR(GetDate()))

UNION ALL

SELECT		LEFT(DATENAME(m, GetDate()),3) + ' ' + CONVERT(NVARCHAR,YEAR(GetDate())) as [Date],
			ULG_ID,
			ChurchStatus_ID,
			Ministry_ID,
			Region_ID,
			COUNT(*)
FROM		Common_ContactInternal CI,
			Church_ULGContact C
WHERE		CI.Contact_ID = C.Contact_ID
AND			C.Inactive = 0
GROUP BY	ULG_ID,
			ChurchStatus_ID,
			Ministry_ID,
			Region_ID
GO
