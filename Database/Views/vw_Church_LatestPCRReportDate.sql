SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_Church_LatestPCRReportDate]

AS
SELECT	TOP 1 * 
FROM	Church_PCRDate
WHERE	DATEADD(hh,(SELECT PCRDueHours FROM Church_PCROptions), Date) > GetDate()
AND		Deleted = 0
ORDER BY [Date] ASC
GO
