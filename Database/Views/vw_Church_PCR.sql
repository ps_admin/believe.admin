SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_Church_PCR]

AS
SELECT		C.Contact_ID,
			CI.ChurchStatus_ID,
			U.Campus_ID,
			U.Ministry_ID,
			U.Region_ID,
			D.Date,
			P.ULG_ID,
			SundayAttendance,
			ULGAttendance,
			BoomAttendance,
			Call,
			Visit,
			Other
FROM		Common_ContactInternal CI,
			Church_PCRContact C,
			Church_PCR P,
			Church_PCRDate D,
			Church_ULG U
WHERE		CI.Contact_ID = C.Contact_ID
AND			C.PCR_ID = P.PCR_ID
AND			P.PCRDate_ID = D.PCRDate_ID
AND			P.ULG_ID = U.ULG_ID
GO
