SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_Common_ContactAddressView]
AS
SELECT		C.Contact_ID, 
			Address1 +
			CASE WHEN C.Address2 <> '' THEN ', ' + C.Address2 ELSE '' END +
			CASE WHEN C.Suburb <> '' THEN ', ' + C.Suburb ELSE '' END +
			ISNULL(CASE WHEN C.State_ID <> 9 THEN ', ' + S.State ELSE (CASE WHEN C.StateOther <> '' THEN ', ' + C.StateOther ELSE '' END) END, '') +
			CASE WHEN C.Postcode <> '' THEN ', ' + C.PostCode ELSE '' END +
			ISNULL(CASE WHEN C.Country_ID <> 9 THEN ', ' + Co.Country ELSE '' END,'') as StreetAddress,
			Address1 +
			CASE WHEN C.Address2 <> '' THEN CHAR(10) + C.Address2 ELSE '' END +
			CASE WHEN C.Suburb <> '' THEN CHAR(10) + C.Suburb ELSE '' END +
			ISNULL(CASE WHEN C.State_ID <> 9 THEN ', ' + S.State ELSE (CASE WHEN C.StateOther <> '' THEN ', ' + C.StateOther ELSE '' END) END, '') +
			CASE WHEN C.Postcode <> '' THEN ', ' + C.PostCode ELSE '' END +
			ISNULL(CASE WHEN C.Country_ID <> 9 THEN CHAR(10) + Co.Country ELSE '' END,'') as StreetAddressMultiLine, 
			ISNULL(PostalAddress1 +
			CASE WHEN CI.PostalAddress2 <> '' THEN ', ' + CI.PostalAddress2 ELSE '' END +
			CASE WHEN CI.PostalSuburb <> '' THEN ', ' + CI.PostalSuburb ELSE '' END +
			ISNULL(CASE WHEN CI.PostalState_ID <> 9 THEN ', ' + SI.State ELSE (CASE WHEN CI.PostalStateOther <> '' THEN ', ' + CI.PostalStateOther ELSE '' END) END, '') +
			CASE WHEN CI.PostalPostcode <> '' THEN ', ' + CI.PostalPostcode ELSE '' END +
			ISNULL(CASE WHEN CI.PostalCountry_ID <> 9 THEN ', ' + CoI.Country ELSE '' END, ''), '')  as PostalAddress,
			InternalExternal = CASE WHEN CI.Contact_ID IS NULL AND CE.Contact_ID IS NULL THEN 'None'
									WHEN CI.Contact_ID IS NOT NULL AND CE.Contact_ID IS NULL THEN 'Internal'
									WHEN CI.Contact_ID IS NULL AND CE.Contact_ID IS NOT NULL THEN 'External'
									WHEN CI.Contact_ID IS NOT NULL AND CE.Contact_ID IS NOT NULL THEN 'Both'
							   END
FROM        Common_Contact C 
				LEFT JOIN Common_GeneralState S
					ON C.State_ID = S.State_ID
				LEFT JOIN Common_GeneralCountry Co
					ON C.Country_ID = Co.Country_ID
				LEFT JOIN Common_ContactInternal CI
					ON C.Contact_ID = CI.Contact_ID
				LEFT JOIN Common_ContactExternal CE
					ON C.Contact_ID = CE.Contact_ID
				LEFT JOIN Common_GeneralState SI
					ON CI.PostalState_ID = SI.State_ID
				LEFT JOIN Common_GeneralCountry CoI
					ON CI.PostalCountry_ID = CoI.Country_ID
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Common_Contact"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 271
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_Common_ContactAddressView'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_Common_ContactAddressView'
GO
