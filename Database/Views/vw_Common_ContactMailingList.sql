SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_Common_ContactMailingList]

AS

SELECT		ML.MailingList_ID,
			MLT.MailingListType_ID,
			C.Contact_ID,
			LT.[Name] as MailingListType
FROM		Common_Contact C,
			Common_ContactMailingList ML,
			Common_ContactMailingListType MLT,
			Common_MailingListType LT
WHERE		C.Contact_ID = ML.Contact_ID
AND			C.Contact_ID = MLT.Contact_ID
AND			MLT.MailingListType_ID = LT.MailingListType_ID
AND			ML.SubscriptionActive = 1
AND			MLT.SubscriptionActive = 1
GO
