SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_Events_GroupBulkRegistrationView]

AS
SELECT		GRV.* 
FROM (
	SELECT		G.GroupLeader_ID,
				CC.State_ID,
				C.Conference_ID,
				V.Venue_ID,
				RT.RegistrationType_ID,
				RT.RegistrationCost,
				SUM(R.Purchased) as Purchased,
				SUM(CASE WHEN R.Purchased < R.Allocated THEN R.Purchased ELSE R.Allocated END) as Allocated,
				SUM(CASE WHEN R.Purchased - R.Allocated > 0 THEN R.Purchased - R.Allocated ELSE 0 END) as UnAllocated,
				RT.RegistrationCost * SUM(Purchased) AS TotalCost
	FROM		Events_Group G,
				Events_Conference C,
				Events_Venue V,
				Events_RegistrationType RT,
				Events_RegistrationTypeVenue RTV,
				Common_Contact CC,
				(
					SELECT	G2.GroupLeader_ID,
							V2.Venue_ID,
							RT2.RegistrationType_ID,
							ISNULL((SELECT SUM(GB.Quantity) FROM Events_GroupBulkRegistration GB WHERE GB.GroupLeader_ID = G2.GroupLeader_ID AND GB.Venue_ID = V2.Venue_ID AND GB.RegistrationType_ID = RT2.RegistrationType_ID),0) as Purchased,
							ISNULL((SELECT COUNT(*) FROM Common_ContactExternal CE, Events_Registration Re WHERE CE.Deleted = 0 AND CE.Contact_ID = Re.Contact_ID AND Re.Venue_ID = V2.Venue_ID AND Re.GroupLeader_ID = G2.GroupLeader_ID AND Re.RegistrationType_ID = RT2.RegistrationType_ID AND Re.Venue_ID = V2.Venue_ID),0) as Allocated
					FROM	Events_Group G2,
							Events_Conference C2,
							Events_Venue V2,
							Events_RegistrationType RT2,
							Events_RegistrationTypeVenue RTV2
					WHERE	V2.Conference_ID = C2.Conference_ID
					AND		V2.Venue_ID = RTV2.Venue_ID
					AND		RTV2.RegistrationType_ID = RT2.RegistrationType_ID
					GROUP BY G2.GroupLeader_ID,
							V2.Venue_ID,
							RT2.RegistrationType_ID
					HAVING	ISNULL((SELECT SUM(GB.Quantity) FROM Events_GroupBulkRegistration GB WHERE GB.GroupLeader_ID = G2.GroupLeader_ID AND GB.Venue_ID = V2.Venue_ID AND GB.RegistrationType_ID = RT2.RegistrationType_ID),0) <> 0
					OR		ISNULL((SELECT COUNT(*) FROM Common_ContactExternal CE, Events_Registration Re WHERE CE.Deleted = 0 AND CE.Contact_ID = Re.Contact_ID AND Re.Venue_ID = V2.Venue_ID AND Re.GroupLeader_ID = G2.GroupLeader_ID AND Re.RegistrationType_ID = RT2.RegistrationType_ID AND Re.Venue_ID = V2.Venue_ID),0) <> 0
				) R
	WHERE		V.Conference_ID = C.Conference_ID
	AND			G.GroupLeader_ID = CC.Contact_ID
	AND			V.Venue_ID = RTV.Venue_ID
	AND			RTV.RegistrationType_ID = RT.RegistrationType_ID
	AND			R.GroupLeader_ID = G.GroupLeader_ID
	AND			R.Venue_ID = V.Venue_ID
	AND			R.RegistrationType_ID = RT.RegistrationType_ID
	GROUP BY	G.GroupLeader_ID,
				CC.State_ID,
				C.Conference_ID,
				V.Venue_ID,
				RT.RegistrationType_ID,
				RT.RegistrationCost
) GRV

WHERE		Purchased <> 0
OR			Allocated <> 0 
OR			UnAllocated <> 0
GO
