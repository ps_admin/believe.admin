SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE VIEW [dbo].[vw_Events_GroupRegistrationView]

AS
SELECT		GRV.* 
FROM (
	SELECT		G.GroupLeader_ID,
				CC.State_ID,
				C.Conference_ID,
				V.Venue_ID,
				RT.RegistrationType_ID,
				ISNULL((SELECT SUM(GB.Quantity) FROM Events_GroupBulkRegistration GB WHERE GB.GroupLeader_ID = G.GroupLeader_ID AND GB.Venue_ID = V.Venue_ID AND GB.RegistrationType_ID = RT.RegistrationType_ID),0) as Purchased,
				ISNULL((SELECT COUNT(*) FROM Common_ContactExternal CE, Events_Registration Re WHERE CE.Deleted = 0 AND CE.Contact_ID = Re.Contact_ID AND Re.Venue_ID = V.Venue_ID AND Re.GroupLeader_ID = G.GroupLeader_ID AND Re.RegistrationType_ID = RT.RegistrationType_ID AND Re.Venue_ID = V.Venue_ID),0) AS Allocated,
				ISNULL((SELECT SUM(GB.Quantity) FROM Events_GroupBulkRegistration GB WHERE GB.GroupLeader_ID = G.GroupLeader_ID AND GB.Venue_ID = V.Venue_ID AND GB.RegistrationType_ID = RT.RegistrationType_ID),0) -
				ISNULL((SELECT COUNT(*) FROM Common_ContactExternal CE, Events_Registration Re WHERE CE.Deleted = 0 AND CE.Contact_ID = Re.Contact_ID AND Re.Venue_ID = V.Venue_ID AND Re.GroupLeader_ID = G.GroupLeader_ID AND Re.RegistrationType_ID = RT.RegistrationType_ID AND Re.Venue_ID = V.Venue_ID),0) AS UnAllocated
	FROM		Events_Group G,
				Events_Conference C,
				Events_Venue V,
				Events_RegistrationType RT,
				Events_RegistrationTypeVenue RTV,
				Common_Contact CC
	WHERE		V.Conference_ID = C.Conference_ID
	AND			G.GroupLeader_ID = CC.Contact_ID
	AND			V.Venue_ID = RTV.Venue_ID
	AND			RTV.RegistrationType_ID = RT.RegistrationType_ID
	GROUP BY	G.GroupLeader_ID,
				CC.State_ID,
				C.Conference_ID,
				V.Venue_ID,
				RT.RegistrationType_ID
) GRV

WHERE		Purchased <> 0
OR			Allocated <> 0 
OR			UnAllocated <> 0
GO
