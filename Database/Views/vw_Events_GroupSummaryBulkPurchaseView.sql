SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_Events_GroupSummaryBulkPurchaseView]

AS

SELECT		G.GroupLeader_ID,
			V.Venue_ID,
			dbo.MaximumInt(GBP.AccommodationQuantity, R.AccommodationQuantity) as AccommodationQuantity,
			dbo.MaximumInt(GBP.CateringQuantity, R.CateringQuantity) as CateringQuantity,
			dbo.MaximumInt(GBP.LeadershipBreakfastQuantity, R.LeadershipBreakfastQuantity) as LeadershipBreakfastQuantity,
			C.AccommodationCost * dbo.MaximumInt(GBP.AccommodationQuantity, R.AccommodationQuantity) as AccommodationCost,
			C.CateringCost * dbo.MaximumInt(GBP.CateringQuantity, R.CateringQuantity) as CateringCost,
			C.LeadershipBreakfastCost * dbo.MaximumInt(GBP.LeadershipBreakfastQuantity, R.LeadershipBreakfastQuantity) as LeadershipBreakfastCost
FROM		Events_Group G,
			Events_Conference C,
			Events_Venue V,
			(SELECT G.GroupLeader_ID, V.Venue_ID,
					ISNULL(SUM(AccommodationQuantity),0) as AccommodationQuantity,
					ISNULL(SUM(CateringQuantity),0) as CateringQuantity,
					ISNULL(SUM(LeadershipBreakfastQuantity),0) as LeadershipBreakfastQuantity
			 FROM	(Events_Venue V CROSS JOIN Events_Group G)
							LEFT JOIN Events_GroupBulkPurchase EGBP ON G.GroupLeader_ID = EGBP.GroupLeader_ID
							AND V.Venue_ID = EGBP.Venue_ID 
			 GROUP BY G.GroupLeader_ID, V.Venue_ID, PlanetKidsRegistrationType_ID) GBP,
			 
			(SELECT G.GroupLeader_ID, V.Venue_ID,
					ISNULL(SUM(CONVERT(INT, Accommodation)),0) as AccommodationQuantity,
					ISNULL(SUM(CONVERT(INT, Catering)),0) as CateringQuantity,
					ISNULL(SUM(CONVERT(INT, LeadershipBreakfast)),0) as LeadershipBreakfastQuantity
			 FROM	(Events_Venue V CROSS JOIN Events_Group G)
							LEFT JOIN Events_Registration ER ON G.GroupLeader_ID = ER.GroupLeader_ID
							AND V.Venue_ID = ER.Venue_ID 
			 GROUP BY G.GroupLeader_ID, V.Venue_ID) R
			 
WHERE		V.Conference_ID = C.Conference_ID
AND			V.Venue_ID = R.Venue_ID
AND			V.Venue_ID = GBP.Venue_ID
AND			G.GroupLeader_ID = R.GroupLeader_ID
AND			G.GroupLeader_ID = GBP.GroupLeader_ID
AND			(dbo.MaximumInt(GBP.AccommodationQuantity, R.AccommodationQuantity) > 0
OR			dbo.MaximumInt(GBP.CateringQuantity, R.CateringQuantity) > 0
OR			dbo.MaximumInt(GBP.LeadershipBreakfastQuantity, R.LeadershipBreakfastQuantity) > 0)
GO
