SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_Events_GroupSummaryRegistrationView]

AS

SELECT	G.GroupLeader_ID,
		C.Conference_ID,
		V.Venue_ID,
		R.RegistrationType_ID,
		Cost = R.RegistrationCost * ISNULL(dbo.MaximumInt(
			(SELECT SUM(Quantity) FROM Events_GroupBulkRegistration WHERE RegistrationType_ID = R.RegistrationType_ID AND Venue_ID = V.Venue_ID AND GroupLeader_ID = G.GroupLeader_ID),
			(SELECT COUNT(*) FROM Events_Registration WHERE RegistrationType_ID = R.RegistrationType_ID AND Venue_ID = V.Venue_ID AND GroupLeader_ID = G.GroupLeader_ID)),0),
		TotalRegistrations = ISNULL(dbo.MaximumInt(
			(SELECT SUM(Quantity) FROM Events_GroupBulkRegistration WHERE RegistrationType_ID = R.RegistrationType_ID AND Venue_ID = V.Venue_ID AND GroupLeader_ID = G.GroupLeader_ID),
			(SELECT COUNT(*) FROM Events_Registration WHERE RegistrationType_ID = R.RegistrationType_ID AND Venue_ID = V.Venue_ID AND GroupLeader_ID = G.GroupLeader_ID)),0),
		BulkRegistrations = ISNULL((SELECT SUM(Quantity) FROM Events_GroupBulkRegistration WHERE RegistrationType_ID = R.RegistrationType_ID AND Venue_ID = V.Venue_ID AND GroupLeader_ID = G.GroupLeader_ID),0),
		Allocated = ISNULL((SELECT COUNT(*) FROM Events_Registration WHERE RegistrationType_ID = R.RegistrationType_ID AND Venue_ID = V.Venue_ID AND GroupLeader_ID = G.GroupLeader_ID),0),
		UnAllocated = ISNULL((SELECT SUM(Quantity) FROM Events_GroupBulkRegistration WHERE RegistrationType_ID = R.RegistrationType_ID AND Venue_ID = V.Venue_ID AND GroupLeader_ID = G.GroupLeader_ID) -
			dbo.MinimumInt(
			(SELECT SUM(Quantity) FROM Events_GroupBulkRegistration WHERE RegistrationType_ID = R.RegistrationType_ID AND GroupLeader_ID = G.GroupLeader_ID AND Venue_ID = V.Venue_ID),
			(SELECT COUNT(*) FROM Events_Registration WHERE RegistrationType_ID = R.RegistrationType_ID AND GroupLeader_ID = G.GroupLeader_ID AND Venue_ID = V.Venue_ID)),0)
FROM	Events_Group G,
		Events_Conference C,
		Events_Venue V,
		Events_RegistrationType R,
		Events_RegistrationTypeVenue RV
WHERE	V.Conference_ID = C.Conference_ID
AND		V.Venue_ID = RV.Venue_ID
AND		RV.RegistrationType_ID = R.RegistrationType_ID
GROUP BY G.GroupLeader_ID, C.Conference_ID, V.Venue_ID, R.RegistrationType_ID, R.RegistrationCost
HAVING  0 < dbo.MaximumInt(
		(SELECT SUM(Quantity) FROM Events_GroupBulkRegistration WHERE RegistrationType_ID = R.RegistrationType_ID AND Venue_ID = V.Venue_ID AND GroupLeader_ID = G.GroupLeader_ID),
		(SELECT COUNT(*) FROM Events_Registration WHERE RegistrationType_ID = R.RegistrationType_ID AND Venue_ID = V.Venue_ID AND GroupLeader_ID = G.GroupLeader_ID))
GO
