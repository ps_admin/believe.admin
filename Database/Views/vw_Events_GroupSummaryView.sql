SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_Events_GroupSummaryView]

AS
SELECT		G.GroupLeader_ID,
			C.Conference_ID,
			V.Venue_ID,
			ISNULL((SELECT SUM(Cost) FROM vw_Events_GroupSummaryRegistrationView RV WHERE RV.GroupLeader_ID = G.GroupLeader_ID AND RV.Venue_ID = V.Venue_ID),0) as Registrations,
			Accommodation = ISNULL((SELECT SUM(AccommodationCost) FROM vw_Events_GroupSummaryBulkPurchaseView BPV WHERE BPV.GroupLeader_ID = G.GroupLeader_ID AND BPV.Venue_ID = V.Venue_ID),0),
			Catering = ISNULL((SELECT SUM(CateringCost) FROM vw_Events_GroupSummaryBulkPurchaseView BPV WHERE BPV.GroupLeader_ID = G.GroupLeader_ID AND BPV.Venue_ID = V.Venue_ID),0),
			Other = ISNULL((SELECT SUM(LeadershipBreakfastCost) FROM vw_Events_GroupSummaryBulkPurchaseView BPV WHERE BPV.GroupLeader_ID = G.GroupLeader_ID AND BPV.Venue_ID = V.Venue_ID),0),
			TotalCost = 
				ISNULL((SELECT SUM(Cost) FROM vw_Events_GroupSummaryRegistrationView RV WHERE RV.GroupLeader_ID = G.GroupLeader_ID AND RV.Venue_ID = V.Venue_ID),0) +
				ISNULL((SELECT SUM(AccommodationCost + CateringCost + LeadershipBreakfastCost) FROM vw_Events_GroupSummaryBulkPurchaseView BPV WHERE BPV.GroupLeader_ID = G.GroupLeader_ID AND BPV.Venue_ID = V.Venue_ID),0)
FROM		Events_Group G,
			Events_Conference C,
			Events_Venue V
WHERE		V.Conference_ID = C.Conference_ID
GROUP BY	G.GroupLeader_ID,
			C.Conference_ID,
			V.Venue_ID
HAVING		0 < ISNULL((SELECT SUM(Cost) FROM vw_Events_GroupSummaryRegistrationView RV WHERE RV.GroupLeader_ID = G.GroupLeader_ID AND RV.Venue_ID = V.Venue_ID),0) +
			ISNULL((SELECT SUM(AccommodationCost + CateringCost + LeadershipBreakfastCost) FROM vw_Events_GroupSummaryBulkPurchaseView BPV WHERE BPV.GroupLeader_ID = G.GroupLeader_ID AND BPV.Venue_ID = V.Venue_ID),0)
GO
