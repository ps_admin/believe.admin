SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_Events_RegistrantCostDetailedView] 
AS
SELECT		Co.Conference_ID,
			C.Contact_ID, 
			Re.Registration_ID, 
			Re.GroupLeader_ID, 
			Re.Venue_ID,
			CE.Deleted,
			SUM(ROUND(RT.RegistrationCost * ((100 - Re.RegistrationDiscount) / 100), 2)) AS RegistrationCost, 
			SUM(Co.CateringCost * CASE WHEN Re.Catering=1 THEN 1 ELSE 0 END) AS CateringCost,
			SUM(Co.AccommodationCost * CASE WHEN Re.Accommodation=1 THEN 1 ELSE 0 END) AS AccommodationCost,
			ISNULL((SELECT SUM(ROUND(PP.Price * ((100 - PO.Discount)/100),2)) FROM Events_ProductPrice PP, Events_PreOrder PO WHERE PP.Product_ID = PO.Product_ID AND PP.Country_ID = Co.Country_ID AND PO.Registration_ID = Re.Registration_ID),0) AS PreOrderCost,
			SUM(Co.LeadershipBreakfastCost * CASE WHEN Re.LeadershipBreakfast=1 THEN 1 ELSE 0 END) AS OtherCost
FROM		Common_Contact C,
			Common_ContactExternal CE,
			Events_Registration Re,
			Events_Venue V,
			Events_Conference Co,
			Events_RegistrationType RT
WHERE		C.Contact_ID = CE.Contact_ID
AND			CE.Contact_ID = Re.Contact_ID
AND			Re.Venue_ID = V.Venue_ID
AND			V.Conference_ID = Co.Conference_ID 
AND			Re.RegistrationType_ID = RT.RegistrationType_ID
AND			Co.Conference_ID = RT.Conference_ID
AND			CE.Deleted = 0
GROUP BY	Co.Conference_ID,
			Co.Country_ID,
			C.Contact_ID, 
			Re.Registration_ID, 
			Re.GroupLeader_ID,
			Re.Venue_ID,
			CE.Deleted
GO
