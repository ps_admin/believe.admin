SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_Events_RegistrantCostView]

AS
SELECT 	Conference_ID,
		Contact_ID,
		Registration_ID,
		GroupLeader_ID,
		Venue_ID,
		Deleted,
		TotalCost = (RegistrationCost + CateringCost + AccommodationCost + PreOrderCost + OtherCost),
		RegistrationCost,
		CateringCost,
		AccommodationCost,
		PreOrderCost,
		OtherCost
FROM	vw_Events_RegistrantCostDetailedView
GO
