SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_Events_RegistrationOptionView]
AS
SELECT	Registration_ID,
		RON.RegistrationOptionName_ID,
		RON.[Name],
		CASE	WHEN FieldType='TXT' THEN RO.RegistrationOptionText
				WHEN FieldType='BIT' THEN CONVERT(VARCHAR, CONVERT(BIT, RO.RegistrationOptionText))
				WHEN FieldType='DDL' THEN CONVERT(VARCHAR,(SELECT [Value] FROM Events_RegistrationOptionValue WHERE RegistrationOptionValue_ID = RO.RegistrationOptionValue_ID))
				ELSE '' END as [Value],
		FieldType
FROM	Events_RegistrationOption RO,
		Events_RegistrationOptionName RON
WHERE	RO.RegistrationOptionName_ID = RON.RegistrationOptionName_ID
GO
