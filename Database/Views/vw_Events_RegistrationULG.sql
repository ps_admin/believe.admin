SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_Events_RegistrationULG]

AS

SELECT		ULG_ID, Name, RANK() OVER (ORDER BY Name) + 2 as SortOrder
FROM		Church_ULG
WHERE		Inactive=0

UNION ALL

SELECT		-2, 'N/A - I''m not in an Urban Life Group', 1

UNION ALL

SELECT		-1, 'Other Group (Not Listed)', 2
GO
