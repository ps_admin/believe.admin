SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_Events_TotalRegistrants]

AS
SELECT	Venue_ID,
		SUM(Registrants) as Registrants
FROM (
	SELECT		Venue_ID, 
				COUNT(*) as Registrants
	FROM		Events_Registration R,
				Events_RegistrationType RT
	WHERE		R.RegistrationType_ID = RT.RegistrationType_ID
	AND			R.GroupLeader_ID IS NULL
	GROUP BY	Venue_ID

	UNION ALL
	
	SELECT		Venue_ID, 
				SUM(TotalRegistrations) as Qty 
	FROM		vw_Events_GroupSummaryRegistrationView V, 
				Events_RegistrationType RT
	WHERE		V.RegistrationType_ID = RT.RegistrationType_ID  
	GROUP BY	Venue_ID

) R
GROUP BY	Venue_ID
GO
