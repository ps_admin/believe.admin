SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_Revolution_ContentCategory]

AS

SELECT	C.Content_ID,
		(SELECT TOP 1 CSC.ContentCategory_ID 
			FROM	Revolution_ContentContentCategory CCC,
					Revolution_ContentSubCategory CSC
			WHERE	CCC.ContentSubCategory_ID = CSC.ContentSubCategory_ID
			AND		CCC.Content_ID = C.Content_ID
			ORDER BY CCC.SortOrder) as ContentCategory_ID
FROM	Revolution_Content C
GO
