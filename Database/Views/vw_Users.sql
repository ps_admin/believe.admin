SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_Users]

AS
SELECT	C.Contact_ID, 
		C.FirstName, 
		C.LastName, 
		[Name] = FirstName + ' ' + LastName,
		C.Email,
		C.State_ID,
		C.Country_ID,
		C.Password,
		UD.LastPasswordChangeDate,
		UD.InvalidPasswordAttempts,
		UD.PasswordResetRequired,
		UD.Inactive,
		(SELECT Campus_ID FROM Common_ContactInternal WHERE Contact_ID = C.Contact_ID) as Campus_ID
FROM	Common_Contact C,
		Common_UserDetail UD
WHERE	C.Contact_ID = UD.Contact_ID
GO
