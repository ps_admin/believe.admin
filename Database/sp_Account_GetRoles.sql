CREATE PROCEDURE [dbo].sp_Account_GetRoles
(
	@ContactID INT
)
AS
BEGIN
	SET NOCOUNT ON

	SELECT			
		CD.ContactDatabaseRole_ID AS ContactRoleID,
		CD.DatabaseRole_ID AS RoleID,
		DR.Module AS RoleModule,
		DR.Name AS RoleName,
		CD.DateAdded
	FROM 
		Common_ContactDatabaseRole CD WITH(NOLOCK) 
	INNER JOIN 
		Common_DatabaseRole DR WITH(NOLOCK) ON DR.DatabaseRole_ID = CD.DatabaseRole_ID
	WHERE 
		CD.Contact_ID = @ContactID

END

