ALTER PROCEDURE [dbo].[sp_Account_Login]
(
	@Email NVARCHAR(100),
	@Password NVARCHAR(100)
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @ContactID INT
		
	SELECT 
		@ContactID = C.Contact_ID
	FROM 
		Common_Contact C 
	WHERE 
		Email <> '' AND Email = @Email
		AND [Password] <> '' AND [Password] = @Password

	SELECT @ContactID AS ContactID

	EXEC usp_UrbanLife_GetByContactId @ContactID
END

-- sp_Account_Login 'hery@live.com', 'h'

