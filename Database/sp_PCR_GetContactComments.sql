ALTER PROCEDURE sp_PCR_GetContactComments
(
	@ContactID INT,
	@CommentTypeID INT = NULL
)
AS 
BEGIN

	SET NOCOUNT ON

	SELECT 
		TOP 20
		C2.FirstName + ' ' + C2.LastName AS ContactName,
		CC.Comment_ID AS ID,	
		CC.CommentDate AS CommentDate,
		CC.Comment AS Comment,
		CS.Name AS CommentSource,		
		CommentType_ID AS CommentType,
		C.FirstName AS CommenterFirstName,
		C.LastName AS CommenterLastName
	FROM [Church_ContactComment] CC WITH(NOLOCK)
	INNER JOIN 
		Church_CommentSource CS WITH(NOLOCK) ON CS.CommentSource_ID = CC.CommentSource_ID
	INNER JOIN
		Common_Contact C WITH(NOLOCK) ON C.Contact_ID = CC.CommentBy_ID	
	INNER JOIN
		Common_Contact C2 WITH(NOLOCK) ON C2.Contact_ID = CC.Contact_ID		
	WHERE 
		CC.Contact_ID = @ContactID
		AND (@CommentTypeID IS NULL OR CommentType_ID = @CommentTypeID)
	ORDER BY 
		CC.CommentDate DESC
		
END		

-- sp_PCR_GetContactComments 117307