ALTER PROCEDURE sp_PCR_GetContactDetails		
	@ContactID INT
AS
BEGIN		
	
	-- Get the latest active PCR
	DECLARE @PCRDate_ID INT
	
	SELECT 
		@PCRDate_ID = PCRDate_ID
	FROM 
		Church_PCRDate 	WITH(NOLOCK)	
	WHERE 
		DATEDIFF(d, [Date], GETDATE()) <= 2 
		AND DATEDIFF(d, [Date], GETDATE()) > -5	

	PRINT 'Getting Contact Details for PCRDate_ID AS ' + CAST(@PCRDate_ID AS NVARCHAR(20))

	-- Get Summary of the contact in past 31 days
	SELECT	
		C.Contact_ID,
		SUM(CONVERT(INT,SundayAttendance)) as MonthSundayTotal,
		SUM(CONVERT(INT,ULGAttendance)) as MonthULGTotal,
		SUM(CONVERT(INT,BoomAttendance)) as MonthBoomTotal,
		SUM(Call) as MonthCallTotal,
		SUM(Visit) as MonthVisitTotal		
	INTO 
		#Summary
	FROM	
		Church_PCRContact C WITH(NOLOCK)	
	INNER JOIN
		Church_PCR PCR WITH(NOLOCK) ON PCR.PCR_ID = C.PCR_ID 
		AND PCR.PCRDate_ID 
			IN (
				SELECT	
					PCRDate_ID
				FROM 
					Church_PCRDate
				WHERE 
					DATEDIFF(d, [Date], GETDATE()) <= 31
					AND DATEDIFF(d, [Date], GETDATE()) > -7	
				)
	WHERE
		C.Contact_ID = @ContactID
	GROUP BY 
		C.Contact_ID	

	-- Get the latest comment date of the contact
	DECLARE @LastCommentMade DATETIME
	SELECT TOP 1
		@LastCommentMade = CommentDate
	FROM
		[Church_ContactComment] WITH(NOLOCK)
	WHERE 
		Contact_ID = @ContactID
	ORDER BY
		CommentDate DESC

	-- Now, returns all details
	SELECT	
			CC.Contact_ID AS ID,
			C.PCRContact_ID AS PCRContactID,
			CC.FirstName as FirstName,
			CC.LastName as LastName,
			CC.DateOfBirth AS DOB,
			CC.Gender AS Gender,
			Email,
			Mobile,
			Address1,
			Address2, 
			Suburb,
			Postcode,							
			CS.Name AS Status,
			CC.CreationDate AS DateRegistered,
			@LastCommentMade AS DateLastComment,
			ISNULL(S.MonthSundayTotal,0) as MonthSundayTotal,
			ISNULL(S.MonthULGTotal,0) as MonthULGTotal,
			ISNULL(S.MonthBoomTotal,0) as MonthBoomTotal,
			ISNULL(S.MonthCallTotal,0) as MonthCallTotal,
			ISNULL(S.MonthVisitTotal,0) as MonthVisitTotal,			
			CC.[GUID] as ContactGUID
	FROM 
		Church_PCR PCR WITH(NOLOCK)
	INNER JOIN 
		Church_PCRContact C  WITH(NOLOCK) ON C.PCR_ID = PCR.PCR_ID
	INNER JOIN 
		Common_Contact CC WITH(NOLOCK) ON CC.Contact_ID = C.Contact_ID
	INNER JOIN 
		Common_ContactInternal CI WITH(NOLOCK) ON CI.Contact_ID = CC.Contact_ID 			
	INNER JOIN 
		Church_ChurchStatus CS WITH(NOLOCK) ON CS.ChurchStatus_ID = CI.ChurchStatus_ID	
	INNER JOIN 
		#Summary S ON C.Contact_ID = S.Contact_ID
	WHERE 
		PCR.PCRDate_ID = @PCRDate_ID
	ORDER BY 
		CC.FirstName, CC.LastName

	DROP TABLE #Summary	

END

-- exec sp_PCR_GetContactDetails @ContactID = 122352