ALTER PROCEDURE [dbo].[sp_PCR_GetReport]
	@ULG_ID INT,
	@PCRDate_ID INT = NULL
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @PCR_ID INT
	EXEC sp_PCR_PrepareWeeklyReport @ULG_ID = @ULG_ID, @PCRDate_ID = @PCRDate_ID, @PCR_ID= @PCR_ID OUTPUT


	-- Get information if latest PCR is the latest one	
	DECLARE @LatestReport BIT
	SET @LatestReport = 0
		
	DECLARE @PCR_DATE DATETIME
	SELECT 
		@PCR_DATE = [Date] 
	FROM 
		Church_PCRDate 
	WHERE 
		PCRDate_ID = @PCRDate_ID OR (@PCRDate_ID IS NULL AND 
		(
			[Date] - 6 < GETDATE() AND 
			[Date] + 1 >= GETDATE()
		))	

	IF (@PCR_DATE - 6 < GETDATE() AND @PCR_DATE + 1 >= GETDATE())
		SET @LatestReport = 1
	


	-- Get total member and attendance
	DECLARE 
		@TotalMember INT,
		@TotalSundayAttendance INT,
		@TotalULGAttendance INT

	SELECT
		@TotalSundayAttendance = COUNT(SundayAttendance) 
	FROM 
		Church_PCRContact WITH(NOLOCK)
	WHERE 
		PCR_ID = @PCR_ID AND
		SundayAttendance = 1
		
	SELECT
		@TotalULGAttendance = COUNT(ULGAttendance) 
	FROM 
		Church_PCRContact WITH(NOLOCK)
	WHERE 
		PCR_ID = @PCR_ID AND
		ULGAttendance = 1	
	
	SELECT
		@TotalMember = COUNT(*) 
	FROM 
		Church_PCRContact WITH(NOLOCK)
	WHERE 
		PCR_ID = @PCR_ID 

		
	SELECT 
		P.PCR_ID,
		D.PCRDate_ID,
		@TotalMember AS TotalMember,
		@TotalULGAttendance AS TotalULGAttendance,
		@TotalSundayAttendance AS TotalSundayAttendance,
		ULGChildren AS TotalChildren,
		ULGVisitors AS TotalVisitor,
		ULGOffering AS TotalOffering,
		GeneralComments AS Comments,
		ReportCompleted AS Completed,
		D.Date AS [Date],
		D.ULG AS UrbanLifeWeek,
		@LatestReport AS LatestReport
	FROM 
		Church_PCR P WITH(NOLOCK)
	INNER JOIN
		Church_PCRDate D WITH(NOLOCK) ON P.PCRDate_ID = D.PCRDate_ID
	WHERE 
		PCR_ID = @PCR_ID
		AND ULG_ID = @ULG_ID	
END

-- sp_PCR_GetReport 43, 239