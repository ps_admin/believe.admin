CREATE PROCEDURE dbo.sp_PCR_GetReportsNonSubmitted
	@UrbanLifeID	INT
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @CurrentPCRDate_ID INT
	
	-- Get current PCR ID as baseline
	SELECT 
		@CurrentPCRDate_ID = PCRDate_ID
	FROM 
		Church_PCRDate 	WITH(NOLOCK)	
	WHERE 
		DATEDIFF(d, [Date], GETDATE()) <= 0 
		AND DATEDIFF(d, [Date], GETDATE()) > -7	
				
	-- Get all previous report with current PCR Week as baseline								
	SELECT 
		TOP 10
		P.PCR_ID,
		D.Date AS PCRDate
	FROM 
		Church_PCR P WITH(NOLOCK)
	INNER JOIN Church_PCRDate D ON D.PCRDate_ID = P.PCRDate_ID 
		AND D.Deleted = 0
	WHERE 
		ReportCompleted = 0 
		AND ULG_ID = @UrbanLifeID 
		AND P.PCRDate_ID < @CurrentPCRDate_ID
	ORDER BY 
		P.PCRDate_ID DESC

END

-- sp_PCR_GetReportsNonSubmitted 43