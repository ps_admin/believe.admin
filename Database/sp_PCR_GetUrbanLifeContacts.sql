SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[sp_PCR_GetUrbanLifeContacts]	
	@ULG_ID INT,
	@PCR_ID	INT = NULL
AS	
BEGIN
	
	-- If PCRID is not given, assume current PCR week is requested
	IF @PCR_ID IS NULL
	BEGIN

		PRINT 'PCRID IS NOT PROVIDED, ASSUMING Current PCR Week is requested!'
	
		DECLARE @PCRDate_ID INT
		
		SELECT 
			@PCRDate_ID = PCRDate_ID
		FROM 
			Church_PCRDate 	WITH(NOLOCK)	
		WHERE 
			DATEDIFF(d, [Date], GETDATE()) <= 0 
			AND DATEDIFF(d, [Date], GETDATE()) > -7	
			AND Deleted = 0


		-- Now, try to get PCR ID given PCRDate and UrbanLife
		SELECT TOP 1 @PCR_ID = PCR_ID 
		FROM Church_PCR WITH(NOLOCK)
		WHERE PCRDate_ID = @PCRDate_ID AND ULG_ID = @ULG_ID

		-- If can't find it, then it is new week, setup new PCR Contact
		IF (@PCR_ID IS NULL) 
		BEGIN
			/**/
			PRINT 'About to insert new entry to table Church_PCR'

			INSERT	
				Church_PCR 
				(PCRDate_ID, ULG_ID, ULGChildren, ULGVisitors, ULGOffering, ReportCompleted)
			VALUES
				(@PCRDate_ID, @ULG_ID, 0, 0, 0, 0)
				
			
			/**/	
			PRINT 'Get the newly created PCR_ID'

			SELECT TOP 1 @PCR_ID = PCR_ID 
			FROM Church_PCR WITH(NOLOCK)
			WHERE PCRDate_ID = @PCRDate_ID AND ULG_ID = @ULG_ID													
		END
	END
	

	/**/
	PRINT 'Import contact data into Church_PCRContact table for those not listed yet'
	
	INSERT	
		Church_PCRContact 
		(PCR_ID, Contact_ID, SundayAttendance, ULGAttendance, BoomAttendance, Call, Visit)
	SELECT DISTINCT 
		P.PCR_ID, Contact_ID, 0, 0, 0, 0, 0
	FROM	
		Church_PCR P WITH(NOLOCK)
	INNER JOIN			
		Church_ULGContact U WITH(NOLOCK) ON U.ULG_ID = P.ULG_ID
			AND	U.ULG_ID = @ULG_ID
			AND	U.Inactive = 0
	WHERE P.PCR_ID = @PCR_ID
		AND NOT EXISTS (
			SELECT PC.Contact_ID
			FROM Church_PCRContact PC WITH(NOLOCK)
			WHERE PC.PCR_ID = P.PCR_ID
				AND	PC.Contact_ID = U.Contact_ID
			)	

	PRINT 'Successfully importing ' + CAST(@@ROWCOUNT AS NVARCHAR(3))

	
	/**/
	PRINT 'RETRIEVE Contact List with PCR_ID AS ' + CAST(@PCR_ID AS NVARCHAR(20))	
	SELECT	
		DISTINCT
		CC.Contact_ID AS ID,
		C.PCRContact_ID AS PCRContactID,
		U.ULG_ID AS UrbanLifeID,
		FirstName,
		LastName,
		Email,
		Mobile,
		Address1,
		Address2, 
		Suburb,
		Postcode,
		CS.Name AS Status, 
		CC.CreationDate AS DateRegistered,
		C.ULGAttendance AS AttendUrbanLife,
		C.SundayAttendance AS AttendSundayService,
		C.BoomAttendance AS AttendBoom
	INTO
		#PCRContacts
	FROM	
		Common_Contact CC WITH(NOLOCK)
	INNER JOIN
		Church_PCRContact C WITH(NOLOCK) ON C.Contact_ID = CC.Contact_ID
	INNER JOIN
		Church_PCR PCR WITH(NOLOCK) ON PCR.PCR_ID = C.PCR_ID 
			AND PCR.PCR_ID = @PCR_ID
	INNER JOIN 
		Common_ContactInternal CI WITH(NOLOCK) ON CI.Contact_ID = CC.Contact_ID 
			AND	CI.ChurchStatus_ID NOT IN (6, 7)		
	INNER JOIN 
		Church_ChurchStatus CS WITH(NOLOCK) ON CS.ChurchStatus_ID = CI.ChurchStatus_ID		
	INNER JOIN			
		Church_ULGContact U WITH(NOLOCK) ON U.Contact_ID = C.Contact_ID
			AND	U.ULG_ID = @ULG_ID
			AND	U.Inactive = 0		
	ORDER BY
		FirstName, LastName
		
	PRINT 'COMPILE latest comment date for all PCR Contacts'		
	SELECT 
		ID,
		MAX(CommentDate) AS LastCommentMade
	INTO
		#PCRContactsLastCommentDate
	FROM
		#PCRContacts
	LEFT OUTER JOIN
		[Church_ContactComment] WITH(NOLOCK) ON Contact_ID = ID
	GROUP BY
		ID
	
	PRINT 'DUMP output'	
	SELECT 
		C.*,
		CDate.LastCommentMade
	FROM
		#PCRContacts C
	INNER JOIN
		#PCRContactsLastCommentDate CDate WITH(NOLOCK) ON CDate.ID = C.ID
	
	DROP TABLE #PCRContacts
	DROP TABLE #PCRContactsLastCommentDate
END

-- sp_PCR_GetUrbanLifeContacts @ULG_ID = 43, @PCR_ID=12094

--select * from Church_PCR where ULG_ID = 43 and PCRDate_ID = 230
--select * from Church_PCRDate where PCRDate_ID = 238
-- select * from Church_PCRContact where PCRContact_ID=775165
-- SELECT * FROM Church_PCR where PCR_ID=24717