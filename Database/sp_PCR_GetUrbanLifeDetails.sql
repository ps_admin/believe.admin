ALTER PROCEDURE sp_PCR_GetUrbanLifeDetails
	@ULG_ID INT,
	@PCRDate_ID INT = NULL
AS	
BEGIN
	
	DECLARE @PCR_ID INT
	EXEC sp_PCR_PrepareWeeklyReport @ULG_ID = @ULG_ID, @PCRDate_ID = @PCRDate_ID, @PCR_ID= @PCR_ID OUTPUT
		
	-- Now output the results	
	SELECT	
		DISTINCT
		PCRDate.Date AS PCRDate,
		PCRDate.ULG AS UrbanLifeWeek,
		PCR.PCR_ID AS PCR_ID,
		PCR.ULGOffering AS TotalOffering,
		PCR.ULGVisitors AS TotalVisitor,
		PCR.ULGChildren AS TotalChildren,
		PCR.GeneralComments AS Comments
	FROM	
		Church_ULG ULG WITH(NOLOCK)
	INNER JOIN
		Church_PCR PCR WITH(NOLOCK) ON PCR.ULG_ID = ULG.ULG_ID 
			AND PCR.PCRDate_ID = @PCRDate_ID
	INNER JOIN
		Church_PCRDate PCRDate WITH(NOLOCK) ON PCR.PCRDate_ID = PCRDate.PCRDate_ID
	INNER JOIN
		Church_ULGDateDay WITH(NOLOCK) ON Church_ULGDateDay.ULGDateDay_ID = ULG.ULGDateDay_ID
	INNER JOIN
		Church_ULGDateTime WITH(NOLOCK) ON Church_ULGDateTime.ULGDateTime_ID = ULG.ULGDateTime_ID
	INNER JOIN
		Church_Ministry WITH(NOLOCK) ON Church_Ministry.Ministry_ID= ULG.Ministry_ID		
	INNER JOIN
		Church_Campus WITH(NOLOCK) ON Church_Campus.Campus_ID= ULG.Campus_ID		
	LEFT JOIN -- PlanetUni doesn't have region
		Church_Region WITH(NOLOCK) ON Church_Region.Region_ID= ULG.Region_ID		
	WHERE
		ULG.ULG_ID = @ULG_ID			
END

-- sp_PCR_GetUrbanLifeDetails @ULG_ID = 43
