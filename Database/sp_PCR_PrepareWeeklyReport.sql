-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	Prepare PCR and Import Contact List
-- =============================================
ALTER PROCEDURE [dbo].[sp_PCR_PrepareWeeklyReport]
	@ULG_ID INT,
	@PCRDate_ID INT = NULL,
	@PCR_ID INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	-- If PCRDate is not given, assume current PCR week is requested	
	IF @PCRDate_ID IS NULL
	BEGIN
		PRINT 'PCRDate_ID IS NOT PROVIDED, ASSUMING Current PCR Week is requested!'
		
		SELECT 
			@PCRDate_ID = PCRDate_ID
		FROM 
			Church_PCRDate 	WITH(NOLOCK)	
		WHERE 
			DATEDIFF(d, [Date], GETDATE()) <= 2 
			AND DATEDIFF(d, [Date], GETDATE()) > -5	


		IF @PCRDate_ID IS NULL
		BEGIN 
			PRINT 'Cant find PCRDate_ID in the database for this week. Please update Church_PCRDate and try again'
			RETURN
		END
		
		PRINT 'Returned PCRDate_ID: ' + CAST(@PCRDate_ID AS NVARCHAR(20))	
	END	

	PRINT 'Now, look up Church PCR WITH Given PCRDate_ID and UrbanLife ID'
	SELECT 
		PCR_ID
	INTO
		#PCRID
	FROM 
		Church_PCR WITH(NOLOCK)
	WHERE 
		PCRDate_ID = @PCRDate_ID 
		AND ULG_ID = @ULG_ID
	
	IF (@@ROWCOUNT = 1)
	BEGIN
		SELECT 
			@PCR_ID = PCR_ID
		FROM 
			#PCRID
		
		DROP TABLE #PCRID

		PRINT 'Returned PCR_ID: ' + CAST(@PCR_ID AS NVARCHAR(20))
	END
	ELSE
	BEGIN
		PRINT 'PCR has not yet been setup, create one and re-map member into PCR'
		
		INSERT	
			Church_PCR 
			(PCRDate_ID, ULG_ID, ULGChildren, ULGVisitors, ULGOffering, ReportCompleted)
		VALUES
			(@PCRDate_ID, @ULG_ID, 0, 0, 0, 0)

		SET @PCR_ID = CAST(@@IDENTITY AS INT)
			
		PRINT 'New PCR_ID ' + CAST(@PCR_ID AS NVARCHAR(20)) + 'has been added into Church_PCR'				
	END
	

	PRINT 'Importing PCRContact to the PCR (Only once not in the list)'
	INSERT	
		Church_PCRContact 
		(PCR_ID, Contact_ID, SundayAttendance, ULGAttendance, BoomAttendance, Call, Visit)
	SELECT DISTINCT 
		PCR_ID, Contact_ID, 0, 0, 0, 0, 0
	FROM	
		Church_PCR P WITH(NOLOCK)
	INNER JOIN 			
		Church_PCRDate PD WITH(NOLOCK) ON PD.PCRDate_ID = P.PCRDate_ID
			AND PD.PCRDate_ID = @PCRDate_ID
			AND PD.Deleted = 0
	INNER JOIN			
		Church_ULGContact U WITH(NOLOCK) ON U.ULG_ID = P.ULG_ID
			AND	U.ULG_ID = @ULG_ID
			AND	U.Inactive = 0
	WHERE NOT EXISTS (
		SELECT PC.Contact_ID
		FROM Church_PCRContact PC WITH(NOLOCK)
		WHERE PC.PCR_ID = P.PCR_ID
			AND	PC.Contact_ID = U.Contact_ID
		)			
		
	
	PRINT 'OUTPUT PARAM @PCR_ID IS ' + CAST(@PCR_ID AS NVARCHAR(10))
END
