CREATE PROCEDURE dbo.sp_PCR_SubmitReport
(
	@PCR_ID				INT,
	@TotalOffering		MONEY = 0,
	@TotalVisitor		INT = 0,
	@TotalChildren		INT = 0,
	@Comments			NVARCHAR(MAX) = '',
	@MarkAsCompleted	BIT = 1
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @CompletedDate DATETIME
	IF @MarkAsCompleted = 1 
		SET @CompletedDate = GETDATE()
	ELSE
		SET @CompletedDate = NULL

	UPDATE 
		Church_PCR
	SET 
		ULGChildren = @TotalChildren,
		ULGVisitors = @TotalVisitor,
		ULGOffering = @TotalOffering,
		GeneralComments = @Comments,
		ReportCompleted = @MarkAsCompleted,
		ReportCompletedDate = @CompletedDate
	WHERE 
		PCR_ID = @PCR_ID
END

-- sp_PCR_SubmitReport 24717, 100, 1, 2, 'test'