ALTER PROCEDURE dbo.sp_PCR_UpdateAttendance 
	@PCRContact_ID	INT,
	@Event			NVARCHAR(50),
	@Attend			BIT
AS
BEGIN
	SET NOCOUNT ON;

	PRINT @Event
	IF @Event = 'UrbanLife'
		UPDATE Church_PCRContact SET ULGAttendance = @Attend WHERE PCRContact_ID = @PCRContact_ID
		
	IF @Event = 'SundayService'
		UPDATE Church_PCRContact SET SundayAttendance = @Attend WHERE PCRContact_ID = @PCRContact_ID

	IF @Event = 'Boom'
		UPDATE Church_PCRContact SET BoomAttendance = @Attend WHERE PCRContact_ID = @PCRContact_ID
END
GO


-- SELECT * FROM Church_PCRContact WHERE PCRContact_ID = 775096
-- dbo.sp_PCR_UpdateAttendance 775092, 'UrbanLife', 1