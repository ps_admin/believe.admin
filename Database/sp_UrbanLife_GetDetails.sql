CREATE PROCEDURE sp_UrbanLife_GetDetails
	@ULG_ID INT
AS	
BEGIN

	SET NOCOUNT ON;
	
	SELECT	
		DISTINCT
		ULG.ULG_ID AS ID,
		ULG.Code,
		ULG.Name,
		ULG.Address1,
		ULG.Address2, 
		ULG.Suburb,
		ULG.Postcode,
		Church_ULGDateDay.Name AS MeetingDay,
		Church_ULGDateTime.Name AS MeetingTime,
		Church_Region.Name AS Region,
		Church_Ministry.Name AS Ministry,
		Church_Campus.Name AS Campus
	FROM	
		Church_ULG ULG WITH(NOLOCK)
	INNER JOIN
		Church_ULGDateDay WITH(NOLOCK) ON Church_ULGDateDay.ULGDateDay_ID = ULG.ULGDateDay_ID
	INNER JOIN
		Church_ULGDateTime WITH(NOLOCK) ON Church_ULGDateTime.ULGDateTime_ID = ULG.ULGDateTime_ID
	INNER JOIN
		Church_Ministry WITH(NOLOCK) ON Church_Ministry.Ministry_ID= ULG.Ministry_ID
	INNER JOIN
		Church_Campus WITH(NOLOCK) ON Church_Campus.Campus_ID= ULG.Campus_ID
	LEFT JOIN -- PlanetUni doesn't have region
		Church_Region WITH(NOLOCK) ON Church_Region.Region_ID= ULG.Region_ID
	WHERE
		ULG.ULG_ID = @ULG_ID 
		AND ULG.Inactive = 0
END

-- sp_UrbanLife_GetDetails @ULG_ID = 43
