﻿IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'usp_Account_Login')
DROP PROCEDURE [dbo].[usp_Account_Login]
GO

CREATE PROCEDURE [dbo].[usp_Account_Login]
(
	@Email NVARCHAR(100),
	@Password NVARCHAR(100)
)
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT 
		cc.Contact_ID AS ContactId,
		FirstName,
		LastName,
		ISNULL(crp.Region_ID, 0) AS PastorOfRegionID
	FROM 
		Common_Contact cc
	LEFT OUTER JOIN 
		Church_RegionalPastor crp ON crp.Contact_ID = cc.Contact_ID
	WHERE 
		Email <> '' 
		AND Email = @Email
		AND [Password] <> '' 
		AND [Password] = @Password
END

-- [usp_Account_Login] 'hery@live.com', 'h'
-- [usp_Account_Login] 'mikew@planetshakers.com', 's'