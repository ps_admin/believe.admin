﻿IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'usp_UrbanLife_GetByContactId')
DROP PROCEDURE [dbo].[usp_UrbanLife_GetByContactId]
GO

CREATE PROCEDURE [dbo].[usp_UrbanLife_GetByContactId]
(
	@ContactId	INT
)
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT 		
		ULG.ULG_ID AS ID,
		ULG.Code,
		ULG.Name,
		ULG.Address1,
		ULG.Address2, 
		ULG.Suburb,
		ULG.Postcode,
		Church_ULGDateDay.Name AS MeetingDay,
		Church_ULGDateTime.Name AS MeetingTime,
		Church_Region.Name AS Region,
		Church_Ministry.Name AS Ministry,
		Church_Campus.Name AS Campus

	FROM 
		Church_ULGContact ULC 
	INNER JOIN 
		Church_ULG ULG ON ULC.ULG_ID = ULG.ULG_ID
		AND ULG.Inactive = 0 -- Make sure UrbanLife still active
	INNER JOIN
		Church_ULGDateDay ON Church_ULGDateDay.ULGDateDay_ID = ULG.ULGDateDay_ID
	INNER JOIN
		Church_ULGDateTime ON Church_ULGDateTime.ULGDateTime_ID = ULG.ULGDateTime_ID
	INNER JOIN
		Church_Ministry ON Church_Ministry.Ministry_ID= ULG.Ministry_ID		
	INNER JOIN
		Church_Campus ON Church_Campus.Campus_ID= ULG.Campus_ID		
	LEFT JOIN 
		-- PlanetUni doesn't have region
		Church_Region ON Church_Region.Region_ID= ULG.Region_ID	
	WHERE 
		ULC.Contact_ID = @ContactId 
		AND ULC.InActive = 0 -- Make sure Contact still partiticpating in that UL
END

-- [usp_UrbanLife_GetByContactId] 90852

	