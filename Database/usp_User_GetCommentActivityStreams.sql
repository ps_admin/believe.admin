﻿IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'usp_User_GetCommentActivityStreams')
DROP PROCEDURE [dbo].[usp_User_GetCommentActivityStreams]
GO

CREATE PROCEDURE [dbo].[usp_User_GetCommentActivityStreams]
(
	@ContactId INT	
)
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT TOP 10 
		CC.Comment_ID, C.FirstName, C.LastName, CC.CommentDate, CC.Comment, CC.CommentType_ID AS CommentType, CC.Actioned
	FROM 
		Church_ContactComment CC
	INNER JOIN
		Common_Contact C ON C.Contact_ID = CC.Contact_ID
	WHERE 
		CommentBy_ID = @ContactId 
	ORDER BY 
		CommentDate DESC
END

-- [usp_User_GetCommentActivityStreams] 104392