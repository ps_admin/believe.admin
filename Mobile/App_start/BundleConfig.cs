﻿using System.Web;
using System.Web.Optimization;

namespace Planetshakers.Events.Mvc
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.IgnoreList.Clear();
  //          bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
    //                    "~/Scripts/jquery-{version}.js"));

        //    bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
          //              "~/Scripts/jquery-ui-{version}.js"));

      //      bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
        //                "~/Scripts/jquery.unobtrusive*",
          //              "~/Scripts/jquery.validate*"));

//            bundles.Add(new ScriptBundle("~/bundles/foundation").Include(
  //                      "~/Content/themes/foundation/js/foundation.min.js",                        
    //                    "~/Content/themes/foundation/js/foundation.forms.js",
      //                  "~/Content/themes/datetimepicker/js/jquery.datetimepicker.js"));

            bundles.Add(new ScriptBundle("~/bundles/planetshakers/events").Include(
                        "~/Content/events/js/events.register.js"));

  //          bundles.Add(new ScriptBundle("~/bundles/planetshakers/accounts").Include(
    //                    "~/Content/themes/planetshakers/js/account.form.js"));

      //      bundles.Add(new ScriptBundle("~/bundles/footable").Include(
        //                "~/Content/themes/footable/js/footable.js"));
            
          //  bundles.Add(new ScriptBundle("~/bundles/development").Include(
            //            "~/Scripts/holder.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
   //         bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
    //                    "~/Content/themes/foundation/js/vendor/modernizr.js"));

      //      bundles.Add(new StyleBundle("~/bundles/Content/css").Include("~/Content/site.css"));
      //
        //    bundles.Add(new StyleBundle("~/bundles/Content/themes/base/css").Include(
          //              "~/Content/themes/base/jquery.ui.core.css",
            //            "~/Content/themes/base/jquery.ui.resizable.css",
  //                      "~/Content/themes/base/jquery.ui.selectable.css",
    //                    "~/Content/themes/base/jquery.ui.accordion.css",
      //                  "~/Content/themes/base/jquery.ui.autocomplete.css",
        //                "~/Content/themes/base/jquery.ui.button.css",
          //              "~/Content/themes/base/jquery.ui.dialog.css",
            //            "~/Content/themes/base/jquery.ui.slider.css",
              //          "~/Content/themes/base/jquery.ui.tabs.css",
                //        "~/Content/themes/base/jquery.ui.datepicker.css",
                  //      "~/Content/themes/base/jquery.ui.progressbar.css",
                    //    "~/Content/themes/base/jquery.ui.theme.css"));

        //    bundles.Add(new StyleBundle("~/bundles/Content/themes/foundation/css").Include(
          //              "~/Content/themes/foundation/css/foundation.min.css",
            //            "~/Content/themes/datetimepicker/css/jquery.datetimepicker.css",
              //          "~/Content/themes/foundation/css/font-awesome.min.css"));

            //bundles.Add(new StyleBundle("~/bundles/Content/themes/footable/css").Include(
              //         "~/Content/themes/footable/css/footable.core.min.css"));

//            bundles.Add(new StyleBundle("~/bundles/Content/themes/planetshakers/css").Include(
  //                      "~/Content/themes/planetshakers/css/navigation-bar.css",
    //                    "~/Content/themes/planetshakers/css/planetshakers.css"));
        }
    }
}