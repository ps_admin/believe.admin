﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Planetshakers.Core.Common;
using Planetshakers.Core.DataInterfaces;
using Planetshakers.Core.Models;
using Planetshakers.Events.Helpers;
using Planetshakers.Events.Models;

namespace Planetshakers.Events.Security
{
    /// <summary>
    /// Since custom identity is stored in the context, this static helper class will help
    /// us to navigate the custom identity. 
    /// TODO: having static instance not a good practise, consider to use injection
    /// </summary>
    public class CurrentContext
    {
        private static readonly object lockObj = new object();
        private static CurrentContext _instance;

        public static CurrentContext Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (lockObj)
                    {
                        var repository = DependencyResolver.Current.GetService<IAccountRepository>();
                        _instance = new CurrentContext(repository);
                    }
                }

                return _instance;
            }
        }

        private readonly IAccountRepository _accountRepository;
        private readonly Hashtable _cacheData;

        /// <summary>
        /// Hide constructor, only singleton Instance can create the object
        /// </summary>
        private CurrentContext(IAccountRepository accountRepository)
        {
            if (accountRepository == null) throw new ArgumentNullException("accountRepository");

            _accountRepository = accountRepository;
            _cacheData = new Hashtable();
        }

        public IList<UrbanLife> UrbanLifeGroup
        {
            get
            {
                var key = User.ContactID + ".Group";
                var groups = _cacheData[key] as IList<UrbanLife>;

                if (groups != null)
                    return groups;
                


                return groups;
            }
        }

        public IList<UrbanLife> Groups
        {
            get
            {
                if (User == null)
                    throw new ArgumentNullException("User");

                var key = User.ContactID + ".Group";
                var groups = _cacheData[key] as IList<UrbanLife>;
                
                if (groups == null)
                {
                    //groups = _accountRepository.GetUrbanLifeGroups(User.ContactID, User.PastorOfRegionId);
                    var restrictedAccessULG = _accountRepository.GetAllRestrictedAccessUrbanLifeGroupByContactId(User.ContactID);
                    var regionalUrbalLife= _accountRepository.GetAllRegionByContactId(User.ContactID);
                    var urbanLifeGroupIds = new List<int>();
                    var regionalUrbanLifeGroupIds = new List<int>();

                    if (restrictedAccessULG.Any())
                    {
                        // Get all urban life groups by contact Id.
                        urbanLifeGroupIds = restrictedAccessULG.Select(ra => ra.ULGId).Distinct().ToList();
                    }

                    if (regionalUrbalLife.Any())
                    {
                        var regionIds = regionalUrbalLife.Select(r => r.RegionId).Distinct().ToList();
                        
                        // Get all urban life which is belong to the region.
                        regionalUrbanLifeGroupIds = _accountRepository.GetAllUrbanLifeIdsByRegionId(regionIds);
                    }

                    urbanLifeGroupIds.AddRange(regionalUrbanLifeGroupIds);

                    // At this point, we have to take only the distinct Urban Life Group Id.
                    var filteredULGIds = urbanLifeGroupIds.Distinct().OrderBy(a => a).ToList();

                    groups = _accountRepository.GetUrbanLifeGroupsById(filteredULGIds);

                    _cacheData[key] = groups;
                }

                return groups;
            }
        }

        public User User
        {
            get
            {
                if (HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    return JsonConverter.Deserialize<User>(HttpContext.Current.User.Identity.Name);
                }

                return null;
            }
        }

        public IList<ContactRole> ContactRole
        {
            get
            {
                if (User == null)
                    throw new ArgumentNullException("User");

                var key = string.Format("{0}.ContactRole", User.ContactID);
                var contactRoles = _cacheData[key] as IList<ContactRole>;

                if (contactRoles == null)
                {
                    contactRoles = _accountRepository.GetContactRoles(User.ContactID);

                    _cacheData[key] = contactRoles;
                }

                return contactRoles;
            }
        }

        public string Controller
        {
            get
            {
                if (HttpContext.Current == null)
                    return null;

                var controller =
                    HttpContext.Current.Request.RequestContext.RouteData.Values
                        .Where(p => p.Key == "controller")
                        .Select(p => p.Value)
                        .FirstOrDefault();

                if (controller == null)
                    return null;

                return controller.ToString().ToUpper();
            }
        }

        public string Action
        {
            get
            {
                if (HttpContext.Current == null)
                    return null;

                var controller =
                    HttpContext.Current.Request.RequestContext.RouteData.Values
                        .Where(p => p.Key == "action")
                        .Select(p => p.Value)
                        .FirstOrDefault();

                if (controller == null)
                    return null;

                return controller.ToString().ToUpper();
            }
        }

        public bool HasAccess()
        {
            return true;
            //var allowedRoles = new[] { "Church.Mobile PCR", "Church.Pastoral Staff" };
            //return allowedRoles.Any(role => HttpContext.Current.User.IsInRole(role.Trim()));
        }

        public bool HasManagementAccess()
        {
            if (ContactRole == null)
                return false;

            return ContactRole.Any(cr => cr.Name == RoleUtiliy.Role.UlCoach);
        }
    }
}