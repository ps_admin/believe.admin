﻿using System;
using System.Collections;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using Planetshakers.Core.DataInterfaces;
using Planetshakers.Events.Helpers;
using Planetshakers.Events.Models;

namespace Planetshakers.Events.Security
{
    public class CustomRoleProvider : RoleProvider
    {
        private readonly IAccountRepository _repository;
        private readonly Hashtable _roleCache;

        public CustomRoleProvider()
        {
            // RoleProvider requires empty constructor, hence it cannot be injected via constructor
            _repository = DependencyResolver.Current.GetService<IAccountRepository>();
            _roleCache = new Hashtable();
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            throw new NotImplementedException();
        }

        public override string[] GetRolesForUser(string username)
        {
            var user = JsonConverter.Deserialize<User>(username);
            var userroles = _roleCache[user.ContactID] as string[];

            if (userroles == null)
            {
                var roles = _repository.GetUserRoles(user.ContactID);
                userroles = roles.Select(x => x.Module + "." + x.Name).ToArray();
                _roleCache[user.ContactID] = userroles;
            }

            return userroles;
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string ApplicationName
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }
    }
}