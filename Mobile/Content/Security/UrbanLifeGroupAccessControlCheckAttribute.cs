﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Planetshakers.Events.Security
{
    public class UrbanLifeGroupAccessControlCheckAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var urbanLifeId = httpContext.Request.Params["urbanLifeId"];
            if (urbanLifeId != null)
            {
                Int32 id;
                if (Int32.TryParse(urbanLifeId, out id))
                    return CurrentContext.Instance.Groups.SingleOrDefault(x => x.Id == id) != null;
            }

            return false;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new RedirectToRouteResult(
                new RouteValueDictionary 
                    {
                        { "action", "Unauthorised" },
                        { "controller", "Error" }
                    });
        }
    }
}