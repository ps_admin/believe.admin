using log4net;
using Planetshakers.Core.DataInterfaces;
using Planetshakers.Events.Helpers;
using Planetshakers.Events.Models;
using Planetshakers.Events.Security;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Web.Mvc;
using System.Web.Security;
using Stripe;
using System.Net.Http;
using Stripe.Checkout;


/* This controller controls all things associate with accounts. This includes login, logout, course enrolment,
 * volunteer application and payments. 
 * */

namespace Planetshakers.Events.Controllers
{
    public class AccountController : Controller
    {
        private readonly IAccountRepository _accountRepository;
        private readonly string _recaptchaSecretKey = AzureServices.KeyVault_RetreiveSecret("believe-apps", "recaptchasecretkey");
        private readonly string _sendgridApi = AzureServices.KeyVault_RetreiveSecret("believe-apps", "SendGridAPIKey");

        public AccountController(IAccountRepository accountRepository)
        {
            _accountRepository = accountRepository;
        }

        #region LogOn and off

        public ActionResult LogOn()
        {
            if (Request.IsAuthenticated)
                return RedirectToAction("Index", "Home");

            return View();
        }

        [HttpPost]
        public ActionResult LogOn(LogOnModel model)
        {
            var response = Request.Form["g-recaptcha-response"];

            if (ModelState.IsValid)
            {
                var retryCounter = 0;

                do
                {
                    try
                    {
                        using (PlanetshakersEntities entity = new PlanetshakersEntities())
                        {

                            // Here is the login SQL query
                            var login = (from c in entity.Common_Contact
                                         where c.Email.Equals(model.UserName)
                                         select c).FirstOrDefault();


                            if (login != null && login.Password.Equals(model.Password))
                            {
                                var account = _accountRepository.Login(login.Email, login.Password);
                                if (account != null)
                                {
                                    var user = new User
                                    {
                                        ContactID = account.ContactId,
                                        Email = account.Email,
                                        FirstName = account.FirstName,
                                        PastorOfRegionId = account.PastorOfRegionId
                                    };
                                    Common_Contact contact = (from cc in entity.Common_Contact where cc.Contact_ID == account.ContactId select cc).FirstOrDefault();
                                    if (contact != null)
                                    {
                                        contact.LastLoginDate = DateTime.Now;
                                        entity.SaveChanges();
                                    }

                                    FormsAuthentication.RedirectFromLoginPage(JsonConverter.Serialize(user), model.RememberMe);
                                } else
                                {
                                    ModelState.AddModelError("", "You don't have access to this website.");
                                    return View(model);
                                }
                            }

                            ModelState.AddModelError("", "Either user name or password provided is incorrect.");
                        }
                        break;
                    }
                    catch (System.Data.SqlClient.SqlException ex)
                    {
                        if (ex.Number == -2)
                        {
                            retryCounter++;
                            Thread.Sleep(100 * retryCounter);
                            continue;
                        }

                        // If not timeout expired, retry the ex and let the main error handler
                        // to catch it
                        ModelState.AddModelError("", "Server timeout. Please try again.");
                        break;
                    }
                    catch (Exception ex)
                    {
                        ModelState.AddModelError("", ex.Message);
                        break;
                    }
                } while (retryCounter < 200);
            }
            else
            {
                ModelState.AddModelError("", "Recaptcha validation failed.");
                return View();
            }

            return View(model);
        }

        public ActionResult LogOff()
        {
            if (Request.IsAuthenticated)
            {
                FormsAuthentication.SignOut();
            }

            Planetshakers.Business.MWebMain.DeleteAllCookies(Request, Response);

            return RedirectToAction("LogOn", "Account");
        }
        #endregion

        public ActionResult ForgetPassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ForgetPassword(String Email)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                Common_Contact c = null;
                try
                {
                    c = (from cc in context.Common_Contact
                         where cc.Email == Email
                         select cc).First();
                }
                catch (Exception)
                {
                    TempData.Add("userNotRegistered", "The email provided is not registerred yet. Please register via the link provided on the home page");
                    return RedirectToAction("LogOn");
                }

                using (MD5CryptoServiceProvider crypto = new MD5CryptoServiceProvider())
                {
                    String firstName = c.FirstName;
                    String lastName = c.LastName;
                    byte[] key = crypto.ComputeHash(ASCIIEncoding.UTF8.GetBytes(firstName));
                    byte[] ivKey = crypto.ComputeHash(ASCIIEncoding.UTF8.GetBytes(lastName));

                    String keyString = HashBuilder(firstName);//ASCIIEncoding.UTF8.GetString(key);
                    String ivKeyString = HashBuilder(lastName);//ASCIIEncoding.UTF8.GetString(ivKey);

                    int keySize = 256;
                    String encryptedString = Encrypt(DateTime.Now.ToString(), key, ivKey, keySize);

                    MailMessage mail = new MailMessage();
                    // mail server configuration. temporarily used the following email address
                    SmtpClient server = new SmtpClient("smtp.sendgrid.net");
                    server.Credentials = new System.Net.NetworkCredential("apikey", _sendgridApi);
                    server.Port = 587;
                    mail.IsBodyHtml = true;
                    mail.From = new MailAddress("info@believeglobal.org", "Administrator");
                    mail.To.Add(Email);
                    mail.Subject = @"Password recovery";
                    //mail.Body = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">";
                    mail.Body += "<HTML><HEAD><META http-equiv=Content-Type content=\"text/html; charset=iso-8859-1\">";
                    mail.Body += mail.Body += "</HEAD><BODY>" +
                                "<div><span style=\"font-size:12px\">Dear ";

                    if (c.Gender.Equals("Male"))
                    {
                        mail.Body += "Mr. ";
                    }
                    else
                    {
                        mail.Body += "Ms. ";
                    }

                    mail.Body += lastName + ",</span></div>" +
                                "<br>" +
                                "<DIV style=\"font-size:12px\">To Reset password please click <a href=\"" +
                                Request.Url.Scheme + System.Uri.SchemeDelimiter + Request.Url.Host +
                                (
                                //Request.Url.IsDefaultPort ? "" : 
                                ":" +
                                Request.Url.Port +
                                "/Account/RetrieveLogin?"
                                + "Email=" + Email + "&Token=" + encryptedString +
                                "\">here</a></DIV><DIV style=\"font-size:12px\">This is an automatically generated email please do not reply to this address</DIV><DIV></BODY><BR></HTML>");

                    mail.Body += "<div style=\"font-size:12px\">Kind regards,</div>";
                    mail.Body += "<div style=\"font-size:12px\"><b><br></b></div>";
                    mail.Body += "<div style=\"font-size:12px\"><br>Administrator</div>";
                    mail.Body += "<a href=\"http://www.planetshakers.com/\" style=\"color:rgb(17,85,204);font-size:12px\" target=\"_blank\"><img src=\"http://static.ideasunplugged.com/signature/s_155/t_sanN5b.jpg?v=7\" style=\"border:none;margin:3px 3px 0px 0px\"></a>";
                    mail.Body += "<span style=\"font-size:12px\"></span>";
                    mail.Body += "<div style=\"font-size:12px;margin:5px 5px 15px 0px;border:none\">Planetshakers City Church<br>PO BOX 641, BOX HILL VIC 3128<br>607 Canterbury Road, Surrey Hills VIC 3127<br>P: 1300 88 33 21<br>F: 9830 7683<br>W:&nbsp;<a href=\"http://www.planetshakers.com/\" style=\"color:rgb(17,85,204)\" target=\"_blank\">www.planetshakers.com</a></div>";
                    mail.Body += "<table style=\"font-size:12px;margin:5px 5px 0px 0px\"><tbody><tr><td><a href=\"http://www.planetshakers.com/promoSIG/icon1.php\" style=\"color:rgb(17,85,204)\" target=\"_blank\"><img width=\"32px\" src=\"http://www.planetshakers.com/promoSIG/icon1.jpg\"></a></td><td><a href=\"http://www.planetshakers.com/promoSIG/icon2.php\" style=\"color:rgb(17,85,204)\" target=\"_blank\"><img width=\"32px\" src=\"http://www.planetshakers.com/promoSIG/icon2.jpg\"></a></td><td><a href=\"http://www.planetshakers.com/promoSIG/icon3.php\" style=\"color:rgb(17,85,204)\" target=\"_blank\"><img width=\"32px\" src=\"http://www.planetshakers.com/promoSIG/icon3.jpg\"></a></td><td><a href=\"http://www.planetshakers.com/promoSIG/icon4.php\" style=\"color:rgb(17,85,204)\" target=\"_blank\"><img width=\"32px\" src=\"http://www.planetshakers.com/promoSIG/icon4.jpg\"></a></td><td><a href=\"http://www.planetshakers.com/promoSIG/icon5.php\" style=\"color:rgb(17,85,204)\" target=\"_blank\"><img width=\"32px\" src=\"http://www.planetshakers.com/promoSIG/icon5.jpg\"></a></td><td><a href=\"http://www.planetshakers.com/promoSIG/icon6.php\" style=\"color:rgb(17,85,204)\" target=\"_blank\"><img width=\"32px\" src=\"http://www.planetshakers.com/promoSIG/icon6.jpg\"></a></td><td><a href=\"http://www.planetshakers.com/promoSIG/icon7.php\" style=\"color:rgb(17,85,204)\" target=\"_blank\"><img width=\"32px\" src=\"http://www.planetshakers.com/promoSIG/icon7.jpg\"></a></td></tr></tbody></table>";
                    server.EnableSsl = true;
                    server.Send(mail);


                }
                ViewBag.forgetPass = " An email has been sent to your provided email address, please follow the instruction given on the email.";
            }

            return View();
        }

        /* reset password method 
         * this method will be activated once the user go to the link provided on the email that was generated from forgetpassword method
         */
        public ActionResult ResetPassword()
        {
            if (Session["ContactReset"] != null)
                return View();
            else
            {
                TempData.Add("userNotFound", "There is a problem with the link provided");
                return RedirectToAction("LogOn");
            }
        }

        [HttpPost]
        public ActionResult ResetPassword(String NewPassword, string ConfirmedPassword)
        {
            Common_Contact c = (Common_Contact)Session["ContactReset"];
            using (PlanetshakersEntities ps = new PlanetshakersEntities())
            {
                Common_Contact con = (from cc in ps.Common_Contact
                                      where cc.Contact_ID == c.Contact_ID
                                      select cc).FirstOrDefault();

                if (NewPassword.Equals(ConfirmedPassword))
                {
                    con.Password = NewPassword;
                    ps.SaveChanges();
                    TempData.Add("successResetPass", "Password has been succesfully changed!");
                    Session["ContactReset"] = null;
                    return RedirectToAction("LogOn");
                }
                else
                {
                    ViewBag.passResetResult = "Password change was unsuccesful, Please try again";
                    return View();
                }
            }

        }

        public ActionResult RetrieveLogin(String email, String token)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                Common_Contact contact = null;

                try
                {
                    contact = (from cc in context.Common_Contact
                               where cc.Email == email
                               select cc).First();
                }
                catch (Exception ex)
                {
                    TempData.Add("userNotFound", "Cant find username " + email);
                    return RedirectToAction("LogOn");

                    throw ex;
                }

                if (contact != null)
                {
                    using (MD5CryptoServiceProvider crypto = new MD5CryptoServiceProvider())
                    {
                        String firstName = contact.FirstName;
                        String lastName = contact.LastName;
                        String currentpassword = contact.Password;
                        byte[] key = crypto.ComputeHash(ASCIIEncoding.UTF8.GetBytes(firstName));
                        byte[] ivKey = crypto.ComputeHash(ASCIIEncoding.UTF8.GetBytes(lastName));

                        //String keyString = Convert.ToBase64String(key);
                        //String ivKeyString = Convert.ToBase64String(ivKey);


                        int keySize = 256;

                        String decriptedString = Decrypt(token, key, ivKey, keySize);
                        DateTime timeCreated = DateTime.Parse(decriptedString);
                        DateTime current = DateTime.Now;
                        TimeSpan diff = current.Subtract(timeCreated);

                        // diff represents the expiry time span of the link given to the user
                        // can be changed to different values 

                        if (diff.Minutes <= 10)
                        {

                            using (PlanetshakersEntities ps = new PlanetshakersEntities())
                            {
                                Common_Contact c = (from cc in ps.Common_Contact where cc.FirstName.Equals(firstName) && cc.LastName.Equals(lastName) select cc).FirstOrDefault();
                                if (c == null)
                                {
                                    TempData.Add("userNotFound", "There is a problem with the link provided");
                                    return RedirectToAction("LogOn");
                                }
                                else
                                {
                                    Session.Add("ContactReset", c);
                                    return RedirectToAction("ResetPassword");
                                }
                            }

                        }
                        else
                        {
                            TempData.Add("linkExpired", "The link has been expired please request for a new link");
                            return RedirectToAction("LogOn");
                        }
                    }
                }
                else
                {
                    TempData.Add("userNotFound", "Cant find username " + email);
                    return RedirectToAction("Login");
                }
            }
        }

        //    [PSAuthorize]
        // Not in use at the moment.
        public ActionResult CCPayment()
        {
            var model = new CCPaymentModel();
            var stripePublishKey = ConfigurationManager.AppSettings["stripePublishableKey"];
            ViewBag.StripePublishKey = stripePublishKey;

            return View(model);
        }

        // Summary page for payments
        public ActionResult Summary(string returnUrl, string error = null)
        {
            var model = Session["Cart"] as CartModel;
   
            if (model == null)
            {
                model = new CartModel()
                {
                    Cart = null
                };
            }

            var contact = CurrentContext.Instance.User.ContactID;

            if (contact == 0)
                return RedirectToAction("LogOn", "Account");

            // Setup the CCPayment model            
            if (model.CCPayment == null) model.CCPayment = new CCPaymentModel();

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var bankAccount = new Common_BankAccount();

                /* Retrieve Bank Account information to transfer funds into */
                bankAccount = (from ba in context.Common_BankAccount
                               join ec in context.Events_Conference
                               on  ba.BankAccount_ID equals ec.BankAccount_ID
                               where ec.Conference_ID == model.Event.id
                               select ba).FirstOrDefault();

                model.CCPayment.BankAccount = bankAccount;
            }

            var successUrl = "http://" + HttpContext.Request.Url.Authority + "/Account/addRegistration?venue_id=" + model.Event.venue_id + "&contact_id=" + model.ContactId;
            var cancelUrl = HttpContext.Request.Url.AbsoluteUri;

            StripeConfiguration.ApiKey = model.CCPayment.BankAccount.SecretKey;

            var options = new Stripe.Checkout.SessionCreateOptions
            {
                PaymentMethodTypes = new List<string> { "card" },
                LineItems = new List<SessionLineItemOptions> { },
                Mode = "payment",
                PaymentIntentData = new SessionPaymentIntentDataOptions { },
                SuccessUrl = successUrl,
                CancelUrl = cancelUrl
            };


            foreach (var item in model.Cart)
            {
                options.LineItems.Add(
                    new SessionLineItemOptions
                    {
                        PriceData = new SessionLineItemPriceDataOptions
                        {
                            Currency = model.CCPayment.BankAccount.Currency,
                            ProductData = new SessionLineItemPriceDataProductDataOptions
                            {
                                Name = model.Event.name + " - " + item.reg_name
                            },
                            UnitAmount = Convert.ToInt32(item.single_price) * 100
                        },
                        Quantity = item.quantity
                    });
            }

            var service = new Stripe.Checkout.SessionService();
            options.PaymentIntentData.Description = model.Event.name;
            Stripe.Checkout.Session session = service.Create(options);

            model.CCPayment.transactionReference = session.PaymentIntentId;

            Session["BankAccount"] = model.CCPayment.BankAccount;
            Session["TransactionReference"] = session.PaymentIntentId;

            ViewData["SessionID"] = session.Id;

            // remove error message for session model
            Session["Cart"] = model;

            model.ErrorMessage = error;

            ViewBag.StripePublishableKey = model.CCPayment.BankAccount.PublishableKey;

            return View(model);
        }

        public ActionResult addRegistration(int venue_id, int contact_id)
        {
            var cartModel = Session["Cart"] as CartModel;
            var transactionReference = Session["transactionReference"] as String;

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                // Retrieve contact details of purchase person
                var contact = (from cc in context.Common_Contact
                               where cc.Contact_ID == contact_id
                               select cc).FirstOrDefault();

                var conference_id = (from v in context.Events_Venue
                                     where v.Venue_ID == venue_id
                                     select v.Conference_ID).FirstOrDefault();

                var is_group_rego = true;

                // If cart only has 1 item, it goes into group payment
                if (cartModel.Cart.Any(x => x.reg_id > 0))
                {
                    // Insert payment and registration into database
                    InsertEventRegistration(is_group_rego);
                    InsertPayment(transactionReference, is_group_rego);

                    SendEventsEmail(transactionReference, is_group_rego);

                    // Clear the Cart
                    Session["Cart"] = null;

                    // Force single registrations to reload
                    Session["registrations_single"] = null;
                    TempData["recently_registered"] = true;

                    // Update session.
                    Session["Cart"] = cartModel;
                    Session["success"] = "Payment and event registration is successful.";

                    return RedirectToAction("RegistrationSuccess", "Administration");
                }
                else
                {
                    InsertPayment(transactionReference, is_group_rego);

                    // Clear the Cart
                    Session["Cart"] = null;

                    // Force single registrations to reload
                    Session["registrations_single"] = null;
                    TempData["recently_registered"] = true;

                    // Update session.
                    Session["Cart"] = cartModel;
                    Session["success"] = "Payment and event registration is successful.";

                    return RedirectToAction("GroupDetails", "Registration", new { Group_ID = contact_id, Conference_ID = conference_id });
                }
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CCPayment(string stripeEmail, string stripeToken)
        {
            // Initiate Logger
            ILog pxlogger = LogManager.GetLogger("GeneralLogger");

            try
            {
                // Retrieve some cart data from session
                var cartModel = Session["Cart"] as CartModel;

                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    /* Retrieve Bank Account information to transfer funds into */
                    var bankAccounts = (from ba in context.Common_BankAccount
                                            join ec in context.Events_Conference
                                            on ba.BankAccount_ID equals ec.BankAccount_ID
                                            where ec.Conference_ID == cartModel.Event.id
                                        select ba).ToList();

                    if (!bankAccounts.Any())
                    {
                        // Something went wrong
                    }

                    var bankAcc = bankAccounts.First();

                    // Setup variables to retrieve responses from the bank
                    string eventName = "";
                    foreach (CartItem item in cartModel.Cart)
                    {
                        for (var i = 0; i < item.quantity; i++)
                        {
                            eventName = Convert.ToString(item.event_name);
                        }
                    }

                    string bankClientID = bankAcc.ClientID;
                    string bankCertName = bankAcc.CertificateName;
                    string bankCertPassPhrase = bankAcc.CertificatePassphrase;
                    string transactionResponse = String.Empty;
                    string transactionReference = String.Empty;

                    bool paymentSuccess = false;

                    // Stripe code
                    //var customers = new StripeCustomerService();
                    //var charges = new StripeChargeService();

                    #region Process Payment

                    /*StripeConfiguration.SetApiKey(bankAcc.SecretKey);

                    var customer = customers.Create(new StripeCustomerCreateOptions
                    {
                        Email = stripeEmail,
                        SourceToken = stripeToken
                    });

                    if (cartModel.Cart.Any())
                    {
                        foreach (CartItem item in cartModel.Cart)
                        {
                            var amount = Convert.ToInt32(item.subtotal) * 100;
                            var charge = charges.Create(new StripeChargeCreateOptions
                            {
                                Amount = amount,       // amount must be 50 cents
                                Description = "Event Registration for " + item.event_name.ToString(),
                                Currency = bankAcc.Currency,
                                CustomerId = customer.Id
                            });

                            if (charge.Status == "succeeded")
                            {
                                paymentSuccess = true;
                                string criteria = charge.StripeResponse.ResponseJson.ToString();
                                string category = "PaywithCard Click";
                                //Log to Common_Accesslog table
                                Business.MWebMain.LogAction(ref category, ref criteria, CurrentContext.Instance.User.ContactID.ToString());
                            }

                            transactionReference = charge.Id.ToString();
                        }
                    }
                    else
                    {
                        var amount = Convert.ToInt32(cartModel.Subtotal) * 100;
                        var charge = charges.Create(new StripeChargeCreateOptions
                        {
                            Amount = amount,       // amount must be 50 cents
                            Description = "Event Registration for " + cartModel.Event.name,
                            Currency = bankAcc.Currency,
                            CustomerId = customer.Id
                        });

                        if (charge.Status == "succeeded")
                        {
                            paymentSuccess = true;

                            string criteria = charge.StripeResponse.ResponseJson.ToString();
                            string category = "PaywithCard Click";
                            //Log to Common_Accesslog table
                            Business.MWebMain.LogAction(ref category, ref criteria, CurrentContext.Instance.User.ContactID.ToString());
                        }

                        transactionReference = charge.Id.ToString();
                    }*/
                    #endregion

                    #region Post Processing
                    // Add database entry
                    if (paymentSuccess)
                    {
                        pxlogger.Info("Payment success.");
                        // At this point, transaction is successful.
                        // Create all the required rows for registrations and payments 

                        // Retrieve contact details of purchase person
                        var contact = (from cc in context.Common_Contact
                                       where cc.Contact_ID == cartModel.ContactId
                                       select cc).FirstOrDefault();

                        var contact_id = cartModel.ContactId;
                        var conference_id = cartModel.Event.id;

                        var is_group_rego = true;
                        

                        // Cart has 1 item - so it goes into group payment
                        if (cartModel.Cart.Any(x => x.reg_id > 0))
                        {
                            // Insert payment and registration into database
                            InsertEventRegistration(is_group_rego);
                            InsertPayment(transactionReference, is_group_rego);

                            SendEventsEmail(transactionReference, is_group_rego);

                            // Clear the Cart
                            pxlogger.Info("Emptying cart.");
                            Session["Cart"] = null;

                            // Force single registrations to reload
                            Session["registrations_single"] = null;
                            TempData["recently_registered"] = true;
                            pxlogger.Info("Reloading page.");
                            pxlogger.Info("------------------------------------------------------------------------------------------------------");

                            // Update session.
                            Session["Cart"] = cartModel;
                            Session["success"] = "Payment and event registration is successful.";

                            return RedirectToAction("RegistrationSuccess", "Administration");
                        }
                        else
                        {
                            InsertPayment(transactionReference, is_group_rego);

                            // Clear the Cart
                            pxlogger.Info("Emptying cart.");
                            Session["Cart"] = null;

                            // Force single registrations to reload
                            Session["registrations_single"] = null;
                            TempData["recently_registered"] = true;
                            pxlogger.Info("Reloading page.");
                            pxlogger.Info("------------------------------------------------------------------------------------------------------");

                            // Update session.
                            Session["Cart"] = cartModel;
                            Session["success"] = "Payment and event registration is successful.";

                            return RedirectToAction("GroupDetails", "Registration", new { Contact_ID = contact_id, Conference_ID = conference_id });
                        }
                    }
                    else
                    {
                        Session["success"] = "Error";
                        return View("Error");
                        throw new Exception("An error occured while processing the payment. "
                                            + transactionResponse);
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                var cartModel = Session["Cart"] as CartModel;
                if (cartModel != null)
                {
                    //cartModel.CCPayment = model;
                    //Session["Cart"] = cartModel;
                }
                else
                {
                    pxlogger.Error(ex.Message);
                }

                return RedirectToAction("Summary", "Account", new { error = ex.Message });
            }
        }
        public void SendEventsEmail(string transactionRef, bool is_group_rego)
        {
            var user_id = CurrentContext.Instance.User.ContactID;

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                /* prepare the confirmation email model */
                ILog pxlogger = LogManager.GetLogger("GeneralLogger");
                // Retrieve some cart data from session
                var cartModel = Session["Cart"] as CartModel;

                pxlogger.Info("Preparing confirmation email model.");
                var contact = (from cc in context.Common_Contact
                               where cc.Contact_ID == cartModel.ContactId
                               select cc).FirstOrDefault();

                var email_model = new RegistrationReceiptEmailModel()
                {
                    Email = contact.Email,
                    ContactId = contact.Contact_ID,
                    Event = cartModel.Event,
                    Items = new List<RegistrationConfirmationItem>(),
                    EventName = cartModel.Event.name,
                    EventDate = cartModel.Event.date,
                    EventVenue = cartModel.Event.location,
                    Currency = cartModel.Event.currency,
                    FullName = contact.FirstName + " " + contact.LastName,
                    Subject = "Registration Confirmation - " + cartModel.Event.name,
                    ReceiptNumber = cartModel.CCPayment.payment_id.ToString(),
                    PaymentType = (cartModel.CCPayment != null ? cartModel.CCPayment.PaymentType : "N/A"),
                    TransactionReference = (cartModel.CCPayment != null ? (!String.IsNullOrEmpty(cartModel.CCPayment.transactionReference) ? cartModel.CCPayment.transactionReference : "N/A") : "N/A"),
                    Promo = (cartModel.Voucher != null ? cartModel.Voucher : null)
                };

                // create an entry for the registration
                pxlogger.Info("Creating entry for registration.");
                foreach (CartItem item in cartModel.Cart)
                {
                    pxlogger.Debug("Creating entries for " + item.event_name + " " + item.reg_name + " | Quantity :" + item.quantity + " | Contact Id: " + cartModel.ContactId);

                    pxlogger.Debug("Adding items detail to email.");
                    email_model.Items.Add(new RegistrationConfirmationItem
                    {
                        reference_number = transactionRef,
                        reg_name = item.reg_name,
                        qty = item.quantity,
                        price = item.single_price
                    });
                }

                // Prepare to send 
                pxlogger.Info("Sending email.");
                if (!String.IsNullOrEmpty(email_model.Email))
                {
                    new MailController().RegistrationReceipt(email_model);
                    CreateEventLog(email_model.ContactId, is_group_rego, "Update", "Sent Tax Invoice Email");
                }
            }
        }

        public void InsertEventRegistration(bool isGroup)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (PlanetshakersEntities context = new PlanetshakersEntities())
                    {
                        var cartModel = Session["Cart"] as CartModel;

                        Common_Contact cc = (from cc1 in context.Common_Contact
                                             where cc1.Contact_ID == cartModel.ContactId
                                             select cc1).FirstOrDefault();

                        
                        var isRegistered = ((from er in context.Events_Registration
                                             where er.Contact_ID == cartModel.ContactId
                                                     && er.Venue_ID == cartModel.Event.venue_id
                                             select er).FirstOrDefault() != null ? true : false);

                        if (!isRegistered)
                        {
                            foreach (var i in cartModel.Cart)
                            {
                                var new_rego = new Events_Registration();
                                new_rego.Contact_ID = cartModel.ContactId;
                                new_rego.GroupLeader_ID = null;
                                new_rego.FamilyRegistration = false;
                                new_rego.RegisteredByFriend_ID = null;
                                new_rego.Venue_ID = cartModel.Event.venue_id;
                                new_rego.RegistrationType_ID = i.reg_id;
                                new_rego.RegistrationDiscount = 0;
                                new_rego.Elective_ID = null;
                                new_rego.Accommodation = false;
                                new_rego.Accommodation_ID = null;
                                new_rego.Catering = false;
                                new_rego.CateringNeeds = "";
                                new_rego.LeadershipBreakfast = false;
                                new_rego.LeadershipSummit = false;
                                new_rego.ULG_ID = null;
                                new_rego.ParentGuardian = "";
                                new_rego.ParentGuardianPhone = "";
                                new_rego.AcceptTermsRego = true;
                                new_rego.AcceptTermsParent = false;
                                new_rego.AcceptTermsCreche = false;
                                new_rego.RegistrationDate = DateTime.Now;
                                new_rego.RegisteredBy_ID = cartModel.ContactId;
                                new_rego.Attended = false;
                                new_rego.Volunteer = false;
                                new_rego.VolunteerDepartment_ID = null;
                                new_rego.ComboRegistration_ID = null;

                                context.Events_Registration.Add(new_rego);
                            }
                            context.SaveChanges();
                        }
                    }
                }
                catch (Exception e)
                {
                    ViewBag.ErrorMessage = "Error creating registrations";
                    throw e;
                }
            }
        }

        private void InsertPayment(string transactionRef, bool isGroup)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (PlanetshakersEntities context = new PlanetshakersEntities())
                    {
                        var cartModel = Session["Cart"] as CartModel;

                        var bank_account_id = (from c in context.Events_Conference
                                               where c.Conference_ID == cartModel.Event.id
                                               select c.BankAccount_ID).FirstOrDefault();

                        Common_Contact cc = (from cc1 in context.Common_Contact
                                             where cc1.Contact_ID == cartModel.ContactId
                                             select cc1).FirstOrDefault();

                        if (isGroup)
                        {
                            #region Insert Group Payment Log into database
                            var new_group_payment = new Events_GroupPayment();
                            new_group_payment.GroupLeader_ID = cartModel.ContactId;
                            new_group_payment.Conference_ID = cartModel.Event.id;
                            new_group_payment.PaymentType_ID = 3; // Credit Card
                            new_group_payment.PaymentAmount = cartModel.GrandTotal;
                            new_group_payment.CCNumber = String.Empty;
                            new_group_payment.CCExpiry = String.Empty;
                            new_group_payment.CCName = String.Empty;
                            new_group_payment.CCPhone = String.Empty;
                            new_group_payment.CCManual = false;
                            new_group_payment.CCTransactionRef = transactionRef;
                            new_group_payment.CCRefund = false;
                            new_group_payment.ChequeDrawer = String.Empty;
                            new_group_payment.ChequeBank = String.Empty;
                            new_group_payment.ChequeBranch = String.Empty;
                            new_group_payment.PaypalTransactionRef = String.Empty;
                            new_group_payment.Comment = cartModel.Comments;
                            new_group_payment.PaymentCompletedDate = DateTime.Now;
                            new_group_payment.PaymentBy_ID = cartModel.ContactId;
                            new_group_payment.BankAccount_ID = bank_account_id;

                            context.Events_GroupPayment.Add(new_group_payment);
                            #endregion
                        }

                        context.SaveChanges();
                    }
                } catch (Exception e)
                {
                    ViewBag.ErrorMessage = "Error creating payment log";
                    throw e;
                }
            }
        }

        #region Select List

        // Retrieve list of genders (not from the database)
        private List<SelectListItem> get_genders()
        {
            var gendersList = new List<SelectListItem>();

            gendersList.Add(new SelectListItem()
            {
                Text = "Male",
                Value = "Male",
            });
            gendersList.Add(new SelectListItem()
            {
                Text = "Female",
                Value = "Female",
            });

            return gendersList;
        }

        // Retrieve list of salutations from the database
        private List<SelectListItem> getSalutations()
        {
            // only continue if salutations have not been previously retrieved
            if (Session != null && Session["salutations"] != null) return Session["salutations"] as List<SelectListItem>;

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var salutations = (from cs in context.Common_Salutation
                                   orderby cs.SortOrder
                                   select new Salutation
                                   {
                                       id = cs.Salutation_ID,
                                       name = cs.Salutation
                                   }).ToList();

                // Convert into select list
                var salutationList = salutations.Select(x => new SelectListItem
                {
                    Text = x.name,
                    Value = x.id.ToString()
                }).ToList();

                Session["salutations"] = salutationList;

                return salutationList;
            }
        }

        #endregion

        #region Helper Functions

        private Common_Contact getContactDetails()
        {
            // Retrieve logged in contact from session

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                // Retrieve contact details (if available)
                var ps_contact = (from cc in context.Common_Contact
                                  where cc.Contact_ID == CurrentContext.Instance.User.ContactID
                                  select cc).First();
                return ps_contact;
            }
        }

        private void CreateEventLog(int contact_id, bool is_group_rego, string action, string item)
        {
            var cartModel = Session["Cart"] as CartModel;
            var user_id = CurrentContext.Instance.User.ContactID;

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                
                var new_history = new Common_ContactHistory();
                new_history.Contact_ID = contact_id;
                new_history.Module = "Events";
                new_history.Desc = String.Empty;
                new_history.Desc2 = String.Empty;
                new_history.DateChanged = DateTime.Now;
                new_history.User_ID = user_id;
                new_history.Action = action;
                new_history.Item = item;
                new_history.Table = String.Empty;
                new_history.FieldName = String.Empty;
                new_history.OldValue = String.Empty;
                new_history.OldText = String.Empty;
                new_history.NewValue = String.Empty;
                new_history.NewText = String.Empty;
                context.Common_ContactHistory.Add(new_history);

                context.SaveChanges();
            }
        }

        #endregion

        #region Encryption
        private static string Encrypt(String plainStr, byte[] key, byte[] ivKey, int keySize)
        {
            AesCryptoServiceProvider aesEncryption = new AesCryptoServiceProvider();
            aesEncryption.IV = ivKey;
            aesEncryption.Key = key;
            byte[] encrypted;
            ICryptoTransform crypto = aesEncryption.CreateEncryptor(aesEncryption.Key, aesEncryption.IV);
            using (MemoryStream msEncrypt = new MemoryStream())
            {
                using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, crypto, CryptoStreamMode.Write))
                {
                    using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                    {

                        //Write all data to the stream.
                        swEncrypt.Write(plainStr);
                    }
                    encrypted = msEncrypt.ToArray();
                }
            }
            return Convert.ToBase64String(encrypted);
        }

        /* decryption method to decript the part of the link that was given to the user n password reset action
         * 
         */

        private static string Decrypt(String encryptedText, byte[] key, byte[] ivKey, int keySize)
        {
            encryptedText = encryptedText.Replace(" ", "+");
            AesCryptoServiceProvider aesEncryption = new AesCryptoServiceProvider();
            aesEncryption.IV = ivKey;
            aesEncryption.Key = key;
            String plaintext = null;
            ICryptoTransform decrypto = aesEncryption.CreateDecryptor(aesEncryption.Key, aesEncryption.IV);
            byte[] encryptedByte = Convert.FromBase64String(encryptedText);
            using (MemoryStream msDecrypt = new MemoryStream(encryptedByte))
            {
                using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decrypto, CryptoStreamMode.Read))
                {
                    using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                    {

                        // Read the decrypted bytes from the decrypting stream 
                        // and place them in a string.
                        plaintext = srDecrypt.ReadToEnd();
                    }
                }
            }
            return plaintext;
        }

        private static string HashBuilder(String s)
        {
            using (MD5CryptoServiceProvider crypto = new MD5CryptoServiceProvider())
            {
                byte[] b = crypto.ComputeHash(ASCIIEncoding.UTF8.GetBytes(s));
                StringBuilder strBuild = new StringBuilder();
                for (int i = 0; i < b.Length; i++)
                {
                    strBuild.Append(b[i].ToString("x2"));
                }
                return strBuild.ToString();
            }
        }
        #endregion

        #region AJAX/Json calls (mailing list)

        //[HttpPost]
        //public ActionResult SubscribeML(int ml_id)
        //{
        //    using (PlanetshakersEntities context = new PlanetshakersEntities())
        //    {

        //        var subscriptionTypes = (from mlt in context.Common_MailingListType
        //                                 select mlt).ToList();
        //        foreach (var type in subscriptionTypes)
        //        {
        //            // check if previously subscribed
        //            var pastSub = (from ccml in context.Common_CombinedContactMailingList
        //                           where ccml.Contact_ID == CurrentContext.Instance.User.ContactID &&
        //                                 ccml.MailingList_ID == ml_id &&
        //                                 ccml.MailingListType_ID == type.MailingListType_ID
        //                           select ccml).ToList();

        //            // Update and set to active if past subscription found, otherwise create new
        //            if (pastSub.Any())
        //            {
        //                pastSub.First().SubscriptionActive = true;
        //            }
        //            else
        //            {
        //                var new_sub = new Common_CombinedContactMailingList();
        //                new_sub.MailingListType_ID = type.MailingListType_ID;
        //                new_sub.MailingList_ID = ml_id;
        //                new_sub.Contact_ID = CurrentContext.Instance.User.ContactID;
        //                new_sub.SubscriptionActive = true;
        //                context.Common_CombinedContactMailingList.Add(new_sub);
        //            }

        //            // save changes
        //            context.SaveChanges();
        //        }
        //    }

        //    return Json(true);
        //}

        //[HttpPost]
        //public ActionResult UnsubscribeML(int ml_id)
        //{

        //    using (PlanetshakersEntities context = new PlanetshakersEntities())
        //    {

        //        var mls = (from cml in context.Common_CombinedContactMailingList
        //                   where cml.Contact_ID == CurrentContext.Instance.User.ContactID &&
        //                         cml.MailingList_ID == ml_id
        //                   select cml).ToList();

        //        foreach (Common_CombinedContactMailingList ml in mls)
        //        {
        //            ml.SubscriptionActive = false;
        //        }

        //        context.SaveChanges();

        //        return Json(true);
        //    }
        //}

        [HttpPost]
        public ActionResult verifyCaptcha(string token)
        {
            const string url = "https://www.google.com/recaptcha/api/siteverify";

            string urlToPost = $"{url}?secret={_recaptchaSecretKey}&response={token}";

            var response = new reCaptchaResponse();
            //var responseString = "";

            using (var httpClient = new HttpClient())
            {
                try
                {
                    response = JsonConverter.Deserialize<reCaptchaResponse>(httpClient.GetStringAsync(urlToPost).Result);
                    //responseString = httpClient.GetStringAsync(urlToPost).Result;
                }
                catch
                {
                    //TODO: Error handling
                }
            }

            return Json(response);
        }

        #endregion

        #region Access Control
        public bool HasVIPAccess()
        {
            var person = getContactDetails();
            return false;
        }

        public bool HasEventAdminAccess()
        {
            var person = getContactDetails();
            var databaseRoles = new List<int> { 1, 62 };
            var access = false;

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                access = (from cdr in context.Common_ContactDatabaseRole
                          where cdr.Contact_ID == person.Contact_ID 
                          && databaseRoles.Contains(cdr.DatabaseRole_ID)
                          select cdr).Any();
            }

            return access;
        }
        #endregion
    }

    public class reCaptchaResponse
    {
        public bool success { get; set; }
        public string challenge_ts { get; set; }
        public decimal score { get; set; }
        public string hostname { get; set; }
        public string action { get; set; }
        //public List<string> error_codes { get; set; }
    }


}