﻿using log4net;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.Mvc;
using Planetshakers.Core.Models;
using Planetshakers.Events.Models;
using Planetshakers.Events.Security;
using Planetshakers.Core.DataInterfaces;
using Stripe;
using SelectPdf;
using System.Web;
using System.IO;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using Stripe.Checkout;
using System.Data.Entity;
using FlexCel.XlsAdapter;

namespace Planetshakers.Events.Controllers
{
    [Authorize]
    public class AdministrationController : Controller
    {
        private readonly IAdministrationRepository _repository;

        public AdministrationController(IAdministrationRepository repository)
        {
            _repository = repository;
        }

        public ActionResult RegistrationAdminIndex()
        {
            return View();
        }

        #region Conference Registrations

        public ActionResult Registrations()
        {
            var model = new PurchaseViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult SearchContact(string query)
        {
            if (query != null)
            {
                int i;

                if (query.Length < 3)
                {
                    ViewBag.Message = "Query is too short. Please provide the full name or full email.";
                    return View("Error");
                }

                if (int.TryParse(query, out i))
                {
                    ViewBag.Message = "Invalid query. Please search by full name or full email.";
                    return View("Error");
                }

                try
                {
                    var model = new PurchaseViewModel();

                    using (PlanetshakersEntities context = new PlanetshakersEntities())
                    {
                        model.searchList = new List<ContactDetails>();

                        // Search for names with spaces
                        if (query.Contains(" "))
                        {
                            var substrings = query.ToLower().Split();

                            var list = (from cc in context.Common_Contact
                                        where (cc.FirstName + " " + cc.LastName == query || cc.LastName + " " + cc.FirstName == query)
                                        select new ContactDetails
                                        {
                                            Id = cc.Contact_ID,
                                            FirstName = cc.FirstName,
                                            LastName = cc.LastName,
                                            ContactNumber = cc.Mobile,
                                            Email = cc.Email
                                        }).ToList();

                            model.searchList.AddRange(list);

                            foreach (var s in substrings)
                            {
                                list = (from cc in context.Common_Contact
                                        where (cc.FirstName.StartsWith(s.ToLower()) || cc.LastName.StartsWith(s.ToLower()))
                                        select new ContactDetails
                                        {
                                            Id = cc.Contact_ID,
                                            FirstName = cc.FirstName,
                                            LastName = cc.LastName,
                                            ContactNumber = cc.Mobile,
                                            Email = cc.Email
                                        }).ToList();

                                var list2 = list.Where(p => !model.searchList.Any(p2 => p2.Id == p.Id));

                                model.searchList.AddRange(list2);
                            }

                            // Search for emails only
                        }
                        else if (query.Contains("@") && !query.Contains(" "))
                        {
                            var list = (from cc in context.Common_Contact
                                        where (cc.Email == query || cc.Email2 == query)
                                        select new ContactDetails
                                        {
                                            Id = cc.Contact_ID,
                                            FirstName = cc.FirstName,
                                            LastName = cc.LastName,
                                            ContactNumber = cc.Mobile,
                                            Email = cc.Email
                                        }).ToList();


                            model.searchList.AddRange(list);

                            // Search for single names
                        }
                        else if (!query.Contains(" "))
                        {
                            var list = (from cc in context.Common_Contact
                                        where (cc.FirstName == query || cc.LastName == query)
                                        select new ContactDetails
                                        {
                                            Id = cc.Contact_ID,
                                            FirstName = cc.FirstName,
                                            LastName = cc.LastName,
                                            ContactNumber = cc.Mobile,
                                            Email = cc.Email
                                        }).ToList();

                            model.searchList.AddRange(list);
                        }

                        return View("Registrations", model);
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            else
            {
                return View("Error", "Shared");
            }
        }

        [HttpPost]
        public ActionResult Register(PurchaseViewModel model)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                if (model.selectedEvent == 0)
                {
                    model.selectedEvent = (from ec in context.Events_Conference
                                           where ec.SortOrder > 0
                                           orderby ec.SortOrder
                                           select ec.Conference_ID).FirstOrDefault();
                }

                if (model.selectedPaymentType > 0)
                {
                    SelfAllocateModel selfallocatedmodel = new SelfAllocateModel();

                    if (model.Comments == null)
                    {
                        model.Comments = String.Empty;
                    }

                    model.RegoTypes = new Dictionary<int, int>();

                    // TODO: FIX THIS!! Data should come from View and require no re-creation
                    // Recreate the model because input from view is not coming through
                    for (int i = 0; i < model.rego_type_list.Count; i++)
                    {
                        model.RegoTypes.Add(model.rego_type_list[i].RegistrationType_ID, model.qty[i]);
                    }

                    // Create cart models
                    var cartModel = new CartModel(model.selectedEvent);

                    cartModel.ContactId = model.selectedID;
                    cartModel.Comments = model.Comments;
                    Session["Cart"] = cartModel;

                    // Populate cart model
                    var regotypes = model.RegoTypes.Where(x => x.Value > 0).ToList();
                    foreach (var pair in regotypes)
                    {
                        cartModel.Cart.Add(new CartItem
                        {
                            ContactId = model.selectedID,
                            reg_id = pair.Key,
                            reg_name = (from ert in context.Events_RegistrationType
                                        where ert.RegistrationType_ID == pair.Key
                                        select ert.RegistrationType).FirstOrDefault(),
                            event_id = cartModel.Event.id,
                            event_name = (from ec in context.Events_Conference
                                          where ec.Conference_ID == cartModel.Event.id
                                          select ec.ConferenceName).FirstOrDefault(),
                            single_price = model.rego_type_list.Where(x => x.RegistrationType_ID == pair.Key).First().RegistrationCost,
                            currency = (from ec in context.Events_Conference
                                        where ec.Conference_ID == model.selectedEvent
                                        join ba in context.Common_BankAccount on ec.BankAccount_ID equals ba.BankAccount_ID
                                        select ba.Currency).FirstOrDefault(),
                            quantity = pair.Value
                        });
                    }

                    if (cartModel.Voucher != null)
                    {
                        var v = cartModel.Voucher.value;
                        var s = cartModel.Cart.Sum(i => i.subtotal);

                        cartModel.Voucher.applied_value = s < v ? s : v;
                    }

                    if (model.selectedPaymentType == 3)
                        return RedirectToAction("Summary", "Payment");

                    // Stripe CC bypass for admin use only
                    if (model.selectedPaymentType == 10000)
                    {
                        model.selectedPaymentType = 3;
                    }

                    if (cartModel.CCPayment == null)
                    {
                        cartModel.CCPayment = new CCPaymentModel();
                    }

                    var is_group_rego = true;


                    if (is_group_rego)
                    {
                        #region Insert Group Payment Log into database
                        var new_group_payment = new Events_GroupPayment();
                        new_group_payment.GroupLeader_ID = cartModel.ContactId;
                        new_group_payment.Conference_ID = model.selectedEvent;
                        new_group_payment.PaymentType_ID = model.selectedPaymentType;
                        new_group_payment.PaymentAmount = cartModel.GrandTotal;
                        new_group_payment.CCNumber = String.Empty;
                        new_group_payment.CCExpiry = String.Empty;
                        new_group_payment.CCName = String.Empty;
                        new_group_payment.CCPhone = String.Empty;
                        new_group_payment.CCManual = false;
                        new_group_payment.CCTransactionRef = String.Empty;
                        new_group_payment.CCRefund = false;
                        new_group_payment.ChequeDrawer = String.Empty;
                        new_group_payment.ChequeBank = String.Empty;
                        new_group_payment.ChequeBranch = String.Empty;
                        new_group_payment.PaypalTransactionRef = String.Empty;
                        new_group_payment.Comment = model.Comments;
                        new_group_payment.PaymentCompletedDate = DateTime.Now;
                        new_group_payment.PaymentCompleted = true;
                        new_group_payment.PaymentBy_ID = cartModel.ContactId;
                        new_group_payment.BankAccount_ID = 4;

                        context.Events_GroupPayment.Add(new_group_payment);

                        cartModel.CCPayment.payment_id = new_group_payment.GroupPayment_ID;
                        cartModel.CCPayment.paymenttype_id = new_group_payment.PaymentType_ID;
                        cartModel.CCPayment.transactionReference = new_group_payment.CCTransactionRef;
                        #endregion
                    }

                    context.SaveChanges();

                    var camps = new List<int> { 119, 120, 121 };

                    // Allocate immediately if event is a camp and only single rego
                    if (camps.Contains(model.selectedEvent) && cartModel.Cart.Count == 1)
                    {
                        #region Create New Registration
                        if (regotypes.Count != 1 || regotypes is null)
                        {
                            // Something went wrong
                            return View("Error", ViewBag.ErrorMessage = "This person bought more than 1 or 0 registration.");
                        }
                        else
                        {
                            var rt_id = cartModel.Cart.First().reg_id;

                            var new_rego = new Events_Registration();
                            new_rego.Contact_ID = cartModel.ContactId;
                            new_rego.GroupLeader_ID = cartModel.ContactId;
                            new_rego.FamilyRegistration = false;
                            new_rego.RegisteredByFriend_ID = null;
                            new_rego.Venue_ID = model.Venue_ID;
                            new_rego.RegistrationType_ID = rt_id;
                            new_rego.RegistrationDiscount = 0;
                            new_rego.Elective_ID = null;
                            new_rego.Accommodation = false;
                            new_rego.Accommodation_ID = null;
                            new_rego.Catering = false;
                            new_rego.CateringNeeds = "";
                            new_rego.LeadershipBreakfast = false;
                            new_rego.LeadershipSummit = false;
                            new_rego.ULG_ID = null;
                            new_rego.ParentGuardian = "";
                            new_rego.ParentGuardianPhone = "";
                            new_rego.AcceptTermsRego = true;
                            new_rego.AcceptTermsParent = false;
                            new_rego.AcceptTermsCreche = false;
                            new_rego.RegistrationDate = DateTime.Now;
                            new_rego.RegisteredBy_ID = CurrentContext.Instance.User.ContactID;
                            new_rego.Attended = false;
                            new_rego.Volunteer = false;
                            new_rego.VolunteerDepartment_ID = null;
                            new_rego.ComboRegistration_ID = null;

                            context.Events_Registration.Add(new_rego);
                            context.SaveChanges();
                        }
                        #endregion
                    }

                    #region Send Email Confirmation

                    var gp = (from egp in context.Events_GroupPayment
                              where egp.Conference_ID == cartModel.Event.id
                                    && egp.GroupLeader_ID == cartModel.ContactId
                                    && (egp.ExpiredIntent != true || egp.ExpiredIntent == null)
                              orderby egp.GroupPayment_ID descending
                              select egp).FirstOrDefault();

                    var email = (from cc in context.Common_Contact
                                 where cc.Contact_ID == cartModel.ContactId
                                 select cc.Email).FirstOrDefault();

                    /*if (cartModel.CCPayment != null)
                    {
                        cartModel.CCPayment.payment_id = gp.GroupPayment_ID;
                        cartModel.CCPayment.paymenttype_id = gp.PaymentType_ID;
                        cartModel.CCPayment.transactionReference = gp.CCTransactionRef;
                    }else
                    {
                        cartModel.CCPayment = new CCPaymentModel();

                        cartModel.CCPayment.payment_id = gp.GroupPayment_ID;
                        cartModel.CCPayment.paymenttype_id = gp.PaymentType_ID;
                        cartModel.CCPayment.transactionReference = gp.CCTransactionRef;
                    }*/

                    if (!String.IsNullOrEmpty(email))
                    {
                        SendEventsEmail(gp.GroupPayment_ID.ToString());
                    }
                    #endregion

                    selfallocatedmodel.Conference_ID = cartModel.Event.id;
                    selfallocatedmodel.Venue_ID = cartModel.Event.venue_id;
                    selfallocatedmodel.Contact_ID = cartModel.ContactId;
                    if (selfallocatedmodel.isAllocatedSameRego)
                    {
                        return View("RegistrationSuccess");
                    }
                    else
                    {
                        return View("AllocateQuickAddTicket", selfallocatedmodel);
                    }

                }

                model.searchList = new List<ContactDetails>();

                model.searchList.Add((from cc in context.Common_Contact
                                      where cc.Contact_ID == model.selectedID
                                      select new ContactDetails {
                                          Id = cc.Contact_ID,
                                          FirstName = cc.FirstName,
                                          LastName = cc.LastName,
                                          ContactNumber = cc.Mobile,
                                          Email = cc.Email
                                      }).FirstOrDefault());

                if (model.selectedEvent > 0)
                {
                    model.RegoTypes = new Dictionary<int, int>();
                    var list = model.rego_type_list;

                    model.qty = new List<int>(new int[list.Count]);

                    foreach (var i in list)
                    {
                        model.RegoTypes.Add(i.RegistrationType_ID, 0);
                    }
                } else
                {
                    model.selectedEvent = (from ec in context.Events_Conference
                                           where ec.SortOrder > 0
                                           orderby ec.SortOrder descending
                                           select ec.Conference_ID).FirstOrDefault();

                    model.RegoTypes = new Dictionary<int, int>();

                    foreach (var i in model.rego_type_list)
                    {
                        model.RegoTypes.Add(i.RegistrationType_ID, 0);
                    }
                }
            }

            return View("Registrations", model);
        }

        //This is newly added function to return ajax call for Quick Add Registrations
        [HttpGet]
        public ActionResult ConferenceRegistrationType(int Conference_ID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var rego_type_list = (from ert in context.Events_RegistrationType
                                      where ert.Conference_ID == Conference_ID
                                      orderby ert.SortOrder
                                      select new RegistrationType
                                      {
                                          cost = ert.RegistrationCost,
                                          name = ert.RegistrationType,
                                          id = ert.RegistrationType_ID
                                      }).ToList();
                var qty = new List<int>(new int[rego_type_list.Count]);
                return Json(new { regoTypeList = rego_type_list, quantity = qty, conferenceID = Conference_ID }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult RegisterNew(PurchaseViewModel model)
        {
            return View(model);
        }

        [HttpPost]
        public ActionResult SubmitNew(PurchaseViewModel model)
        {
            #region Form Validation

            if (model.NewFirstName == null)
            {
                model.NewFirstName = String.Empty;
            }

            if (model.NewLastName == null)
            {
                model.NewLastName = String.Empty;
            }

            if (model.NewGender == null)
            {
                model.NewGender = String.Empty;
            }

            if (model.NewMobile == null)
            {
                model.NewMobile = String.Empty;
            }

            if (model.NewEmail == null)
            {
                model.NewEmail = String.Empty;
            }

            #endregion

            try
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    #region Create new Common Contact

                    var new_contact = new Common_Contact();
                    new_contact.Salutation_ID = 1; // Empty Salutation
                    new_contact.FirstName = model.NewFirstName;
                    new_contact.LastName = model.NewLastName;
                    new_contact.Gender = model.NewGender;
                    new_contact.DateOfBirth = null;
                    new_contact.Address1 = String.Empty;
                    new_contact.Address2 = String.Empty;
                    new_contact.Suburb = String.Empty;
                    new_contact.Postcode = String.Empty;
                    new_contact.State_ID = null;
                    new_contact.StateOther = String.Empty;
                    new_contact.Country_ID = null;
                    new_contact.Phone = String.Empty;
                    new_contact.PhoneWork = String.Empty;
                    new_contact.Fax = String.Empty;
                    new_contact.Mobile = model.NewMobile;

                    // Search if email exists in database
                    var exists = (from cc in context.Common_Contact
                                  where cc.Email == model.NewEmail
                                  select cc).Any();

                    // If exist, put into email2, otherwise email
                    if (exists)
                    {
                        new_contact.Email = String.Empty;
                        new_contact.Email2 = model.NewEmail;
                    } else
                    {
                        new_contact.Email = model.NewEmail;
                        new_contact.Email2 = String.Empty;
                    }

                    new_contact.DoNotIncludeEmail1InMailingList = false;
                    new_contact.DoNotIncludeEmail2InMailingList = false;
                    new_contact.Family_ID = null;
                    new_contact.FamilyMemberType_ID = null;
                    new_contact.Password = String.Empty; // model.Password;
                    new_contact.SecretQuestion_ID = null;
                    new_contact.SecretAnswer = null;
                    new_contact.VolunteerPoliceCheck = String.Empty;
                    new_contact.VolunteerPoliceCheckBit = false;
                    new_contact.VolunteerPoliceCheckDate = null;
                    new_contact.VolunteerWWCC = String.Empty;
                    new_contact.VolunteerWWCCDate = null;
                    new_contact.VolunteerWWCCType_ID = null;
                    new_contact.CreatedBy_ID = CurrentContext.Instance.User.ContactID;
                    new_contact.CreationDate = DateTime.Now;
                    new_contact.ModifiedBy_ID = null;
                    new_contact.ModificationDate = null;
                    new_contact.DeDupeVerified = false;
                    new_contact.GUID = Guid.NewGuid();
                    new_contact.MaritalStatus_ID = null;
                    new_contact.AnotherLanguage_ID = null;
                    new_contact.Nationality_ID = null;
                    new_contact.LastLoginDate = null;

                    context.Common_Contact.Add(new_contact);
                    context.SaveChanges();
                    #endregion

                    model.selectedID = ((from cc in context.Common_Contact
                                         orderby cc.Contact_ID descending
                                         select cc.Contact_ID).FirstOrDefault());

                    model.searchList = new List<ContactDetails>();

                    model.searchList.Add((from cc in context.Common_Contact
                                          where cc.Contact_ID == model.selectedID
                                          select new ContactDetails
                                          {
                                              Id = cc.Contact_ID,
                                              FirstName = cc.FirstName,
                                              LastName = cc.LastName,
                                              ContactNumber = cc.Mobile,
                                              Email = cc.Email
                                          }).FirstOrDefault());
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return View("Registrations", model);
        }

        public ActionResult AllocateQuickAddTicket(SelfAllocateModel model)
        {
            return View("AllocateQuickAddTicket", model);
        }

        public ActionResult SelfAllocate(SelfAllocateModel model)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var IsUser = (from cud in context.Common_UserDetail
                              where cud.Contact_ID == model.Contact_ID
                              select cud).FirstOrDefault() != null ? true : false;

                if (!IsUser)
                {
                    var new_user = new Common_UserDetail();
                    new_user.Contact_ID = model.Contact_ID;
                    new_user.LastPasswordChangeDate = null;
                    new_user.InvalidPasswordAttempts = 0;
                    new_user.PasswordResetRequired = false;
                    new_user.Inactive = false;

                    context.SaveChanges();
                }

                var new_registration = new Events_Registration();
                new_registration.Contact_ID = model.Contact_ID;
                new_registration.GroupLeader_ID = model.Contact_ID;
                new_registration.FamilyRegistration = false;
                new_registration.RegisteredByFriend_ID = null;
                new_registration.Venue_ID = model.Venue_ID;
                new_registration.RegistrationType_ID = model.RegistrationType_ID;
                new_registration.RegistrationDiscount = (decimal)0.00;
                new_registration.Elective_ID = null;
                new_registration.Accommodation = false;
                new_registration.Accommodation_ID = null;
                new_registration.Catering = false;
                new_registration.CateringNeeds = String.Empty;
                new_registration.LeadershipBreakfast = false;
                new_registration.LeadershipSummit = false;
                new_registration.ULG_ID = null;
                new_registration.ParentGuardian = String.Empty;
                new_registration.ParentGuardianPhone = String.Empty;
                new_registration.AcceptTermsRego = true;
                new_registration.AcceptTermsParent = false;
                new_registration.AcceptTermsCreche = false;
                new_registration.RegistrationDate = DateTime.Now;
                new_registration.RegisteredBy_ID = CurrentContext.Instance.User.ContactID;
                new_registration.Attended = model.Attended;
                new_registration.Volunteer = false;
                new_registration.VolunteerDepartment_ID = null;
                new_registration.ComboRegistration_ID = null;

                context.Events_Registration.Add(new_registration);
                context.SaveChanges();

                //Send allocation email
                var contact = (from cc in context.Common_Contact
                               where cc.Contact_ID == model.Contact_ID
                               select cc).FirstOrDefault();

                var conference = (from ev in context.Events_Venue
                                  join ec in context.Events_Conference
                                  on ev.Conference_ID equals ec.Conference_ID
                                  where ev.Venue_ID == model.Venue_ID
                                  select new Planetshakers.Events.Models.Event
                                  {
                                      id = ec.Conference_ID,
                                      venue_id = ev.Venue_ID,
                                      name = ec.ConferenceName,
                                      date = ec.ConferenceDate,
                                      location = ec.ConferenceLocation,
                                      venueLocation = ev.VenueName,
                                      shortname = ec.ConferenceNameShort
                                  }).FirstOrDefault();

                var confirmationBarcode = new ConfirmationBarcode()
                {
                    Subject = "Quick Check In - " + conference.name,
                    EventName = conference.name,
                    EventDate = conference.date.ToString(),
                    EventVenue = conference.location,
                    VenueLocation = conference.venueLocation,
                    Email = (String.IsNullOrEmpty(contact.Email)) ? contact.Email2 : contact.Email,
                    RegistrationNumber = new_registration.Registration_ID.ToString()
                };


                if (!String.IsNullOrEmpty(confirmationBarcode.Email))
                {
                    //new MailController().ConfirmationBarcode(barcode_model);
                    new MailController().ConfirmationBarcodeEmail(confirmationBarcode);
                    CreateEventLog(contact.Contact_ID, false, "Update", "Sent Confirmation Barcode Email");
                }
            }
            return RedirectToAction("RegistrationSuccess");
        }

        public ActionResult RegistrationSuccess()
        {

            return View("RegistrationSuccess");
        }

        #endregion

        #region Conference Allocations

        public ActionResult Allocations()
        {
            var model = new AllocationViewModel();

            return View(model);
        }

        [HttpPost]
        public ActionResult SearchContactAllocation(AllocationViewModel model, string query)
        {
            if (query != null)
            {
                int i;

                if (query.Length < 3)
                {
                    ViewBag.Message = "Query is too short. Please provide the full name or full email.";
                    return View("Error");
                }

                if (int.TryParse(query, out i))
                {
                    ViewBag.Message = "Invalid query. Please search by full name or full email.";
                    return View("Error");
                }

                try
                {
                    //var model = new AllocationViewModel();

                    using (PlanetshakersEntities context = new PlanetshakersEntities())
                    {
                        model.searchList = new List<ContactDetails>();
                        // Search for names with spaces
                        if (query.Contains(" "))
                        {
                            var substrings = query.ToLower().Split();

                            var list = (from cc in context.Common_Contact
                                        where (cc.FirstName + " " + cc.LastName == query || cc.LastName + " " + cc.FirstName == query)
                                        select new ContactDetails
                                        {
                                            Id = cc.Contact_ID,
                                            FirstName = cc.FirstName,
                                            LastName = cc.LastName,
                                            ContactNumber = cc.Mobile,
                                            Email = cc.Email
                                        }).ToList();

                            model.searchList.AddRange(list);

                            foreach (var s in substrings)
                            {
                                list = (from cc in context.Common_Contact
                                        where (cc.FirstName.StartsWith(s.ToLower()) || cc.LastName.StartsWith(s.ToLower()))
                                        select new ContactDetails
                                        {
                                            Id = cc.Contact_ID,
                                            FirstName = cc.FirstName,
                                            LastName = cc.LastName,
                                            ContactNumber = cc.Mobile,
                                            Email = cc.Email
                                        }).ToList();

                                var list2 = list.Where(p => !model.searchList.Any(p2 => p2.Id == p.Id));

                                model.searchList.AddRange(list2);
                            }

                            // Search for emails only
                        }
                        else if (query.Contains("@") && !query.Contains(" "))
                        {
                            var list = (from cc in context.Common_Contact
                                        where (cc.Email == query || cc.Email2 == query)
                                        select new ContactDetails
                                        {
                                            Id = cc.Contact_ID,
                                            FirstName = cc.FirstName,
                                            LastName = cc.LastName,
                                            ContactNumber = cc.Mobile,
                                            Email = cc.Email
                                        }).ToList();


                            model.searchList.AddRange(list);

                            // Search for single names
                        }
                        else if (!query.Contains(" "))
                        {
                            var list = (from cc in context.Common_Contact
                                        where (cc.FirstName == query || cc.LastName == query)
                                        select new ContactDetails
                                        {
                                            Id = cc.Contact_ID,
                                            FirstName = cc.FirstName,
                                            LastName = cc.LastName,
                                            ContactNumber = cc.Mobile,
                                            Email = cc.Email
                                        }).ToList();

                            model.searchList.AddRange(list);
                        }


                        if (model.SelectedOption == "Existing")
                            return View("ExistingAllocation", model);

                        return View("Allocations", model);
                    }

                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            else
            {
                return View("Error", "Shared");
            }
        }

        [HttpPost]
        public ActionResult Allocate(AllocationViewModel model)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                if (model.SelectedOption != null && model.RegistrationType_ID > 0)
                {
                    if (model.SelectedOption == "Existing")
                        return View("ExistingAllocation", model);
                    else if (model.SelectedOption == "New")
                        return View("NewAllocation", model);
                }

                if (model.Contact_ID > 0)
                {
                    model.searchList = (from cc in context.Common_Contact
                                        where cc.Contact_ID == model.Contact_ID
                                        select new ContactDetails {
                                            Id = cc.Contact_ID,
                                            FirstName = cc.FirstName,
                                            LastName = cc.LastName,
                                            ContactNumber = cc.Mobile,
                                            Email = cc.Email
                                        }).ToList();
                }

                if (model.Venue_ID <= 0)
                {
                    model.Venue_ID = (from ec in context.Events_Conference
                                      join ev in context.Events_Venue
                                      on ec.Conference_ID equals ev.Conference_ID
                                      where ec.SortOrder > 0
                                      orderby ec.SortOrder
                                      select ev.Venue_ID).FirstOrDefault();
                }
            }

            return View("Allocations", model);
        }

        [HttpGet]
        public ActionResult GetUnallocatedList(int Venue_ID, int Contact_ID)
        {
            AllocationViewModel model = new AllocationViewModel();
            model.Venue_ID = Venue_ID;
            model.Contact_ID = Contact_ID;
            var availableRegoList = model.unallocatedRegoList.Where(x => x.Unallocated >= 1).ToList();
            return Json(new { unallocatedList = model.unallocatedRegoList, allocatedList = model.AllocatedRegoList, availableRegistrationList = availableRegoList },
                JsonRequestBehavior.AllowGet);
        }

        #region Existing Allocation

        public ActionResult ExistingAllcoation(AllocationViewModel model)
        {
            model.SelectedOption = "Existing";

            return View(model);
        }

        [HttpPost]
        public ActionResult AllocateExisting(AllocationViewModel model)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var contact = (from cc in context.Common_Contact
                               where cc.Contact_ID == model.Allocation_ID
                               select cc).FirstOrDefault();

                //For Email model use
                var conference = (from ev in context.Events_Venue
                                  join ec in context.Events_Conference
                                  on ev.Conference_ID equals ec.Conference_ID
                                  where ev.Venue_ID == model.Venue_ID
                                  select new Planetshakers.Events.Models.Event
                                  {
                                      id = ec.Conference_ID,
                                      venue_id = ev.Venue_ID,
                                      name = ec.ConferenceName,
                                      date = ec.ConferenceDate,
                                      location = ec.ConferenceLocation,
                                      venueLocation = ev.VenueName,
                                      shortname = ec.ConferenceNameShort
                                  }).FirstOrDefault();

                
                    var new_contact_event = new Common_ContactExternalEvents();
                    new_contact_event.EmergencyContactName = "";
                    new_contact_event.EmergencyContactPhone = "";
                    new_contact_event.EmergencyContactRelationship = "";
                    new_contact_event.MedicalInformation = "";
                    new_contact_event.MedicalAllergies = "";
                    new_contact_event.AccessibilityInformation = "";
                    new_contact_event.EventComments = "";

                    context.Common_ContactExternalEvents.Add(new_contact_event);

                    context.SaveChanges();

                var IsUser = (from cud in context.Common_UserDetail
                              where cud.Contact_ID == model.Allocation_ID
                              select cud).FirstOrDefault() != null ? true : false;

                if (!IsUser)
                {
                    var new_user = new Common_UserDetail();
                    new_user.Contact_ID = model.Allocation_ID;
                    new_user.LastPasswordChangeDate = null;
                    new_user.InvalidPasswordAttempts = 0;
                    new_user.PasswordResetRequired = false;
                    new_user.Inactive = false;

                    context.SaveChanges();
                }

                var new_registration = new Events_Registration();
                new_registration.Contact_ID = model.Allocation_ID;
                new_registration.GroupLeader_ID = model.Contact_ID;
                new_registration.FamilyRegistration = false;
                new_registration.RegisteredByFriend_ID = null;
                new_registration.Venue_ID = model.Venue_ID;
                new_registration.RegistrationType_ID = model.RegistrationType_ID;
                new_registration.RegistrationDiscount = (decimal)0.00;
                new_registration.Elective_ID = null;
                new_registration.Accommodation = false;
                new_registration.Accommodation_ID = null;
                new_registration.Catering = false;
                new_registration.CateringNeeds = String.Empty;
                new_registration.LeadershipBreakfast = false;
                new_registration.LeadershipSummit = false;
                new_registration.ULG_ID = null;
                new_registration.ParentGuardian = String.Empty;
                new_registration.ParentGuardianPhone = String.Empty;
                new_registration.AcceptTermsRego = true;
                new_registration.AcceptTermsParent = false;
                new_registration.AcceptTermsCreche = false;
                new_registration.RegistrationDate = DateTime.Now;
                new_registration.RegisteredBy_ID = CurrentContext.Instance.User.ContactID;
                new_registration.Attended = model.NewAttended;
                new_registration.Volunteer = false;
                new_registration.VolunteerDepartment_ID = null;
                new_registration.ComboRegistration_ID = null;

                context.Events_Registration.Add(new_registration);
                context.SaveChanges();

                model.SuccessMessage = "Allocation to existing person completed!";
                model.Conference_ID = (from ev in context.Events_Venue
                                       where ev.Venue_ID == model.Venue_ID
                                       select ev.Conference_ID).FirstOrDefault();

                //Send allocation email
                var confirmationBarcode = new ConfirmationBarcode()
                {
                    Subject = "Quick Check In - " + conference.name,
                    EventName = conference.name,
                    EventDate = conference.date.ToString(),
                    EventVenue = conference.location,
                    VenueLocation = conference.venueLocation,
                    Email = (String.IsNullOrEmpty(contact.Email)) ? contact.Email2 : contact.Email,
                    RegistrationNumber = new_registration.Registration_ID.ToString()
                };

                if (!String.IsNullOrEmpty(confirmationBarcode.Email))
                {
                    new MailController().ConfirmationBarcodeEmail(confirmationBarcode);
                    CreateEventLog(contact.Contact_ID, false, "Update", "Sent Confirmation Barcode Email");
                }
            }

            model.NewFirstName = "";
            model.NewLastName = "";
            model.NewMobile = "";
            model.NewEmail = "";
            model.NewAttended = false;

            return View("ExistingAllocation", model);
        }

        [HttpGet]
        public ActionResult Registered(int Allocation_ID, int Venue_ID)
        {
            bool registered = false;
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                registered = (from er in context.Events_Registration
                              where er.Contact_ID == Allocation_ID && er.Venue_ID == Venue_ID
                              select er).FirstOrDefault() != null ? true : false;
                return Json(registered, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region New Allocation

        public ActionResult NewAllocation(AllocationViewModel model)
        {
            return View(model);
        }

        [HttpPost]
        public ActionResult AllocateNew(AllocationViewModel model)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                #region Allocate Validation
                if (model.NewFirstName == null)
                {
                    model.NewFirstName = String.Empty;
                }

                if (model.NewLastName == null)
                {
                    model.NewLastName = String.Empty;
                }

                if (model.NewMobile == null)
                {
                    model.NewMobile = String.Empty;
                }

                if (model.NewEmail == null)
                {
                    model.NewEmail = String.Empty;
                }
                #endregion

                //For Email model use
                var conference = (from ev in context.Events_Venue
                                  join ec in context.Events_Conference
                                  on ev.Conference_ID equals ec.Conference_ID
                                  where ev.Venue_ID == model.Venue_ID
                                  select new Planetshakers.Events.Models.Event
                                  {
                                      id = ec.Conference_ID,
                                      venue_id = ev.Venue_ID,
                                      name = ec.ConferenceName,
                                      date = ec.ConferenceDate,
                                      location = ec.ConferenceLocation,
                                      venueLocation = ev.VenueName,
                                      shortname = ec.ConferenceNameShort
                                  }).FirstOrDefault();

                // create new common_contactExternalEvents
                var new_contact_event = new Common_ContactExternalEvents();
                new_contact_event.EmergencyContactName = "";
                new_contact_event.EmergencyContactPhone = "";
                new_contact_event.EmergencyContactRelationship = "";
                new_contact_event.MedicalInformation = "";
                new_contact_event.MedicalAllergies = "";
                new_contact_event.AccessibilityInformation = "";
                new_contact_event.EventComments = "";


                // create new contact object
                var new_contact = new Common_Contact();
                new_contact.Salutation_ID = 1; // Empty Salutation
                new_contact.FirstName = model.NewFirstName;
                new_contact.LastName = model.NewLastName;
                new_contact.Gender = String.Empty;
                new_contact.DateOfBirth = null;
                new_contact.Address1 = String.Empty;
                new_contact.Address2 = String.Empty;
                new_contact.Suburb = String.Empty;
                new_contact.Postcode = String.Empty;
                new_contact.State_ID = null;
                new_contact.StateOther = String.Empty;
                new_contact.Country_ID = null;
                new_contact.Phone = String.Empty;
                new_contact.PhoneWork = String.Empty;
                new_contact.Fax = String.Empty;
                new_contact.Mobile = model.NewMobile;

                // Search if email exists in database
                var exists = (from cc in context.Common_Contact
                              where cc.Email == model.NewEmail
                              select cc).Any();

                // If exist, put into email2, otherwise email
                if (exists)
                {
                    new_contact.Email = String.Empty;
                    new_contact.Email2 = model.NewEmail;
                }
                else
                {
                    new_contact.Email = model.NewEmail;
                    new_contact.Email2 = String.Empty;
                }

                new_contact.DoNotIncludeEmail1InMailingList = false;
                new_contact.DoNotIncludeEmail2InMailingList = false;
                new_contact.Family_ID = null;
                new_contact.FamilyMemberType_ID = null;
                new_contact.Password = String.Empty; // model.Password;
                new_contact.SecretQuestion_ID = null;
                new_contact.SecretAnswer = null;
                new_contact.VolunteerPoliceCheck = String.Empty;
                new_contact.VolunteerPoliceCheckBit = false;
                new_contact.VolunteerPoliceCheckDate = null;
                new_contact.VolunteerWWCC = String.Empty;
                new_contact.VolunteerWWCCDate = null;
                new_contact.VolunteerWWCCType_ID = null;
                new_contact.CreatedBy_ID = CurrentContext.Instance.User.ContactID;
                new_contact.CreationDate = DateTime.Now;
                new_contact.ModifiedBy_ID = null;
                new_contact.ModificationDate = null;
                new_contact.DeDupeVerified = false;
                new_contact.GUID = Guid.NewGuid();
                new_contact.MaritalStatus_ID = null;
                new_contact.AnotherLanguage_ID = null;
                new_contact.Nationality_ID = null;
                new_contact.LastLoginDate = null;

                context.Common_Contact.Add(new_contact);
                context.SaveChanges();

                model.Allocation_ID = (from cc in context.Common_Contact
                                       orderby cc.Contact_ID descending
                                       select cc.Contact_ID).Take(1).FirstOrDefault();

                var new_registration = new Events_Registration();
                new_registration.Contact_ID = model.Allocation_ID;
                new_registration.GroupLeader_ID = model.Contact_ID;
                new_registration.FamilyRegistration = false;
                new_registration.RegisteredByFriend_ID = null;
                new_registration.Venue_ID = model.Venue_ID;
                new_registration.RegistrationType_ID = model.RegistrationType_ID;
                new_registration.RegistrationDiscount = (decimal)0.00;
                new_registration.Elective_ID = null;
                new_registration.Accommodation = false;
                new_registration.Accommodation_ID = null;
                new_registration.Catering = false;
                new_registration.CateringNeeds = String.Empty;
                new_registration.LeadershipBreakfast = false;
                new_registration.LeadershipSummit = false;
                new_registration.ULG_ID = null;
                new_registration.ParentGuardian = String.Empty;
                new_registration.ParentGuardianPhone = String.Empty;
                new_registration.AcceptTermsRego = true;
                new_registration.AcceptTermsParent = false;
                new_registration.AcceptTermsCreche = false;
                new_registration.RegistrationDate = DateTime.Now;
                new_registration.RegisteredBy_ID = CurrentContext.Instance.User.ContactID;
                new_registration.Attended = model.NewAttended;
                new_registration.Volunteer = false;
                new_registration.VolunteerDepartment_ID = null;
                new_registration.ComboRegistration_ID = null;

                context.Events_Registration.Add(new_registration);
                context.SaveChanges();

                model.SuccessMessage = "Allocation to new person completed!";
                model.Conference_ID = (from ev in context.Events_Venue
                                       where ev.Venue_ID == ev.Venue_ID
                                       select ev.Conference_ID).FirstOrDefault();

                //Send allocation email
                var contact = (from cc in context.Common_Contact
                               where cc.Contact_ID == model.Allocation_ID
                               select cc).FirstOrDefault();

                var confirmationBarcode = new ConfirmationBarcode()
                {
                    Subject = "Quick Check In - " + conference.name,
                    EventName = conference.name,
                    EventDate = conference.date.ToString(),
                    EventVenue = conference.location,
                    VenueLocation = conference.venueLocation,
                    Email = (String.IsNullOrEmpty(contact.Email)) ? contact.Email2 : contact.Email,
                    RegistrationNumber = new_registration.Registration_ID.ToString()
                };

                if (!String.IsNullOrEmpty(confirmationBarcode.Email))
                {
                    new MailController().ConfirmationBarcodeEmail(confirmationBarcode);
                    CreateEventLog(contact.Contact_ID, false, "Update", "Sent Confirmation Barcode Email");
                }
            }

            model.NewFirstName = String.Empty;
            model.NewLastName = String.Empty;
            model.NewMobile = String.Empty;
            model.NewEmail = String.Empty;

            return View("NewAllocation", model);
        }

        #endregion

        public ActionResult Registered()
        {
            return View();
        }

        #region Credit Card Payment

        /*[HttpPost]
        //[PSAuthorize]
        [ValidateAntiForgeryToken]
        public ActionResult CCPayment(string stripeEmail, string stripeToken)
        {
            // Initiate Logger
            ILog pxlogger = LogManager.GetLogger("GeneralLogger");

            try
            {
                // Retrieve some cart data from session
                var cartModel = Session["Cart"] as CartModel;

                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    // Retrieve Bank Account information to transfer funds into
                    var bankAccounts = (from ba in context.Common_BankAccount
                                        where ba.BankAccount_ID == 4
                                        select ba).ToList();

                    if (!bankAccounts.Any())
                    {
                        // Something went wrong
                    }

                    var bankAcc = bankAccounts.First();

                    // Setup variables to retrieve responses from the bank
                    string eventName = "";
                    foreach (CartItem item in cartModel.Cart)
                    {
                        for (var i = 0; i < item.quantity; i++)
                        {
                            eventName = Convert.ToString(item.event_name);
                        }
                    }

                    string bankClientID = bankAcc.ClientID;
                    string bankCertName = bankAcc.CertificateName;
                    string bankCertPassPhrase = bankAcc.CertificatePassphrase;
                    string transactionResponse = String.Empty;
                    string transactionReference = String.Empty;

                    bool paymentSuccess = false;

                    // Stripe code
                    var customers = new StripeCustomerService();
                    var charges = new StripeChargeService();

                    //StripeConfiguration.SetApiKey(ConfigurationManager.AppSettings["stripeSecretKey"]);

                    // Live Key
                    StripeConfiguration.SetApiKey("sk_live_QuHIgYWT7lQeqCx1xn7AGZlX");

                    // Test Key
                    //StripeConfiguration.SetApiKey("sk_test_eUoxi5QmlGc8UiWRpohWYcQ6");

                    var customer = customers.Create(new StripeCustomerCreateOptions
                    {
                        Email = stripeEmail,
                        SourceToken = stripeToken
                    });



                    foreach (CartItem item in cartModel.Cart)
                    {
                        var amount = Convert.ToInt32(item.single_price) * 100;
                        var charge = charges.Create(new StripeChargeCreateOptions
                        {
                            Amount = amount,       // //amount must be 50 cents
                            Description = "Course Charge for " + item.event_name.ToString(),
                            Currency = "AUD",
                            CustomerId = customer.Id
                        });
                        if (charge.Status == "succeeded")
                        {
                            paymentSuccess = true;
                            string criteria = charge.StripeResponse.ResponseJson.ToString();
                            string category = "PaywithCard Click";
                            //Log to Common_Accesslog table
                            Business.MWebMain.LogAction(ref category, ref criteria, CurrentContext.Instance.User.ContactID.ToString());
                        }

                        transactionReference = charge.Id.ToString();
                    }

                    // Add database entry
                    if (paymentSuccess)
                    {
                        pxlogger.Info("Payment success.");
                        // At this point, transaction is successful.
                        // Create all the required rows for registrations and payments

                        // Retrieve logged in contact details
                        var contact = getContactDetails();

                        pxlogger.Info("Preparing confirmation email model.");
                        var email_model = new RegistrationConfirmationEmailModel()
                        {
                            Email = contact.Email,
                            ContactId = cartModel.ContactId,
                            Event = cartModel.Event,
                            Items = new List<RegistrationConfirmationItem>()
                        };

                        // create an entry for the registration
                        pxlogger.Info("Creating entry for registration.");
                        foreach (CartItem item in cartModel.Cart)
                        {

                            pxlogger.Debug("Creating entries for " + item.event_name + " " + item.reg_name + " | Quantity :" + item.quantity + " | Contact Id: " + cartModel.ContactId);
                            for (var i = 0; i < item.quantity; i++)
                            {
                                pxlogger.Debug("Adding items detail to email.");
                                email_model.Items.Add(new RegistrationConfirmationItem
                                {
                                    reference_number = transactionReference,
                                    reg_name = item.event_name,
                                    price = item.single_price
                                });
                            }
                        }


                        //Save enrolledcourse details to database
                        WriteCourseDetails();

                        pxlogger.Info("Inserting payment details to database.");
                        context.sp_Church_CourseContactPaymentInsert(
                            contact_ID: cartModel.ContactId,
                            courseInstance_ID: cartModel.Event.id,
                            paymentType_ID: 3, // credit card payment
                            paymentAmount: cartModel.GrandTotal,
                            cCNumber: "",
                            cCExpiry: "",
                            cCName: "",
                            cCPhone: "",
                            cCManual: false,
                            cCTransactionRef: transactionReference,
                            cCRefund: false,
                            chequeDrawer: "",
                            chequeBank: "",
                            chequeBranch: "",
                            paypalTransactionRef: "",
                            comment: "",
                            paymentDate: DateTime.Now,
                            paymentBy_ID: CurrentContext.Instance.User.ContactID,
                            bankAccount_ID: 3,
                            user_ID: 101);
                        pxlogger.Info("Saving all the changes.");
                        context.SaveChanges();

                        // Prepare to send 
                        pxlogger.Info("Sending email.");
                        new MailController().RegistrationConfirmation(email_model).Deliver();

                        //Create pdf and send to coursepayments@planetshakers.com
                        pxlogger.Info("Creating PDF for payment confirmation.");
                        CreatePDFforPayment(cartModel, transactionReference);

                        // Clear the Cart
                        pxlogger.Info("Emptying cart.");
                        Session["Cart"] = null;

                        // Force single registrations to reload
                        Session["registrations_single"] = null;
                        TempData["recently_registered"] = true;
                        pxlogger.Info("Reloading page.");
                        pxlogger.Info("------------------------------------------------------------------------------------------------------");

                        // Update session.
                        Session["Cart"] = cartModel;
                        Session["success"] = "Payment and course enrolment is successful.";

                        return RedirectToAction("CourseEnrollment", "Account"); //, new { id = cartModel.Event.id });
                    }
                    else
                    {
                        Session["success"] = "";
                        throw new Exception("An error occured while processing the payment. "
                                            + transactionResponse);

                    }
                }
            }
            catch (Exception ex)
            {
                var cartModel = Session["Cart"] as CartModel;
                if (cartModel != null)
                {
                    //cartModel.CCPayment = model;
                    //Session["Cart"] = cartModel;
                }
                else
                {
                    pxlogger.Error(ex.Message);
                }

                return RedirectToAction("Summary", "Account", new { error = ex.Message });
            }
        }*/

        #endregion

        #endregion

        #region Send Bulk Email
        public ActionResult SendBulkConfirmationEmail()
        {
            var model = new BulkEmailModel();

            return View(model);
        }

        public JsonResult SendConfirmationEmail(int venue_ID, int? contact_ID)
        {
            var list = new List<ConfirmationBarcode>();

            var obj = new { Message = "Successfully sent" };

            try
            {
                using (PlanetshakersEntities entity = new PlanetshakersEntities())
                {
                    var conf = (from ev in entity.Events_Venue
                                join ec in entity.Events_Conference
                                on ev.Conference_ID equals ec.Conference_ID
                                where ev.Venue_ID == venue_ID
                                select new
                                {
                                    conf_id = ec.Conference_ID,
                                    name = ec.ConferenceName,
                                    location = ec.ConferenceLocation,
                                    venueLocation = ev.VenueLocation,
                                    date = ec.ConferenceDate
                                }).FirstOrDefault();

                    if (contact_ID != null)
                    {
                        list = (from er in entity.Events_Registration
                                join cc in entity.Common_Contact
                                on er.Contact_ID equals cc.Contact_ID
                                where er.Venue_ID == venue_ID && er.Attended == false && er.Contact_ID == contact_ID
                                select new ConfirmationBarcode
                                {
                                    Contact_ID = cc.Contact_ID,
                                    Subject = "Quick Check In - " + conf.name,
                                    EventName = conf.name,
                                    EventDate = conf.date.ToString(),
                                    EventVenue = conf.location,
                                    VenueLocation = conf.venueLocation,
                                    Email = (String.IsNullOrEmpty(cc.Email)) ? cc.Email2 : cc.Email,
                                    RegistrationNumber = er.Registration_ID.ToString()
                                }).ToList();

                    } else
                    {
                        list = (from er in entity.Events_Registration
                                join cc in entity.Common_Contact
                                on er.Contact_ID equals cc.Contact_ID
                                where er.Venue_ID == venue_ID && er.Attended == false
                                select new ConfirmationBarcode
                                {
                                    Contact_ID = cc.Contact_ID,
                                    Subject = "Quick Check In - " + conf.name,
                                    EventName = conf.name,
                                    EventDate = conf.date.ToString(),
                                    EventVenue = conf.location,
                                    VenueLocation = conf.venueLocation,
                                    Email = (String.IsNullOrEmpty(cc.Email)) ? cc.Email2 : cc.Email,
                                    RegistrationNumber = er.Registration_ID.ToString()
                                }).ToList();
                    }

                    list = list.Where(x => x.Email != "").ToList();

                    foreach (var i in list)
                    {
                        if (!String.IsNullOrEmpty(i.Email))
                        {
                            new MailController().ConfirmationBarcodeEmail(i);
                            CreateEventLog(i.Contact_ID, false, "Update", "Sent Confirmation Barcode Email");
                        }

                    }
                }

            } catch (Exception ex)
            {
                throw ex;
            }

            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Send Tickets

        public void getTicket(int venue_id, int? contact_id)
        {
            var model = new TicketModel();

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                if (contact_id != null)
                {
                    var leaders = (from er in context.Events_Registration
                                   where er.Venue_ID == venue_id && er.GroupLeader_ID == contact_id
                                   select er.GroupLeader_ID).Distinct().ToList();

                    foreach (var id in leaders)
                    {
                        var ticket_list = (from er in context.Events_Registration
                                           where er.GroupLeader_ID == id && er.Venue_ID == venue_id
                                           select new Ticket
                                           {
                                               reg_barcode = er.Registration_ID
                                           }).ToList();

                        var contact = (from cc in context.Common_Contact
                                       where cc.Contact_ID == id
                                       select cc).FirstOrDefault();

                        model.ID = contact.Contact_ID;
                        model.FirstName = contact.FirstName;
                        model.Email = contact.Email != null ? contact.Email : contact.Email2;
                        model.Tickets = ticket_list;
                        SendTicket(model);
                    }
                } else
                {
                    var leaders = (from er in context.Events_Registration
                                   where er.Venue_ID == venue_id && er.GroupLeader_ID > 16810 && er.GroupLeader_ID <= 16861
                                   select er.GroupLeader_ID).Distinct().ToList();

                    foreach (var id in leaders)
                    {
                        var ticket_list = (from er in context.Events_Registration
                                           where er.GroupLeader_ID == id && er.Venue_ID == venue_id
                                           select new Ticket
                                           {
                                               reg_barcode = er.Registration_ID
                                           }).ToList();

                        var contact = (from cc in context.Common_Contact
                                       where cc.Contact_ID == id
                                       select cc).FirstOrDefault();

                        model.ID = contact.Contact_ID;
                        model.FirstName = contact.FirstName;
                        model.Email = contact.Email != null ? contact.Email : contact.Email2;
                        model.Tickets = ticket_list;

                        SendTicket(model);
                    }
                }

            }
        }

        public void SendTicket(TicketModel model)
        {
            try
            {
                MemoryStream stream = new MemoryStream();

                HtmlToPdf converter = new HtmlToPdf();
                PdfDocument doc = new PdfDocument();

                var html = getEmailBody();

                foreach (var ticket in model.Tickets)
                {
                    string html_var = html.Replace("[rego_barcode]", ticket.reg_barcode.ToString());

                    PdfDocument page = converter.ConvertHtmlString(html_var);

                    doc.Append(page);
                }

                //var guid = Guid.NewGuid().ToString();
                var fileName = model.ID + ".pdf";

                var filePath = Path.Combine(HttpContext.Server.MapPath("~"), "Temp", fileName);

                var docBytes = doc.Save().ToString();

                //doc.Save(filePath);


                /*Response.Clear();
                Response.Buffer = true;
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.AddHeader("Content-Disposition", "attachment; filename=ticket.pdf");
                Response.ContentType = "application/pdf";
                Response.TransmitFile(Server.MapPath("~/Temp/" + fileName));
                //Response.BinaryWrite(docBytes);
                Response.Flush();
                Response.End();*/

                //Response.OutputStream.Write(docBytes, 0, docBytes.Length);
                //Response.Flush();

                //SGConcert2020 dynamicTemplateData = new SGConcert2020 { FirstName = model.FirstName };
                //_ = new MailController().SendMailViaPsApi(model.Email, "d-4bdbc2d5a470456f930efa312dc5e753", dynamicTemplateData, docBytes, fileName);

                //return File(fileName)
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string getEmailBody()
        {
            string html = string.Empty;
            string path = Path.Combine(HttpRuntime.AppDomainAppPath, "Views/Mail/Ticket.html.cshtml");

            using (StreamReader reader = new StreamReader(path))
            {
                html = reader.ReadToEnd();
            }

            return html;
        }
        #endregion

        #region Child Enrolment Form
        //============================================================Child Enrolment Form=====================
        public ActionResult ChildEnrolmentForm()
        {
            ChildEnrolmentFormModel model = new ChildEnrolmentFormModel();
            return View("ChildEnrolmentForm", model);
        }

        public ActionResult GenerateChildEnrolmentForm(ChildEnrolmentFormModel model)
        {
            model.generateResult = true;
            return View("ChildEnrolmentForm", model);
        }
        #endregion

        #region Sponsorship

        public ActionResult SponsorshipIndex()
        {
            var model = new SponsorshipManagementModel();

            return View("SponsorshipIndex", model);
        }

        public ActionResult SponsorshipManagement(int Conference_ID)
        {
            var model = new SponsorshipManagementModel();
            model.Conference_ID = Conference_ID;
            var totalBreakdownResult = _repository.SponsorshipCampusMinistryBreakdown(model.Conference_ID);
            model.CampusBreakdown = totalBreakdownResult.Item1;
            model.MinistryBreakdown = totalBreakdownResult.Item2;
            model.PaymentTypeBreakdown = _repository.SponsorshipPaymentTypeBreakdown(model.Conference_ID);

            return View("SponsorshipManagement", model);
        }

        public ActionResult EditSponsorship(int VariablePayment_ID, int Conference_ID)
        {
            var model = new SponsorshipManagementModel();
            model.Conference_ID = Conference_ID;
            model.SelectedSponsorship = model.SponsorshipList.Find(x => x.VariablePaymentID == VariablePayment_ID);
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var refunded = false;
                var intentNotFound = false;
                var RemovedAllocation = false;
                var bank_account = new Common_BankAccount();
                var ba_id = (from p in context.Events_VariablePayment
                             where p.VariablePayment_ID == VariablePayment_ID
                             select p.BankAccount_ID).FirstOrDefault();
                var transactionRef = model.SelectedSponsorship.CCTransactionRef;
                if (ba_id > 0)
                {
                    bank_account = (from ba in context.Common_BankAccount
                                    where ba.BankAccount_ID == ba_id
                                    select ba).FirstOrDefault();
                }

                //check stripe for refund
                StripeConfiguration.ApiKey = bank_account.SecretKey;
                if(transactionRef != null)
                {
                    if (transactionRef != "" && !transactionRef.Contains("ref:"))
                    {
                        var paymentservice = new PaymentIntentService();
                        var chargeservice = new ChargeService();
                        var refundservice = new RefundService();
                        if (transactionRef.Contains("ch_"))
                        {
                            Charge charge = chargeservice.Get(transactionRef);
                            if (charge.Refunds.Data.Count() != 0 && charge.Refunded)
                            {
                                Refund refund = refundservice.Get(charge.Refunds.Data[0].Id);
                                if (refund.Status == "succeeded")
                                {
                                    refunded = true;
                                }
                            }
                        }
                        else if (transactionRef.Contains("pi_"))
                        {
                            PaymentIntent paymentIntent = paymentservice.Get(transactionRef);
                            if (paymentIntent.Charges.Data.Count() != 0)
                            {
                                Charge charge = chargeservice.Get(paymentIntent.Charges.Data[0].Id);
                                if (charge.Refunds.Data.Count() != 0 && charge.Refunded)
                                {
                                    Refund refund = refundservice.Get(charge.Refunds.Data[0].Id);
                                    if (refund.Status == "succeeded")
                                    {
                                        refunded = true;
                                    }
                                }
                            }
                        }
                        else
                        {
                            intentNotFound = true;
                        }
                    }
                    else
                    {
                        //could be a removal of sponsorship allocation
                        RemovedAllocation = (from v in context.Events_VariablePayment
                                             where v.Refund == true
                                             && v.Conference_ID == model.SelectedSponsorship.Conference_ID
                                             && v.CCTransactionReference == model.SelectedSponsorship.CCTransactionRef
                                             select v).Any();
                    }
                } else
                {
                    model.removeBtn = true;
                }
                
                model.RemovedAllocation = RemovedAllocation;
                model.Refunded = refunded;
                model.intentNotFound = intentNotFound;

            }
            return View("_EditSponsorship", model);
        }

        [HttpPost]
        public ActionResult RemoveSponsorship(int VariablePayment_ID)
        {
            var model = new SponsorshipManagementModel();
            var suucess = model.RemoveRecipientVariablePayment(VariablePayment_ID);

            return Json(new { suucess = suucess });
        }

        [HttpPost]
        public ActionResult RefundSponsorship(int VariablePayment_ID, bool Donor)
        {
            var model = new SponsorshipManagementModel();
            model.RefundVariablePayment(VariablePayment_ID, CurrentContext.Instance.User.ContactID, Donor);

            return Json(new { message = "succeed" });
        }

        public ActionResult UpdateVariablePayment(int VariablePayment_ID, int Conference_ID, decimal PaymentAmount, string Comment)
        {
            bool success = false;
            var model = new SponsorshipManagementModel();
            success = model.UpdateVariablePayment(VariablePayment_ID, Conference_ID, PaymentAmount, Comment);

            return Json(new { success }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SponsorshipPayment()
        {
            SponsorshipCartModel model = new SponsorshipCartModel();
            model.Cart = new SponsorshipCartItem();
            return View("SponsorshipPayment", model);
        }

        //This searchContact is for json result instead of inputing whole string
        [HttpPost]
        public JsonResult SearchContacts(string FirstName, string LastName, string Mobile, string Email)
        {
            var list = new List<SponsorshipContactSearch>();

            #region Data Validation

            if (string.IsNullOrEmpty(FirstName)
                && string.IsNullOrEmpty(LastName)
                && string.IsNullOrEmpty(Mobile)
                && string.IsNullOrEmpty(Email))
            {
                return Json(list);
            }

            #endregion

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PlanetshakersDB"].ToString());

            var commandText = "select cc.contact_id, firstname, lastname, mobile, email, ci.contact_id as internal, ce.contact_id as [external] from common_contact cc ";
            commandText += "left join common_contactinternal ci on cc.Contact_ID = ci.Contact_ID ";
            commandText += "left join common_contactexternal ce on cc.Contact_ID = ce.Contact_ID ";
            commandText += "where (cc.contact_id in (select contact_id from common_contact))";
            SqlCommand cmd = new SqlCommand();

            if (!string.IsNullOrEmpty(FirstName))
            {
                commandText += " and firstname = @firstname";

                cmd.CommandText = commandText;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@firstname";
                param.Value = FirstName;
                cmd.Parameters.Add(param);
            }

            if (!string.IsNullOrEmpty(LastName))
            {
                commandText += " and lastname = @lastname";

                cmd.CommandText = commandText;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@lastname";
                param.Value = LastName;
                cmd.Parameters.Add(param);
            }

            if (!string.IsNullOrEmpty(Mobile))
            {
                commandText += " and mobile = @mobile";

                cmd.CommandText = commandText;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@mobile";
                param.Value = Mobile;
                cmd.Parameters.Add(param);
            }

            if (!string.IsNullOrEmpty(Email))
            {
                commandText += " and email = @email";

                cmd.CommandText = commandText;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@email";
                param.Value = Email;
                cmd.Parameters.Add(param);
            }

            commandText += " order by firstname, lastname, contact_id";
            cmd.CommandText = commandText;
            cmd.Connection = conn;

            try
            {
                conn.Open();

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    list.Add(new SponsorshipContactSearch
                    {
                        Contact_ID = Convert.ToInt32(reader["Contact_ID"]),
                        FirstName = reader["FirstName"].ToString(),
                        LastName = reader["LastName"].ToString(),
                        Mobile = reader["Mobile"].ToString(),
                        Email = reader["Email"].ToString(),
                        hasInternal = (reader["Internal"] != DBNull.Value) ? (int?)reader["Internal"] : null,
                        hasExternal = (reader["External"] != DBNull.Value) ? (int?)reader["External"] : null,
                    });
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return Json(list);
        }

        [HttpPost]
        public ActionResult CheckCurrency(int Conference_ID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var Currency = (from ec in context.Events_Conference
                                join ba in context.Common_BankAccount
                                on ec.BankAccount_ID equals ba.BankAccount_ID
                                where ec.Conference_ID == Conference_ID
                                select ba.Currency).FirstOrDefault();

                return Json(new { Currency }, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult SponsorshipSummary(SponsorshipCartModel model)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                // Setup the CCPayment model            
                if (model.CCPayment == null) model.CCPayment = new CCPaymentModel();

                /* Retrieve Bank Account information via Conference_ID to pull the correct publishable key for Stripe */

                model.CCPayment.BankAccount = (from ec in context.Events_Conference
                                               join ba in context.Common_BankAccount
                                                on ec.BankAccount_ID equals ba.BankAccount_ID
                                               where ec.Conference_ID == model.Event.id
                                               select ba).FirstOrDefault();

                model.Event = (from ec in context.Events_Conference
                               join ev in context.Events_Venue
                               on ec.Conference_ID equals ev.Conference_ID
                               join ba in context.Common_BankAccount
                               on ec.BankAccount_ID equals ba.BankAccount_ID
                               where ec.Conference_ID == model.Event.id
                               select new Models.Event
                               {
                                   id = ec.Conference_ID,
                                   venue_id = ev.Venue_ID,
                                   date = ec.ConferenceDate,
                                   location = ec.ConferenceLocation,
                                   name = ec.ConferenceName,
                                   currency = ba.Currency
                               }).FirstOrDefault();

                model.Cart.event_venue_id = model.Event.venue_id;

                if (model.PaymentType_ID == 3) //Credit card payment
                {
                    var successUrl = "http://" + HttpContext.Request.Url.Authority + "/Administration/addSponsorship?venueID=" + model.Event.venue_id;
                    var cancelUrl = HttpContext.Request.Url.AbsoluteUri;

                    StripeConfiguration.ApiKey = model.CCPayment.BankAccount.SecretKey;

                    var options = new Stripe.Checkout.SessionCreateOptions
                    {
                        PaymentMethodTypes = new List<string> { "card" },
                        LineItems = new List<SessionLineItemOptions> { },
                        Mode = "payment",
                        PaymentIntentData = new SessionPaymentIntentDataOptions { },
                        SuccessUrl = successUrl,
                        CancelUrl = cancelUrl
                    };

                    if (model.Cart != null)
                    {
                        options.LineItems.Add(
                            new SessionLineItemOptions
                            {
                                PriceData = new SessionLineItemPriceDataOptions
                                {
                                    Currency = model.CCPayment.BankAccount.Currency,
                                    ProductData = new SessionLineItemPriceDataProductDataOptions
                                    {
                                        Name = model.Event.name + " - Sponsorship"
                                    },
                                    UnitAmount = Convert.ToInt32(model.Cart.totalSponsoredAmount) * 100
                                },
                                Quantity = 1
                            });
                    }

                    var service = new Stripe.Checkout.SessionService();
                    options.PaymentIntentData.Description = model.Event.name + " -  Sponsorship";
                    Stripe.Checkout.Session session = service.Create(options);

                    model.CCPayment.transactionReference = session.PaymentIntentId;

                    Session["BankAccount"] = model.CCPayment.BankAccount;
                    Session["TransactionReference"] = session.PaymentIntentId;

                    ViewData["SessionID"] = session.Id;

                    Session["SponsorshipCart"] = model;

                    return View(model);
                }
                else
                {
                    // Bypass stripe for administrative credit card payments
                    if (model.PaymentType_ID == 10000)
                    {
                        model.PaymentType_ID = 3;
                    }
                    StoreVariablePayment(model, null);
                    SendSponsorshipConfirmationEmail(model);
                    ViewBag.EventName = model.Event.name;

                    return RedirectToAction("SponsorshipConfirmation");
                }
            }
        }

        public ActionResult addSponsorship(int venueID)
        {
            var model = Session["SponsorshipCart"] as SponsorshipCartModel;

            StoreVariablePayment(model, null);
            SendSponsorshipConfirmationEmail(model);
            ViewBag.EventName = model.Event.name;

            return RedirectToAction("SponsorshipConfirmation");
        }

        // Not in use anymore
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SponsorshipCCPayment(CCPaymentModel model, string stripeEmail, string stripeToken)
        {
            try
            {
                #region Validate parameters
                if (model == null)
                {
                    throw new Exception("An unknown error occured.");
                }

                if (stripeToken == null)
                {
                    throw new Exception("An unknown error occured.");
                }

                if (stripeEmail == null)
                {
                    throw new Exception("An unknown error occured.");
                }
                #endregion

                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var cartModel = Session["SponsorshipCart"] as SponsorshipCartModel;
                    #region Check whether or not this contact has an External and ExternalEvent object and create
                    int cID = cartModel.ContactId;
                    
                    var has_external_event = (from cee in context.Common_ContactExternalEvents
                                              where cee.Contact_ID == cID
                                              select cee).ToList();

                    if (!has_external_event.Any())
                    {
                        // create new common_contactExternalEvents
                        var new_contact_event = new Common_ContactExternalEvents();
                        new_contact_event.Contact_ID = cartModel.ContactId;
                        new_contact_event.EmergencyContactName = "";
                        new_contact_event.EmergencyContactPhone = "";
                        new_contact_event.EmergencyContactRelationship = "";
                        new_contact_event.MedicalInformation = "";
                        new_contact_event.MedicalAllergies = "";
                        new_contact_event.AccessibilityInformation = "";
                        new_contact_event.EventComments = "";

                        context.Common_ContactExternalEvents.Add(new_contact_event);
                        context.SaveChanges();
                    }

                    #endregion

                    /* Retrieve Bank Account information via Conference_ID to transfer funds into */
                    var bankAccount = (from ec in context.Events_Conference
                                       join ba in context.Common_BankAccount
                                          on ec.BankAccount_ID equals ba.BankAccount_ID
                                       where ec.Conference_ID == cartModel.Event.id
                                       select ba).FirstOrDefault();

                    model.BankAccount = bankAccount;
                    cartModel.Event.currency = bankAccount.Currency;
                    cartModel.PaymentType_ID = (int)PaymentType.CreditCard;
                    cartModel.CCPayment = model;

                    if (bankAccount == null || String.IsNullOrEmpty(bankAccount.Currency))
                    {
                        ViewBag.Message = "Something went wrong.";
                        return View("Error");
                    }

                    var venue_id = cartModel.Cart.event_venue_id;

                    // Get connection string from web.config
                    var con = ConfigurationManager.ConnectionStrings["PlanetshakersEntities"].ConnectionString;

                    /* ================= Sponsorship Payment Process ================= */

                    #region Process Sponsorship Payment
                    // Setup variables to retrieve responses from the bank
                    string eventName = "";
                    var item = cartModel.Cart;
                    eventName = Convert.ToString(item.event_name);
                    string transactionResponse = String.Empty;
                    string transactionReference = String.Empty;

                    bool paymentSuccess = false;

                    // Instantiate stripe service models
                    /*var customers = new StripeCustomerService();
                    var charges = new StripeChargeService();

                    StripeConfiguration.SetApiKey(bankAccount.SecretKey);

                    // Create stripe token
                    var customer = customers.Create(new StripeCustomerCreateOptions
                    {
                        Email = stripeEmail,
                        SourceToken = stripeToken
                    });

                    // Create stripe charge
                    var amount = Convert.ToInt32(cartModel.GrandTotal) * 100;
                    var charge = charges.Create(new StripeChargeCreateOptions
                    {
                        Amount = amount,       // amount must be at least $0.50
                        Description = "Sponsorship for " + cartModel.Event.name.ToString(),
                        Currency = bankAccount.Currency,
                        CustomerId = customer.Id
                    });

                    if (charge.Status == "succeeded")
                    {
                        paymentSuccess = true;
                    }
                    else
                    {
                        throw new StripeException();
                    }

                    transactionReference = charge.Id.ToString();*/
                    #endregion

                    #region Post processing
                    if (paymentSuccess)
                    {
                        /* At this point, transaction is successful.
                            * Create all the required rows for registrations and payments
                            */

                        string Category = "Stripe SponsorshipPayment";
                        string Criteria = "Venue ID : " + cartModel.Cart.event_venue_id.ToString();
                        string ContactID = cartModel.ContactId.ToString();
                        Business.MWebMain.LogAction(ref Category, ref Criteria, ContactID);

                        #region Insert Variable Payments into database
                        StoreVariablePayment(cartModel, transactionReference);
                        #endregion

                        #region Sponsorship Email Confirmation
                        SendSponsorshipConfirmationEmail(cartModel);
                        #endregion
                    }
                    else
                    {
                        // Remove registrations that have been added
                        throw new Exception("An error occured while processing the payment. "
                                            + transactionResponse);
                    }
                    #endregion 
                    ViewBag.EventName = cartModel.Event.name;
                    return RedirectToAction("SponsorshipConfirmation", "Administration");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult SponsorshipConfirmation()
        {
            return View();
        }

        public ActionResult TermsAndConditions()
        {
            return View();
        }

        private void StoreVariablePayment(SponsorshipCartModel cartModel, string transactionReference)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                // Create new variable payment
                var variablePayment = new Events_VariablePayment();
                variablePayment.IncomingContact_ID = cartModel.ContactId;
                variablePayment.Conference_ID = cartModel.Event.id;
                variablePayment.PaymentType_ID = cartModel.PaymentType_ID; // Credit Card Payment
                variablePayment.PaymentAmount = cartModel.GrandTotal;
                variablePayment.CCTransactionReference = transactionReference;
                variablePayment.Refund = false;
                variablePayment.OutgoingContact_ID = null;
                variablePayment.PaymentDate = DateTime.Now;
                variablePayment.PaymentBy_ID = CurrentContext.Instance.User.ContactID;
                variablePayment.Comment = cartModel.Comment;
                variablePayment.BankAccount_ID = cartModel.CCPayment.BankAccount.BankAccount_ID;
                variablePayment.PaymentCompleted = true;
                variablePayment.PaymentCompletedDate = DateTime.Now;
                variablePayment.Anonymous = true;
                context.Events_VariablePayment.Add(variablePayment);

                try
                {
                    // Save changes                                                               
                    context.SaveChanges();
                }
                catch (Exception ex)
                {

                    throw new Exception(ex.Message);
                }
            }
        }

        public void SendSponsorshipConfirmationEmail(SponsorshipCartModel cartModel)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var email = (from cc in context.Common_Contact
                             where cc.Contact_ID == cartModel.ContactId
                             select cc.Email).FirstOrDefault();
                var email_model = new SponsorshipConfirmationEmailModel()
                {
                    Email = email,
                    ContactId = cartModel.ContactId,
                    Event = cartModel.Event,
                    GrandTotal = cartModel.GrandTotal
                };

                new MailController().SponsorshipConfirmation(email_model);
            }
        }

        #region for sponsorship
        public ActionResult SponsorshipRecipient(SponsorshipCartModel model)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                // Setup the CCPayment model            
                if (model.CCPayment == null) model.CCPayment = new CCPaymentModel();

                /* Retrieve Bank Account information via Conference_ID to pull the correct publishable key for Stripe */

                model.CCPayment.BankAccount = (from ec in context.Events_Conference
                                               join ba in context.Common_BankAccount
                                                on ec.BankAccount_ID equals ba.BankAccount_ID
                                               where ec.Conference_ID == model.Event.id
                                               select ba).FirstOrDefault();

                var evnt = (from ec in context.Events_Conference
                            join ev in context.Events_Venue
                            on ec.Conference_ID equals ev.Conference_ID
                            where ec.Conference_ID == model.Event.id
                            select new
                            {
                                Venue_ID = ev.Venue_ID,
                                ConferenceName = ec.ConferenceName,
                                ConferenceLocation = ec.ConferenceLocation
                            }).FirstOrDefault();

                model.Event.venue_id = evnt.Venue_ID;
                model.Event.name = evnt.ConferenceName;
                model.Event.location = evnt.ConferenceLocation;
                model.Event.currency = model.CCPayment.BankAccount.Currency;
                model.Cart.event_venue_id = model.Event.venue_id;

                StoreRecipientVariablePayment(model);
                ViewBag.EventName = model.Event.name;

                return RedirectToAction("SponsorshipRecipientConfirmation");
            }
        }

        private void StoreRecipientVariablePayment(SponsorshipCartModel cartModel)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                // Create new  group payment
                var variablePayment = new Events_VariablePayment();
                variablePayment.IncomingContact_ID = null;
                variablePayment.Conference_ID = cartModel.Event.id;
                variablePayment.PaymentType_ID = 12;
                variablePayment.PaymentAmount = cartModel.GrandTotal;
                variablePayment.CCTransactionReference = null;
                variablePayment.Refund = false;
                variablePayment.OutgoingContact_ID = cartModel.ContactId;
                variablePayment.PaymentDate = DateTime.Now;
                variablePayment.PaymentBy_ID = CurrentContext.Instance.User.ContactID;
                variablePayment.Comment = cartModel.Comment;
                variablePayment.BankAccount_ID = cartModel.CCPayment.BankAccount.BankAccount_ID;
                variablePayment.PaymentCompletedDate = DateTime.Now;
                variablePayment.PaymentCompleted = true;
                variablePayment.Anonymous = true;

                context.Events_VariablePayment.Add(variablePayment);

                try
                {
                    // Save changes                                                               
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }
        #endregion

        public ActionResult SponsorshipRecipientConfirmation()
        {
            return View();
        }
        #endregion

        #region Transfer management

        public ActionResult Transfer()
        {
            var model = new TransferViewModel();

            return View("Transfer", model);
        }

        //Return the view page to transfer single registration
        public ActionResult TransferSingle(int Registration_ID, int Contact_ID, int Conference_ID, string TransferType)
        {
            var model = new TransferViewModel();
            model.TransferType = TransferType;
            model.Conference_ID = Conference_ID;
            model.ContactId = Contact_ID;

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                if (TransferType == "Single" && Contact_ID > 0 && Conference_ID > 0 && Registration_ID > 0)
                {
                    model.selectedRegistration = (from er in context.Events_Registration
                                                  join ert in context.Events_RegistrationType
                                                  on er.RegistrationType_ID equals ert.RegistrationType_ID
                                                  join ev in context.Events_Venue
                                                  on er.Venue_ID equals ev.Venue_ID
                                                  where er.Contact_ID == Contact_ID && ev.Conference_ID == Conference_ID
                                                  && er.Registration_ID == Registration_ID
                                                  select new Registration
                                                  {
                                                      ID = er.Registration_ID,
                                                      VenueId = er.Venue_ID,
                                                      Conference_ID = ev.Conference_ID,
                                                      RegistrationDate = er.RegistrationDate,
                                                      RegoType_ID = ert.RegistrationType_ID,
                                                      RegoType = ert.RegistrationType,
                                                      Attended = er.Attended,
                                                      IsGroupLeader = (er.GroupLeader_ID == Contact_ID) ? true : false,
                                                      RegoDiscount = er.RegistrationDiscount,
                                                      Volunteer = er.Volunteer,
                                                      VolunteerDepartment_ID = er.VolunteerDepartment_ID,
                                                      GroupLeader_ID = er.GroupLeader_ID,
                                                      HasGroup = (er.GroupLeader_ID != null) ? true : false,
                                                      RegisteredBy_ID = er.RegisteredBy_ID
                                                  }).FirstOrDefault();

                    if (model.selectedRegistration == null)
                    {
                        model.TransferType = "";
                        ViewBag.executedSuccessfully = false;
                        ViewBag.Message = "Cannot find the single registration.";
                    }
                }
                else //Something went wrong, handle that
                {
                    model.TransferType = "";
                    ViewBag.executedSuccessfully = false;
                    ViewBag.Message = "Cannot find the single registration.";
                }

            }

            return View("Transfer", model);
        }

        //public ActionResult TransferSponsorship(int VariablePayment_ID)
        //{
        //    var model = new TransferViewModel();

        //    using (PlanetshakersEntities context = new PlanetshakersEntities())
        //    {
        //        var sponsorship = (from vp in context.Events_VariablePayment
        //                           where vp.VariablePayment_ID == VariablePayment_ID
        //                           select vp).FirstOrDefault();

        //        var selectedConference = (from ec in context.Events_Conference
        //                                  where ec.Conference_ID == sponsorship.Conference_ID
        //                                  select ec).FirstOrDefault();

        //        var currency = (from ba in context.Common_BankAccount
        //                        where sponsorship.BankAccount_ID == ba.BankAccount_ID
        //                        select ba.Currency).FirstOrDefault();
        //        if (sponsorship != null)
        //        {
        //            //Calculate the total available amount of the person selected transferring from
        //            var availableAmount = (from vp in context.Events_VariablePayment
        //                                   where vp.IncomingContact_ID == sponsorship.IncomingContact_ID && vp.Conference_ID == sponsorship.Conference_ID
        //                                   select vp.PaymentAmount);

        //            var fullName = (from cc in context.Common_Contact
        //                            where cc.Contact_ID == sponsorship.IncomingContact_ID
        //                            select cc.FirstName + " " + cc.LastName).FirstOrDefault();

        //            model.Conference_ID = sponsorship.Conference_ID;
        //            model.ConferenceName = selectedConference.ConferenceName;

        //            //Some sponsorship payment details used to display in transfer page
        //            model.selectedSponsorship = new SponsorshipItem();
        //            model.selectedSponsorship.VariablePaymentID = sponsorship.VariablePayment_ID;
        //            model.selectedSponsorship.PaymentAmount = (decimal)availableAmount.Sum();
        //            model.selectedSponsorship.Donor = sponsorship.IncomingContact_ID != null ? true : false;
        //            model.selectedSponsorship.Currency = currency;
        //            model.selectedSponsorship.FullName = fullName;
        //            if (model.selectedSponsorship.Donor)
        //            {
        //                model.ContactId = (int)sponsorship.IncomingContact_ID;
        //            }
        //        }
        //    }
        //    model.TransferType = "Sponsorship";

        //    return View("Transfer", model);
        //}

        //public ActionResult TransferSponsorshipPayment(int VariablePayment_ID, int TransferFromID, int TransferToID, string Comment, decimal TransferAmount)
        //{
        //    if (String.IsNullOrEmpty(Comment))
        //    {
        //        Comment = "";
        //    }

        //    var Conference_ID = 0;
        //    using (PlanetshakersEntities context = new PlanetshakersEntities())
        //    {
        //        var oldPayment = (from vp in context.Events_VariablePayment
        //                          where vp.VariablePayment_ID == VariablePayment_ID
        //                          select vp).FirstOrDefault();

        //        var transferFromName = (from cc in context.Common_Contact
        //                                where cc.Contact_ID == TransferFromID
        //                                select cc.FirstName + " " + cc.LastName).FirstOrDefault();

        //        Conference_ID = oldPayment.Conference_ID;

        //        var positivePayment = CopyVariablePamentDetails(oldPayment, true, TransferAmount, Comment);
        //        positivePayment.IncomingContact_ID = TransferToID;
        //        positivePayment.Comment = TransferComment(Comment, TransferFromID, transferFromName);

        //        var negativePayment = CopyVariablePamentDetails(oldPayment, false, TransferAmount, Comment);
        //        negativePayment.IncomingContact_ID = TransferFromID;
        //        negativePayment.Comment = TransferComment(Comment, TransferFromID, transferFromName);

        //        context.Events_VariablePayment.Add(positivePayment);
        //        context.Events_VariablePayment.Add(negativePayment);

        //        context.SaveChanges();
        //    }

        //    return Json(Url.Action("TransferSuccess", new { Contact_ID = TransferFromID, Conference_ID, TransferType = "Sponsorship" }), JsonRequestBehavior.AllowGet);
        //}

        public ActionResult TransferSuccess(int Contact_ID, int Conference_ID, string TransferType)
        {
            var model = new TransferViewModel();
            model.Conference_ID = Conference_ID;
            model.ContactId = Contact_ID;
            model.TransferType = TransferType;

            return View("TransferSuccess", model);
        }


        public Events_GroupPayment CreateGroupPayment(TransferGroupRegistration transferRego, int Conference_ID, bool positive,
                                                    decimal PaymentAmount, string Comment, int BankAccount_ID)
        {
            Events_GroupPayment newGroupPayment = new Events_GroupPayment();
            newGroupPayment.Conference_ID = Conference_ID;
            newGroupPayment.PaymentType_ID = 9; //9 for transfer payment type

            if (positive)
            {
                newGroupPayment.PaymentAmount = transferRego.TransferQty * PaymentAmount;
            }
            else
            {
                newGroupPayment.PaymentAmount = transferRego.TransferQty * -1 * PaymentAmount;
            }

            newGroupPayment.CCExpiry = "";
            newGroupPayment.CCManual = false;
            newGroupPayment.CCName = "";
            newGroupPayment.CCNumber = "";
            newGroupPayment.CCPhone = "";
            newGroupPayment.CCRefund = false;
            newGroupPayment.ChequeBank = "";
            newGroupPayment.ChequeBranch = "";
            newGroupPayment.ChequeDrawer = "";
            newGroupPayment.PaypalTransactionRef = "";
            newGroupPayment.PaymentCompletedDate = DateTime.Now;
            newGroupPayment.PaymentCompleted = true;
            newGroupPayment.BankAccount_ID = BankAccount_ID;
            newGroupPayment.PaymentBy_ID = CurrentContext.Instance.User.ContactID;

            return newGroupPayment;
        }

        public string TransferComment(string Comment, int Contact_ID, string fullName)
        {
            if (!(Comment.Contains(Contact_ID.ToString()) || Comment.Contains(fullName)))
            {
                Comment = "Transferred from " + Contact_ID.ToString();
                Comment += " - ";
                Comment += fullName;
            }

            return Comment;

        }



        #endregion

        #region Emergency Contact
        public ActionResult EmergencyContact()
        {
            EmergencyContactViewModel model = new EmergencyContactViewModel();
            return View("EmergencyContact", model);
        }

        public ActionResult SearchEmergencyContact(EmergencyContactViewModel model)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var list = (from er in context.Events_Registration
                            join v in context.Events_Venue on er.Venue_ID equals v.Venue_ID
                            join cc in context.Common_Contact on er.Contact_ID equals cc.Contact_ID
                            join ci in context.Common_ContactInternal on er.Contact_ID equals ci.Contact_ID into subci
                            from ci in subci.DefaultIfEmpty()
                            join c in context.Church_Campus on ci.Campus_ID equals c.Campus_ID into subc
                            from c in subc.DefaultIfEmpty()
                            join m in context.Church_Ministry on ci.Ministry_ID equals m.Ministry_ID into subm
                            from m in subm.DefaultIfEmpty()
                            join ce in context.Common_ContactExternalEvents on er.Contact_ID equals ce.Contact_ID
                            where v.Conference_ID == model.Event_ID
                            select new EmergencyContact
                            {
                                Contact_ID = er.Contact_ID,
                                FirstName = cc.FirstName,
                                LastName = cc.LastName,
                                Campus = c.ShortName,
                                Ministry = m.Name
                            }).ToList();
                model.ContactList = list;

            return View("EmergencyContact", model);
            }
        }

        public ActionResult getEmergencyContactDetails(int Contact_ID)
        {
            var lst = new List<string>();

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var list = (from cc in context.Common_Contact 
                            join ce in context.Common_ContactExternalEvents on cc.Contact_ID equals ce.Contact_ID
                            where cc.Contact_ID == Contact_ID
                            select new EmergencyContact
                            {
                                Contact_ID = cc.Contact_ID,
                                FirstName = cc.FirstName,
                                LastName = cc.LastName,
                                Mobile = cc.Mobile,
                                Email = cc.Email,
                                EmergencyContactName = ce.EmergencyContactName,
                                EmergencyContactPhone = ce.EmergencyContactPhone,
                                EmergencyContactRelationship = ce.EmergencyContactRelationship,
                                MedicalInformation = ce.MedicalInformation,
                                MedicalAllegies = ce.MedicalAllergies,
                                AccessibilityInformation = ce.AccessibilityInformation,
                                
                            }).FirstOrDefault();
                var str1 = "<td>" + list.FirstName + ' ' + list.LastName + "</td><td>" + list.Mobile + "</td><td>" + list.Email + "</td>";
                var str2 = "<td>" + list.EmergencyContactName + "</td><td>" + list.EmergencyContactPhone + "</td><td>" + list.EmergencyContactRelationship + "</td>";
                var str3 = "<td>" + list.MedicalInformation + "</td><td>" + list.MedicalAllegies + "</td><td>" + list.AccessibilityInformation + "</td>";

                lst.Add(str1);
                lst.Add(str2);
                lst.Add(str3);
                return Json(lst, JsonRequestBehavior.AllowGet);
            }
        }

        public FileResult ExportCSV_EmergencyContact(int event_id)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                List<EmergencyContact> list = pullEmergencyContact(event_id);


                StringBuilder sb = new StringBuilder();
                sb.Append("Contact_ID, FirstName, LastName, Campus, Ministry, ULG, Mobile, Email, EmergencyContactName, EmergencyContactPhone, Relationship, MedicalInformation, MedicalAllergies, AccessibilityInformation \r\n");
                foreach (var item in list) {
                    var str = new StringBuilder();
                    str.Append(item.Contact_ID + "," + item.FirstName + "," + item.LastName + "," + item.Campus + "," + item.Ministry + ",");
                    if(item.ULG != null)
                    {
                        str.Append(item.ULG.Code + " - " + item.ULG.Name + ",");
                    } else
                    {
                        str.Append(",");
                    }

                    str.Append(item.Mobile + "," + item.Email + "," + item.EmergencyContactName + "," + item.EmergencyContactPhone + "," + item.EmergencyContactRelationship + ",");
                    str.Append(item.MedicalInformation + "," + item.MedicalAllegies + "," + item.AccessibilityInformation + "\r\n");

                    sb.Append(str.ToString());
                };
                return File(Encoding.UTF8.GetBytes(sb.ToString()), "text/csv", "EmergencyContact.csv");
            }
        }

        public void exportExcel_EmergencyContact(int event_id)
        {
            XlsFile excel = new XlsFile();

            excel.NewFile(1);
            excel.ActiveSheet = 1;
            excel.SheetName = "EmergencyContact";

            //input data into excel
            var list = pullEmergencyContact(event_id);
            var header = 1;
            foreach (var field in list[0].GetType().GetProperties())
            {
                excel.SetCellValue(1, header, field.Name);
                header++;
            }
            var row = 2;
            foreach(var item in list)
            {
                var col = 1;
                foreach (var field in item.GetType().GetProperties())
                {
                    if (field.Name == "ULG")
                    {
                        UrbanLife value = (UrbanLife)field.GetValue(item, null);
                        if (value != null)
                        {
                            excel.SetCellValue(row, col, value.Code + " - " + value.Name);
                        } else
                        {
                            excel.SetCellValue(row, col, "");
                        }
                    } else
                    {
                        excel.SetCellValue(row, col, field.GetValue(item, null));
                    }
                    
                    col++;
                }
                row++;
            }

            var filePath = Path.Combine(HttpContext.Server.MapPath("~"), "EmergencyContact.xlsx");
            excel.Save(filePath);

            Response.Buffer = true;
            Response.ClearContent();
            Response.ClearHeaders();
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("content-disposition", "attachment; filename=EmergencyContact.xlsx");

            System.IO.FileStream oFileStream = new System.IO.FileStream(filePath, System.IO.FileMode.Open);

            byte[] oBuffer = new byte[oFileStream.Length + 1];
            oFileStream.Read(oBuffer, 0, (int)oFileStream.Length);
            oFileStream.Close();

            Response.BinaryWrite(oBuffer);

            oFileStream.Close();
            System.IO.File.Delete(filePath);

            Response.End();
        }

        public List<EmergencyContact> pullEmergencyContact(int event_id)
        {

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var list = (from er in context.Events_Registration
                            join v in context.Events_Venue on er.Venue_ID equals v.Venue_ID
                            join cc in context.Common_Contact on er.Contact_ID equals cc.Contact_ID
                            join ci in context.Common_ContactInternal on er.Contact_ID equals ci.Contact_ID into subci
                            from ci in subci.DefaultIfEmpty()
                            join c in context.Church_Campus on ci.Campus_ID equals c.Campus_ID into subc
                            from c in subc.DefaultIfEmpty()
                            join m in context.Church_Ministry on ci.Ministry_ID equals m.Ministry_ID into subm
                            from m in subm.DefaultIfEmpty()
                            join ce in context.Common_ContactExternalEvents on er.Contact_ID equals ce.Contact_ID
                            where v.Conference_ID == event_id
                            select new EmergencyContact
                            {
                                Contact_ID = er.Contact_ID,
                                FirstName = cc.FirstName,
                                LastName = cc.LastName,
                                Campus = c.ShortName,
                                Ministry = m.Name,
                                Mobile = cc.Mobile,
                                Email = cc.Email,
                                EmergencyContactName = ce.EmergencyContactName,
                                EmergencyContactPhone = ce.EmergencyContactPhone,
                                EmergencyContactRelationship = ce.EmergencyContactRelationship,
                                MedicalInformation = ce.MedicalInformation,
                                MedicalAllegies = ce.MedicalAllergies,
                                AccessibilityInformation = ce.AccessibilityInformation

                            }).ToList();
                return list;
            }
        }
        #endregion

        #region Private Helpers

        private void CreateRegistration(AllocationViewModel model)
        {
            // TODO: Convert create rego to a function
        }

        #region Access Control
        public static bool HasAccess(int user_id)
        {
            var access = false;
            var dr = new List<int> { 16 };

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                // check for any access on database roles
                access = (from ccdr in context.Common_ContactDatabaseRole
                               where ccdr.Contact_ID == user_id && (dr.Contains(ccdr.DatabaseRole_ID))
                               select ccdr).Any();
            }

            return access;
        }
        public static bool HasAdminAccess(int user_id)
        {
            var access = false;
            var dr = new List<int> { 17 };

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                // check for any access on database roles
                access = (from ccdr in context.Common_ContactDatabaseRole
                               where ccdr.Contact_ID == user_id && (dr.Contains(ccdr.DatabaseRole_ID))
                               select ccdr).Any();
            }

            return access;
        }

        public static List<int> HasObjectAccess(int Contact_ID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var contactDatabaseRoles = (from r in context.Common_ContactDatabaseRole
                                            where r.Contact_ID == Contact_ID
                                            select r.DatabaseRole_ID).ToList();
                var hasAccess = (from o in context.Common_DatabaseRolePermission
                                 where contactDatabaseRoles.Contains(o.DatabaseRole_ID) && o.Read == true
                                 select o.DatabaseObject_ID).ToList();
                
                var customAccess = (from a in context.Common_ContactRolePermission
                                 where a.Contact_ID == Contact_ID && a.Read == true
                                 select (int)a.DatabaseObject_ID).ToList();

                //return hasAccess.Concat(customAccess).ToList();
                return hasAccess.Union(customAccess).ToList();
            }
        }

        #endregion

        private void CreateEventLog(int contact_id, bool is_group_rego, string action, string item)
        {
            var cartModel = Session["Cart"] as CartModel;
            var user_id = CurrentContext.Instance.User.ContactID;

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var new_history = new Common_ContactHistory();
                new_history.Contact_ID = contact_id;
                new_history.Module = "Events";
                new_history.Desc = String.Empty;
                new_history.Desc2 = String.Empty;
                new_history.DateChanged = DateTime.Now;
                new_history.User_ID = user_id;
                new_history.Action = action;
                new_history.Item = item;
                new_history.Table = String.Empty;
                new_history.FieldName = String.Empty;
                new_history.OldValue = String.Empty;
                new_history.OldText = String.Empty;
                new_history.NewValue = String.Empty;
                new_history.NewText = String.Empty;
                context.Common_ContactHistory.Add(new_history);

                context.SaveChanges();
            }
        }

        #region Send Emails
        public void SendEventsEmail(string ref_no)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                // prepare the confirmation email model
                ILog pxlogger = LogManager.GetLogger("GeneralLogger");

                // Retrieve some cart data from session
                var cartModel = Session["Cart"] as CartModel;

                pxlogger.Info("Preparing confirmation email model.");
                var contact = (from cc in context.Common_Contact
                               where cc.Contact_ID == cartModel.ContactId
                               select cc).FirstOrDefault();

                var email_model = new RegistrationReceiptEmailModel()
                {
                    Email = contact.Email,
                    ContactId = contact.Contact_ID,
                    Event = cartModel.Event,
                    Items = new List<RegistrationConfirmationItem>(),
                    EventName = cartModel.Event.name,
                    EventDate = cartModel.Event.date,
                    EventVenue = cartModel.Event.location,
                    Currency = cartModel.Event.currency,
                    FullName = contact.FirstName + " " + contact.LastName,
                    Subject = "Registration Confirmation - " + cartModel.Event.name,
                    ReceiptNumber = ref_no,
                    PaymentType = (cartModel.CCPayment != null ? cartModel.CCPayment.PaymentType : "N/A"),
                    TransactionReference = (cartModel.CCPayment != null ? (!String.IsNullOrEmpty(cartModel.CCPayment.transactionReference) ? cartModel.CCPayment.transactionReference : "N/A") : "N/A"),
                    Promo = (cartModel.Voucher != null ? cartModel.Voucher : null)
                };

                var is_group_rego = false;

                // create an entry for the registration
                pxlogger.Info("Creating entry for registration.");
                foreach (CartItem item in cartModel.Cart)
                {
                    pxlogger.Debug("Creating entries for " + item.event_name + " " + item.reg_name + " | Quantity :" + item.quantity + " | Contact Id: " + cartModel.ContactId);

                    pxlogger.Debug("Adding items detail to email.");
                    email_model.Items.Add(new RegistrationConfirmationItem
                    {
                        reference_number = ref_no,
                        reg_name = item.reg_name,
                        qty = item.quantity,
                        price = item.single_price
                    });
                }

                // Prepare to send 
                pxlogger.Info("Sending email...");

                if (!String.IsNullOrEmpty(email_model.Email)) 
                {
                    new MailController().RegistrationReceipt(email_model);
                    CreateEventLog(email_model.ContactId, is_group_rego, "Update", "Sent Tax Invoice Email");
                }                    

                var camps = new List<int> { 119, 120, 121 };

                if (camps.Contains(cartModel.Event.id))
                {
                    var id = (from er in context.Events_Registration
                                  where er.Contact_ID == contact.Contact_ID
                                    && er.Venue_ID == cartModel.Event.venue_id
                                  select er.Registration_ID).FirstOrDefault();

                    var conference = (from ev in context.Events_Venue
                                      join ec in context.Events_Conference
                                      on ev.Conference_ID equals ec.Conference_ID
                                      where ev.Venue_ID == cartModel.Event.venue_id
                                      select new Planetshakers.Events.Models.Event
                                      {
                                          id = ec.Conference_ID,
                                          venue_id = ev.Venue_ID,
                                          name = ec.ConferenceName,
                                          date = ec.ConferenceDate,
                                          location = ec.ConferenceLocation,
                                          venueLocation = ev.VenueName,
                                          shortname = ec.ConferenceNameShort
                                      }).FirstOrDefault();

                    var confirmationBarcode = new ConfirmationBarcode()
                    {
                        Subject = "Quick Check In - " + conference.name,
                        EventName = conference.name,
                        EventDate = conference.date.ToString(),
                        EventVenue = conference.location,
                        VenueLocation = conference.venueLocation,
                        Email = (String.IsNullOrEmpty(contact.Email)) ? contact.Email2 : contact.Email,
                        RegistrationNumber = id.ToString()
                    };

                    if (!String.IsNullOrEmpty(confirmationBarcode.Email))
                    {
                        new MailController().ConfirmationBarcodeEmail(confirmationBarcode);
                        CreateEventLog(contact.Contact_ID, false, "Update", "Sent Confirmation Barcode Email");
                    }
                }

            }
        }
        #endregion

        #endregion

        #region Email Mailing List
        public ActionResult EmailMailingList()
        {
            EmailMailingListViewModel model = new EmailMailingListViewModel();
            return View("MailingList/EmailMailingList", model);
        }
        public static List<MailingListResult> EmailListResultList;
        [HttpPost]
        public ActionResult GenerateEmailMailingList(int Event_ID, List<int> FilterRegoType, 
                    List<int> FilterCampus, List<int> FilterRole, List<int> FilterGroup )
        {
            EmailMailingListViewModel model = new EmailMailingListViewModel()
            {
                Event_ID = Event_ID,
                FilterRegistrationType = FilterRegoType,
                FilterCampus = FilterCampus,
                FilterGroup = FilterGroup,
                FilterRole = FilterRole
            };
            EmailListResultList = model.generateList();
            return View("MailingList/_EmailMailingList", model);
        }

        [HttpPost]
        public void EmailMailingListCheckContact(int Contact_ID)
        {
            var pointer = EmailListResultList.Where(x => x.Contact_ID == Contact_ID).FirstOrDefault();
            pointer.check = true;
        }

        [HttpPost]
        public void EmailMailingList_SendToSelf(string Template_ID, string senderEmail)
        {
            var Email = CurrentContext.Instance.User.Email;
            EmailMailingListViewModel model = new EmailMailingListViewModel();
            model.SendTestEmail(Template_ID, senderEmail, Email);
        }

        [HttpPost]
        public void EmailMailingList_Send(string Template_ID, string senderEmail, string campaignName, DateTime scheduleTime, bool SendToAll)
        {
            try
            {
                var contact_id = CurrentContext.Instance.User.ContactID;
                var sendList = (SendToAll) ? EmailListResultList : EmailListResultList.Where(x => x.check).ToList();
                EmailMailingListViewModel model = new EmailMailingListViewModel();
                model.ScheduleEmails(sendList, Template_ID, senderEmail, campaignName, scheduleTime, contact_id);
            } catch
            {
                throw;
            }
        }

        [HttpPost]
        public ActionResult GetGroup(int Conference_ID)
        {
            using(PlanetshakersEntities _ctx = new PlanetshakersEntities())
            {
                var list = (from t in _ctx.Events_Team
                            where t.Inactive == false && t.Event_ID == Conference_ID
                            select new SelectListItem()
                            {
                                Value = t.Team_ID.ToString().Trim(),
                                Text = t.TeamName
                            }).ToList();
                return Json(new { list = list });
            }
        }

        #endregion

        #region SMS Mailing List
        public ActionResult SmsMailingList()
        {
            SmsMailingListViewModel model = new SmsMailingListViewModel();
            return View("MailingList/SmsMailingList", model);
        }
        public static List<MailingListResult> SmsListResultList;
        [HttpPost]
        public ActionResult GenerateSmsMailingList(int Event_ID, List<int> FilterRegoType,
                    List<int> FilterCampus, List<int> FilterRole, List<int> FilterGroup)
        {
            SmsMailingListViewModel model = new SmsMailingListViewModel()
            {
                Event_ID = Event_ID,
                FilterRegistrationType = FilterRegoType,
                FilterCampus = FilterCampus,
                FilterGroup = FilterGroup,
                FilterRole = FilterRole
            };
            EmailListResultList = model.generateList();
            return View("MailingList/_SmsMailingList", model);
        }

        [HttpPost]
        public void SmsMailingListCheckContact(int Contact_ID)
        {
            var pointer = EmailListResultList.Where(x => x.Contact_ID == Contact_ID).FirstOrDefault();
            pointer.check = true;
        }

        [HttpPost]
        public void SmsMailingList_SendToSelf(string message)
        {
            var Mobile = getPhoneNumber(CurrentContext.Instance.User.ContactID);
            SmsMailingListViewModel model = new SmsMailingListViewModel();
            model.SendTestSms(message, Mobile);
        }
        [HttpPost]
        public void SmsMailingList_Send(string message, DateTime scheduleTime, bool SendToAll)
        {
            var contact_id = CurrentContext.Instance.User.ContactID;
            var sendList = (SendToAll) ? EmailListResultList : EmailListResultList.Where(x => x.check).ToList();
            SmsMailingListViewModel model = new SmsMailingListViewModel();
            model.ScheduleSms(sendList, message, scheduleTime, contact_id);
        }

        public string getPhoneNumber(int Contact_ID)
        {
            using(PlanetshakersEntities _ctx = new PlanetshakersEntities())
            {
                var mobile = (from c in _ctx.Common_Contact where c.Contact_ID == Contact_ID select c.Mobile).FirstOrDefault();
                return mobile;
            }
        }
        #endregion
    }
}