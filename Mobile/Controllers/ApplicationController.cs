﻿using Planetshakers.Events.Models;
using System.Web.Mvc;
using System;
using System.Linq;
using Planetshakers.Core.Models;
using System.Collections.Generic;
using Planetshakers.Events.Security;
using Twilio.Rest.Preview.Wireless;
using RazorEngine.Compilation.ImpromptuInterface.InvokeExt;

namespace Planetshakers.Events.Controllers
{
    [Authorize]
    public class ApplicationController : Controller
    {
        public ApplicationController()
        {

        }

        #region Application
        public ActionResult Application()
        {
            ApplicationViewModel viewModel = new ApplicationViewModel();
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult GenerateApplication(int Event_ID, int Status_ID)
        {
            ApplicationViewModel model = new ApplicationViewModel()
            {
                SelectedConferenceID = Event_ID
            };
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var list = (from v in context.Events_Venue
                            join r in context.Events_Registration on v.Venue_ID equals r.Venue_ID
                            join pr in context.Volunteer_PendingRole on r.Registration_ID equals pr.Registration_ID
                            join p in context.Volunteer_ApplicationProgress on pr.Application_ID equals p.VolunteerApplication_ID
                            join a in context.Volunteer_Application on pr.Application_ID equals a.Application_ID
                            join c in context.Common_Contact on a.Contact_ID equals c.Contact_ID
                            join rt in context.Events_RegistrationType on r.RegistrationType_ID equals rt.RegistrationType_ID
                            where v.Conference_ID == Event_ID 
                            select new Application
                            {
                                Application_ID = a.Application_ID,
                                Contact_ID = c.Contact_ID,
                                FirstName = c.FirstName,
                                LastName = c.LastName,
                                SubmittedTime = a.DateRequested,
                                ApprovedTime = (System.DateTime)a.DateActioned,
                                RegistrationType_ID = r.RegistrationType_ID,
                                RegistrationType = rt.RegistrationType,
                                Status_ID = p.Status_ID
                            }).ToList();
                if (Status_ID != 0)
                {
                    list = list.Where(x => x.Status_ID == Status_ID).ToList();
                }

                var registrationTypeList = (from rt in context.Events_RegistrationType
                                            where rt.Conference_ID == Event_ID && rt.ApplicationRegistrationType == false && rt.AddOnRegistrationType == false
                                            select new SelectListItem
                                            {
                                                Value = rt.RegistrationType_ID.ToString().Trim(),
                                                Text = rt.RegistrationType
                                            }).ToList();
                model.RegistrationTypeList = registrationTypeList;
                model.ApplicationList = list;
            }
            return View("_Application", model);
        }
        [HttpPost]
        public ActionResult getApplicationDetails(int Application_ID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                Application model = new Application();
                var list = (from eq in context.Volunteer_EventQuestion
                            join q in context.Volunteer_ApplicationQuestion on eq.Question_ID equals q.Question_ID
                            join a in context.Volunteer_ApplicationAnswer on eq.Question_ID equals a.Question_ID
                            join p in context.Volunteer_Application on a.Application_ID equals p.Application_ID
                            where a.Application_ID == Application_ID && eq.Inactive == false && p.RegistrationType_ID == eq.RegistrationType_ID
                            orderby q.SortOrder
                            select new QuestionAnswer
                            {
                                Answer_ID = a.Answer_ID,
                                Question_ID = a.Question_ID,
                                Answer = a.Answer,
                                Question = q.Question
                            }).ToList();
                model.Details = list;
                var Contact_ID = (from a in context.Volunteer_Application
                                  where a.Application_ID == Application_ID
                                  select a.Contact_ID).FirstOrDefault();
                var ContactDetails = (from cc in context.Common_Contact
                                      join c in context.Common_GeneralCountry on cc.Country_ID equals c.Country_ID into subc
                                      from c in subc.DefaultIfEmpty()
                                      join s in context.Common_GeneralState on cc.State_ID equals s.State_ID into subs
                                      from s in subs.DefaultIfEmpty()
                                      where cc.Contact_ID == Contact_ID
                                      select new ContactQuestion
                                      {
                                          FirstName = cc.FirstName,
                                          LastName = cc.LastName,
                                          DOB = (DateTime)cc.DateOfBirth,
                                          Email = cc.Email,
                                          Phone = cc.Mobile,
                                          Gender = cc.Gender,
                                          Address1 = cc.Address1,
                                          Address2 = cc.Address2,
                                          Suburb = cc.Suburb,
                                          Postcode = cc.Postcode,
                                          Country = c.Country,
                                          State = s.State
                                      }).FirstOrDefault();
                model.ContactQuestion = ContactDetails;
                return View("_ApplicationDetails", model);
            }
        }

        [HttpPost]
        public ActionResult getApplicationReferrals(int Application_ID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                Application model = new Application();
                var list = (from ar in context.Volunteer_ApplicationReferral
                            where ar.Application_ID == Application_ID
                            select new ReferralModel
                            {
                                Referral_ID = ar.ApplicationReferral_ID,
                                ReferralType = ar.ReferralType,
                                FullName = ar.FullName,
                                Relationship = ar.Relationship,
                                Phone = ar.Phone,
                                Email = ar.Email,
                                Approved = ar.Approved,
                                Comment = ar.Comment
                            }).ToList();
                model.Referrals = list;
                return View("_ApplicationReferrals", model);
            }
        }
        [HttpPost]
        public bool approveReferral(int Referral_ID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var referral = (from r in context.Volunteer_ApplicationReferral
                                where r.ApplicationReferral_ID == Referral_ID
                                select r).FirstOrDefault();
                referral.Approved = true;
                context.SaveChanges();
                return true;
            }
        }
        [HttpPost]
        public bool commentReferral(int Referral_ID, string comment)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var referral = (from r in context.Volunteer_ApplicationReferral
                                where r.ApplicationReferral_ID == Referral_ID
                                select r).FirstOrDefault();
                referral.Comment = comment;
                context.SaveChanges();
                return true;
            }
        }

        [HttpPost]
        public ActionResult outcomeApprove(int Application_ID, int RegistrationType_ID)
        {
            try
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    //update Volunteer_Application
                    var Application = (from a in context.Volunteer_Application
                                       where a.Application_ID == Application_ID
                                       select a).FirstOrDefault();
                    Application.Approved = true;
                    Application.Actioned = true;
                    Application.DateActioned = System.DateTime.Now;
                    Application.ActionedBy = CurrentContext.Instance.User.ContactID;

                    //update Volunteer_PendingRole
                    var pendingRole = (from p in context.Volunteer_PendingRole
                                       where p.Application_ID == Application_ID
                                       select p).FirstOrDefault();
                    pendingRole.Actioned = true;
                    pendingRole.ActionedDateTime = System.DateTime.Now;

                    //update Volunteer_ApplicationProgress
                    var progress = (from p in context.Volunteer_ApplicationProgress
                                    where p.VolunteerApplication_ID == Application_ID
                                    select p).FirstOrDefault();
                    progress.Status_ID = 3; //Approved
                    progress.ModifiedDate = System.DateTime.Now;
                    progress.ActionedBy = CurrentContext.Instance.User.ContactID;


                    //change registrationType from deposit to full
                    var registration = (from r in context.Events_Registration
                                        where r.Registration_ID == pendingRole.Registration_ID
                                        select r).FirstOrDefault();
                    var originalRegoType = registration.RegistrationType_ID;
                    registration.RegistrationType_ID = RegistrationType_ID;

                    var groupbulkrego = (from r in context.Events_GroupBulkRegistration
                                         where r.GroupLeader_ID == registration.GroupLeader_ID && r.Venue_ID == registration.Venue_ID && r.RegistrationType_ID == originalRegoType
                                         select r).FirstOrDefault();
                    groupbulkrego.RegistrationType_ID = RegistrationType_ID;
                    context.SaveChanges();

                    var contact = (from c in context.Common_Contact where c.Contact_ID == Application.Contact_ID select c).FirstOrDefault();
                    var email_model = (from v in context.Events_Venue
                                 join c in context.Events_Conference on v.Conference_ID equals c.Conference_ID
                                 join r in context.Events_RegistrationType on c.Conference_ID equals r.Conference_ID
                                 where RegistrationType_ID == r.RegistrationType_ID
                                 select new ApplicationApprovedEmailModal()
                                 {
                                     Venue_ID = v.Venue_ID,
                                     TripName = c.ConferenceName,
                                     FirstName = contact.FirstName,
                                     Email = contact.Email,
                                 }).FirstOrDefault();
                    email_model.TripDashBoard = email_model.getTripDashBoard();
                    email_model.TripPaymentProgress = email_model.getTripPaymentProgress();
                   new MailController().ApplicationApproveEmail(email_model);

                    var registrationTypeName = (from rt in context.Events_RegistrationType where rt.RegistrationType_ID == RegistrationType_ID select rt.RegistrationType).FirstOrDefault();
                    return Json(new { success = true, regoName = registrationTypeName });
                }
            } catch
            {
                return Json(new { success = false });
            }
        }

        [HttpPost]
        public ActionResult outcomeDeny(int Application_ID)
        {
            try
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    //update Volunteer_Application
                    var Application = (from a in context.Volunteer_Application
                                       where a.Application_ID == Application_ID
                                       select a).FirstOrDefault();
                    Application.Approved = false;
                    Application.Actioned = true;
                    Application.DateActioned = System.DateTime.Now;
                    Application.ActionedBy = CurrentContext.Instance.User.ContactID;

                    //update Volunteer_PendingRole
                    var pendingRole = (from p in context.Volunteer_PendingRole
                                       where p.Application_ID == Application_ID
                                       select p).FirstOrDefault();
                    pendingRole.Actioned = true;
                    pendingRole.ActionedDateTime = System.DateTime.Now;

                    //update Volunteer_ApplicationProgress
                    var progress = (from p in context.Volunteer_ApplicationProgress
                                    where p.VolunteerApplication_ID == Application_ID
                                    select p).FirstOrDefault();
                    progress.Status_ID = 4; //Denied
                    progress.ModifiedDate = System.DateTime.Now;
                    progress.ActionedBy = CurrentContext.Instance.User.ContactID;

                    context.SaveChanges();
                    return Json(new { success = true });
                }
            } catch
            {
                return Json(new { success = false });
            }
        }


        #endregion 

        #region Application Question Set Up
        public ActionResult Questions()
        {
            ApplicationQuestionsViewModel model = new ApplicationQuestionsViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult AddQuestionModal()
        {
            ApplicationQuestion model = new ApplicationQuestion();
            model.isEdit = false;
            return View("_QuestionModal", model);
        }

        [HttpPost]
        public ActionResult EditQuestionModal(int Question_ID)
        {
            ApplicationQuestion model = new ApplicationQuestion();
            model.isEdit = true;
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var question = (from q in context.Volunteer_ApplicationQuestion
                                where q.Question_ID == Question_ID
                                select q).FirstOrDefault();
                model.Question_ID = Question_ID;
                model.Question = question.Question;
                model.Type = question.Type;
                model.Active = question.Active;
                model.ShortName = question.ShortName;
                return View("_QuestionModal", model);
            }
        }

        [HttpPost]
        public ActionResult AddQuestion(string Question, string Type, string ShortName)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var sortOrder = (from q in context.Volunteer_ApplicationQuestion
                                 orderby q.SortOrder descending
                                 select q.SortOrder).FirstOrDefault();

                var question = new Volunteer_ApplicationQuestion();
                question.Question = Question;
                question.Type = Type;
                question.ShortName = ShortName;
                question.Required = true;
                question.Active = true;
                question.SortOrder = sortOrder + 1;
                context.Volunteer_ApplicationQuestion.Add(question);
                context.SaveChanges();
                return Json(new { success = true });
            }

        }
        [HttpPost]
        public ActionResult EditQuestion(int Question_ID, string Question, string Type, bool Active, string ShortName)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var question = (from q in context.Volunteer_ApplicationQuestion
                                where q.Question_ID == Question_ID
                                select q).FirstOrDefault();

                question.Question = Question;
                question.Type = Type;
                question.ShortName = ShortName;
                question.Active = Active;
                context.SaveChanges();
                return Json(new { success = true });
            }

        }
        [HttpPost]
        public ActionResult DeleteQuestion(int Question_ID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var question = (from q in context.Volunteer_ApplicationQuestion
                                where q.Question_ID == Question_ID
                                select q).FirstOrDefault();

                context.Volunteer_ApplicationQuestion.Remove(question);
                context.SaveChanges();
                return Json(new { success = true });
            }

        }

        [HttpPost]
        public ActionResult QuestionSortOrder(List<SortOrderItem> List)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                foreach (var item in List)
                {
                    var question = (from q in context.Volunteer_ApplicationQuestion
                                    where q.Question_ID == item.ItemID
                                    select q).FirstOrDefault();
                    question.SortOrder = item.ItemSortOrderID;
                }

                context.SaveChanges();
                return Json(new { success = true });
            }
        }
        #endregion

        #region Application Document Type Set Up
        public ActionResult DocumentType()
        {
            ApplicationDocumentTypeViewModel model = new ApplicationDocumentTypeViewModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult AddDTModal()
        {
            ApplicationDocumentType model = new ApplicationDocumentType();
            model.isEdit = false;
            return View("_DocumentTypeModal", model);
        }

        [HttpPost]
        public ActionResult EditDTModal(int DocumentType_ID)
        {
            ApplicationDocumentType model = new ApplicationDocumentType();
            model.isEdit = true;
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var DT = (from d in context.Volunteer_DocumentType
                          where d.DocumentType_ID == DocumentType_ID
                          select d).FirstOrDefault();
                model.DocumentType_ID = DT.DocumentType_ID;
                model.DocumentType = DT.DocumentType;
                model.Active = DT.Active;
                return View("_DocumentTypeModal", model);
            }
        }

        [HttpPost]
        public ActionResult AddDT(string DocumentType)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var sortOrder = (from q in context.Volunteer_DocumentType
                                 orderby q.SortOrder descending
                                 select q.SortOrder).FirstOrDefault();

                var dt = new Volunteer_DocumentType();
                dt.DocumentType = DocumentType;
                dt.Active = true;
                dt.SortOrder = sortOrder + 1;
                context.Volunteer_DocumentType.Add(dt);
                context.SaveChanges();
                return Json(new { success = true });
            }

        }
        [HttpPost]
        public ActionResult EditDT(int DocumentType_ID, string DocumentType, bool Active)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var dt = (from q in context.Volunteer_DocumentType
                          where q.DocumentType_ID == DocumentType_ID
                          select q).FirstOrDefault();

                dt.DocumentType = DocumentType;
                dt.Active = Active;
                context.SaveChanges();
                return Json(new { success = true });
            }

        }
        [HttpPost]
        public ActionResult DeleteDT(int DocumentType_ID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var dt = (from q in context.Volunteer_DocumentType
                          where q.DocumentType_ID == DocumentType_ID
                          select q).FirstOrDefault();

                context.Volunteer_DocumentType.Remove(dt);
                context.SaveChanges();
                return Json(new { success = true });
            }

        }
        [HttpPost]
        public ActionResult DTsortOrder(List<SortOrderItem> List)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                foreach (var item in List)
                {
                    var dt = (from q in context.Volunteer_DocumentType
                              where q.DocumentType_ID == item.ItemID
                              select q).FirstOrDefault();
                    dt.SortOrder = item.ItemSortOrderID;
                }

                context.SaveChanges();
                return Json(new { success = true });
            }
        }
        #endregion

        #region New Application
        [HttpPost]
        public ActionResult GetContact(string query)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var contact = (from c in context.Common_Contact
                               where c.FirstName == query || c.LastName == query || c.Email == query || c.Phone == query || c.Mobile == query
                               select new Application()
                               {
                                   Contact_ID = c.Contact_ID,
                                   FirstName = c.FirstName,
                                   LastName = c.LastName,
                                   Email = c.Email,
                                   Mobile = c.Mobile,
                               }).ToList();
                var model = new ApplicationViewModel()
                {
                    ApplicationList = contact
                };
                return View("_ContactSearch", model);
            };
        }
        [HttpPost]
        public ActionResult GetRegistrationTypes(int Conference_ID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var RegoTypes = (from rt in context.Events_RegistrationType
                                 where rt.Conference_ID == Conference_ID && rt.ApplicationRegistrationType
                                 select new CartItem()
                                 {
                                     reg_id = rt.RegistrationType_ID,
                                     reg_name = rt.RegistrationType
                                 }).ToList();
                return Json(new { rego = RegoTypes });
            }
        }

        [HttpPost]
        public ActionResult AddApplication(int Conference_ID, int RegistrationType_ID, int Contact_ID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var Venue_ID = (from v in context.Events_Venue where v.Conference_ID == Conference_ID select v.Venue_ID).FirstOrDefault();

                #region Create Contact External (if doesn't exist)
                Common_ContactExternal ce = (from ce1 in context.Common_ContactExternal
                                             where ce1.Contact_ID == Contact_ID
                                             select ce1).FirstOrDefault();
                if (ce == null)
                {
                    ce = new Common_ContactExternal();
                    ce.Contact_ID = Contact_ID;
                    ce.ChurchName = "";
                    ce.Denomination_ID = 49;
                    ce.SeniorPastorName = "";
                    ce.ChurchAddress1 = "";
                    ce.ChurchAddress2 = "";
                    ce.ChurchSuburb = "";
                    ce.ChurchPostcode = "";
                    ce.ChurchState_ID = null;
                    ce.ChurchStateOther = "";
                    ce.ChurchCountry_ID = null;
                    ce.ChurchWebsite = "";
                    ce.LeaderName = "";
                    ce.ChurchNumberOfPeople = "";
                    ce.ChurchPhone = "";
                    ce.ChurchEmail = "";
                    ce.ContactDetailsVerified = true;
                    ce.Deleted = false;
                    ce.Inactive = false;

                    context.Common_ContactExternal.Add(ce);
                    context.SaveChanges();
                }

               
                Common_Contact cc = (from cc1 in context.Common_Contact
                                     where cc1.Contact_ID == Contact_ID
                                     select cc1).FirstOrDefault();
                #endregion
                #region Create Group (if doesn't exist)
                var isGroupLeader = ((from eg in context.Events_Group
                                      where eg.GroupLeader_ID == Contact_ID
                                      select eg).FirstOrDefault() != null ? true : false);

                if (!isGroupLeader)
                {
                    var new_group = new Events_Group();
                    new_group.GroupLeader_ID = Contact_ID;
                    new_group.GroupName = cc.FirstName + " " + cc.LastName;
                    new_group.GroupColeaders = String.Empty;
                    new_group.YouthLeaderName = String.Empty;
                    new_group.GroupSize_ID = null;
                    new_group.GroupCreditStatus_ID = 1;
                    new_group.GroupType_ID = 1;
                    new_group.Comments = String.Empty;
                    new_group.DetailsVerified = false;
                    new_group.Deleted = false;
                    new_group.CreationDate = DateTime.Now;
                    new_group.CreatedBy_ID = Contact_ID;
                    new_group.ModificationDate = null;
                    new_group.ModifiedBy_ID = null;
                    new_group.GUID = Guid.NewGuid();

                    context.Events_Group.Add(new_group);
                    context.SaveChanges();
                }

                #endregion

                //add registraion 
                //add group registration / purchase
                //add application
                //add pending 
                //add progress
                //add referrals 

                createGroupRegistration(Contact_ID, Venue_ID, RegistrationType_ID, 1);
                var Registration_ID = AllocateRegistration(Contact_ID, Venue_ID, RegistrationType_ID);
                AddApplication(Contact_ID, Venue_ID, Registration_ID, RegistrationType_ID);

                return Json(new { });
            };
        }

        private void createGroupRegistration(int contact_id, int venue_id, int rt_id, int qty)
        {
            var now = DateTime.Now;

            using (var ctx = new PlanetshakersEntities())
            {
                var new_bulk_rego = new Events_GroupBulkRegistration();
                new_bulk_rego.GroupLeader_ID = contact_id;
                new_bulk_rego.Venue_ID = venue_id;
                new_bulk_rego.RegistrationType_ID = rt_id;
                new_bulk_rego.Quantity = qty;
                new_bulk_rego.DateAdded = now;
                new_bulk_rego.Deleted = false;

                ctx.Events_GroupBulkRegistration.Add(new_bulk_rego);
                ctx.SaveChanges();
            }
        }
        private int AllocateRegistration(int contact_id, int venue_id, int rt_id)
        {
            using (var ctx = new PlanetshakersEntities())
            {
                var new_registration = new Events_Registration()
                {
                    Contact_ID = contact_id,
                    GroupLeader_ID = contact_id,
                    FamilyRegistration = false,
                    RegisteredByFriend_ID = null,
                    Venue_ID = venue_id,
                    RegistrationType_ID = rt_id,
                    RegistrationDiscount = 0,
                    Elective_ID = null,
                    Accommodation = false,
                    Accommodation_ID = null,
                    Catering = false,
                    CateringNeeds = "",
                    LeadershipBreakfast = false,
                    LeadershipSummit = false,
                    ULG_ID = null,
                    ParentGuardian = "",
                    ParentGuardianPhone = "",
                    AcceptTermsRego = true,
                    AcceptTermsCreche = false,
                    RegistrationDate = DateTime.Now,
                    RegisteredBy_ID = CurrentContext.Instance.User.ContactID,
                    Attended = false,
                    Volunteer = false,
                    VolunteerDepartment_ID = null,
                    ComboRegistration_ID = null
                };
                ctx.Events_Registration.Add(new_registration);
                ctx.SaveChanges();
                return new_registration.Registration_ID;
            }
        }
        public void AddApplication(int Contact_ID, int venue_id, int Registration_ID, int rt_id)
        {
            using (var context = new PlanetshakersEntities())
            {
                var Application = new Volunteer_Application();
                Application.Contact_ID = Contact_ID;
                Application.RegistrationType_ID = rt_id;
                Application.DateRequested = DateTime.Now;
                Application.Actioned = false;
                Application.ActionedBy = null;
                Application.DateActioned = null;
                Application.Approved = false;
                context.Volunteer_Application.Add(Application);

                var pending = new Volunteer_PendingRole();
                pending.Application_ID = Application.Application_ID;
                pending.Registration_ID = Registration_ID;
                pending.CreateDateTime = DateTime.Now;
                pending.Actioned = false;
                pending.ActionedDateTime = null;
                pending.CreatedBy = Contact_ID;
                pending.Deleted = false;
                context.Volunteer_PendingRole.Add(pending);

                //add application progress
                var progress = new Volunteer_ApplicationProgress();
                progress.VolunteerApplication_ID = Application.Application_ID;
                progress.Status_ID = 1;
                progress.ModifiedDate = DateTime.Now;
                context.Volunteer_ApplicationProgress.Add(progress);

                //default insert 2 referrals to application
                var referral = new Volunteer_ApplicationReferral()
                {
                    Application_ID = Application.Application_ID,
                    ReferralType = "",
                    FullName = "",
                    Relationship = "",
                    ReferralDate = DateTime.Now,
                    Phone = "",
                    Email = "",
                    Comment = null,
                    Approved = false
                };
                var referral2 = new Volunteer_ApplicationReferral()
                {
                    Application_ID = Application.Application_ID,
                    ReferralType = "",
                    FullName = "",
                    Relationship = "",
                    ReferralDate = DateTime.Now,
                    Phone = "",
                    Email = "",
                    Comment = null,
                    Approved = false
                };
                context.Volunteer_ApplicationReferral.Add(referral);
                context.Volunteer_ApplicationReferral.Add(referral2);
                context.SaveChanges();
            }
        }

        #endregion 
    }
}
