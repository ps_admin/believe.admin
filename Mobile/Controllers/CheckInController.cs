using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.SessionState;
using System.Collections.Generic;
using System.Diagnostics;
using System.Data.SqlClient;
using Planetshakers.Core.DataInterfaces;
using Planetshakers.Events.Security;
using Planetshakers.Events.Models;
using Planetshakers.Core.Models;

namespace Planetshakers.Events.Controllers
{
    [Authorize]
    public class CheckInController : Controller
    {
        private readonly ICheckInRepository _repository;

        public CheckInController(ICheckInRepository repository)
        {
            _repository = repository;
        }

        #region Conference

        public ActionResult Conference()
        {
            if (HasRegoAccess(CurrentContext.Instance.User.ContactID))
            {
                string category = "Checkin: Conference Click";
                string criteria = "Contact ID: " + CurrentContext.Instance.User.ContactID.ToString();

                //Log to Common_Accesslog table
                Business.MWebMain.LogAction(ref category, ref criteria, CurrentContext.Instance.User.ContactID.ToString());

                return View();
            }
            else
            {
                ViewBag.Message = "You do not have access to this page.";
                return View("Error");
            }
        }

        public ActionResult Submit(List<int> attended)
        {
            string category = "Page Load";
            string criteria = "Contact ID: " + CurrentContext.Instance.User.ContactID.ToString();

            //Log to Common_Accesslog table
            Business.MWebMain.LogAction(ref category, ref criteria, CurrentContext.Instance.User.ContactID.ToString());

            try
            {
                if (attended == null)
                {
                    ViewBag.Message = "No ID(s) seleceted";
                    return View("Error");
                }

                foreach (int regos in attended)
                {
                    _repository.MarkById(regos);
                }

                ViewBag.Attended = attended;

                return View("Success");
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
                return View("Error");
            }
            
        }

        public ActionResult Success()
        {
            return View();
        }

        public ActionResult Attended()
        {
            return View();
        }

        public ActionResult NotInRange()
        {
            return View();
        }

        #region Group Registrations
        public ActionResult GroupRegistrations()
        {
            if (HasRegoAccess(CurrentContext.Instance.User.ContactID))
            {
                string category = "Page Load";
                string criteria = "Contact ID: " + CurrentContext.Instance.User.ContactID.ToString();

                //Log to Common_Accesslog table
                Business.MWebMain.LogAction(ref category, ref criteria, CurrentContext.Instance.User.ContactID.ToString());

                var model = new RegistrationViewModel();

                return View(model);
            }
            else
            {
                ViewBag.Message = "You do not have access to this page.";
                return View("Error");
            }
        }

        #endregion

        #region Search
        public ActionResult Registrations()
        {
            if (HasRegoAccess(CurrentContext.Instance.User.ContactID))
            {
                string category = "Page Load";
                string criteria = "Contact ID: " + CurrentContext.Instance.User.ContactID.ToString();

                //Log to Common_Accesslog table
                Business.MWebMain.LogAction(ref category, ref criteria, CurrentContext.Instance.User.ContactID.ToString());

                var model = new RegistrationViewModel();

                return View(model);
            }
            else
            {
                return RedirectToAction("Error", "Index");
            }

        }

        [HttpPost]
        public ActionResult SearchRegistration(string query)
        {

            if (query != null)
            {
                int i;

                // Don't search for short queries
                if (query.Length < 3)
                {
                    ViewBag.Message = "Query is too short. Please provide the full name or full email.";
                    return View("Error");
                }

                if (int.TryParse(query, out i)){
                    ViewBag.Message = "Invalid query. Please search by full name or full email.";
                    return View("Error");
                }

                try
                {
                    //using (PlanetshakersEntities context = new PlanetshakersEntities())
                    //{
                        var model = new RegistrationViewModel();
                    //    model.searchList = new List<Registration>();

                    //    // Search for names with spaces
                    //    if (query.Contains(" "))
                    //    {
                    //        var parts = query.ToLower().Split();

                    //        var list = (from er in context.vw_Events_RegistrationCheckIn
                    //                    join cc in context.Common_Contact
                    //                        on er.Contact_ID equals cc.Contact_ID
                    //                    where (er.FirstName + " " + er.LastName == query || er.LastName + " " + er.FirstName == query)
                    //                    select new Registration
                    //                    {
                    //                        ID = er.Registration_ID,
                    //                        FirstName = er.FirstName,
                    //                        LastName = er.LastName,
                    //                        Conference_ID = er.Conference_ID,
                    //                        ConferenceName = er.ConferenceName,
                    //                        Email = (String.IsNullOrEmpty(cc.Email) ? cc.Email2 : cc.Email)
                    //                    }).ToList();

                    //        model.searchList.AddRange(list);

                    //        foreach (var s in parts)
                    //        {
                    //            list = (from er in context.vw_Events_RegistrationCheckIn
                    //                    join cc in context.Common_Contact
                    //                        on er.Contact_ID equals cc.Contact_ID
                    //                    where (er.FirstName.StartsWith(s) || er.LastName.StartsWith(s))
                    //                    select new Registration
                    //                    {
                    //                        ID = er.Registration_ID,
                    //                        FirstName = cc.FirstName,
                    //                        LastName = cc.LastName,
                    //                        Conference_ID = er.Conference_ID,
                    //                        ConferenceName = er.ConferenceName,
                    //                        Email = (String.IsNullOrEmpty(cc.Email) ? cc.Email2 : cc.Email)
                    //                    }).ToList();

                    //            var list2 = list.Where(p => !model.searchList.Any(p2 => p2.ID == p.ID));

                    //            model.searchList.AddRange(list2);
                    //        }

                    //        // Search for emails only
                    //    } else if (query.Contains("@") && !query.Contains(" "))
                    //    {
                    //        var list = (from er in context.vw_Events_RegistrationCheckIn
                    //                    join cc in context.Common_Contact
                    //                        on er.Contact_ID equals cc.Contact_ID
                    //                    where (cc.Email == query || cc.Email2 == query)
                    //                    select new Registration
                    //                    {
                    //                        ID = er.Registration_ID,
                    //                        FirstName = cc.FirstName,
                    //                        LastName = cc.LastName,
                    //                        Conference_ID = er.Conference_ID,
                    //                        ConferenceName = er.ConferenceName,
                    //                        Email = (String.IsNullOrEmpty(cc.Email) ? cc.Email2 : cc.Email)
                    //                    }).ToList();

                    //        model.searchList.AddRange(list);
                        
                    //    // Search for single names
                    //    } else if (!query.Contains(" "))
                    //    {
                    //        var list = (from er in context.vw_Events_RegistrationCheckIn
                    //                    join cc in context.Common_Contact
                    //                        on er.Contact_ID equals cc.Contact_ID
                    //                    where ((er.FirstName.StartsWith(query) || er.LastName.StartsWith(query)))
                    //                    select new Registration
                    //                    {
                    //                        ID = er.Registration_ID,
                    //                        FirstName = cc.FirstName,
                    //                        LastName = cc.LastName,
                    //                        Conference_ID = er.Conference_ID,
                    //                        ConferenceName = er.ConferenceName,
                    //                        Email = (String.IsNullOrEmpty(cc.Email) ? cc.Email2 : cc.Email)
                    //                    }).ToList();

                    //        model.searchList.AddRange(list);
                    //    } 

                        return View("Registrations", model);
                    //}

                }
                catch (Exception e)
                {

                    throw e;
                }
            }
            else
            {
                ViewBag.Message = "Invalid search query.";
                return View("Error");
            }
        }

        
        #endregion

        #region Allocation
        ///* Force allocate all remaining registrations to the group leader */
        //public ActionResult Allocate(int registrationId, int? groupleaderId, int? venueId)
        //{
        //    string category = "Page Load";
        //    string criteria = "Contact ID: " + CurrentContext.Instance.User.ContactID.ToString();

        //    //Log to Common_Accesslog table
        //    Business.MWebMain.LogAction(ref category, ref criteria, CurrentContext.Instance.User.ContactID.ToString());

        //    using (PlanetshakersEntities context = new PlanetshakersEntities())
        //    {
        //        if (registrationId > 0)
        //        {
        //            var instance = (from er in context.Events_Registration
        //                            where er.Registration_ID == registrationId
        //                            select er).FirstOrDefault();

        //            int venue_ID = instance.Venue_ID;
        //            int contact_ID = instance.Contact_ID;
        //            int registrationtype_ID = instance.RegistrationType_ID;

        //            var total = (from egbr in context.Events_GroupBulkRegistration
        //                         where egbr.GroupLeader_ID == contact_ID
        //                                && egbr.Venue_ID == venue_ID
        //                         select egbr.Quantity).Sum();

        //            var allocated = (from er in context.Events_Registration
        //                             where (er.GroupLeader_ID == contact_ID || er.Contact_ID == contact_ID)
        //                                    && er.Venue_ID == venue_ID
        //                             select er).Count();

        //            var unallocated = total - allocated;

        //            for (int i = 0; i < unallocated; i++)
        //            {
        //                _repository.InsertRegistration(contact_ID, venue_ID, registrationtype_ID);
        //            }
        //        } else
        //        {
        //            var instance = (from egbr in context.Events_GroupBulkRegistration
        //                            where egbr.GroupLeader_ID == groupleaderId && egbr.Venue_ID == venueId
        //                            select egbr).ToList();

        //            foreach (var i in instance)
        //            {
        //                var allocated = (from er in context.Events_Registration
        //                                 where er.Venue_ID == i.Venue_ID
        //                                 && er.GroupLeader_ID == i.GroupLeader_ID
        //                                 && er.RegistrationType_ID == i.RegistrationType_ID
        //                                 select er.Registration_ID).Count();

        //                var unallocated = i.Quantity - allocated;

        //                for(int k = 0; k < i.Quantity; k++)
        //                {
        //                    _repository.InsertRegistration(i.GroupLeader_ID, i.Venue_ID, i.RegistrationType_ID);
        //                }
        //            }

        //            registrationId = (from er in context.Events_Registration
        //                              where er.GroupLeader_ID == groupleaderId && er.Venue_ID == venueId
        //                              select er.Registration_ID).FirstOrDefault();
        //        }
                
        //    }

        //    return RedirectToAction("Collect", new { registrationId = registrationId });
        //}
        
        //public ActionResult Allocation()
        //{
        //    string category = "Page Load";
        //    string criteria = "Contact ID: " + CurrentContext.Instance.User.ContactID.ToString();

        //    //Log to Common_Accesslog table
        //    Business.MWebMain.LogAction(ref category, ref criteria, CurrentContext.Instance.User.ContactID.ToString());

        //    var model = new AllocationViewModel();

        //    return View(model);
        //}

        //public ViewResult SearchResults(string keyword)
        //{
        //    var model = new AllocationViewModel();

        //    if (keyword != null)
        //    {
        //        try
        //        {
        //            List<int> event_list = new List<int>();

        //            using (PlanetshakersEntities context = new PlanetshakersEntities())
        //            {
        //                event_list = (from ec in context.Events_Conference
        //                                join ev in context.Events_Venue
        //                                on ec.Conference_ID equals ev.Conference_ID
        //                                where ev.ConferenceEndDate >= DateTime.Now
        //                                orderby ev.ConferenceStartDate
        //                                select ev.Venue_ID).ToList();

        //                model.searchList = new List<ContactDetails>();

        //                var found = false;

        //                if (!found)
        //                {
        //                    var keywords = keyword.Split();

        //                    foreach (var s in keywords)
        //                    {
        //                        var list = (from cc in context.Common_Contact
        //                                    join egbr in context.Events_GroupBulkRegistration
        //                                    on cc.Contact_ID equals egbr.GroupLeader_ID
        //                                    where (cc.FirstName.Contains(s) && cc.LastName.Contains(s)) || cc.Email.Contains(s)
        //                                          && (event_list.Contains(egbr.Venue_ID))
        //                                    select new ContactDetails
        //                                    {
        //                                        Id = cc.Contact_ID,
        //                                        FirstName = cc.FirstName,
        //                                        LastName = cc.LastName,
        //                                        ContactNumber = cc.Mobile,
        //                                        Email = cc.Email                                                
        //                                    }).Distinct().ToList();

        //                        var list2 = list.Where(p => !model.searchList.Any(p2 => p2.Id == p.Id));

        //                        model.searchList.AddRange(list2);
        //                    }
        //                } else
        //                {

        //                }

        //                return View("Allocation", model);
        //            }

        //        }
        //        catch (Exception e)
        //        {
        //            ViewBag.Message = e.Message;
        //            throw e;
        //        }
        //    }
        //    else
        //    {
        //        ViewBag.Message = "Keyword not found";
        //        return View("Error");
        //    }
        //}

        //[HttpPost]
        //public ActionResult ViewRegistrations(AllocationViewModel model)
        //{
        //    string category = "Page Load";
        //    string criteria = "Contact ID: " + CurrentContext.Instance.User.ContactID.ToString();

        //    //Log to Common_Accesslog table
        //    Business.MWebMain.LogAction(ref category, ref criteria, CurrentContext.Instance.User.ContactID.ToString());

        //    try
        //    {
        //        using (PlanetshakersEntities context = new PlanetshakersEntities())
        //        {
        //            model.Venue_ID = (from ev in context.Events_Venue
        //                              where DateTime.Now <= ev.ConferenceEndDate
        //                              orderby ev.ConferenceStartDate
        //                              select ev.Venue_ID).FirstOrDefault();

        //            model.Registration_ID = (from er in context.Events_Registration
        //                                     where er.Contact_ID == model.Contact_ID && (er.Venue_ID == model.Venue_ID)
        //                                     select er.Registration_ID).FirstOrDefault();
        //        }
        //    } catch (Exception ex)
        //    {
        //        ViewBag.Message = ex.Message;
        //        return View("Error");
        //    }            

        //    return View(model);
        //}

        //public ActionResult AllocateNow(AllocationViewModel model, int toAllocate)
        //{
        //    string category = "Page Load";
        //    string criteria = "Contact ID: " + CurrentContext.Instance.User.ContactID.ToString();

        //    //Log to Common_Accesslog table
        //    Business.MWebMain.LogAction(ref category, ref criteria, CurrentContext.Instance.User.ContactID.ToString());

        //    toAllocate -= model.TotalAllocated;

        //    try
        //    {
        //        for (int i = 0; i < toAllocate; i++)
        //        {
        //            _repository.InsertRegistration(model.Contact_ID, model.Venue_ID, model.RegistrationType_ID);
        //        }
        //    } catch (Exception ex)
        //    {
        //        ViewBag.Message = ex.Message;
        //        return View("Error");
        //    }

        //    return View("ViewRegistrations", model);
        //}
        #endregion

        #endregion

        #region Helper Functions
        /* ========================================================= 
         * HELPER FUNCTIONS 
         * ========================================================= */

        public bool IsGroupLeader(int cid, int? group_id)
        {
            return cid == group_id;
        }

        public List<int> GetRegistrations(int glid, int venue_id)
        {
            List<int> registrations = new List<int>();

            // Retrieve all registrations belonging to a Group Leader
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                registrations = (from er in context.Events_Registration
                                 where (er.GroupLeader_ID == glid) &&
                                        (er.Venue_ID == venue_id)
                                 select er.Registration_ID).ToList();
            }

            return registrations;
        }

        public static bool HasRegoAccess(int contactid)
        {
            List<int> accessrole = new List<int> { 26, 73, 222 };
            using (PlanetshakersEntities entity = new PlanetshakersEntities())
            {
                var role = (from cr in entity.Church_ContactRole
                            join ccdr in entity.Common_ContactDatabaseRole
                               on cr.Contact_ID equals ccdr.Contact_ID into x
                               from cx in x.DefaultIfEmpty()
                            where cr.Contact_ID == contactid && (accessrole.Contains(cr.Role_ID) || cx.DatabaseRole_ID == 62) && cr.Inactive == false
                            select cr).FirstOrDefault();

                if (role != null)
                    return true;
                else
                    return false;
            }
        }

        public JsonResult GetRegoRegistrationTypes(int contact_id, int venue_id, int count)
        {
            //var json = new Dictionary<string, int>();

            using (PlanetshakersEntities entity = new PlanetshakersEntities())
            {
                if (count > 1)
                {
                    var list = (from er in entity.Events_Registration
                                join ert in entity.Events_RegistrationType
                                on er.RegistrationType_ID equals ert.RegistrationType_ID
                                where er.GroupLeader_ID == contact_id && er.Venue_ID == venue_id && er.Attended == false
                                group er by ert.RegistrationType into g
                                select new
                                {
                                    RegistrationType = g.Key,
                                    Count = g.Count()
                                });

                    var json = list.ToDictionary(g => g.RegistrationType, g => g.Count);

                    return Json(json, JsonRequestBehavior.AllowGet);

                } else
                {
                    return Json("error", JsonRequestBehavior.AllowGet);
                }
            }
            
        }

        #endregion
    }

}