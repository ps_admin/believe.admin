﻿using System.Web.Mvc;

namespace Planetshakers.Events.Controllers
{
    public class ErrorController : Controller
    {
        //
        // GET: /Error/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Unauthorised()
        {
            return View("Details", 401);
        }
    }
}
