﻿using System.Web.Mvc;

namespace Planetshakers.Events.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public HomeController()
        {
            
        }

        public ActionResult Index()
        {
            return View();
        }
    }
}
