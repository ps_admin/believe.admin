﻿using System;
using System.Collections.Generic;
using Planetshakers.Events.Models;
using System.Web.Mvc;

namespace Planetshakers.Events.Controllers
{
    [Authorize]
    public class HostEventsController : Controller
    {
        public ActionResult HostEvents()
        {
            HostEventViewModel model = new HostEventViewModel();
            return View(model);
        }

        public ActionResult getHostEvents(int Event_ID)
        {
            HostEventViewModel model = new HostEventViewModel();
            model.populateInstanctList(Event_ID);
            model.Event_ID = Event_ID;
            return View("_HostEvent", model);
        }

        public ActionResult HostEventManagement()
        {
            HostEventsManagementViewModel model = new HostEventsManagementViewModel();
            return View(model);
        }

        public ActionResult HostEventDetails(int HostEvent_ID)
        {
            HostEvents model = new HostEvents();
            if (HostEvent_ID != 0)
            {
                model.getHostEvent(HostEvent_ID);
                model.populateInstanceList(HostEvent_ID);
                model.populateTopicList(HostEvent_ID);
                model.isEdit = true;
            }
            else
            {
                model.isEdit = false;
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult AddEditHostEventDetails(int HostEvent_ID, string Name, int Points, string Template_ID, bool customReport, bool logSession)
        {

            HostEvents model = new HostEvents();
            model.HostEvent_ID = HostEvent_ID;
            model.Name = Name;
            model.PointRequired = Points;
            model.LogCompletedSession = logSession;
            model.ShowInCustomReport = customReport;
            if (HostEvent_ID == 0)
            {
                model.HostEvent_ID = model.addHostEventDetails();
            } else
            {
                model.editHostEventDetails();
            }

            return Json(new { HostEvent_ID = model.HostEvent_ID });
        }

        [HttpPost]
        public ActionResult GetTopicDetailModal(int Topic_ID, int HostEvent_ID)
        {
            HostEventTopic model = new HostEventTopic();
            if (Topic_ID != 0)
            {
                model.getTopicModal(Topic_ID);
            }

            return View("_TopicDetails", model);
        }

        [HttpPost]
        public ActionResult addTopic(int HostEvent_ID, string Name, int Point)
        {
            HostEventTopic model = new HostEventTopic();
            model.HostEvent_ID = HostEvent_ID;
            model.Name=Name;
            model.PointWeighting = Point;
            model.addTopic();
            return Json(new { });
        }

        [HttpPost]
        public ActionResult editTopic(int Topic_ID, string Name, int Point)
        {
            HostEventTopic model = new HostEventTopic();
            model.HostEventTopic_ID = Topic_ID;
            model.Name = Name;
            model.PointWeighting = Point;
            model.editTopic();
            return Json(new { });
        }

        public ActionResult HostEventInstances(int HostEventInstance_ID, int HostEvent_ID)
        {
            HostEventInstance model = new HostEventInstance();
            if (HostEventInstance_ID != 0)
            {
                model.getInstance(HostEventInstance_ID);
                model.getSession(HostEventInstance_ID);
                model.getAttendanceList(HostEventInstance_ID);
                foreach(var item in model.SessionList)
                {
                    item.Topics = model.getTopic(item.Session_ID);
                    if(item.Topics != null)
                    {
                        foreach(var topic in item.Topics)
                        {
                            item.TopicStringList = item.TopicStringList + ", " + topic.Name;
                        }
                    }
                }
                model.isEdit = true;
            } else
            {
                model.HostEvent_ID = HostEvent_ID;
                model.StartDate = DateTime.Now;
                model.EndDate = DateTime.Now;
                model.isEdit = false;
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult AddEditHostEventInstanceDetails(int HostEventInstance_ID, int HostEvent_ID, string Name, DateTime StartDate, DateTime EndDate, int MaxCapacity, int Event_ID)
        {
            HostEventInstance model = new HostEventInstance();
            model.HostEventInstance_ID = HostEventInstance_ID;
            model.HostEvent_ID = HostEvent_ID;
            model.Name = Name;
            model.StartDate = StartDate;
            model.EndDate = EndDate;
            model.MaxCapacity = MaxCapacity;
            model.Event_ID = Event_ID;

            if (HostEventInstance_ID == 0)
            {
                model.HostEventInstance_ID = model.addHostEventInstances();
            }
            else
            {
                model.editHostEventInstances();
            }

            return Json(new { HostEventInstance_ID = model.HostEventInstance_ID, HostEvent_ID = model.HostEvent_ID });

        }

        [HttpPost]
        public ActionResult GetHostEventSessionModal(int Session_ID, int HostEvent_ID, int HostEventInstance_ID)
        {
            HostEventSession model = new HostEventSession();
            model.HostEvent_ID = HostEvent_ID;
            model.Instanct_ID = HostEventInstance_ID;
            if (Session_ID != 0)
            {
                model.getSession(Session_ID);
            } else
            {
                model.Date = DateTime.Now;
            }

            return View("_SessionDetails", model);
        }

        [HttpPost]
        public ActionResult addSession(int Instance_ID, DateTime SessionDate, List<int> TopicList)
        {
            HostEventSession model = new HostEventSession()
            {
                Instanct_ID = Instance_ID,
                Date = SessionDate,
                selectedTopic = TopicList
            };
            model.addSession();
            return Json(new { });
        }

        [HttpPost]
        public ActionResult editSession(int Session_ID, DateTime SessionDate, List<int> TopicList)
        {
            HostEventSession model = new HostEventSession()
            {
                Session_ID = Session_ID,
                Date = SessionDate,
                selectedTopic = TopicList
            };
            model.editSession();
            return Json(new { });
        }

        [HttpPost]
        public ActionResult AddAttendee(int Contact_ID, int HostEventInstance_ID)
        {
            CourseEnrolment model = new CourseEnrolment();
            model.addAttendee(Contact_ID, HostEventInstance_ID);
            return Json(new { Contact_ID = Contact_ID, FirstName = model.FirstName, LastName = model.LastName});
        }

        [HttpPost]
        public ActionResult InstanceSessionModal(int Instance_ID, int Event_ID)
        {
            HostEventInstance model = new HostEventInstance();
            model.getSession(Instance_ID);
            model.Event_ID = Event_ID;
            var todayDate = DateTime.Now.Date;

            foreach(var item in model.SessionList)
            {
                if (item.Date == todayDate.Date)
                {
                    item.isToday = true;
                } else
                {
                    item.isToday = false;
                    item.Passed = (item.Date < todayDate)? true: false;
                }
            }
            return View("_SessionModal", model);
        }

        public ActionResult MarkAttendance(int Instance_ID, int Session_ID, int Event_ID, bool mark)
        {
            HostEventInstance helper = new HostEventInstance();
            helper.Event_ID = Event_ID;
            helper.Session_ID = Session_ID;
            helper.getAttendanceList(Instance_ID);

            MarkAttendanceViewModel model = new MarkAttendanceViewModel()
            {
                Event_ID = Event_ID,
                Session_ID = Session_ID,
                mark = mark
            };
            model.AttendeeList = helper.AttendeeList;
            model.getDetails();
            return View(model);
        }

        [HttpPost]
        public ActionResult SessionMarkAttendance(int Contact_ID, int Session_ID, bool attend)
        {
            MarkAttendanceViewModel model = new MarkAttendanceViewModel();
            if (attend)
            {
                model.MarkAttendance(Contact_ID, Session_ID);
            } else
            {
                model.RemoveAttendance(Contact_ID, Session_ID);
            }
            return Json(new { });
        }
    }
}