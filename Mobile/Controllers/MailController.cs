﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ActionMailer.Net.Mvc;
using Newtonsoft.Json;
using Planetshakers.Events.Helpers;
using Planetshakers.Events.Models;
using Planetshakers.Events.Security;
using SendGrid.SmtpApi;

namespace Planetshakers.Events.Controllers
{
    public class MailController : MailerBase
    {
        JsonSerializerSettings _serializerSettings = new JsonSerializerSettings()
        {
            TypeNameHandling = TypeNameHandling.None,
            TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Simple
        };

        private readonly string _psApiKey = AzureServices.KeyVault_RetreiveSecret("believe-apps", "PsApiKey");
        private readonly string _tokenSalt = AzureServices.KeyVault_RetreiveSecret("believe-apps", "TokenSalt");

        public EmailResult RegistrationConfirmation(RegistrationReceiptEmailModel model)
        {
            // Retrieve full name
            model.FullName = getContactFullName(model.ContactId);
            From = model.Email;
            To.Add(getContactEmail(model.ContactId));
            Subject = "Planetshakers Event Registration Confirmation";
            return Email("RegistrationConfirmation", model);
        }

        public EmailResult VolunteerRegistrationConfirmation(VolunteerRegistrationConfirmationItem model, List<string> emails)
        {
            From = model.Email;
            //Retrieve the mail ID specified in web.config            
            string ToEmail = ConfigurationManager.AppSettings["VolunteerRegistrationEmail"];

            foreach (string e in emails)
            {
                To.Add(e);
            }

            Subject = "Planetshakers Volunteer Registration Confirmation";
            Attachments.Add("Volunteer Application.pdf", model.VolunteerForm);
            return Email("VolunteerRegistrationConfirmation", model);
        }

        public EmailResult PaymentConfirmation(PaymentConfirmationItem model)
        {
            //Sender's SMTP mail ID
            From = model.Email;
            //Retrieve the mail ID specified in web.config
            string ToEmail = ConfigurationManager.AppSettings["CoursePaymentEmail"];
            To.Add(ToEmail);
            Subject = "Planetshakers Payment Confirmation";
            Attachments.Add("Payment Details.pdf", model.PaymentForm);
            return Email("PaymentConfirmation", model);
        }

        public void RegistrationReceipt(RegistrationReceiptEmailModel model)
        {
            try
            {
                var con = ConfigurationManager.ConnectionStrings["PlanetshakersEntities"].ConnectionString;
                var template_id = ConfigurationManager.AppSettings["SendGridTemplateTaxInvoice"];

                if (con.Contains("PlanetshakersSG"))
                    template_id = ConfigurationManager.AppSettings["SendGridTemplateTaxInvoiceSG"];

                model.ItemList = "";
                foreach (RegistrationConfirmationItem item in model.Items)
                {
                    model.ItemList += "<tr>";
                    model.ItemList += "<td>" + item.reg_name + "</td>";
                    model.ItemList += "<td>" + item.qty + "</td>";
                    model.ItemList += "<td>" + item.price + "</td>";
                    model.ItemList += "<td>" + model.Event.currency + " " + item.subtotal + "</td>";
                    model.ItemList += "</tr>";
                }

                _ = SendConferenceMailViaPsApi(model.Email, template_id, model);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }

        public void ApplicationApproveEmail(ApplicationApprovedEmailModal model)
        {
            try
            {
                _ = SendMailViaPsApi(model.Email, model.Template_ID, model);
            } catch (Exception ex)
            {
                throw ex;
            }
        }

        //Used in sending registration tax invoice
        public void RegistrationTaxInvoiceReceipt(RegistrationReceiptEmailModel model)
        {
            try
            {
                var template_id = ConfigurationManager.AppSettings["SendGridEventPaymentReceipt"];

                model.ItemList = "";
                foreach (RegistrationConfirmationItem item in model.Items)
                {
                    model.ItemList += "<tr>";
                    model.ItemList += "<td>" + item.reg_name + "</td>";
                    model.ItemList += "<td>" + item.qty + "</td>";
                    model.ItemList += "</tr>";
                }

                _ = SendConferenceMailViaPsApi(model.Email, template_id, model);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }

        public void SponsorshipConfirmation(SponsorshipConfirmationEmailModel model)
        {
            try
            {
                model.FullName = getContactFullName(model.ContactId);
                string templateID = ConfigurationManager.AppSettings["SendGridTemplateSponsorshipConfirmation"];
                _ = SendConferenceMailViaPsApi(model.Email, templateID, model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Using dynamic template in substitute of ConfirmationBarcode function
        public void ConfirmationBarcodeEmail(ConfirmationBarcode dynamicTemplateData)
        {
            try
            {
                var con = ConfigurationManager.ConnectionStrings["PlanetshakersEntities"].ConnectionString;
                string templateID = ConfigurationManager.AppSettings["SendGridTemplateRegistrationConfirmation2"];
                if (con.Contains("PlanetshakersSG"))
                {
                    dynamicTemplateData.ContactDetails = "Email us at church@planetshakers.sg.";
                }
                else
                {
                    dynamicTemplateData.ContactDetails = "Email us at conference@planetshakers.com or call us at +61 3 9896 7999.";
                }
                _ = SendConferenceMailViaPsApi(dynamicTemplateData.Email, templateID, dynamicTemplateData);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public async Task SendMailViaPsApi(string RecipientEmail, string TemplateId, object dynamicTemplateData, string SenderEmail = "info@believeglobal.org")
        {
            objMailApi objMailApi = new objMailApi()
            {
                AuthKey = hashHMACSHA256(_psApiKey),
                DynamicTemplateData = dynamicTemplateData,
                RecipientEmail = RecipientEmail,
                TemplateId = TemplateId,
                SenderEmail = SenderEmail,
                SenderEmailName = "Believe Global"
            };

            string PostData = JsonConvert.SerializeObject(objMailApi, _serializerSettings);
            var URL = "https://api.planetshakers.com/API/ApiMail/SendMail";
            string ServerResponse = await getJSON(URL, PostData);

            if (ServerResponse == "ERROR. Signature Mismatch.")
            {
                throw new UnauthorizedAccessException(ServerResponse);
            }
            else if (ServerResponse != "\"ok\"")
            {
                throw new HttpException(ServerResponse);
            }
        }

        public async Task SendMailViaPsApi(string RecipientEmail, string TemplateId, object dynamicTemplateData, string attachment, string attachmentFileName)
        {
            objMailApi objMailApi = new objMailApi()
            {
                AuthKey = hashHMACSHA256(_psApiKey),
                DynamicTemplateData = dynamicTemplateData,
                RecipientEmail = RecipientEmail,
                TemplateId = TemplateId,
                SenderEmail = "info@believeglobal.org",
                SenderEmailName = "Believe Global",
                Attachment = attachment,
                AttachmentFileName = attachmentFileName
            };

            string PostData = JsonConvert.SerializeObject(objMailApi, _serializerSettings);
            var URL = "https://api.planetshakers.com/API/ApiMail/SendMail";
            string ServerResponse = await getJSON(URL, PostData);

            if (ServerResponse == "ERROR. Signature Mismatch.")
            {
                throw new UnauthorizedAccessException(ServerResponse);
            }
            else if (ServerResponse != "\"ok\"")
            {
                throw new HttpException(ServerResponse);
            }
        }

        public async Task SendConferenceMailViaPsApi(string RecipientEmail, string TemplateId, object dynamicTemplateData)
        {
            objMailApi objMailApi = new objMailApi()
            {
                AuthKey = hashHMACSHA256(_psApiKey),
                DynamicTemplateData = dynamicTemplateData,
                RecipientEmail = RecipientEmail,
                TemplateId = TemplateId,
                SenderEmail = "info@believeglobal.org",
                SenderEmailName = "Believe Global"
            };

            string PostData = JsonConvert.SerializeObject(objMailApi, _serializerSettings);
            var URL = "https://api.planetshakers.com/API/ApiMail/SendMail";
            string ServerResponse = await getJSON(URL, PostData);

            if (ServerResponse == "ERROR. Signature Mismatch.")
            {
                throw new UnauthorizedAccessException(ServerResponse);
            }
            else if (ServerResponse != "\"ok\"")
            {
                throw new HttpException(ServerResponse);
            }
        }

        private string getContactEmail(int contact_id)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                return (from cc in context.Common_Contact
                        where cc.Contact_ID == contact_id
                        select cc.Email).First();
            }
        }

        private string getContactFullName(int contact_id)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                return (from cc in context.Common_Contact
                        where cc.Contact_ID == contact_id
                        select cc.FirstName + " " + cc.LastName).First();
            }
        }

        private string GetMessageHeader(RegistrationReceiptEmailModel model)
        {
            var header = new Header();

            header.AddSubstitution("-FullName-", new List<string> { model.FullName });
            header.AddSubstitution("-EventName-", new List<string> { model.Event.name });
            header.AddSubstitution("-ItemList-", new List<string> { model.ItemList });
            header.AddSubstitution("-Currency-", new List<string> { model.Event.currency });
            header.AddSubstitution("-TotalCost-", new List<string> { model.GrandTotal.ToString() });
            header.AddSubstitution("-EmailAddress-", new List<string> { model.Email });

            return header.JsonString();
        }

        #region Helpers
        private string hashHMACSHA256(string message)
        {
            ASCIIEncoding encoding = new ASCIIEncoding();

            byte[] byteSalt = encoding.GetBytes(_tokenSalt);
            byte[] byteMessage = encoding.GetBytes(message);

            HMACSHA256 hmacSHA256 = new HMACSHA256(byteSalt);
            byte[] hashMessage = hmacSHA256.ComputeHash(byteMessage);

            return BitConverter.ToString(hashMessage).Replace("-", "");
        }

        public async Task<string> getJSON(string URL, string jsondata)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            WebRequest json = WebRequest.Create(URL);
            byte[] postBytes = Encoding.UTF8.GetBytes(jsondata);
            json.Method = "POST";
            json.ContentType = "application/json";
            json.ContentLength = postBytes.Length;

            Stream requestStream = json.GetRequestStream();
            requestStream.Write(postBytes, 0, postBytes.Length);
            requestStream.Close();

            WebResponse Response = await json.GetResponseAsync();
            Stream stream = Response.GetResponseStream();
            StreamReader Reader = new System.IO.StreamReader(stream);
            string ServerResponse = "";

            using (Response)
            using (stream)
                ServerResponse = Reader.ReadToEnd();

            return ServerResponse;
        }

        private static string RenderViewToString(ControllerContext context,
                                    string viewPath,
                                    object model = null,
                                    bool partial = false)
        {
            // first find the ViewEngine for this view
            ViewEngineResult viewEngineResult = null;
            if (partial)
                viewEngineResult = ViewEngines.Engines.FindPartialView(context, viewPath);
            else
                viewEngineResult = ViewEngines.Engines.FindView(context, viewPath, null);

            if (viewEngineResult == null)
                throw new FileNotFoundException("View cannot be found.");

            // get the view and attach the model to view data
            var view = viewEngineResult.View;
            context.Controller.ViewData.Model = model;

            string result = null;

            using (var sw = new StringWriter())
            {
                var ctx = new ViewContext(context, view,
                                            context.Controller.ViewData,
                                            context.Controller.TempData,
                                            sw);
                view.Render(ctx, sw);
                result = sw.ToString();
            }

            return result;
        }

        private void CreateEmailLog(int contact_id, bool is_group_rego, string action, string item)
        {
            var cartModel = Session["Cart"] as CartModel;
            var user_id = CurrentContext.Instance.User.ContactID;

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var new_history = new Common_ContactHistory();
                new_history.Contact_ID = contact_id;
                new_history.Module = "Events";
                new_history.Desc = String.Empty;
                new_history.Desc2 = String.Empty;
                new_history.DateChanged = DateTime.Now;
                new_history.User_ID = user_id;
                new_history.Action = action;
                new_history.Item = item;
                new_history.Table = String.Empty;
                new_history.FieldName = String.Empty;
                new_history.OldValue = String.Empty;
                new_history.OldText = String.Empty;
                new_history.NewValue = String.Empty;
                new_history.NewText = String.Empty;
                context.Common_ContactHistory.Add(new_history);

                context.SaveChanges();
            }
        }
        #endregion
    }

    public class objApi
    {
        public string AuthKey { get; set; }
    }

    public class objMailApi : objApi
    {
        public string RecipientEmail { get; set; }
        public string TemplateId { get; set; }
        public object DynamicTemplateData { get; set; }
        public string SenderEmail { get; set; }
        public string SenderEmailName { get; set; }
        public string Attachment { get; set; }
        public string AttachmentFileName { get; set; }
    }


}
