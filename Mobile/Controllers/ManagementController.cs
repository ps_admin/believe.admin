﻿using Planetshakers.Core.DataInterfaces;
using Planetshakers.Core.Models;
using Planetshakers.Events.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Planetshakers.Events.Controllers
{
    [Authorize]
    public class ManagementController : Controller
    {

        private readonly IManagementRepository _repository;

        public ManagementController(IManagementRepository repository)
        {
            _repository = repository;
        }
        public ActionResult DatabaseRoleManagement()
        {
            DatabaseRoleManagementModel model = new DatabaseRoleManagementModel();
            return View("DatabaseRoleManagement", model);
        }

        [HttpPost]
        public ActionResult SaveDbRolePermission(int DbRolePermissionID, string DbObjectName, string DbObjectDesc, bool DbRolePermRead, bool DbRolePermWrite)
        {
            DatabaseRoleManagementModel model = new DatabaseRoleManagementModel();
            bool success = model.SaveDbRolePermission(DbRolePermissionID, DbObjectName, DbObjectDesc, DbRolePermRead, DbRolePermWrite);
            return Json(new { success });
        }

        [HttpPost]
        public ActionResult DeleteDbRole(int DbRoleID)
        {
            DatabaseRoleManagementModel model = new DatabaseRoleManagementModel();
            bool success = model.DeleteDbRole(DbRoleID);
            return Json(new { success });
        }

        public ActionResult AddDbRolePage()
        {
            DatabaseRoleManagementModel model = new DatabaseRoleManagementModel();
            return View("AddDatabaseRole", model);
        }

        
        [HttpPost]
        public ActionResult AddDbRole(string DbRoleName)
        {
            DatabaseRoleManagementModel model = new DatabaseRoleManagementModel();
            bool success = model.AddDbRole(DbRoleName);
            return Json(new {success });
        }

        [HttpPost]
        public ActionResult SaveNewDbRolePermission(int DbObjID, bool DbRolePermRead, bool DbRolePermWrite)
        {
            DatabaseRoleManagementModel model = new DatabaseRoleManagementModel();
            bool success = model.SaveNewDbRolePermission(DbObjID, DbRolePermRead, DbRolePermWrite);
            return Json(new { success });
        }


        public ActionResult AccessLevel()
        {
            DatabaseRoleManagementModel model = new DatabaseRoleManagementModel();
            model.GenerateResult = false;
            return View("AccessLevel", model);
        }

        public ActionResult GenerateAccessLevelPermissionSearchResult(DatabaseRoleManagementModel model)
        {
            model.GenerateResult = true;
            return View("AccessLevel", model);
        }

        public ActionResult GenerateContactAccessLevelPermission(DatabaseRoleManagementModel model)
        {
            model.GenerateResult = true;
            model.userDbPermDictionary = new Dictionary<int, List<AccessLevelPermission>>();
            model.userDbRoleDictionary = new Dictionary<int, List<ContactAccessLevel>>();
            foreach (var user in model.searchedResultContactList)
            {
                List<AccessLevelPermission> userdbroleperm = new List<AccessLevelPermission>();
                List<ContactAccessLevel> userRole = new List<ContactAccessLevel>();
                userdbroleperm = _repository.GetContactAccessLevelPermission(user.Contact_ID);
                model.userDbPermDictionary.Add(user.Contact_ID, userdbroleperm);
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    userRole = (from cdr in context.Common_ContactDatabaseRole
                                join dbr in context.Common_DatabaseRole
                                on cdr.DatabaseRole_ID equals dbr.DatabaseRole_ID
                                where cdr.Contact_ID == user.Contact_ID && dbr.Module == "Believe"
                                select new ContactAccessLevel
                                {
                                    ContactId = user.Contact_ID,
                                    DbRoleId = cdr.DatabaseRole_ID,
                                    Name = dbr.Name,
                                    Module = dbr.Module,
                                    DateAdded = cdr.DateAdded
                                }).ToList();
                    model.userDbRoleDictionary.Add(user.Contact_ID, userRole);
                }
            }
            return View("AccessLevel", model);
        }

        public ActionResult DeleteContactAccessLevel(int DbRoleID, int ContactID)
        {
            DatabaseRoleManagementModel model = new DatabaseRoleManagementModel();
            bool success = model.DeleteContactAccessLevel(DbRoleID, ContactID);
            return Json(new { success });
        }

        public ActionResult AddContactAccessLevel(int DbRoleID, int ContactID)
        {
            DatabaseRoleManagementModel model = new DatabaseRoleManagementModel();
            bool success = model.AddContactAccessLevel(DbRoleID, ContactID);
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var dbRole = (from cdr in context.Common_ContactDatabaseRole
                              join dr in context.Common_DatabaseRole
                              on cdr.DatabaseRole_ID equals dr.DatabaseRole_ID
                              where cdr.Contact_ID == ContactID && cdr.DatabaseRole_ID == DbRoleID
                              select dr).FirstOrDefault();
                return Json(new { success, dbRoleID = DbRoleID, contactID = ContactID, addedDate = DateTime.Now.ToString(), dbRoleName = dbRole.Name });
            } 
            
        }

        public ActionResult UpdateAccessLevelReadPermission(int DbObjID, int ContactID, bool Read)
        {
            DatabaseRoleManagementModel model = new DatabaseRoleManagementModel();
            bool success = model.UpdateAccessLevelReadPermission( DbObjID,  ContactID,  Read);
            return Json(new { success });
        }

        public ActionResult UpdateAccessLevelWritePermission(int DbObjID, int ContactID, bool Write)
        {
            DatabaseRoleManagementModel model = new DatabaseRoleManagementModel();
            bool success = model.UpdateAccessLevelWritePermission(DbObjID, ContactID, Write);
            return Json(new { success });
        }

        public ActionResult AddContactAccessLevelReadWritePermission(int DbObjID, int ContactID, bool ReadCheck, bool WriteCheck)
        {
            DatabaseRoleManagementModel model = new DatabaseRoleManagementModel();
            bool success = model.AddContactAccessLevelReadWritePermission(DbObjID, ContactID, ReadCheck, WriteCheck);
            return Json(new { success });
        }
    }
}