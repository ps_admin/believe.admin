﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Planetshakers.Events.Models;
using Planetshakers.Events.Security;
using Stripe;
using Stripe.Checkout;

namespace Planetshakers.Events.Controllers
{

    public class PaymentController : Controller
    {

        public ActionResult Summary(string returnUrl, string error = null)
        {
            var model = Session["Cart"] as CartModel;

            if (model == null)
            {
                model = new CartModel()
                {
                    Cart = null
                };
            }

            var contact = CurrentContext.Instance.User.ContactID;

            if (contact == 0)
                return RedirectToAction("LogOn", "Account");


            // Setup the CCPayment model            
            if (model.CCPayment == null) model.CCPayment = new CCPaymentModel();

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var bankAccount = new Common_BankAccount();

                /* Retrieve Bank Account information to transfer funds into */
                bankAccount = (from ba in context.Common_BankAccount
                               join ec in context.Events_Conference
                               on ba.BankAccount_ID equals ec.BankAccount_ID
                               where ec.Conference_ID == model.Event.id
                               select ba).FirstOrDefault();

                model.CCPayment.BankAccount = bankAccount;
            }
            //check for grandtotal and voucher value 
            if (model.GrandTotal <= 0 && !String.IsNullOrEmpty(model.Voucher.code))
            {
                model.CCPayment.payment_id = InsertPayment(String.Empty, true);

                Session["Cart"] = model;
                Session["TransactionReference"] = null;
                Session["ClientSecret"] = null;
            }
            else if (model.GrandTotal > 0)
            {
                StripeConfiguration.ApiKey = model.CCPayment.BankAccount.SecretKey;

                var invoice = createStripePayment(model);

                Session["TransactionReference"] = invoice.PaymentIntentId;

                // remove error message for session model
                Session["Cart"] = model;

                model.ErrorMessage = error;

                Session["ClientSecret"] = invoice.PaymentIntent.ClientSecret;

                ViewBag.StripePublishableKey = model.CCPayment.BankAccount.PublishableKey;
            }

            return View(model);
        }

        public ActionResult addRegistration(int venue_id, int contact_id)
        {
            var cartModel = Session["Cart"] as CartModel;
            var transactionReference = Session["transactionReference"] as String;

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                // Retrieve contact details of purchase person
                var contact = (from cc in context.Common_Contact
                               where cc.Contact_ID == contact_id
                               select cc).FirstOrDefault();

                var conference_id = (from v in context.Events_Venue
                                     where v.Venue_ID == venue_id
                                     select v.Conference_ID).FirstOrDefault();

                var is_group_rego = true;

                // This needs a better way to distinguish source of payment
                // 1 - use string parameter (cartType)
                // If cart only has 1 item, it goes into group payment
                if (cartModel.Cart.Any(x => x.reg_id > 0))
                {
                    // Insert payment and registration into database
                    InsertEventRegistration(is_group_rego);
                    var payment_id = markPaymentCompleted(cartModel.CCPayment.payment_id, transactionReference, "purchase");
                    
                    SendEventsEmail(payment_id, is_group_rego);

                    // Clear the Cart
                    Session["Cart"] = null;

                    // Force single registrations to reload
                    Session["registrations_single"] = null;
                    TempData["recently_registered"] = true;

                    // Update session.
                    Session["Cart"] = cartModel;
                    Session["success"] = "Payment and event registration is successful.";

                    if (String.IsNullOrEmpty(transactionReference) && cartModel.Voucher != null && cartModel.GrandTotal <= 0)
                    {
                        return RedirectToAction("RegistrationSuccess", "Administration");
                    } else
                    {
                        return Json(
                            new
                            {
                                message = "Registration and payment completed",
                                link = Url.Action("RegistrationSuccess", "Administration")
                            });
                    }
                }
                else
                {
                    var payment_id = markPaymentCompleted(cartModel.CCPayment.payment_id, transactionReference, "purchase");

                    
                    SendEventsEmail(payment_id, is_group_rego);

                    // Clear the Cart
                    Session["Cart"] = null;

                    // Force single registrations to reload
                    Session["registrations_single"] = null;
                    TempData["recently_registered"] = true;

                    // Update session.
                    Session["Cart"] = cartModel;
                    Session["success"] = "Payment and event registration is successful.";

                    if (String.IsNullOrEmpty(transactionReference) && cartModel.Voucher != null && cartModel.GrandTotal <= 0)
                    {
                        return RedirectToAction("GroupDetails", "Registration", new { Group_ID = contact_id, Conference_ID = conference_id });
                    }
                    else
                    {
                        return Json(
                            new
                            {
                                message = "Registration and payment completed",
                                link = Url.Action("GroupDetails", "Registration", new { Group_ID = contact_id, Conference_ID = conference_id })
                            });
                    }
                }
            }
        }
        public Invoice createStripePayment(CartModel model)
        {
            // create stripe customer
            var customerOptions = new CustomerCreateOptions
            {
                Email = model.Email,
                Name = model.FullName
            };

            var customerService = new CustomerService();
            var customer = customerService.Create(customerOptions);

            foreach (var item in model.Cart)
            {
                // create stripe product
                var productOptions = new ProductCreateOptions
                {
                    Name = model.Event.name + " - " + item.reg_name,
                    Description = model.Event.name
                };

                var productService = new ProductService();
                var product = productService.Create(productOptions);

                // create stripe invoice item
                var invoiceItemOptions = new InvoiceItemCreateOptions
                {
                    Customer = customer.Id,
                    PriceData = new InvoiceItemPriceDataOptions
                    {
                        Currency = model.CCPayment.BankAccount.Currency,
                        Product = product.Id,
                        UnitAmount = Convert.ToInt32(item.single_price * 100)
                    },
                    Quantity = item.quantity
                };

                var invoiceItemService = new InvoiceItemService();
                var invoiceItem = invoiceItemService.Create(invoiceItemOptions);
            }

            // create stripe invoice
            var invoiceOptions = new InvoiceCreateOptions
            {
                Customer = customer.Id,
                Description = model.Event.name,
                AutoAdvance = false
            };

            var invoiceService = new InvoiceService();
            var invoice = invoiceService.Create(invoiceOptions);

            // finalise invoice
            var invoiceFinalizeOptions = new InvoiceFinalizeOptions();
            invoice = invoiceService.FinalizeInvoice(invoice.Id, invoiceFinalizeOptions);

            // get paymentintent for invoice
            var service = new PaymentIntentService();
            invoice.PaymentIntent = service.Get(invoice.PaymentIntentId);

            // add details to payment intent
            var paymentIntentOptions = new PaymentIntentUpdateOptions
            {
                Description = model.Event.name,
                Metadata = new Dictionary<string, string>
                {
                    { "integration_check", "accept_a_payment" }
                }
            };

            service.Update(invoice.PaymentIntentId, paymentIntentOptions);

            model.CCPayment.transactionReference = invoice.PaymentIntentId;

            model.CCPayment.payment_id = InsertPayment(model.CCPayment.transactionReference, true);

            return invoice;
        }

        #region Private Functions
        private Common_Contact getContactDetails()
        {
            // Retrieve logged in contact from session

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                // Retrieve contact details (if available)
                var ps_contact = (from cc in context.Common_Contact
                                  where cc.Contact_ID == CurrentContext.Instance.User.ContactID
                                  select cc).First();
                return ps_contact;
            }
        }

        private string markPaymentCompleted(int payment_id, string transactionRef, string type)
        {
            using (var context = new PlanetshakersEntities())
            {

                if (type == "sponsorship")
                {
                    var sponsor = new Events_VariablePayment();
                    if (!String.IsNullOrEmpty(transactionRef))
                    {
                        sponsor = (from vp in context.Events_VariablePayment
                                   where vp.CCTransactionReference == transactionRef
                                   select vp).FirstOrDefault();
                    }
                    else
                    {
                        sponsor = (from vp in context.Events_VariablePayment
                                   where vp.VariablePayment_ID == payment_id
                                   select vp).FirstOrDefault();
                    }

                    if (sponsor != null)
                    {
                        sponsor.PaymentCompletedDate = DateTime.Now;
                        sponsor.PaymentCompleted = true;

                        context.SaveChanges();
                    }
                    else
                    {
                        throw new Exception();
                    }

                    return sponsor.VariablePayment_ID.ToString();
                }
                else if (type == "purchase")
                {
                    var payment = new Events_GroupPayment();

                    if (!String.IsNullOrEmpty(transactionRef))
                    {
                        payment = (from gp in context.Events_GroupPayment
                                   where gp.CCTransactionRef == transactionRef
                                   select gp).FirstOrDefault();
                    }
                    else
                    {
                        payment = (from gp in context.Events_GroupPayment
                                   where gp.GroupPayment_ID == payment_id
                                   select gp).FirstOrDefault();
                    }

                    if (payment != null)
                    {
                        payment.PaymentCompletedDate = DateTime.Now;
                        payment.PaymentCompleted = true;

                        context.SaveChanges();
                    }
                    else
                    {
                        throw new Exception();
                    }

                    return payment.GroupPayment_ID.ToString();
                }

                return String.Empty;
            }
        }


        private void SendEventsEmail(string payment_id, bool is_group_rego)
        {
            var user_id = CurrentContext.Instance.User.ContactID;

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                // Retrieve some cart data from session
                var cartModel = Session["Cart"] as CartModel;

                var contact = (from cc in context.Common_Contact
                               where cc.Contact_ID == cartModel.ContactId
                               select cc).FirstOrDefault();

                var email_model = new RegistrationReceiptEmailModel()
                {
                    Email = contact.Email,
                    ContactId = contact.Contact_ID,
                    Event = cartModel.Event,
                    Items = new List<RegistrationConfirmationItem>(),
                    EventName = cartModel.Event.name,
                    EventDate = cartModel.Event.date,
                    EventVenue = cartModel.Event.location,
                    Currency = cartModel.Event.currency,
                    FullName = contact.FirstName + " " + contact.LastName,
                    Subject = "Registration Confirmation - " + cartModel.Event.name,
                    ReceiptNumber = payment_id,
                    PaymentType = (cartModel.CCPayment != null ? cartModel.CCPayment.PaymentType : "N/A"),
                    TransactionReference = (cartModel.CCPayment != null ? (!String.IsNullOrEmpty(cartModel.CCPayment.transactionReference) ? cartModel.CCPayment.transactionReference : "N/A") : "N/A"),
                    Promo = (cartModel.Voucher != null ? cartModel.Voucher : null)
                };

                // create an entry for the registration
                foreach (CartItem item in cartModel.Cart)
                {
                    email_model.Items.Add(new RegistrationConfirmationItem
                    {
                        reference_number = payment_id,
                        reg_name = item.reg_name,
                        qty = item.quantity,
                        price = item.single_price
                    });
                }

                // Prepare to send 
                if (!String.IsNullOrEmpty(email_model.Email))
                {
                    new MailController().RegistrationReceipt(email_model);
                    CreateEventLog(email_model.ContactId, is_group_rego, "Update", "Sent Tax Invoice Email");
                }
            }
        }

        private void CreateEventLog(int contact_id, bool is_group_rego, string action, string item)
        {
            var cartModel = Session["Cart"] as CartModel;
            var user_id = CurrentContext.Instance.User.ContactID;

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                
                var new_history = new Common_ContactHistory();
                new_history.Contact_ID = contact_id;
                new_history.Module = "Events";
                new_history.Desc = String.Empty;
                new_history.Desc2 = String.Empty;
                new_history.DateChanged = DateTime.Now;
                new_history.User_ID = user_id;
                new_history.Action = action;
                new_history.Item = item;
                new_history.Table = String.Empty;
                new_history.FieldName = String.Empty;
                new_history.OldValue = String.Empty;
                new_history.OldText = String.Empty;
                new_history.NewValue = String.Empty;
                new_history.NewText = String.Empty;
                context.Common_ContactHistory.Add(new_history);

                context.SaveChanges();
            }
        }

        private void InsertEventRegistration(bool isGroup)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (PlanetshakersEntities context = new PlanetshakersEntities())
                    {
                        var cartModel = Session["Cart"] as CartModel;

                        Common_ContactExternalEvents cee = (from cee1 in context.Common_ContactExternalEvents
                                                            where cee1.Contact_ID == cartModel.ContactId
                                                            select cee1).FirstOrDefault();
                        if (cee == null)
                        {
                            cee = new Common_ContactExternalEvents();
                            cee.Contact_ID = cartModel.ContactId;
                            cee.EmergencyContactName = "";
                            cee.EmergencyContactPhone = "";
                            cee.EmergencyContactRelationship = "";
                            cee.MedicalAllergies = "";
                            cee.MedicalInformation = "";
                            cee.AccessibilityInformation = "";
                            cee.EventComments = "";
                            context.Common_ContactExternalEvents.Add(cee);
                            context.SaveChanges();
                        }

                        Common_Contact cc = (from cc1 in context.Common_Contact
                                             where cc1.Contact_ID == cartModel.ContactId
                                             select cc1).FirstOrDefault();

                        #endregion

                        if (isGroup)
                        {
                            #region Create Group (if doesn't exist)
                            var isGroupLeader = ((from eg in context.Events_Group
                                                  where eg.GroupLeader_ID == cartModel.ContactId
                                                  select eg).FirstOrDefault() != null ? true : false);

                            if (!isGroupLeader)
                            {
                                var new_group = new Events_Group();
                                new_group.GroupLeader_ID = cartModel.ContactId;
                                new_group.GroupName = cc.FirstName + " " + cc.LastName;
                                new_group.GroupColeaders = String.Empty;
                                new_group.YouthLeaderName = String.Empty;
                                new_group.GroupSize_ID = null;
                                new_group.GroupCreditStatus_ID = 1;
                                new_group.GroupType_ID = 2;
                                new_group.Comments = String.Empty;
                                new_group.DetailsVerified = false;
                                new_group.Deleted = false;
                                new_group.CreationDate = DateTime.Now;
                                new_group.CreatedBy_ID = CurrentContext.Instance.User.ContactID;
                                new_group.ModificationDate = null;
                                new_group.ModifiedBy_ID = null;
                                new_group.GUID = Guid.NewGuid();

                                context.Events_Group.Add(new_group);
                                context.SaveChanges();
                            }

                            #endregion

                            #region Insert Group Bulk Registrations into database

                            foreach (var i in cartModel.Cart)
                            {
                                var new_bulk_rego = new Events_GroupBulkRegistration();
                                new_bulk_rego.GroupLeader_ID = cartModel.ContactId;
                                new_bulk_rego.Venue_ID = cartModel.Event.venue_id;
                                new_bulk_rego.RegistrationType_ID = i.reg_id;
                                new_bulk_rego.Quantity = i.quantity;
                                new_bulk_rego.DateAdded = DateTime.Now;

                                context.Events_GroupBulkRegistration.Add(new_bulk_rego);
                                context.SaveChanges();
                            }

                            #endregion

                            #region Convert old single registrations to group

                            // check for previous registrations
                            var single_regos = (from r in context.Events_Registration
                                                where r.Contact_ID == cartModel.ContactId &&
                                                      r.GroupLeader_ID == null &&
                                                      r.Venue_ID == cartModel.Event.venue_id
                                                select r).ToList();

                            if (single_regos.Any())
                            {
                                // retrieve registration types
                                var rego_types = single_regos.Select(x => x.RegistrationType_ID)
                                                             .Distinct().ToList();
                                // Create bulk registrations
                                foreach (var rego_type in rego_types)
                                {
                                    // Retrieve registration date
                                    var reg_date = single_regos
                                                   .Where(x => x.RegistrationType_ID == rego_type)
                                                   .Select(x => x.RegistrationDate).First();

                                    // retrieve number of rego
                                    var qty = single_regos.Where(x => x.RegistrationType_ID == rego_type).Count();

                                    // create regos
                                    var new_bulk_regos = new Events_GroupBulkRegistration();
                                    new_bulk_regos.GroupLeader_ID = cartModel.ContactId;
                                    new_bulk_regos.Venue_ID = cartModel.Event.venue_id;
                                    new_bulk_regos.RegistrationType_ID = rego_type;
                                    new_bulk_regos.Quantity = qty;
                                    new_bulk_regos.DateAdded = reg_date;

                                    context.Events_GroupBulkRegistration.Add(new_bulk_regos);
                                    context.SaveChanges();
                                }

                            }

                            #endregion
                        }
                        else
                        {
                            #region Create New Registration or Add To Bulk Registration
                            var isRegistered = ((from er in context.Events_Registration
                                                 where er.Contact_ID == cartModel.ContactId
                                                         && er.Venue_ID == cartModel.Event.venue_id
                                                 select er).FirstOrDefault() != null ? true : false);

                            if (!isRegistered)
                            {
                                foreach (var i in cartModel.Cart)
                                {
                                    var new_rego = new Events_Registration();
                                    new_rego.Contact_ID = cartModel.ContactId;
                                    new_rego.GroupLeader_ID = null;
                                    new_rego.FamilyRegistration = false;
                                    new_rego.RegisteredByFriend_ID = null;
                                    new_rego.Venue_ID = cartModel.Event.venue_id;
                                    new_rego.RegistrationType_ID = i.reg_id;
                                    new_rego.RegistrationDiscount = 0;
                                    new_rego.Elective_ID = null;
                                    new_rego.Accommodation = false;
                                    new_rego.Accommodation_ID = null;
                                    new_rego.Catering = false;
                                    new_rego.CateringNeeds = "";
                                    new_rego.LeadershipBreakfast = false;
                                    new_rego.LeadershipSummit = false;
                                    new_rego.ULG_ID = null;
                                    new_rego.ParentGuardian = "";
                                    new_rego.ParentGuardianPhone = "";
                                    new_rego.AcceptTermsRego = true;
                                    new_rego.AcceptTermsParent = false;
                                    new_rego.AcceptTermsCreche = false;
                                    new_rego.RegistrationDate = DateTime.Now;
                                    new_rego.RegisteredBy_ID = cartModel.ContactId;
                                    new_rego.Attended = false;
                                    new_rego.Volunteer = false;
                                    new_rego.VolunteerDepartment_ID = null;
                                    new_rego.ComboRegistration_ID = null;

                                    context.Events_Registration.Add(new_rego);
                                }

                                context.SaveChanges();
                            }
                            else
                            {
                                var isGroupLeader = ((from eg in context.Events_Group
                                                      where eg.GroupLeader_ID == cartModel.ContactId
                                                      select eg).FirstOrDefault() != null ? true : false);

                                if (!isGroupLeader)
                                {
                                    var new_group = new Events_Group();
                                    new_group.GroupLeader_ID = cartModel.ContactId;
                                    new_group.GroupName = cc.FirstName + " " + cc.LastName;
                                    new_group.GroupColeaders = String.Empty;
                                    new_group.YouthLeaderName = String.Empty;
                                    new_group.GroupSize_ID = null;
                                    new_group.GroupCreditStatus_ID = 1;
                                    new_group.GroupType_ID = 2;
                                    new_group.Comments = String.Empty;
                                    new_group.DetailsVerified = false;
                                    new_group.Deleted = false;
                                    new_group.CreationDate = DateTime.Now;
                                    new_group.CreatedBy_ID = CurrentContext.Instance.User.ContactID;
                                    new_group.ModificationDate = null;
                                    new_group.ModifiedBy_ID = null;
                                    new_group.GUID = Guid.NewGuid();

                                    context.Events_Group.Add(new_group);
                                }

                                context.SaveChanges();

                                foreach (var i in cartModel.Cart)
                                {
                                    var new_bulk_rego = new Events_GroupBulkRegistration();
                                    new_bulk_rego.GroupLeader_ID = cartModel.ContactId;
                                    new_bulk_rego.Venue_ID = cartModel.Event.venue_id;
                                    new_bulk_rego.RegistrationType_ID = i.reg_id;
                                    new_bulk_rego.Quantity = i.quantity;
                                    new_bulk_rego.DateAdded = DateTime.Now;

                                    context.Events_GroupBulkRegistration.Add(new_bulk_rego);
                                }

                                context.SaveChanges();
                            }
                            #endregion    
                        }
                    }
                }
                catch (Exception e)
                {
                    ViewBag.ErrorMessage = "Error creating registrations";
                    throw e;
                }
            }
        }

        private int InsertPayment(string transactionRef, bool isGroup)
        {
            var payment_id = 0;

            if (ModelState.IsValid)
            {
                try
                {
                    using (PlanetshakersEntities context = new PlanetshakersEntities())
                    {
                        var cartModel = Session["Cart"] as CartModel;

                        var bank_account_id = (from c in context.Events_Conference
                                               where c.Conference_ID == cartModel.Event.id
                                               select c.BankAccount_ID).FirstOrDefault();

                        #region Create Contact External (if doesn't exist)
                       
                        Common_ContactExternalEvents cee = (from cee1 in context.Common_ContactExternalEvents
                                                            where cee1.Contact_ID == cartModel.ContactId
                                                            select cee1).FirstOrDefault();
                        if (cee == null)
                        {
                            cee = new Common_ContactExternalEvents();
                            cee.Contact_ID = cartModel.ContactId;
                            cee.EmergencyContactName = "";
                            cee.EmergencyContactPhone = "";
                            cee.EmergencyContactRelationship = "";
                            cee.MedicalAllergies = "";
                            cee.MedicalInformation = "";
                            cee.AccessibilityInformation = "";
                            cee.EventComments = "";
                            context.Common_ContactExternalEvents.Add(cee);
                            context.SaveChanges();
                        }

                        Common_Contact cc = (from cc1 in context.Common_Contact
                                             where cc1.Contact_ID == cartModel.ContactId
                                             select cc1).FirstOrDefault();

                        #endregion

                        if (isGroup)
                        {
                            #region Create Group (if doesn't exist)
                            var isGroupLeader = ((from eg in context.Events_Group
                                                  where eg.GroupLeader_ID == cartModel.ContactId
                                                  select eg).FirstOrDefault() != null ? true : false);

                            if (!isGroupLeader)
                            {
                                var new_group = new Events_Group();
                                new_group.GroupLeader_ID = cartModel.ContactId;
                                new_group.GroupName = cc.FirstName + " " + cc.LastName;
                                new_group.GroupColeaders = String.Empty;
                                new_group.YouthLeaderName = String.Empty;
                                new_group.GroupSize_ID = null;
                                new_group.GroupCreditStatus_ID = 1;
                                new_group.GroupType_ID = 2;
                                new_group.Comments = String.Empty;
                                new_group.DetailsVerified = false;
                                new_group.Deleted = false;
                                new_group.CreationDate = DateTime.Now;
                                new_group.CreatedBy_ID = getContactDetails().Contact_ID;
                                new_group.ModificationDate = null;
                                new_group.ModifiedBy_ID = null;
                                new_group.GUID = Guid.NewGuid();

                                context.Events_Group.Add(new_group);
                                context.SaveChanges();
                            }

                            #endregion

                            #region Insert Group Payment Log into database

                            var paymenttype_id = 3; //Credit Card
                            var paymentsource_id = 2; //Payment 
                            payment_id = createGroupPayment(cartModel, paymenttype_id, paymentsource_id, transactionRef);

                            cartModel.CCPayment.paymenttype_id = paymenttype_id;
                            #endregion

                            Session["Cart"] = cartModel;
                        }
                    }
                }
                catch (Exception e)
                {
                    ViewBag.ErrorMessage = "Error creating payment log";
                    throw e;
                }
            }

            return payment_id;
        }

        public int createGroupPayment(CartModel model, int paymenttype_id, int paymentsource_id, string transactionRef)
        {
            using (var ctx = new PlanetshakersEntities())
            {
                var new_payment = new Events_GroupPayment();

                new_payment.GroupLeader_ID = model.ContactId;
                new_payment.Conference_ID = model.Event.id;

                new_payment.PaymentSource_ID = paymentsource_id;
                new_payment.PaymentType_ID = paymenttype_id;
                new_payment.PaymentAmount = model.GrandTotal;

                new_payment.CCTransactionRef = transactionRef;
                new_payment.Comment = String.Empty;
                new_payment.PaymentStartDate = DateTime.Now;
                new_payment.PaymentCompleted = false;

                new_payment.CCNumber = String.Empty;
                new_payment.CCExpiry = String.Empty;
                new_payment.CCName = String.Empty;
                new_payment.CCPhone = String.Empty;
                new_payment.CCManual = false;
                new_payment.CCRefund = false;
                new_payment.ChequeDrawer = String.Empty;
                new_payment.ChequeBank = String.Empty;
                new_payment.ChequeBranch = String.Empty;
                new_payment.PaypalTransactionRef = String.Empty;
                new_payment.PaymentBy_ID = model.ContactId;
                new_payment.BankAccount_ID = model.CCPayment.BankAccount.BankAccount_ID;

                ctx.Events_GroupPayment.Add(new_payment);
                ctx.SaveChanges();

                return new_payment.GroupPayment_ID;
            }
        }

        [HttpPost]
        public ActionResult loadStripePayment()
        {
            var model = Session["Cart"] as CartModel;
            var transactionReference = Session["transactionReference"] as String;
            var stripeBypass = false;

            stripeBypass = model.Voucher != null && String.IsNullOrEmpty(transactionReference) && model.GrandTotal == 0;

            ViewData["Cart"] = model;
            ViewData["ClientSecret"] = Session["ClientSecret"];

            if (stripeBypass && Session["ClientSecret"] == null)
            {
                Session["transactionReference"] = null;
                return PartialView("_CheckoutButton", model);
            }
            else
            {
                return PartialView("_StripePayment", model);
            }
        }

        [HttpPost]
        public ActionResult Checkout()
        {
            var model = Session["Cart"] as CartModel;

            var contact = getContactDetails();

            return RedirectToAction("addRegistration", new { venue_id = model.Event.venue_id, contact_id = contact.Contact_ID });
        }
    }
}