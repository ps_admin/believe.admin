﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Planetshakers.Events.Models;
using Planetshakers.Core.Models;
using Planetshakers.Core.DataInterfaces;
using System.Web.Mvc;
using Planetshakers.Events.Security;
using Stripe;
using Stripe.Checkout;
using System.Threading.Tasks;
using System.Data.Linq.Mapping;

namespace Planetshakers.Events.Controllers
{
    [Authorize]
    public class RegistrationController : Controller
    {
        private readonly IRegistrationRepository _repository;
        public RegistrationController(IRegistrationRepository repository)
        {
            _repository = repository;
        }
        public RegistrationController() { }

        public ActionResult RegistrationIndex()
        {
            return View();
        }


        #region Single Registration
        // View for single registrations search
        //public ActionResult SingleRegistration()
        //{
        //    var model = new SearchViewModel();

        //    using (PlanetshakersEntities context = new PlanetshakersEntities())
        //    {
        //        model.Search = new SearchModel();
        //        model.Search.Conference_ID = (from ec in context.Events_Conference
        //                                      join ev in context.Events_Venue
        //                                      on ec.Conference_ID equals ev.Conference_ID
        //                                      where ev.ConferenceEndDate >= DateTime.Today
        //                                      orderby ec.SortOrder
        //                                      select ec.Conference_ID).FirstOrDefault();
        //    }

        //    return View(model);
        //}

        //public ActionResult SearchSingleContact(string query, int Conference_ID)
        //{
        //    SearchViewModel model = new SearchViewModel();
        //    model.Search = new SearchModel();
        //    model.Search.QuickSearch = query;
        //    model.Search.Conference_ID = Conference_ID;
        //    return SingleSearch(model);
        //}

        //[HttpPost]
        //public ActionResult SingleSearch(SearchViewModel model)
        //{
        //    model.Results = new ResultsModel();

        //    model.Results.Headers = new List<string> {
        //        "ID",
        //        "First Name",
        //        "Last Name",
        //        "Mobile",
        //        "Email",
        //        "Gender",
        //        "Group Name",
        //        "Registered"
        //    };

        //    model.Results.SingleRego = _repository.FindSingle(model.Search);

        //    model.Conference_ID = model.Search.Conference_ID;

        //    return View("SingleRegistration", model);
        //}
                

        //public ActionResult ContactDetails(int Contact_ID, int Conference_ID)
        //{
        //    var model = new ContactViewModel(Contact_ID, Conference_ID);
        //    var conf = new Conference();
        //    var regos = new List<Registration>();
        //    var payments = new List<Payment>();
        //    var comments = new List<CommentItem>();

        //    using (PlanetshakersEntities context = new PlanetshakersEntities())
        //    {
        //        if (Conference_ID > 0)
        //        {
        //            conf = (from ec in context.Events_Conference
        //                    join ev in context.Events_Venue
        //                    on ec.Conference_ID equals ev.Conference_ID
        //                    where ec.Conference_ID == Conference_ID
        //                    select new Conference
        //                    {
        //                        ID = ec.Conference_ID,
        //                        ConferenceName = ec.ConferenceName
        //                    }).FirstOrDefault();

        //            model.Conference = conf;
        //        }

        //        if (Contact_ID > 0)
        //        {
        //            // UPDATE: Coverted all possible null values into left outer joins
        //            model.Contact = (from cc in context.Common_Contact
        //                             join cce in context.Common_ContactExternalEvents
        //                             on cc.Contact_ID equals cce.Contact_ID
        //                             join cs in context.Common_Salutation
        //                             on cc.Salutation_ID equals cs.Salutation_ID into cs2
        //                             from cs in cs2.DefaultIfEmpty()
        //                             join gs in context.Common_GeneralState
        //                             on cc.State_ID equals gs.State_ID into gs2
        //                             from gs in gs2.DefaultIfEmpty()
        //                             join gc in context.Common_GeneralCountry
        //                             on cc.Country_ID equals gc.Country_ID into gc2
        //                             from gc in gc2.DefaultIfEmpty()
        //                             where cc.Contact_ID == Contact_ID
        //                             select new ContactDetails
        //                             {
        //                                 Id = cc.Contact_ID,
        //                                 Salutation = cs.Salutation,
        //                                 FirstName = cc.FirstName,
        //                                 LastName = cc.LastName,
        //                                 Password = cc.Password,
        //                                 Email = cc.Email,
        //                                 ContactNumber = cc.Mobile,
        //                                 Gender = cc.Gender,
        //                                 AddressLine1 = cc.Address1,
        //                                 AddressLine2 = cc.Address2,
        //                                 Suburb = cc.Suburb,
        //                                 State = gs.State,
        //                                 Postcode = cc.Postcode,
        //                                 Country = gc.Country,
        //                                 MedicalInformation = cce.MedicalInformation,
        //                                 MedicalAllergies = cce.MedicalAllergies
        //                             }).FirstOrDefault();
        //        }

        //        if (Contact_ID > 0 && Conference_ID > 0)
        //        {
        //            regos = (from er in context.Events_Registration
        //                    join ert in context.Events_RegistrationType
        //                    on er.RegistrationType_ID equals ert.RegistrationType_ID
        //                    join ev in context.Events_Venue
        //                    on er.Venue_ID equals ev.Venue_ID
        //                    where er.Contact_ID == Contact_ID && ev.Conference_ID == Conference_ID
        //                    select new Registration
        //                    {
        //                        ID = er.Registration_ID,
        //                        VenueId = er.Venue_ID,
        //                        Conference_ID = ev.Conference_ID,
        //                        RegistrationDate = er.RegistrationDate,
        //                        RegoType_ID = ert.RegistrationType_ID,
        //                        RegoType = ert.RegistrationType,
        //                        Attended = er.Attended,
        //                        IsGroupLeader = (er.GroupLeader_ID == Contact_ID) ? true : false,
        //                        RegoDiscount = er.RegistrationDiscount,
        //                        Volunteer = er.Volunteer,
        //                        VolunteerDepartment_ID = er.VolunteerDepartment_ID,
        //                        GroupLeader_ID = er.GroupLeader_ID,
        //                        HasGroup = (er.GroupLeader_ID != null) ? true : false,
        //                        RegisteredBy_ID = er.RegisteredBy_ID
        //                    }).ToList();

        //            if (regos.Any())
        //            {
        //                foreach (Registration r in regos)
        //                {
        //                    r.RegisteredBy = (from cc in context.Common_Contact
        //                                      where cc.Contact_ID == r.RegisteredBy_ID
        //                                      select cc.FirstName + " " + cc.LastName).FirstOrDefault();
        //                }
        //            }

        //            model.Registrations = regos;

        //            comments = (from gc in context.Events_ContactComment
        //                        join cc in context.Common_Contact
        //                        on gc.CommentBy equals cc.Contact_ID
        //                        where gc.Contact_ID == model.Contact.Id && gc.Conference_ID == model.Conference.ID
        //                        select new CommentItem
        //                        {
        //                            Id = gc.ContactComment_ID,
        //                            Conference_ID = gc.Conference_ID,
        //                            Comment = gc.Comment,
        //                            CommentDate = gc.Date,
        //                            Commenter_ID = gc.CommentBy,
        //                            CommenterFirstName = cc.FirstName,
        //                            CommenterLastName = cc.LastName
        //                        }).ToList();

        //            model.CommentList = comments;

        //            var groupsingle = "Single";

        //            model.OutstandingBalance = _repository.GetOutstandingBalance(groupsingle, Contact_ID, Conference_ID);
        //        }
        //    }

        //    return View(model);
        //}

        //public ActionResult ContactHistory(int Contact_ID)
        //{
        //    var ch = new List<ContactHistory>();

        //    var modules = new List<string> { "Events", "Common" };

        //    using (PlanetshakersEntities context = new PlanetshakersEntities())
        //    {
        //        ch = (from cch in context.Common_ContactHistory
        //              join cc in context.Common_Contact
        //              on cch.User_ID equals cc.Contact_ID
        //              where cch.Contact_ID == Contact_ID
        //              && modules.Contains(cch.Module)
        //              orderby cch.DateChanged descending
        //              select new ContactHistory
        //              {
        //                  CH_ID = cch.CH_ID,
        //                  Contact_ID = cch.Contact_ID,
        //                  User_ID = cch.User_ID,
        //                  User = cc.FirstName + " " + cc.LastName,
        //                  DateChanged = cch.DateChanged,
        //                  Description = cch.Desc,
        //                  Description2 = cch.Desc2,
        //                  Action = cch.Action,
        //                  Item = cch.Item,
        //                  OldText = cch.OldText,
        //                  NewText = cch.NewText
        //              }).ToList();
        //    }

        //    var model = new ContactViewModel();

        //    model.History = ch;

        //    return View(model);
        //}

        ////Used to register new registrant that dont have new account
        //public ActionResult NewRegistrant()
        //{
        //    NewRegistrantModel model = new NewRegistrantModel();
        //    model.common_contact = new Common_Contact();
        //    model.common_contactExternalEvent = new Common_ContactExternalEvents();
        //    return View("NewRegistrant", model);
        //}

        //public ActionResult NewRegistrantSignUp(NewRegistrantModel model)
        //{
        //    var user_id = CurrentContext.Instance.User.ContactID;
        //    Common_Contact common_contact = model.common_contact;
        //    Common_ContactExternalEvents common_contactEventExternal = model.common_contactExternalEvent;

        //    using (PlanetshakersEntities entity = new PlanetshakersEntities())
        //    {
        //        var exist = (from c in entity.Common_Contact
        //                     where c.Email == common_contact.Email
        //                     select c).FirstOrDefault();

        //        // insert into common_contact
        //        var new_contact = new Common_Contact();
        //        new_contact.Salutation_ID = common_contact.Salutation_ID;
        //        new_contact.FirstName = common_contact.FirstName;
        //        new_contact.LastName = common_contact.LastName;
        //        new_contact.Gender = common_contact.Gender;
        //        new_contact.DateOfBirth = common_contact.DateOfBirth;
        //        new_contact.Address1 = common_contact.Address1 == null ? "" : common_contact.Address1;
        //        new_contact.Address2 = String.Empty;
        //        new_contact.Suburb = common_contact.Suburb == null ? "" : common_contact.Suburb;
        //        new_contact.Postcode = common_contact.Postcode == null ? "" : common_contact.Postcode;
        //        new_contact.State_ID = common_contact.State_ID ;
        //        new_contact.StateOther = String.Empty;
        //        new_contact.Country_ID = common_contact.Country_ID;
        //        new_contact.Phone = common_contact.Phone == null ? "" : common_contact.Phone;
        //        new_contact.PhoneWork = String.Empty;
        //        new_contact.Fax = String.Empty;
        //        new_contact.Mobile = common_contact.Mobile == null ? "" : common_contact.Mobile;
        //        new_contact.DoNotIncludeEmail1InMailingList = false;
        //        new_contact.DoNotIncludeEmail2InMailingList = false;
        //        new_contact.Family_ID = null;
        //        new_contact.FamilyMemberType_ID = null;
        //        new_contact.Password = common_contact.Password == null ? "" : common_contact.Password;
        //        new_contact.SecretQuestion_ID = null;
        //        new_contact.SecretAnswer = String.Empty;
        //        new_contact.VolunteerPoliceCheck = String.Empty;
        //        new_contact.VolunteerPoliceCheckBit = false;
        //        new_contact.VolunteerPoliceCheckDate = null;
        //        new_contact.VolunteerWWCC = String.Empty;
        //        new_contact.VolunteerWWCCDate = null;
        //        new_contact.VolunteerWWCCType_ID = null;
        //        new_contact.CreatedBy_ID = user_id;
        //        new_contact.CreationDate = DateTime.Now;
        //        new_contact.ModifiedBy_ID = null;
        //        new_contact.ModificationDate = null;
        //        new_contact.DeDupeVerified = false;
        //        new_contact.GUID = Guid.NewGuid();
        //        if (exist != null)
        //        {
        //            new_contact.Email = String.Empty;
        //            new_contact.Email2 = common_contact.Email;
        //        }
        //        else
        //        {
        //            new_contact.Email = common_contact.Email;
        //            new_contact.Email2 = String.Empty;
        //        }
        //        entity.Common_Contact.Add(new_contact);

        //        // insert into common_contactexternalevents
        //        var new_contact_event = new Common_ContactExternalEvents();
        //        new_contact_event.EmergencyContactName = common_contactEventExternal.EmergencyContactName == null ? "" : common_contactEventExternal.EmergencyContactName;
        //        new_contact_event.EmergencyContactPhone = common_contactEventExternal.EmergencyContactPhone == null ? "" : common_contactEventExternal.EmergencyContactPhone;
        //        new_contact_event.EmergencyContactRelationship = common_contactEventExternal.EmergencyContactRelationship == null ? "" : common_contactEventExternal.EmergencyContactRelationship;
        //        new_contact_event.MedicalInformation = common_contactEventExternal.MedicalInformation == null ? "" : common_contactEventExternal.MedicalInformation;
        //        new_contact_event.MedicalAllergies = common_contactEventExternal.MedicalAllergies == null ? "" : common_contactEventExternal.MedicalAllergies;
        //        new_contact_event.AccessibilityInformation = "";
        //        new_contact_event.EventComments = "";
        //        entity.Common_ContactExternalEvents.Add(new_contact_event);

        //        entity.SaveChanges();
               
        //    }
        //    model.success = true;
        //    model.common_contact = new Common_Contact();
        //    model.common_contactExternalEvent = new Common_ContactExternalEvents();
        //    return View("NewRegistrant", model);
        //}

        //public ActionResult NewGroup(int contact_ID, int conference_ID)
        //{
        //    NewGroupModel newgroupmodel = new NewGroupModel();
        //    newgroupmodel.group = new Group
        //    {
        //        ID = contact_ID

        //    };
        //    newgroupmodel.Conference_ID = conference_ID;
        //    return View("NewGroup", newgroupmodel);
        //}
        //public ActionResult NewGroupCreate(NewGroupModel model)
        //{
        //    using (PlanetshakersEntities entity = new PlanetshakersEntities())
        //    {
                
        //        var new_group = new Events_Group();
        //        Group currentgroup = model.group;
        //        var existedGroup = (from eg in entity.Events_Group
        //                            where eg.GroupLeader_ID == currentgroup.ID
        //                            select eg).Any();
        //        if (existedGroup)
        //        {
        //            model.message = "Existed Group found. Please update group details instead of creating new group.";
        //            return View("NewGroup",model);
        //        }
        //        else
        //        {
        //            new_group.GroupLeader_ID = currentgroup.ID;
        //            new_group.GroupName = currentgroup.GroupName;
        //            new_group.GroupColeaders = currentgroup.GroupColeaders == null ? String.Empty : currentgroup.GroupColeaders;
        //            new_group.YouthLeaderName = currentgroup.YouthLeaderName == null ? String.Empty : currentgroup.YouthLeaderName;
        //            new_group.GroupSize_ID = currentgroup.GroupSizeID == null ? 1 : currentgroup.GroupSizeID;
        //            new_group.GroupCreditStatus_ID = 1;
        //            new_group.GroupType_ID = currentgroup.GroupTypeID == null ? 2 : (int)currentgroup.GroupTypeID;
        //            new_group.Comments = currentgroup.Comments == null ? String.Empty : currentgroup.Comments;
        //            new_group.DetailsVerified = false;
        //            new_group.Deleted = false;
        //            new_group.CreationDate = DateTime.Now;
        //            new_group.CreatedBy_ID = CurrentContext.Instance.User.ContactID;
        //            new_group.ModificationDate = null;
        //            new_group.ModifiedBy_ID = null;
        //            new_group.GUID = new Guid();

        //            entity.Events_Group.Add(new_group);
        //            entity.SaveChanges();

        //            NewGroupModel newmodel = new NewGroupModel();
        //            newmodel.success = true;
        //            newmodel.group = new Group
        //            {
        //                ID = model.group.ID
        //            };
        //            newmodel.Conference_ID = model.Conference_ID;
        //            return View("NewGroup", newmodel);

        //        }
                
        //    }
        //}

        //public ActionResult CreateNewGroupPageLoad()
        //{
        //    CreateNewGroupModel model = new CreateNewGroupModel();
        //    model.NewPeopleModel = new NewRegistrantModel();
        //    model.success = false;
        //    return View("CreateNewGroup", model);
        //}
        //public ActionResult CreateNewGroupWithNewRegistrant(CreateNewGroupModel model)
        //{
        //    var user_id = CurrentContext.Instance.User.ContactID;
        //    Common_Contact common_contact = model.NewPeopleModel.common_contact;
        //    Common_ContactExternalEvents common_contactEventExternal = model.NewPeopleModel.common_contactExternalEvent;

        //    using (PlanetshakersEntities entity = new PlanetshakersEntities())
        //    {
        //        /*First off by creating new profile for groupleader first, then go for create new group for him*/
        //        var exist = (from c in entity.Common_Contact
        //                     where c.Email == common_contact.Email
        //                     select c).FirstOrDefault();
        //        if (exist != null)
        //        {
        //            model.message = "Email is existed. Please use another email address.";
        //            return View("CreateNewGroup", model);
        //        }
        //        else
        //        {
        //            // insert into common_contact
        //            var new_contact = new Common_Contact();
        //            new_contact.Salutation_ID = common_contact.Salutation_ID;
        //            new_contact.FirstName = common_contact.FirstName;
        //            new_contact.LastName = common_contact.LastName;
        //            new_contact.Gender = common_contact.Gender;
        //            new_contact.DateOfBirth = common_contact.DateOfBirth;
        //            new_contact.Address1 = common_contact.Address1 == null ? "" : common_contact.Address1;
        //            new_contact.Address2 = String.Empty;
        //            new_contact.Suburb = common_contact.Suburb == null ? "" : common_contact.Suburb;
        //            new_contact.Postcode = common_contact.Postcode == null ? "" : common_contact.Postcode;
        //            new_contact.State_ID = common_contact.State_ID;
        //            new_contact.StateOther = String.Empty;
        //            new_contact.Country_ID = common_contact.Country_ID;
        //            new_contact.Phone = common_contact.Phone == null ? "" : common_contact.Phone;
        //            new_contact.PhoneWork = String.Empty;
        //            new_contact.Fax = String.Empty;
        //            new_contact.Mobile = common_contact.Mobile == null ? "" : common_contact.Mobile;
        //            new_contact.DoNotIncludeEmail1InMailingList = false;
        //            new_contact.DoNotIncludeEmail2InMailingList = false;
        //            new_contact.Family_ID = null;
        //            new_contact.FamilyMemberType_ID = null;
        //            new_contact.Password = common_contact.Password == null ? "" : common_contact.Password;
        //            new_contact.SecretQuestion_ID = null;
        //            new_contact.SecretAnswer = String.Empty;
        //            new_contact.VolunteerPoliceCheck = String.Empty;
        //            new_contact.VolunteerPoliceCheckBit = false;
        //            new_contact.VolunteerPoliceCheckDate = null;
        //            new_contact.VolunteerWWCC = String.Empty;
        //            new_contact.VolunteerWWCCDate = null;
        //            new_contact.VolunteerWWCCType_ID = null;
        //            new_contact.CreatedBy_ID = user_id;
        //            new_contact.CreationDate = DateTime.Now;
        //            new_contact.ModifiedBy_ID = null;
        //            new_contact.ModificationDate = null;
        //            new_contact.DeDupeVerified = false;
        //            new_contact.GUID = Guid.NewGuid();
        //            new_contact.Email = common_contact.Email;
        //            new_contact.Email2 = String.Empty;
                    
        //            entity.Common_Contact.Add(new_contact);

        //            // insert into common_contactexternalevents
        //            var new_contact_event = new Common_ContactExternalEvents();
        //            new_contact_event.EmergencyContactName = common_contactEventExternal.EmergencyContactName == null ? "" : common_contactEventExternal.EmergencyContactName;
        //            new_contact_event.EmergencyContactPhone = common_contactEventExternal.EmergencyContactPhone == null ? "" : common_contactEventExternal.EmergencyContactPhone;
        //            new_contact_event.EmergencyContactRelationship = common_contactEventExternal.EmergencyContactRelationship == null ? "" : common_contactEventExternal.EmergencyContactRelationship;
        //            new_contact_event.MedicalInformation = common_contactEventExternal.MedicalInformation == null ? "" : common_contactEventExternal.MedicalInformation;
        //            new_contact_event.MedicalAllergies = common_contactEventExternal.MedicalAllergies == null ? "" : common_contactEventExternal.MedicalAllergies;
        //            new_contact_event.AccessibilityInformation = "";
        //            new_contact_event.EventComments = "";
        //            entity.Common_ContactExternalEvents.Add(new_contact_event);

        //            entity.SaveChanges();

        //            var newgroupleaderID = (from cc in entity.Common_Contact
        //                                    where cc.Email == new_contact.Email
        //                                    select cc.Contact_ID).FirstOrDefault();
        //            if (newgroupleaderID != 0)
        //            {
        //                //New Group Creation go here
        //                var new_group = new Events_Group();
        //                Group currentgroup = model.group;
        //                new_group.GroupLeader_ID = newgroupleaderID;
        //                new_group.GroupName = currentgroup.GroupName;
        //                new_group.GroupColeaders = currentgroup.GroupColeaders == null ? String.Empty : currentgroup.GroupColeaders;
        //                new_group.YouthLeaderName = currentgroup.YouthLeaderName == null ? String.Empty : currentgroup.YouthLeaderName;
        //                new_group.GroupSize_ID = currentgroup.GroupSizeID == null ? 1 : currentgroup.GroupSizeID;
        //                new_group.GroupCreditStatus_ID = 1;
        //                new_group.GroupType_ID = currentgroup.GroupTypeID == null ? 2 : (int)currentgroup.GroupTypeID;
        //                new_group.Comments = currentgroup.Comments == null ? String.Empty : currentgroup.Comments;
        //                new_group.DetailsVerified = false;
        //                new_group.Deleted = false;
        //                new_group.CreationDate = DateTime.Now;
        //                new_group.CreatedBy_ID = CurrentContext.Instance.User.ContactID;
        //                new_group.ModificationDate = null;
        //                new_group.ModifiedBy_ID = null;
        //                new_group.GUID = new Guid();

        //                entity.Events_Group.Add(new_group);
        //                entity.SaveChanges();
        //                model.success = true;
        //                return View("CreateNewGroup", model);
        //            }
        //            else
        //            {
        //                model.message = "Failed to retrieve groupleader ID.";
        //                return View("CreateNewGroup", model);
        //            }

        //        }

        //    }
            
        //}

        #region Registration
        /**
         * If registration exists, update registration. Otherwise, add registration.
         **/
        [HttpPost]
        public ActionResult UpdateRego(int Contact_ID, int? Venue_ID, int Registration_ID, int Conference_ID, int RegistrationType_ID, decimal RegoDiscount, int? GroupLeader_ID, bool Attended, bool Volunteer, int? VolunteerDepartment_ID)
        {
            var rego = new Registration();
            var user_id = CurrentContext.Instance.User.ContactID;

            using (PlanetshakersEntities entity = new PlanetshakersEntities())
            {
                var has_group = (from eg in entity.Events_Group
                                 where eg.GroupLeader_ID == Contact_ID
                                 select eg.GroupLeader_ID).Any();

                if (!has_group)
                {
                    var new_group = new Events_Group();
                    new_group.GroupLeader_ID = Contact_ID;
                    new_group.GroupName = String.Empty;
                    new_group.GroupColeaders = String.Empty;
                    new_group.YouthLeaderName = String.Empty;
                    new_group.GroupSize_ID = 1;
                    new_group.GroupCreditStatus_ID = 1;
                    new_group.GroupType_ID = 2;
                    new_group.Comments = String.Empty;
                    new_group.DetailsVerified = false;
                    new_group.Deleted = false;
                    new_group.CreationDate = DateTime.Now;
                    new_group.CreatedBy_ID = CurrentContext.Instance.User.ContactID;
                    new_group.ModificationDate = null;
                    new_group.ModifiedBy_ID = null;
                    new_group.GUID = new Guid();

                    entity.Events_Group.Add(new_group);
                    entity.SaveChanges();
                }
            }

            if (Registration_ID > 0)
            {
                rego.ContactID = Contact_ID;
                rego.ID = Registration_ID;
                rego.VenueId = Venue_ID;
                rego.Conference_ID = Conference_ID;
                rego.RegoType_ID = RegistrationType_ID;
                rego.GroupLeader_ID = (GroupLeader_ID != null) ? GroupLeader_ID : Contact_ID;
                rego.Attended = Attended;
                rego.Volunteer = Volunteer;
                rego.VolunteerDepartment_ID = VolunteerDepartment_ID;
                rego.RegistrationDate = DateTime.Now;
                rego.RegisteredBy_ID = user_id;
                rego.RegoDiscount = RegoDiscount;
                rego.User_ID = user_id;

                var lst = new List<Registration>();

                // TODO: HANDLE NULL INPUTS FOR ALL FIELDS
                lst = _repository.UpdateRegistration(rego);
            }
            else
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    rego.ContactID = Contact_ID;
                    rego.Conference_ID = Conference_ID;

                    rego.VenueId = (from ev in context.Events_Venue
                                    where ev.Conference_ID == Conference_ID
                                    select ev.Venue_ID).FirstOrDefault();

                    rego.RegoType_ID = RegistrationType_ID;
                    rego.GroupLeader_ID = (GroupLeader_ID != null) ? GroupLeader_ID : Contact_ID;
                    rego.Attended = Attended;
                    rego.Volunteer = Volunteer;
                    rego.VolunteerDepartment_ID = VolunteerDepartment_ID;
                    rego.RegistrationDate = DateTime.Now;
                    rego.RegisteredBy_ID = user_id;
                    rego.RegoDiscount = RegoDiscount;
                    rego.User_ID = user_id;

                    // TODO: HANDLE NULL VALUES FOR ALL FIELDS
                    _repository.InsertRegistration(rego);

                    #region Boom Camp Exception
                    if (RegistrationType_ID == 2874 || RegistrationType_ID == 2876)
                    {
                        var has_group = (from g in context.Events_Group
                                         where g.GroupLeader_ID == Contact_ID
                                         select g.GroupLeader_ID).Any();

                        if (!has_group)
                        {
                            // Create group
                            var new_group = new Events_Group();
                            new_group.GroupLeader_ID = Contact_ID;

                            new_group.GroupName = (from c in context.Common_Contact
                                                   where c.Contact_ID == Contact_ID
                                                   select c.FirstName + " " + c.LastName).Single();

                            new_group.GroupColeaders = String.Empty;
                            new_group.YouthLeaderName = String.Empty;
                            new_group.GroupSize_ID = 1;
                            new_group.GroupCreditStatus_ID = 1;
                            new_group.GroupType_ID = 1;
                            new_group.Comments = String.Empty;
                            new_group.DetailsVerified = false;
                            new_group.Deleted = false;
                            new_group.CreationDate = DateTime.Now;
                            new_group.CreatedBy_ID = user_id;
                            new_group.ModificationDate = null;
                            new_group.ModifiedBy_ID = null;
                            new_group.GUID = Guid.NewGuid();

                            context.Events_Group.Add(new_group);
                            context.SaveChanges();
                        }

                        // Insert group bulk_rego
                        var complimentary = new BulkRegistration();
                        complimentary.GroupLeader_ID = Contact_ID;
                        complimentary.Venue_ID = 142;
                        complimentary.RegistrationType_ID = 2889;
                        complimentary.Quantity = 1;

                        _repository.InsertBulkRegistration(complimentary, user_id);
                    }
                    #endregion
                }
            }

            return RedirectToAction("ContactDetails", "Registration", new { Contact_ID = rego.ContactID, Conference_ID = rego.Conference_ID });
        }

        /**
         * Remove selected registration based on Registration_ID and Conference_ID
         * **/
        public ActionResult RemoveRego(int Registration_ID, int Contact_ID, int Conference_ID)
        {
            var User_ID = CurrentContext.Instance.User.ContactID;

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var regtype = (from r in context.Events_Registration
                               where r.Registration_ID == Registration_ID
                               select r.RegistrationType_ID).SingleOrDefault();

                if (regtype == 2874 || regtype == 2876)
                {
                    var complimentary = (from br in context.Events_GroupBulkRegistration
                                         where br.RegistrationType_ID == 2889 && br.Quantity == 1
                                         select br.GroupBulkRegistration_ID).FirstOrDefault();

                    _repository.DeleteBulkRegistration(complimentary, User_ID);
                }
            }

            _repository.RemoveRegistration(Registration_ID, User_ID);

            return RedirectToAction("ContactDetails", "Registration", new { Contact_ID = Contact_ID, Conference_ID = Conference_ID });
        }

        #endregion

        #endregion

        #region Group Registration
        // View for group registration search
        //public ActionResult GroupRegistration()
        //{
        //    var model = new SearchViewModel();

        //    using (PlanetshakersEntities context = new PlanetshakersEntities())
        //    {
        //        model.Search = new SearchModel();
        //        model.Search.Conference_ID = (from ec in context.Events_Conference
        //                                      join ev in context.Events_Venue
        //                                      on ec.Conference_ID equals ev.Conference_ID
        //                                      where ev.ConferenceEndDate >= DateTime.Today
        //                                      orderby ec.SortOrder
        //                                      select ec.Conference_ID).FirstOrDefault();
        //    }

        //    return View(model);
        //}

        //public ActionResult SearchGroupContact(string query, int Conference_ID)
        //{
        //    SearchViewModel model = new SearchViewModel();
        //    model.Search = new SearchModel();
        //    model.Search.QuickSearch = query;
        //    model.Search.Conference_ID = Conference_ID;
        //    return GroupSearch(model);
        //}

        //[HttpPost]
        //public ActionResult GroupSearch(SearchViewModel model)
        //{
        //    model.Results = new ResultsModel();

        //    model.Results.Headers = new List<string> {
        //        "Group ID",
        //        "Group Name",
        //        "First Name",
        //        "Last Name",
        //        "Balance",
        //        "Creation Date",
        //        "Registered",
        //        "Unallocated"
        //    };

        //    model.Results.GroupRego = _repository.FindGroup(model.Search);

        //    model.Conference_ID = model.Search.Conference_ID;

        //    return View("GroupRegistration", model);
        //}

        public ActionResult GroupDetails(int Group_ID, int Conference_ID)
        {
            var model = new GroupViewModel();

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var purchases = new List<Purchase>();
                var regos = new List<Registration>();
                var conf = new Conference();
                var payments = new List<Payment>();
                var comments = new List<CommentItem>();

                if (Group_ID > 0)
                {
                    //update payment intends over 1 hour for individual (to expired)
                    var expireTime = DateTime.Now.AddHours(-1);
                    var expiredIntends = (from gp in context.Events_GroupPayment
                                          where gp.GroupLeader_ID == Group_ID 
                                          && gp.PaymentCompleted == false 
                                          && gp.PaymentCompletedDate == null 
                                          && gp.PaymentStartDate < expireTime 
                                          && (gp.ExpiredIntent != true || gp.ExpiredIntent == null)
                                          select gp).ToList();
                    foreach(var item in expiredIntends)
                    {
                        item.ExpiredIntent = true;
                        item.Voucher_ID = null;
                    }
                    context.SaveChanges();

                    //update any duplicated sponsorship allocation (mark incomplete)
                    checkSponsorshipDuplicate(Conference_ID, Group_ID);

                    // Get details of the selected group
                    model.Group = (from eg in context.Events_Group
                                   join cc in context.Common_Contact
                                   on eg.GroupLeader_ID equals cc.Contact_ID
                                   where eg.GroupLeader_ID == Group_ID
                                   select new GroupDetails
                                   {
                                       ID = eg.GroupLeader_ID,
                                       GroupName = eg.GroupName,
                                       LeaderFirstName = cc.FirstName,
                                       LeaderLastName = cc.LastName,
                                       LeaderEmail = cc.Email,
                                       LeaderMobile = cc.Mobile,
                                       LeaderPhone = cc.Phone,
                                   }).FirstOrDefault();

                    model.GroupRegistration = new GroupRegistration();
                }
                else
                {
                    //Something went wrong
                }

                if (Conference_ID > 0)
                {
                    model.Conference = (from ec in context.Events_Conference
                                        join ev in context.Events_Venue
                                        on ec.Conference_ID equals ev.Conference_ID
                                        where ec.Conference_ID == Conference_ID
                                        select new Conference
                                        {
                                            ID = ec.Conference_ID,
                                            ConferenceName = ec.ConferenceName,
                                            Venue_ID = ev.Venue_ID
                                        }).FirstOrDefault();
                }
                else
                {
                    model.Conference = new Conference();
                }

                if (Conference_ID > 0 && Group_ID > 0)
                {
                    // Get registrations with selected group leader IDs
                    regos = (from er in context.Events_Registration
                             join ev in context.Events_Venue
                             on er.Venue_ID equals ev.Venue_ID
                             join ec in context.Events_Conference
                             on ev.Conference_ID equals ec.Conference_ID
                             join cc in context.Common_Contact
                             on er.GroupLeader_ID equals cc.Contact_ID
                             join ert in context.Events_RegistrationType
                             on er.RegistrationType_ID equals ert.RegistrationType_ID
                             where er.GroupLeader_ID == Group_ID && ec.Conference_ID == Conference_ID
                             select new Registration
                             {
                                 ID = er.Registration_ID,
                                 GroupLeader_ID = er.GroupLeader_ID,
                                 ContactID = cc.Contact_ID,
                                 FirstName = cc.FirstName,
                                 LastName = cc.LastName,
                                 Conference_ID = ev.Conference_ID,
                                 RegoType_ID = ert.RegistrationType_ID,
                                 RegoType = ert.RegistrationType,
                                 //quantity = er.Quantity
                             }).ToList();

                    model.GroupRegistration.Regos = regos;

                    //// Get count of purchased registrations
                    purchases = (from grv in context.vw_Events_GroupBulkRegistrationView
                                 join ert in context.Events_RegistrationType
                                 on grv.RegistrationType_ID equals ert.RegistrationType_ID
                                 where grv.GroupLeader_ID == Group_ID && grv.Conference_ID == Conference_ID
                                 select new Purchase
                                 {
                                     RegistrationType_ID = ert.RegistrationType_ID,
                                     RegistrationType = ert.RegistrationType,
                                     PurchasedCount = grv.Purchased,
                                     AllocatedCount = grv.Allocated,
                                     UnallocatedCount = grv.UnAllocated
                                 }).ToList();

                    model.GroupRegistration.GroupPurchases = purchases;
                    model.GroupRegistration.NumRegistered = 0;
                    model.GroupRegistration.UnallocatedRegoCount = 0;

                    foreach (var x in purchases)
                    {
                        model.GroupRegistration.NumRegistered += x.PurchasedCount;
                        model.GroupRegistration.UnallocatedRegoCount += x.UnallocatedCount;
                    }

                    // Get all group payments made for selected conference
                    payments = (from gp in context.Events_GroupPayment
                                join pt in context.Common_PaymentType on gp.PaymentType_ID equals pt.PaymentType_ID
                                join ps in context.Common_PaymentSource on gp.PaymentSource_ID equals ps.PaymentSource_ID
                                join ba in context.Common_BankAccount on gp.BankAccount_ID equals ba.BankAccount_ID
                                where gp.GroupLeader_ID == Group_ID 
                                        && gp.Conference_ID == Conference_ID 
                                        && (gp.ExpiredIntent != true || gp.ExpiredIntent == null)
                                select new Payment
                                {
                                    ID = gp.GroupPayment_ID,
                                    PaymentType_ID = gp.PaymentType_ID,
                                    PaymentType = pt.PaymentType,
                                    PaymentSource_ID = gp.PaymentSource_ID,
                                    PaymentSource = ps.PaymentSource,
                                    TransactionRef = gp.CCTransactionRef,
                                    Amount = gp.PaymentAmount,
                                    Currency = ba.Currency,
                                    StartDate = gp.PaymentStartDate,
                                    CompletedDate = gp.PaymentCompletedDate,
                                    Completed = gp.PaymentCompleted,
                                }).ToList();
                    model.PaymentList = payments;

                    comments = (from gc in context.Events_GroupComment
                                join cc in context.Common_Contact
                                on gc.CommentBy equals cc.Contact_ID
                                where gc.Group_ID == model.Group.ID && gc.Conference_ID == model.Conference.ID
                                select new CommentItem
                                {
                                    Id = gc.GroupComment_ID,
                                    Conference_ID = gc.Conference_ID,
                                    Comment = gc.Comment,
                                    CommentDate = gc.Date,
                                    Commenter_ID = gc.CommentBy,
                                    CommenterFirstName = cc.FirstName,
                                    CommenterLastName = cc.LastName
                                }).ToList();

                    model.CommentList = comments;

                    model.Documents.Contact_ID = Group_ID;
                    model.Documents.Event_ID = Conference_ID;

                    model.applicationDetails = (from v in context.Events_Venue
                                join r in context.Events_Registration on v.Venue_ID equals r.Venue_ID
                                join pr in context.Volunteer_PendingRole on r.Registration_ID equals pr.Registration_ID
                                join p in context.Volunteer_ApplicationProgress on pr.Application_ID equals p.VolunteerApplication_ID
                                join a in context.Volunteer_Application on pr.Application_ID equals a.Application_ID
                                join c in context.Common_Contact on a.Contact_ID equals c.Contact_ID
                                join rt in context.Events_RegistrationType on a.RegistrationType_ID equals rt.RegistrationType_ID
                                where v.Conference_ID == Conference_ID && r.Contact_ID == Group_ID /*&& p.Status_ID == 2*/
                                select new Models.Application
                                {
                                    Application_ID = a.Application_ID,
                                    Contact_ID = c.Contact_ID,
                                    FirstName = c.FirstName,
                                    LastName = c.LastName,
                                    SubmittedTime = a.DateRequested,
                                    ApprovedTime = a.DateActioned,
                                    RegistrationType_ID = r.RegistrationType_ID,
                                    RegistrationType = rt.RegistrationType,
                                    //Event_ID = rt.Conference_ID
                                }).FirstOrDefault();
                    model.Withdrawn = (from p in context.Volunteer_ApplicationProgress
                                       where p.VolunteerApplication_ID == model.applicationDetails.Application_ID && p.Status_ID == 6
                                       select p).Any();

                    var groupsingle = "Group";
                    var emergencyList = (from e in context.Events_EmergencyContact
                                         where e.Contact_ID == Group_ID
                                         select new ApplicationEmergencyContact()
                                         {
                                             Name = e.EmergencyContactName,
                                             Mobile = e.EmergencyContactPhone,
                                             Relationship = e.EmergencyContactRelationship,
                                             EmergencyContact_ID = e.EmergencyContact_ID
                                         }).ToList();
                    model.emergencyContacts = emergencyList;

                    model.OutstandingBalance = _repository.GetOutstandingBalance(groupsingle, Group_ID, Conference_ID);
                    //get sponsorship that are not transactions 
                    var sponsorshipAmount = (from v in context.Events_VariablePayment
                                             where v.IncomingContact_ID == Group_ID && v.Conference_ID == Conference_ID && v.CCTransactionReference == null && v.PaymentCompleted == true
                                             select v.PaymentAmount).Sum() ?? 0;
                    model.OutstandingBalance.TotalPayment -= sponsorshipAmount;
                    model.OutstandingBalance.Balance += sponsorshipAmount;


                    var ContactProfile_ID = (from c in context.Events_ContactProfile
                                               where c.Contact_ID == Group_ID && c.Event_ID == Conference_ID
                                               select c.ContactProfile_ID).FirstOrDefault();
                    if (ContactProfile_ID == 0)
                    {
                        var newContactProfile = new Events_ContactProfile()
                        {
                            Contact_ID = Group_ID,
                            Event_ID = Conference_ID,
                            PictureURL = "",
                            AboutMyself = "",
                            TripDetail = ""
                        };
                        context.Events_ContactProfile.Add(newContactProfile);
                        context.SaveChanges();
                        model.ContactProfile_ID = newContactProfile.ContactProfile_ID;
                    } else
                    {
                        model.ContactProfile_ID = ContactProfile_ID;
                    }
                }
                else
                {
                    //Something went terribly wrong
                }
            }

            return View(model);
        }

        #region Contact Profile
        public ActionResult ViewContactProfile(int ContactProfile_ID)
        {
            ContactProfileModel model = new ContactProfileModel()
            {
                ContactProfile_ID = ContactProfile_ID
            };
            model.populate();
            return View("ContactProfileEdit", model);

        }

        [HttpPost]
        public ActionResult updateContactProfile(int Event_ID, int Contact_ID, int ContactProfile_ID, string AboutMyself, string TripDetails)
        {
            ContactProfileModel model = new ContactProfileModel()
            {
                Event_ID = Event_ID,
                ContactProfile_ID = ContactProfile_ID,
                Contact_ID = Contact_ID,
                AboutMyself = AboutMyself,
                TripDetails = TripDetails
            };
            var success = model.updateContactProfile();
            return Json(new { success = success });
        }

        [HttpPost]
        public ActionResult RemovePicture(int ContactProfile_ID)
        {
            ContactProfileModel model = new ContactProfileModel()
            {
                ContactProfile_ID = ContactProfile_ID
            };
            var success = model.deletePhoto();
            return Json(new { success = success });
        }
        #endregion  

        #region Add Registrants
        //[HttpPost]
        //public ActionResult AllocateCurrent(GroupViewModel model)
        //{
        //    var conference_id = model.Conference.ID;

        //    if (model.NewRegistration.ContactID > 0)
        //    {
        //        var details = model.NewRegistration;

        //        using (PlanetshakersEntities context = new PlanetshakersEntities())
        //        {
        //            var venue_id = (from ev in context.Events_Venue
        //                            where ev.Conference_ID == conference_id
        //                            select ev.Venue_ID).FirstOrDefault();

        //            var new_registration = new Events_Registration();
        //            new_registration.Contact_ID = details.ContactID;
        //            new_registration.GroupLeader_ID = details.GroupLeader_ID;
        //            new_registration.FamilyRegistration = false;
        //            new_registration.RegisteredByFriend_ID = null;
        //            new_registration.Venue_ID = venue_id;
        //            new_registration.RegistrationType_ID = details.RegoType_ID;
        //            new_registration.RegistrationDiscount = (decimal)0.00;
        //            new_registration.Elective_ID = null;
        //            new_registration.Accommodation = false;
        //            new_registration.Accommodation_ID = null;
        //            new_registration.Catering = false;
        //            new_registration.CateringNeeds = String.Empty;
        //            new_registration.LeadershipBreakfast = false;
        //            new_registration.LeadershipSummit = false;
        //            new_registration.ULG_ID = null;
        //            new_registration.ParentGuardian = String.Empty;
        //            new_registration.ParentGuardianPhone = String.Empty;
        //            new_registration.AcceptTermsRego = true;
        //            new_registration.AcceptTermsParent = false;
        //            new_registration.AcceptTermsCreche = false;
        //            new_registration.RegistrationDate = DateTime.Now;
        //            new_registration.RegisteredBy_ID = CurrentContext.Instance.User.ContactID;
        //            new_registration.Attended = false;
        //            new_registration.Volunteer = false;
        //            new_registration.VolunteerDepartment_ID = null;
        //            new_registration.ComboRegistration_ID = null;

        //            context.Events_Registration.Add(new_registration);
        //            context.SaveChanges();

        //            //TODO: Send allocation email
        //        }
        //    }
        //    else
        //    {
        //        // Something wrong happened
        //    }

        //    return RedirectToAction("GroupDetails", "Registration", new { Group_ID = model.NewRegistration.GroupLeader_ID, Conference_ID = model.Conference.ID });
        //}
        //[HttpPost]
        //public ActionResult AllocatePrevious(GroupViewModel model)
        //{
        //    var conference_id = model.Conference.ID;

        //    if (model.NewRegistration.ContactID > 0)
        //    {
        //        var details = model.NewRegistration;

        //        using (PlanetshakersEntities context = new PlanetshakersEntities())
        //        {
        //            var venue_id = (from ev in context.Events_Venue
        //                            where ev.Conference_ID == conference_id
        //                            select ev.Venue_ID).FirstOrDefault();

        //            var new_registration = new Events_Registration();
        //            new_registration.Contact_ID = details.ContactID;
        //            new_registration.GroupLeader_ID = details.GroupLeader_ID;
        //            new_registration.FamilyRegistration = false;
        //            new_registration.RegisteredByFriend_ID = null;
        //            new_registration.Venue_ID = venue_id;
        //            new_registration.RegistrationType_ID = details.RegoType_ID;
        //            new_registration.RegistrationDiscount = (decimal)0.00;
        //            new_registration.Elective_ID = null;
        //            new_registration.Accommodation = false;
        //            new_registration.Accommodation_ID = null;
        //            new_registration.Catering = false;
        //            new_registration.CateringNeeds = String.Empty;
        //            new_registration.LeadershipBreakfast = false;
        //            new_registration.LeadershipSummit = false;
        //            new_registration.ULG_ID = null;
        //            new_registration.ParentGuardian = String.Empty;
        //            new_registration.ParentGuardianPhone = String.Empty;
        //            new_registration.AcceptTermsRego = true;
        //            new_registration.AcceptTermsParent = false;
        //            new_registration.AcceptTermsCreche = false;
        //            new_registration.RegistrationDate = DateTime.Now;
        //            new_registration.RegisteredBy_ID = CurrentContext.Instance.User.ContactID;
        //            new_registration.Attended = false;
        //            new_registration.Volunteer = false;
        //            new_registration.VolunteerDepartment_ID = null;
        //            new_registration.ComboRegistration_ID = null;

        //            context.Events_Registration.Add(new_registration);
        //            context.SaveChanges();

        //            //TODO: Send allocation email
        //        }
        //    }
        //    else
        //    {
        //        // Something wrong happened
        //    }

        //    return RedirectToAction("GroupDetails", "Registration", new { Group_ID = model.NewRegistration.GroupLeader_ID, Conference_ID = model.Conference.ID });
        //}

        [HttpPost]
        public ActionResult SearchRegistrant(string query, int Group_ID, int Conference_ID)
        {
            var user_id = CurrentContext.Instance.User.ContactID;

            // Execute search here


            // Cannot redirect search to group details
            return RedirectToAction("GroupDetails", "Registration", new { Group_ID = Group_ID, Conference_ID = Conference_ID });
        }

        [HttpPost]
        public ActionResult NewRegistrant(GroupViewModel model)
        {
            var rego = model.NewRegistration;

            if (rego != null)
            {
                var conference_id = model.Conference.ID;
                var details = rego;

                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var venue_id = (from ev in context.Events_Venue
                                    where ev.Conference_ID == conference_id
                                    select ev.Venue_ID).FirstOrDefault();

                    // create new common_contactExternalEvents
                    var new_contact_event = new Common_ContactExternalEvents();
                    new_contact_event.EmergencyContactName = "";
                    new_contact_event.EmergencyContactPhone = "";
                    new_contact_event.EmergencyContactRelationship = "";
                    new_contact_event.MedicalInformation = "";
                    new_contact_event.MedicalAllergies = "";
                    new_contact_event.AccessibilityInformation = "";
                    new_contact_event.EventComments = "";

                    context.Common_ContactExternalEvents.Add(new_contact_event);

                    // create new common contact
                    var new_contact = new Common_Contact();
                    new_contact.Salutation_ID = 1;
                    new_contact.FirstName = details.FirstName;
                    new_contact.LastName = details.LastName;
                    new_contact.Gender = String.Empty;
                    new_contact.DateOfBirth = details.DoB;
                    new_contact.Address1 = String.Empty;
                    new_contact.Address2 = String.Empty;
                    new_contact.Suburb = String.Empty;
                    new_contact.Postcode = String.Empty;
                    new_contact.State_ID = 9;
                    new_contact.StateOther = String.Empty;
                    new_contact.Country_ID = (details.Country_ID > 0 ? details.Country_ID : 11);
                    new_contact.Phone = String.Empty;
                    new_contact.PhoneWork = String.Empty;
                    new_contact.Fax = String.Empty;
                    new_contact.Mobile = (details.Mobile != null ? details.Mobile : String.Empty);

                    var lst = (from cc in context.Common_Contact
                               where cc.Email == details.Email
                               select cc).ToList();

                    if (lst.Any())
                    {
                        new_contact.Email = String.Empty;
                        new_contact.Email2 = details.Email != null ? details.Email : String.Empty;
                    }
                    else
                    {
                        new_contact.Email = details.Email != null ? details.Email : String.Empty;
                        new_contact.Email2 = String.Empty;
                    }

                    new_contact.DoNotIncludeEmail1InMailingList = false;
                    new_contact.DoNotIncludeEmail2InMailingList = false;
                    new_contact.Family_ID = null;
                    new_contact.FamilyMemberType_ID = null;
                    new_contact.Password = String.Empty;
                    new_contact.SecretQuestion_ID = null;
                    new_contact.SecretAnswer = null;
                    new_contact.VolunteerPoliceCheck = String.Empty;
                    new_contact.VolunteerPoliceCheckBit = false;
                    new_contact.VolunteerPoliceCheckDate = null;
                    new_contact.VolunteerWWCC = String.Empty;
                    new_contact.VolunteerWWCCDate = null;
                    new_contact.VolunteerWWCCType_ID = null;
                    new_contact.CreatedBy_ID = CurrentContext.Instance.User.ContactID;
                    new_contact.CreationDate = DateTime.Now;
                    new_contact.ModificationDate = null;
                    new_contact.ModifiedBy_ID = null;
                    new_contact.GUID = Guid.NewGuid();
                    new_contact.MaritalStatus_ID = null;
                    new_contact.AnotherLanguage_ID = null;
                    new_contact.Nationality_ID = null;
                    new_contact.LastLoginDate = null;

                    context.Common_Contact.Add(new_contact);
                    context.SaveChanges();

                    var new_registration = new Events_Registration();
                    new_registration.Contact_ID = new_contact.Contact_ID;
                    new_registration.GroupLeader_ID = rego.GroupLeader_ID;
                    new_registration.FamilyRegistration = false;
                    new_registration.RegisteredByFriend_ID = null;
                    new_registration.Venue_ID = venue_id;
                    new_registration.RegistrationType_ID = rego.RegoType_ID;
                    new_registration.RegistrationDiscount = (decimal)0.00;
                    new_registration.Elective_ID = null;
                    new_registration.Accommodation = false;
                    new_registration.Accommodation_ID = null;
                    new_registration.Catering = false;
                    new_registration.CateringNeeds = String.Empty;
                    new_registration.LeadershipBreakfast = false;
                    new_registration.LeadershipSummit = false;
                    new_registration.ULG_ID = null;
                    new_registration.ParentGuardian = String.Empty;
                    new_registration.ParentGuardianPhone = String.Empty;
                    new_registration.AcceptTermsRego = true;
                    new_registration.AcceptTermsParent = false;
                    new_registration.AcceptTermsCreche = false;
                    new_registration.RegistrationDate = DateTime.Now;
                    new_registration.RegisteredBy_ID = CurrentContext.Instance.User.ContactID;
                    new_registration.Attended = false;
                    new_registration.Volunteer = false;
                    new_registration.VolunteerDepartment_ID = null;
                    new_registration.ComboRegistration_ID = null;

                    context.Events_Registration.Add(new_registration);
                    context.SaveChanges();
                }
            }
            else
            {
                // Something wrong happened
            }

            return RedirectToAction("GroupDetails", "Registration", new { Group_ID = model.NewRegistration.GroupLeader_ID, Conference_ID = model.Conference.ID });
        }

        public ActionResult ManualAllocationSearch(string firstname, string lastname, string email, string mobile)
        {
            List<PCRContact> searchresult = _repository.GetValidInternalExternal(CurrentContext.Instance.User.ContactID, firstname, lastname, email, mobile);
            return Json(searchresult);
        }

        public ActionResult AddExternalContact(int Id)
        {
            bool success = false;

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var new_contact_event = new Common_ContactExternalEvents();
                new_contact_event.Contact_ID = Id;
                new_contact_event.EmergencyContactName = "";
                new_contact_event.EmergencyContactPhone = "";
                new_contact_event.EmergencyContactRelationship = "";
                new_contact_event.MedicalInformation = "";
                new_contact_event.MedicalAllergies = "";
                new_contact_event.AccessibilityInformation = "";
                new_contact_event.EventComments = "";
                // create new external contact
                
                context.Common_ContactExternalEvents.Add(new_contact_event);
                context.SaveChanges();
                success = true;
            }
            return Json(success);
        }
        [HttpPost]
        public ActionResult RemoveRegoTypeCheck(int Registration_ID, int Group_ID, int Conference_ID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var success = (from r in context.Events_Registration
                               join rt in context.Events_RegistrationType on r.RegistrationType_ID equals rt.RegistrationType_ID
                               where r.GroupLeader_ID == Group_ID && rt.Conference_ID == Conference_ID && r.Registration_ID != Registration_ID
                               select r).Any();
                return Json(new { success = success });
            }
        }

        public bool MakeWithdrawnStatus(int Application_ID)
        {

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                try
                {
                    var applicationProgress = (from a in context.Volunteer_ApplicationProgress
                                               where a.VolunteerApplication_ID == Application_ID
                                               select a).FirstOrDefault();
                    applicationProgress.Status_ID = 6;
                    applicationProgress.ModifiedDate = DateTime.Now;
                    context.SaveChanges();
                    return true;
                } catch
                {
                    throw;
                }
            }
        }
        public ActionResult RemoveRegistrant(int Registration_ID, int Group_ID, int Conference_ID)
        {

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var user_id = CurrentContext.Instance.User.ContactID;
                var registrationtype_id = (from r in context.Events_Registration where r.Registration_ID == Registration_ID select r.RegistrationType_ID).FirstOrDefault();
                var pendingApp = (from p in context.Volunteer_PendingRole where p.Registration_ID == Registration_ID select p).FirstOrDefault();

                //when this is the primary registration 
                if (pendingApp != null)
                {
                    //only update pending role if there is other existing allocated registration type 
                    var currentRegistration = (from r in context.Events_Registration
                                               join rt in context.Events_RegistrationType on r.RegistrationType_ID equals rt.RegistrationType_ID
                                               where r.GroupLeader_ID == Group_ID && rt.Conference_ID == Conference_ID && r.Registration_ID != Registration_ID
                                               select r).FirstOrDefault();
                    if (currentRegistration != null)
                    {
                        pendingApp.Registration_ID = currentRegistration.Registration_ID;
                        context.SaveChanges();
                    }
                } 

                _repository.RemoveRegistration(Registration_ID, user_id);
                var bulkRego = (from b in context.Events_GroupBulkRegistration
                                where b.GroupLeader_ID == Group_ID && b.RegistrationType_ID == registrationtype_id && b.Deleted == false
                                select b).FirstOrDefault();
                if(bulkRego.Quantity <= 1)
                {
                    bulkRego.Deleted = true;
                }
                else
                {
                    bulkRego.Quantity = bulkRego.Quantity - 1;
                }
                context.SaveChanges();
                return RedirectToAction("GroupDetails", "Registration", new { Group_ID = Group_ID, Conference_ID = Conference_ID });
            }
        }

        //public ActionResult EditRegistrant(int Contact_ID, int Conference_ID)
        //{
        //    return RedirectToAction("ContactDetails", "Registration", new { Contact_ID = Contact_ID, Conference_ID = Conference_ID });
        //}
        #endregion

        public ActionResult AddNewRego(Dictionary<int, int> type, int GroupLeader_ID, int Conference_ID)
        {
            var bulk = new BulkRegistration();
            var registration = new Registration();

            var purchases = type.Where(x => x.Value != 0); // Filter wanted rego types

            bulk.GroupLeader_ID = GroupLeader_ID;
            var items = purchases.ToDictionary(id => id.Key, qty => qty.Value);
            var user_id = CurrentContext.Instance.User.ContactID;

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                bulk.Venue_ID = (from ev in context.Events_Venue
                                 where ev.Conference_ID == Conference_ID
                                 select ev.Venue_ID).FirstOrDefault();

                registration.VenueId = bulk.Venue_ID;
                registration.GroupLeader_ID = GroupLeader_ID;
                registration.ContactID = GroupLeader_ID;
                registration.RegoDiscount = 0;
                registration.RegistrationDate = DateTime.Now;
                registration.RegisteredBy_ID = user_id;
                registration.Attended = false;
                registration.Volunteer = false;
                registration.User_ID = user_id;
                registration.VolunteerDepartment_ID = null;


                foreach (var item in items)
                {
                    bulk.RegistrationType_ID = item.Key;
                    bulk.Quantity = item.Value;

                    registration.RegoType_ID = item.Key;

                    // add bulk registrations to database
                    _repository.InsertBulkRegistration(bulk, user_id);
                    // add registration - auto allocation
                    for(int i = 0; i < bulk.Quantity; i++)
                    {
                        _repository.InsertRegistration(registration);
                    }           
                }
            }

            return RedirectToAction("GroupDetails", "Registration", new { Group_ID = GroupLeader_ID, Conference_ID = Conference_ID });
        }

        public ActionResult UpdateGroupPayment(int Payment_ID, string TransactionRef, string Comment)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var groupPayment = (from gp in context.Events_GroupPayment
                                    where gp.GroupPayment_ID == Payment_ID
                                    select gp).FirstOrDefault();

                if (groupPayment != null)
                {
                    groupPayment.CCTransactionRef = TransactionRef;
                    groupPayment.Comment = Comment;
                    context.SaveChanges();
                }
            }
            return Json(new { }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult confirmAllocation(int BulkRegistration_ID)
        {            
            var has_allocation = false;
            var allocations = new List<Registration>();


            using (PlanetshakersEntities entity = new PlanetshakersEntities())
            {
                var bulk_rego = new Events_GroupBulkRegistration();

                try
                {
                    bulk_rego = (from br in entity.Events_GroupBulkRegistration
                                 where br.GroupBulkRegistration_ID == BulkRegistration_ID
                                 select br).SingleOrDefault();
                }
                catch (Exception e)
                {
                    throw e;
                }

                var complimentary_regtype_id = 2889; // PS20 Boom Camp Complimentary Rego

                // Boom Camp Full Registration exceptions
                if (bulk_rego.RegistrationType_ID == 2874 || bulk_rego.RegistrationType_ID == 2876)
                {
                    var complimentary = (from br in entity.Events_GroupBulkRegistration
                                         where br.RegistrationType_ID == complimentary_regtype_id
                                         && br.GroupLeader_ID == bulk_rego.GroupLeader_ID
                                         select br.GroupBulkRegistration_ID);

                    if (complimentary.Any())
                    {
                        allocations = (from r in entity.Events_Registration
                                       join c in entity.Common_Contact
                                        on r.Contact_ID equals c.Contact_ID
                                       where r.GroupLeader_ID == bulk_rego.GroupLeader_ID
                                       && r.RegistrationType_ID == complimentary_regtype_id
                                       select new Registration {
                                           ID = r.Registration_ID,
                                           FirstName = c.FirstName,
                                           LastName = c.LastName,
                                           RegoType_ID = r.RegistrationType_ID,
                                           RegistrationDate = r.RegistrationDate
                                       }).ToList();

                        if (allocations.Any())
                        {
                            has_allocation = true;
                        }
                    }
                }

                if (has_allocation)
                {
                    // return a list of allocations to be selected for removal
                    return Json(allocations);
                }
            }

            return Json(allocations);
        }

        public ActionResult RemoveBulkRego(int BulkRegistration_ID, int Conference_ID)
        {
            var user_id = CurrentContext.Instance.User.ContactID;

            _repository.DeleteBulkRegistration(BulkRegistration_ID, user_id);

            return Json(new { }, JsonRequestBehavior.AllowGet);
        }
        
        /** Remove combo allocations when removing another registration **/
        public void RemoveComplimentaryRego(List<int> Registration_IDs, int Group_ID, int RegType_ID)
        {
            var user_id = CurrentContext.Instance.User.ContactID;

            using (PlanetshakersEntities entity = new PlanetshakersEntities())
            {
                var bulk_id = (from br in entity.Events_GroupBulkRegistration
                            where br.GroupLeader_ID == Group_ID && br.RegistrationType_ID == RegType_ID && br.Quantity == Registration_IDs.Count()
                            select br.GroupBulkRegistration_ID).FirstOrDefault();

                if (bulk_id > 0)
                {
                    _repository.DeleteBulkRegistration(bulk_id, user_id);
                } else
                {
                    ViewBag.ErrorMessage = "Error finding bulk registration.";
                    return;
                }
            }

            // Can change this into a sql string and use 'in' instead of '='
            // var result = string.Join(";", data); - joins list of int to a delimited string
            foreach (int reg in Registration_IDs)
            {
                _repository.RemoveRegistration(reg, user_id);
            }
        }



        #endregion

        #region Group Payment
        public ActionResult AddGroupPayment(int Group_ID, int Conference_ID, string PaymentCategory, int PaymentType_ID, int PaymentSource_ID, decimal PaymentAmount, string Comment, string promo)
        {
            if (PaymentType_ID == 3) // Credit card payment
            {
                var cartModel = new CartModel(Conference_ID);
                cartModel.ContactId = Group_ID;
                cartModel.Cart = new List<CartItem>{
                    new CartItem(){
                        quantity = 1,
                        reg_name = "Group Payment",
                        event_name = cartModel.Event.name,
                        single_price = PaymentAmount
                    }
                };

                cartModel.Comments = Comment;

                Session["Cart"] = cartModel;

                // Need to write a new function to take in subtotal amount
                return RedirectToAction("Summary", "Payment");
            } else
            {
                //By passing summary so that the payment can go through without stripe
                //Yet remain the same credit card payment type by reassigning payment type id
                if (PaymentType_ID == 10000)
                {
                    PaymentType_ID = 3;
                }

                // Should check for payment category before checking for payment type
                if (PaymentCategory == "Refund")
                {
                    var payment = new Payment();
                    payment.Group_ID = Group_ID;
                    payment.Conference_ID = Conference_ID;
                    payment.PaymentType_ID = PaymentType_ID;
                    payment.PaymentSource_ID = PaymentSource_ID;
                    payment.Category = PaymentCategory;
                    payment.Amount = ((PaymentCategory == "Refund" && PaymentAmount > 0) ? PaymentAmount * -1 : PaymentAmount);
                    payment.StartDate = DateTime.Now;
                    payment.CompletedDate = DateTime.Now;
                    payment.Completed = true;
                    payment.Comment = Comment;
                    payment.PaymentBy_ID = CurrentContext.Instance.User.ContactID;
                    using (PlanetshakersEntities context = new PlanetshakersEntities())
                    {
                        payment.BankAccount_ID = (from ec in context.Events_Conference
                                                  where ec.Conference_ID == Conference_ID
                                                  select ec.BankAccount_ID).FirstOrDefault();
                    }
                    payment.User_ID = CurrentContext.Instance.User.ContactID;

                    _repository.InsertGroupPayment(payment);
                } else
                {                    
                    var payment = new Payment();
                    payment.Group_ID = Group_ID;
                    payment.Conference_ID = Conference_ID;
                    payment.PaymentType_ID = PaymentType_ID;
                    payment.PaymentSource_ID = PaymentSource_ID;
                    payment.Category = PaymentCategory;
                    payment.Amount = ((PaymentCategory == "Refund" && PaymentAmount > 0) ? PaymentAmount * -1 : PaymentAmount);
                    payment.StartDate = DateTime.Now;
                    payment.CompletedDate = DateTime.Now;
                    payment.Completed = true;
                    payment.Comment = Comment;
                    payment.PaymentBy_ID = CurrentContext.Instance.User.ContactID;
                    using (PlanetshakersEntities context = new PlanetshakersEntities())
                    {
                        payment.BankAccount_ID = (from ec in context.Events_Conference
                                                    where ec.Conference_ID == Conference_ID
                                                    select ec.BankAccount_ID).FirstOrDefault();
                    }
                    payment.User_ID = CurrentContext.Instance.User.ContactID;

                    _repository.InsertGroupPayment(payment);
                }
                
            }

            return RedirectToAction("GroupDetails", "Registration", new { Group_ID = Group_ID, Conference_ID = Conference_ID });
        }

        [HttpPost]
        public ActionResult RefundGroupPayment(int Payment_ID, string transactionRef, string Comment)
        {
            if (!String.IsNullOrEmpty(transactionRef))
            {
                try
                {
                    var bank_account = new Common_BankAccount();

                    using (PlanetshakersEntities context = new PlanetshakersEntities())
                    {
                        // stripe account based on payment
                        var ba_id = (from p in context.Events_GroupPayment
                                     where p.GroupPayment_ID == Payment_ID
                                     select p.BankAccount_ID).FirstOrDefault();

                        if (ba_id > 0)
                        {
                            bank_account = (from ba in context.Common_BankAccount
                                            where ba.BankAccount_ID == ba_id
                                            select ba).FirstOrDefault();
                        }
                    }

                    StripeConfiguration.ApiKey = bank_account.SecretKey;

                    // refund method differs between payment intent and charge type
                    if (transactionRef.Contains("pi_"))
                    {
                        var refundService = new RefundService();
                        var refundOptions = new RefundCreateOptions
                        {
                            Reason = "requested_by_customer",
                            PaymentIntent = transactionRef
                        };

                        Refund refund = refundService.Create(refundOptions);

                    }
                    else if (transactionRef.Contains("ch_"))
                    {
                        var refundService = new RefundService();
                        var refundOptions = new RefundCreateOptions
                        {
                            Reason = "requested_by_customer",
                            Charge = transactionRef
                        };

                        Refund refund = refundService.Create(refundOptions);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                    // TODO: Throw meaningful error to UI
                }
            }

            var payment = new Payment();

            var now = DateTime.Now;

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                payment = (from ecp in context.Events_GroupPayment
                           where ecp.GroupPayment_ID == Payment_ID
                           select new Payment
                           {
                               ID = ecp.GroupPayment_ID,
                               Group_ID = ecp.GroupLeader_ID,
                               Category = "Refund",
                               Conference_ID = ecp.Conference_ID,
                               PaymentType_ID = ecp.PaymentType_ID,
                               PaymentSource_ID = ecp.PaymentSource_ID,
                               Amount = ecp.PaymentAmount,
                               StartDate = ecp.PaymentStartDate,
                               CompletedDate = now,
                               Completed = true,
                               PaymentBy_ID = CurrentContext.Instance.User.ContactID,
                               BankAccount_ID = ecp.BankAccount_ID
                           }).FirstOrDefault();

                payment.Amount *= -1;
                payment.Comment = Comment;
            }

            payment.User_ID = CurrentContext.Instance.User.ContactID;

            var id = _repository.InsertGroupPayment(payment);

            //return RedirectToAction("GroupDetails", "Registration", new { Group_ID = payment.Group_ID, Conference_ID = payment.Conference_ID });
            return Json(new { });
        }

        public ActionResult RemoveGroupPayment(int Payment_ID, int Group_ID, int Conference_ID)
        {
            var user_id = CurrentContext.Instance.User.ContactID;

            _repository.RemoveGroupPayment(Payment_ID, user_id);

            return RedirectToAction("GroupDetails", "Registration", new { Group_ID = Group_ID, Conference_ID = Conference_ID });
        }

        public JsonResult AddGroupComment(int group_id, int conference_id, string new_comment)
        {
            var comment_item = new CommentItem();
            var now = DateTime.Now;
            var user_id = CurrentContext.Instance.User.ContactID;
            var new_commentitem = new Events_GroupComment();

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                new_commentitem.Conference_ID = conference_id;
                new_commentitem.Group_ID = group_id;
                new_commentitem.Comment = new_comment;
                new_commentitem.Date = now;
                new_commentitem.CommentBy = user_id;

                context.Events_GroupComment.Add(new_commentitem);
                context.SaveChanges();
            }

            comment_item.Id = new_commentitem.GroupComment_ID;
            comment_item.Comment = new_comment;
            comment_item.CommentDate = now;
            comment_item.sCommentDate = now.ToString();
            comment_item.CommenterFirstName = CurrentContext.Instance.User.FirstName;
            comment_item.CommenterLastName = CurrentContext.Instance.User.LastName;

            return Json(comment_item);
        }

        public JsonResult RemoveGroupComment(int comment_id)
        {
            var user_id = CurrentContext.Instance.User.ContactID;
            var commentItem = new Events_GroupComment();

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                commentItem = (from gc in context.Events_GroupComment
                               where gc.GroupComment_ID == comment_id
                               select gc).FirstOrDefault();

                context.Events_GroupComment.Remove(commentItem);
                context.SaveChanges();
            }

            return Json(commentItem);
        }

        #endregion

        #region Emails
        public ActionResult SendConfirmationEmail(int Registration_ID)
        {
            var errorMessage = "";

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var rego = (from er in context.Events_Registration
                            where er.Registration_ID == Registration_ID
                            select er).FirstOrDefault();

                var contact = (from cc in context.Common_Contact
                               where cc.Contact_ID == rego.Contact_ID
                               select cc).FirstOrDefault();

                var conference = (from ev in context.Events_Venue
                                  join ec in context.Events_Conference
                                    on ev.Conference_ID equals ec.Conference_ID
                                  where ev.Venue_ID == rego.Venue_ID
                                  select new Models.Event
                                  {
                                      id = ec.Conference_ID,
                                      venue_id = ev.Venue_ID,
                                      name = ec.ConferenceName,
                                      date = ec.ConferenceDate,
                                      location = ec.ConferenceLocation,
                                      venueLocation = ev.VenueName
                                  }).FirstOrDefault();

                var confirmationBarcode = new ConfirmationBarcode()
                {
                    Subject = "Quick Check In - " + conference.name,
                    EventName = conference.name,
                    EventDate = conference.date.ToString(),
                    EventVenue = conference.location,
                    VenueLocation = conference.venueLocation,
                    Email = (String.IsNullOrEmpty(contact.Email)) ? contact.Email2 : contact.Email,
                    RegistrationNumber = Registration_ID.ToString()
                };

                try
                {
                    if (!String.IsNullOrEmpty(confirmationBarcode.Email))
                    {
                        new MailController().ConfirmationBarcodeEmail(confirmationBarcode);
                        var cartModel = new CartModel(contact.Contact_ID, conference);
                        CreateEventLog(cartModel, false, "Update", "Sent Confirmation Barcode Email");
                    }

                    errorMessage = "Email successfully sent!";

                }
                catch (Exception ex)
                {
                    errorMessage = ex.Message;
                }
            }

            return Json("errorMessage", JsonRequestBehavior.AllowGet);
        }

        //Old Code for sending tax invoice
        /*
        public ActionResult SendTaxInvoice(List<int> GroupBulk_IDs)
        {
            var errorMessage = "";

            // Add error message when no selected ID
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                // Change this to group bulk rego
                var regos = (from egbr in context.Events_GroupBulkRegistration
                             where GroupBulk_IDs.Contains(egbr.GroupBulkRegistration_ID)
                             select egbr).ToList();

                foreach (var item in regos)
                {
                    var contact = (from cc in context.Common_Contact
                                   where cc.Contact_ID == item.GroupLeader_ID
                                   select cc).FirstOrDefault();

                    var conf = (from ev in context.Events_Venue
                                join ec in context.Events_Conference
                                on ev.Conference_ID equals ec.Conference_ID
                                where ev.Venue_ID == item.Venue_ID
                                select ec).FirstOrDefault();

                    var bank_acc = (from ba in context.Common_BankAccount
                                    where ba.BankAccount_ID == conf.BankAccount_ID
                                    select ba).FirstOrDefault();

                    var reg_type = (from ert in context.Events_RegistrationType
                                    where ert.RegistrationType_ID == item.RegistrationType_ID
                                    select ert).FirstOrDefault();

                    var invoice_model = new RegistrationReceiptEmailModel()
                    {
                        ContactId = item.GroupLeader_ID,
                        FullName = contact.FirstName + " " + contact.LastName,
                        Email = contact.Email,
                        Event = new Models.Event()
                        {
                            id = conf.Conference_ID,
                            name = conf.ConferenceName,
                            date = conf.ConferenceDate,
                            location = conf.ConferenceLocation,
                            currency = bank_acc.Currency,
                            venue_id = item.Venue_ID
                        },
                        Items = new List<RegistrationConfirmationItem>() {
                            new RegistrationConfirmationItem
                            {
                                reference_number = item.GroupBulkRegistration_ID.ToString(),
                                reg_name = reg_type.RegistrationType,
                                qty = item.Quantity,
                                price = reg_type.RegistrationCost * item.Quantity
                            }
                        }
                    };
                    invoice_model.EventName = invoice_model.Event.name;
                    invoice_model.Currency = invoice_model.Event.currency;
                    invoice_model.Subject = "Registration Confirmation - " + invoice_model.Event.name;

                    try
                    {
                        if (!String.IsNullOrEmpty(invoice_model.Email))
                        {
                            new MailController().RegistrationReceipt(invoice_model);

                            var is_group_rego = invoice_model.QuantityTotal > 1;
                            var cartModel = new CartModel(invoice_model.ContactId, invoice_model.Event);
                            CreateEventLog(cartModel, is_group_rego, "Update", "Sent Tax Invoice Email");
                        }

                        errorMessage = "Email successfully sent!";

                    }
                    catch (Exception ex)
                    {
                        errorMessage = ex.Message;
                    }
                }
            }

            return Json(errorMessage, JsonRequestBehavior.AllowGet);
        }*/

        public ActionResult SendTaxInvoice(int GroupLeader_ID, int Conference_ID)
        {
            var errorMessage = "";

            // Add error message when no selected ID
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var contact = (from cc in context.Common_Contact
                               where cc.Contact_ID == GroupLeader_ID
                               select cc).FirstOrDefault();

                var conf = (from ec in context.Events_Conference
                            where ec.Conference_ID == Conference_ID
                            select ec).FirstOrDefault();

                var bank_acc = (from ba in context.Common_BankAccount
                                where ba.BankAccount_ID == conf.BankAccount_ID
                                select ba).FirstOrDefault();

                var cartModel = new CartModel(Conference_ID);

                var paymentList = (from gp in context.Events_GroupPayment
                                   join pt in context.Common_PaymentType on gp.PaymentType_ID equals pt.PaymentType_ID
                                   join ps in context.Common_PaymentSource on gp.PaymentSource_ID equals ps.PaymentSource_ID
                                   where gp.Conference_ID == Conference_ID 
                                   && gp.GroupLeader_ID == GroupLeader_ID
                                   && (gp.ExpiredIntent != true || gp.ExpiredIntent == null)
                                   select new GroupPayment{
                                       GroupPayment_ID = gp.GroupPayment_ID,
                                       CCTransactionRef = gp.CCTransactionRef,
                                       VoucherID = gp.Voucher_ID,
                                       PaymentType_ID = gp.PaymentType_ID,
                                       PaymentType = pt.PaymentType,
                                       PaymentSource = ps.PaymentSource,
                                       PaymentSource_ID = ps.PaymentSource_ID,
                                       PaymentAmount = gp.PaymentAmount
                                   }).ToList();

                var email_model = new RegistrationReceiptEmailModel()
                {
                    Email = contact.Email,
                    ContactId = contact.Contact_ID,
                    Event = cartModel.Event,
                    EventName = cartModel.Event.name,
                    EventDate = cartModel.Event.date,
                    EventVenue = cartModel.Event.location,
                    Currency = cartModel.Event.currency,
                    FullName = contact.FirstName + " " + contact.LastName,
                    Subject = "Registration Confirmation - " + cartModel.Event.name
                };

                email_model.Items = (from gbr in context.Events_GroupBulkRegistration
                                     join rt in context.Events_RegistrationType
                                     on gbr.RegistrationType_ID equals rt.RegistrationType_ID
                                     where gbr.Venue_ID == cartModel.Event.venue_id && gbr.GroupLeader_ID == GroupLeader_ID && gbr.Deleted == false
                                     select new RegistrationConfirmationItem
                                     {
                                         reg_name = rt.RegistrationType,
                                         qty = gbr.Quantity,
                                         price = rt.RegistrationCost
                                     }).ToList();
                
                //variable to determine if the item in payment list is the last later
                email_model.Promo = new Voucher { value = 0, VoucherValue = 0, 
                                                code = "", applied_value = 0 };

                foreach (var payment in paymentList)
                {
                    email_model.PaymentList += "<tr>";
                    email_model.PaymentList += "<td>" + payment.PaymentType + "</td>";
                    email_model.PaymentList += "<td>" + (!String.IsNullOrEmpty(payment.CCTransactionRef) ? payment.CCTransactionRef : "N/A") + "</td>";
                    
                    email_model.PaymentList += "<td>" + email_model.Event.currency + " " + payment.PaymentAmount.ToString("0.00") + "</td><tr>";
                    email_model.PaymentTotal += payment.PaymentAmount;
                }

                try
                {
                    if (!String.IsNullOrEmpty(email_model.Email))
                    {
                        new MailController().RegistrationTaxInvoiceReceipt(email_model);

                        var is_group_rego = true;
                        cartModel.ContactId = GroupLeader_ID;
                        CreateEventLog(cartModel, is_group_rego, "Update", "Sent Tax Invoice Email");
                    }

                    errorMessage = "Email successfully sent!";

                }
                catch (Exception ex)
                {
                    errorMessage = ex.Message;
                }
            }

            return Json(errorMessage, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Update Details
        /** Change primary email of given contact_id **/
        public JsonResult changeDetails(int cid, string newEmail, string newMobile, string newFirstName, string newLastName, string newPassword)
        {
            var message = "";

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var contact = (from cc in context.Common_Contact
                               where cc.Contact_ID == cid
                               select cc).FirstOrDefault();

                var xemail = (from cc in context.Common_Contact
                              where cc.Email == newEmail
                              select cc).Any();

                if (contact.Email != newEmail && xemail)
                {
                    message = "Email is already in use.";
                    return Json(message, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    contact.Email = newEmail;
                }

                contact.FirstName = newFirstName;
                contact.LastName = newLastName;
                contact.Mobile = newMobile;

                if (newPassword != "default-password")
                    contact.Password = newPassword;

                context.SaveChanges();
            }

            message = "Details saved!";
            return Json(message, JsonRequestBehavior.AllowGet);
        }

        public JsonResult changeGroupDetails(int gid, string newGroupName, string newEmail, string newMobile, string newPassword)
        {
            var message = "";

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var group = (from eg in context.Events_Group
                             where eg.GroupLeader_ID == gid
                             select eg).FirstOrDefault();

                if (group.GroupName != newGroupName)
                    group.GroupName = newGroupName;

                var contact = (from cc in context.Common_Contact
                               where cc.Contact_ID == gid
                               select cc).FirstOrDefault();

                var xemail = (from cc in context.Common_Contact
                              where cc.Email == newEmail
                              select cc).Any();

                if (contact.Email != newEmail && xemail)
                {
                    message = "Email is already in use.";
                    return Json(message, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    contact.Email = newEmail;
                }

                if (contact.Mobile != newMobile)
                    contact.Mobile = newMobile;

                if (newPassword != "default-password")
                    contact.Password = newPassword;

                context.SaveChanges();
            }

            message = "Details saved!";
            return Json(message, JsonRequestBehavior.AllowGet);
        }

        /** Update quantity of given Group Bulk Registrations ID **/
        public JsonResult updatePurchase(int reg_id, int newQty)
        {
            var message = "";

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var bulk_rego = (from egbr in context.Events_GroupBulkRegistration
                                 where egbr.GroupBulkRegistration_ID == reg_id
                                 select egbr).FirstOrDefault();

                var conf_id = (from ev in context.Events_Venue
                               where ev.Venue_ID == bulk_rego.Venue_ID
                               select ev.Conference_ID).FirstOrDefault();

                if (bulk_rego != null)
                {
                    bulk_rego.Quantity = newQty;
                    context.SaveChanges();

                    message = "Update successful.";
                }
                else
                {
                    message = "Bulk Registration does not exist.";
                }

                return Json(new
                {
                    message = message,
                    link = Url.Action("GroupDetails", "Registration", new { Group_ID = bulk_rego.GroupLeader_ID, Conference_ID = conf_id })
                }, JsonRequestBehavior.AllowGet);
            }

            
        }
        #endregion

        #region Helper Functions

        // Retrieve Rego Types based on conference_ID selected
        public ActionResult GetRegoType(int Conference_ID)
        {
            var model = new SearchViewModel();

            model.Conference_ID = Conference_ID;

            var lst = model.RegistrationTypeList;

            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        //public ActionResult GetPayment(int Payment_ID)
        //{
        //    var model = new SearchViewModel();

        //    model.Pay_ID = Payment_ID;

        //    var payment = model.selectedPayment;
        //    var accessroles = new List<int> { 1, 21, 62 };

        //    var isAdmin = false;
        //    using (PlanetshakersEntities context = new PlanetshakersEntities())
        //    {
        //        isAdmin = (from ccr in context.Common_ContactDatabaseRole
        //                   where ccr.Contact_ID == CurrentContext.Instance.User.ContactID && accessroles.Contains(ccr.DatabaseRole_ID)
        //                   select ccr).Any();
        //    }
                
        //    return Json(new { payment, isAdmin  }, JsonRequestBehavior.AllowGet);
        //}

        public ActionResult GetGroupPayment(int Payment_ID)
        {
            var model = new SearchViewModel();
            var bank_account = new Common_BankAccount();

            model.Pay_ID = Payment_ID;

            var payment = model.selectedGroupPayment;
            var accessroles = new List<int> { 17 }; // Admins and Super Admin
            var refunded = false;
            var isAdmin = false;
            var intentNotFound = false;
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                isAdmin = (from ccr in context.Common_ContactDatabaseRole
                           where ccr.Contact_ID == CurrentContext.Instance.User.ContactID && accessroles.Contains(ccr.DatabaseRole_ID) 
                           select ccr).Any();

                var ba_id = (from p in context.Events_GroupPayment
                             where p.GroupPayment_ID == Payment_ID
                             select p.BankAccount_ID).FirstOrDefault();

                if (ba_id > 0)
                {
                    bank_account = (from ba in context.Common_BankAccount
                                    where ba.BankAccount_ID == ba_id
                                    select ba).FirstOrDefault();
                }

                //check stripe for refund
                StripeConfiguration.ApiKey = bank_account.SecretKey;
                if (payment.TransactionRef != "" && !payment.TransactionRef.Contains("ref:"))
                {
                    var paymentservice = new PaymentIntentService();
                    var chargeservice = new ChargeService();
                    var refundservice = new RefundService();
                    if (payment.TransactionRef.Contains("ch_"))
                    {
                        Charge charge = chargeservice.Get(payment.TransactionRef);
                        if (charge.Refunds.Data.Count() != 0 && charge.Refunded)
                        {
                            Refund refund = refundservice.Get(charge.Refunds.Data[0].Id);
                            if (refund.Status == "succeeded")
                            {
                                refunded = true;
                            }
                        }
                    }
                    else if (payment.TransactionRef.Contains("pi_"))
                    {
                        PaymentIntent paymentIntent = paymentservice.Get(payment.TransactionRef);
                        if (paymentIntent.Charges.Data.Count() != 0)
                        {
                            Charge charge = chargeservice.Get(paymentIntent.Charges.Data[0].Id);
                            if (charge.Refunds.Data.Count() != 0 && charge.Refunded)
                            {
                                Refund refund = refundservice.Get(charge.Refunds.Data[0].Id);
                                if (refund.Status == "succeeded")
                                {
                                    refunded = true;
                                }
                            }
                        }
                    } else
                    {
                        intentNotFound = true;
                    }
                }
            }
            PaymentDetailModel returnModel = new PaymentDetailModel()
            {
                payment = payment,
                isAdmin = isAdmin,
                refunded = refunded,
                intentNotFound = intentNotFound
            };
            return View("_PaymentDetailsModal", returnModel);
            //return Json(new { payment, isAdmin, refunded, intentNotFound }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetPaymentType(int Payment_ID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var paymenttype = (from gp in context.Events_GroupPayment
                                   join pt in context.Common_PaymentType on gp.PaymentType_ID equals pt.PaymentType_ID
                                   where gp.GroupPayment_ID == Payment_ID
                                   select pt.PaymentType).FirstOrDefault();
            return Json(new { paymenttype }, JsonRequestBehavior.AllowGet);
            }
        }

            // THIS THING NEEDS TO CHANGE
            public ActionResult GetSearchRegistrant(string query)
        {
            var model = new SearchViewModel();
            model.Search = new SearchModel();

            model.Search.QuickSearch = query;

            var lst = _repository.FindSingle(model.Search);

            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        private void CreateEventLog(CartModel model, bool is_group_rego, string action, string item)
        {
            var cartModel = Session["Cart"] as CartModel;
            var user_id = CurrentContext.Instance.User.ContactID;

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                
                var new_history = new Common_ContactHistory();
                new_history.Contact_ID = model.ContactId;
                new_history.Module = "Events";
                new_history.Desc = String.Empty;
                new_history.Desc2 = String.Empty;
                new_history.DateChanged = DateTime.Now;
                new_history.User_ID = user_id;
                new_history.Action = action;
                new_history.Item = item;
                new_history.Table = String.Empty;
                new_history.FieldName = String.Empty;
                new_history.OldValue = String.Empty;
                new_history.OldText = String.Empty;
                new_history.NewValue = String.Empty;
                new_history.NewText = String.Empty;
                context.Common_ContactHistory.Add(new_history);

                context.SaveChanges();
            }
        }

        #endregion

        #region Files functions
        [HttpPost]
        public ActionResult DownloadFile(int Document_ID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var doc = (from d in context.Volunteer_ApplicationDocument
                           where d.Document_ID == Document_ID
                           select d).FirstOrDefault();
                if (doc != null)
                {
                    FileModel fileModel = new FileModel("s3-amazon-believe-application-documents");
                    ResourcesItem item = fileModel.GetFile(doc.Contact_ID.ToString(), doc.S3Filename);
                    return Json(new { url = item.DownloadUrl, name = doc.OriginalFilename });
                }
                else
                {
                    return Json(new { });
                }
            }
        }

        [HttpPost]
        public bool ApproveFile(int Document_ID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var doc = (from d in context.Volunteer_ApplicationDocument
                           where d.Document_ID == Document_ID
                           select d).FirstOrDefault();
                if (doc != null)
                {
                    doc.Approved = true;
                    context.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        [HttpPost]
        public bool UnapproveFile(int Document_ID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var doc = (from d in context.Volunteer_ApplicationDocument
                           where d.Document_ID == Document_ID
                           select d).FirstOrDefault();
                if (doc != null)
                {
                    doc.Approved = false;
                    context.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        #endregion

        #region Comments
        public JsonResult AddComment(int contact_id, int conference_id, string new_comment)
        {
            var comment_item = new CommentItem();
            var now = DateTime.Now;
            var user_id = CurrentContext.Instance.User.ContactID;
            var new_commentitem = new Events_ContactComment();

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                new_commentitem.Conference_ID = conference_id;
                new_commentitem.Contact_ID = contact_id;
                new_commentitem.Comment = new_comment;
                new_commentitem.Date = now;
                new_commentitem.CommentBy = user_id;

                context.Events_ContactComment.Add(new_commentitem);
                context.SaveChanges();
            }

            comment_item.Id = new_commentitem.ContactComment_ID;
            comment_item.Comment = new_comment;
            comment_item.CommentDate = now;
            comment_item.sCommentDate = now.ToString();
            comment_item.CommenterFirstName = CurrentContext.Instance.User.FirstName;
            comment_item.CommenterLastName = CurrentContext.Instance.User.LastName;

            return Json(comment_item);
        }

        public JsonResult RemoveComment(int comment_id)
        {
            var user_id = CurrentContext.Instance.User.ContactID;
            var commentItem = new Events_ContactComment();

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                commentItem = (from gc in context.Events_ContactComment
                               where gc.ContactComment_ID == comment_id
                               select gc).FirstOrDefault();

                context.Events_ContactComment.Remove(commentItem);
                context.SaveChanges();
            }

            return Json(commentItem);
        }
        #endregion

        #region Search Event Registrations
        public ActionResult EventSearch()
        {
            EventSearchViewModel model = new EventSearchViewModel
            {
                RegistrationTypeList = new List<SelectListItem>()
            };
            return View(model);
        }
        public ActionResult SearchEventRegistrations(int Event_ID, List<int> RegistrationType_IDs)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                EventSearchViewModel model = new EventSearchViewModel();
                model.SelectedConferenceID = Event_ID;
                var list = (from r in context.Events_Registration
                            join c in context.Common_Contact on r.Contact_ID equals c.Contact_ID
                            join e in context.Events_RegistrationType on r.RegistrationType_ID equals e.RegistrationType_ID
                            where e.Conference_ID == Event_ID
                            select new RegoSearchReult
                            {
                                Contact_ID = r.Contact_ID,
                                FirstName = c.FirstName,
                                LastName = c.LastName,
                                RegistrationType_ID = r.RegistrationType_ID,
                                RegoType = e.RegistrationType,
                                applicationType = e.ApplicationRegistrationType

                            }).ToList();
                if (RegistrationType_IDs != null)
                {
                    list = list.Where(x => RegistrationType_IDs.Contains(x.RegistrationType_ID)).ToList();
                } else
                {
                    list = list.Where(x => x.applicationType == false).ToList();
                }
                var distinctItems = list.Distinct(new DistinctItemComparer()).ToList();

                //mark any duplicated allocation as incomplete 
                checkSponsorshipDuplicate(Event_ID);

                foreach (var item in distinctItems)
                {
                    var totalCost = (from r in context.Events_GroupBulkRegistration
                                     join rt in context.Events_RegistrationType on r.RegistrationType_ID equals rt.RegistrationType_ID
                                     where rt.Conference_ID == Event_ID && r.GroupLeader_ID == item.Contact_ID && r.Deleted == false
                                     select rt.RegistrationCost * r.Quantity).Sum();
                    var totalPaymentList = (from p in context.Events_GroupPayment
                                            where p.Conference_ID == Event_ID
                                                && p.GroupLeader_ID == item.Contact_ID
                                                && (p.ExpiredIntent == false || p.ExpiredIntent == null)
                                                && p.PaymentCompleted == true
                                            select p.PaymentAmount).ToList();
                    var totalPayment = (totalPaymentList != null) ? totalPaymentList.Sum() : 0;
                    //add in variable payment - recipient 
                    var recipientPayment = (decimal)(from vp in context.Events_VariablePayment
                                                     where vp.Conference_ID == Event_ID 
                                                     && vp.IncomingContact_ID == null && vp.OutgoingContact_ID == item.Contact_ID && vp.PaymentCompleted == true
                                                     select vp.PaymentAmount).ToList().Sum();

                    //get sponsorship that are not transactions (only count amount that is transferred from sponsorship pool, not donated amount)
                    var sponsorshipAmount = (from v in context.Events_VariablePayment
                                             where v.IncomingContact_ID == item.Contact_ID && v.Conference_ID == Event_ID && v.CCTransactionReference == null && v.PaymentCompleted == true
                                             select v.PaymentAmount).Sum() ?? 0;

                    var outstandingBalance = totalCost - totalPayment - recipientPayment + sponsorshipAmount;
                    item.Paid = totalPayment + recipientPayment - sponsorshipAmount;
                    item.Balance = outstandingBalance;
                }
                model.SearchResult = distinctItems;
                return View("_SearchResult", model);
            }
        }
        #endregion

        #region helper function

        public void checkSponsorshipDuplicate(int Event_ID, int Contact_ID = 0)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {

                var sponsorshipAllocations = (from s in context.Events_VariablePayment
                                              where s.Conference_ID == Event_ID && s.IncomingContact_ID == null && s.OutgoingContact_ID != null && (s.PaymentCompleted ?? false)
                                              && ((Contact_ID != 0) ? s.OutgoingContact_ID == Contact_ID : true) && s.Refund == false && s.CCTransactionReference != null
                                              select s).GroupBy(x => x.CCTransactionReference);
                foreach (var transaction in sponsorshipAllocations)
                {
                    if (transaction.Count() > 1)
                    {
                        //duplicated allocation
                        foreach (var item in transaction.Skip(1))
                        {
                            item.PaymentCompleted = false;
                            item.PaymentCompletedDate = null;
                            item.Comment = "Duplicated Allocation | updated: " + DateTime.Now.ToString("dd-MMM-yy HH:mm");
                        }
                    }
                }
                context.SaveChanges();
            }
        }
        #endregion

    }

    class DistinctItemComparer : IEqualityComparer<RegoSearchReult>
    {

        public bool Equals(RegoSearchReult x, RegoSearchReult y)
        {
            return x.Contact_ID == y.Contact_ID;
        }

        public int GetHashCode(RegoSearchReult obj)
        {
            return obj.Contact_ID.GetHashCode();
        }
    }
}