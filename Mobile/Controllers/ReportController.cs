﻿using Planetshakers.Core.DataInterfaces;
using Planetshakers.Core.Models;
using Planetshakers.Events.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;
using System.Text;

namespace Planetshakers.Events.Controllers
{
    [Authorize]
    public class ReportController : Controller
    {
        private readonly IReportRepository _reportRepository;

        // Not sure if this is needed. 
        public ReportController(IReportRepository reportRepository)
        {
            _reportRepository = reportRepository;
        }

        #region View Page for Reports
        // GET: Report

        public ActionResult ApplicationReport()
        {
            var model = new ApplicationReportViewModel();
            return View(model);
        }
        public ActionResult DocumentReport()
        {
            var model = new DocumentReportViewModel();
            return View(model);
        }

        public ActionResult DataIntegrityReport()
        {
            var model = new DataIntegrityReportModel();
            return View("DataIntegrityReport", model);
        }

        public ActionResult RegistrationReport()
        {
            var model = new ReportModel();

            return View(model);
        }

        public ActionResult TransferReport()
        {
            var model = new TransferReprotViewModel();
            model.displayReport = false;
            return View(model);
        }

        public ActionResult CheckInReport()
        {
            var model = new ReportModel();
            model.displayReport = false;
            return View(model);
        }

        public ActionResult MailingListReport()
        {
            var model = new MailingListReportModel();
            model.displayReport = false;

            return View("MailingListReport", model);
        }

        public ActionResult BankingReport()
        {
            var model = new ReportModel();
            model.selectedDateOpt = 0;
            model.EndDate = DateTime.Today;
            model.StartDate = DateTime.Today.AddDays(-7);

            return View(model);
        }

        public ActionResult PurchaseReport()
        {
            var model = new ReportModel();

            return View(model);
        }

        public ActionResult SummaryReport()
        {
            var model = new ReportModel();
            model.selectedDateOpt = 0;
            model.EndDate = DateTime.Today;
            model.StartDate = DateTime.Today.AddDays(-7);

            return View(model);
        }

        public ActionResult AuditReport()
        {
            var model = new ReportModel();
            model.selectedDateOpt = 0;
            model.EndDate = DateTime.Today;
            model.StartDate = DateTime.Today.AddDays(-7);

            return View(model);
        }

        public ActionResult TotalRegistrationReport()
        {
            var model = new ReportModel();
            model.conferenceSelectionID = 0;
            model.selectedDateOpt = 0;
            model.EndDate = DateTime.Today;
            model.StartDate = DateTime.Today.AddDays(-7);
            return View(model);
        }

        public ActionResult TotalRegistrantsReport()
        {
            using (PlanetshakersEntities ctx = new PlanetshakersEntities())
            {

                var conf_list = (from EC in ctx.Events_Conference
                                 from EV in ctx.Events_Venue.Where(EV => EC.Conference_ID == EV.Conference_ID)
                                 where EV.ConferenceEndDate > DateTime.Now && EV.Conference_ID != 54
                                 orderby EV.ConferenceStartDate
                                 select new ObjConfList
                                 {
                                     Conference = EC.ConferenceName,
                                     Conference_ID = EC.Conference_ID,
                                     TotalRegistrations = (from r in ctx.Events_Registration
                                                           join rt in ctx.Events_RegistrationType
                                                           on r.RegistrationType_ID equals rt.RegistrationType_ID
                                                           where rt.Conference_ID == EC.Conference_ID && !rt.ApplicationRegistrationType && !rt.AddOnRegistrationType && rt.RegistrationCost != 0
                                                           select r).Count()
                                 }).ToList();

                foreach (var item in conf_list)
                {
                    if (item.TotalRegistrations == null)
                    {
                        item.TotalRegistrations = 0;
                    }
                }
                RegistrantReportModel model = new RegistrantReportModel()
                {
                    conferenceList = conf_list
                };

                return View(model);
            }
        }
        public ActionResult PaymentReport()
        {
            var model = new ReportModel();
            model.selectedDateOpt = 0;
            model.EndDate = DateTime.Today;
            model.StartDate = DateTime.Today.AddDays(-7);

            return View(model);
        }

        public ActionResult PaymentSummary()
        {
            var model = new ReportModel();
            return View(model);
        }
        #endregion

        #region Generate Report

        // Summary Report
        [HttpPost]
        public ActionResult GenerateSummaryReports(ReportModel model, int Conference_ID, String reportType, int selectedDateOpt, DateTime? StartDate, DateTime? EndDate)
        {
            if (selectedDateOpt != 8)
            {
                TimeSelection time = model._availableTimeSelection.Find(x => x.dateId == selectedDateOpt);
                StartDate = time.startdate;
                EndDate = time.enddate;
            }

            decimal total = Decimal.Zero;
            int totalRego = 0;
            model.displayReport = true;
            model.reportType = reportType;

            if (reportType == "Allocation")
            {
                model.RegoTypeBreakdownList = new List<RegistrationTypeAllocation>();

                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {

                    model.RegoTypeBreakdownList = (from ert in context.Events_RegistrationType
                                                   where ert.Conference_ID == Conference_ID
                                                   select new RegistrationTypeAllocation
                                                   {
                                                       RegistrationType_ID = ert.RegistrationType_ID,
                                                       RegistrationType = ert.RegistrationType
                                                   }).ToList();

                    foreach (var regoType in model.RegoTypeBreakdownList)
                    {
                        regoType.Total = (from gbr in context.Events_GroupBulkRegistration
                                          where gbr.RegistrationType_ID == regoType.RegistrationType_ID
                                          && gbr.Deleted == false
                                          select gbr.Quantity).ToList().Sum();

                        regoType.Allocated = (from er in context.Events_Registration
                                              where er.RegistrationType_ID == regoType.RegistrationType_ID
                                              && er.GroupLeader_ID != null
                                              select er).ToList().Count();
                    }
                }
            }
            else
            {
                model.eventSummary = _reportRepository.GenerateSummaryReport(Conference_ID, reportType, (DateTime)StartDate, (DateTime)EndDate);

                if (reportType == "Registration Types")
                {
                    foreach (DataTable table in model.eventSummary.ds.Tables)
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            foreach (DataColumn col in table.Columns)
                            {
                                if (col.ColumnName == "TotalCost")
                                {
                                    total += Decimal.Parse(row[col.ColumnName].ToString());
                                }
                                if (col.ColumnName == "Total")
                                {
                                    totalRego += Int32.Parse(row[col.ColumnName].ToString());
                                }
                            }
                        }
                    }
                }

                if (reportType == "Payment Types")
                {
                    foreach (DataTable table in model.eventSummary.ds.Tables)
                    {
                        foreach (DataRow row in table.Rows)
                        {
                            foreach (DataColumn col in table.Columns)
                            {
                                if (col.ColumnName == "Total")
                                {
                                    total += Decimal.Parse(row[col.ColumnName].ToString());
                                }
                            }
                        }
                    }
                }

                model.paymentSum = total;
                model.totalRego = totalRego;
            }

            return View("SummaryReport", model);
        }

        // Total Registrant Report
        public ActionResult GenerateTotalRegistrationReports(int conferenceSelectionID)
        {
            ReportModel model = new ReportModel();

            model.pastConferenceReport = _reportRepository.GenerateTotalRegistrationReport(conferenceSelectionID);

            model.displayReport = true;
            return PartialView("_TotalRegistrationReport", model);
        }

        // Payment Report
        public ActionResult GeneratePaymentReport(int Event_ID, int SelDateOpt, DateTime StartDate, DateTime EndDate, decimal minimumPaid, decimal maximumPaid)
        {
            PaymentReportViewModel model = new PaymentReportViewModel()
            {
                Event_ID = Event_ID
            };
            if (SelDateOpt != 8)
            {
                TimeSelection time = new ReportModel()._availableTimeSelection.Find(x => x.dateId == SelDateOpt);
                StartDate = time.startdate;
                EndDate = time.enddate;
            }
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                //update payment intends over 1 hour (to expired)
                var expireTime = DateTime.Now.AddHours(-1);
                var expiredIntends = (from gp in context.Events_GroupPayment
                                      where gp.PaymentCompleted == false
                                          && gp.PaymentCompletedDate == null
                                          && gp.PaymentStartDate < expireTime
                                          && (gp.ExpiredIntent != true || gp.ExpiredIntent == null)
                                      select gp).ToList();
                foreach (var item in expiredIntends)
                {
                    item.ExpiredIntent = true;
                    item.Voucher_ID = null;
                }
                context.SaveChanges();

                // group payments
                var list = (from gp in context.Events_GroupPayment
                            join cc in context.Common_Contact on gp.GroupLeader_ID equals cc.Contact_ID
                            where gp.Conference_ID == Event_ID && gp.PaymentCompleted == true
                                && gp.PaymentCompletedDate >= StartDate && gp.PaymentCompletedDate <= EndDate
                            select new PaymentReportGroup()
                            {
                                Contact_ID = cc.Contact_ID,
                                FirstName = cc.FirstName,
                                LastName = cc.LastName,
                                PaidAmount = gp.PaymentAmount
                            }).ToList();
                var report = (from gp in list
                              group gp by gp.Contact_ID into newGroup
                              select new PaymentReportInstance()
                              {
                                  Contact_ID = newGroup.Key,
                                  FirstName = newGroup.Select(x => x.FirstName).FirstOrDefault(),
                                  LastName = newGroup.Select(x => x.LastName).FirstOrDefault(),
                                  TotalPaid = newGroup.Select(x => x.PaidAmount).Sum()
                              }).ToList();

                //variable payments - being sponsored
                var sponsorshipList = (from v in context.Events_VariablePayment
                                       join cc in context.Common_Contact on v.OutgoingContact_ID equals cc.Contact_ID
                                       where v.IncomingContact_ID == null && v.PaymentCompleted == true && v.Conference_ID == Event_ID
                                            && v.PaymentCompletedDate >= StartDate && v.PaymentCompletedDate <= EndDate
                                       select new PaymentReportGroup()
                                       {
                                           Contact_ID = v.OutgoingContact_ID ?? 0,
                                           PaidAmount = v.PaymentAmount ?? 0,
                                           FirstName = cc.FirstName,
                                           LastName = cc.LastName,
                                       }).ToList();
                var sponsorSummary = (from v in sponsorshipList
                                      group v by v.Contact_ID into newGroup
                                      select new PaymentReportInstance()
                                      {
                                          Contact_ID = newGroup.Key,
                                          TotalPaid = newGroup.Select(x => x.PaidAmount).Sum(),
                                          FirstName = newGroup.Select(x => x.FirstName).FirstOrDefault(),
                                          LastName = newGroup.Select(x => x.LastName).FirstOrDefault(),
                                      }).ToList();
                foreach (var item in sponsorSummary)
                {
                    if (item.TotalPaid != 0)
                    {
                        var instance = report.Where(x => x.Contact_ID == item.Contact_ID).FirstOrDefault();
                        if (instance != null)
                        {
                            instance.TotalPaid += item.TotalPaid;
                        }
                        else
                        {
                            report.Add(item);
                        }
                    }
                }

                //sponsored someone internally
                var internalSponsor = (from v in context.Events_VariablePayment
                                       join cc in context.Common_Contact on v.IncomingContact_ID equals cc.Contact_ID
                                       where v.Conference_ID == Event_ID && v.PaymentCompleted == true
                                            && v.PaymentCompletedDate >= StartDate && v.PaymentCompletedDate <= EndDate
                                            && v.CCTransactionReference == null && v.OutgoingContact_ID == null
                                       select new PaymentReportGroup()
                                       {
                                           Contact_ID = v.IncomingContact_ID ?? 0,
                                           PaidAmount = -v.PaymentAmount ?? 0,
                                           FirstName = cc.FirstName,
                                           LastName = cc.LastName,
                                       }).ToList();
                foreach (var item in internalSponsor)
                {
                    if (item.PaidAmount != 0)
                    {
                        var instance = report.Where(x => x.Contact_ID == item.Contact_ID).FirstOrDefault();
                        if (instance != null)
                        {
                            instance.TotalPaid += item.PaidAmount;
                        }
                        else
                        {
                            PaymentReportInstance newInstance = new PaymentReportInstance()
                            {
                                FirstName = item.FirstName,
                                LastName = item.LastName,
                                Contact_ID = item.Contact_ID,
                                TotalPaid = item.PaidAmount
                            };
                            report.Add(newInstance);
                        }
                    }
                }

                model.report = report.Where(x => x.TotalPaid >= minimumPaid && x.TotalPaid <= maximumPaid).ToList();
                return PartialView("_PaymentReport", model);
            }
        }

        // Banking Report
        public ActionResult GenerateBankingReports(ReportModel model, int selectedDateOpt, int Conference_ID, DateTime? StartDate, DateTime? EndDate)
        {
            if (selectedDateOpt != 8)
            {
                TimeSelection time = model._availableTimeSelection.Find(x => x.dateId == selectedDateOpt);
                StartDate = time.startdate;
                EndDate = time.enddate;
            }
            model.Conference_ID = Conference_ID;
            model.displayReport = true;

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                //update payment intends over 1 hour (to expired)
                var expireTime = DateTime.Now.AddHours(-1);
                var expiredIntends = (from gp in context.Events_GroupPayment
                                      where gp.PaymentCompleted == false
                                          && gp.PaymentCompletedDate == null
                                          && gp.PaymentStartDate < expireTime
                                          && (gp.ExpiredIntent != true || gp.ExpiredIntent == null)
                                      select gp).ToList();
                foreach (var item in expiredIntends)
                {
                    item.ExpiredIntent = true;
                    item.Voucher_ID = null;
                }
                context.SaveChanges();

                //update duplocated sponsorship allocation (mark incomplete)
                checkSponsorshipDuplicate(Conference_ID);

                model.eventSummary = _reportRepository.GenerateBankingReport(Conference_ID, StartDate, EndDate, "", false);

                model.paymentSum = (decimal)(from vp in context.Events_VariablePayment //payment sum used for calculating total sponsorship payment
                                             where vp.Conference_ID == Conference_ID && vp.PaymentDate <= EndDate && vp.PaymentDate >= StartDate
                                             && vp.IncomingContact_ID != null && vp.OutgoingContact_ID == null && vp.PaymentCompleted == true
                                             select vp.PaymentAmount).ToList().Sum();

                model.paymentType = (from pt in context.Common_PaymentType //Finding the details of sponsorship payment type
                                     where pt.PaymentType == "Sponsorship"
                                     select pt).FirstOrDefault();

            }
            return View("BankingReport", model);
        }
        [HttpPost]
        public ActionResult GeneratePaymentSummary(int Event_ID, List<int> RegistrationType_IDs)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                //update payment intends over 1 hour (to expired)
                var expireTime = DateTime.Now.AddHours(-1);
                var expiredIntends = (from gp in context.Events_GroupPayment
                                      where gp.PaymentCompleted == false
                                          && gp.PaymentCompletedDate == null
                                          && gp.PaymentStartDate < expireTime
                                          && (gp.ExpiredIntent != true || gp.ExpiredIntent == null)
                                      select gp).ToList();
                foreach (var item in expiredIntends)
                {
                    item.ExpiredIntent = true;
                    item.Voucher_ID = null;
                }
                context.SaveChanges();

                //update duplocated sponsorship allocation (mark incomplete)
                checkSponsorshipDuplicate(Event_ID);

                RegistrationType_IDs = (RegistrationType_IDs == null) ? (from rt in context.Events_RegistrationType where rt.Conference_ID == Event_ID select rt.RegistrationType_ID).ToList() : RegistrationType_IDs;
                PaymentSummaryViewModel model = new PaymentSummaryViewModel();

                var contactList = (from r in context.Events_Registration
                                   where RegistrationType_IDs.Contains(r.RegistrationType_ID)
                                   select r.Contact_ID).Distinct().ToList();

                var list = (from gp in context.Events_GroupPayment
                            where gp.Conference_ID == Event_ID && gp.PaymentCompleted == true && contactList.Contains(gp.GroupLeader_ID)
                            select new PaymentReportGroup()
                            {
                                Contact_ID = gp.GroupLeader_ID,
                                PaidAmount = gp.PaymentAmount
                            }).ToList();
                var report = (from gp in list
                              group gp by gp.Contact_ID into newGroup
                              select new PaymentReportInstance()
                              {
                                  Contact_ID = newGroup.Key,
                                  TotalPaid = newGroup.Select(x => x.PaidAmount).Sum()
                              }).ToList();

                //variable payments - being sponsored
                var sponsorshipList = (from v in context.Events_VariablePayment
                                       where v.IncomingContact_ID == null && v.PaymentCompleted == true 
                                            && v.Conference_ID == Event_ID && contactList.Contains(v.OutgoingContact_ID ?? 0)
                                       select new PaymentReportGroup()
                                       {
                                           Contact_ID = v.OutgoingContact_ID ?? 0,
                                           PaidAmount = v.PaymentAmount ?? 0,
                                       }).ToList();
                var sponsorSummary = (from v in sponsorshipList
                                      group v by v.Contact_ID into newGroup
                                      select new PaymentReportInstance()
                                      {
                                          Contact_ID = newGroup.Key,
                                          TotalPaid = newGroup.Select(x => x.PaidAmount).Sum(),
                                      }).ToList();
                foreach (var item in sponsorSummary)
                {
                    if (item.TotalPaid != 0)
                    {
                        var instance = report.Where(x => x.Contact_ID == item.Contact_ID).FirstOrDefault();
                        if (instance != null)
                        {
                            instance.TotalPaid += item.TotalPaid;
                        }
                        else
                        {
                            report.Add(item);
                        }
                    }
                }

                //sponsored someone internally
                var internalSponsor = (from v in context.Events_VariablePayment
                                       where v.Conference_ID == Event_ID && v.PaymentCompleted == true && contactList.Contains(v.IncomingContact_ID ?? 0)
                                            && v.CCTransactionReference == null && v.OutgoingContact_ID == null
                                       select new PaymentReportGroup()
                                       {
                                           Contact_ID = v.IncomingContact_ID ?? 0,
                                           PaidAmount = -v.PaymentAmount ?? 0,
                                       }).ToList();
                foreach (var item in internalSponsor)
                {
                    if (item.PaidAmount != 0)
                    {
                        var instance = report.Where(x => x.Contact_ID == item.Contact_ID).FirstOrDefault();
                        if (instance != null)
                        {
                            instance.TotalPaid += item.PaidAmount;
                        }
                        else
                        {
                            PaymentReportInstance newInstance = new PaymentReportInstance()
                            {
                                Contact_ID = item.Contact_ID,
                                TotalPaid = item.PaidAmount
                            };
                            report.Add(newInstance);
                        }
                    }
                }
                var buckets = new List<PaymentSummaryBucket>();
                var maxValue = report.Select(x => x.TotalPaid).Max();
                var i = 0;
                while (i < maxValue)
                {

                    var bucket = new PaymentSummaryBucket()
                    {
                        minValue = (i == 0) ? i : i + 1,
                        maxValue = i + 500,
                        count = 0,
                        Name = ((i == 0) ? i : i + 1) + " - " + (i + 500)
                    };
                    buckets.Add(bucket);
                    i = i + 500;
                }
                foreach (var item in report)
                {
                    var value = item.TotalPaid;
                    var bucket = buckets.Where(x => x.minValue <= value && x.maxValue >= value).FirstOrDefault();
                    bucket.count += 1;
                }
                model.buckets = buckets;

                return PartialView("_PaymentSummary", model);
            }
        }

        [HttpPost]
        public ActionResult Banking_ManTransferBreakdown(int Event_ID)
        {
            BankingReportBreakdown model = new BankingReportBreakdown()
            {
                Event_ID = Event_ID
            };
            model.generateReport(9);
            return PartialView("_BankingReportBreakdownModal", model);
        }

        [HttpPost]
        public ActionResult Banking_SponsorshipBreakdown(int Event_ID)
        {
            BankingReportBreakdown model = new BankingReportBreakdown()
            {
                Event_ID = Event_ID
            };
            model.generateReport(12);
            return PartialView("_BankingReportBreakdownModal", model);
        }


        // Purchase Report
        public ActionResult GeneratePurchaseReports(ReportModel model, int Contact_ID, int Conference_ID)
        {
            model.displayReport = true;

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {

                //update payment intends over 1 hour for individual (to expired)
                var expireTime = DateTime.Now.AddHours(-1);
                var expiredIntends = (from gp in context.Events_GroupPayment
                                      where gp.GroupLeader_ID == Contact_ID
                                          && gp.PaymentCompleted == false
                                          && gp.PaymentCompletedDate == null
                                          && gp.PaymentStartDate < expireTime
                                          && (gp.ExpiredIntent != true || gp.ExpiredIntent == null)
                                      select gp).ToList();
                foreach (var item in expiredIntends)
                {
                    item.ExpiredIntent = true;
                    item.Voucher_ID = null;
                }
                context.SaveChanges();

                //update duplocated sponsorship allocation (mark incomplete)
                checkSponsorshipDuplicate(Conference_ID, Contact_ID);

                model.grouppayment = (from gp in context.Events_GroupPayment
                                      join cc in context.Common_Contact on gp.GroupLeader_ID equals cc.Contact_ID
                                      join ec in context.Events_Conference on gp.Conference_ID equals ec.Conference_ID
                                      join cpt in context.Common_PaymentType on gp.PaymentType_ID equals cpt.PaymentType_ID
                                      join cps in context.Common_PaymentSource on gp.PaymentSource_ID equals cps.PaymentSource_ID
                                      join cba in context.Common_BankAccount on gp.BankAccount_ID equals cba.BankAccount_ID
                                      join ccc in context.Common_Contact on gp.PaymentBy_ID equals ccc.Contact_ID
                                      where gp.GroupLeader_ID == Contact_ID
                                          && gp.Conference_ID == Conference_ID
                                          && (gp.ExpiredIntent != true || gp.ExpiredIntent == null)
                                      select new GroupPayment
                                      {
                                          GroupPayment_ID = gp.GroupPayment_ID,
                                          GroupLeader_ID = gp.GroupLeader_ID,
                                          GroupLeader_Name = cc.FirstName,
                                          Conference_ID = gp.Conference_ID,
                                          Conference_Name = ec.ConferenceName,
                                          PaymentType_ID = gp.PaymentType_ID,
                                          PaymentType = cpt.PaymentType,
                                          PaymentSource_ID = cps.PaymentSource_ID,
                                          PaymentSource = cps.PaymentSource,
                                          PaymentAmount = Math.Round(gp.PaymentAmount, 2),
                                          CCTransactionRef = gp.CCTransactionRef,
                                          CCRefund = gp.CCRefund,
                                          PaymentDate = gp.PaymentCompletedDate,
                                          PaymentBy_ID = gp.PaymentBy_ID,
                                          PaymentBy_Name = ccc.FirstName,
                                          BankAccount_ID = gp.BankAccount_ID,
                                          BankDescription = cba.Description
                                      }).ToList();

                model.bulkrego = (from bg in context.Events_GroupBulkRegistration
                                  join cc in context.Common_Contact
                                  on bg.GroupLeader_ID equals cc.Contact_ID
                                  join ev in context.Events_Venue
                                  on bg.Venue_ID equals ev.Venue_ID
                                  join rt in context.Events_RegistrationType
                                  on bg.RegistrationType_ID equals rt.RegistrationType_ID
                                  where bg.GroupLeader_ID == Contact_ID && ev.Conference_ID == Conference_ID
                                  && bg.Deleted == false
                                  select new BulkRegistration
                                  {
                                      ID = bg.GroupBulkRegistration_ID,
                                      GroupLeader_ID = bg.GroupLeader_ID,
                                      GroupLeader_Name = cc.FirstName,
                                      Venue_ID = bg.Venue_ID,
                                      Venue_Name = ev.VenueName,
                                      RegistrationType_ID = bg.RegistrationType_ID,
                                      RegistrationType = rt.RegistrationType,
                                      Quantity = bg.Quantity,
                                      DateAdded = bg.DateAdded
                                  }).ToList();
            }

            return View("PurchaseReport", model);
        }

        // Registration Report
        public ActionResult GenerateRegReports(int Conference_ID, String reportType, List<int> SelectRegoTypes)
        {
            ReportModel model = new ReportModel();
            model.displayReport = true;
            model.reportType = reportType;
            var venue_id = 0;
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                venue_id = (from v in context.Events_Venue
                            where v.Conference_ID == Conference_ID
                            select v.Venue_ID).FirstOrDefault();
            }
            model.SelectedRegoType = SelectRegoTypes;
            model.eventSummary = _reportRepository.GenerateRegReport(Conference_ID, venue_id, reportType, SelectRegoTypes);
            return PartialView("_RegistrationReport", model);
        }

        // Auditing Report
        public ActionResult GenerateAuditReport(ReportModel model, int selectedDateOpt, DateTime? StartDate, DateTime? EndDate, int Conference_ID)
        {
            if (selectedDateOpt != 8)
            {
                TimeSelection time = model._availableTimeSelection.Find(x => x.dateId == selectedDateOpt);
                StartDate = time.startdate;
                EndDate = time.enddate;
            }
            else
            {
                StartDate = (DateTime)StartDate.Value.Date;
                EndDate = (DateTime)EndDate.Value.AddDays(1);
            }

            model.displayReport = true;

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                //update payment intends over 1 hour (to expired)
                var expireTime = DateTime.Now.AddHours(-1);
                var expiredIntends = (from gp in context.Events_GroupPayment
                                      where gp.PaymentCompleted == false
                                          && gp.PaymentCompletedDate == null
                                          && gp.PaymentStartDate < expireTime
                                          && (gp.ExpiredIntent != true || gp.ExpiredIntent == null)
                                      select gp).ToList();
                foreach (var item in expiredIntends)
                {
                    item.ExpiredIntent = true;
                    item.Voucher_ID = null;
                }
                context.SaveChanges();

                //update duplocated sponsorship allocation (mark incomplete)
                checkSponsorshipDuplicate(Conference_ID);

                model.grouppayment = pullAuditReport(Conference_ID, (DateTime)StartDate, (DateTime)EndDate);
                model.paymentSum = model.grouppayment.ToList().Sum(x => x.PaymentAmount);/* + model.singlepayment.ToList().Sum(x => x.PaymentAmount);*/
            }

            return View("AuditReport", model);
        }
        [HttpPost]
        public FileResult ExportCSV_AuditReport(int Conference_ID, int selectedDateOpt, DateTime? StartDate, DateTime? EndDate)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                ReportModel model = new ReportModel();
                if (selectedDateOpt != 8)
                {
                    TimeSelection time = model._availableTimeSelection.Find(x => x.dateId == selectedDateOpt);
                    StartDate = time.startdate;
                    EndDate = time.enddate;
                }
                else
                {
                    StartDate = (DateTime)StartDate.Value.Date;
                    EndDate = (DateTime)EndDate.Value.AddDays(1);
                }
                var list = pullAuditReport(Conference_ID, (DateTime)StartDate, (DateTime)EndDate);

                StringBuilder sb = new StringBuilder();
                sb.Append("Payment_ID, Contact Name, Email, PaymentType, PaymentSource, CC Transaction Ref., Voucher Code, Payment Date, Payment By, Payment Amount (AUD), Payment Comment \r\n");
                foreach (var item in list)
                {
                    var str = new StringBuilder();
                    str.Append(item.GroupPayment_ID + "," + item.GroupLeader_Name + "," + item.Email + "," + item.PaymentType + "," + item.PaymentSource + ",");
                    str.Append(item.CCTransactionRef + "," + item.VoucherCode + "," + item.PaymentDate + "," + item.PaymentBy_Name + "," + item.PaymentAmount + ",");
                    str.Append(item.Comment + "\r\n");

                    sb.Append(str.ToString());
                };
                return File(Encoding.UTF8.GetBytes(sb.ToString()), "text/csv", "AuditReport.csv");
            }
        }
        public List<GroupPayment> pullAuditReport(int Conference_ID, DateTime StartDate, DateTime EndDate)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var report = (from gp in context.Events_GroupPayment
                              join cc in context.Common_Contact on gp.GroupLeader_ID equals cc.Contact_ID
                              join ec in context.Events_Conference on gp.Conference_ID equals ec.Conference_ID
                              join cpt in context.Common_PaymentType on gp.PaymentType_ID equals cpt.PaymentType_ID
                              join cps in context.Common_PaymentSource on gp.PaymentSource_ID equals cps.PaymentSource_ID
                              join cba in context.Common_BankAccount on gp.BankAccount_ID equals cba.BankAccount_ID
                              join ccc in context.Common_Contact on gp.PaymentBy_ID equals ccc.Contact_ID
                              where gp.Conference_ID == Conference_ID
                                  && gp.PaymentCompletedDate < EndDate
                                  && gp.PaymentCompletedDate >= StartDate
                                  && (gp.ExpiredIntent != true || gp.ExpiredIntent == null)
                              orderby gp.PaymentCompletedDate
                              select new GroupPayment
                              {
                                  GroupPayment_ID = gp.GroupPayment_ID,
                                  GroupLeader_ID = gp.GroupLeader_ID,
                                  GroupLeader_Name = cc.FirstName + " " + cc.LastName,
                                  Email = (cc.Email != "") ? cc.Email : cc.Email2,
                                  Conference_ID = gp.Conference_ID,
                                  Conference_Name = ec.ConferenceName,
                                  PaymentType_ID = gp.PaymentType_ID,
                                  PaymentType = cpt.PaymentType,
                                  PaymentSource_ID = cps.PaymentSource_ID,
                                  PaymentSource = cps.PaymentSource,
                                  PaymentAmount = Math.Round(gp.PaymentAmount, 2),
                                  CCTransactionRef = gp.CCTransactionRef,
                                  CCRefund = gp.CCRefund,
                                  PaymentDate = gp.PaymentCompletedDate,
                                  PaymentBy_ID = gp.PaymentBy_ID,
                                  PaymentBy_Name = ccc.FirstName,
                                  BankAccount_ID = gp.BankAccount_ID,
                                  BankDescription = cba.Description,
                                  Comment = gp.Comment,
                              }).ToList();
                return report;
            }
        }

        [HttpPost]
        public ActionResult GenerateApplicationReport(int Event_ID, List<int> Question_IDs, List<int> RegistrationType_IDs)
        {
            using (PlanetshakersEntities _ctx = new PlanetshakersEntities())
            {
                ApplicationReportViewModel model = new ApplicationReportViewModel()
                {
                    Event_ID = Event_ID
                };
                if (Question_IDs != null)
                {
                    IQueryable<ApplicationReportQuestion> getlist = (from q in _ctx.Volunteer_ApplicationQuestion
                                                                     join eq in _ctx.Volunteer_EventQuestion on q.Question_ID equals eq.Question_ID
                                                                     join rt in _ctx.Events_RegistrationType on eq.RegistrationType_ID equals rt.RegistrationType_ID
                                                                     where Question_IDs.Contains(q.Question_ID) && rt.Conference_ID == Event_ID && eq.Inactive == false
                                                                     orderby q.SortOrder
                                                                     select new ApplicationReportQuestion()
                                                                     {
                                                                         FullQuestion = q.Question,
                                                                         Question = q.ShortName,
                                                                         Question_ID = q.Question_ID,
                                                                         Type = q.Type,
                                                                         RegistrationType_ID = eq.RegistrationType_ID
                                                                     });
                    //if (RegistrationType_IDs != null)
                    //{
                    //    getlist = getlist.Where(x => RegistrationType_IDs.Contains(x.RegistrationType_ID));
                    //}
                    var list = getlist.ToList();
                    model.Questions = (from q in list
                                       select new ApplicationReportQuestion()
                                       {
                                           FullQuestion = q.Question,
                                           Question = q.Question,
                                           Question_ID = q.Question_ID,
                                           Type = q.Type,
                                       }).Distinct().ToList();

                    //list of people who are registered to the filtered registration type, filter out contacts that have withdrawn or denied
                    var registered = getActiveRegistrants(Event_ID, RegistrationType_IDs);

                    model.Applications = (from a in _ctx.Volunteer_Application
                                          join c in _ctx.Common_Contact on a.Contact_ID equals c.Contact_ID
                                          join rt in _ctx.Events_RegistrationType on a.RegistrationType_ID equals rt.RegistrationType_ID
                                          join s in _ctx.Volunteer_ApplicationProgress on a.Application_ID equals s.VolunteerApplication_ID
                                          where rt.Conference_ID == Event_ID && a.Approved == true && s.Status_ID != 6 && registered.Contains(c.Contact_ID)
                                          select new ApplicationReportApplication()
                                          {
                                              Contact_ID = c.Contact_ID,
                                              ContactName = c.FirstName + " " + c.LastName,
                                              Application_ID = a.Application_ID,
                                          }).ToList();
                    foreach (var item in model.Applications)
                    {
                        item.Answers = (from aa in _ctx.Volunteer_ApplicationAnswer
                                        join q in _ctx.Volunteer_ApplicationQuestion on aa.Question_ID equals q.Question_ID
                                        orderby q.SortOrder
                                        where aa.Application_ID == item.Application_ID && Question_IDs.Contains(aa.Question_ID)
                                        select new ApplicationReportAnswer()
                                        {
                                            Question_ID = aa.Question_ID,
                                            Answer = aa.Answer
                                        }).ToList();
                    }

                    model.Summary = new List<ApplicationReportSummary>();
                    var summary_id = model.Questions.Where(x => x.Type == "Checkbox" || x.Type == "Multiple-Choice").Select(x => x.Question_ID).ToList();
                    if (summary_id.Count() > 0)
                    {
                        foreach (var Question_ID in summary_id)
                        {
                            var answers = (from a in _ctx.Volunteer_ApplicationAnswer
                                           join ap in _ctx.Volunteer_Application on a.Application_ID equals ap.Application_ID
                                           join rt in _ctx.Events_RegistrationType on ap.RegistrationType_ID equals rt.RegistrationType_ID
                                           join s in _ctx.Volunteer_ApplicationProgress on a.Application_ID equals s.VolunteerApplication_ID
                                           where a.Question_ID == Question_ID && rt.Conference_ID == Event_ID && ap.Approved == true
                                                && s.Status_ID != 6 && registered.Contains(ap.Contact_ID)
                                           group a by a.Answer into newGroup
                                           select new ApplicationReportSummaryOption()
                                           {
                                               option = newGroup.Key,
                                               count = newGroup.Count()
                                           }).Distinct().ToList();
                            var summary = new ApplicationReportSummary()
                            {
                                Question_ID = Question_ID,
                                Options = answers,
                                Question = model.Questions.Where(x => x.Question_ID == Question_ID).Select(x => x.FullQuestion).FirstOrDefault()
                            };
                            model.Summary.Add(summary);
                        }
                    }
                }
                else
                {
                    model.Questions = new List<ApplicationReportQuestion>();
                    model.Applications = new List<ApplicationReportApplication>();
                    model.Summary = new List<ApplicationReportSummary>();
                }
                return PartialView("_ApplicationReport", model);
            }
        }

        [HttpPost]
        public ActionResult GenerateDocumentReport(int Event_ID, List<int> DocumentType_IDs, List<int> RegistrationType_IDs)
        {
            using (PlanetshakersEntities _ctx = new PlanetshakersEntities())
            {
                DocumentReportViewModel model = new DocumentReportViewModel()
                {
                    Event_ID = Event_ID,
                };
                if (DocumentType_IDs != null)
                {
                    //model.DocumentTypes
                    IQueryable<DocumentReportDocumentType> list = (from d in _ctx.Volunteer_DocumentType
                                                                   join ed in _ctx.Volunteer_EventDocument on d.DocumentType_ID equals ed.DocumentType_ID
                                                                   join rt in _ctx.Events_RegistrationType on ed.RegistrationType_ID equals rt.RegistrationType_ID
                                                                   orderby d.SortOrder
                                                                   where DocumentType_IDs.Contains(d.DocumentType_ID)
                                                                    && ed.Inactive == false && rt.Conference_ID == Event_ID
                                                                   select new DocumentReportDocumentType()
                                                                   {
                                                                       DocumentType = d.DocumentType,
                                                                       DocumentType_ID = d.DocumentType_ID,
                                                                       RegistrationType_ID = ed.RegistrationType_ID
                                                                   });
                    if (RegistrationType_IDs != null)
                    {
                        list = list.Where(x => RegistrationType_IDs.Contains(x.RegistrationType_ID));
                    }
                    var getlist = list.ToList();
                    model.DocumentTypes = (from a in getlist
                                           select new DocumentReportDocumentType()
                                           {
                                               DocumentType = a.DocumentType,
                                               DocumentType_ID = a.DocumentType_ID
                                           }).Distinct().ToList();

                    //list of people who are registered to the filtered registration type, filter out contacts that have withdrawn or denied
                    var registered = getActiveRegistrants(Event_ID, RegistrationType_IDs);

                    model.Applications = (from a in registered
                                          join c in _ctx.Common_Contact on a equals c.Contact_ID
                                          select new DocumentReportApplication()
                                          {
                                              Contact_ID = c.Contact_ID,
                                              ContactName = c.FirstName + " " + c.LastName
                                          }).ToList();
                    var allDoc = (from d in _ctx.Volunteer_ApplicationDocument
                                  where d.Event_ID == Event_ID && d.Deleted == false
                                  select d).ToList();
                    foreach (var item in model.Applications)
                    {
                        item.DocSubmit = (from d in model.DocumentTypes
                                          join dc in _ctx.Volunteer_DocumentType on d.DocumentType_ID equals dc.DocumentType_ID
                                          orderby dc.SortOrder
                                          select new DocumentReportDocSubmit
                                          {
                                              DocumentType_ID = d.DocumentType_ID,
                                              status = "Not Provided"
                                          }).ToList();
                        foreach (var doc in item.DocSubmit)
                        {
                            var submittedDoc = allDoc.Where(x => x.Contact_ID == item.Contact_ID && x.DocumentType_ID == doc.DocumentType_ID).FirstOrDefault();
                            doc.status = (submittedDoc == null) ? doc.status : (submittedDoc.Approved) ? "Approved" : "Submitted";
                        }
                    }
                    model.Summary = (from d in model.DocumentTypes
                                     join s in _ctx.Volunteer_ApplicationDocument on d.DocumentType_ID equals s.DocumentType_ID
                                     where s.Event_ID == Event_ID && registered.Contains(s.Contact_ID) && s.Deleted == false
                                     group s by s.DocumentType_ID into newGroup
                                     select new DocumentReportSummary()
                                     {
                                         DocumentType_ID = newGroup.Key,
                                         DocumentType = model.DocumentTypes.Where(x => x.DocumentType_ID == newGroup.Key).Select(x => x.DocumentType).FirstOrDefault(),
                                         Submitted = newGroup.Count(),
                                         UnsubmitCount = registered.Count() - newGroup.Count(),
                                         Approved = newGroup.Where(x => x.Approved == true).Count()
                                     }).Distinct().ToList();
                }
                else
                {
                    model.DocumentTypes = new List<DocumentReportDocumentType>();
                    model.Applications = new List<DocumentReportApplication>();
                    model.Summary = new List<DocumentReportSummary>();
                }
                return PartialView("_DocumentReport", model);
            }
        }

        public ActionResult GenerateCheckInReport(ReportModel model, int Conference_ID)
        {
            model.displayReport = true;
            List<CheckInDetails> checkindeets = new List<CheckInDetails>();
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                int venueID = (from v in context.Events_Venue
                               where v.Conference_ID == Conference_ID
                               select v.Venue_ID).FirstOrDefault();
                model.checkInList = (from ev in context.Events_Registration
                                     join cc in context.Common_Contact
                                     on ev.Contact_ID equals cc.Contact_ID
                                     join rt in context.Events_RegistrationType
                                     on ev.RegistrationType_ID equals rt.RegistrationType_ID
                                     where ev.Venue_ID == venueID
                                     orderby ev.Attended, ev.Registration_ID
                                     select new CheckInDetails
                                     {
                                         RegistrationID = ev.Registration_ID,
                                         Attended = ev.Attended,
                                         FirstName = cc.FirstName,
                                         LastName = cc.LastName,
                                         Mobile = cc.Mobile,
                                         Email = cc.Email,
                                         RegistrationType = rt.RegistrationType,
                                         RegistrationType_ID = rt.RegistrationType_ID
                                     }
                                ).ToList();
                model.RegistrationTypeList = (from rt in context.Events_RegistrationType
                                              where rt.Conference_ID == Conference_ID
                                              select new SelectListItem
                                              {
                                                  Text = rt.RegistrationType,
                                                  Value = rt.RegistrationType_ID.ToString().Trim()
                                              }).ToList();
            }
            return View("CheckInReport", model);
        }

        public ActionResult GenerateTransferReport(TransferReprotViewModel model, int Conference_ID)
        {
            model.displayReport = true;
            if (model.selectedDateOpt != 8)
            {
                TimeSelection time = model._availableTimeSelection.Find(x => x.dateId == model.selectedDateOpt);
                model.StartDate = time.startdate;
                model.EndDate = time.enddate;
            }

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                //update payment intends over 1 hour (to expired)
                var expireTime = DateTime.Now.AddHours(-1);
                var expiredIntends = (from gp in context.Events_GroupPayment
                                      where gp.PaymentCompleted == false && gp.PaymentCompletedDate == null && gp.PaymentStartDate < expireTime && (gp.ExpiredIntent != true || gp.ExpiredIntent == null)
                                      select gp).ToList();
                foreach (var item in expiredIntends)
                {
                    item.ExpiredIntent = true;
                    item.Voucher_ID = null;
                }
                context.SaveChanges();

                model.Currency = (from ec in context.Events_Conference
                                  join ba in context.Common_BankAccount
                                  on ec.BankAccount_ID equals ba.BankAccount_ID
                                  where ec.Conference_ID == model.Conference_ID
                                  select ba.Currency).FirstOrDefault();

                model.groupPaymentList = (from gp in context.Events_GroupPayment
                                          join cc in context.Common_Contact on gp.GroupLeader_ID equals cc.Contact_ID
                                          join ec in context.Events_Conference on gp.Conference_ID equals ec.Conference_ID
                                          join cpt in context.Common_PaymentType on gp.PaymentType_ID equals cpt.PaymentType_ID
                                          join cps in context.Common_PaymentSource on gp.PaymentSource_ID equals cps.PaymentSource_ID
                                          join cba in context.Common_BankAccount on gp.BankAccount_ID equals cba.BankAccount_ID
                                          join ccc in context.Common_Contact on gp.PaymentBy_ID equals ccc.Contact_ID
                                          where gp.Conference_ID == Conference_ID
                                              && gp.PaymentCompletedDate <= model.EndDate
                                              && gp.PaymentCompletedDate >= model.StartDate
                                              && (gp.ExpiredIntent != true || gp.ExpiredIntent == null)
                                              && gp.PaymentType_ID == 9
                                          select new GroupPayment
                                          {
                                              GroupPayment_ID = gp.GroupPayment_ID,
                                              GroupLeader_ID = gp.GroupLeader_ID,
                                              GroupLeader_Name = cc.FirstName + " " + cc.LastName,
                                              Conference_ID = gp.Conference_ID,
                                              Conference_Name = ec.ConferenceName,
                                              PaymentType_ID = gp.PaymentType_ID,
                                              PaymentType = cpt.PaymentType,
                                              PaymentSource_ID = cps.PaymentSource_ID,
                                              PaymentSource = cps.PaymentSource,
                                              PaymentAmount = Math.Round(gp.PaymentAmount, 2),
                                              CCTransactionRef = gp.CCTransactionRef,
                                              CCRefund = gp.CCRefund,
                                              PaymentDate = gp.PaymentCompletedDate,
                                              PaymentBy_ID = gp.PaymentBy_ID,
                                              PaymentBy_Name = ccc.FirstName + " " + ccc.LastName,
                                              BankAccount_ID = gp.BankAccount_ID,
                                              BankDescription = cba.Description,
                                              Comment = gp.Comment
                                          }).ToList();

                //model.totalSingleTransferAmount = model.singlePaymentList.Sum(x => x.PaymentAmount);
                model.totalGroupTransferAmount = model.groupPaymentList.Sum(x => x.PaymentAmount);
            }
            return View("TransferReport", model);
        }

        //Generate mailing list report based on the selected mailing list, mailing list type, conference
        //also pull if they are internal contact/external contact
        //public ActionResult GenerateMailingListReport(MailingListReportModel model)
        //{
        //    using (PlanetshakersEntities context = new PlanetshakersEntities())
        //    {
        //        //If user is not selected the value, make the where statement checking true
        //        //while joining table
        //        model.contactMailingList = (from cml in context.Common_CombinedContactMailingList
        //                                    join ml in context.Common_MailingList
        //                                    on cml.MailingList_ID equals ml.MailingList_ID
        //                                    join mlt in context.Common_MailingListType
        //                                    on cml.MailingListType_ID equals mlt.MailingListType_ID
        //                                    join cc in context.Common_Contact
        //                                    on cml.Contact_ID equals cc.Contact_ID
        //                                    where cml.Contact_ID != null
        //                                    &&
        //                                    (model.Conference_ID != null
        //                                    ? (from er in context.Events_Registration
        //                                       join ev in context.Events_Venue
        //                                       on er.Venue_ID equals ev.Venue_ID
        //                                       where ev.Conference_ID == model.Conference_ID && cml.Contact_ID == er.Contact_ID
        //                                       select er.Contact_ID).Any() //Subquery to see if the contact is in selected event
        //                                    : true)
        //                                    && (model.MailingList_ID != null ? cml.MailingList_ID == model.MailingList_ID : true)
        //                                    && (model.MailingListType_ID != null ? mlt.MailingListType_ID == model.MailingListType_ID : true)
        //                                    && cml.SubscriptionActive
        //                                    select new ContactMailingList
        //                                    {
        //                                        Contact_ID = (int)cml.Contact_ID,
        //                                        FullName = cc.FirstName + " " + cc.LastName,
        //                                        Email = cc.Email,
        //                                        Mobile = cc.Mobile,
        //                                        MailingListName = ml.Name,
        //                                        MailingListTypeName = mlt.Name,
        //                                        MailingListSubscription = cml.SubscriptionActive
        //                                    }).ToList();


        //        foreach (var contactML in model.contactMailingList)
        //        {
        //            contactML.Internal = (from cci in context.Common_ContactInternal
        //                                  where cci.Contact_ID == contactML.Contact_ID
        //                                  && (cci.ChurchStatus_ID == 1 || cci.ChurchStatus_ID == 2
        //                                  || cci.ChurchStatus_ID == 4 || cci.ChurchStatus_ID == 5) //Partner, contact, np, nc respectively
        //                                  select cci).Any();

        //            contactML.External = (from cce in context.Common_ContactExternal
        //                                  where cce.Contact_ID == contactML.Contact_ID
        //                                  select cce).Any() ||
        //                                  (from cci in context.Common_ContactInternal
        //                                   where cci.Contact_ID == contactML.Contact_ID
        //                                   && (cci.ChurchStatus_ID == 6 || cci.ChurchStatus_ID == 7) //mailing list, inactive respectively
        //                                   select cci).Any();
        //        }
        //    }
        //    model.displayReport = true;

        //    return View("MailingListReport", model);
        //}

        //in depth conference report for total registrant report
        [HttpGet]
        public ActionResult pullstatistics(int conference_ID)
        {
            using (PlanetshakersEntities ctx = new PlanetshakersEntities())
            {
                ConferenceStatsList confStatsList = new ConferenceStatsList();

                List<int> AdultRegistrationType_IDs = (from ert in ctx.Events_RegistrationType
                                                       where ert.Conference_ID == conference_ID && !ert.IsCollegeRegistrationType && !ert.AddOnRegistrationType
                                                       select ert.RegistrationType_ID).ToList();
                List<int> KidsRegistrationType_IDs = (from ert in ctx.Events_RegistrationType
                                                      where ert.Conference_ID == conference_ID && ert.IsCollegeRegistrationType && ert.RegistrationCost != 0
                                                      select ert.RegistrationType_ID).ToList();
                List<int> BoomRegistrationType_IDs = (from ert in ctx.Events_RegistrationType
                                                      where ert.Conference_ID == conference_ID && ert.AddOnRegistrationType
                                                      select ert.RegistrationType_ID).ToList();
                List<int> RegistrationType_IDs = (from ert in ctx.Events_RegistrationType
                                                  where ert.Conference_ID == conference_ID && ert.RegistrationCost != 0
                                                  select ert.RegistrationType_ID).ToList();

                //summary stats for adult
                if (AdultRegistrationType_IDs.Count() != 0)
                {
                    ConferenceStats AdultStats = new ConferenceStats();
                    AdultStats.TotalRegistration = (from r in ctx.Events_Registration
                                                    where AdultRegistrationType_IDs.Contains(r.RegistrationType_ID) && r.GroupLeader_ID == null
                                                    select r).Count() +
                                                     ((from br in ctx.Events_GroupBulkRegistration
                                                       where AdultRegistrationType_IDs.Contains(br.RegistrationType_ID)
                                                       && br.Deleted == false
                                                       select (int?)br.Quantity).Sum()) ?? 0;
                    AdultStats.Allocated = (from r in ctx.Events_Registration
                                            where AdultRegistrationType_IDs.Contains(r.RegistrationType_ID) && r.GroupLeader_ID != null
                                            select r).Count();
                    AdultStats.Unallocated = AdultStats.TotalRegistration - AdultStats.Allocated;
                    AdultStats.RegistrationTypeName = "General Registration";

                    confStatsList.AdultList = AdultStats;
                }

                //summary for boom
                if (BoomRegistrationType_IDs.Count() != 0)
                {
                    ConferenceStats BoomStats = new ConferenceStats();
                    BoomStats.TotalRegistration = (from r in ctx.Events_Registration
                                                   where BoomRegistrationType_IDs.Contains(r.RegistrationType_ID) && r.GroupLeader_ID == null
                                                   select r).Count() +
                                                     ((from br in ctx.Events_GroupBulkRegistration
                                                       where BoomRegistrationType_IDs.Contains(br.RegistrationType_ID)
                                                       && br.Deleted == false
                                                       select (int?)br.Quantity).Sum()) ?? 0;
                    BoomStats.Allocated = (from r in ctx.Events_Registration
                                           where BoomRegistrationType_IDs.Contains(r.RegistrationType_ID) && r.GroupLeader_ID != null
                                           select r).Count();
                    BoomStats.Unallocated = BoomStats.TotalRegistration - BoomStats.Allocated;
                    BoomStats.RegistrationTypeName = "College Registration";

                    confStatsList.BoomList = BoomStats;
                }

                //summary stats for kids 
                if (KidsRegistrationType_IDs.Count() != 0)
                {
                    ConferenceStats KidsStats = new ConferenceStats();
                    KidsStats.TotalRegistration = (from r in ctx.Events_Registration
                                                   where KidsRegistrationType_IDs.Contains(r.RegistrationType_ID) && r.GroupLeader_ID == null
                                                   select r).Count() +
                                                     ((from br in ctx.Events_GroupBulkRegistration
                                                       where KidsRegistrationType_IDs.Contains(br.RegistrationType_ID)
                                                       && br.Deleted == false
                                                       select (int?)br.Quantity).Sum()) ?? 0;
                    KidsStats.Allocated = (from r in ctx.Events_Registration
                                           where KidsRegistrationType_IDs.Contains(r.RegistrationType_ID) && r.GroupLeader_ID != null
                                           select r).Count();
                    KidsStats.Unallocated = KidsStats.TotalRegistration - KidsStats.Allocated;
                    KidsStats.RegistrationTypeName = "Add Ons Registration";

                    confStatsList.KidsList = KidsStats;
                }

                //stats for each registrationType_ID
                List<ConferenceStats> regoID_list = new List<ConferenceStats>();
                foreach (var regid in RegistrationType_IDs)
                {
                    ConferenceStats details = new ConferenceStats();
                    details.RegistrationType_ID = regid;
                    details.RegistrationTypeName = (from rt in ctx.Events_RegistrationType
                                                    where rt.RegistrationType_ID == regid
                                                    select rt.RegistrationType).FirstOrDefault();
                    details.TotalRegistration = (from r in ctx.Events_Registration
                                                 where r.RegistrationType_ID == regid && r.GroupLeader_ID == null
                                                 select r).Count() +
                                                 ((from br in ctx.Events_GroupBulkRegistration
                                                   where br.RegistrationType_ID == regid
                                                   && br.Deleted == false
                                                   select (int?)br.Quantity).Sum()) ?? 0;

                    details.Allocated = (from r in ctx.Events_Registration
                                         where r.RegistrationType_ID == regid && r.GroupLeader_ID != null
                                         select r).Count();

                    details.Unallocated = details.TotalRegistration - details.Allocated;

                    details.Capacity = (from rt in ctx.Events_RegistrationType
                                        where rt.RegistrationType_ID == regid
                                        select rt.Limit).FirstOrDefault();

                    regoID_list.Add(details);
                }

                confStatsList.StatsList = regoID_list;
                confStatsList.ConferenceName = (from c in ctx.Events_Conference
                                                where c.Conference_ID == conference_ID
                                                select c.ConferenceName).FirstOrDefault();
                confStatsList.StaffRegistration = (from r in ctx.Events_Registration
                                                   join cr in ctx.Church_ContactRole
                                                   on r.Contact_ID equals cr.Contact_ID
                                                   where RegistrationType_IDs.Contains(r.RegistrationType_ID) && cr.Role_ID == 26 && cr.Inactive == false
                                                   select r).Count();

                return Json(new { result = confStatsList }, JsonRequestBehavior.AllowGet);
            }
        }

        public void GenerateCustomReport(CustomReportModel model)
        {
            var list = new List<Guest>();

            string command = "select * from Events_PriorityGuest pg ";
            command += "join Common_Contact cc on pg.Contact_ID = cc.Contact_ID";
            command += "join Events_ConferencePriorityType cpt on pg.ConferencePriorityType_ID = cpt.ConferencePriorityType_ID";

            command += "where pg.ConferencePriorityType_ID in ()";

            SqlCommand cmd = new SqlCommand();
            if (!string.IsNullOrEmpty(model.ChurchName))
            {
                command += " and pg.ChurchName in (@lastname)";

                cmd.CommandText = command;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@lastname";
                //param.Value = LastName;
                cmd.Parameters.Add(param);
            }

            if (!string.IsNullOrEmpty(model.ChurchRole))
            {

            }
        }
        #endregion

        #region Helper Functions
        // TODO: if the following is not specified, then ninject error occurs. Page initially loads with constructor ReportController().
        // If it is specified, then when we attempt to change value of _reportRepository, it has null exception. 
        // Bug can probably be overcome by fixing something in this function. 
        public JsonResult searchContact(string FirstName, string LastName)
        {
            var list = new List<Common_Contact>();

            #region Data Validation

            if (string.IsNullOrEmpty(FirstName)
                && string.IsNullOrEmpty(LastName))
            {
                return Json(list);
            }

            #endregion

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["PlanetshakersDB"].ToString());

            var commandText = "select contact_id, firstname, lastname from common_contact where ";
            commandText += "(contact_id in (select contact_id from common_contact))";
            SqlCommand cmd = new SqlCommand();

            if (!string.IsNullOrEmpty(FirstName))
            {
                commandText += " and firstname = @firstname";

                cmd.CommandText = commandText;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@firstname";
                param.Value = FirstName;
                cmd.Parameters.Add(param);
            }

            if (!string.IsNullOrEmpty(LastName))
            {
                commandText += " and lastname = @lastname";

                cmd.CommandText = commandText;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@lastname";
                param.Value = LastName;
                cmd.Parameters.Add(param);
            }

            commandText += " order by firstname, lastname, contact_id";
            cmd.Connection = conn;

            try
            {
                conn.Open();

                var reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    list.Add(new Common_Contact
                    {
                        Contact_ID = Convert.ToInt32(reader["Contact_ID"]),
                        FirstName = reader["FirstName"].ToString(),
                        LastName = reader["LastName"].ToString()
                    });
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return Json(list);
        }

        [HttpGet]
        public ActionResult GetConferenceRegoType(int Conference_ID)
        {
            JsonResult json = new JsonResult();
            RegistrationTypeList model = new RegistrationTypeList();
            model.Conference_ID = Conference_ID;
            json.Data = model.ConferenceRegotypeList;
            json.JsonRequestBehavior = JsonRequestBehavior.AllowGet;

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        public List<int> getActiveRegistrants(int Event_ID, List<int> RegistrationType_IDs)
        {
            using (PlanetshakersEntities _ctx = new PlanetshakersEntities())
            {
                var registered = new List<int>();
                if (RegistrationType_IDs != null)
                {
                    registered = (from r in _ctx.Events_Registration
                                  join a in _ctx.Volunteer_Application on r.Contact_ID equals a.Contact_ID
                                  join rt in _ctx.Events_RegistrationType on a.RegistrationType_ID equals rt.RegistrationType_ID
                                  join ap in _ctx.Volunteer_ApplicationProgress on a.Application_ID equals ap.VolunteerApplication_ID
                                  where RegistrationType_IDs.Contains(r.RegistrationType_ID) && rt.Conference_ID == Event_ID && a.Approved == true && ap.Status_ID != 6
                                  select r.Contact_ID).Distinct().ToList();
                }
                else
                {
                    registered = (from r in _ctx.Events_Registration
                                  join rt in _ctx.Events_RegistrationType on r.RegistrationType_ID equals rt.RegistrationType_ID
                                  join a in _ctx.Volunteer_Application on r.Contact_ID equals a.Contact_ID
                                  join rt2 in _ctx.Events_RegistrationType on a.RegistrationType_ID equals rt2.RegistrationType_ID
                                  join ap in _ctx.Volunteer_ApplicationProgress on a.Application_ID equals ap.VolunteerApplication_ID
                                  where rt.Conference_ID == Event_ID && rt2.Conference_ID == Event_ID && a.Approved == true && ap.Status_ID != 6
                                  select r.Contact_ID).Distinct().ToList();
                }
                return registered;
            }
        }

        public void checkSponsorshipDuplicate(int Event_ID, int Contact_ID = 0)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {

                var sponsorshipAllocations = (from s in context.Events_VariablePayment
                                              where s.Conference_ID == Event_ID && s.IncomingContact_ID == null && s.OutgoingContact_ID != null && (s.PaymentCompleted ?? false)
                                              && ((Contact_ID != 0) ? s.OutgoingContact_ID == Contact_ID : true) && s.Refund == false && s.CCTransactionReference != null
                                              select s).GroupBy(x => x.CCTransactionReference);
                foreach (var transaction in sponsorshipAllocations)
                {
                    if (transaction.Count() > 1)
                    {
                        //duplicated allocation
                        foreach (var item in transaction.Skip(1))
                        {
                            item.PaymentCompleted = false;
                            item.PaymentCompletedDate = null;
                            item.Comment = "Duplicated Allocation | updated: " + DateTime.Now.ToString("dd-MMM-yy HH:mm");
                        }
                    }
                }
                context.SaveChanges();
            }
        }
        #endregion
    }
}