﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Net.Mime;
using Planetshakers.Core.Models;
using Planetshakers.Events.Models;
using Planetshakers.Church.Web.vNext.Helpers;

namespace Planetshakers.Events.Controllers
{
    [Authorize]
    public class SetupController : Controller
    {
        // GET: Setup
        public ActionResult Index()
        {
            return View();
        }
        #region Registration Type
        public ActionResult RegistrationType()
        {
            RegistrationTypeViewModel newRegoTypeModel = new RegistrationTypeViewModel();
            return View(newRegoTypeModel);
        }
        public ActionResult GenerateRegistrationType(string ConferenceID)
        {
            RegistrationTypeViewModel newRegoTypeModel = new RegistrationTypeViewModel();
            newRegoTypeModel.SetRegistrationTypesList(Int32.Parse(ConferenceID));
            newRegoTypeModel.SelectedConferenceID = Int32.Parse(ConferenceID);
            newRegoTypeModel.SelectedConference = ConferenceID;
            return View("_RegistrationType", newRegoTypeModel);
        }

        [HttpPost]
        public ActionResult CheckRegoTypeName(int Conference_ID, string RegoName)
        {
            var sameName = false;
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                sameName = (from rt in context.Events_RegistrationType
                            where rt.Conference_ID == Conference_ID && rt.RegistrationType == RegoName
                            select rt).Any();
                return Json(new { sameName }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AddNewRegoType(string conferenceID, string regoTypeName, string regoLimit, string regoCost, string startDate,
                                           string endDate, string startTime, string endTime, string IsCollegeRego,string IsApplicationRego, string IsAddonRego, 
                                           List<int> QuestionList, List<int> DocTypeList)
        {
            Events_RegistrationType newregotype = new Events_RegistrationType
            {
                AvailableOnlineGroup = false,
                AvailableOnlineSingle = IsApplicationRego == "true" ? true : false,
                Conference_ID = Int32.Parse(conferenceID),
                EndDate = DateTime.Parse(endDate + " " + endTime),
                GroupMinimumQuantity = 1,
                IsCollegeRegistrationType = false,
                //IsCollegeRegistrationType = IsCollegeRego == "true" ? true : false,
                ApplicationRegistrationType = IsApplicationRego == "true" ? true : false,
                AddOnRegistrationType = IsAddonRego == "true" ? true : false,
                RegistrationCost = Decimal.Parse(regoCost),
                RegistrationType = regoTypeName,
                StartDate = DateTime.Parse(startDate + " " + startTime),
                Limit = Int32.Parse(regoLimit),

            };
            
            RegistrationTypeViewModel newRegoTypeModel = new RegistrationTypeViewModel();
            newRegoTypeModel.SelectedConference = conferenceID;
            newRegoTypeModel.addRegistrationType(newregotype);
            newRegoTypeModel.SetRegistrationTypesList(Int32.Parse(conferenceID));
            newRegoTypeModel.AddRTquestions(QuestionList, newregotype.RegistrationType_ID);
            newRegoTypeModel.AddRTdocs(DocTypeList, newregotype.RegistrationType_ID);



            return Json(new { });
        }

        public ActionResult EditRegoType(string conferenceID, string regoTypeID, string regoTypeName, string regoLimit,
                                        string regoCost, string startDate, string endDate, string startTime, string endTime,
                                        string IsCollegeRego, string IsApplicationRego, string IsAddonRego, 
                                        List<int> QuestionList, List<int> DocTypeList)
        {
            Events_RegistrationType editregotype = new Events_RegistrationType
            {
                RegistrationType_ID = Int32.Parse(regoTypeID),
                Conference_ID = Int32.Parse(conferenceID),
                EndDate = DateTime.Parse(endDate + " " + endTime),
                AvailableOnlineSingle = IsApplicationRego == "true" ? true : false,
                IsCollegeRegistrationType = false,
                //IsCollegeRegistrationType = IsCollegeRego == "true" ? true : false,
                ApplicationRegistrationType = IsApplicationRego == "true" ? true : false,
                AddOnRegistrationType = IsAddonRego == "true" ? true : false,
                RegistrationCost = Decimal.Parse(regoCost),
                RegistrationType = regoTypeName,
                StartDate = DateTime.Parse(startDate + " " + startTime),
                Limit = Int32.Parse(regoLimit)
            };

            RegistrationTypeViewModel newRegoTypeModel = new RegistrationTypeViewModel();
            newRegoTypeModel.SelectedConference = conferenceID;
            newRegoTypeModel.editRegistrationType(editregotype);
            newRegoTypeModel.EditRTquestions(QuestionList, editregotype.RegistrationType_ID);
            newRegoTypeModel.EditRTdocs(DocTypeList, editregotype.RegistrationType_ID);

            return Json(new
            {
                //regoID = editregotype.RegistrationType_ID,
                //startDate = editregotype.StartDate.ToString(),
                //endDate = editregotype.EndDate.ToString(),
                //regoName = editregotype.RegistrationType,
                //cost = editregotype.RegistrationCost.ToString("0.00")
            });
        }
        [HttpPost]
        public ActionResult GetQuestionAndDoc(int RegistrationType_ID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var questionList = (from aq in context.Volunteer_ApplicationQuestion
                                    where aq.Active
                                    select new Event_Question
                                    {
                                        Question_ID = aq.Question_ID,
                                        ShortName = aq.ShortName,
                                        Inactive = !(from q in context.Volunteer_EventQuestion where q.RegistrationType_ID == RegistrationType_ID && q.Question_ID == aq.Question_ID && q.Inactive == false select q).Any()
                                    }).ToList();

                var html = "";
                html += "<label>Include Questions: (Questions are referred as their short names)</label><select name='selValue' id='QuestionSelected_" + RegistrationType_ID+"' class='form-control' multiple='multiple' data-live-search='true' data-style='multi-select-btn'>";
                foreach (var question in questionList)
                {
                    if (question.Inactive)
                    {

                        html += "<option value = '" + question.Question_ID + "' >" + question.ShortName + "</option > ";
                    }
                    else
                    {
                        html += "<option value = '" + question.Question_ID + "' selected>" + question.ShortName + "</option > ";
                    }
                }
                html += "</select><br />";


                var docList = (from aq in context.Volunteer_DocumentType
                               where aq.Active
                               select new Event_Doc
                               {
                                   Doc_ID = aq.DocumentType_ID,
                                   Name = aq.DocumentType,
                                   Inactive = !(from q in context.Volunteer_EventDocument where q.RegistrationType_ID == RegistrationType_ID && q.DocumentType_ID == aq.DocumentType_ID && q.Inactive == false select q).Any()
                               }).ToList();


                var html2 = "";
                html2 += "<label>Include Document Type: </label><select name='selValue' id='DocSelected_" + RegistrationType_ID + "' class='form-control' multiple='multiple' data-live-search='true' data-style='multi-select-btn'>";
                foreach (var question in docList)
                {
                    if (question.Inactive)
                    {

                        html2 += "<option value = '" + question.Doc_ID + "' >" + question.Name + "</option > ";
                    }
                    else
                    {
                        html2 += "<option value = '" + question.Doc_ID + "' selected>" + question.Name + "</option > ";
                    }
                }
                html2 += "</select><br />";


                return Json(new { question = html, doc = html2 });
            }
        }

        public ActionResult DeleteRegoType(string regoTypeID)
        {
            int regotypeid = Int32.Parse(regoTypeID);
            RegistrationTypeViewModel newRegoTypeModel = new RegistrationTypeViewModel();
            bool success = newRegoTypeModel.deleteRegoType(regotypeid);

            return Json(new { success });
        }
        [HttpPost]
        public ActionResult SortRegistrationType(List<SortOrderItem> RegistrationTypeList)
        {
            string success = "Registration Type order saved successfully.";
            SortOrderModel model = new SortOrderModel();
            model.SortRegistrationType(RegistrationTypeList);
            return Json(new { resultmessage = success }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Funding Goals
        public ActionResult OpenFundingGoalModel(int RegistrationType_ID)
        {
            FundingGoalViewModel model = new FundingGoalViewModel();
            model.RegistrationType_ID = RegistrationType_ID;
            return View("_FundingGoal", model);
        }
        [HttpPost]
        public ActionResult AddEditFundingGoal(int FundingGoal_ID, int RegistrationType_ID, string Milestone, int Amount, DateTime DueDate)
        {
            if (FundingGoal_ID == 0)
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var goal = new Events_FundingGoal();
                    goal.RegistrationType_ID = RegistrationType_ID;
                    goal.Milestone = Milestone;
                    goal.CumulativeTotal = Amount;
                    goal.DueDate = DueDate;
                    context.Events_FundingGoal.Add(goal);
                    context.SaveChanges();
                }
                return Json(new { message = "Successfully added funding goal." });
            }
            else
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var goal = (from fg in context.Events_FundingGoal
                                where fg.FundingGoal_ID == FundingGoal_ID && fg.RegistrationType_ID == RegistrationType_ID
                                select fg).FirstOrDefault();
                    goal.Milestone = Milestone;
                    goal.CumulativeTotal = Amount;
                    goal.DueDate = DueDate;
                    context.SaveChanges();
                }
                return Json(new { message = "Successfully updated funding goal." });
            }

        }
        [HttpPost]
        public ActionResult PullFundingGoal(int FundingGoal_ID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var goal = (from fg in context.Events_FundingGoal
                            where fg.FundingGoal_ID == FundingGoal_ID
                            select fg).FirstOrDefault();
                return Json(new { Milestone = goal.Milestone, Amount = goal.CumulativeTotal, DueDate = goal.DueDate, RegistrationType_ID = goal.RegistrationType_ID });
            }

        }
        [HttpPost]
        public ActionResult DeleteFundingGoal(int FundingGoal_ID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var goal = (from fg in context.Events_FundingGoal
                            where fg.FundingGoal_ID == FundingGoal_ID
                            select fg).FirstOrDefault();
                context.Events_FundingGoal.Remove(goal);
                context.SaveChanges();
                return Json(new { success = true });
            }
        }

        #endregion

        #region Event
        public ActionResult Conference()
        {
            ConferenceViewModel newConfViewModel = new ConferenceViewModel();
            return View(newConfViewModel);
        }

        public ActionResult GenerateConference(string ConferenceID)
        {
            ConferenceViewModel newConfModel = new ConferenceViewModel();
            newConfModel.selectedConfDetails = newConfModel.ConferenceList.Find(x => x.conf.Conference_ID == Int32.Parse(ConferenceID));
            newConfModel.SelectedConferenceID = Int32.Parse(ConferenceID);

            return View("_Conference", newConfModel);
        }

        public ActionResult AddConference()
        {
            ConferenceViewModel newConfModel = new ConferenceViewModel();
            newConfModel.selectedConfDetails = new ConferenceViewModel.SetUpConf();

            newConfModel.selectedConfDetails.conf = new Events_Conference
            {
                Conference_ID = 0
            };

            newConfModel.selectedConfDetails.venue = new Events_Venue
            {

            };
            return View("_Conference", newConfModel);
        }

        public ActionResult SaveConference(string confID, string confName, string confShortName, string confDate,
                                           string confType, string confCountry, string confShowOnWeb,
                                           string confBankAccount, string conferenceStartDate, string conferenceEndDate,
                                           string conferenceStartTime, string conferenceEndTime,
                                           string venueMaxRegistrants, string conferenceVisibility,
                                           decimal minAge, decimal maxAge)
        {
            ConferenceViewModel newConfModel = new ConferenceViewModel();

            string successMessage = newConfModel.saveConference(Int32.Parse(confID), confName, confShortName, confDate, confType, confCountry, confShowOnWeb,
                            confBankAccount, conferenceStartDate, conferenceEndDate, conferenceStartTime, conferenceEndTime, venueMaxRegistrants,
                            conferenceVisibility, minAge, maxAge);

            return Json(new { success = successMessage });
        }

        //public ActionResult DeleteConference(string confID)
        //{
        //    ConferenceViewModel newConfModel = new ConferenceViewModel();
        //    bool resultMessage = newConfModel.deleteConference(confID);

        //    return Json(new { success = resultMessage });
        //}

        //-------------------------------------------------------------Sort order functionality goes here-----------------------------------------------------------------
        public ActionResult SortConference()
        {
            SortOrderModel model = new SortOrderModel();
            return View("SortOrder", model);
        }

        //[HttpPost]
        //public ActionResult SortConference(List<SortOrderItem> ConferenceList)
        //{
        //    string success = "Conference order saved successfully.";
        //    SortOrderModel model = new SortOrderModel();
        //    model.SortConference(ConferenceList);
        //    return Json(new { resultmessage = success }, JsonRequestBehavior.AllowGet);
        //}
        #endregion

        #region Sign up links

        public ActionResult SignUpLinks()
        {
            SignUpLinkViewModel model = new SignUpLinkViewModel();
            return View(model);
        }
        #endregion

        #region Terms and Conditions 

        public ActionResult TermsAndConditions()
        {
            TermsAndConditionsViewModel model = new TermsAndConditionsViewModel();
            return View("TermsAndConditions", model);
        }
        [HttpPost]
        public ActionResult getTermsAndConditions(int Event_ID)
        {
            var content = "";
            var fileName = "TnC_" + Event_ID.ToString() + ".md";
            FileModel fileModel = new FileModel("s3-amazon-believe-general");
            var file = fileModel.GetFile("TermsAndConditions", fileName);
            if (file.DownloadUrl != null)
            {
                content = WebOperations.ParseUrlToString(file.DownloadUrl);
            }
            return Json(new { content = content });
        }
        [HttpPost]
        public ActionResult saveTermsAndConditions(int Event_ID, string Content)
        {
            //convert content into mb file and update to s3 
            var fileName = "TnC_" + Event_ID + ".md";
            string uri = new Uri(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace('\\', '/')).LocalPath;
            string path = uri + "/" + fileName;

            using (FileStream fs = System.IO.File.Create(path))
            {
                FileModel fileModel = new FileModel("s3-amazon-believe-general");
                byte[] info = new UTF8Encoding(true).GetBytes(Content);
                fs.Write(info, 0, info.Length);
                fileModel.UploadFile("TermsAndConditions", fileName, fs);
                fs.Dispose();
                fs.Close();
                System.IO.File.Delete(path);
            }
            return Json(new { });
        }
        
        [HttpPost]
        public ActionResult loadMarkDown(string Content)
        {
            var html = WebOperations.ParseMarkdown(Content);
            return Json(new { html = html });
        }

        #endregion  


        //public ActionResult BulkChangeRegoType(int Conference_ID)
        //{
        //    var model = new BulkChangeRegoTypeViewModel();
        //    model.selectedConferenceID = Conference_ID;
        //    model.Transferred = false;

        //    return View("BulkChangeRegistrationType", model);
        //}

        //public ActionResult GenerateRegistrations(BulkChangeRegoTypeViewModel model)
        //{
        //    model.Transferred = false;

        //    return View("BulkCHangeRegistrationType", model);
        //}

        //public ActionResult TransferBulkRego(BulkChangeRegoTypeViewModel model)
        //{
        //    model.TransferRegoType();
        //    model.Transferred = true;
        //    model.selectedRegistrationTypeID = 0;
        //    model.newRegistrationTypeID = 0;

        //    return View("BulkChangeRegistrationType", model);
        //}

    }


}