﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Planetshakers.Events.Models;
using Planetshakers.Core.Models;
using Planetshakers.Core.DataInterfaces;
using System.Web.Mvc;
using Planetshakers.Events.Security;
using Stripe;
using Stripe.Checkout;
using System.Threading.Tasks;

namespace Planetshakers.Events.Controllers
{
    [Authorize]
    public class TeamsController : Controller
    {
        #region Teams
        public ActionResult Teams()
        {
            TeamSearchViewModel model = new TeamSearchViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult SearchTeam(int Event_ID)
        {
            TeamSearchViewModel model = new TeamSearchViewModel();
            model.SelectedConferenceID = Event_ID;
            model.populateResult();
            return View("_TeamSearch", model);
        }

        [HttpPost]
        public ActionResult GetTeamDetails(int Team_ID, int Event_ID)
        {
            TeamModel model = new TeamModel();
            if (Team_ID == 0)
            {
                model.Team_ID = 0;
                model.isEdit = false;
                model.Members = null;
                model.Event_ID = Event_ID;
            }
            else
            {
                model.Team_ID = Team_ID;
                model.getTeam();
            }
            return View("_TeamDetailModal", model);
        }

        [HttpPost]
        public ActionResult GetTeamMemberDetails(int Team_ID)
        {
            TeamModel model = new TeamModel();
            model.Team_ID = Team_ID;
            model.getMember();
            model.populatePotentialMembers();
            return View("_TeamMemberModal", model);
        }

        [HttpPost]
        public ActionResult getExistingRoles(int TeamContact_ID)
        {
            TeamMember model = new TeamMember();
            var html = model.getMemberRole(TeamContact_ID);
            return Json(new { html = html });

        }

        [HttpPost]
        public ActionResult AddMember(int Team_ID, List<int> MemberList)
        {
            TeamModel model = new TeamModel();
            model.Team_ID = Team_ID;
            foreach (var newMemeber in MemberList)
            {
                model.addMember(newMemeber);
            }
            return Json(new { });
        }

        [HttpPost]
        public ActionResult RemoveMember(int TeamContact_ID)
        {
            TeamModel model = new TeamModel();
            var ID = model.removeMember(TeamContact_ID);

            //TeamMember memberModel = new TeamMember();
            //_ = memberModel.deleteMemberTeamRole(TeamContact_ID);
            return Json(new { Team_ID = ID});
        }

        [HttpPost]
        public ActionResult AddTeam(string TeamName, int Event_ID, bool Inactive)
        {
            TeamModel model = new TeamModel();
            model.TeamName = TeamName;
            model.Inactive = Inactive;
            model.Event_ID = Event_ID;
            model.addTeam();
            return Json(new { });
        }

        [HttpPost]
        public ActionResult EditTeam(int Team_ID, int Event_ID, string TeamName, bool Inactive)
        {
            TeamModel model = new TeamModel();
            model.TeamName = TeamName;
            model.Inactive = Inactive;
            model.editTeam(Team_ID);
            return Json(new { });
        }

        #endregion

        #region Team Role
        public ActionResult TeamRole()
        {
            TeamRoleModel model = new TeamRoleModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult getTeamRoleDetails(int TeamRole_ID)
        {
            TeamRole model = new TeamRole();
            if (TeamRole_ID == 0)
            {
                model.Active = true;
                model.TeamRoleName = "";
                model.TeamRole_ID = 0;
            } else
            {
                model.getTeamRole(TeamRole_ID);
            }
            
            return View("_TeamRoleModal", model);
        }

        [HttpPost]
        public ActionResult AddTeamRole(string Name, bool Active)
        {
            TeamRole model = new TeamRole();
            model.TeamRoleName = Name;
            model.Active = Active;
            model.addTeamRole();
            return Json(new { });
        }

        [HttpPost]
        public ActionResult EditTeamRole(int TeamRole_ID, string Name, bool Active)
        {
            TeamRole model = new TeamRole();
            model.TeamRole_ID = TeamRole_ID;
            model.TeamRoleName = Name;
            model.Active = Active;
            model.editTeamRole();
            return Json(new { });
        }

        [HttpPost]
        public ActionResult RemoveTeamRole(int TeamRole_ID)
        {
            TeamRole model = new TeamRole();
            model.removeTeamRole(TeamRole_ID);
            return Json(new { });
        }

        [HttpPost]
        public ActionResult AddEditMemberRole(int TeamContact_ID, List<int> TeamRole_IDs)
        {
            TeamMember model = new TeamMember();
            model.TeamContact_ID = TeamContact_ID;
            var id = model.addEditMemberTeamRole(TeamRole_IDs);
            return Json(new { Team_ID = id });
        }

        #endregion
    }
}