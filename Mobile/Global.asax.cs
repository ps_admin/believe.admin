using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Routing;

namespace Planetshakers.Events
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("robots.txt");
            routes.IgnoreRoute("elmah.axd");
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{*favicon}", new { favicon = @"(.*/)?favicon.ico(/.*)?" });

            routes.MapRoute(
                "Default", 
                "{controller}/{action}/{id}", 
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } 
            );
        }

        protected void Application_BeginRequest()
        {
            if (((string)Application.Get("RequireHttps")).Equals("True"))
            {
                if (!Context.Request.IsSecureConnection)
                {
                    Response.Redirect(Context.Request.Url.ToString().Replace("http:", "https:"));
                }
            }
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);

            Application.Set("RequireHttps", WebConfigurationManager.AppSettings.Get("RequireHttps"));

            System.Configuration.ConnectionStringSettings connString;
            connString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["PlanetshakersDB"];
            Planetshakers.Business.MDataAccess.ConnectionString = connString.ToString();
            

        }      
    }
}