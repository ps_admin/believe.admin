﻿using System;

public static class GlobalText
{
    private static readonly DateTime LastUpdate = new DateTime(2018, 07, 26);

    public static string Version
    {
        get { return string.Format("{0}.{1}.{2:M.d}", 1, 2, LastUpdate); }
    }
}
