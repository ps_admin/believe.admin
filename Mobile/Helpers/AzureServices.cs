﻿using System.Threading.Tasks;
using System;
using Azure.Identity;
using Azure.Security.KeyVault.Secrets;

namespace Planetshakers.Events.Helpers
{
    public static class AzureServices
    {
        private static readonly string _tenantId = "944da9c5-9dc7-4d02-9c8e-a630c656fc2d";
        private static readonly string _applicationId = "bd51ad0e-1c94-49f4-9f5e-864e3e9eeedd";
        private static readonly string _clientSecret = "pOc8Q~auLfB8bgoWvg3pxra~LAxz7dEeSoQWIc3N";

        public static async Task<string> KeyVault_RetreiveSecretAsync(string keyVaultName, string secretName)
        {
            ClientSecretCredential credentials = new ClientSecretCredential(_tenantId, _applicationId, _clientSecret);

            var secretClient = new SecretClient(new Uri($"https://{keyVaultName}.vault.azure.net/"), credentials);

            try
            {
                var secret = await secretClient.GetSecretAsync(secretName);
                return secret.Value.Value.ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Failed to retrieve secret: {ex.Message}");
                return ex.Message.ToString();
            }
        }

        public static string KeyVault_RetreiveSecret(string keyVaultName, string secretName)
        {
            ClientSecretCredential credentials = new ClientSecretCredential(_tenantId, _applicationId, _clientSecret);

            var secretClient = new SecretClient(new Uri($"https://{keyVaultName}.vault.azure.net/"), credentials);

            try
            {
                var secret = secretClient.GetSecret(secretName);
                return secret.Value.Value.ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Failed to retrieve secret: {ex.Message}");
                return ex.Message.ToString();
            }
        }
    }
}