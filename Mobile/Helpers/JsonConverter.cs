﻿namespace Planetshakers.Events.Helpers
{
    public static class JsonConverter
    {
        public static string Serialize(object obj)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(obj);
        }

        public static T Deserialize<T>(string str) where T : class
        {
            return (T)Newtonsoft.Json.JsonConvert.DeserializeObject(str, typeof(T));
        }
    }
}