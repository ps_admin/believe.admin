﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace Planetshakers.Events.Helpers
{
    public static class ViewExtensions
    {
        public static MvcHtmlString ErrorValidationSummary(this HtmlHelper helper, string validationMessage)
        {
            string retVal = string.Empty;

            if (helper.ViewData.ModelState.IsValid)
                return MvcHtmlString.Empty;

            retVal += "<div data-alert class='alert-box alert'><div>";
            
            if (!string.IsNullOrEmpty(validationMessage))
                retVal += helper.Encode(validationMessage);
            
            retVal += "</div>";
            
            foreach (var key in helper.ViewData.ModelState.Keys)
            {
                foreach (var err in helper.ViewData.ModelState[key].Errors)
                    retVal += "<div>" + helper.Encode(err.ErrorMessage) + "</div>";
            }
            
            retVal += "</div>";
            
            return MvcHtmlString.Create(retVal.ToString());
        }
    }
}
