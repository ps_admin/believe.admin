﻿using Markdig;
using System.Net;

namespace Planetshakers.Church.Web.vNext.Helpers
{
    public static class WebOperations
    {
        public static string ParseMarkdown(string input)
        {
            if (string.IsNullOrEmpty(input))
                return "";
            var pipeline = new MarkdownPipelineBuilder().Build();
            var result = Markdown.ToHtml(input, pipeline);
            return result;
        }

        public static string ParseUrlToString(string url)
        {
            using (WebClient client = new WebClient())
            {
                string s = client.DownloadString(url);
                return s;
            }
        }
    }
}
