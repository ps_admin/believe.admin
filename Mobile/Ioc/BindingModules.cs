﻿using System.Configuration;
using Ninject.Modules;
using Planetshakers.Core.DataInterfaces;
using Planetshakers.Data.SqlServer.Repositories;

namespace Planetshakers.Events.Ioc
{
    public class BindingModules : NinjectModule
    {
        public override void Load()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["PlanetshakersDB"].ConnectionString;

            Bind<IAccountRepository>().To<AccountRepository>().WithConstructorArgument("connectionString", connectionString);
            /*Bind<IUserRepository>().To<UserRepository>().WithConstructorArgument("connectionString", connectionString);
            Bind<IUrbanLifeRepository>().To<UrbanLifeRepository>().WithConstructorArgument("connectionString", connectionString);
            Bind<IPastoralCareRepository>().To<PastoralCareRepository>().WithConstructorArgument("connectionString", connectionString);
            Bind<ICheckInRepository>().To<CheckInRepository>().WithConstructorArgument("connectionString", connectionString);
            Bind<IAdministrationRepository>().To<AdministrationRepository>().WithConstructorArgument("connectionString", connectionString);*/
        }
    }
}