﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;
using Planetshakers.Business.External.Events;
using Newtonsoft.Json;
using Planetshakers.Core.Models;

namespace Planetshakers.Events.Models
{
    public class LogOnModel
    {
        public LogOnModel()
        {
            RememberMe = true;
            GPlus = false;
        }

        public string UserName { get; set; }
        public string Password { get; set; }
        public bool RememberMe { get; set; }
        public bool GPlus { get; set; }

    }

    public class GoogleProfile
    {
        public string Id { get; set; }
        public string DisplayName { get; set; }
        public GImage Image { get; set; }
        public List<GEmail> Emails { get; set; }
        public string Gender { get; set; }
        public string ObjectType { get; set; }
        public string BirthDay { get; set; }
        public gFN Name { get; set; }
       // public string givenName { get; set; }
        public string sAddress1 { get; set; }
        public string sAddress2 { get; set; }
        public string sSuburb { get; set; }
        public string sPostCode { get; set; }
        public string sState { get; set; }
        public string sStateOther { get; set; }
        public string sCountry { get; set; }
        public string sMobile { get; set; }
        public string IsPlusUserCode { get; set; }
        public string accessCode { get; set; }
        public string verified { get; set; }
        public string Language { get; set; }
        }

    public class GEmail
    {
        public string Value { get; set; }
        public string Type { get; set; }
    }

    public class GImage
    {
        public string Url { get; set; }
    }

    public class gFN
    {
        public string familyName { get; set; }
        public string givenName { get; set; }

    }
    public class RegistrationCategory
    {
        // Name of the registration category (should generally be Single/Group)
        public string name { get; set; }

        // Determines whether this is a group registration
        public bool is_group_rego { get; set; }

        // The id which groups up the registration together (based on the RegistrationTypeQuantitiyGroupId in the database)
        public int? group_id { get; set; }

        // Determines the minimum qty before the group bonuses apply (set to 1 for single registration}
        public int group_min_qty { get; set; }

        // List of registration types under this category
        public List<RegistrationType> rego_types { get; set; }

    }

    public class RegistrationType
    {
        // registration type id
        public int id { get; set; }

        // name of the registration type
        public string name { get; set; }

        // cost per registration
        private decimal _cost;
        public decimal cost
        {
            get
            {
                return Math.Round(_cost, 2);
            }
            set
            {
                this._cost = value;
            }
        }
    }

    /* Datamodel for the event */
    public class Event
    {
        public Event()
        {

        }

        public Event(int id, string name, string date, string location, string venueLocation)
        {
            this.id = id;
            this.name = name;
            this.date = date;
            this.location = location;
            this.venueLocation = venueLocation;
        }

        // ID of the conference / event
        public int id { get; set; }

        public string name { get; set; }

        // short name of the event
        public string shortname { get; set; }

        // event tagline
        public string tagline { get; set; }

        // date of the event
        public string date { get; set; }

        // location of the event
        public string location { get; set; }

        public string venueLocation { get; set; }

        // venue id
        public int venue_id { get; set; }
        
        // conference type
        public MEnumerators.EnumConferenceType conference_type { get; set; }

        // path of the banner image
        public string banner_img
        {
            get
            {
                return "~/Content/themes/planetshakers/img/banner_" + shortname + ".jpg";
            }
        }

        // path to the promo image
        public string promo_img
        {
            get
            {
                return "~/Content/themes/planetshakers/img/promo_" + shortname + ".jpg";
            }
        }

        // event currency
        public string currency { get; set; }

        // List of available registration categories
        public List<RegistrationCategory> rego_categories { get; set; }

        // function to generate header for the index
        public string generateHeader()
        {
            if (String.IsNullOrWhiteSpace(tagline)) return name;
            else return name + " - " + tagline;
        }

        public decimal cost;
        public int sort_order { get; set; }
    }

    public class CartModel
    {
        public CartModel()
        {
            Event = new Event();
            CCPayment = new CCPaymentModel();
            Cart = new List<CartItem>();
        }

        public CartModel(int event_id)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                this.Event = (from ec in context.Events_Conference
                              join ev in context.Events_Venue
                              on ec.Conference_ID equals ev.Conference_ID
                              join ba in context.Common_BankAccount
                              on ec.BankAccount_ID equals ba.BankAccount_ID
                              where ec.Conference_ID == event_id
                              select new Event
                              {
                                  id = ec.Conference_ID,
                                  venue_id = ev.Venue_ID,
                                  name = ec.ConferenceName,
                                  date = ec.ConferenceDate,
                                  location = ev.VenueName,
                                  venueLocation = ev.VenueLocation,
                                  currency = ba.Currency
                              }).FirstOrDefault();

                Cart = new List<CartItem>();
                CartType = "Events and Conferences";
            }
        }

        public CartModel(int contact_id, Event evnt)
        {
            ContactId = contact_id;
            Event = evnt;
        }

        public string ErrorMessage { get; set; }

        // registrant contact
        public int ContactId { get; set; }

        public string FullName 
        {
            get 
            {
                var name = String.Empty;

                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    name = (from cc in context.Common_Contact
                            where cc.Contact_ID == ContactId
                            select cc.FirstName + " " + cc.LastName).FirstOrDefault();
                }

                return name;
            }                
        }

        public string Email
        {
            get
            {
                var name = String.Empty;

                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    // Need to account for people who may not have an email
                    name = (from cc in context.Common_Contact
                            where cc.Contact_ID == ContactId
                            select (String.IsNullOrEmpty(cc.Email) ? cc.Email2 : cc.Email )).FirstOrDefault();
                }

                return name;
            }
        }

        // Items in the cart
        public List<CartItem> Cart { get; set; }

        // Event 
        public Event Event { get; set; }

        public int TotalRegos { get { return Cart.Sum(r => r.quantity); } }

        // Total cost
        public decimal GrandTotal
        {
            get
            {
                var total = Cart.Sum(r => r.subtotal) - (Voucher != null ? Voucher.value : 0);

                return (total > 0 ? total : 0);
            }
        }
        public decimal RemainingCredit
        {
            get
            {
                var credits = Cart.Sum(r => r.subtotal) - (Voucher != null ? Voucher.value : 0);

                return (credits < 0 ? credits * -1 : 0);
            }
        }
        // CC Info
        public CCPaymentModel CCPayment { get; set; }

        public string CartType { get; set; }

        public string Comments { get; set; }

        public decimal Subtotal { get; set; }
        public Voucher Voucher { get; set; }

    }

    public class CCPaymentModel
    {
        /*
        [Display(Name = "Credit Card Type")]
        public int CCType { get; set; }
        public SelectList CCTypeList { get; set; }
        */
        [Required]
        [Display(Name = "Credit Card Number")]
        public string CCNumber { get; set; }
        public int payment_id { get; set; }
        public int paymenttype_id { get; set; }
        public string PaymentType 
        { 
            get
            {
                using (var ctx = new PlanetshakersEntities())
                {
                    var type = (from pt in ctx.Common_PaymentType
                                  where pt.PaymentType_ID == paymenttype_id
                                  select pt.PaymentType).SingleOrDefault();

                    return type;
                }
            } 
        }
        public string getCCNumber(bool masked)
        {
            if (masked)
            {
                return String.Concat(CCNumber.Substring(0, 4),
                                     "".PadLeft(CCNumber.Length - 8, 'X'),
                                     CCNumber.Substring(CCNumber.Length - 4));
            }
            else
            {
                return CCNumber.Replace("-", "").Replace(" ", "").Replace("/", "");
            }
        }

        [Required]
        [Display(Name = "Credit Card Expiry")]
        public int CCExpiryMonth { get; set; }
        public SelectList CCExpMonthList { get; set; }
        [Required]
        public int CCExpiryYear { get; set; }
        public SelectList CCExpYearList { get; set; }

        public string getCCExpiry()
        {
            return CCExpiryMonth.ToString("00") + CCExpiryYear.ToString("00");
        }

        [Required]
        [Display(Name = "CVC2")]
        public string CCCVC { get; set; }

        [Required]
        [Display(Name = "Cardholder's Name")]
        public string CCName { get; set; }

        [Required]
        [Display(Name = "Cardholder's Phone #")]
        public string CCPhone { get; set; }

        public Common_BankAccount BankAccount { get; set; }

        public string transactionReference { get; set; }

    }
    public class CartItem
    {

        // Logged in contact
        public int ContactId { get; set; }

        public string ErrorMessage { get; set; }

        // registration type id
        public int reg_id { get; set; }

        // registration name
        public string reg_name { get; set; }

            // path of the banner image
        public string banner_img
        {
            get
            {
                return "~/Content/images/banner_" + reg_name + ".jpg";
            }
        }
        // event id
        public int event_id { get; set; }

        // event name
        public string event_name { get; set; }

        // date of the event
        public string date { get; set; }

        // location of the event
        public string location { get; set; }
        // quantity to purchase
        public int quantity { get; set; }

        // single price
        private decimal _single_price;
        public decimal single_price
        {
            get
            {
                return Math.Round(_single_price, 2);
            }
            set
            {
                this._single_price = value;
            }
        }

        // event currency
        public string currency { get; set; }

        // subtotal
        public decimal subtotal { get { return Math.Round(quantity * single_price, 2); } }

        // CC Info
        public CCPaymentModel CCPayment { get; set; }
    }

    // duplicate defn.. available here and Mail controler.

    public class RegistrationConfirmationItem
    {
        // reference number
        public string reference_number { get; set; }

        // registration name
        public string reg_name { get; set; }
          
        public int qty { get; set; }

        // single price
        private decimal _price;
        public decimal price
        {
            get
            {
                return Math.Round(_price, 2);
            }
            set
            {
                this._price = value;
            }
        }

        public decimal subtotal { get { return price * qty ; } }
    }
    
    public class RegistrationReceiptEmailModel
    {
        public int ContactId { get; set; }

        [JsonProperty("ReceiptNumber")]
        public string ReceiptNumber { get; set; }

        [JsonProperty("Email")]
        public string Email { get; set; }

        [JsonProperty("FullName")]
        public string FullName { get; set; }

        [JsonProperty("Subject")]
        public string Subject { get; set; }

        public List<RegistrationConfirmationItem> Items { get; set; }

        [JsonProperty("ItemList")]
        public string ItemList { get; set; }

        //Used in Event payment receipt to send tax invoice
        [JsonProperty("PaymentList")]
        public string PaymentList { get; set; }

        public Event Event { get; set; }

        [JsonProperty("EventName")]
        public string EventName { get; set; }

        [JsonProperty("EventDate")]        
        public string EventDate { get; set; }

        [JsonProperty("EventVenue")]
        public string EventVenue { get; set; }

        [JsonProperty("Currency")]
        public string Currency { get; set; }
        
        [JsonProperty("PaymentType")]
        public string PaymentType { get; set; }
            
        [JsonProperty("TransactionReference")]
        public string TransactionReference { get; set; }

        public decimal _grandtotal { get; set; }

        //Payment total is used in sending registration tax invoice
        [JsonProperty("PaymentTotal")]
        public decimal PaymentTotal { get; set; }

        [JsonProperty("GrandTotal")]
        public decimal GrandTotal
        {
            get
            {
                var total = Items.Sum(r => r.price * r.qty) - (Promo != null ? Promo.value : 0);

                if (total <= 0)
                {
                    return 0;
                }
                else
                {
                    return total;
                }
            }
        }

        public int QuantityTotal { get { return Items.Sum(r => r.qty); } }

        public Voucher Promo { get; set; }
    }


    public class RegisterModel
    {
        public string SuccessMessage { get; set; }
        public string ErrorMessage { get; set; }
        public string returnUrl { get; set; }

        [Required]
        [DataType(DataType.EmailAddress, ErrorMessage = "Invalid Email Address")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [System.ComponentModel.DataAnnotations.Compare("Email", ErrorMessage = "Emails do not match")]
        [Display(Name = "Confirm Email")]
        public string ConfirmEmail { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Password must be a minimum of 8 characters long", MinimumLength = 8)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "Passwords do not match")]
        [Display(Name = "Confirm Password")]
        public string ConfirmPassword { get; set; }

        [Required]
        [Display(Name = "Title")]
        public int Salutation { get; set; }
        public SelectList SalutationList { get; set; }

        [Required]
        [Display(Name = "First Name")]
        public string Firstname { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string Lastname { get; set; }

        [Required]
        [Display(Name = "Date of Birth")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd MMMM yyyy}")]
        public DateTime? DoB { get; set; }

        [Required]
        [Display(Name = "Gender")]
        public string Gender { get; set; }
        public SelectList GenderList { get; set; }

        [Display(Name = "Address")]
        public string Address1 { get; set; }
        public string Address2 { get; set; }

        [Required]
        [Display(Name = "Suburb / City")]
        public string Suburb { get; set; }

        // Temporarily unused
        [Required]
        [Display(Name = "State")]
        public int State { get; set; }
        public SelectList StateList { get; set; }

        [Required]
        [Display(Name = "State")]
        public string StateOther { get; set; }

        [Required]
        [Display(Name = "Zip / Postcode")]
        public string Postcode { get; set; }

        [Required]
        [Display(Name = "Country")]
        public int Country { get; set; }
        public SelectList CountryList { get; set; }

        [Required]
        [Display(Name = "Mobile")]
        public string Mobile { get; set; }

    }

}