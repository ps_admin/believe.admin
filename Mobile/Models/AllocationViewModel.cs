﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

using Planetshakers.Core.Models;

namespace Planetshakers.Events.Models
{
    public class AllocationViewModel
    {
        public int Contact_ID { get; set; }
        public string ContactName
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var name = (from cc in context.Common_Contact
                                where cc.Contact_ID == Contact_ID
                                select cc.FirstName + " " + cc.LastName).FirstOrDefault();

                    return name;
                }
            }
        }

        public int Registration_ID { get; set; }     

        [Display(Name = "Event")]
        public int Venue_ID { get; set; }

        public int Conference_ID { get; set; }

        public string ConferenceName
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var name = (from ev in context.Events_Venue
                                join ec in context.Events_Conference
                                    on ev.Conference_ID equals ec.Conference_ID
                                where ev.Venue_ID == Venue_ID
                                select ec.ConferenceName).FirstOrDefault();

                    return name;
                }
            }
        }

        private List<EventModel> _events
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var _events = (from ec in context.Events_Conference
                                   join ev in context.Events_Venue
                                   on ec.Conference_ID equals ev.Conference_ID
                                   orderby ev.ConferenceStartDate descending, Venue_ID ascending
                                   select new EventModel
                                   {
                                       VenueID = ev.Venue_ID,
                                       ConferenceName = ec.ConferenceName
                                   }).ToList();

                    return _events;
                }
            }
        }

        public IEnumerable<SelectListItem> EventList { get { return new SelectList(_events, "VenueID", "ConferenceName"); } }

        public List<ContactDetails> searchList { get; set; }

        public int RegistrationType_ID { get; set; }
        public string RegistrationType
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var name = (from ert in context.Events_RegistrationType
                                where ert.RegistrationType_ID == RegistrationType_ID
                                select ert.RegistrationType).FirstOrDefault();

                    return name;
                }
            }
        }

        public List<UnallocatedRegistration> unallocatedRegoList
        {
            get
            {
                var unallocated_regos = new List<UnallocatedRegistration>();

                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = (from egbr in context.Events_GroupBulkRegistration
                                where egbr.GroupLeader_ID == Contact_ID
                                        && egbr.Venue_ID == Venue_ID
                                select egbr).ToList();
                    


                    foreach (var item in list)
                    {
                        var unallocated = new UnallocatedRegistration();

                        unallocated.GroupLeader_ID = item.GroupLeader_ID;
                        unallocated.Venue_ID = item.Venue_ID;

                        unallocated.GroupLeader = (from eg in context.Events_Group
                                                    where eg.GroupLeader_ID == item.GroupLeader_ID
                                                    select eg.GroupName).FirstOrDefault();

                        unallocated.RegistrationType_ID = item.RegistrationType_ID;

                        unallocated.RegistrationType = (from ert in context.Events_RegistrationType
                                                         where ert.RegistrationType_ID == item.RegistrationType_ID
                                                         select ert.RegistrationType).FirstOrDefault();

                        unallocated.Venue_ID = item.Venue_ID;
                        //unallocated.Allocated = (int) item.Allocated;
                        //unallocated.Unallocated = (int) item.UnAllocated;

                        unallocated_regos.Add(unallocated);
                    }

                    return unallocated_regos;                    
                }
            }
        }

        public int TotalUnallocated
        {
            get
            {
                return this.unallocatedRegoList.Sum(x => x.Unallocated);
            }
        }

        public int TotalAllocated
        {
            get
            {
                return this.unallocatedRegoList.Sum(x => x.Allocated);
            }
        }

        public List<Registration> AllocatedRegoList
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = (from er in context.Events_Registration
                                join cc in context.Common_Contact
                                    on er.Contact_ID equals cc.Contact_ID
                                where er.GroupLeader_ID == Contact_ID && er.Venue_ID == this.Venue_ID
                                select new Registration
                                {
                                    ID = er.Registration_ID,
                                    FirstName = cc.FirstName,
                                    LastName = cc.LastName,
                                    Mobile = cc.Mobile,
                                    Email = (cc.Email == String.Empty ? cc.Email2 : cc.Email)
                                }).ToList();

                    return list;
                }                
            }
        }

        public List<SelectListItem> AvailableRegistrationTypeList
        {
            get
            {
                var list = unallocatedRegoList.Where(x => x.Unallocated >= 1).ToList();

                var selectlist = new List<SelectListItem>();

                foreach (var i in list)
                    selectlist.Add(new SelectListItem { Text=i.RegistrationType, Value=i.RegistrationType_ID.ToString() });

                return selectlist;
            }
        }
        
        public string SelectedOption { get; set; }
        public List<SelectListItem> AllocateOption
        {
            get
            {
                var selectlist = new List<SelectListItem> {
                    new SelectListItem{ Text="Existing", Value="Existing" },
                    new SelectListItem{ Text="New", Value="New" }
                };

                return selectlist;
            }
        }

        public int Allocation_ID { get; set; }

        public string SuccessMessage { get; set; }
        
        [Display(Name = "First Name")]
        public string NewFirstName { get; set; }

        [Display(Name = "Last Name")]
        public string NewLastName { get; set; }

        [Display(Name = "Mobile")]
        public string NewMobile { get; set; }

        [Display(Name = "Email")]
        public string NewEmail { get; set; }

        [Display(Name = "Attended?")]
        public bool NewAttended { get; set; }
    }
}