﻿using System.Collections.Generic;
using System.Linq;

namespace Planetshakers.Events.Models
{
    public class ApplicationDocumentTypeViewModel
    {
        
        public List<Volunteer_DocumentType> ApplicationDocumentType
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = (from q in context.Volunteer_DocumentType
                                orderby q.SortOrder
                                select q).ToList();
                    return list;
                }
            }
        }
    }
    public class ApplicationDocumentType
    {
        public int DocumentType_ID { get; set; }
        public string DocumentType { get; set; }
        public bool Active { get; set; }
        public bool isEdit { get; set; }
    }

}