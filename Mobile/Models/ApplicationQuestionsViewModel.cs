﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Planetshakers.Events.Models
{
    public class ApplicationQuestionsViewModel
    {
        
        public List<Volunteer_ApplicationQuestion> ApplicationQuestions
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = (from q in context.Volunteer_ApplicationQuestion
                                orderby q.SortOrder
                                select q).ToList();
                    return list;
                }
            }
        }
    }
    public class ApplicationQuestion
    {
        public int Question_ID { get; set; }
        public string Question { get; set; }
        public string Type { get; set; }
        public List<SelectListItem> TypeList { get
            {
                var List = new List<SelectListItem>
                {
                    new SelectListItem() { Text = "Checkbox", Value = "Checkbox" },
                    new SelectListItem() { Text = "Paragraph", Value = "Paragraph" },
                    new SelectListItem() { Text = "Short Answer", Value = "Short Answer" },
                    new SelectListItem() { Text = "Multiple-Choice", Value = "Multiple-Choice" }
                };

                return List;
            } }
        public string ShortName { get; set; }
        public bool Required { get; set; }
        public bool Active { get; set; }
        public bool isEdit { get; set; }
    }

}