﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Planetshakers.Events.Models
{
    public class ApplicationReportViewModel
    {
        public int Event_ID { get; set; }
        public List<ApplicationReportQuestion> Questions { get; set; }
        public List<ApplicationReportApplication> Applications { get; set; }
        public List<ApplicationReportSummary> Summary { get; set; }

        public List<SelectListItem> ConferenceList
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = (from c in context.Events_Conference
                                orderby c.SortOrder descending
                                select new SelectListItem
                                {
                                    Text = c.ConferenceName,
                                    Value = c.Conference_ID.ToString().Trim()
                                }).ToList();

                    return list;
                }
            }
        }
        public List<SelectListItem> QuestionList
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = (from c in context.Volunteer_ApplicationQuestion
                                orderby c.SortOrder
                                select new SelectListItem
                                {
                                    Text = c.ShortName,
                                    Value = c.Question_ID.ToString().Trim()
                                }).ToList();
                    return list;
                }
            }
        }

    }

    public class ApplicationReportQuestion
    {
        public string Question { get; set; }
        public string FullQuestion { get; set; }
        public int Question_ID { get; set; }    
        public string Type { get; set; }
        public int RegistrationType_ID { get; set; }
    }

    public class ApplicationReportApplication
    {
        public int Contact_ID { get; set; }
        public string ContactName { get; set; }
        public int Application_ID { get; set; }
        public List<ApplicationReportAnswer> Answers { get; set; }
    }

    public class ApplicationReportAnswer
    {
        public int Question_ID { get; set; }
        public string Answer { get; set; }
    }
    public class ApplicationReportSummary
    {
        public string Question { get; set; }
        public int Question_ID { get; set; }
        public List<ApplicationReportSummaryOption> Options { get; set; }
    }
    public class ApplicationReportSummaryOption
    {
        public string option { get; set; }
        public int count { get; set; }
    }
}