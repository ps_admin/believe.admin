﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Planetshakers.Events.Models
{
    public class ApplicationViewModel
    {
        public string SelectedConference { get; set; }
        public int? SelectedConferenceID { get; set; }
        public List<Application> ApplicationList { get; set; }
        public List<SelectListItem> event_list
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {

                    var list = (from ec in context.Events_Conference
                                join ev in context.Events_Venue
                                    on ec.Conference_ID equals ev.Conference_ID
                                //orderby ev.ConferenceStartDate descending
                                orderby ec.SortOrder ascending
                                select new SelectListItem
                                {
                                    Value = ec.Conference_ID.ToString().Trim(),
                                    Text = ec.ConferenceName
                                }).ToList();

                    return list;
                }
            }
        }
        public List<SelectListItem> RegistrationTypeList { get; set; }

        public List<SelectListItem> ApplicationStatus
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = (from ec in context.Volunteer_ApplicationStatus
                                orderby ec.SortOrder
                                select new SelectListItem
                                {
                                    Value = ec.Status_ID.ToString().Trim(),
                                    Text = ec.Name
                                }).ToList();

                    return list;
                }
            }
        }

    }

    public class Application
    {
        public int Application_ID { get; set; }
        public int Contact_ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public DateTime SubmittedTime { get; set; }
        public DateTime? ApprovedTime { get; set; }
        public string RegistrationType { get; set; }
        public int RegistrationType_ID { get; set; }
        public int Event_ID { get; set; }
        public int Status_ID { get; set; }

        public List<QuestionAnswer> Details { get; set; }
        public List<ReferralModel> Referrals { get; set; }
        public ContactQuestion ContactQuestion { get; set; }

    }

    public class QuestionAnswer
    {
        public int Answer_ID { get; set; }
        public int Question_ID { get; set; }
        public string Answer { get; set; }
        public string Question { get; set; }

    }

    public class ReferralModel
    {
        public int Referral_ID { get; set; }
        public int ReferralType_ID { get; set; }
        public string ReferralType { get; set; }
        public string FullName { get; set; }
        public string Relationship { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public bool Approved { get; set; }
        public string Comment { get; set; }

    }

    public class ContactQuestion
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public DateTime DOB { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Suburb { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }
        public string Gender { get; set; }
        public string State { get; set; }
    }

    public class ApplicationEmergencyContact
    {
        public int EmergencyContact_ID { get; set; }
        public string Name { get; set; }
        public string Mobile { get; set; }
        public string Relationship { get; set; }

    }

    public class ApplicationApprovedEmailModal
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string TripName { get; set; }
        public int Venue_ID { get; set; }
        public string TripDashBoard {get; set;}

        public string getTripDashBoard()
        {
            var link = "missions.believeglobal.org/TripDetails/TripDetails/" + Venue_ID;
            //return "<a href='"+link+"'>Trip Dash Board</a>";
            return link;
        }
        public string TripPaymentProgress { get; set; }
        public string getTripPaymentProgress()
        {
            var link = "missions.believeglobal.org/Events/Registrations/" + Venue_ID;
            //return "<a href='" + link + "'>Trip Payment Progress</a>";
            return link;
        }
        public string Template_ID
        {
            get
            {
                return "d-390548d0842b4e7186f53ce94c5a3148";
            }
        }
    }
}