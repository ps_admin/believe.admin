﻿using System.Collections.Generic;
using Planetshakers.Core.Models;

namespace Planetshakers.Events.Models
{
    public class AssignModel
    {
        public AssignModel(int contactId)
        {
            ContactId = contactId;
            CarerAccessDetails = new List<CarerAccessDetail>();
        }

        public int ContactId { get; private set; }

        public List<CarerAccessDetail> CarerAccessDetails { get; set; }
    }
}