﻿using System.Collections.Generic;
using System.Linq;

namespace Planetshakers.Events.Models
{
    public class BankingReportBreakdown
    {
        public int Event_ID { get; set; }
        public List<BankingReportBreakdownInstance> report { get; set; }

        public void generateReport(int PaymentType_ID)
        {
            using (PlanetshakersEntities _ctx = new PlanetshakersEntities())
            {
                if (PaymentType_ID == 9)
                {
                    var paymentSources = new List<int>() { 1, 2, 3, 4, 6 };
                    //man transfer
                    var payments = (from g in _ctx.Events_GroupPayment
                                    join s in _ctx.Common_PaymentSource on g.PaymentSource_ID equals s.PaymentSource_ID
                                    where g.Conference_ID == Event_ID && g.PaymentType_ID == PaymentType_ID 
                                            && paymentSources.Contains(g.PaymentSource_ID) && g.PaymentCompleted == true && g.PaymentAmount > 0
                                    select new BankingReportBreakdownInstance
                                    {
                                        PaymentType = s.PaymentSource,
                                        Value = g.PaymentAmount
                                    }).ToList();
                    var refundAmount = (from g in _ctx.Events_GroupPayment
                                        where g.Conference_ID == Event_ID && g.PaymentType_ID == PaymentType_ID
                                            && g.PaymentCompleted == true && g.CCRefund == true
                                        select g.PaymentAmount).Sum();
                    this.report = (from p in payments
                                   group p by p.PaymentType into newGroup
                                   select new BankingReportBreakdownInstance()
                                   {
                                       PaymentType = newGroup.Key,
                                       Value = newGroup.Sum(x => x.Value)
                                   }).ToList();
                    if(refundAmount != 0)
                    {
                        this.report.Add(new BankingReportBreakdownInstance()
                        {
                            PaymentType = "Refund",
                            Value = refundAmount
                        });
                    }
                }
                else if (PaymentType_ID == 12)
                {
                    //sponsorship
                    var paymentTypes = new List<int> { 3, 9, 12 };
                    var payments = (from v in _ctx.Events_VariablePayment
                                    join t in _ctx.Common_PaymentType on v.PaymentType_ID equals t.PaymentType_ID
                                    where v.Conference_ID == Event_ID && paymentTypes.Contains(v.PaymentType_ID) 
                                            && v.PaymentCompleted == true && v.OutgoingContact_ID == null && v.Refund == false
                                    select new BankingReportBreakdownInstance
                                    {
                                        PaymentType = t.PaymentType,
                                        Value = v.PaymentAmount ?? 0
                                    }).ToList();
                    var refundAmount = (from v in _ctx.Events_VariablePayment
                                        where v.Conference_ID == Event_ID && v.PaymentCompleted == true && v.OutgoingContact_ID == null && v.Refund == true
                                        select v.PaymentAmount).Sum();
                    this.report = (from p in payments
                                   group p by p.PaymentType into newGroup
                                   select new BankingReportBreakdownInstance()
                                   {
                                       PaymentType = newGroup.Key,
                                       Value = newGroup.Sum(x => x.Value)
                                   }).ToList();
                    if (refundAmount != null)
                    {
                        this.report.Add(new BankingReportBreakdownInstance()
                        {
                            PaymentType = "Refund",
                            Value = (decimal)refundAmount
                        });
                    }
                }
            }
        }
    }

    public class BankingReportBreakdownInstance
    {
        public string PaymentType { get; set; }
        public decimal Value { get; set; }
    }

}