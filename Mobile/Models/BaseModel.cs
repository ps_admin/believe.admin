﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Planetshakers.Events.Models
{
    public class BaseModel
    {
        /// <summary>
        /// Used to differentiate different database connections
        /// </summary>
        public static string databaseInstance { get; set; }
        
        /// <summary>
        /// Webpage tite
        /// </summary>
        public static string pageTitle { get; set; }

        /// <summary>
        /// Logo of the
        /// </summary>
        public static string logoImagePath { get; set; }

        /// <summary>
        /// Feedback email
        /// </summary>
        public static string feedbackEmail { get; set; }

        /// <summary>
        /// Link to the main pcr website
        /// </summary>
        public static string mainSite { get; set; }

        //Initialise
        static BaseModel() 
        { 
            if (String.IsNullOrWhiteSpace(databaseInstance))
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    databaseInstance = (from di in context.Common_DatabaseInstance
                                        select di.DatabaseInstance).FirstOrDefault();
                }
            }
            
            pageTitle = "Believe Admin";
            logoImagePath = "/Content/images/logo-planetshakers.png";
            feedbackEmail = "feedback@planetshakers.com";
        }

    }
}
