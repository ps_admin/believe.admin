﻿using Planetshakers.Core.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Planetshakers.Events.Models
{
    public class BulkChangeRegoTypeViewModel
    {

        public int selectedConferenceID { get; set; }

        public int selectedRegistrationTypeID { get; set; }

        public int newRegistrationTypeID { get; set; }

        public Events_Conference selectedConference { get {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    Events_Conference selected = new Events_Conference();
                    selected = (from ec in context.Events_Conference
                                where ec.Conference_ID == selectedConferenceID
                                select ec).FirstOrDefault();

                    return selected;
                }
            } 
        }

        public Events_RegistrationType selectedRegistrationType { get {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    Events_RegistrationType selected = new Events_RegistrationType();
                    selected = (from ert in context.Events_RegistrationType
                                where ert.RegistrationType_ID == selectedRegistrationTypeID
                                select ert).FirstOrDefault();

                    return selected;
                }
            }
        }

        public List<SelectListItem> availableRegistrationTypeList
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {

                    var list = (from ert in context.Events_RegistrationType
                                where ert.Conference_ID == selectedConferenceID
                                select new SelectListItem
                                {
                                    Value = ert.RegistrationType_ID.ToString().Trim(),
                                    Text = ert.RegistrationType
                                }).ToList();

                    return list;
                }
            }
        }

        public List<Registration> registrationList {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var registrations = (from er in context.Events_Registration
                                         join cc in context.Common_Contact
                                         on er.Contact_ID equals cc.Contact_ID
                                         where er.RegistrationType_ID == selectedRegistrationTypeID
                                         select new Registration
                                         {
                                             ID = er.Registration_ID,
                                             ContactID = er.Contact_ID,
                                             FirstName = cc.FirstName,
                                             LastName = cc.LastName,
                                             Attended = er.Attended
                                         }).ToList();

                    return registrations;
                }
            }
        }

        public void TransferRegoType()
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var regolist = (from er in context.Events_Registration
                                where er.RegistrationType_ID == selectedRegistrationTypeID
                                select er).ToList();

                foreach (var rego in regolist)
                {
                    rego.RegistrationType_ID = newRegistrationTypeID;
                    context.SaveChanges();
                }
            }
        }

        public bool Transferred { get; set; }
    }
}