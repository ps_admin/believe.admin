﻿namespace Planetshakers.Events.Models
{
    public class CallListViewModel
    {
        /*public string SelectedListOption { get; set; }

        public List<SelectListItem> ListOption
        {
            get
            {
                var list = new List<SelectListItem> {
                    new SelectListItem { Text="Internal", Value="Internal" },
                    new SelectListItem { Text="External", Value="External"}
                };

                return list;
            }
        }        

        public int SelectedCampus_ID { get; set; }

        public List<SelectListItem> Campus_List
        {
            get
            {
                var list = new List<SelectListItem>();

                using(PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    list = (from ccam in context.Church_Campus
                            select new SelectListItem
                            {
                                Text = ccam.Name,
                                Value = ccam.Campus_ID.ToString().Trim()
                            }).ToList();
                }

                return list;
            }
        }

        public int SelectedMinistry_ID { get; set; }

        public List<SelectListItem> Ministry_List
        {
            get
            {
                var list = new List<SelectListItem>();

                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    list = (from cm in context.Church_Ministry
                            select new SelectListItem
                            {
                                Text = cm.Name,
                                Value = cm.Ministry_ID.ToString().Trim()
                            }).ToList();
                }

                return list;
            }
        }

        public List<CallItem> CallList
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {

                    var list = new List<CallItem>();                    
                    
                    if (SelectedListOption == "Internal")
                    {                        
                        list = (from icl in context.vw_Events_InternalCallList
                                where (icl.Campus_ID == SelectedCampus_ID) && (icl.Ministry_ID == SelectedMinistry_ID)                                       
                                    select new CallItem
                                    {
                                        Contact_ID = icl.Contact_ID,
                                        FirstName = icl.FirstName,
                                        LastName = icl.LastName,
                                        Mobile = icl.Mobile,
                                        CallStatus_ID = icl.CallStatus_ID,
                                        CurrentStatus = icl.Status,
                                        Comment = icl.Comments
                                    }).OrderBy(x => x.LastName).Skip(range).Take(100).ToList();
                    } else if (SelectedListOption == "External")
                    {
                        list = (from icl in context.vw_Events_ExternalCallList
                                
                                    select new CallItem
                                    {
                                        Contact_ID = icl.Contact_ID,
                                        FirstName = icl.FirstName,
                                        LastName = icl.LastName,
                                        Mobile = icl.Mobile,
                                        CallStatus_ID = icl.CallStatus_ID,
                                        CurrentStatus = icl.Status,
                                        Comment = icl.Comments
                                    }).OrderBy(x => x.LastName).Skip(range).Take(100).ToList();
                    }

                    return list;
                }
            }
        }        



        public int range { get; set; }

        public List<SelectListItem> num_range
        { 
            get
            {
                var range = Enumerable.Range(0, 9).Select(x => x * 100);

                var list = new List<SelectListItem>();

                foreach (int i in range)
                {
                    list.Add(new SelectListItem { Text = i.ToString() + " - " + (i + 100).ToString(), Value = i.ToString() });
                }

                return list;
            }
        }

        public string DisplayMode { get; set; }

        public CallItem SelectedCallItem { get; set; }

        public int SelectedContactID { get; set; }

        public int SelectedCallStatus { get; set; }

        public List<SelectListItem> StatusOptions
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var select_list = (from ecs in context.Events_CallStatus
                                       select new SelectListItem
                                       {
                                           Text = ecs.Status,
                                           Value = ecs.CallStatus_ID.ToString().Trim()
                                       }).ToList();

                    return select_list;
                }
            }
        }

        public string NewComment { get; set; }*/
    }
}