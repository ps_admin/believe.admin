﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Planetshakers.Core.Models;
using System.Web.Mvc;

namespace Planetshakers.Events.Models
{

    public class CampColorAdminModel
    {
        public CampColorContact currentCampContact { get; set; }
        public List<CampColorContact> SearchedContacts { get; set; }

        public bool generateSearchedResult { get; set; }

        public string query { get; set; }

        public IEnumerable<SelectListItem> ColorSelectList
        {
            get
            {
                var colorList = new List<SelectListItem>();
                colorList.Add(new SelectListItem()
                {
                    Text = "",
                    Value = ""
                });
                colorList.Add(new SelectListItem()
                {
                    Text = "Grey",
                    Value = "Grey"
                });
                colorList.Add(new SelectListItem()
                {
                    Text = "Gold",
                    Value = "Gold"
                });
                colorList.Add(new SelectListItem()
                {
                    Text = "Purple",
                    Value = "Purple"
                });
                colorList.Add(new SelectListItem()
                {
                    Text = "Pink",
                    Value = "Pink"
                });
                colorList.Add(new SelectListItem()
                {
                    Text = "Orange",
                    Value = "Orange"
                });
                return colorList;
            }
            
        }

    }
}