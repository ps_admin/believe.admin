﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Planetshakers.Core.Models;

namespace Planetshakers.Events.Models
{
    public class ChildEnrolmentFormModel
    {
        public int Conference_ID { get; set; }
        public List<EventModel> _events
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var _events = (from ec in context.Events_Conference
                                   join ev in context.Events_Venue
                                   on ec.Conference_ID equals ev.Conference_ID
                                   orderby ev.ConferenceStartDate descending
                                   select new EventModel
                                   {
                                       ConferenceID = ec.Conference_ID,
                                       ConferenceName = ec.ConferenceName
                                   }).ToList();

                    return _events;
                }
            }
        }
        public IEnumerable<SelectListItem> EventList { get { return new SelectList(_events, "ConferenceID", "ConferenceName"); } }
        public bool generateResult { get; set;}

        //public List<ChildEnrolmentFormItem> ChildEnrolmentFormList
        //{
        //    get
        //    {
        //        List<ChildEnrolmentFormItem> newItemList = new List<ChildEnrolmentFormItem>();
        //        using (PlanetshakersEntities context = new PlanetshakersEntities())
        //        {
        //            newItemList = (from cef in context.Events_RegistrationChildEnrolment
        //                           join cc in context.Common_Contact
        //                           on cef.Contact_ID equals cc.Contact_ID
        //                           join er in context.Events_Registration
        //                           on cef.Registration_ID equals er.Registration_ID
        //                           join ert in context.Events_RegistrationType
        //                           on er.RegistrationType_ID equals ert.RegistrationType_ID
        //                           select new ChildEnrolmentFormItem
        //                           {
        //                               ChildEnrolmentForm = cef,
        //                               FirstName = cc.FirstName,
        //                               LastName = cc.LastName,
        //                               RegistrationType = ert.RegistrationType
        //                           }).ToList();
        //        }
        //        return newItemList;
        //    }
        //}

    }

    public class ChildEnrolmentFormItem
    {
        //public Events_RegistrationChildEnrolment ChildEnrolmentForm { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string RegistrationType { get; set; }
        

        //public List<Events_ChildEnrolmentQuestion> ChildQuestions
        //{
        //    get
        //    {
        //        List<Events_ChildEnrolmentQuestion> childQuestionList = new List<Events_ChildEnrolmentQuestion>();
        //        using (PlanetshakersEntities context = new PlanetshakersEntities())
        //        {
        //            childQuestionList = (from cq in context.Events_ChildEnrolmentQuestion
        //                                 select cq).ToList();
        //        }
        //        return childQuestionList;
        //    }
        //}

        //public List<Events_ChildEnrolmentAnswer> ChildAnswer
        //{
        //    get
        //    {
        //        List<Events_ChildEnrolmentAnswer> childAnswerList = new List<Events_ChildEnrolmentAnswer>();
        //        using (PlanetshakersEntities context = new PlanetshakersEntities())
        //        {
        //            childAnswerList = (from ca in context.Events_ChildEnrolmentAnswer
        //                               where ca.ChildEnrolment_ID == ChildEnrolmentForm.ChildEnrolment_ID
        //                               orderby ca.Question_ID
        //                               select ca).ToList();
        //        }
        //        return childAnswerList;
        //    }
        //}

        //public string SignedByName {
        //    get
        //    {
        //        string name = "";
        //        using (PlanetshakersEntities context = new PlanetshakersEntities())
        //        {
        //            name = (from cc in context.Common_Contact
        //                    where cc.Contact_ID == ChildEnrolmentForm.SignedBy
        //                    select cc.FirstName + " " + cc.LastName).FirstOrDefault();
        //        }
        //        if (name == null)
        //        {
        //            name = "";
        //        }
        //        return name;
        //    }
        //}

        //public DateTime? DateOfBirth {
        //    get
        //    {
        //        DateTime? DoB;
        //        using (PlanetshakersEntities context = new PlanetshakersEntities())
        //        {
        //            DoB = (from cc in context.Common_Contact
        //                   where cc.Contact_ID == ChildEnrolmentForm.Contact_ID
        //                   select cc.DateOfBirth).FirstOrDefault();
        //        }
                
        //        return DoB;
        //    }
        //}
    }

}