//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Planetshakers.Events.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Church_ContactCourseInstance
    {
        public int ContactCourseInstance_ID { get; set; }
        public int Contact_ID { get; set; }
        public int CourseInstance_ID { get; set; }
        public System.DateTime DateAdded { get; set; }
    
        public virtual Church_CourseInstance Church_CourseInstance { get; set; }
        public virtual Common_ContactInternal Common_ContactInternal { get; set; }
    }
}
