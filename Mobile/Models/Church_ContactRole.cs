//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Planetshakers.Events.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Church_ContactRole
    {
        public int ContactRole_ID { get; set; }
        public int Contact_ID { get; set; }
        public int Role_ID { get; set; }
        public int RegistrationType_ID { get; set; }
        public System.DateTime DateAdded { get; set; }
        public Nullable<bool> Inactive { get; set; }
    
        public virtual Church_Role Church_Role { get; set; }
        public virtual Events_RegistrationType Events_RegistrationType { get; set; }
        public virtual Common_Contact Common_Contact { get; set; }
    }
}
