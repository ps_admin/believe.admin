﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Planetshakers.Events.Models
{
    public class CommentModel
    {
        public int Id { get; set; }
        public int PCRContactId { get; set; }

        public string Action { get; set; }
        public string Comments { get; set; }

        public int NumOfCallWeek { get; set; }
        public int NumOfVisitWeek { get; set; }
        public int NumOfOthersWeek { get; set; }
        public bool PastoralComment { get; set; }
    }
}