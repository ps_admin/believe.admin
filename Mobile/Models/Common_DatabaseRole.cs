//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Planetshakers.Events.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Common_DatabaseRole
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Common_DatabaseRole()
        {
            this.Common_DatabaseRolePermission = new HashSet<Common_DatabaseRolePermission>();
            this.Common_ContactDatabaseRole = new HashSet<Common_ContactDatabaseRole>();
        }
    
        public int DatabaseRole_ID { get; set; }
        public string Module { get; set; }
        public string Name { get; set; }
        public int SortOrder { get; set; }
        public bool Deleted { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Common_DatabaseRolePermission> Common_DatabaseRolePermission { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Common_ContactDatabaseRole> Common_ContactDatabaseRole { get; set; }
    }
}
