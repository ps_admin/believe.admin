//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Planetshakers.Events.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Common_GeneralState
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Common_GeneralState()
        {
            this.Common_AustralianSuburb = new HashSet<Common_AustralianSuburb>();
            this.Common_ContactExternal = new HashSet<Common_ContactExternal>();
        }
    
        public int State_ID { get; set; }
        public string State { get; set; }
        public string FullName { get; set; }
        public int Country_ID { get; set; }
        public int SortOrder { get; set; }
    
        public virtual Common_GeneralCountry Common_GeneralCountry { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Common_AustralianSuburb> Common_AustralianSuburb { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Common_ContactExternal> Common_ContactExternal { get; set; }
    }
}
