﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.SqlServer;
//using System.Data.Objects.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Planetshakers.Core.Models;

namespace Planetshakers.Events.Models
{
    public class ConferenceModel
    {
        public List<ConferenceModel> ConferenceList { get; set; }
        public int Conference_ID { get; set; }
        public string ConferenceName { get; set; }
        public string ConferenceNameShort { get; set; }
        public string ConferenceDate { get; set; }
        public string ConferenceLocation { get; set; }
        public string ConferenceTagline { get; set; }
        public int ConferenceType { get; set; }
        public int Country_ID { get; set; }
        public bool AllowOnlineRegistrationSingle { get; set; }
        public bool AllowOnlineRegistrationGroup { get; set; }
        public bool ShowConferenceOnWeb { get; set; }
        public bool AllowGroupCreditApproval { get; set; }
        public bool AllowGroupOnlineBulkRegistrationPurchase { get; set; }
        public bool RequireEmergencyContactInfo { get; set; }
        public bool RequireMedicalInfo { get; set; }
        public bool IncludeULGOption { get; set; }
        public bool CrecheAvailable { get; set; }
        public bool PreOrderAvailable { get; set; }
        public bool ElectivesAvailable { get; set; }
        public bool AccommodationAvailable { get; set; }
        public bool CateringAvailable { get; set; }
        public bool LeadershipBreakfastAvailable { get; set; }
        public decimal AccommodationCost { get; set; }
        public decimal CateringCost { get; set; }
        public decimal LeadershipBreakfastCost { get; set; }
        public Nullable<decimal> PlanetKidsMinAge { get; set; }
        public Nullable<decimal> PlanetKidsMaxAge { get; set; }
        public int BankAccount_ID { get; set; }
        public int SortOrder { get; set; }
        public string LetterHead { get; set; }
        public Nullable<System.DateTime> VolunteersManagementCutOffDate { get; set; }
        public Nullable<System.DateTime> VolunteersManagerCutOffDate { get; set; }
        public Nullable<int> VolunteersManager_ID { get; set; }
        public Nullable<System.DateTime> LastDateAllocateGroupRegistrations { get; set; }

        public List<SelectListItem> event_list
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    DateTime today = DateTime.Today;

                    var list = (from ec in context.Events_Conference
                                join ev in context.Events_Venue
                                    on ec.Conference_ID equals ev.Conference_ID
                                orderby ev.ConferenceStartDate descending
                                select new SelectListItem
                                {
                                    Value = SqlFunctions.StringConvert((double)ec.Conference_ID).Trim(),
                                    Text = ec.ConferenceName
                                }).ToList();

                    return list;
                }
            }
        }

        [Display(Name = "Conference Name")]
        public string NewConference { get; set; }
        [Display(Name = "Conference Short Name")]
        public string NewConferenceNameShort { get; set; }
        [Display(Name = "Location")]
        public string NewConferenceLocation { get; set; }
        [Display(Name = "Date")]
        public string NewConferenceDate { get; set; }
        [Display(Name = "Tag Line")]
        public string NewConferenceTagline { get; set; }

    }

}