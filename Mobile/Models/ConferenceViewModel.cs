﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Planetshakers.Events.Models
{
    public class ConferenceViewModel
    {
        public string SelectedConference { get; set; }
        public int? SelectedConferenceID { get; set; }

        // Specialised datatype to store conference, venue and other stuff that will be used in view or edit
        public class SetUpConf
        {
            public Events_Conference conf { get; set; }
            public Events_Venue venue { get; set; }
            public string CountryName { get; set; }
            public string StateName { get; set; }
            public string ConferenceTypeName { get; set; }
            public string BankAccountName { get; set; }

        }

        // Conference list that was stored in database with all info needed
        public List<SetUpConf> ConferenceList {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = (from ec in context.Events_Conference
                                join ev in context.Events_Venue
                                on ec.Conference_ID equals ev.Conference_ID
                                join gc in context.Common_GeneralCountry
                                on ec.Country_ID equals gc.Country_ID
                                join ba in context.Common_BankAccount
                                on ec.BankAccount_ID equals ba.BankAccount_ID
                                join ct in context.Events_ConferenceType
                                on ec.ConferenceType equals ct.ConferenceType_ID
                                select new SetUpConf
                                {
                                    conf = ec,
                                    venue = ev,
                                    CountryName = gc.Country,
                                    ConferenceTypeName = ct.ConferenceType,
                                    BankAccountName = ba.Description
                                }).ToList();
                    return list;
                }
            }
        }
        //find the specific conference from all the conference list
        public SetUpConf selectedConfDetails { get; set; }

        public List<SelectListItem> state_list
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {

                    var list = (from gs in context.Common_GeneralState
                                select new SelectListItem
                                {
                                    Value = gs.State_ID.ToString().Trim(),
                                    Text = gs.State
                                }).ToList();
                    list.Insert(0, new SelectListItem
                    {
                        Value = "0",
                        Text = "---Select State---"
                    });

                    return list;
                }
            }
        }

        public List<SelectListItem> country_list
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {

                    var list = (from gc in context.Common_GeneralCountry
                                select new SelectListItem
                                {
                                    Value = gc.Country_ID.ToString().Trim(),
                                    Text = gc.Country
                                }).ToList();
                    list.Insert(0, new SelectListItem
                    {
                        Value = "0",
                        Text = "---Select Country---"
                    });

                    return list;
                }
            }
        }

        public List<SelectListItem> bank_list
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {

                    var list = (from ba in context.Common_BankAccount
                                where ba.PublishableKey != null
                                select new SelectListItem
                                {
                                    Value = ba.BankAccount_ID.ToString().Trim(),
                                    Text = ba.Description
                                }).ToList();
                    list.Insert(0, new SelectListItem
                    {
                        Value = "0",
                        Text = "---Select Bank Account---"
                    });

                    return list;
                }
            }
        }

        public List<SelectListItem> confType_list
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {

                    var list = (from ct in context.Events_ConferenceType
                                select new SelectListItem
                                {
                                    Value =     ct.ConferenceType_ID.ToString().Trim(),
                                    Text = ct.ConferenceType
                                }).ToList();
                    list.Insert(0, new SelectListItem
                    {
                        Value = "0",
                        Text = "---Select Trip Type---"
                    });

                    return list;
                }
            }
        }

        public List<SelectListItem> event_list
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {

                    var list = (from ec in context.Events_Conference
                                join ev in context.Events_Venue
                                    on ec.Conference_ID equals ev.Conference_ID
                                orderby ev.ConferenceStartDate descending
                                select new SelectListItem
                                {
                                    Value = ec.Conference_ID.ToString().Trim(),
                                    Text = ec.ConferenceName
                                }).ToList();

                    return list;
                }
            }
        }

        public string saveConference(int confID, string confName, string confShortName, string confDate,
            string confType, string confCountry, string confShowOnWeb, string confBankAccount, string conferenceStartDate, string conferenceEndDate,
            string conferenceStartTime, string conferenceEndTime, string venueMaxRegistrants, string conferenceVisibility, decimal minAge, decimal maxAge)
            
        {
            string s = "";
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                Events_Conference currentConf = new Events_Conference();
                

                //Editing current existed Conference 
                if (confID != 0)
                {
                    s = "Conference edited successfully.";
                    currentConf = (from ec in context.Events_Conference
                                   where ec.Conference_ID == confID
                                   select ec).FirstOrDefault();
                }
                //Adding new conference
                else
                {
                    s = "Conference added successfully.";
                    currentConf.IncludeULGOption = false;
                    currentConf.CrecheAvailable = false;
                    currentConf.PreOrderAvailable = false;
                    currentConf.ElectivesAvailable = false;
                    currentConf.AccommodationAvailable = false;
                    currentConf.CateringAvailable = false;
                    currentConf.LeadershipBreakfastAvailable = false;
                    currentConf.AccommodationCost = 0;
                    currentConf.CateringCost = 0;
                    currentConf.LeadershipBreakfastCost = 0;
                    currentConf.PlanetKidsMaxAge = (int)maxAge;
                    currentConf.PlanetKidsMinAge = (int)minAge;
                    currentConf.SortOrder = 1;
                    currentConf.LetterHead = null;
                    currentConf.VolunteersManagementCutOffDate = null;
                    currentConf.VolunteersManager_ID = null;
                    currentConf.VolunteersManagerCutOffDate = null;
                    currentConf.LastDateAllocateGroupRegistrations = null;

                }
                currentConf.ConferenceName = confName;
                currentConf.ConferenceNameShort = !String.IsNullOrEmpty(confShortName) ? confShortName : "";
                currentConf.ConferenceDate = confDate;
                currentConf.ConferenceLocation = "";
                currentConf.ConferenceTagline = "";
                currentConf.ConferenceType = Int32.Parse(confType);
                currentConf.Country_ID = Int32.Parse(confCountry);
                currentConf.ShowConferenceOnWeb = (confShowOnWeb == "true" ? true : false);
                currentConf.AllowOnlineRegistrationSingle = false;
                currentConf.AllowOnlineRegistrationGroup = true;
                currentConf.AllowGroupCreditApproval = false;
                currentConf.AllowGroupOnlineBulkRegistrationPurchase = false;
                currentConf.RequireEmergencyContactInfo = true;
                currentConf.RequireMedicalInfo = true;
                currentConf.BankAccount_ID = Int32.Parse(confBankAccount);
                currentConf.PlanetKidsMaxAge = (int)maxAge;
                currentConf.PlanetKidsMinAge = (int)minAge;
                //Adding new conference to database
                if (confID == 0)
                {
                    context.Events_Conference.Add(currentConf);
                }
                context.SaveChanges();
                Events_Venue currentVenue = new Events_Venue();
                if (confID != 0)
                {
                    int conferenceID = confID;
                    currentVenue = (from ev in context.Events_Venue
                                    where ev.Conference_ID == conferenceID
                                    select ev).FirstOrDefault();
                    currentVenue.Conference_ID = conferenceID;
                }
                //Linking latest conference to new venue
                else
                {
                    currentVenue.Conference_ID = (from cc in context.Events_Conference
                                                  select cc.Conference_ID).Max();
                }
                currentVenue.VenueName = "";
                currentVenue.VenueLocation = "";
                currentVenue.VenueDate = null;
                currentVenue.State_ID = null;
                currentVenue.ConferenceStartDate = DateTime.Parse(conferenceStartDate+" "+conferenceStartTime);
                currentVenue.ConferenceEndDate = DateTime.Parse(conferenceEndDate+" "+conferenceEndTime);
                currentVenue.MaxRegistrants = Int32.Parse(venueMaxRegistrants);
                currentVenue.AllowMultipleRegistrations = false;
                currentConf.Visibility = (conferenceVisibility == "true" ? true : false);
                if (confID == 0)
                {
                    context.Events_Venue.Add(currentVenue);
                }
                context.SaveChanges();
            }

            return s;
        }

        //public bool deleteConference(string confID)
        //{
        //    int conf_ID = Int32.Parse(confID);
        //    using (PlanetshakersEntities context = new PlanetshakersEntities())
        //    {
        //        bool hasConferenceDepartmentMapping = (from cdm in context.Events_ConferenceDepartmentMapping
        //                                               where cdm.Conference_ID == conf_ID
        //                                               select cdm).Any();
        //        bool hasContactPayment = (from cp in context.Events_ContactPayment
        //                                  where cp.Conference_ID == conf_ID
        //                                  select cp).Any();
        //        bool hasGroupPayment = (from gp in context.Events_GroupPayment
        //                                where gp.Conference_ID == conf_ID
        //                                select gp).Any();
        //        bool hasRegoType = (from rt in context.Events_RegistrationType
        //                            where rt.Conference_ID == conf_ID
        //                            select rt).Any();
        //        bool hasCreChildCost = (from cc in context.Events_CrecheChildCost
        //                                where cc.Conference_ID == conf_ID
        //                                select cc).Any();
        //        bool hasCreChildRego = (from ccr in context.Events_CrecheChildRegistrationType
        //                                where ccr.Conference_ID == conf_ID
        //                                select ccr).Any();
        //        List<Events_Venue> venueList = (from ev in context.Events_Venue
        //                                        where ev.Conference_ID == conf_ID
        //                                        select ev).ToList();
        //        bool venueUsed = false;
        //        foreach (Events_Venue ev in venueList)
        //        {
        //            bool hasGroupBulkPurchase = (from gbp in context.Events_GroupBulkPurchase
        //                           where gbp.Venue_ID == ev.Venue_ID
        //                           select gbp).Any();
        //            bool hasGroupBulkRego = (from gbr in context.Events_GroupBulkRegistration
        //                           where gbr.Venue_ID == ev.Venue_ID
        //                           select gbr).Any();
        //            bool hasGroupHistory = (from gh in context.Events_GroupHistory
        //                                    where gh.Venue_ID == ev.Venue_ID || gh.Conference_ID == conf_ID
        //                                    select gh).Any();
        //            bool hasEventRego = (from eg in context.Events_Registration
        //                                 where eg.Venue_ID == ev.Venue_ID
        //                                 select eg).Any();


        //            venueUsed = venueUsed || hasGroupBulkPurchase || hasGroupBulkRego || hasGroupHistory || hasEventRego;
        //        }
        //        bool hasLinkedData = hasConferenceDepartmentMapping || hasContactPayment || hasGroupPayment || hasRegoType || hasCreChildCost || hasCreChildRego || venueUsed;
        //        if (!hasLinkedData)
        //        {
        //            Events_Conference conference = (from ec in context.Events_Conference
        //                                            where ec.Conference_ID == conf_ID
        //                                            select ec).FirstOrDefault();
        //            context.Events_Conference.Remove(conference);
        //            foreach (Events_Venue ev in venueList)
        //            {
        //                context.Events_Venue.Remove(ev);
        //            }
        //            context.SaveChanges();
        //        }
        //        return !hasLinkedData;
        //    }
        //}

    }

}