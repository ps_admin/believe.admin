﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Planetshakers.Core.Models;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace Planetshakers.Events.Models {

    #region Models

    public class ContactUpdateModel {

        public string SuccessMessage { get; set; }

        public string ErrorMessage { get; set; }

        public int ContactId { get; set; }

        public string Avatar { get; set; }

        [Display(Name = "First Name")]
        public string Firstname { get; set; }

        [Display(Name = "Last Name")]
        public string Lastname { get; set; }

        [Display(Name = "Date of Birth")]
        public string Dob { get; set; }

        [Display(Name = "Address")]
        public string Address1 { get; set; }

        public string Address2 { get; set; }

        [Required]
        [Display(Name = "Country")]
        public int Country { get; set; }

        public SelectList CountryList { get; set; }

        [Required]
        [Display(Name = "Suburb / City")]
        public string Suburb { get; set; }

        [Required]
        [Display(Name = "State")]
        public int State { get; set; }

        public SelectList StateList { get; set; }

        [Display(Name = "State Other")]
        public string StateOther { get; set; }

        [Required]
        [Display(Name = "Zip / Postcode")]
        public string Postcode { get; set; }

        [Required]
        [Display(Name = "Mobile")]
        public string Mobile { get; set; }

        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [StringLength(100, ErrorMessage = "Password must be a minimum of 8 characters long", MinimumLength = 8)]
        [Display(Name = "New Password")]
        public string Password { get; set; }

        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "Passwords do not match")]
        [Display(Name = "Confirm Password")]
        public string ConfirmPassword { get; set; }

        public List<ContactMailingList> MailingList { get; set; }

        public SelectList MailingListList { get; set; }
    }

    public class VolunteerRegistrationConfirmationItem
    {
        // registration name
        public string Name { get; set; }

        public string Email { get; set; }

        public byte[] VolunteerForm { get; set; }


    }

    public class PaymentConfirmationItem
    {
        ////public string TransRefCode { get; set; }

        ////public string CourseName { get; set; }

        ////public int Contact_id { get; set; }

        ////public decimal Cost { get; set; }

        public string Name { get; set; }

        ////public int Quantity { get; set; }

        public string Email { get; set; }

        public byte[] PaymentForm { get; set; }


    }

    // paste this inside #region Objects
    public class Question
    {

        // Question ID
        public int question_ID { get; set; }

        // Question ID
        public int sortOrder { get; set; }

        // The question
        public string question { get; set; }

        // Question type
        public string type { get; set; }

        // Required
        public bool required { get; set; }

        // The question
        public int answer_ID { get; set; }

        // Required
        public string answer { get; set; }

    }

    public class CovenantModel
    {
        public int Contact_Id { get; set; }

        public string Role { get; set; }

        public string FullName { get; set; }

        [Display(Name = "Code of Conduct")]
        [Range(typeof(bool), "true", "true", ErrorMessage = "Please tick the above boxes.")]
        public Boolean CodeOfConduct { get; set; }

        [Display(Name = "OH&S Policy")]
        [Range(typeof(bool), "true", "true", ErrorMessage = "Please tick the above boxes.")]
        public Boolean OHSPolicy { get; set; }

        [Display(Name = "PlanetBoom Training")]
        [Range(typeof(bool), "true", "true", ErrorMessage = "Please tick the above boxes.")]
        public Boolean PBTraining { get; set; }

        public DateTime SignedDate { get; set; }
    }

    public class VoucherEmailTemplate { 

        public int Contact_ID { get; set; }

        public string Email { get; set; }

        [Display(Name = "FullName")]
        public string FullName { get; set; }

        [Display(Name = "Currency")]
        public string Currency { get; set; }

        [Display(Name = "TotalAmount")]
        public string TotalAmount { get; set; }

        [Display(Name = "VoucherCode")]
        public string VoucherCode { get; set; }
    }

    public class ConfirmationBarcode
    {
        public int Contact_ID { get; set; }

        public string Email { get; set; }

        [JsonProperty("Subject")]
        public string Subject { get; set; }

        [JsonProperty("EventName")]
        public string EventName { get; set; }

        [JsonProperty("EventDate")]
        public string EventDate { get; set; }

        [JsonProperty("EventVenue")]
        public string EventVenue { get; set; }

        [JsonProperty("VenueLocation")]
        public string VenueLocation { get; set; }

        [JsonProperty("RegistrationNumber")]
        public string RegistrationNumber { get; set; }

        [JsonProperty("ContactDetails")]
        public string ContactDetails { get; set; }
    }

    #endregion

    #region Objects

    /*    public class Course {

            public int Course_ID { get; set; }

            public int CourseInstance_ID { get; set; }

            public string Name { get; set; }

            public string StartDate { get; set; }

            public string Campus { get; set; }

            public bool Completed { get; set; }

            public string scost { get; set; }

            public decimal cost { get; set; }
        }*/

    public class Salutation {

        // salutation id
        public int id { get; set; }

        // salutation name
        public string name { get; set; }
    }

    public class Country {

        // country id
        public int id { get; set; }

        // name
        public string name { get; set; }

        // currency
        public string currency { get; set; }
    }

    public class State {

        // state id
        public int id { get; set; }

        // id of the country this state belongs to
        public int country_id { get; set; }

        // name
        public string name { get; set; }
    }

    public class MailingList {

        public int ML_ID { get; set; }

        public string ML_Name { get; set; }
    }

  /*  public class Question {
    
        // Question ID
        public int question_ID { get; set; }

        // The question
        public string question { get; set; }

        // Question type
        public string type { get; set; }

        // Required
        public bool required { get; set; }

    }*/

    #endregion
    
}