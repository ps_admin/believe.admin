﻿using System.Linq;

namespace Planetshakers.Events.Models
{
    public class ContactProfileModel
    {
        public int ContactProfile_ID { get; set; }
        public int Contact_ID { get; set; }
        public int Event_ID { get; set; }
        public string AboutMyself { get; set; }
        public string TripDetails { get; set; }
        public string PhotoURL { get; set; }
        public string GivingURL { get; set; }
        public string FullName { get; set; }
        public string Eventname { get; set; }
        public void populate()
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var profile = (from c in context.Events_ContactProfile
                               where c.ContactProfile_ID == this.ContactProfile_ID
                               select c).FirstOrDefault();
                if (profile != null)
                {
                    this.Contact_ID = profile.Contact_ID;
                    this.Event_ID = profile.Event_ID;
                    FileModel fileModel = new FileModel("s3-amazon-believe-application-documents");
                    var photoModel = fileModel.GetFile(Contact_ID.ToString(), profile.PictureURL);

                    this.PhotoURL = photoModel.DownloadUrl;
                    this.AboutMyself = profile.AboutMyself;
                    this.TripDetails = profile.TripDetail;

                    this.Eventname = (from e in context.Events_Conference
                                      where e.Conference_ID == this.Event_ID
                                      select e.ConferenceName).FirstOrDefault();

                    var contact = (from cc in context.Common_Contact
                                   where cc.Contact_ID == this.Contact_ID
                                   select cc).FirstOrDefault();
                    this.GivingURL = "missions.believeglobal.org/Sponsorship/Donation?Event_ID=" + Event_ID + "&GUID=" + contact.GUID;
                    this.FullName = contact.FirstName + " " + contact.LastName;
                }


            }
        }

        public bool updateContactProfile()
        {
            try
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var contactProfile = (from c in context.Events_ContactProfile
                                          where c.ContactProfile_ID == this.ContactProfile_ID
                                          select c).FirstOrDefault();
                    contactProfile.AboutMyself = this.AboutMyself;
                    contactProfile.TripDetail = this.TripDetails;
                    context.SaveChanges();
                }
            }
            catch
            {
                return false;
            }
            return true;
        }

        public bool deletePhoto()
        {
            try
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var file = (from c in context.Events_ContactProfile
                                where c.ContactProfile_ID == this.ContactProfile_ID
                                select c).FirstOrDefault();

                    FileModel fileModel = new FileModel("s3-amazon-believe-application-documents");
                    fileModel.DeleteFile(file.Contact_ID + "/" + file.PictureURL);
                    return true;
                }
            } catch
            {
                return false;
            }
        }
    }
    
}