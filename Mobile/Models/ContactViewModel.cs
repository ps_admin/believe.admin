﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Planetshakers.Core.Models;

namespace Planetshakers.Events.Models
{
    public class ContactViewModel
    {
        public ContactViewModel()
        {
            Contact = new ContactDetails();
            Conference = new Conference();
            Registration = new Registration();
            Registrations = new List<Registration>();
            Payment = new Payment();
            CommentList = new List<CommentItem>();
            OutstandingBalance = new OutstandingBalance();
            PaymentList = new List<Payment>();
            History = new List<ContactHistory>();
        }

        public ContactViewModel(int contact_id, int conference_id)
        {
            Contact = new ContactDetails(contact_id);
            Conference = new Conference(conference_id);
            Registration = new Registration();
            Registrations = new List<Registration>();
            Payment = new Payment();
            CommentList = new List<CommentItem>();
            OutstandingBalance = new OutstandingBalance();
            PaymentList = new List<Payment>();
            History = new List<ContactHistory>();
        }

        public ContactDetails Contact { get; set; }
        public Conference Conference { get; set; }
        public Registration Registration { get; set; }
        public List<Registration> Registrations { get; set; }
        public Payment Payment { get; set; }

        public string NewComment { get; set; }

        public List<CommentItem> CommentList { get; set; }

        public OutstandingBalance OutstandingBalance { get; set; }

        public List<Payment> PaymentList { get; set; }

        public List<SponsorshipItem> SponsorshipList 
        { 
            get
            {
                var list = new List<SponsorshipItem>();

                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    list = (from vp in context.Events_VariablePayment
                            join pt in context.Common_PaymentType
                            on vp.PaymentType_ID equals pt.PaymentType_ID
                            where vp.Conference_ID == Conference.ID 
                            && (vp.IncomingContact_ID == Contact.Id || vp.OutgoingContact_ID == Contact.Id)
                            select new SponsorshipItem
                            {
                                Donor = (vp.IncomingContact_ID == Contact.Id) ? true : false,
                                Recipient = (vp.OutgoingContact_ID == Contact.Id) ? true : false,
                                PaymentType = pt.PaymentType,
                                PaymentAmount = (decimal)vp.PaymentAmount,
                                PaymentDate = vp.PaymentDate
                            }).ToList();
                }

                return list;
            }
        }

        public List<ContactHistory> History { get; set; }

        private List<Events_Conference> _Conference_List
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var lst = (from ec in context.Events_Conference
                               join ev in context.Events_Venue
                               on ec.Conference_ID equals ev.Conference_ID
                               orderby ev.ConferenceStartDate descending
                               select ec).ToList();

                    return lst;
                }
            }
        }

        public SelectList Conference_List
        {
            get
            {
                return new SelectList(_Conference_List, "Conference_ID", "ConferenceName");
            }
        }

        private List<Events_RegistrationType> _RegistrationType_List
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var lst = (from ert in context.Events_RegistrationType
                               where ert.Conference_ID == Conference.ID
                               orderby ert.SortOrder
                               select ert).ToList();

                    return lst;
                }
            }
        }

        public SelectList RegistrationType_List
        {
            get
            {
                return new SelectList(_RegistrationType_List, "RegistrationType_ID", "RegistrationType");
            }
        }

        private List<Events_Group> _EventGroup_List
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var lst = (from eg in context.Events_Group
                               where eg.GroupName != "" && eg.Deleted == false
                               orderby eg.GroupName
                               select eg).ToList();

                    return lst;
                }
            }
        }

        public SelectList EventGroup_List
        {
            get
            {
                return new SelectList(_EventGroup_List, "GroupLeader_ID", "GroupName");
            }
        }


        public List<Common_PaymentType> _PaymentType_List
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var lst = (from pt in context.Common_PaymentType
                               select pt).ToList();

                    //For by passing credit card payment use
                    lst.Insert(2, new Common_PaymentType { PaymentType_ID = 3, PaymentType = "Credit Card - STRIPE" });
                    lst.Insert(3, new Common_PaymentType { PaymentType_ID = 10000, PaymentType = "Credit Card - ADMIN" }); //Chosen a big number so there is no chance for payment type to be that id
                    lst.Remove(lst.Find(x => x.PaymentType == "Credit Card"));

                    return lst;
                }
            }
        }

        public SelectList PaymentType_List
        {
            get
            {
                return new SelectList(_PaymentType_List, "PaymentType_ID", "PaymentType");
            }
        }

        public List<SelectListItem> PaymentCategory_List
        {
            get
            {
                var lst = new List<SelectListItem>
                {
                    new SelectListItem { Text = "Payment", Value = "Payment" },
                    new SelectListItem { Text = "Refund", Value = "Refund" }
                };

                return lst;
            }
        }

        public bool hasGroup
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var hasgroup = (from eg in context.Events_Group
                               where eg.GroupLeader_ID == Contact.Id
                               select eg).Any();

                    return hasgroup;
                }

            }
        }
    }
}