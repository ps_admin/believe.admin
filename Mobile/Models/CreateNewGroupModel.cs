﻿using Planetshakers.Core.Models;

namespace Planetshakers.Events.Models
{
    public class CreateNewGroupModel
    {
        public Group group { get; set; }

        public bool success { get; set; }
        public string message { get; set; }

        public int Conference_ID { get; set; }
        public NewRegistrantModel NewPeopleModel { get; set; }
    }
}