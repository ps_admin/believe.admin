﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Planetshakers.Core.Models;

namespace Planetshakers.Events.Models
{
    public class DataIntegrityItem
    {
        public int Contact_ID { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public bool isLeader { get; set; }
    }
}
