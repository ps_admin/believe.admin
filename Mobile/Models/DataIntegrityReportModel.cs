﻿using Planetshakers.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Planetshakers.Events.Models
{
    public class DataIntegrityReportModel
    {
        public Boolean displayReport = false;

        public List<DataIntegrityItem> AllocationList { get; set; }

        public List<DataIntegrityItem> PurchaseList { get; set; }

        public List<SelectListItem> availableSelection
        {
            get
            {
                List<SelectListItem> newlist = new List<SelectListItem>();
                newlist.Add(new SelectListItem() { Text = "Melbourne Conference 2022 and Beautiful Conference 2021", Value = "1" }); 
                
                return newlist;
            }
        }

        public int Selection_ID { get; set; }
    }
}