﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Planetshakers.Core.Models;

namespace Planetshakers.Events.Models
{
    public class DatabaseRoleManagementModel
    {
        public string AddedDbRoleName { get; set; }
        public bool Read { get; set; } 
        public bool Write { get; set; }
        public string searchQuery { get; set; }
        public bool GenerateResult { get; set; }
        public int selectedContactID { get; set; }
        public List<Common_DatabaseRole> DatabaseRoleList
        {
            get
            {
                List<Common_DatabaseRole> rolelist = new List<Common_DatabaseRole>();
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    rolelist = (from dr in context.Common_DatabaseRole
                                where dr.Module == "Believe"
                                select dr).ToList();
                }
                return rolelist;
            }
        }

        //Used to get DbPermission List given Role ID
        public List<DatabaseRolePermission> EditRolePermissionLists(int DatabaseRoleID)
        {
            List<DatabaseRolePermission> rolepermissionlist = new List<DatabaseRolePermission>();
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                rolepermissionlist = (from drp in context.Common_DatabaseRolePermission
                            join dr in context.Common_DatabaseRole
                            on drp.DatabaseRole_ID equals dr.DatabaseRole_ID
                            join dor in context.Common_DatabaseObject
                            on drp.DatabaseObject_ID equals dor.DatabaseObject_ID
                            where dr.Module == "Believe" && dor.Module == "Believe" && dr.DatabaseRole_ID == DatabaseRoleID
                            orderby dor.Order ascending
                            select new DatabaseRolePermission
                            {
                                DatabaseRolePermissionID = drp.DatabaseRolePermission_ID,
                                DatabaseObjectID = dor.DatabaseObject_ID,
                                DatabaseRoleID = dr.DatabaseRole_ID,
                                RoleName = dr.Name,
                                DatabaseObjectDescription = dor.DatabaseObjectDesc,
                                DatabaseObjectName = dor.DatabaseObjectName,
                                Read = drp.Read,
                                Write = drp.Write
                            }).ToList();
            }
            return rolepermissionlist;
        }

        //Changing info from DbObject and also DbPermission, both operation must be successful in order to save changes.
        public bool SaveDbRolePermission(int DbRolePermissionID, string DbObjectName, string DbObjectDesc, bool DbRolePermRead, bool DbRolePermWrite)
        {
            bool saveDbObjSuccess = false;
            bool saveDbPermSuccess = false;
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                Common_DatabaseRolePermission roleperm = (from drp in context.Common_DatabaseRolePermission
                                                          where drp.DatabaseRolePermission_ID == DbRolePermissionID
                                                          select drp).FirstOrDefault();

                Common_DatabaseObject roleobj = (from drp in context.Common_DatabaseRolePermission
                                                 join dor in context.Common_DatabaseObject
                                                 on drp.DatabaseObject_ID equals dor.DatabaseObject_ID
                                                 where drp.DatabaseRolePermission_ID == DbRolePermissionID
                                                 select dor).FirstOrDefault();
                if (roleobj != null)
                {
                    roleobj.DatabaseObjectName = DbObjectName;
                    roleobj.DatabaseObjectDesc = DbObjectDesc;
                    context.SaveChanges();
                    saveDbObjSuccess = true;
                }
                if (roleperm != null)
                {
                    roleperm.Read = DbRolePermRead;
                    roleperm.Write = DbRolePermWrite;
                    context.SaveChanges();
                    saveDbPermSuccess = true;
                }
            }
            return (saveDbObjSuccess && saveDbPermSuccess);
        }

        public bool DeleteDbRole(int DbRoleID)
        {
            bool success = false;
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {

                Common_DatabaseRole pendingDeleted = (from dr in context.Common_DatabaseRole
                                                        where dr.DatabaseRole_ID == DbRoleID
                                                        select dr).FirstOrDefault();

                List<Common_DatabaseRolePermission> rolePerm = (from drp in context.Common_DatabaseRolePermission
                                                                where drp.DatabaseRole_ID == DbRoleID
                                                                select drp).ToList();

                List<Common_ContactDatabaseRole> ContactAccessLevel = (from cdr in context.Common_ContactDatabaseRole
                                                                  where cdr.DatabaseRole_ID == DbRoleID
                                                                  select cdr).ToList();
                try
                {
                    foreach (Common_DatabaseRolePermission rp in rolePerm)
                    {
                        context.Common_DatabaseRolePermission.Remove(rp);
                    }
                    foreach (Common_ContactDatabaseRole cdr in ContactAccessLevel)
                    {
                        context.Common_ContactDatabaseRole.Remove(cdr);
                    }
                    context.SaveChanges();

                    context.Common_DatabaseRole.Remove(pendingDeleted);
                    context.SaveChanges();
                    success = true;
                }
                catch (Exception ex)
                {
                    success = false;
                    throw ex;
                }

            }
            return success;
        }
        //=============================================================Following section is used to add new database role==============================================
        public List<Common_DatabaseObject> DbObjectList
        { get
            {
                List<Common_DatabaseObject> DbObj = new List<Common_DatabaseObject>();
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    DbObj = (from dbo in context.Common_DatabaseObject
                             where dbo.Module == "Believe"
                             orderby dbo.Order ascending
                             select dbo).ToList();
                }
                return DbObj;
            }
        }

        public bool AddDbRole(string DbRoleName)
        {
            bool success = false;
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                try
                {
                    Common_DatabaseRole newDbRole = new Common_DatabaseRole();
                    newDbRole.Name = DbRoleName;
                    int latestSortOrder = (from dr in context.Common_DatabaseRole
                                           where dr.Module == "Believe"
                                           orderby dr.SortOrder descending
                                           select dr.SortOrder).FirstOrDefault();
                    newDbRole.SortOrder = latestSortOrder + 1;
                    newDbRole.Module = "Believe";
                    newDbRole.Deleted = false;
                    context.Common_DatabaseRole.Add(newDbRole);
                    context.SaveChanges();
                    success = true;
                }
                catch (Exception ex)
                {
                    success = false;
                    throw ex;
                }

            }
            return success;
        }

        public bool SaveNewDbRolePermission(int DbObjID, bool DbRolePermRead, bool DbRolePermWrite)
        {
            bool success = true;
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                try
                {
                    int newDbRoleId = (from dr in context.Common_DatabaseRole
                                       where dr.Module == "Believe"
                                       orderby dr.DatabaseRole_ID descending
                                       select dr.DatabaseRole_ID).FirstOrDefault();
                    Common_DatabaseRolePermission newPerm = new Common_DatabaseRolePermission();
                    newPerm.DatabaseRole_ID = newDbRoleId;
                    newPerm.DatabaseObject_ID = DbObjID;
                    newPerm.Read = DbRolePermRead;
                    newPerm.Write = DbRolePermWrite;
                    newPerm.ViewState = false;
                    newPerm.ViewCountry = false;
                    context.Common_DatabaseRolePermission.Add(newPerm);
                    context.SaveChanges();
                }
                catch(Exception ex)
                {
                    success = false;
                    throw ex;
                }
            }
            return success;
        }

        //=========================================================================Following section is used to do user Db role management=================================
        public List<Common_Contact> searchedResultContactList
        {
            get
            {
                List<Common_Contact> resultList = new List<Common_Contact>();
                resultList = SearchContact(searchQuery);
                return resultList;
            }
        }

        //This search function only used to search for internal contact
        public List<Common_Contact> SearchContact(string query)
        {
            List<Common_Contact> ccList = new List<Common_Contact>();
            if (query != null)
            {
                try
                {
                    using (PlanetshakersEntities context = new PlanetshakersEntities())
                    {

                        // Search for names with spaces
                        if (query.Contains(" "))
                        {
                            var substrings = query.ToLower().Split();

                            var list = (from cc in context.Common_Contact
                                        join cci in context.Common_ContactInternal
                                        on cc.Contact_ID equals cci.Contact_ID
                                        where (cc.FirstName.ToLower() + " " + cc.LastName.ToLower() == query.ToLower() || cc.LastName.ToLower() + " " + cc.FirstName.ToLower() == query.ToLower())
                                        select cc).ToList();
                            var list2 = list.Where(p => !ccList.Any(p2 => p2.Contact_ID == p.Contact_ID));
                            ccList.AddRange(list2);

                            if (substrings.Count() > 1)
                            {
                                //Condition where the last substring is start of FirstName and the rest is start of Lastname,
                                //or last substring is the start of LastName and the rest the start of FirstName
                                List<string> stringlist = new List<string>(substrings);
                                var lastSubstring = stringlist.Last();
                                stringlist.RemoveAt(stringlist.Count() - 1);
                                var restSubstring = "";
                                for (int i = 0; i < stringlist.Count(); i++)
                                {
                                    restSubstring += stringlist[i];
                                    if (i != stringlist.Count() - 1)
                                    {
                                        restSubstring += " ";
                                    }
                                }
                                list = (from cc in context.Common_Contact
                                        join cci in context.Common_ContactInternal
                                        on cc.Contact_ID equals cci.Contact_ID
                                        where ((cc.FirstName.ToLower().StartsWith(restSubstring.ToLower()) && cc.LastName.ToLower().StartsWith(lastSubstring.ToLower()))
                                        || (cc.FirstName.ToLower().StartsWith(lastSubstring.ToLower()) && cc.LastName.ToLower().StartsWith(restSubstring.ToLower())))
                                        select cc).ToList();

                                list2 = list.Where(p => !ccList.Any(p2 => p2.Contact_ID == p.Contact_ID));

                                ccList.AddRange(list2);

                                //Condition where the first substring is start of FirstName and the rest is start of LastName,
                                //or first substring is the start of FirstName and the rest the start of LastName
                                List<string> stringlist2 = new List<string>(substrings);
                                var firstSubstring = stringlist2.First();
                                stringlist2.RemoveAt(0);
                                restSubstring = "";
                                for (int i = 0; i < stringlist.Count(); i++)
                                {
                                    restSubstring += stringlist[i];
                                    if (i != stringlist.Count() - 1)
                                    {
                                        restSubstring += " ";
                                    }
                                }
                                list = (from cc in context.Common_Contact
                                        join cci in context.Common_ContactInternal
                                        on cc.Contact_ID equals cci.Contact_ID
                                        where ((cc.FirstName.ToLower().StartsWith(restSubstring.ToLower()) && cc.LastName.ToLower().StartsWith(firstSubstring.ToLower()))
                                        || (cc.FirstName.ToLower().StartsWith(firstSubstring.ToLower()) && cc.LastName.ToLower().StartsWith(restSubstring.ToLower())))
                                        select cc).ToList();

                                list2 = list.Where(p => !ccList.Any(p2 => p2.Contact_ID == p.Contact_ID));

                                ccList.AddRange(list2);

                            }
                            var fullstring = "";
                            foreach (var s in substrings)
                            {
                                fullstring += s;
                            }
                            list = (from cc in context.Common_Contact
                                        join cci in context.Common_ContactInternal
                                        on cc.Contact_ID equals cci.Contact_ID
                                        where (cc.Email.Contains(fullstring) || cc.Email2.Contains(fullstring))
                                        select cc).ToList();

                            list2 = list.Where(p => !ccList.Any(p2 => p2.Contact_ID == p.Contact_ID));

                            ccList.AddRange(list2);

                        }
                        else if (query.Contains("@") && !query.Contains(" "))
                        {
                            var list = (from cc in context.Common_Contact
                                        join cci in context.Common_ContactInternal
                                        on cc.Contact_ID equals cci.Contact_ID
                                        where (cc.Email == query || cc.Email2 == query)
                                        select cc).ToList();

                            ccList.AddRange(list);

                            // Search for single names
                        }
                        else if (!query.Contains(" "))
                        {
                            if (query.All(char.IsDigit)) //Search for mobile number
                            {
                                var list = (from cc in context.Common_Contact
                                            //join cci in context.Common_ContactInternal
                                            //on cc.Contact_ID equals cci.Contact_ID
                                            where (cc.Mobile.Contains(query))
                                            select cc).ToList();
                            }
                            else
                            {
                                //Search for first name first, then see if there is any matching for last name or email
                                var list = (from cc in context.Common_Contact
                                            //join cci in context.Common_ContactInternal
                                            //on cc.Contact_ID equals cci.Contact_ID
                                            where cc.FirstName.ToLower().StartsWith(query.ToLower()) 
                                            select cc).ToList();

                                var list2 = list.Where(p => !ccList.Any(p2 => p2.Contact_ID == p.Contact_ID));

                                ccList.AddRange(list2);

                                list = (from cc in context.Common_Contact
                                        //join cci in context.Common_ContactInternal
                                        //on cc.Contact_ID equals cci.Contact_ID
                                        where cc.LastName.ToLower().StartsWith(query.ToLower()) || cc.Email.Contains(query)
                                        select cc).ToList();

                                list2 = list.Where(p => !ccList.Any(p2 => p2.Contact_ID == p.Contact_ID));

                                ccList.AddRange(list2);
                            }
                            
                        }
                        

                        return ccList;
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            else
            {
                return ccList;
            }
        } //Searching function to help with search contact

        public Dictionary<int, List<AccessLevelPermission>> userDbPermDictionary { get; set; }

        public Dictionary<int, List<ContactAccessLevel>> userDbRoleDictionary { get; set; }

        public bool UpdateAccessLevelReadPermission(int DbObjID, int ContactID, bool Read)
        {
            bool success = false;
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                Common_ContactRolePermission contactRp = (from crp in context.Common_ContactRolePermission
                                                    where crp.DatabaseObject_ID == DbObjID && crp.Contact_ID == ContactID
                                                    select crp).FirstOrDefault();
                if (contactRp != null)
                {
                    contactRp.Read = Read;
                    try
                    {
                        context.SaveChanges();
                        success = true;
                    }
                    catch (Exception ex)
                    {
                        success = false;
                        throw ex;
                    }
                }
                else
                {
                    success = false;
                }
            }
            return success;
        }

        public bool UpdateAccessLevelWritePermission(int DbObjID, int ContactID, bool Write)
        {
            bool success = false;
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                Common_ContactRolePermission contactRp = (from crp in context.Common_ContactRolePermission
                                                          where crp.DatabaseObject_ID == DbObjID && crp.Contact_ID == ContactID
                                                          select crp).FirstOrDefault();
                if (contactRp != null)
                {
                    contactRp.Write = Write;
                    try
                    {
                        context.SaveChanges();
                        success = true;
                    }
                    catch (Exception ex)
                    {
                        success = false;
                        throw ex;
                    }
                }
                else
                {
                    success = false;
                }
            }
            return success;
        }

        //THis function finding the existing databse object from database, and check if user alrd have the customed database role.
        //Only return available selection of Database object for contact role permission to be added
        public List<AccessLevelPermission> AvailableContactAccessLevelPerm(int Contact_ID)
        {
            List<AccessLevelPermission> existedDbRolePerm = userDbPermDictionary[Contact_ID];
            List<AccessLevelPermission> finalResult = new List<AccessLevelPermission>();

            foreach (var obj in existedDbRolePerm)
            {
                if (!obj.CustomRead && !obj.CustomWrite)
                {
                    finalResult.Add(obj);
                }
            }
            return finalResult;
        }

        //Getting all available role to be added for user. This will eliminate the role that user already have.
        public IEnumerable<SelectListItem> AvailableContactAccessLevelAdd(int Contact_ID)
        {
            var currentUserDbRoleID = userDbRoleDictionary[Contact_ID].Select(x => x.DbRoleId).ToList();
            List<ContactAccessLevel> allUserDbRole = new List<ContactAccessLevel>();
            var finalResult = new List<SelectListItem>();
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                allUserDbRole = (from cdr in context.Common_DatabaseRole
                                 where cdr.Module == "Believe" && cdr.Deleted == false
                                 select new ContactAccessLevel
                                 {
                                     DbRoleId = cdr.DatabaseRole_ID,
                                     Name = cdr.Name
                                 }).ToList();
            }

            foreach (var role in allUserDbRole)
            {
                if (!currentUserDbRoleID.Exists(x => x == role.DbRoleId))
                {
                    finalResult.Add(new SelectListItem
                    {
                        Text = role.Name,
                        Value = role.DbRoleId.ToString()
                    });
                }
            }
            return finalResult;
        }

        public bool DeleteContactAccessLevel(int DbRoleID, int ContactID)
        {
            bool success = false;

            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {

                Common_ContactDatabaseRole ContactAccessLevel = (from cdr in context.Common_ContactDatabaseRole
                                                            where cdr.Contact_ID == ContactID && cdr.DatabaseRole_ID == DbRoleID
                                                            select cdr).FirstOrDefault();
                try
                {
                    if (ContactAccessLevel != null)
                    {
                        context.Common_ContactDatabaseRole.Remove(ContactAccessLevel);
                        context.SaveChanges();
                        success = true;
                    }
                    
                }
                catch (Exception ex)
                {
                    success = false;
                    throw ex;
                }
            }
            return success;
        }

        public bool AddContactAccessLevel(int DbRoleID, int ContactID)
        {
            bool success = false;

            //add internal contact and user detail if doesnt exist
            //for access to backend during login
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var existingRole = (from cr in context.Common_ContactDatabaseRole
                                    where cr.Contact_ID == ContactID
                                    select cr).Any();
                if (!existingRole)
                {
                    //create internal contact and user details 
                    var internalContact = (from ci in context.Common_ContactInternal
                                           where ci.Contact_ID == ContactID
                                           select ci).Any();
                    var userDetail = (from u in context.Common_UserDetail
                                      where u.Contact_ID == ContactID
                                      select u).Any();
                    if(!internalContact)
                    {
                        var newInternal = new Common_ContactInternal()
                        {
                            Contact_ID = ContactID,
                            Barcode = "a1" + ContactID + "5",
                            ChurchStatus_ID = 1,
                            Campus_ID = null,
                            Ministry_ID = null,
                            Region_ID = null,
                            PostalAddress1 = "",
                            PostalAddress2 = "",
                            PostalSuburb = "",
                            PostalPostcode = "",
                            PostalState_ID = null,
                            PostalStateOther = "",
                            PostalCountry_ID = null,
                            Photo = null,
                            EntryPoint_ID = null,
                            DateJoinedChurch = null,
                            CovenantFormDate = null,
                            SalvationDate = null,
                            WaterBaptismDate = null,
                            VolunteerFormDate = null,
                            PrimaryCarer = "",
                            CountryOfOrigin_ID = null,
                            PrivacyNumber = "",
                            PrimarySchool = "",
                            SchoolGrade_ID = null,
                            HighSchool = "",
                            University = "",
                            InformationIsConfidential = false,
                            FacebookAddress = "",
                            Education_ID = null,
                            ModeOfTransport_ID = null,
                            CarParkUsed_ID = null,
                            ServiceAttending_ID = null,
                            Occupation = "",
                            Employer = "",
                            EmploymentStatus_ID = null,
                            EmploymentIndustry_ID = null,
                            EmploymentPosition_ID = null,
                            KidsMinistryComments = "",
                            KidsMinistryGroup = "",
                            Deleted = false,
                            DeleteReason_ID = null,
                            BaptismInHolySpirit = null
                        };
                        context.Common_ContactInternal.Add(newInternal);
                    }
                    
                    if(!userDetail)
                    {
                        var newUserDetail = new Common_UserDetail()
                        {
                            Contact_ID = ContactID,
                            LastPasswordChangeDate = null,
                            InvalidPasswordAttempts = 0,
                            PasswordResetRequired = false,
                            Inactive = false
                        };
                        context.Common_UserDetail.Add(newUserDetail);
                    }
                    context.SaveChanges();
                } 


                Common_ContactDatabaseRole newCdr = new Common_ContactDatabaseRole();
                try
                {
                    newCdr.Contact_ID = ContactID;
                    newCdr.DatabaseRole_ID = DbRoleID;
                    newCdr.DateAdded = DateTime.Now;
                    context.Common_ContactDatabaseRole.Add(newCdr);
                    context.SaveChanges();
                    success = true;
                }
                catch (Exception ex)
                {
                    success = false;
                    throw ex;
                }
            }
            return success;
        }

        public bool AddContactAccessLevelReadWritePermission(int DbObjID, int ContactID, bool ReadCheck, bool WriteCheck)
        {
            bool success = false;
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                Common_ContactRolePermission crp = (from ccrp in context.Common_ContactRolePermission
                                                    where ccrp.Contact_ID == ContactID && ccrp.DatabaseObject_ID == DbObjID
                                                    select ccrp).FirstOrDefault();
                if (!ReadCheck && !WriteCheck)
                {
                    if (crp != null)
                    {
                        context.Common_ContactRolePermission.Remove(crp);
                        context.SaveChanges();
                    }
                    success = true;
                }
                else
                {
                    Common_ContactRolePermission newCrp = new Common_ContactRolePermission();
                    newCrp.DatabaseObject_ID = DbObjID;
                    newCrp.Contact_ID = ContactID;
                    newCrp.Read = ReadCheck;
                    newCrp.Write = WriteCheck;
                    try
                    {
                        if (crp == null)
                        {
                            context.Common_ContactRolePermission.Add(newCrp);
                            context.SaveChanges();
                        }
                        else
                        {
                            crp.Read = ReadCheck;
                            crp.Write = WriteCheck;
                            context.SaveChanges();
                        }
                        
                        success = true;
                    }
                    catch (Exception ex)
                    {
                        success = false;
                        throw ex;
                    }
                }
            }
            return success;
        }
    }
}