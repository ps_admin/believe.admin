﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Planetshakers.Events.Models
{
    public class DocumentReportViewModel
    {
        public int Event_ID { get; set; }
        public List<DocumentReportDocumentType> DocumentTypes { get; set; }
        public List<DocumentReportApplication> Applications { get; set; }
        public List<DocumentReportSummary> Summary { get; set; }

        public List<SelectListItem> ConferenceList
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = (from c in context.Events_Conference
                                orderby c.SortOrder descending
                                select new SelectListItem
                                {
                                    Text = c.ConferenceName,
                                    Value = c.Conference_ID.ToString().Trim()
                                }).ToList();

                    return list;
                }
            }
        }
        public List<SelectListItem> DocumentList
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = (from c in context.Volunteer_DocumentType
                                orderby c.SortOrder
                                select new SelectListItem
                                {
                                    Text = c.DocumentType,
                                    Value = c.DocumentType_ID.ToString().Trim()
                                }).ToList();
                    return list;
                }
            }
        }
    }

    public class DocumentReportDocumentType : IEquatable<DocumentReportDocumentType>
    {
        public string DocumentType { get; set; }
        public int DocumentType_ID { get; set; }   
        public int RegistrationType_ID { get; set; }

        #region override function
        public bool Equals(DocumentReportDocumentType other)
        {
            if (DocumentType == other.DocumentType && DocumentType_ID == other.DocumentType_ID)
                return true;

            return false;
        }

        public override int GetHashCode()
        {
            int hashDocumentType = DocumentType.GetHashCode();
            int hashDocumentType_ID = DocumentType_ID.GetHashCode();

            return hashDocumentType ^ hashDocumentType_ID;
        }
        #endregion
    }

    public class DocumentReportApplication
    {
        public string ContactName { get; set; }
        public int Contact_ID { get; set; }
        public List<DocumentReportDocSubmit> DocSubmit { get; set; }
    }

    public class DocumentReportDocSubmit
    {
        public int Contact_ID { get; set; }
        public int Event_ID { get; set; }
        public int DocumentType_ID { get; set; }
        public string status { get; set; }
    }
    public class DocumentReportSummary
    {
        public string DocumentType { get; set; }
        public int DocumentType_ID { get; set; }
        public int UnsubmitCount { get; set; }
        public int Submitted { get; set; }
        public int Approved { get; set; }
    }
}