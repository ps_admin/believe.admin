﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Planetshakers.Core.Models;

namespace Planetshakers.Events.Models
{
    public class DocumentUploads
    {
        public int Contact_ID { get; set; }
        public int Venue_ID
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    return (from v in context.Events_Venue
                            where v.Conference_ID == Event_ID
                            select v.Venue_ID).FirstOrDefault();
                }
            }
        }

        public int Event_ID { get; set; }
        //public int Venue_ID { get; set; }
        public List<Document> DocumentList
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var document = (from eq in context.Volunteer_EventDocument
                                    join q in context.Volunteer_DocumentType
                                    on eq.DocumentType_ID equals q.DocumentType_ID
                                    where eq.Inactive == false && RegistrationType_IDs.Contains(eq.RegistrationType_ID)
                                    orderby q.SortOrder
                                    select new Document
                                    {
                                        DocumnetType_ID = q.DocumentType_ID,
                                        DocumentType = q.DocumentType

                                    }).ToList();
                    var eventID = Event_ID;
                    foreach (var item in document)
                    {
                        var upload = (from a in context.Volunteer_ApplicationDocument
                                      where a.Contact_ID == Contact_ID && a.DocumentType_ID == item.DocumnetType_ID && a.Event_ID == eventID
                                      select a).FirstOrDefault();
                        if (upload != null)
                        {
                            item.Document_ID = upload.Document_ID;
                            item.UploadedDateTime = upload.UpdatedDateTime;
                            item.Approved = upload.Approved;
                            item.S3FileName = upload.S3Filename;
                            item.originalName = upload.OriginalFilename;
                            item.UploadedByName = (from c in context.Common_Contact where c.Contact_ID == upload.Contact_ID select c.FirstName + " " + c.LastName).FirstOrDefault();
                            item.uploaded = true;
                        }
                        else
                        {
                            item.uploaded = false;
                        }
                    }
                    return document;
                }
            }
        }
        public List<int> RegistrationType_IDs
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var RegistrationType_ID = (from r in context.Events_GroupBulkRegistration
                                               join rt in context.Events_RegistrationType on r.RegistrationType_ID equals rt.RegistrationType_ID
                                               where r.Venue_ID == Venue_ID && r.GroupLeader_ID == Contact_ID && r.Deleted == false
                                               select r.RegistrationType_ID).ToList();
                    return RegistrationType_ID;
                }
            }
        }
    }

    public class Document
    {
        public int Document_ID { get; set; }
        public int DocumnetType_ID { get; set; }
        public string DocumentType { get; set; }
        public string S3FileName { get; set; }
        public string originalName { get; set; }
        public DateTime UploadedDateTime { get; set; }
        public bool Approved { get; set; }
        public string FileName { get; set; }
        public string UploadedByName { get; set; }
        public bool uploaded { get; set; }

    }
}
