﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Planetshakers.Core.Models;

namespace Planetshakers.Events.Models
{
    public class BarcodeEmailModel
    {
        public BarcodeEmailModel()
        {
            Event = new Event();
        }

        public BarcodeEmailModel(int event_id)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                Event = (from ec in context.Events_Conference
                         join ev in context.Events_Venue
                         on ec.Conference_ID equals ev.Conference_ID
                         where ec.Conference_ID == event_id
                         select new Event
                         {
                             id = ec.Conference_ID,
                             name = ec.ConferenceName,
                             shortname = ec.ConferenceNameShort,
                             tagline = ec.ConferenceTagline,
                             date = ec.ConferenceDate,
                             location = ec.ConferenceLocation,
                             venueLocation = ev.VenueLocation,
                             venue_id = ev.Venue_ID
                         }).FirstOrDefault();
            }
        }

        // Name of contact
        public string FullName { get; set; }

        // Event
        public Event Event { get; set; }

        public int ContactId { get; set; }

        public int VenueId { get; set; }

        // Registration Number
        /*private int _RegistrationNumber { get; set; }
        public int RegistrationNumber
        {
            get
            {

                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    _RegistrationNumber = (from r in context.Events_Registration
                                           where r.Contact_ID == ContactId &&
                                               r.PBBVenue_ID == VenueId
                                           select r.Registration_ID).First();
                }

                return _RegistrationNumber;
            }
        }*/
        public int RegistrationNumber { get; set; }

        public string Email { get; set; }
    }

    public class BulkEmailModel
    {
        public SelectList conference_list
        {
            get
            {
                return new SelectList(_conference_list, "Venue_ID", "ConferenceName");
            }
        }

        private List<Conference> _conference_list
        {
            get
            {
                var list = new List<Conference>();

                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    list = (from ec in context.Events_Conference
                            join ev in context.Events_Venue
                                on ec.Conference_ID equals ev.Conference_ID
                            orderby ev.ConferenceStartDate descending
                            select new Conference {
                                Venue_ID = ev.Venue_ID,
                                ID = ec.Conference_ID,
                                ConferenceName = ec.ConferenceName
                            }).ToList();
                }

                return list;
            }
        }
    }

    public class SponsorshipConfirmationEmailModel
    {
        // Logged in contact
        public int ContactId { get; set; }

        // Name of contact
        [JsonProperty("FullName")]
        public string FullName { get; set; }

        // Email of contact
        public string Email { get; set; }

        // Event 
        public Event Event { get; set; }

        // Total sponsorshipAmount
        public decimal GrandTotal { get; set; }

        [JsonProperty("EventName")]
        public string EventName
        {
            get
            {
                return Event.name;
            }
        }

        [JsonProperty("Total")]
        public string Total
        {
            get
            {
                return Event.currency + " " + GrandTotal.ToString("0.00");
            }
        }
    }

    public class CreditedEmailModel
    {

        public string Email { get; set; }

        [JsonProperty("FullName")]
        public string FullName { get; set; }

        [JsonProperty("TotalAmount")]
        public string TotalAmount { get; set; }

        [JsonProperty("Currency")]
        public string Currency { get; set; }

        [JsonProperty("VoucherCode")]
        public string VoucherCode { get; set; }
    }

}