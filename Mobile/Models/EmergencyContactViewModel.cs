﻿using System.Collections.Generic;
using Planetshakers.Core.Models;
using System.Linq;
using System.Web.Mvc;

namespace Planetshakers.Events.Models
{
    public class EmergencyContactViewModel
    {
        public int Event_ID { get; set; }

        public List<SelectListItem> EventList
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = (from ec in context.Events_Conference
                                join ev in context.Events_Venue
                                on ec.Conference_ID equals ev.Conference_ID
                                orderby ev.ConferenceStartDate descending
                                select new SelectListItem
                                {
                                    Text = ec.ConferenceName,
                                    Value = ec.Conference_ID.ToString().Trim()
                                }).ToList();

                    return list;
                }
            }
        }

        public List<EmergencyContact> ContactList { get; set; }


    }
}