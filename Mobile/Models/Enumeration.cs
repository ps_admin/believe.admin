﻿
namespace Planetshakers.Events.Models
{
    public enum GroupSizeID
    {
        zero_to_ten = 1,
        eleven_to
    }

    public enum GroupCreditStatus
    {
        NonCredit = 1,
        AwaitingCreditApproval = 2,
        CreditApproved = 3,
        CreditDeclined = 4
    }

    public enum GroupType
    {
        YouthGroup = 1,
        NonYouthGroup = 2
    }

    public enum PaymentType
    {
        Cash = 1,
        Cheque = 2,
        CreditCard = 3,
        EFTPOS = 4,
        PayPal = 5,
        SPONSORHIP = 7
    }
}