﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Planetshakers.Events.Models
{
    public class EventSearchViewModel
    {
        public string SelectedConference { get; set; }
        public int? SelectedConferenceID { get; set; }
        public List<SelectListItem> RegistrationTypeList { get; set; }
        public List<int> RegistrationType_IDs { get; set; }
        public List<RegoSearchReult> SearchResult { get; set; }

        public List<SelectListItem> event_list
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {

                    var list = (from ec in context.Events_Conference
                                join ev in context.Events_Venue
                                    on ec.Conference_ID equals ev.Conference_ID
                                orderby ec.SortOrder
                                select new SelectListItem
                                {
                                    Value = ec.Conference_ID.ToString().Trim(),
                                    Text = ec.ConferenceName
                                }).ToList();

                    return list;
                }
            }
        }
    }
    public class RegoSearchReult
    {
        public int Contact_ID { get; set; }
        public decimal Balance { get; set; }
        public decimal Paid { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int RegistrationType_ID { get; set; }
        public string RegoType { get; set; }
        public bool applicationType { get; set; }

    }

}