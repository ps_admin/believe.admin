﻿using Planetshakers.Events.Models;
using System.Linq;
public class EventReportTotalRegistrationConference
{
    public string confName { get; set; }
    public int confSelectedID { get; set; }
    
    public string getConferenceName(int conference_ID)
    {

        using (PlanetshakersEntities context = new PlanetshakersEntities())
        {
            var name = (from ec in context.Events_Conference
                        where ec.Conference_ID == conference_ID
                        select ec.ConferenceName).FirstOrDefault();

            return name;
        }
    }
}