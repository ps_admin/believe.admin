//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Planetshakers.Events.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Events_GroupBulkRegistration
    {
        public int GroupBulkRegistration_ID { get; set; }
        public int GroupLeader_ID { get; set; }
        public int Venue_ID { get; set; }
        public int RegistrationType_ID { get; set; }
        public int Quantity { get; set; }
        public System.DateTime DateAdded { get; set; }
        public bool Deleted { get; set; }
    
        public virtual Events_Venue Events_Venue { get; set; }
        public virtual Events_Group Events_Group { get; set; }
        public virtual Events_RegistrationType Events_RegistrationType { get; set; }
    }
}
