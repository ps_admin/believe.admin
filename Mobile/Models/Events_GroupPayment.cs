//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Planetshakers.Events.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Events_GroupPayment
    {
        public int GroupPayment_ID { get; set; }
        public int GroupLeader_ID { get; set; }
        public int Conference_ID { get; set; }
        public int PaymentType_ID { get; set; }
        public int PaymentSource_ID { get; set; }
        public decimal PaymentAmount { get; set; }
        public string CCNumber { get; set; }
        public string CCExpiry { get; set; }
        public string CCName { get; set; }
        public string CCPhone { get; set; }
        public bool CCManual { get; set; }
        public string CCTransactionRef { get; set; }
        public bool CCRefund { get; set; }
        public string ChequeDrawer { get; set; }
        public string ChequeBank { get; set; }
        public string ChequeBranch { get; set; }
        public string PaypalTransactionRef { get; set; }
        public string Comment { get; set; }
        public Nullable<System.DateTime> PaymentStartDate { get; set; }
        public Nullable<bool> PaymentCompleted { get; set; }
        public Nullable<System.DateTime> PaymentCompletedDate { get; set; }
        public int PaymentBy_ID { get; set; }
        public int BankAccount_ID { get; set; }
        public Nullable<int> Voucher_ID { get; set; }
        public Nullable<bool> ExpiredIntent { get; set; }
    
        public virtual Common_PaymentType Common_PaymentType { get; set; }
        public virtual Events_Conference Events_Conference { get; set; }
        public virtual Events_Group Events_Group { get; set; }
        public virtual Common_PaymentSource Common_PaymentSource { get; set; }
    }
}
