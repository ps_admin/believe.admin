//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Planetshakers.Events.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Events_Team
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Events_Team()
        {
            this.Events_TeamContact = new HashSet<Events_TeamContact>();
        }
    
        public int Team_ID { get; set; }
        public string TeamName { get; set; }
        public int Event_ID { get; set; }
        public bool Inactive { get; set; }
        public int SortOrder { get; set; }
    
        public virtual Events_Conference Events_Conference { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Events_TeamContact> Events_TeamContact { get; set; }
    }
}
