﻿using System.Collections.Generic;
using Planetshakers.Core.Models;
using System;
using System.Linq;
using System.Web;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using System.IO;
using Planetshakers.Events.Helpers;

namespace Planetshakers.Events.Models
{
    public class FileModel
    {
        public string bucketName { get; set; }

        private static readonly RegionEndpoint bucketRegion = RegionEndpoint.APSoutheast2;
        private static IAmazonS3 client;
        private static readonly string _psAccessKey = AzureServices.KeyVault_RetreiveSecret("believe-apps", "AWS-S3-AccessKey") ?? "";
        private static readonly string _psSecretAccessKey = AzureServices.KeyVault_RetreiveSecret("believe-apps", "AWS-S3-SecretKey") ?? "";

        public FileModel(string bucket)
        {
            bucketName = bucket;
        }

        public void UploadFile(string awsFolderName, string fileName, Stream FileStream)
        {
            try
            {
                PutObjectRequest putObjectRequest = new PutObjectRequest
                {
                    BucketName = bucketName,
                    StorageClass = S3StorageClass.Standard,
                    ServerSideEncryptionMethod = ServerSideEncryptionMethod.AES256,
                    CannedACL = S3CannedACL.Private,
                    Key = awsFolderName + "/" + fileName,
                    InputStream = FileStream
                };

                // recheck keys
                using (client = new AmazonS3Client(_psAccessKey, _psSecretAccessKey, bucketRegion))
                    client.PutObject(putObjectRequest);
            }
            catch (AmazonS3Exception e)
            {
                Console.WriteLine("Error encountered on server. Message:'{0}' when writing an object", e.Message);
                throw e;
            }
            catch (Exception e)
            {
                Console.WriteLine("Unknown encountered on server. Message:'{0}' when writing an object", e.Message);
                throw e;
            }
        }

        public bool FolderExist(string folderName)
        {
            try
            {
                ListObjectsRequest request = new ListObjectsRequest
                {
                    BucketName = bucketName
                };

                using (client = new AmazonS3Client(_psAccessKey, _psSecretAccessKey, bucketRegion))
                {
                    ListObjectsResponse response = client.ListObjects(request);
                    if (response.S3Objects.Find(x => x.Key == (folderName + "/")) != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                };
            }
            catch (AmazonS3Exception e)
            {
                Console.WriteLine("Error encountered on server. Message:'{0}' when writing an object", e.Message);
                throw e;
            }
            catch (Exception e)
            {
                Console.WriteLine("Unknown encountered on server. Message:'{0}' when writing an object", e.Message);
                throw e;
            }
        }

        public ResourcesItem GetFile(string folderName, string filename)
        {
            try
            {
                ListObjectsRequest request = new ListObjectsRequest
                {
                    BucketName = bucketName,
                    Prefix = folderName + "/"
                };

                ResourcesItem result = new ResourcesItem();
                using (client = new AmazonS3Client(_psAccessKey, _psSecretAccessKey, bucketRegion))
                    do
                    {
                        ListObjectsResponse response = client.ListObjects(request);
                        foreach (S3Object downloadable in response.S3Objects)
                        {
                            if (downloadable.Key == folderName + "/" + filename)
                            {
                                ResourcesItem newitem = new ResourcesItem();

                                GetPreSignedUrlRequest requestURL = new GetPreSignedUrlRequest();
                                requestURL.BucketName = bucketName;
                                requestURL.Key = downloadable.Key;
                                requestURL.Expires = DateTime.Now.AddHours(1);
                                requestURL.Protocol = Protocol.HTTP;

                                string url = client.GetPreSignedURL(requestURL);
                                string size = SizeSuffix(downloadable.Size, 2);

                                newitem.Size = size;
                                newitem.Name = downloadable.Key;
                                newitem.DownloadUrl = url;

                                result = newitem;
                            }

                        }
                        // If the response is truncated, set the marker to get the next 
                        // set of keys.
                        if (response.IsTruncated)
                        {
                            request.Marker = response.NextMarker;
                        }
                        else
                        {
                            request = null;
                        }

                    } while (request != null);
                return result;
            }
            catch (AmazonS3Exception e)
            {
                Console.WriteLine("Error encountered on server. Message:'{0}' when writing an object", e.Message);
                throw e;
            }
            catch (Exception e)
            {
                Console.WriteLine("Unknown encountered on server. Message:'{0}' when writing an object", e.Message);
                throw e;
            }
        }

        public void DeleteFile(string fileName)
        {
            try
            {
                DeleteObjectRequest request = new DeleteObjectRequest
                {
                    BucketName = bucketName,
                    Key = fileName
                };
                using (client = new AmazonS3Client(_psAccessKey, _psSecretAccessKey, bucketRegion))
                {
                    client.DeleteObject(request);
                };
            }
            catch (AmazonS3Exception e)
            {
                Console.WriteLine("Error encountered on server. Message:'{0}' when writing an object", e.Message);
                throw e;
            }
            catch (Exception e)
            {
                Console.WriteLine("Unknown encountered on server. Message:'{0}' when writing an object", e.Message);
                throw e;
            }
        }

        static readonly string[] SizeSuffixes =
                   { "bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };

        static string SizeSuffix(long value, int decimalPlaces = 2)
        {
            if (decimalPlaces < 0) { throw new ArgumentOutOfRangeException("decimalPlaces"); }
            if (value < 0) { return "-" + SizeSuffix(-value); }
            if (value == 0) { return string.Format("{0:n" + decimalPlaces + "} bytes", 0); }

            // mag is 0 for bytes, 1 for KB, 2, for MB, etc.
            int mag = (int)Math.Log(value, 1024);

            // 1L << (mag * 10) == 2 ^ (10 * mag) 
            // [i.e. the number of bytes in the unit corresponding to mag]
            decimal adjustedSize = (decimal)value / (1L << (mag * 10));

            // make adjustment when the value is large enough that
            // it would round up to 1000 or more
            if (Math.Round(adjustedSize, decimalPlaces) >= 1000)
            {
                mag += 1;
                adjustedSize /= 1024;
            }

            return string.Format("{0:n" + decimalPlaces + "} {1}",
                adjustedSize,
                SizeSuffixes[mag]);
        }
    }
}