﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Planetshakers.Events.Models
{
    public class FundingGoalViewModel
    {
        public int RegistrationType_ID { get; set; }
        public List<FundingGoal> FundingGoalList { get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = (from fg in context.Events_FundingGoal
                                where fg.RegistrationType_ID == RegistrationType_ID
                                orderby fg.DueDate
                                select new FundingGoal
                                {
                                    ID = fg.FundingGoal_ID,
                                    Milestone = fg.Milestone,
                                    CumulativeTotal = fg.CumulativeTotal, 
                                    DueDate = fg.DueDate,
                                    RegistrationType_ID = RegistrationType_ID
                                }).ToList();
                    return list;
                }
            }
        }       
    }
    public class FundingGoal
    {
        public int ID { get; set; }
        public int RegistrationType_ID { get; set; }
        public string Milestone { get; set; }
        public int CumulativeTotal { get; set; }
        public DateTime DueDate { get; set; }

    }

}