﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Planetshakers.Core.Models;

namespace Planetshakers.Events.Models
{
    public class GroupViewModel
    {
        public GroupViewModel()
        {
            Group = new GroupDetails();
            Conference = new Conference();
            GroupRegistration = new GroupRegistration();
            NewRegistration = new Registration();
            Payment = new Payment();
            CommentList = new List<CommentItem>();
            OutstandingBalance = new OutstandingBalance();
            PaymentList = new List<Payment>();
            History = new List<GroupHistory>();
            Documents = new DocumentUploads();
            applicationDetails = new Application();
            emergencyContacts = new List<ApplicationEmergencyContact>();
        }

        public GroupViewModel(int group_id, int conference_id)
        {
            Group = new GroupDetails(group_id);
            Conference = new Conference(conference_id);
            GroupRegistration = new GroupRegistration();
            NewRegistration = new Registration();
            Payment = new Payment();
            CommentList = new List<CommentItem>();
            OutstandingBalance = new OutstandingBalance();
            PaymentList = new List<Payment>();
            History = new List<GroupHistory>();
            Documents = new DocumentUploads();
            applicationDetails = new Application();
            emergencyContacts = new List<ApplicationEmergencyContact>();

        }

        public GroupDetails Group { get; set; }
        public Conference Conference { get; set; }
        public GroupRegistration GroupRegistration { get; set; }
        public Payment Payment { get; set; }

        public CommentItem NewComment { get; set; }

        public List<GroupHistory> History { get; set; }

        public Registration NewRegistration { get; set; }

        public OutstandingBalance OutstandingBalance { get; set; }

        public List<Payment> PaymentList { get; set; }

        public DocumentUploads Documents { get; set; }

        public Application applicationDetails { get; set; }
        public List<ApplicationEmergencyContact> emergencyContacts { get; set; }
        public int ContactProfile_ID { get; set; }

        public List<SponsorshipItem> SponsorshipList
        {
            get
            {
                var list = new List<SponsorshipItem>();

                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    list = (from vp in context.Events_VariablePayment
                            join pt in context.Common_PaymentType on vp.PaymentType_ID equals pt.PaymentType_ID
                            join c1 in context.Common_Contact on vp.IncomingContact_ID equals c1.Contact_ID into subc1 from c1 in subc1.DefaultIfEmpty()
                            join c2 in context.Common_Contact on vp.OutgoingContact_ID equals c2.Contact_ID into subc2 from c2 in subc2.DefaultIfEmpty()
                            where vp.Conference_ID == Conference.ID
                                    && (vp.IncomingContact_ID == Group.ID || vp.OutgoingContact_ID == Group.ID)
                                    && (vp.PaymentCompleted == true)
                            select new SponsorshipItem
                            {
                                VariablePaymentID = vp.VariablePayment_ID,
                                Donor = (vp.IncomingContact_ID == Group.ID) ? true : false,
                                Recipient = (vp.OutgoingContact_ID == Group.ID) ? true : false,
                                PaymentType = pt.PaymentType,
                                PaymentAmount = (decimal)vp.PaymentAmount,
                                PaymentDate = vp.PaymentDate,
                                //FullName = (vp.IncomingContact_ID == Group.ID) ? c1.FirstName + " " + c1.LastName : c2.FirstName + " " + c2.LastName,
                                //Mobile = (vp.IncomingContact_ID == Group.ID) ? c1.Mobile : c2.Mobile, 
                                //Email = (vp.IncomingContact_ID == Group.ID) ? c1.Email: c2.Email,
                                //Anonymous = vp.Anonymous
                            }).ToList();
                }
                return list;
            }
        }

        public List<CommentItem> CommentList { get; set; }
        public List<Common_Contact> Registrants { get; set; }

        public List<BulkRegistration> GroupBulkRegistration_List
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var lst = (from egrv in context.Events_GroupBulkRegistration
                               join ert in context.Events_RegistrationType
                               on egrv.RegistrationType_ID equals ert.RegistrationType_ID
                               where egrv.GroupLeader_ID == Group.ID && egrv.Venue_ID == Conference.Venue_ID && egrv.Deleted == false
                               select new BulkRegistration
                               {
                                   ID = egrv.GroupBulkRegistration_ID,
                                   GroupLeader_ID = egrv.GroupLeader_ID,
                                   RegistrationType_ID = egrv.RegistrationType_ID,
                                   RegistrationType = ert.RegistrationType,
                                   Quantity = egrv.Quantity,
                                   TotalCost = ert.RegistrationCost * egrv.Quantity,
                                   DateAdded = egrv.DateAdded
                               }).ToList();

                    return lst;
                }
            }
        }

        public List<Events_RegistrationType> RegistrationType_List
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var lst = (from ert in context.Events_RegistrationType
                               where ert.Conference_ID == Conference.ID
                               select ert).ToList();

                    return lst;
                }
            }
        }

        public List<PCRContact> PCRList { get; set; }
        public bool Withdrawn { get; set; }

        #region Select List
        private List<Events_Conference> _Conference_List
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var lst = (from ec in context.Events_Conference
                               join ev in context.Events_Venue
                               on ec.Conference_ID equals ev.Conference_ID
                               orderby ev.ConferenceStartDate descending
                               select ec).ToList();

                    return lst;
                }
            }
        }

        public SelectList Conference_List
        {
            get
            {
                return new SelectList(_Conference_List, "Conference_ID", "ConferenceName");
            }
        }        

        private List<Common_GeneralCountry> _Country_List
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var lst = (from gc in context.Common_GeneralCountry
                               orderby gc.Country
                               select gc).ToList();

                    return lst;
                }
            }
        }

        public SelectList Country_List
        {
            get
            {
                return new SelectList(_Country_List, "Country_ID", "Country");
            }
        }

        private List<Registration> _PreviousAllocation_List
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var lst = (from er in context.Events_Registration
                                join cc in context.Common_Contact
                                on er.Contact_ID equals cc.Contact_ID
                                where er.GroupLeader_ID == Group.ID
                                orderby cc.FirstName
                                select new Registration
                                {
                                    ContactID = cc.Contact_ID,
                                    FirstName = cc.FirstName + " " + cc.LastName
                                }).Distinct().ToList();

                    return lst;
                }
            }
        }

        public SelectList PreviousAllocation_List
        {
            get
            {
                return new SelectList(_PreviousAllocation_List, "ContactID", "FirstName");
            }
        }
        //updated for believe
        public List<Events_RegistrationType> _AvailableRegistration_List
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var lst = (from egrv in context.Events_GroupBulkRegistration
                               join ert in context.Events_RegistrationType
                               on egrv.RegistrationType_ID equals ert.RegistrationType_ID
                               where egrv.GroupLeader_ID == Group.ID && egrv.Venue_ID == Conference.Venue_ID 
                               select ert).Distinct().ToList();

                    return lst;
                }
            }
        }

        public SelectList AvailableRegistration_List
        {
            get
            {
                return new SelectList(_AvailableRegistration_List, "RegistrationType_ID", "RegistrationType");
            }
        }

        public List<Common_PaymentType> _PaymentType_List
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var lst = (from pt in context.Common_PaymentType
                               select pt).ToList();

                    //For by passing credit card payment use
                    lst.Insert(2, new Common_PaymentType { PaymentType_ID = 3, PaymentType = "Credit Card - STRIPE" });
                    lst.Insert(3, new Common_PaymentType { PaymentType_ID = 10000, PaymentType = "Credit Card - ADMIN" }); //Chosen a big number so there is no chance for payment type to be that id
                    lst.Remove(lst.Find(x => x.PaymentType == "Credit Card"));

                    return lst;
                }
            }
        }

        public SelectList PaymentType_List
        {
            get
            {
                return new SelectList(_PaymentType_List, "PaymentType_ID", "PaymentType");
            }
        }

        public List<SelectListItem> PaymentCategory_List
        {
            get
            {
                var lst = new List<SelectListItem>();

                lst.Add(new SelectListItem { Text = "Payment", Value = "Payment" });
                lst.Add(new SelectListItem { Text = "Refund", Value = "Refund" });

                return lst;
            }
        }

        public List<Common_PaymentSource> _PaymentSource_List
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = (from s in context.Common_PaymentSource
                                select s).ToList();
                    return list;
                }
            }
        }
        public SelectList PaymentSource_List
        {
            get
            {
                return new SelectList(_PaymentSource_List, "PaymentSource_ID", "PaymentSource");
            }
        }

        #endregion
    }
}