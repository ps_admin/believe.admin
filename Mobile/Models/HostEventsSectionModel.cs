﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Planetshakers.Events.Security;

namespace Planetshakers.Events.Models
{
    public class HostEventsManagementViewModel
    {
        public List<HostEvents> HostEventList
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = (from c in context.Church_Course
                                select new HostEvents
                                {
                                    HostEvent_ID = c.Course_ID,
                                    Name = c.Name,
                                }).ToList();
                    foreach (var item in list)
                    {
                        var instance = (from ci in context.Church_CourseInstance
                                        where ci.Course_ID == item.HostEvent_ID
                                        orderby ci.StartDate descending
                                        select ci).FirstOrDefault();
                        if (instance != null)
                        {
                            item.LatestCourseInstance = instance.Name + " (Starting " + instance.StartDate.Value.ToString("dd MMM yyyy") + ")";
                        }
                    }
                    return list;
                }
            }
        }
    }

    public class HostEventViewModel
    {
        public List<SelectListItem> EventList
        {
            get
            {
                using(PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var _events = (from ec in context.Events_Conference
                                   join ev in context.Events_Venue
                                   on ec.Conference_ID equals ev.Conference_ID
                                   orderby ev.ConferenceStartDate descending
                                   select new SelectListItem
                                   {
                                       Value = ec.Conference_ID.ToString().Trim(),
                                       Text = ec.ConferenceName
                                   }).ToList();

                    return _events;
                }
            }
        }
        public List<HostEventInstance> InstanceList { get; set; }
        public void populateInstanctList(int Event_ID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var list = (from ci in context.Church_CourseInstance
                            join c in context.Church_Course on ci.Course_ID equals c.Course_ID
                            join e in context.Events_Conference on ci.Event_ID equals e.Conference_ID
                            where ci.Event_ID == Event_ID
                            select new HostEventInstance
                            {
                                HostEventInstance_ID = ci.CourseInstance_ID,
                                Name = ci.Name,
                                HostEvent_ID = ci.Event_ID,
                                HostEventName = c.Name,
                                StartDate = (DateTime)ci.StartDate
                            }).ToList();
                this.InstanceList = list;
            }
        }
        public int Event_ID { get; set; }
    }

    public class HostEvents
    {
        public int HostEvent_ID { get; set; }
        public string Name { get; set; }
        public int PointRequired { get; set; }
        public bool ShowInCustomReport { get; set; }
        public bool LogCompletedSession { get; set; }
        public bool Deleted { get; set; }
        public int SortOrder { get; set; }
        public string Template_ID { get; set; }
        public bool isEdit { get; set; }
        public string LatestCourseInstance { get; set; }
        public List<HostEventInstance> InstanceList { get; set; }
        public List<HostEventTopic> TopicList { get; set; }
        public void getHostEvent(int HostEvent_ID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var hostevent = (from c in context.Church_Course
                                 where c.Course_ID == HostEvent_ID && Deleted == false
                                 select c).FirstOrDefault();
                this.HostEvent_ID = HostEvent_ID;
                this.Name = hostevent.Name;
                this.PointRequired = hostevent.PointsRequiredForCompletion;
                this.LogCompletedSession = hostevent.LogCompletedSession;
                this.ShowInCustomReport = hostevent.ShowInCustomReport;
                this.SortOrder = hostevent.SortOrder;

            }
        }
        public void populateInstanceList(int HostEvent_ID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var list = (from ci in context.Church_CourseInstance
                            where ci.Course_ID == HostEvent_ID && ci.Deleted == false
                            orderby ci.StartDate descending
                            select new HostEventInstance
                            {
                                HostEventInstance_ID = ci.CourseInstance_ID,
                                HostEvent_ID = HostEvent_ID,
                                Name = ci.Name,
                                StartDate = (DateTime)ci.StartDate,
                                EndDate = (DateTime)ci.EndDate,
                                MaxCapacity = (ci.MaxCapacity == null) ? 0 : (int)ci.MaxCapacity,
                            }).ToList();

                this.InstanceList = list;
            }
        }
        public void populateTopicList(int HostEvent_ID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var list = (from ci in context.Church_CourseTopic
                            where ci.Course_ID == HostEvent_ID && ci.Deleted == false
                            orderby ci.Name
                            select new HostEventTopic
                            {
                                HostEventTopic_ID = ci.CourseTopic_ID,
                                HostEvent_ID = HostEvent_ID,
                                Name = ci.Name,
                                PointWeighting = ci.PointWeighting,
                            }).ToList();

                this.TopicList = list;
            }
        }
        public int addHostEventDetails()
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                Church_Course newCourse = new Church_Course()
                {
                    Course_ID = this.HostEvent_ID,
                    Name = this.Name,
                    PointsRequiredForCompletion = this.PointRequired,
                    ShowInCustomReport = this.ShowInCustomReport,
                    LogCompletedSession = this.LogCompletedSession,
                    Deleted = false,
                    SortOrder = 1
                };
                context.Church_Course.Add(newCourse);
                context.SaveChanges();
                return newCourse.Course_ID;
            }
        }
        public void editHostEventDetails()
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var course = (from c in context.Church_Course
                              where c.Course_ID == this.HostEvent_ID
                              select c).FirstOrDefault();
                course.Name = this.Name;
                course.PointsRequiredForCompletion = this.PointRequired;
                course.ShowInCustomReport = this.ShowInCustomReport;
                course.LogCompletedSession = this.LogCompletedSession;
                context.SaveChanges();
            }
        }
    }

    public class HostEventInstance
    {
        public int HostEventInstance_ID { get; set; }
        public int HostEvent_ID { get; set; }
        public string Name { get; set; }
        public string HostEventName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int MaxCapacity { get; set; }
        public int Event_ID { get; set; }
        public bool Deleted { get; set; }
        public bool isEdit { get; set; }
        public List<SelectListItem> EventList
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var _events = (from ec in context.Events_Conference
                                   join ev in context.Events_Venue
                                   on ec.Conference_ID equals ev.Conference_ID
                                   orderby ev.ConferenceStartDate descending
                                   select new SelectListItem
                                   {
                                       Value = ec.Conference_ID.ToString().Trim(),
                                       Text = ec.ConferenceName
                                   }).ToList();

                    return _events;
                }
            }
        }
        public List<HostEventSession> SessionList { get; set; }
        public List<ContactItem> RegistrantList { get; set; }
        public List<ContactItem> AttendeeList { get; set; }
        public void getInstance(int Instance_ID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var instance = (from c in context.Church_CourseInstance
                                where c.CourseInstance_ID == Instance_ID && Deleted == false
                                select c).FirstOrDefault();
                this.HostEvent_ID = instance.Course_ID;
                this.Name = instance.Name;
                this.StartDate = (DateTime)instance.StartDate;
                this.EndDate = (DateTime)instance.EndDate;
                this.MaxCapacity = (int)instance.MaxCapacity;
                this.HostEvent_ID = instance.Course_ID;
                this.Event_ID = instance.Event_ID;
            }
        }
        public void getSession(int Instance_ID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var list = (from c in context.Church_CourseSession
                            where c.CourseInstance_ID == Instance_ID && Deleted == false
                            select new HostEventSession
                            {
                                Session_ID = c.CourseSession_ID,
                                Instanct_ID = Instance_ID,
                                Date = c.Date,
                            }).ToList();
                this.SessionList = list;
            }
        }
        public void getAttendanceList(int Instance_ID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var RegoTypeList = (from r in context.Events_RegistrationType
                                    where r.Conference_ID == this.Event_ID
                                    select r.RegistrationType_ID).ToList();
                var ContactList = (from a in context.Volunteer_Application
                                   join c in context.Common_Contact on a.Contact_ID equals c.Contact_ID
                                   join rt in context.Events_RegistrationType on a.RegistrationType_ID equals rt.RegistrationType_ID
                                   where a.Approved == true && RegoTypeList.Contains((int)a.RegistrationType_ID)
                                   select new ContactItem
                                   {
                                        Contact_ID = c.Contact_ID,
                                        FirstName = c.FirstName,
                                        LastName = c.LastName,
                                        isCollege = rt.IsCollegeRegistrationType,
                                        email = c.Email,
                                   }).ToList();
                var RegistrantList = new List<ContactItem>();
                var AttendeeList = new List<ContactItem>();
                foreach(var item in ContactList)
                {
                    var Attending = (from e in context.Church_ContactCourseEnrolment
                                     where e.Contact_ID == item.Contact_ID && e.CourseInstance_ID == Instance_ID
                                     select e).Any();
                    if (Attending)
                    {
                        AttendeeList.Add(item);
                    } else
                    {
                        RegistrantList.Add(item);
                    }
                }
                this.RegistrantList = RegistrantList;
                this.AttendeeList = AttendeeList;
            }
        }
        public List<HostEventTopic> getTopic(int Session_ID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var list = (from c in context.Church_CourseSessionTopic
                            join s in context.Church_CourseTopic on c.CourseTopic_ID equals s.CourseTopic_ID
                            where c.CourseSession_ID == Session_ID && s.Deleted == false
                            select new HostEventTopic
                            {
                                Name = s.Name,
                                HostEventTopic_ID = s.CourseTopic_ID,

                            }).ToList();
                return list;

            }
        }
        public int addHostEventInstances()
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                Church_CourseInstance newInstance = new Church_CourseInstance()
                {
                    Course_ID = this.HostEvent_ID,
                    Name = this.Name,
                    StartDate = this.StartDate,
                    EndDate = this.EndDate,
                    Cost = 0,
                    MaxCapacity = this.MaxCapacity,
                    Deleted = false,
                    BankAccount_ID = null,
                    Event_ID = this.Event_ID
                };
                context.Church_CourseInstance.Add(newInstance);
                context.SaveChanges();
                return newInstance.CourseInstance_ID;
            }
        }
        public void editHostEventInstances()
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var instance = (from i in context.Church_CourseInstance
                                where i.CourseInstance_ID == this.HostEventInstance_ID
                                select i).FirstOrDefault();
                instance.Name = this.Name;
                instance.StartDate = this.StartDate;
                instance.EndDate = this.EndDate;
                instance.MaxCapacity = this.MaxCapacity;
                instance.Event_ID = this.Event_ID;
                context.SaveChanges();
            }
        }

        //for mark Attendance
        public int Session_ID { get; set; }
    }

    public class HostEventSession
    {
        public int Session_ID { get; set; }
        public int Instanct_ID { get; set; }
        public int HostEvent_ID { get; set; }
        public DateTime Date { get; set; }
        public List<SelectListItem> SelectTopicList {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var takenTopics = (from st in context.Church_CourseSessionTopic
                                       where st.CourseInstance_ID == this.Instanct_ID && st.CourseSession_ID != this.Session_ID
                                       select st.CourseTopic_ID).ToList();
                    var list = (from st in context.Church_CourseTopic
                                where st.Course_ID == this.HostEvent_ID && !takenTopics.Contains(st.CourseTopic_ID)
                                select new SelectListItem
                                {
                                    Value = st.CourseTopic_ID.ToString().Trim(),
                                    Text = st.Name
                                }).ToList();
                    return list;
                }
            }
        }
        public List<int> selectedTopic { get; set; }
        public List<HostEventTopic> Topics { get; set; }
        public string TopicStringList { get; set; }
        public bool isEdit { get; set; }
        public bool isToday { get; set; }
        public bool Passed { get; set; }
        public void getSession(int Session_ID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var list = (from c in context.Church_CourseSession
                            join i in context.Church_CourseInstance on c.CourseInstance_ID equals i.CourseInstance_ID
                            where c.CourseSession_ID == Session_ID && c.Deleted == false
                            select c).FirstOrDefault();
                this.Session_ID = Session_ID;
                this.Instanct_ID = list.CourseInstance_ID;
                this.Date = list.Date;

                var topicList = (from st in context.Church_CourseSessionTopic
                                 where st.CourseSession_ID == Session_ID
                                 select st.CourseTopic_ID).ToList();
                this.selectedTopic = topicList;
            }
        }
        public void addSession()
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                Church_CourseSession newSession = new Church_CourseSession()
                {
                    CourseInstance_ID = this.Instanct_ID,
                    Date = this.Date,
                    Deleted = false,
                };
                context.Church_CourseSession.Add(newSession);
                context.SaveChanges();

                foreach(var item in this.selectedTopic)
                {
                    Church_CourseSessionTopic newSessionTopic = new Church_CourseSessionTopic()
                    {
                        CourseInstance_ID = this.Instanct_ID,
                        CourseSession_ID = newSession.CourseSession_ID,
                        CourseTopic_ID = item
                    };
                    context.Church_CourseSessionTopic.Add(newSessionTopic);
                    context.SaveChanges();
                }
            }
        }
        public void editSession()
        {
            using(PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var session = (from s in context.Church_CourseSession
                               where s.CourseSession_ID == this.Session_ID
                               select s).FirstOrDefault();
                session.Date = this.Date;
                context.SaveChanges();

                var Instance_ID = session.CourseInstance_ID;
                var existingTopics = (from st in context.Church_CourseSessionTopic
                                      where st.CourseSession_ID == this.Session_ID
                                      select st.CourseTopic_ID).ToList();
                //add topics that are not in the existing list
                if (this.selectedTopic != null)
                {
                    foreach (var item in this.selectedTopic)
                    {
                        if (!existingTopics.Contains(item))
                        {
                            Church_CourseSessionTopic newSessionTopic = new Church_CourseSessionTopic()
                            {
                                CourseInstance_ID = Instance_ID,
                                CourseSession_ID = this.Session_ID,
                                CourseTopic_ID = item
                            };
                            context.Church_CourseSessionTopic.Add(newSessionTopic);
                        }
                    }
                    context.SaveChanges();
                } else
                {
                    this.selectedTopic = new List<int>();
                }
                //remove topics that are not in the new list 
                foreach(var item in existingTopics)
                {
                    if(!this.selectedTopic.Contains(item))
                    {
                        var removeItem = (from st in context.Church_CourseSessionTopic
                                          where st.CourseTopic_ID == item && st.CourseSession_ID == this.Session_ID
                                          select st).FirstOrDefault();
                        if(removeItem != null)
                        {
                            context.Church_CourseSessionTopic.Remove(removeItem);
                        }
                    }
                }
                context.SaveChanges();
            }
        }
    }

    public class HostEventTopic
    {
        public int HostEventTopic_ID { get; set; }
        public int HostEvent_ID { get; set; }
        public string Name { get; set; }
        public int PointWeighting { get; set; }
        public bool Deleted { get; set; }
        public bool isEdit { get; set; }
        public void getTopicModal(int Topic_ID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var topic = (from t in context.Church_CourseTopic
                             where t.CourseTopic_ID == Topic_ID
                             select t).FirstOrDefault();
                this.Name = topic.Name;
                this.PointWeighting = topic.PointWeighting;
            }
        }
        public void addTopic()
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                Church_CourseTopic newTopic = new Church_CourseTopic();
                newTopic.Course_ID = this.HostEvent_ID;
                newTopic.Name = this.Name;
                newTopic.PointWeighting = this.PointWeighting;
                newTopic.Deleted = false;
                context.Church_CourseTopic.Add(newTopic);
                context.SaveChanges();
            }
        }
        public void editTopic()
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var topic = (from t in context.Church_CourseTopic
                             where t.CourseTopic_ID == this.HostEventTopic_ID
                             select t).FirstOrDefault();
                topic.Name = this.Name;
                topic.PointWeighting = this.PointWeighting;
                context.SaveChanges();
            }
        }
    }

    public class ContactItem
    {
        public int Contact_ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }    
        public string email { get; set; }
        public bool isCollege { get; set; }
        public bool Attended { get; set; }
    
    }

    public class CourseEnrolment
    {
        public int Church_ContactCourseEnrolment_ID { get; set; }
        public int Contact_ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int CourseInstance_ID { get; set; }
        public DateTime EnrolmentDate { get; set; }
        public string Comment { get; set; }
        public DateTime IntroductoryEmailSent { get; set; }
        public bool Completed { get; set; }
        public bool addAttendee(int Contact_ID, int CourseInstance_ID)
        {
            using(PlanetshakersEntities context = new PlanetshakersEntities())
            {
                Church_ContactCourseEnrolment newEnrolment = new Church_ContactCourseEnrolment()
                {
                    Contact_ID = Contact_ID,
                    CourseInstance_ID = CourseInstance_ID,
                    EnrolmentDate = DateTime.Now,
                    Comments = "",
                    IntroductoryEmailSentDate = null,
                    Completed = false
                };
                context.Church_ContactCourseEnrolment.Add(newEnrolment);
                context.SaveChanges();
                var contact = (from c in context.Common_Contact
                               where c.Contact_ID == Contact_ID
                               select c).FirstOrDefault();
                this.FirstName = contact.FirstName;
                this.LastName = contact.LastName;
                return true;
            }
        }
    }

    public class MarkAttendanceViewModel
    {
        public int Event_ID { get; set; }
        public List<ContactItem> AttendeeList { get; set; }
        public int Session_ID { get; set; }
        public DateTime SessionDate { get; set; }
        public string HostEventName { get; set; }
        public string InstanceName { get; set; }
        public string EventName { get; set; }
        public bool mark { get; set; }
        public void getDetails()
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var Session = (from s in context.Church_CourseSession
                                    where s.CourseSession_ID == this.Session_ID
                                    select s).FirstOrDefault();
                this.SessionDate = Session.Date;
                var Instance = (from i in context.Church_CourseInstance
                                     where i.CourseInstance_ID == Session.CourseInstance_ID
                                select i).FirstOrDefault();
                this.InstanceName = Instance.Name;
                this.HostEventName = (from c in context.Church_Course
                                      where c.Course_ID == Instance.Course_ID
                                      select c.Name).FirstOrDefault();
                this.EventName = (from e in context.Events_Conference
                                  where e.Conference_ID == Instance.Event_ID
                                  select e.ConferenceName).FirstOrDefault();
                foreach (var item in this.AttendeeList)
                {
                    item.Attended = (from a in context.Church_CourseEnrolmentAttendance
                                     where a.Contact_ID == item.Contact_ID && a.CourseSession_ID == this.Session_ID
                                     select a).Any();
                }
            }
        }

        public void MarkAttendance(int Contact_ID, int Session_ID)
        {
            using(PlanetshakersEntities context = new PlanetshakersEntities())
            {
                Church_CourseEnrolmentAttendance newAttendance = new Church_CourseEnrolmentAttendance()
                {
                    Contact_ID = Contact_ID,
                    CourseSession_ID = Session_ID,
                    MarkedOffDateTime = DateTime.Now,
                    MarkedOffBy_ID = CurrentContext.Instance.User.ContactID
                };
                context.Church_CourseEnrolmentAttendance.Add(newAttendance);
                context.SaveChanges();

                // -- check whether it acheive the prereq to complete the course and mark course completed on enrolment
                var instance_id = (from i in context.Church_CourseInstance
                                    join s in context.Church_CourseSession on i.CourseInstance_ID equals s.CourseInstance_ID
                                    where s.CourseSession_ID == Session_ID
                                    select i.CourseInstance_ID).FirstOrDefault();

                var pointsNeeded = (from i in context.Church_CourseInstance
                                    join c in context.Church_Course on i.Course_ID equals c.Course_ID
                                    where i.CourseInstance_ID == instance_id
                                    select c.PointsRequiredForCompletion).FirstOrDefault();
                var attendedSessionPoints = (from a in context.Church_CourseEnrolmentAttendance
                                             join s in context.Church_CourseSession on a.CourseSession_ID equals s.CourseSession_ID
                                             join st in context.Church_CourseSessionTopic on s.CourseSession_ID equals st.CourseSession_ID
                                             join t in context.Church_CourseTopic on st.CourseTopic_ID equals t.CourseTopic_ID
                                             where a.Contact_ID == Contact_ID && s.CourseInstance_ID == instance_id
                                             select t.PointWeighting).Sum();
                if(attendedSessionPoints >= pointsNeeded)
                {
                    var enrolment = (from e in context.Church_ContactCourseEnrolment
                                     where e.Contact_ID == Contact_ID && e.CourseInstance_ID == instance_id
                                     select e).FirstOrDefault();
                    if(enrolment != null)
                    {
                        enrolment.Completed = true;
                        context.SaveChanges();
                    }
                }
            }
        }
        public void RemoveAttendance(int Contact_ID, int Session_ID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var attendance = (from a in context.Church_CourseEnrolmentAttendance
                                  where a.Contact_ID == Contact_ID && a.CourseSession_ID == Session_ID
                                  select a).FirstOrDefault();
                context.Church_CourseEnrolmentAttendance.Remove(attendance);
                context.SaveChanges();
            }
        }
    }
}