﻿using Planetshakers.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Planetshakers.Events.Models
{
    public class MailingListReportModel
    {
        public Boolean displayReport = false;

        public int? Conference_ID { get; set; }

        public int? MailingList_ID { get; set; }

        public int? MailingListType_ID { get; set; }

        public List<EventModel> _events
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var _events = (from ec in context.Events_Conference
                                   join ev in context.Events_Venue
                                   on ec.Conference_ID equals ev.Conference_ID
                                   orderby ev.ConferenceStartDate descending
                                   select new EventModel
                                   {
                                       ConferenceID = ec.Conference_ID,
                                       ConferenceName = ec.ConferenceName
                                   }).ToList();

                    return _events;
                }
            }
        }

        public IEnumerable<SelectListItem> EventList { get { return new SelectList(_events, "ConferenceID", "ConferenceName"); } }

        public List<SelectListItem> MailingList
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var mailingList = (from ml in context.Common_MailingList
                                       orderby ml.SortOrder
                                       select new SelectListItem
                                       {
                                           Text = ml.Name,
                                           Value = ml.MailingList_ID.ToString().Trim()
                                       }).ToList();

                    return mailingList;
                }
            }
        }

        public List<ContactMailingList> contactMailingList { get; set; }

        public List<SelectListItem> FilterDropdown { get
            {
                var list = new List<SelectListItem>
                {
                    new SelectListItem { Text = "Internal", Value = "internal" },
                    new SelectListItem { Text = "External", Value = "external" },
                    new SelectListItem { Text = "Both", Value = "both" }
                };

                return list;
            } 
        }

        public List<SelectListItem> MailingListType
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var mailingList = (from mlt in context.Common_MailingListType
                                       select new SelectListItem
                                       {
                                           Text = mlt.Name,
                                           Value = mlt.MailingListType_ID.ToString().Trim()
                                       }).ToList();

                    return mailingList;
                }
            }
        }
    }
}