﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Planetshakers.Events.Controllers;
using System.Web.Configuration;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Planetshakers.Events.Helpers;


namespace Planetshakers.Events.Models
{
    public class MailingListViewModel
    {
        public int Event_ID { get; set; }
        public List<int> FilterRegistrationType { get; set; }
        public List<int> FilterCampus { get; set; }
        public List<int> FilterRole { get; set; }
        public List<int> FilterGroup { get; set; } 
        public List<MailingListResult> ResultList { get; set; }

        //select Lists
        public List<SelectListItem> EventList
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = (from c in context.Events_Conference
                                orderby c.SortOrder descending
                                select new SelectListItem
                                {
                                    Text = c.ConferenceName,
                                    Value = c.Conference_ID.ToString().Trim()
                                }).ToList();

                    return list;
                }
            }
        }
        public List<SelectListItem> CampusList
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = (from c in context.Church_Campus
                                orderby c.SortOrder descending
                                select new SelectListItem
                                {
                                    Text = c.Name,
                                    Value = c.Campus_ID.ToString().Trim()
                                }).ToList();

                    return list;
                }
            }
        }
        public List<SelectListItem> RoleList
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = (from c in context.Events_TeamRole
                                orderby c.TeamRole descending
                                select new SelectListItem
                                {
                                    Text = c.TeamRole,
                                    Value = c.TeamRole_ID.ToString().Trim()
                                }).ToList();

                    return list;
                }
            }
        }

        //modal
        public DateTime scheduleTime
        {
            get
            {
                return DateTime.Now;
            }
        }

        public List<MailingListResult> generateList()
        {
            using (PlanetshakersEntities _ctx = new PlanetshakersEntities())
            {
                var list = new List<MailingListResult>();
                IQueryable<MailingListResult> getList;
                getList = (from c in _ctx.Common_Contact
                           select new MailingListResult()
                           {
                               Contact_ID = c.Contact_ID,
                               ContactName = c.FirstName + " " + c.LastName,
                               Email = c.Email,
                               Mobile = c.Mobile,
                               check = false,
                           });
                var contactList = (from r in _ctx.Events_Registration
                                   join rt in _ctx.Events_RegistrationType on r.RegistrationType_ID equals rt.RegistrationType_ID
                                   where rt.Conference_ID == Event_ID
                                   select r.Contact_ID).ToList();
                if (contactList != null)
                {
                    getList = getList.Where(x => contactList.Contains(x.Contact_ID));
                }

                if (FilterRegistrationType != null)
                {
                    contactList = (from r in _ctx.Events_Registration
                                   where FilterRegistrationType.Contains(r.RegistrationType_ID)
                                   select r.Contact_ID).ToList();
                    if (contactList != null)
                    {
                        getList = getList.Where(x => contactList.Contains(x.Contact_ID));
                    }
                }
                if (FilterCampus != null)
                {
                    var filterCampusName = (from c in _ctx.Church_Campus where FilterCampus.Contains(c.Campus_ID) select c.Name).ToList();
                    // get campus from application answers
                    contactList = (from a in _ctx.Volunteer_Application
                                   join rt in _ctx.Events_RegistrationType on a.RegistrationType_ID equals rt.RegistrationType_ID
                                   join aa in _ctx.Volunteer_ApplicationAnswer on a.Application_ID equals aa.Application_ID
                                   where rt.Conference_ID == Event_ID
                                        && aa.Question_ID == 7 && filterCampusName.Contains(aa.Answer)
                                   select a.Contact_ID).ToList();
                    if (contactList != null)
                    {
                        getList = getList.Where(x => contactList.Contains(x.Contact_ID));
                    }
                }
                if (FilterGroup != null)
                {
                    contactList = (from t in _ctx.Events_Team
                                   join tc in _ctx.Events_TeamContact on t.Team_ID equals tc.Team_ID
                                   where t.Event_ID == Event_ID
                                        && t.Inactive == false
                                        && tc.Inactive == false
                                        && FilterGroup.Contains(tc.Team_ID)
                                   select tc.Contact_ID).ToList();

                    if (contactList.Count == 0)
                    {
                        getList = Enumerable.Empty<MailingListResult>().AsQueryable();
                    }
                    else
                    {
                        getList = getList.Where(x => contactList.Contains(x.Contact_ID));
                    }
                }
                if (FilterRole != null)
                {
                    contactList = (from t in _ctx.Events_Team
                                   join tc in _ctx.Events_TeamContact on t.Team_ID equals tc.Team_ID
                                   join tr in _ctx.Events_TeamContactRole on tc.TeamContact_ID equals tr.TeamContact_ID
                                   where t.Event_ID == Event_ID
                                       && t.Inactive == false
                                       && tc.Inactive == false
                                       && tr.Active == true
                                       && FilterRole.Contains(tr.Role_ID)
                                   select tc.Contact_ID).ToList();
                    if (contactList.Count == 0)
                    {
                        getList = Enumerable.Empty<MailingListResult>().AsQueryable();
                    } else
                    {
                        getList = getList.Where(x => contactList.Contains(x.Contact_ID));
                    }
                }
                list = getList.Distinct().ToList();
                ResultList = list;
                return list;
            }
        }

    }

    public class EmailMailingListViewModel : MailingListViewModel
    {
        public List<SelectListItem> EmailList
        {
            get
            {
                var List = new List<SelectListItem>
                {
                    new SelectListItem() { Text = "info@believeglobal.org", Value = "info@believeglobal.org" }
                };

                return List;
            }
        }

        public void SendTestEmail(string Template_ID, string senderEmail, string Email)
        {
            _ = new MailController().SendMailViaPsApi(Email, Template_ID, null, senderEmail);
        }

        public void ScheduleEmails(List<MailingListResult> SendList, string Template_ID, string senderEmail, string campaignName, DateTime SendDateTime, int User_ID)
        {
            using (PlanetshakersEntities _ctx = new PlanetshakersEntities())
            {
                //create batch
                var batch = new Email_BatchLog()
                {
                    BatchCampaignName = campaignName,
                    CampaignDT = DateTime.Now
                };
                _ctx.Email_BatchLog.Add(batch);
                _ctx.SaveChanges();
                var Batch_ID = batch.Batch_ID;

                foreach (var item in SendList)
                {
                    var recipient = new Email_RecipientLog()
                    {
                        Contact_ID = item.Contact_ID,
                        SendgridTemplateID = Template_ID.Trim(),
                        DateTimeAdded = DateTime.Now,
                        DateTimeToSend = SendDateTime,
                        Sender_ID = User_ID,
                        Sent = false,
                        Cancelled = false,
                        Remarks = "",
                        SenderEmail = senderEmail,
                        Tags = null,
                        Batch_ID = Batch_ID
                    };
                    _ctx.Email_RecipientLog.Add(recipient);
                }
                _ctx.SaveChanges();
            }
        }
    }

    public class SmsMailingListViewModel : MailingListViewModel
    {
        private readonly string _accountSid = AzureServices.KeyVault_RetreiveSecret("believe-apps", "Twilio-accountSid");
        private readonly string _authToken = AzureServices.KeyVault_RetreiveSecret("believe-apps", "Twilio-authToken");

        public void SendTestSms(string messagetext, string Mobile)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            TwilioClient.Init(_accountSid, _authToken);

            if (Mobile.Trim() != "")
            {
                if (Mobile.Substring(0,1) == "0")
                {
                    Mobile = Mobile.Substring(1);
                }
                Mobile = "+61" + Mobile;
                var toNum = new Twilio.Types.PhoneNumber(Mobile);
                var fromNum = new Twilio.Types.PhoneNumber("+61447481999");
                var message = MessageResource.Create(toNum, from: fromNum, body: messagetext);
            }
        }

        public void ScheduleSms(List<MailingListResult> SendList, string message, DateTime SendDateTime, int User_ID)
        {
            using(PlanetshakersEntities _ctx = new PlanetshakersEntities())
            {
                var content = new Sms_ContentLog()
                {
                    SmsContent = message
                };
                _ctx.Sms_ContentLog.Add(content);
                _ctx.SaveChanges();
                var content_id = content.SmsContent_ID;

                foreach (var item in SendList)
                {
                    var recipient = new Sms_RecipientLog()
                    {
                        Contact_ID = item.Contact_ID,
                        SmsContent_ID = content_id,
                        DateTimeAdded = DateTime.Now,
                        DateTimeToSend = SendDateTime,
                        Sender_ID = User_ID,
                        Sent = false,
                        Cancelled = false,
                        Remarks = ""
                    };
                    _ctx.Sms_RecipientLog.Add(recipient);
                }
                _ctx.SaveChanges();
            }
        }
    }
    public class MailingListResult
    {
        public int Contact_ID { get; set; }
        public string ContactName { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public bool check { get; set; }

        #region override function
        public bool Equals(MailingListResult other)
        {
            if (
                    Contact_ID == other.Contact_ID 
                    && ContactName == other.ContactName
                    && Email == other.Email
                    && Mobile == other.Mobile
                )
                return true;

            return false;
        }

        public override int GetHashCode()
        {
            int hashContact_ID = Contact_ID.GetHashCode();
            int hashContactName = ContactName.GetHashCode();
            int hashEmail = Email.GetHashCode();
            int hashMobile = Mobile.GetHashCode();

            return hashContact_ID ^ hashContactName + hashEmail ^ hashMobile;
        }
        #endregion
    }
}