﻿using Planetshakers.Events.Security;

namespace Planetshakers.Events.Models
{
    public class MenuItemModel
    {
        public MenuItemModel()
        {
            Visible = true;
        }

        public string Name { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public string Roles { get; set; }
        public bool Visible { get; set; }
        public string TargetPanel { get; set; }

        public string[] RoleList
        {
            get
            {
                return Roles.Split(new[] { ',' });
            }
        }

        ///// <summary>
        ///// Indicates if current controller and action is active 
        ///// </summary>
        //public bool IsSelected
        //{
        //    get
        //    {
        //        return (ControllerName.ToUpper() == CurrentContext.Instance.Controller)
        //               && (ActionName.ToUpper() == CurrentContext.Instance.Action);
        //    }
        //}
    }
}