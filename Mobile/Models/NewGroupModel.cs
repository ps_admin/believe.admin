﻿using Planetshakers.Core.Models;

namespace Planetshakers.Events.Models
{
    public class NewGroupModel
    {
        public Group group { get; set; }
    
        public bool success { get; set; }

        public int Conference_ID { get; set; }
        public string message { get; set; }
    }
}