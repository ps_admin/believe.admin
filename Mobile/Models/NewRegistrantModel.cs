﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Planetshakers.Events.Models
{
    public class NewRegistrantModel
    {
        public Common_Contact common_contact { get; set; }
        public Common_ContactExternalEvents common_contactExternalEvent { get; set; }
        public bool success { get; set; }
        public List<SelectListItem> SalutationList
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = (from s in context.Common_Salutation
                                select new SelectListItem
                                {
                                    Text = s.Salutation,
                                    Value = s.Salutation_ID.ToString().Trim()
                                }).ToList();

                    return list;
                }
            }
        }

        public List<SelectListItem> GenderList
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = new List<SelectListItem> {
                            new SelectListItem { Text = "Male", Value = "Male" },
                            new SelectListItem { Text = "Female", Value = "Female" }
                        };

                    return list;
                }
            }
        }

        public List<SelectListItem> CampusList
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = (from c in context.Church_Campus
                                select new SelectListItem
                                {
                                    Text = c.Name,
                                    Value = c.Campus_ID.ToString().Trim()
                                }).ToList();

                    return list;
                }
            }
        }

        public List<SelectListItem> StateList
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = (from s in context.Common_GeneralState
                                where s.State != "Google"
                                select new SelectListItem
                                {
                                    Text = s.State,
                                    Value = s.State_ID.ToString().Trim()
                                }).ToList();

                    return list;
                }
            }
        }

        public List<SelectListItem> CountryList
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = (from s in context.Common_GeneralCountry
                                select new SelectListItem
                                {
                                    Text = s.Country,
                                    Value = s.Country_ID.ToString().Trim()
                                }).ToList();

                    return list;
                }
            }
        }

        public static List<SelectListItem> SuburbList
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = (from m in context.Common_AustralianSuburb
                                select new SelectListItem
                                {
                                    Text = m.Suburb,
                                    Value = m.Suburb
                                }).ToList();

                    return list;
                }
            }
        }


    }
}