﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Planetshakers.Core.Models;

namespace Planetshakers.Events.Models
{

    public class PaymentDetailModel
    {
        public Payment payment { get; set; }
        public bool isAdmin { get; set; }
        public bool refunded { get; set; }
        public bool intentNotFound { get; set; }
    }
}