﻿using System.Collections.Generic;

namespace Planetshakers.Events.Models
{
    public class PaymentReportViewModel
    {
        public int Event_ID { get; set; }
        public List<PaymentReportInstance> report { get; set; }
    }

    public class PaymentReportInstance
    {
        public int Contact_ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public double TotalCost { get; set; }
        public decimal TotalPaid { get; set; }
        public double OutstandingAmount { get; set; }
    }

    public class PaymentReportGroup
    {
        public int Contact_ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public decimal PaidAmount { get; set; }
    }
}