﻿using System.Collections.Generic;

namespace Planetshakers.Events.Models
{

    public class PaymentSummaryViewModel
    {
        public List<PaymentSummaryBucket> buckets { get; set; }
    }

    public class PaymentSummaryBucket
    {
        public string Name { get; set; }
        public int count { get; set; }
        public int minValue { get; set; }
        public int maxValue { get; set; }
    }
}