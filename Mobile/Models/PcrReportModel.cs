﻿using System;
using System.Linq;
using System.Collections.Generic;
using Planetshakers.Core.Models;
using Planetshakers.Events.Security;

namespace Planetshakers.Events.Models
{
    public class PcrReportModel
    {
        public int PCRId { get; set; }
        public double TotalOffering { get; set; }
        public int TotalVisitor { get; set; }
        public int TotalChildren { get; set; }
        public string Comments { get; set; }
        public bool MarkAsCompleted { get; set; }
    }

}