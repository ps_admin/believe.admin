
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, and Azure
-- --------------------------------------------------
-- Date Created: 01/05/2017 14:01:35
-- Generated from EDMX file: F:\BSL\planetshakers-planetshakers.church.mobile-8ae8fa292d91\Mobile\Models\PlanetshakersEntity.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [DEV_INDIA_Planetshakers];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK__Common_Co__Conta__519D5005]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Common_CombinedContactMailingList] DROP CONSTRAINT [FK__Common_Co__Conta__519D5005];
GO
IF OBJECT_ID(N'[dbo].[FK__Common_Co__Maili__5291743E]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Common_CombinedContactMailingList] DROP CONSTRAINT [FK__Common_Co__Maili__5291743E];
GO
IF OBJECT_ID(N'[dbo].[FK__Common_Co__Maili__53859877]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Common_CombinedContactMailingList] DROP CONSTRAINT [FK__Common_Co__Maili__53859877];
GO
IF OBJECT_ID(N'[dbo].[FK_Church_CarersAccess_Common_ContactInternal]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Church_CarersAccess] DROP CONSTRAINT [FK_Church_CarersAccess_Common_ContactInternal];
GO
IF OBJECT_ID(N'[dbo].[FK_Church_CarersAccess_Common_ContactInternal1]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Church_CarersAccess] DROP CONSTRAINT [FK_Church_CarersAccess_Common_ContactInternal1];
GO
IF OBJECT_ID(N'[dbo].[FK_Church_ContactComment_Common_ContactInternal]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Church_ContactComment] DROP CONSTRAINT [FK_Church_ContactComment_Common_ContactInternal];
GO
IF OBJECT_ID(N'[dbo].[FK_Church_ContactComment_Common_UserDetail]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Church_ContactComment] DROP CONSTRAINT [FK_Church_ContactComment_Common_UserDetail];
GO
IF OBJECT_ID(N'[dbo].[FK_Church_ContactCourseEnrollment_Church_CourseInstance]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Church_ContactCourseEnrolment] DROP CONSTRAINT [FK_Church_ContactCourseEnrollment_Church_CourseInstance];
GO
IF OBJECT_ID(N'[dbo].[FK_Church_ContactCourseEnrollment_Common_ContactInternal]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Church_ContactCourseEnrolment] DROP CONSTRAINT [FK_Church_ContactCourseEnrollment_Common_ContactInternal];
GO
IF OBJECT_ID(N'[dbo].[FK_Church_ContactCourseInstance_Church_CourseInstance]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Church_ContactCourseInstance] DROP CONSTRAINT [FK_Church_ContactCourseInstance_Church_CourseInstance];
GO
IF OBJECT_ID(N'[dbo].[FK_Church_ContactCourseInstance_Common_ContactInternal]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Church_ContactCourseInstance] DROP CONSTRAINT [FK_Church_ContactCourseInstance_Common_ContactInternal];
GO
IF OBJECT_ID(N'[dbo].[FK_Church_ContactRole_Church_Role]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Church_ContactRole] DROP CONSTRAINT [FK_Church_ContactRole_Church_Role];
GO
IF OBJECT_ID(N'[dbo].[FK_Church_ContactRole_Common_ContactInternal]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Church_ContactRole] DROP CONSTRAINT [FK_Church_ContactRole_Common_ContactInternal];
GO
IF OBJECT_ID(N'[dbo].[FK_Church_ContactULG_Common_ContactInternal]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Church_ULGContact] DROP CONSTRAINT [FK_Church_ContactULG_Common_ContactInternal];
GO
IF OBJECT_ID(N'[dbo].[FK_Church_CourseInstance_Church_Course]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Church_CourseInstance] DROP CONSTRAINT [FK_Church_CourseInstance_Church_Course];
GO
IF OBJECT_ID(N'[dbo].[FK_Church_CourseInstanceCampus_Church_Campus]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Church_CourseInstanceCampus] DROP CONSTRAINT [FK_Church_CourseInstanceCampus_Church_Campus];
GO
IF OBJECT_ID(N'[dbo].[FK_Church_CourseInstanceCampus_Church_CourseInstance]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Church_CourseInstanceCampus] DROP CONSTRAINT [FK_Church_CourseInstanceCampus_Church_CourseInstance];
GO
IF OBJECT_ID(N'[dbo].[FK_Church_NPNC_Common_ContactInternal]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Church_NPNC] DROP CONSTRAINT [FK_Church_NPNC_Common_ContactInternal];
GO
IF OBJECT_ID(N'[dbo].[FK_Church_NPNC_Common_UserDetail]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Church_NPNC] DROP CONSTRAINT [FK_Church_NPNC_Common_UserDetail];
GO
IF OBJECT_ID(N'[dbo].[FK_Church_RegionalPastor_Church_Campus]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Church_RegionalPastor] DROP CONSTRAINT [FK_Church_RegionalPastor_Church_Campus];
GO
IF OBJECT_ID(N'[dbo].[FK_Church_RegionalPastor_Church_Region]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Church_RegionalPastor] DROP CONSTRAINT [FK_Church_RegionalPastor_Church_Region];
GO
IF OBJECT_ID(N'[dbo].[FK_Church_RestrictedAccessULG_Church_ContactRole]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Church_RestrictedAccessULG] DROP CONSTRAINT [FK_Church_RestrictedAccessULG_Church_ContactRole];
GO
IF OBJECT_ID(N'[dbo].[FK_Church_RestrictedAccessULG_Church_ULG]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Church_RestrictedAccessULG] DROP CONSTRAINT [FK_Church_RestrictedAccessULG_Church_ULG];
GO
IF OBJECT_ID(N'[dbo].[FK_Church_Role_Church_RoleType]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Church_Role] DROP CONSTRAINT [FK_Church_Role_Church_RoleType];
GO
IF OBJECT_ID(N'[dbo].[FK_Church_Role_Church_RoleTypeUL]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Church_Role] DROP CONSTRAINT [FK_Church_Role_Church_RoleTypeUL];
GO
IF OBJECT_ID(N'[dbo].[FK_Church_ULGContact_Church_ULG]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Church_ULGContact] DROP CONSTRAINT [FK_Church_ULGContact_Church_ULG];
GO
IF OBJECT_ID(N'[dbo].[FK_Common_Contact_Common_Salutation]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Common_Contact] DROP CONSTRAINT [FK_Common_Contact_Common_Salutation];
GO
IF OBJECT_ID(N'[dbo].[FK_Common_Contact_Common_UserDetail]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Common_Contact] DROP CONSTRAINT [FK_Common_Contact_Common_UserDetail];
GO
IF OBJECT_ID(N'[dbo].[FK_Common_ContactDatabaseRole_Common_UserDetail]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Common_ContactDatabaseRole] DROP CONSTRAINT [FK_Common_ContactDatabaseRole_Common_UserDetail];
GO
IF OBJECT_ID(N'[dbo].[FK_Common_ContactExternal_Common_GeneralCountry]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Common_ContactExternal] DROP CONSTRAINT [FK_Common_ContactExternal_Common_GeneralCountry];
GO
IF OBJECT_ID(N'[dbo].[FK_Common_ContactExternal_Common_GeneralState]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Common_ContactExternal] DROP CONSTRAINT [FK_Common_ContactExternal_Common_GeneralState];
GO
IF OBJECT_ID(N'[dbo].[FK_Common_ContactExternalEvents_Common_ContactExternal]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Common_ContactExternalEvents] DROP CONSTRAINT [FK_Common_ContactExternalEvents_Common_ContactExternal];
GO
IF OBJECT_ID(N'[dbo].[FK_Common_ContactInternal_Church_Campus]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Common_ContactInternal] DROP CONSTRAINT [FK_Common_ContactInternal_Church_Campus];
GO
IF OBJECT_ID(N'[dbo].[FK_Common_ContactInternal_Church_Ministry]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Common_ContactInternal] DROP CONSTRAINT [FK_Common_ContactInternal_Church_Ministry];
GO
IF OBJECT_ID(N'[dbo].[FK_Common_ContactInternal_Church_Region]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Common_ContactInternal] DROP CONSTRAINT [FK_Common_ContactInternal_Church_Region];
GO
IF OBJECT_ID(N'[dbo].[FK_Common_ContactInternal_Common_Contact]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Common_ContactInternal] DROP CONSTRAINT [FK_Common_ContactInternal_Common_Contact];
GO
IF OBJECT_ID(N'[dbo].[FK_Common_ContactMailingList_Common_Contact]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Common_ContactMailingList] DROP CONSTRAINT [FK_Common_ContactMailingList_Common_Contact];
GO
IF OBJECT_ID(N'[dbo].[FK_Common_ContactMailingList_Common_MailingList]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Common_ContactMailingList] DROP CONSTRAINT [FK_Common_ContactMailingList_Common_MailingList];
GO
IF OBJECT_ID(N'[dbo].[FK_Common_UserDetail_Common_Contact]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Common_UserDetail] DROP CONSTRAINT [FK_Common_UserDetail_Common_Contact];
GO
IF OBJECT_ID(N'[dbo].[FK_ContactExternal_Contact]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Common_ContactExternal] DROP CONSTRAINT [FK_ContactExternal_Contact];
GO
IF OBJECT_ID(N'[dbo].[FK_GeneralState_GeneralCountry]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Common_GeneralState] DROP CONSTRAINT [FK_GeneralState_GeneralCountry];
GO
IF OBJECT_ID(N'[dbo].[fk_Volunteer_Application_Church_Campus]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Volunteer_Application] DROP CONSTRAINT [fk_Volunteer_Application_Church_Campus];
GO
IF OBJECT_ID(N'[dbo].[fk_Volunteer_Application_Church_Ministry]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Volunteer_Application] DROP CONSTRAINT [fk_Volunteer_Application_Church_Ministry];
GO
IF OBJECT_ID(N'[dbo].[fk_Volunteer_Application_Church_Region]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Volunteer_Application] DROP CONSTRAINT [fk_Volunteer_Application_Church_Region];
GO
IF OBJECT_ID(N'[dbo].[fk_Volunteer_Application_Church_ULG]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Volunteer_Application] DROP CONSTRAINT [fk_Volunteer_Application_Church_ULG];
GO
IF OBJECT_ID(N'[dbo].[fk_Volunteer_Application_Common_Contact]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Volunteer_Application] DROP CONSTRAINT [fk_Volunteer_Application_Common_Contact];
GO
IF OBJECT_ID(N'[dbo].[fk_Volunteer_Application_Common_Contact_ActionedBy]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Volunteer_Application] DROP CONSTRAINT [fk_Volunteer_Application_Common_Contact_ActionedBy];
GO
IF OBJECT_ID(N'[dbo].[fk_Volunteer_Application_Volunteer_Area]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Volunteer_Application] DROP CONSTRAINT [fk_Volunteer_Application_Volunteer_Area];
GO
IF OBJECT_ID(N'[dbo].[fk_Volunteer_ApplicationAnswer_Volunteer_Application]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Volunteer_ApplicationAnswer] DROP CONSTRAINT [fk_Volunteer_ApplicationAnswer_Volunteer_Application];
GO
IF OBJECT_ID(N'[dbo].[fk_Volunteer_ApplicationAnswer_Volunteer_ApplicationQuestion]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Volunteer_ApplicationAnswer] DROP CONSTRAINT [fk_Volunteer_ApplicationAnswer_Volunteer_ApplicationQuestion];
GO
IF OBJECT_ID(N'[dbo].[fk_Volunteer_ApplicationApproval_Common_Contact]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Volunteer_ApplicationApproval] DROP CONSTRAINT [fk_Volunteer_ApplicationApproval_Common_Contact];
GO
IF OBJECT_ID(N'[dbo].[fk_Volunteer_ApplicationApproval_Volunteer_Application]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Volunteer_ApplicationApproval] DROP CONSTRAINT [fk_Volunteer_ApplicationApproval_Volunteer_Application];
GO
IF OBJECT_ID(N'[dbo].[fk_Volunteer_ApplicationApproval_Volunteer_ApplicationApprovalType]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Volunteer_ApplicationApproval] DROP CONSTRAINT [fk_Volunteer_ApplicationApproval_Volunteer_ApplicationApprovalType];
GO
IF OBJECT_ID(N'[dbo].[fk_Volunteer_ApplicationApprover_Church_Campus]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Volunteer_ApplicationApprover] DROP CONSTRAINT [fk_Volunteer_ApplicationApprover_Church_Campus];
GO
IF OBJECT_ID(N'[dbo].[fk_Volunteer_ApplicationApprover_Church_Ministry]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Volunteer_ApplicationApprover] DROP CONSTRAINT [fk_Volunteer_ApplicationApprover_Church_Ministry];
GO
IF OBJECT_ID(N'[dbo].[fk_Volunteer_ApplicationApprover_Church_Region]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Volunteer_ApplicationApprover] DROP CONSTRAINT [fk_Volunteer_ApplicationApprover_Church_Region];
GO
IF OBJECT_ID(N'[dbo].[fk_Volunteer_ApplicationApprover_Common_Contact]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Volunteer_ApplicationApprover] DROP CONSTRAINT [fk_Volunteer_ApplicationApprover_Common_Contact];
GO
IF OBJECT_ID(N'[dbo].[fk_Volunteer_ApplicationApprover_Volunteer_ApplicationApprovalType]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Volunteer_ApplicationApprover] DROP CONSTRAINT [fk_Volunteer_ApplicationApprover_Volunteer_ApplicationApprovalType];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Church_Campus]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Church_Campus];
GO
IF OBJECT_ID(N'[dbo].[Church_CarersAccess]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Church_CarersAccess];
GO
IF OBJECT_ID(N'[dbo].[Church_ChurchStatus]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Church_ChurchStatus];
GO
IF OBJECT_ID(N'[dbo].[Church_ContactComment]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Church_ContactComment];
GO
IF OBJECT_ID(N'[dbo].[Church_ContactCourseEnrolment]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Church_ContactCourseEnrolment];
GO
IF OBJECT_ID(N'[dbo].[Church_ContactCourseInstance]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Church_ContactCourseInstance];
GO
IF OBJECT_ID(N'[dbo].[Church_ContactRole]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Church_ContactRole];
GO
IF OBJECT_ID(N'[dbo].[Church_Course]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Church_Course];
GO
IF OBJECT_ID(N'[dbo].[Church_CourseInstance]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Church_CourseInstance];
GO
IF OBJECT_ID(N'[dbo].[Church_CourseInstanceCampus]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Church_CourseInstanceCampus];
GO
IF OBJECT_ID(N'[dbo].[Church_Ministry]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Church_Ministry];
GO
IF OBJECT_ID(N'[dbo].[Church_NPNC]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Church_NPNC];
GO
IF OBJECT_ID(N'[dbo].[Church_PCRDate]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Church_PCRDate];
GO
IF OBJECT_ID(N'[dbo].[Church_Region]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Church_Region];
GO
IF OBJECT_ID(N'[dbo].[Church_RegionalPastor]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Church_RegionalPastor];
GO
IF OBJECT_ID(N'[dbo].[Church_RestrictedAccessULG]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Church_RestrictedAccessULG];
GO
IF OBJECT_ID(N'[dbo].[Church_Role]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Church_Role];
GO
IF OBJECT_ID(N'[dbo].[Church_RoleType]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Church_RoleType];
GO
IF OBJECT_ID(N'[dbo].[Church_RoleTypeUL]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Church_RoleTypeUL];
GO
IF OBJECT_ID(N'[dbo].[Church_ULG]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Church_ULG];
GO
IF OBJECT_ID(N'[dbo].[Church_ULGContact]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Church_ULGContact];
GO
IF OBJECT_ID(N'[dbo].[Common_CombinedContactMailingList]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Common_CombinedContactMailingList];
GO
IF OBJECT_ID(N'[dbo].[Common_Contact]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Common_Contact];
GO
IF OBJECT_ID(N'[dbo].[Common_ContactDatabaseRole]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Common_ContactDatabaseRole];
GO
IF OBJECT_ID(N'[dbo].[Common_ContactExternal]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Common_ContactExternal];
GO
IF OBJECT_ID(N'[dbo].[Common_ContactExternalEvents]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Common_ContactExternalEvents];
GO
IF OBJECT_ID(N'[dbo].[Common_ContactInternal]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Common_ContactInternal];
GO
IF OBJECT_ID(N'[dbo].[Common_ContactMailingList]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Common_ContactMailingList];
GO
IF OBJECT_ID(N'[PlanetshakersModelStoreContainer].[Common_DatabaseInstance]', 'U') IS NOT NULL
    DROP TABLE [PlanetshakersModelStoreContainer].[Common_DatabaseInstance];
GO
IF OBJECT_ID(N'[dbo].[Common_GeneralCountry]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Common_GeneralCountry];
GO
IF OBJECT_ID(N'[dbo].[Common_GeneralState]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Common_GeneralState];
GO
IF OBJECT_ID(N'[dbo].[Common_MailingList]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Common_MailingList];
GO
IF OBJECT_ID(N'[dbo].[Common_MailingListType]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Common_MailingListType];
GO
IF OBJECT_ID(N'[dbo].[Common_Salutation]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Common_Salutation];
GO
IF OBJECT_ID(N'[dbo].[Common_UserDetail]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Common_UserDetail];
GO
IF OBJECT_ID(N'[dbo].[Volunteer_Application]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Volunteer_Application];
GO
IF OBJECT_ID(N'[dbo].[Volunteer_ApplicationAnswer]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Volunteer_ApplicationAnswer];
GO
IF OBJECT_ID(N'[dbo].[Volunteer_ApplicationApproval]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Volunteer_ApplicationApproval];
GO
IF OBJECT_ID(N'[dbo].[Volunteer_ApplicationApprovalType]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Volunteer_ApplicationApprovalType];
GO
IF OBJECT_ID(N'[dbo].[Volunteer_ApplicationApprover]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Volunteer_ApplicationApprover];
GO
IF OBJECT_ID(N'[dbo].[Volunteer_ApplicationQuestion]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Volunteer_ApplicationQuestion];
GO
IF OBJECT_ID(N'[dbo].[Volunteer_Area]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Volunteer_Area];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Church_ContactRole'
CREATE TABLE [dbo].[Church_ContactRole] (
    [ContactRole_ID] int IDENTITY(1,1) NOT NULL,
    [Contact_ID] int  NOT NULL,
    [Role_ID] int  NOT NULL,
    [Campus_ID] int  NOT NULL,
    [DateAdded] datetime  NOT NULL
);
GO

-- Creating table 'Church_NPNC'
CREATE TABLE [dbo].[Church_NPNC] (
    [NPNC_ID] int IDENTITY(1,1) NOT NULL,
    [Contact_ID] int  NOT NULL,
    [NPNCType_ID] int  NOT NULL,
    [FirstContactDate] datetime  NOT NULL,
    [FirstFollowupCallDate] datetime  NULL,
    [FirstFollowupVisitDate] datetime  NULL,
    [InitialStatus_ID] int  NOT NULL,
    [NCDecisionType_ID] int  NULL,
    [NCTeamMember_ID] int  NULL,
    [CampusDecision_ID] int  NULL,
    [Campus_ID] int  NULL,
    [Ministry_ID] int  NULL,
    [Region_ID] int  NULL,
    [ULG_ID] int  NULL,
    [ULGDateOfIssue] datetime  NULL,
    [Carer_ID] int  NULL,
    [OutcomeStatus_ID] int  NULL,
    [OutcomeDate] datetime  NULL,
    [OutcomeInactive_ID] int  NULL,
    [GUID] uniqueidentifier  NOT NULL
);
GO

-- Creating table 'Church_Region'
CREATE TABLE [dbo].[Church_Region] (
    [Region_ID] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(50)  NOT NULL,
    [SortOrder] int  NOT NULL,
    [Deleted] bit  NOT NULL
);
GO

-- Creating table 'Church_RegionalPastor'
CREATE TABLE [dbo].[Church_RegionalPastor] (
    [RegionalPastor_ID] int IDENTITY(1,1) NOT NULL,
    [Campus_ID] int  NOT NULL,
    [Ministry_ID] int  NOT NULL,
    [Region_ID] int  NULL,
    [Contact_ID] int  NOT NULL
);
GO

-- Creating table 'Church_RestrictedAccessULG'
CREATE TABLE [dbo].[Church_RestrictedAccessULG] (
    [RestrictedAccessULG_ID] int IDENTITY(1,1) NOT NULL,
    [ContactRole_ID] int  NOT NULL,
    [ULG_ID] int  NOT NULL
);
GO

-- Creating table 'Church_Role'
CREATE TABLE [dbo].[Church_Role] (
    [Role_ID] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(50)  NOT NULL,
    [RoleType_ID] int  NOT NULL,
    [RoleTypeUL_ID] int  NOT NULL,
    [Deleted] bit  NOT NULL
);
GO

-- Creating table 'Church_RoleType'
CREATE TABLE [dbo].[Church_RoleType] (
    [RoleType_ID] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(50)  NOT NULL,
    [SortOrder] int  NOT NULL
);
GO

-- Creating table 'Church_RoleTypeUL'
CREATE TABLE [dbo].[Church_RoleTypeUL] (
    [RoleTypeUL_ID] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(50)  NOT NULL,
    [SortOrder] int  NOT NULL
);
GO

-- Creating table 'Church_ULG'
CREATE TABLE [dbo].[Church_ULG] (
    [ULG_ID] int IDENTITY(1,1) NOT NULL,
    [Code] nvarchar(20)  NOT NULL,
    [Name] nvarchar(50)  NOT NULL,
    [GroupType] nvarchar(50)  NOT NULL,
    [Campus_ID] int  NOT NULL,
    [Ministry_ID] int  NOT NULL,
    [Region_ID] int  NULL,
    [ActiveDate] datetime  NOT NULL,
    [ULGDateTime_ID] int  NULL,
    [ULGDateDay_ID] int  NULL,
    [Address1] nvarchar(80)  NOT NULL,
    [Address2] nvarchar(80)  NOT NULL,
    [Suburb] nvarchar(50)  NOT NULL,
    [State_ID] int  NOT NULL,
    [Postcode] nvarchar(20)  NOT NULL,
    [Country_ID] int  NOT NULL,
    [Inactive] bit  NOT NULL,
    [SortOrder] int  NOT NULL
);
GO

-- Creating table 'Church_ULGContact'
CREATE TABLE [dbo].[Church_ULGContact] (
    [ULGContact_ID] int IDENTITY(1,1) NOT NULL,
    [ULG_ID] int  NOT NULL,
    [Contact_ID] int  NOT NULL,
    [DateJoined] datetime  NOT NULL,
    [Inactive] bit  NOT NULL,
    [SortOrder] int  NOT NULL,
    [GUID] uniqueidentifier  NOT NULL
);
GO

-- Creating table 'Common_ContactInternal'
CREATE TABLE [dbo].[Common_ContactInternal] (
    [Contact_ID] int  NOT NULL,
    [ChurchStatus_ID] int  NOT NULL,
    [Campus_ID] int  NULL,
    [Ministry_ID] int  NULL,
    [Region_ID] int  NULL,
    [PostalAddress1] nvarchar(100)  NOT NULL,
    [PostalAddress2] nvarchar(100)  NOT NULL,
    [PostalSuburb] nvarchar(20)  NOT NULL,
    [PostalPostcode] nvarchar(50)  NOT NULL,
    [PostalState_ID] int  NULL,
    [PostalStateOther] nvarchar(20)  NOT NULL,
    [PostalCountry_ID] int  NULL,
    [Photo] varbinary(max)  NULL,
    [EntryPoint_ID] int  NULL,
    [DateJoinedChurch] datetime  NULL,
    [CovenantFormDate] datetime  NULL,
    [SalvationDate] datetime  NULL,
    [WaterBaptismDate] datetime  NULL,
    [VolunteerFormDate] datetime  NULL,
    [PrimaryCarer] nvarchar(50)  NOT NULL,
    [CountryOfOrigin_ID] int  NULL,
    [PrivacyNumber] nvarchar(50)  NOT NULL,
    [PrimarySchool] nvarchar(100)  NOT NULL,
    [SchoolGrade_ID] int  NULL,
    [HighSchool] nvarchar(100)  NOT NULL,
    [University] nvarchar(100)  NOT NULL,
    [InformationIsConfidential] bit  NOT NULL,
    [FacebookAddress] nvarchar(100)  NOT NULL,
    [Education_ID] int  NULL,
    [ModeOfTransport_ID] int  NULL,
    [CarParkUsed_ID] int  NULL,
    [ServiceAttending_ID] int  NULL,
    [Occupation] nvarchar(100)  NOT NULL,
    [Employer] nvarchar(50)  NOT NULL,
    [EmploymentStatus_ID] int  NULL,
    [EmploymentPosition_ID] int  NULL,
    [EmploymentIndustry_ID] int  NULL,
    [KidsMinistryComments] nvarchar(max)  NOT NULL,
    [KidsMinistryGroup] nvarchar(50)  NOT NULL,
    [Deleted] bit  NOT NULL,
    [DeleteReason_ID] int  NULL
);
GO

-- Creating table 'Church_CarersAccess'
CREATE TABLE [dbo].[Church_CarersAccess] (
    [CarersAccess_ID] int IDENTITY(1,1) NOT NULL,
    [Contact_ID] int  NOT NULL,
    [Carer_ID] int  NOT NULL,
    [DateAllocated] datetime  NOT NULL,
    [DateInactive] datetime  NULL,
    [Inactive] bit  NOT NULL
);
GO

-- Creating table 'Church_ContactComment'
CREATE TABLE [dbo].[Church_ContactComment] (
    [Comment_ID] int IDENTITY(1,1) NOT NULL,
    [Contact_ID] int  NOT NULL,
    [CommentBy_ID] int  NOT NULL,
    [CommentDate] datetime  NOT NULL,
    [Comment] nvarchar(max)  NOT NULL,
    [CommentType_ID] int  NOT NULL,
    [CommentSource_ID] int  NOT NULL,
    [NPNC_ID] int  NULL,
    [InReplyToComment_ID] int  NULL,
    [Actioned] bit  NOT NULL,
    [ActionedBy_ID] int  NULL,
    [ActionedDate] datetime  NULL
);
GO

-- Creating table 'Church_PCRDate'
CREATE TABLE [dbo].[Church_PCRDate] (
    [PCRDate_ID] int IDENTITY(1,1) NOT NULL,
    [Date] datetime  NOT NULL,
    [ULG] bit  NOT NULL,
    [Boom] bit  NOT NULL,
    [FNL] bit  NOT NULL,
    [ReportIsDue] bit  NOT NULL,
    [Deleted] bit  NOT NULL
);
GO

-- Creating table 'Church_ChurchStatus'
CREATE TABLE [dbo].[Church_ChurchStatus] (
    [ChurchStatus_ID] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(50)  NOT NULL,
    [RequestReasonWhenChangingTo] bit  NOT NULL,
    [SortOrder] int  NOT NULL,
    [Deleted] bit  NOT NULL
);
GO

-- Creating table 'Common_ContactDatabaseRole'
CREATE TABLE [dbo].[Common_ContactDatabaseRole] (
    [ContactDatabaseRole_ID] int IDENTITY(1,1) NOT NULL,
    [Contact_ID] int  NOT NULL,
    [DatabaseRole_ID] int  NOT NULL,
    [DateAdded] datetime  NOT NULL
);
GO

-- Creating table 'Common_UserDetail'
CREATE TABLE [dbo].[Common_UserDetail] (
    [Contact_ID] int  NOT NULL,
    [LastPasswordChangeDate] datetime  NULL,
    [InvalidPasswordAttempts] int  NOT NULL,
    [PasswordResetRequired] bit  NOT NULL,
    [Inactive] bit  NOT NULL
);
GO

-- Creating table 'Common_GeneralCountry'
CREATE TABLE [dbo].[Common_GeneralCountry] (
    [Country_ID] int IDENTITY(1,1) NOT NULL,
    [Country] varchar(50)  NOT NULL,
    [Currency] varchar(3)  NOT NULL,
    [Numeric] char(3)  NOT NULL,
    [Alpha_3] char(3)  NOT NULL,
    [Alpha_2] char(2)  NOT NULL,
    [SortOrder] int  NOT NULL
);
GO

-- Creating table 'Common_GeneralState'
CREATE TABLE [dbo].[Common_GeneralState] (
    [State_ID] int IDENTITY(1,1) NOT NULL,
    [State] varchar(10)  NOT NULL,
    [FullName] varchar(50)  NOT NULL,
    [Country_ID] int  NOT NULL,
    [SortOrder] int  NOT NULL
);
GO

-- Creating table 'Common_Salutation'
CREATE TABLE [dbo].[Common_Salutation] (
    [Salutation_ID] int IDENTITY(1,1) NOT NULL,
    [Salutation] varchar(20)  NOT NULL,
    [SortOrder] int  NOT NULL
);
GO

-- Creating table 'Church_ContactCourseEnrolment'
CREATE TABLE [dbo].[Church_ContactCourseEnrolment] (
    [ContactCourseEnrolment_ID] int IDENTITY(1,1) NOT NULL,
    [Contact_ID] int  NOT NULL,
    [CourseInstance_ID] int  NOT NULL,
    [EnrolmentDate] datetime  NOT NULL,
    [Comments] nvarchar(max)  NOT NULL,
    [IntroductoryEmailSentDate] datetime  NULL,
    [Completed] bit  NOT NULL
);
GO

-- Creating table 'Church_CourseInstance'
CREATE TABLE [dbo].[Church_CourseInstance] (
    [CourseInstance_ID] int IDENTITY(1,1) NOT NULL,
    [Course_ID] int  NOT NULL,
    [Name] nvarchar(50)  NOT NULL,
    [StartDate] datetime  NULL,
    [Deleted] bit  NOT NULL
);
GO

-- Creating table 'Church_CourseInstanceCampus'
CREATE TABLE [dbo].[Church_CourseInstanceCampus] (
    [CourseInstanceCampus_ID] int IDENTITY(1,1) NOT NULL,
    [CourseInstance_ID] int  NOT NULL,
    [Campus_ID] int  NOT NULL
);
GO

-- Creating table 'Church_Course'
CREATE TABLE [dbo].[Church_Course] (
    [Course_ID] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(50)  NOT NULL,
    [PointsRequiredForCompletion] int  NOT NULL,
    [ShowInCustomReport] bit  NOT NULL,
    [LogCompletedSession] bit  NOT NULL,
    [Deleted] bit  NOT NULL,
    [SortOrder] int  NOT NULL
);
GO

-- Creating table 'Church_Campus'
CREATE TABLE [dbo].[Church_Campus] (
    [Campus_ID] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(50)  NOT NULL,
    [ShortName] nvarchar(10)  NOT NULL,
    [SortOrder] int  NOT NULL
);
GO

-- Creating table 'Common_MailingList'
CREATE TABLE [dbo].[Common_MailingList] (
    [MailingList_ID] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(30)  NOT NULL,
    [Description] nvarchar(50)  NOT NULL,
    [Database] nvarchar(50)  NOT NULL,
    [TickedByDefault] bit  NOT NULL,
    [TickedForCampus_ID] int  NULL,
    [SortOrder] int  NOT NULL
);
GO

-- Creating table 'Common_MailingListType'
CREATE TABLE [dbo].[Common_MailingListType] (
    [MailingListType_ID] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'Common_ContactMailingList'
CREATE TABLE [dbo].[Common_ContactMailingList] (
    [ContactMailingList_ID] int IDENTITY(1,1) NOT NULL,
    [Contact_ID] int  NOT NULL,
    [MailingList_ID] int  NOT NULL,
    [SubscriptionActive] bit  NOT NULL
);
GO

-- Creating table 'Common_CombinedContactMailingList'
CREATE TABLE [dbo].[Common_CombinedContactMailingList] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [Contact_ID] int  NULL,
    [MailingList_ID] int  NULL,
    [MailingListType_ID] int  NULL,
    [SubscriptionActive] bit  NOT NULL
);
GO

-- Creating table 'Common_Contact'
CREATE TABLE [dbo].[Common_Contact] (
    [Contact_ID] int IDENTITY(1,1) NOT NULL,
    [Salutation_ID] int  NOT NULL,
    [FirstName] nvarchar(30)  NOT NULL,
    [LastName] nvarchar(30)  NOT NULL,
    [Gender] nvarchar(6)  NOT NULL,
    [DateOfBirth] datetime  NULL,
    [Address1] nvarchar(100)  NOT NULL,
    [Address2] nvarchar(100)  NOT NULL,
    [Suburb] nvarchar(20)  NOT NULL,
    [Postcode] nvarchar(10)  NOT NULL,
    [State_ID] int  NULL,
    [StateOther] nvarchar(20)  NOT NULL,
    [Country_ID] int  NULL,
    [Phone] nvarchar(20)  NOT NULL,
    [PhoneWork] nvarchar(20)  NOT NULL,
    [Fax] nvarchar(20)  NOT NULL,
    [Mobile] nvarchar(20)  NOT NULL,
    [Email] nvarchar(80)  NOT NULL,
    [Email2] nvarchar(80)  NOT NULL,
    [DoNotIncludeEmail1InMailingList] bit  NOT NULL,
    [DoNotIncludeEmail2InMailingList] bit  NOT NULL,
    [Family_ID] int  NULL,
    [FamilyMemberType_ID] int  NULL,
    [Password] nvarchar(20)  NOT NULL,
    [SecretQuestion_ID] int  NULL,
    [SecretAnswer] nvarchar(100)  NULL,
    [VolunteerPoliceCheck] nvarchar(20)  NOT NULL,
    [VolunteerPoliceCheckBit] bit  NOT NULL,
    [VolunteerPoliceCheckDate] datetime  NULL,
    [VolunteerWWCC] nvarchar(20)  NOT NULL,
    [VolunteerWWCCDate] datetime  NULL,
    [VolunteerWWCCType_ID] int  NULL,
    [CreatedBy_ID] int  NULL,
    [CreationDate] datetime  NOT NULL,
    [ModifiedBy_ID] int  NULL,
    [ModificationDate] datetime  NULL,
    [DeDupeVerified] bit  NOT NULL,
    [GUID] uniqueidentifier  NOT NULL,
    [MaritalStatus_ID] int  NULL,
    [AnotherLanguage_ID] int  NULL,
    [Nationality_ID] int  NULL,
    [LastLoginDate] datetime  NULL
);
GO

-- Creating table 'Church_ContactCourseInstance'
CREATE TABLE [dbo].[Church_ContactCourseInstance] (
    [ContactCourseInstance_ID] int IDENTITY(1,1) NOT NULL,
    [Contact_ID] int  NOT NULL,
    [CourseInstance_ID] int  NOT NULL,
    [DateAdded] datetime  NOT NULL
);
GO

-- Creating table 'Church_Ministry'
CREATE TABLE [dbo].[Church_Ministry] (
    [Ministry_ID] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(50)  NOT NULL,
    [MinistryPastor_ID] int  NULL,
    [SortOrder] int  NOT NULL
);
GO

-- Creating table 'Common_DatabaseInstance'
CREATE TABLE [dbo].[Common_DatabaseInstance] (
    [DatabaseInstance] nvarchar(10)  NOT NULL,
    [DefaultEmailAddress] nvarchar(50)  NOT NULL,
    [TimeDifference] int  NOT NULL
);
GO

-- Creating table 'Volunteer_Application'
CREATE TABLE [dbo].[Volunteer_Application] (
    [Application_ID] int IDENTITY(1,1) NOT NULL,
    [Contact_ID] int  NOT NULL,
    [VolunteerArea_ID] int  NOT NULL,
    [Campus_ID] int  NOT NULL,
    [Ministry_ID] int  NOT NULL,
    [Region_ID] int  NULL,
    [ULG_ID] int  NULL,
    [DateRequested] datetime  NOT NULL,
    [Actioned] bit  NOT NULL,
    [ActionedBy] int  NULL,
    [DateActioned] datetime  NULL,
    [Approved] bit  NOT NULL
);
GO

-- Creating table 'Volunteer_ApplicationAnswer'
CREATE TABLE [dbo].[Volunteer_ApplicationAnswer] (
    [Answer_ID] int IDENTITY(1,1) NOT NULL,
    [Application_ID] int  NOT NULL,
    [Question_ID] int  NOT NULL,
    [Answer] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Volunteer_ApplicationApprovalType'
CREATE TABLE [dbo].[Volunteer_ApplicationApprovalType] (
    [ApprovalType_ID] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(128)  NOT NULL,
    [Active] bit  NOT NULL,
    [VolunteerArea_ID] int  NOT NULL,
    [Priority] int  NOT NULL
);
GO

-- Creating table 'Volunteer_ApplicationQuestion'
CREATE TABLE [dbo].[Volunteer_ApplicationQuestion] (
    [Question_ID] int IDENTITY(1,1) NOT NULL,
    [Question] nvarchar(max)  NOT NULL,
    [Type] nvarchar(128)  NOT NULL,
    [Required] bit  NOT NULL,
    [Active] bit  NOT NULL,
    [SortOrder] int  NOT NULL
);
GO

-- Creating table 'Volunteer_Area'
CREATE TABLE [dbo].[Volunteer_Area] (
    [VolunteerArea_ID] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(128)  NOT NULL,
    [Active] bit  NOT NULL
);
GO

-- Creating table 'Volunteer_ApplicationApproval'
CREATE TABLE [dbo].[Volunteer_ApplicationApproval] (
    [Approval_ID] int IDENTITY(1,1) NOT NULL,
    [Application_ID] int  NOT NULL,
    [ActionedBy] int  NULL,
    [ApprovalType_ID] int  NOT NULL,
    [Approved] bit  NULL,
    [Comments] nvarchar(max)  NULL,
    [ActionDate] datetime  NULL
);
GO

-- Creating table 'Common_ContactExternal'
CREATE TABLE [dbo].[Common_ContactExternal] (
    [Contact_ID] int  NOT NULL,
    [ChurchName] nvarchar(50)  NOT NULL,
    [Denomination_ID] int  NOT NULL,
    [SeniorPastorName] nvarchar(50)  NOT NULL,
    [ChurchAddress1] nvarchar(100)  NOT NULL,
    [ChurchAddress2] nvarchar(100)  NOT NULL,
    [ChurchSuburb] nvarchar(20)  NOT NULL,
    [ChurchPostcode] nvarchar(10)  NOT NULL,
    [ChurchState_ID] int  NULL,
    [ChurchStateOther] nvarchar(20)  NOT NULL,
    [ChurchCountry_ID] int  NULL,
    [ChurchWebsite] nvarchar(50)  NOT NULL,
    [LeaderName] nvarchar(60)  NOT NULL,
    [ChurchNumberOfPeople] nvarchar(10)  NOT NULL,
    [ChurchPhone] nvarchar(20)  NOT NULL,
    [ChurchEmail] nvarchar(100)  NOT NULL,
    [ContactDetailsVerified] bit  NOT NULL,
    [Deleted] bit  NOT NULL,
    [Inactive] bit  NOT NULL
);
GO

-- Creating table 'Common_ContactExternalEvents'
CREATE TABLE [dbo].[Common_ContactExternalEvents] (
    [Contact_ID] int  NOT NULL,
    [EmergencyContactName] nvarchar(50)  NOT NULL,
    [EmergencyContactPhone] nvarchar(20)  NOT NULL,
    [EmergencyContactRelationship] nvarchar(30)  NOT NULL,
    [MedicalInformation] nvarchar(200)  NOT NULL,
    [MedicalAllergies] nvarchar(200)  NOT NULL,
    [AccessibilityInformation] nvarchar(max)  NOT NULL,
    [EventComments] nvarchar(500)  NOT NULL
);
GO

-- Creating table 'Volunteer_ApplicationApprover'
CREATE TABLE [dbo].[Volunteer_ApplicationApprover] (
    [Approver_ID] int IDENTITY(1,1) NOT NULL,
    [Contact_ID] int  NOT NULL,
    [Ministry_ID] int  NULL,
    [Region_ID] int  NULL,
    [Campus_ID] int  NULL,
    [ApprovalType_ID] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [ContactRole_ID] in table 'Church_ContactRole'
ALTER TABLE [dbo].[Church_ContactRole]
ADD CONSTRAINT [PK_Church_ContactRole]
    PRIMARY KEY CLUSTERED ([ContactRole_ID] ASC);
GO

-- Creating primary key on [NPNC_ID] in table 'Church_NPNC'
ALTER TABLE [dbo].[Church_NPNC]
ADD CONSTRAINT [PK_Church_NPNC]
    PRIMARY KEY CLUSTERED ([NPNC_ID] ASC);
GO

-- Creating primary key on [Region_ID] in table 'Church_Region'
ALTER TABLE [dbo].[Church_Region]
ADD CONSTRAINT [PK_Church_Region]
    PRIMARY KEY CLUSTERED ([Region_ID] ASC);
GO

-- Creating primary key on [RegionalPastor_ID] in table 'Church_RegionalPastor'
ALTER TABLE [dbo].[Church_RegionalPastor]
ADD CONSTRAINT [PK_Church_RegionalPastor]
    PRIMARY KEY CLUSTERED ([RegionalPastor_ID] ASC);
GO

-- Creating primary key on [RestrictedAccessULG_ID] in table 'Church_RestrictedAccessULG'
ALTER TABLE [dbo].[Church_RestrictedAccessULG]
ADD CONSTRAINT [PK_Church_RestrictedAccessULG]
    PRIMARY KEY CLUSTERED ([RestrictedAccessULG_ID] ASC);
GO

-- Creating primary key on [Role_ID] in table 'Church_Role'
ALTER TABLE [dbo].[Church_Role]
ADD CONSTRAINT [PK_Church_Role]
    PRIMARY KEY CLUSTERED ([Role_ID] ASC);
GO

-- Creating primary key on [RoleType_ID] in table 'Church_RoleType'
ALTER TABLE [dbo].[Church_RoleType]
ADD CONSTRAINT [PK_Church_RoleType]
    PRIMARY KEY CLUSTERED ([RoleType_ID] ASC);
GO

-- Creating primary key on [RoleTypeUL_ID] in table 'Church_RoleTypeUL'
ALTER TABLE [dbo].[Church_RoleTypeUL]
ADD CONSTRAINT [PK_Church_RoleTypeUL]
    PRIMARY KEY CLUSTERED ([RoleTypeUL_ID] ASC);
GO

-- Creating primary key on [ULG_ID] in table 'Church_ULG'
ALTER TABLE [dbo].[Church_ULG]
ADD CONSTRAINT [PK_Church_ULG]
    PRIMARY KEY CLUSTERED ([ULG_ID] ASC);
GO

-- Creating primary key on [ULGContact_ID] in table 'Church_ULGContact'
ALTER TABLE [dbo].[Church_ULGContact]
ADD CONSTRAINT [PK_Church_ULGContact]
    PRIMARY KEY CLUSTERED ([ULGContact_ID] ASC);
GO

-- Creating primary key on [Contact_ID] in table 'Common_ContactInternal'
ALTER TABLE [dbo].[Common_ContactInternal]
ADD CONSTRAINT [PK_Common_ContactInternal]
    PRIMARY KEY CLUSTERED ([Contact_ID] ASC);
GO

-- Creating primary key on [CarersAccess_ID] in table 'Church_CarersAccess'
ALTER TABLE [dbo].[Church_CarersAccess]
ADD CONSTRAINT [PK_Church_CarersAccess]
    PRIMARY KEY CLUSTERED ([CarersAccess_ID] ASC);
GO

-- Creating primary key on [Comment_ID] in table 'Church_ContactComment'
ALTER TABLE [dbo].[Church_ContactComment]
ADD CONSTRAINT [PK_Church_ContactComment]
    PRIMARY KEY CLUSTERED ([Comment_ID] ASC);
GO

-- Creating primary key on [PCRDate_ID] in table 'Church_PCRDate'
ALTER TABLE [dbo].[Church_PCRDate]
ADD CONSTRAINT [PK_Church_PCRDate]
    PRIMARY KEY CLUSTERED ([PCRDate_ID] ASC);
GO

-- Creating primary key on [ChurchStatus_ID] in table 'Church_ChurchStatus'
ALTER TABLE [dbo].[Church_ChurchStatus]
ADD CONSTRAINT [PK_Church_ChurchStatus]
    PRIMARY KEY CLUSTERED ([ChurchStatus_ID] ASC);
GO

-- Creating primary key on [ContactDatabaseRole_ID] in table 'Common_ContactDatabaseRole'
ALTER TABLE [dbo].[Common_ContactDatabaseRole]
ADD CONSTRAINT [PK_Common_ContactDatabaseRole]
    PRIMARY KEY CLUSTERED ([ContactDatabaseRole_ID] ASC);
GO

-- Creating primary key on [Contact_ID] in table 'Common_UserDetail'
ALTER TABLE [dbo].[Common_UserDetail]
ADD CONSTRAINT [PK_Common_UserDetail]
    PRIMARY KEY CLUSTERED ([Contact_ID] ASC);
GO

-- Creating primary key on [Country_ID] in table 'Common_GeneralCountry'
ALTER TABLE [dbo].[Common_GeneralCountry]
ADD CONSTRAINT [PK_Common_GeneralCountry]
    PRIMARY KEY CLUSTERED ([Country_ID] ASC);
GO

-- Creating primary key on [State_ID] in table 'Common_GeneralState'
ALTER TABLE [dbo].[Common_GeneralState]
ADD CONSTRAINT [PK_Common_GeneralState]
    PRIMARY KEY CLUSTERED ([State_ID] ASC);
GO

-- Creating primary key on [Salutation_ID] in table 'Common_Salutation'
ALTER TABLE [dbo].[Common_Salutation]
ADD CONSTRAINT [PK_Common_Salutation]
    PRIMARY KEY CLUSTERED ([Salutation_ID] ASC);
GO

-- Creating primary key on [ContactCourseEnrolment_ID] in table 'Church_ContactCourseEnrolment'
ALTER TABLE [dbo].[Church_ContactCourseEnrolment]
ADD CONSTRAINT [PK_Church_ContactCourseEnrolment]
    PRIMARY KEY CLUSTERED ([ContactCourseEnrolment_ID] ASC);
GO

-- Creating primary key on [CourseInstance_ID] in table 'Church_CourseInstance'
ALTER TABLE [dbo].[Church_CourseInstance]
ADD CONSTRAINT [PK_Church_CourseInstance]
    PRIMARY KEY CLUSTERED ([CourseInstance_ID] ASC);
GO

-- Creating primary key on [CourseInstanceCampus_ID] in table 'Church_CourseInstanceCampus'
ALTER TABLE [dbo].[Church_CourseInstanceCampus]
ADD CONSTRAINT [PK_Church_CourseInstanceCampus]
    PRIMARY KEY CLUSTERED ([CourseInstanceCampus_ID] ASC);
GO

-- Creating primary key on [Course_ID] in table 'Church_Course'
ALTER TABLE [dbo].[Church_Course]
ADD CONSTRAINT [PK_Church_Course]
    PRIMARY KEY CLUSTERED ([Course_ID] ASC);
GO

-- Creating primary key on [Campus_ID] in table 'Church_Campus'
ALTER TABLE [dbo].[Church_Campus]
ADD CONSTRAINT [PK_Church_Campus]
    PRIMARY KEY CLUSTERED ([Campus_ID] ASC);
GO

-- Creating primary key on [MailingList_ID] in table 'Common_MailingList'
ALTER TABLE [dbo].[Common_MailingList]
ADD CONSTRAINT [PK_Common_MailingList]
    PRIMARY KEY CLUSTERED ([MailingList_ID] ASC);
GO

-- Creating primary key on [MailingListType_ID] in table 'Common_MailingListType'
ALTER TABLE [dbo].[Common_MailingListType]
ADD CONSTRAINT [PK_Common_MailingListType]
    PRIMARY KEY CLUSTERED ([MailingListType_ID] ASC);
GO

-- Creating primary key on [ContactMailingList_ID] in table 'Common_ContactMailingList'
ALTER TABLE [dbo].[Common_ContactMailingList]
ADD CONSTRAINT [PK_Common_ContactMailingList]
    PRIMARY KEY CLUSTERED ([ContactMailingList_ID] ASC);
GO

-- Creating primary key on [ID] in table 'Common_CombinedContactMailingList'
ALTER TABLE [dbo].[Common_CombinedContactMailingList]
ADD CONSTRAINT [PK_Common_CombinedContactMailingList]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [Contact_ID] in table 'Common_Contact'
ALTER TABLE [dbo].[Common_Contact]
ADD CONSTRAINT [PK_Common_Contact]
    PRIMARY KEY CLUSTERED ([Contact_ID] ASC);
GO

-- Creating primary key on [ContactCourseInstance_ID] in table 'Church_ContactCourseInstance'
ALTER TABLE [dbo].[Church_ContactCourseInstance]
ADD CONSTRAINT [PK_Church_ContactCourseInstance]
    PRIMARY KEY CLUSTERED ([ContactCourseInstance_ID] ASC);
GO

-- Creating primary key on [Ministry_ID] in table 'Church_Ministry'
ALTER TABLE [dbo].[Church_Ministry]
ADD CONSTRAINT [PK_Church_Ministry]
    PRIMARY KEY CLUSTERED ([Ministry_ID] ASC);
GO

-- Creating primary key on [DatabaseInstance], [DefaultEmailAddress], [TimeDifference] in table 'Common_DatabaseInstance'
ALTER TABLE [dbo].[Common_DatabaseInstance]
ADD CONSTRAINT [PK_Common_DatabaseInstance]
    PRIMARY KEY CLUSTERED ([DatabaseInstance], [DefaultEmailAddress], [TimeDifference] ASC);
GO

-- Creating primary key on [Application_ID] in table 'Volunteer_Application'
ALTER TABLE [dbo].[Volunteer_Application]
ADD CONSTRAINT [PK_Volunteer_Application]
    PRIMARY KEY CLUSTERED ([Application_ID] ASC);
GO

-- Creating primary key on [Answer_ID] in table 'Volunteer_ApplicationAnswer'
ALTER TABLE [dbo].[Volunteer_ApplicationAnswer]
ADD CONSTRAINT [PK_Volunteer_ApplicationAnswer]
    PRIMARY KEY CLUSTERED ([Answer_ID] ASC);
GO

-- Creating primary key on [ApprovalType_ID] in table 'Volunteer_ApplicationApprovalType'
ALTER TABLE [dbo].[Volunteer_ApplicationApprovalType]
ADD CONSTRAINT [PK_Volunteer_ApplicationApprovalType]
    PRIMARY KEY CLUSTERED ([ApprovalType_ID] ASC);
GO

-- Creating primary key on [Question_ID] in table 'Volunteer_ApplicationQuestion'
ALTER TABLE [dbo].[Volunteer_ApplicationQuestion]
ADD CONSTRAINT [PK_Volunteer_ApplicationQuestion]
    PRIMARY KEY CLUSTERED ([Question_ID] ASC);
GO

-- Creating primary key on [VolunteerArea_ID] in table 'Volunteer_Area'
ALTER TABLE [dbo].[Volunteer_Area]
ADD CONSTRAINT [PK_Volunteer_Area]
    PRIMARY KEY CLUSTERED ([VolunteerArea_ID] ASC);
GO

-- Creating primary key on [Approval_ID] in table 'Volunteer_ApplicationApproval'
ALTER TABLE [dbo].[Volunteer_ApplicationApproval]
ADD CONSTRAINT [PK_Volunteer_ApplicationApproval]
    PRIMARY KEY CLUSTERED ([Approval_ID] ASC);
GO

-- Creating primary key on [Contact_ID] in table 'Common_ContactExternal'
ALTER TABLE [dbo].[Common_ContactExternal]
ADD CONSTRAINT [PK_Common_ContactExternal]
    PRIMARY KEY CLUSTERED ([Contact_ID] ASC);
GO

-- Creating primary key on [Contact_ID] in table 'Common_ContactExternalEvents'
ALTER TABLE [dbo].[Common_ContactExternalEvents]
ADD CONSTRAINT [PK_Common_ContactExternalEvents]
    PRIMARY KEY CLUSTERED ([Contact_ID] ASC);
GO

-- Creating primary key on [Approver_ID] in table 'Volunteer_ApplicationApprover'
ALTER TABLE [dbo].[Volunteer_ApplicationApprover]
ADD CONSTRAINT [PK_Volunteer_ApplicationApprover]
    PRIMARY KEY CLUSTERED ([Approver_ID] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Role_ID] in table 'Church_ContactRole'
ALTER TABLE [dbo].[Church_ContactRole]
ADD CONSTRAINT [FK_Church_ContactRole_Church_Role]
    FOREIGN KEY ([Role_ID])
    REFERENCES [dbo].[Church_Role]
        ([Role_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Church_ContactRole_Church_Role'
CREATE INDEX [IX_FK_Church_ContactRole_Church_Role]
ON [dbo].[Church_ContactRole]
    ([Role_ID]);
GO

-- Creating foreign key on [Contact_ID] in table 'Church_ContactRole'
ALTER TABLE [dbo].[Church_ContactRole]
ADD CONSTRAINT [FK_Church_ContactRole_Common_ContactInternal]
    FOREIGN KEY ([Contact_ID])
    REFERENCES [dbo].[Common_ContactInternal]
        ([Contact_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Church_ContactRole_Common_ContactInternal'
CREATE INDEX [IX_FK_Church_ContactRole_Common_ContactInternal]
ON [dbo].[Church_ContactRole]
    ([Contact_ID]);
GO

-- Creating foreign key on [ContactRole_ID] in table 'Church_RestrictedAccessULG'
ALTER TABLE [dbo].[Church_RestrictedAccessULG]
ADD CONSTRAINT [FK_Church_RestrictedAccessULG_Church_ContactRole]
    FOREIGN KEY ([ContactRole_ID])
    REFERENCES [dbo].[Church_ContactRole]
        ([ContactRole_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Church_RestrictedAccessULG_Church_ContactRole'
CREATE INDEX [IX_FK_Church_RestrictedAccessULG_Church_ContactRole]
ON [dbo].[Church_RestrictedAccessULG]
    ([ContactRole_ID]);
GO

-- Creating foreign key on [Contact_ID] in table 'Church_NPNC'
ALTER TABLE [dbo].[Church_NPNC]
ADD CONSTRAINT [FK_Church_NPNC_Common_ContactInternal]
    FOREIGN KEY ([Contact_ID])
    REFERENCES [dbo].[Common_ContactInternal]
        ([Contact_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Church_NPNC_Common_ContactInternal'
CREATE INDEX [IX_FK_Church_NPNC_Common_ContactInternal]
ON [dbo].[Church_NPNC]
    ([Contact_ID]);
GO

-- Creating foreign key on [Region_ID] in table 'Church_RegionalPastor'
ALTER TABLE [dbo].[Church_RegionalPastor]
ADD CONSTRAINT [FK_Church_RegionalPastor_Church_Region]
    FOREIGN KEY ([Region_ID])
    REFERENCES [dbo].[Church_Region]
        ([Region_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Church_RegionalPastor_Church_Region'
CREATE INDEX [IX_FK_Church_RegionalPastor_Church_Region]
ON [dbo].[Church_RegionalPastor]
    ([Region_ID]);
GO

-- Creating foreign key on [Region_ID] in table 'Common_ContactInternal'
ALTER TABLE [dbo].[Common_ContactInternal]
ADD CONSTRAINT [FK_Common_ContactInternal_Church_Region]
    FOREIGN KEY ([Region_ID])
    REFERENCES [dbo].[Church_Region]
        ([Region_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Common_ContactInternal_Church_Region'
CREATE INDEX [IX_FK_Common_ContactInternal_Church_Region]
ON [dbo].[Common_ContactInternal]
    ([Region_ID]);
GO

-- Creating foreign key on [ULG_ID] in table 'Church_RestrictedAccessULG'
ALTER TABLE [dbo].[Church_RestrictedAccessULG]
ADD CONSTRAINT [FK_Church_RestrictedAccessULG_Church_ULG]
    FOREIGN KEY ([ULG_ID])
    REFERENCES [dbo].[Church_ULG]
        ([ULG_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Church_RestrictedAccessULG_Church_ULG'
CREATE INDEX [IX_FK_Church_RestrictedAccessULG_Church_ULG]
ON [dbo].[Church_RestrictedAccessULG]
    ([ULG_ID]);
GO

-- Creating foreign key on [RoleType_ID] in table 'Church_Role'
ALTER TABLE [dbo].[Church_Role]
ADD CONSTRAINT [FK_Church_Role_Church_RoleType]
    FOREIGN KEY ([RoleType_ID])
    REFERENCES [dbo].[Church_RoleType]
        ([RoleType_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Church_Role_Church_RoleType'
CREATE INDEX [IX_FK_Church_Role_Church_RoleType]
ON [dbo].[Church_Role]
    ([RoleType_ID]);
GO

-- Creating foreign key on [RoleTypeUL_ID] in table 'Church_Role'
ALTER TABLE [dbo].[Church_Role]
ADD CONSTRAINT [FK_Church_Role_Church_RoleTypeUL]
    FOREIGN KEY ([RoleTypeUL_ID])
    REFERENCES [dbo].[Church_RoleTypeUL]
        ([RoleTypeUL_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Church_Role_Church_RoleTypeUL'
CREATE INDEX [IX_FK_Church_Role_Church_RoleTypeUL]
ON [dbo].[Church_Role]
    ([RoleTypeUL_ID]);
GO

-- Creating foreign key on [ULG_ID] in table 'Church_ULGContact'
ALTER TABLE [dbo].[Church_ULGContact]
ADD CONSTRAINT [FK_Church_ULGContact_Church_ULG]
    FOREIGN KEY ([ULG_ID])
    REFERENCES [dbo].[Church_ULG]
        ([ULG_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Church_ULGContact_Church_ULG'
CREATE INDEX [IX_FK_Church_ULGContact_Church_ULG]
ON [dbo].[Church_ULGContact]
    ([ULG_ID]);
GO

-- Creating foreign key on [Contact_ID] in table 'Church_ULGContact'
ALTER TABLE [dbo].[Church_ULGContact]
ADD CONSTRAINT [FK_Church_ContactULG_Common_ContactInternal]
    FOREIGN KEY ([Contact_ID])
    REFERENCES [dbo].[Common_ContactInternal]
        ([Contact_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Church_ContactULG_Common_ContactInternal'
CREATE INDEX [IX_FK_Church_ContactULG_Common_ContactInternal]
ON [dbo].[Church_ULGContact]
    ([Contact_ID]);
GO

-- Creating foreign key on [Contact_ID] in table 'Church_CarersAccess'
ALTER TABLE [dbo].[Church_CarersAccess]
ADD CONSTRAINT [FK_Church_CarersAccess_Common_ContactInternal]
    FOREIGN KEY ([Contact_ID])
    REFERENCES [dbo].[Common_ContactInternal]
        ([Contact_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Church_CarersAccess_Common_ContactInternal'
CREATE INDEX [IX_FK_Church_CarersAccess_Common_ContactInternal]
ON [dbo].[Church_CarersAccess]
    ([Contact_ID]);
GO

-- Creating foreign key on [Carer_ID] in table 'Church_CarersAccess'
ALTER TABLE [dbo].[Church_CarersAccess]
ADD CONSTRAINT [FK_Church_CarersAccess_Common_ContactInternal1]
    FOREIGN KEY ([Carer_ID])
    REFERENCES [dbo].[Common_ContactInternal]
        ([Contact_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Church_CarersAccess_Common_ContactInternal1'
CREATE INDEX [IX_FK_Church_CarersAccess_Common_ContactInternal1]
ON [dbo].[Church_CarersAccess]
    ([Carer_ID]);
GO

-- Creating foreign key on [Contact_ID] in table 'Church_ContactComment'
ALTER TABLE [dbo].[Church_ContactComment]
ADD CONSTRAINT [FK_Church_ContactComment_Common_ContactInternal]
    FOREIGN KEY ([Contact_ID])
    REFERENCES [dbo].[Common_ContactInternal]
        ([Contact_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Church_ContactComment_Common_ContactInternal'
CREATE INDEX [IX_FK_Church_ContactComment_Common_ContactInternal]
ON [dbo].[Church_ContactComment]
    ([Contact_ID]);
GO

-- Creating foreign key on [CommentBy_ID] in table 'Church_ContactComment'
ALTER TABLE [dbo].[Church_ContactComment]
ADD CONSTRAINT [FK_Church_ContactComment_Common_UserDetail]
    FOREIGN KEY ([CommentBy_ID])
    REFERENCES [dbo].[Common_UserDetail]
        ([Contact_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Church_ContactComment_Common_UserDetail'
CREATE INDEX [IX_FK_Church_ContactComment_Common_UserDetail]
ON [dbo].[Church_ContactComment]
    ([CommentBy_ID]);
GO

-- Creating foreign key on [Carer_ID] in table 'Church_NPNC'
ALTER TABLE [dbo].[Church_NPNC]
ADD CONSTRAINT [FK_Church_NPNC_Common_UserDetail]
    FOREIGN KEY ([Carer_ID])
    REFERENCES [dbo].[Common_UserDetail]
        ([Contact_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Church_NPNC_Common_UserDetail'
CREATE INDEX [IX_FK_Church_NPNC_Common_UserDetail]
ON [dbo].[Church_NPNC]
    ([Carer_ID]);
GO

-- Creating foreign key on [Contact_ID] in table 'Common_ContactDatabaseRole'
ALTER TABLE [dbo].[Common_ContactDatabaseRole]
ADD CONSTRAINT [FK_Common_ContactDatabaseRole_Common_UserDetail]
    FOREIGN KEY ([Contact_ID])
    REFERENCES [dbo].[Common_UserDetail]
        ([Contact_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Common_ContactDatabaseRole_Common_UserDetail'
CREATE INDEX [IX_FK_Common_ContactDatabaseRole_Common_UserDetail]
ON [dbo].[Common_ContactDatabaseRole]
    ([Contact_ID]);
GO

-- Creating foreign key on [Country_ID] in table 'Common_GeneralState'
ALTER TABLE [dbo].[Common_GeneralState]
ADD CONSTRAINT [FK_GeneralState_GeneralCountry]
    FOREIGN KEY ([Country_ID])
    REFERENCES [dbo].[Common_GeneralCountry]
        ([Country_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_GeneralState_GeneralCountry'
CREATE INDEX [IX_FK_GeneralState_GeneralCountry]
ON [dbo].[Common_GeneralState]
    ([Country_ID]);
GO

-- Creating foreign key on [CourseInstance_ID] in table 'Church_ContactCourseEnrolment'
ALTER TABLE [dbo].[Church_ContactCourseEnrolment]
ADD CONSTRAINT [FK_Church_ContactCourseEnrollment_Church_CourseInstance]
    FOREIGN KEY ([CourseInstance_ID])
    REFERENCES [dbo].[Church_CourseInstance]
        ([CourseInstance_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Church_ContactCourseEnrollment_Church_CourseInstance'
CREATE INDEX [IX_FK_Church_ContactCourseEnrollment_Church_CourseInstance]
ON [dbo].[Church_ContactCourseEnrolment]
    ([CourseInstance_ID]);
GO

-- Creating foreign key on [Contact_ID] in table 'Church_ContactCourseEnrolment'
ALTER TABLE [dbo].[Church_ContactCourseEnrolment]
ADD CONSTRAINT [FK_Church_ContactCourseEnrollment_Common_ContactInternal]
    FOREIGN KEY ([Contact_ID])
    REFERENCES [dbo].[Common_ContactInternal]
        ([Contact_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Church_ContactCourseEnrollment_Common_ContactInternal'
CREATE INDEX [IX_FK_Church_ContactCourseEnrollment_Common_ContactInternal]
ON [dbo].[Church_ContactCourseEnrolment]
    ([Contact_ID]);
GO

-- Creating foreign key on [CourseInstance_ID] in table 'Church_CourseInstanceCampus'
ALTER TABLE [dbo].[Church_CourseInstanceCampus]
ADD CONSTRAINT [FK_Church_CourseInstanceCampus_Church_CourseInstance]
    FOREIGN KEY ([CourseInstance_ID])
    REFERENCES [dbo].[Church_CourseInstance]
        ([CourseInstance_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Church_CourseInstanceCampus_Church_CourseInstance'
CREATE INDEX [IX_FK_Church_CourseInstanceCampus_Church_CourseInstance]
ON [dbo].[Church_CourseInstanceCampus]
    ([CourseInstance_ID]);
GO

-- Creating foreign key on [Course_ID] in table 'Church_CourseInstance'
ALTER TABLE [dbo].[Church_CourseInstance]
ADD CONSTRAINT [FK_Church_CourseInstance_Church_Course]
    FOREIGN KEY ([Course_ID])
    REFERENCES [dbo].[Church_Course]
        ([Course_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Church_CourseInstance_Church_Course'
CREATE INDEX [IX_FK_Church_CourseInstance_Church_Course]
ON [dbo].[Church_CourseInstance]
    ([Course_ID]);
GO

-- Creating foreign key on [Campus_ID] in table 'Church_CourseInstanceCampus'
ALTER TABLE [dbo].[Church_CourseInstanceCampus]
ADD CONSTRAINT [FK_Church_CourseInstanceCampus_Church_Campus]
    FOREIGN KEY ([Campus_ID])
    REFERENCES [dbo].[Church_Campus]
        ([Campus_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Church_CourseInstanceCampus_Church_Campus'
CREATE INDEX [IX_FK_Church_CourseInstanceCampus_Church_Campus]
ON [dbo].[Church_CourseInstanceCampus]
    ([Campus_ID]);
GO

-- Creating foreign key on [Campus_ID] in table 'Church_RegionalPastor'
ALTER TABLE [dbo].[Church_RegionalPastor]
ADD CONSTRAINT [FK_Church_RegionalPastor_Church_Campus]
    FOREIGN KEY ([Campus_ID])
    REFERENCES [dbo].[Church_Campus]
        ([Campus_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Church_RegionalPastor_Church_Campus'
CREATE INDEX [IX_FK_Church_RegionalPastor_Church_Campus]
ON [dbo].[Church_RegionalPastor]
    ([Campus_ID]);
GO

-- Creating foreign key on [Campus_ID] in table 'Common_ContactInternal'
ALTER TABLE [dbo].[Common_ContactInternal]
ADD CONSTRAINT [FK_Common_ContactInternal_Church_Campus]
    FOREIGN KEY ([Campus_ID])
    REFERENCES [dbo].[Church_Campus]
        ([Campus_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Common_ContactInternal_Church_Campus'
CREATE INDEX [IX_FK_Common_ContactInternal_Church_Campus]
ON [dbo].[Common_ContactInternal]
    ([Campus_ID]);
GO

-- Creating foreign key on [MailingList_ID] in table 'Common_ContactMailingList'
ALTER TABLE [dbo].[Common_ContactMailingList]
ADD CONSTRAINT [FK_Common_ContactMailingList_Common_MailingList]
    FOREIGN KEY ([MailingList_ID])
    REFERENCES [dbo].[Common_MailingList]
        ([MailingList_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Common_ContactMailingList_Common_MailingList'
CREATE INDEX [IX_FK_Common_ContactMailingList_Common_MailingList]
ON [dbo].[Common_ContactMailingList]
    ([MailingList_ID]);
GO

-- Creating foreign key on [MailingList_ID] in table 'Common_CombinedContactMailingList'
ALTER TABLE [dbo].[Common_CombinedContactMailingList]
ADD CONSTRAINT [FK__Common_Co__Maili__69478F08]
    FOREIGN KEY ([MailingList_ID])
    REFERENCES [dbo].[Common_MailingList]
        ([MailingList_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__Common_Co__Maili__69478F08'
CREATE INDEX [IX_FK__Common_Co__Maili__69478F08]
ON [dbo].[Common_CombinedContactMailingList]
    ([MailingList_ID]);
GO

-- Creating foreign key on [MailingListType_ID] in table 'Common_CombinedContactMailingList'
ALTER TABLE [dbo].[Common_CombinedContactMailingList]
ADD CONSTRAINT [FK__Common_Co__Maili__6A3BB341]
    FOREIGN KEY ([MailingListType_ID])
    REFERENCES [dbo].[Common_MailingListType]
        ([MailingListType_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__Common_Co__Maili__6A3BB341'
CREATE INDEX [IX_FK__Common_Co__Maili__6A3BB341]
ON [dbo].[Common_CombinedContactMailingList]
    ([MailingListType_ID]);
GO

-- Creating foreign key on [Contact_ID] in table 'Common_CombinedContactMailingList'
ALTER TABLE [dbo].[Common_CombinedContactMailingList]
ADD CONSTRAINT [FK__Common_Co__Conta__519D5005]
    FOREIGN KEY ([Contact_ID])
    REFERENCES [dbo].[Common_Contact]
        ([Contact_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__Common_Co__Conta__519D5005'
CREATE INDEX [IX_FK__Common_Co__Conta__519D5005]
ON [dbo].[Common_CombinedContactMailingList]
    ([Contact_ID]);
GO

-- Creating foreign key on [Salutation_ID] in table 'Common_Contact'
ALTER TABLE [dbo].[Common_Contact]
ADD CONSTRAINT [FK_Common_Contact_Common_Salutation]
    FOREIGN KEY ([Salutation_ID])
    REFERENCES [dbo].[Common_Salutation]
        ([Salutation_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Common_Contact_Common_Salutation'
CREATE INDEX [IX_FK_Common_Contact_Common_Salutation]
ON [dbo].[Common_Contact]
    ([Salutation_ID]);
GO

-- Creating foreign key on [CreatedBy_ID] in table 'Common_Contact'
ALTER TABLE [dbo].[Common_Contact]
ADD CONSTRAINT [FK_Common_Contact_Common_UserDetail]
    FOREIGN KEY ([CreatedBy_ID])
    REFERENCES [dbo].[Common_UserDetail]
        ([Contact_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Common_Contact_Common_UserDetail'
CREATE INDEX [IX_FK_Common_Contact_Common_UserDetail]
ON [dbo].[Common_Contact]
    ([CreatedBy_ID]);
GO

-- Creating foreign key on [Contact_ID] in table 'Common_ContactInternal'
ALTER TABLE [dbo].[Common_ContactInternal]
ADD CONSTRAINT [FK_Common_ContactInternal_Common_Contact]
    FOREIGN KEY ([Contact_ID])
    REFERENCES [dbo].[Common_Contact]
        ([Contact_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Contact_ID] in table 'Common_ContactMailingList'
ALTER TABLE [dbo].[Common_ContactMailingList]
ADD CONSTRAINT [FK_Common_ContactMailingList_Common_Contact]
    FOREIGN KEY ([Contact_ID])
    REFERENCES [dbo].[Common_Contact]
        ([Contact_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Common_ContactMailingList_Common_Contact'
CREATE INDEX [IX_FK_Common_ContactMailingList_Common_Contact]
ON [dbo].[Common_ContactMailingList]
    ([Contact_ID]);
GO

-- Creating foreign key on [Contact_ID] in table 'Common_UserDetail'
ALTER TABLE [dbo].[Common_UserDetail]
ADD CONSTRAINT [FK_Common_UserDetail_Common_Contact]
    FOREIGN KEY ([Contact_ID])
    REFERENCES [dbo].[Common_Contact]
        ([Contact_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [CourseInstance_ID] in table 'Church_ContactCourseInstance'
ALTER TABLE [dbo].[Church_ContactCourseInstance]
ADD CONSTRAINT [FK_Church_ContactCourseInstance_Church_CourseInstance]
    FOREIGN KEY ([CourseInstance_ID])
    REFERENCES [dbo].[Church_CourseInstance]
        ([CourseInstance_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Church_ContactCourseInstance_Church_CourseInstance'
CREATE INDEX [IX_FK_Church_ContactCourseInstance_Church_CourseInstance]
ON [dbo].[Church_ContactCourseInstance]
    ([CourseInstance_ID]);
GO

-- Creating foreign key on [Contact_ID] in table 'Church_ContactCourseInstance'
ALTER TABLE [dbo].[Church_ContactCourseInstance]
ADD CONSTRAINT [FK_Church_ContactCourseInstance_Common_ContactInternal]
    FOREIGN KEY ([Contact_ID])
    REFERENCES [dbo].[Common_ContactInternal]
        ([Contact_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Church_ContactCourseInstance_Common_ContactInternal'
CREATE INDEX [IX_FK_Church_ContactCourseInstance_Common_ContactInternal]
ON [dbo].[Church_ContactCourseInstance]
    ([Contact_ID]);
GO

-- Creating foreign key on [Ministry_ID] in table 'Common_ContactInternal'
ALTER TABLE [dbo].[Common_ContactInternal]
ADD CONSTRAINT [FK_Common_ContactInternal_Church_Ministry]
    FOREIGN KEY ([Ministry_ID])
    REFERENCES [dbo].[Church_Ministry]
        ([Ministry_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Common_ContactInternal_Church_Ministry'
CREATE INDEX [IX_FK_Common_ContactInternal_Church_Ministry]
ON [dbo].[Common_ContactInternal]
    ([Ministry_ID]);
GO

-- Creating foreign key on [Campus_ID] in table 'Volunteer_Application'
ALTER TABLE [dbo].[Volunteer_Application]
ADD CONSTRAINT [fk_Volunteer_Application_Church_Campus]
    FOREIGN KEY ([Campus_ID])
    REFERENCES [dbo].[Church_Campus]
        ([Campus_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'fk_Volunteer_Application_Church_Campus'
CREATE INDEX [IX_fk_Volunteer_Application_Church_Campus]
ON [dbo].[Volunteer_Application]
    ([Campus_ID]);
GO

-- Creating foreign key on [Ministry_ID] in table 'Volunteer_Application'
ALTER TABLE [dbo].[Volunteer_Application]
ADD CONSTRAINT [fk_Volunteer_Application_Church_Ministry]
    FOREIGN KEY ([Ministry_ID])
    REFERENCES [dbo].[Church_Ministry]
        ([Ministry_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'fk_Volunteer_Application_Church_Ministry'
CREATE INDEX [IX_fk_Volunteer_Application_Church_Ministry]
ON [dbo].[Volunteer_Application]
    ([Ministry_ID]);
GO

-- Creating foreign key on [Region_ID] in table 'Volunteer_Application'
ALTER TABLE [dbo].[Volunteer_Application]
ADD CONSTRAINT [fk_Volunteer_Application_Church_Region]
    FOREIGN KEY ([Region_ID])
    REFERENCES [dbo].[Church_Region]
        ([Region_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'fk_Volunteer_Application_Church_Region'
CREATE INDEX [IX_fk_Volunteer_Application_Church_Region]
ON [dbo].[Volunteer_Application]
    ([Region_ID]);
GO

-- Creating foreign key on [ULG_ID] in table 'Volunteer_Application'
ALTER TABLE [dbo].[Volunteer_Application]
ADD CONSTRAINT [fk_Volunteer_Application_Church_ULG]
    FOREIGN KEY ([ULG_ID])
    REFERENCES [dbo].[Church_ULG]
        ([ULG_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'fk_Volunteer_Application_Church_ULG'
CREATE INDEX [IX_fk_Volunteer_Application_Church_ULG]
ON [dbo].[Volunteer_Application]
    ([ULG_ID]);
GO

-- Creating foreign key on [Contact_ID] in table 'Volunteer_Application'
ALTER TABLE [dbo].[Volunteer_Application]
ADD CONSTRAINT [fk_Volunteer_Application_Common_Contact]
    FOREIGN KEY ([Contact_ID])
    REFERENCES [dbo].[Common_Contact]
        ([Contact_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'fk_Volunteer_Application_Common_Contact'
CREATE INDEX [IX_fk_Volunteer_Application_Common_Contact]
ON [dbo].[Volunteer_Application]
    ([Contact_ID]);
GO

-- Creating foreign key on [ActionedBy] in table 'Volunteer_Application'
ALTER TABLE [dbo].[Volunteer_Application]
ADD CONSTRAINT [fk_Volunteer_Application_Common_Contact_ActionedBy]
    FOREIGN KEY ([ActionedBy])
    REFERENCES [dbo].[Common_Contact]
        ([Contact_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'fk_Volunteer_Application_Common_Contact_ActionedBy'
CREATE INDEX [IX_fk_Volunteer_Application_Common_Contact_ActionedBy]
ON [dbo].[Volunteer_Application]
    ([ActionedBy]);
GO

-- Creating foreign key on [VolunteerArea_ID] in table 'Volunteer_Application'
ALTER TABLE [dbo].[Volunteer_Application]
ADD CONSTRAINT [fk_Volunteer_Application_Volunteer_Area]
    FOREIGN KEY ([VolunteerArea_ID])
    REFERENCES [dbo].[Volunteer_Area]
        ([VolunteerArea_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'fk_Volunteer_Application_Volunteer_Area'
CREATE INDEX [IX_fk_Volunteer_Application_Volunteer_Area]
ON [dbo].[Volunteer_Application]
    ([VolunteerArea_ID]);
GO

-- Creating foreign key on [Application_ID] in table 'Volunteer_ApplicationAnswer'
ALTER TABLE [dbo].[Volunteer_ApplicationAnswer]
ADD CONSTRAINT [fk_Volunteer_ApplicationAnswer_Volunteer_Application]
    FOREIGN KEY ([Application_ID])
    REFERENCES [dbo].[Volunteer_Application]
        ([Application_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'fk_Volunteer_ApplicationAnswer_Volunteer_Application'
CREATE INDEX [IX_fk_Volunteer_ApplicationAnswer_Volunteer_Application]
ON [dbo].[Volunteer_ApplicationAnswer]
    ([Application_ID]);
GO

-- Creating foreign key on [Question_ID] in table 'Volunteer_ApplicationAnswer'
ALTER TABLE [dbo].[Volunteer_ApplicationAnswer]
ADD CONSTRAINT [fk_Volunteer_ApplicationAnswer_Volunteer_ApplicationQuestion]
    FOREIGN KEY ([Question_ID])
    REFERENCES [dbo].[Volunteer_ApplicationQuestion]
        ([Question_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'fk_Volunteer_ApplicationAnswer_Volunteer_ApplicationQuestion'
CREATE INDEX [IX_fk_Volunteer_ApplicationAnswer_Volunteer_ApplicationQuestion]
ON [dbo].[Volunteer_ApplicationAnswer]
    ([Question_ID]);
GO

-- Creating foreign key on [ActionedBy] in table 'Volunteer_ApplicationApproval'
ALTER TABLE [dbo].[Volunteer_ApplicationApproval]
ADD CONSTRAINT [fk_Volunteer_ApplicationApproval_Common_Contact]
    FOREIGN KEY ([ActionedBy])
    REFERENCES [dbo].[Common_Contact]
        ([Contact_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'fk_Volunteer_ApplicationApproval_Common_Contact'
CREATE INDEX [IX_fk_Volunteer_ApplicationApproval_Common_Contact]
ON [dbo].[Volunteer_ApplicationApproval]
    ([ActionedBy]);
GO

-- Creating foreign key on [Application_ID] in table 'Volunteer_ApplicationApproval'
ALTER TABLE [dbo].[Volunteer_ApplicationApproval]
ADD CONSTRAINT [fk_Volunteer_ApplicationApproval_Volunteer_Application]
    FOREIGN KEY ([Application_ID])
    REFERENCES [dbo].[Volunteer_Application]
        ([Application_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'fk_Volunteer_ApplicationApproval_Volunteer_Application'
CREATE INDEX [IX_fk_Volunteer_ApplicationApproval_Volunteer_Application]
ON [dbo].[Volunteer_ApplicationApproval]
    ([Application_ID]);
GO

-- Creating foreign key on [ApprovalType_ID] in table 'Volunteer_ApplicationApproval'
ALTER TABLE [dbo].[Volunteer_ApplicationApproval]
ADD CONSTRAINT [fk_Volunteer_ApplicationApproval_Volunteer_ApplicationApprovalType]
    FOREIGN KEY ([ApprovalType_ID])
    REFERENCES [dbo].[Volunteer_ApplicationApprovalType]
        ([ApprovalType_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'fk_Volunteer_ApplicationApproval_Volunteer_ApplicationApprovalType'
CREATE INDEX [IX_fk_Volunteer_ApplicationApproval_Volunteer_ApplicationApprovalType]
ON [dbo].[Volunteer_ApplicationApproval]
    ([ApprovalType_ID]);
GO

-- Creating foreign key on [Contact_ID] in table 'Common_ContactExternal'
ALTER TABLE [dbo].[Common_ContactExternal]
ADD CONSTRAINT [FK_ContactExternal_Contact]
    FOREIGN KEY ([Contact_ID])
    REFERENCES [dbo].[Common_Contact]
        ([Contact_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [ChurchCountry_ID] in table 'Common_ContactExternal'
ALTER TABLE [dbo].[Common_ContactExternal]
ADD CONSTRAINT [FK_Common_ContactExternal_Common_GeneralCountry]
    FOREIGN KEY ([ChurchCountry_ID])
    REFERENCES [dbo].[Common_GeneralCountry]
        ([Country_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Common_ContactExternal_Common_GeneralCountry'
CREATE INDEX [IX_FK_Common_ContactExternal_Common_GeneralCountry]
ON [dbo].[Common_ContactExternal]
    ([ChurchCountry_ID]);
GO

-- Creating foreign key on [ChurchState_ID] in table 'Common_ContactExternal'
ALTER TABLE [dbo].[Common_ContactExternal]
ADD CONSTRAINT [FK_Common_ContactExternal_Common_GeneralState]
    FOREIGN KEY ([ChurchState_ID])
    REFERENCES [dbo].[Common_GeneralState]
        ([State_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Common_ContactExternal_Common_GeneralState'
CREATE INDEX [IX_FK_Common_ContactExternal_Common_GeneralState]
ON [dbo].[Common_ContactExternal]
    ([ChurchState_ID]);
GO

-- Creating foreign key on [Contact_ID] in table 'Common_ContactExternalEvents'
ALTER TABLE [dbo].[Common_ContactExternalEvents]
ADD CONSTRAINT [FK_Common_ContactExternalEvents_Common_ContactExternal]
    FOREIGN KEY ([Contact_ID])
    REFERENCES [dbo].[Common_ContactExternal]
        ([Contact_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Campus_ID] in table 'Volunteer_ApplicationApprover'
ALTER TABLE [dbo].[Volunteer_ApplicationApprover]
ADD CONSTRAINT [fk_Volunteer_ApplicationApprover_Church_Campus]
    FOREIGN KEY ([Campus_ID])
    REFERENCES [dbo].[Church_Campus]
        ([Campus_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'fk_Volunteer_ApplicationApprover_Church_Campus'
CREATE INDEX [IX_fk_Volunteer_ApplicationApprover_Church_Campus]
ON [dbo].[Volunteer_ApplicationApprover]
    ([Campus_ID]);
GO

-- Creating foreign key on [Ministry_ID] in table 'Volunteer_ApplicationApprover'
ALTER TABLE [dbo].[Volunteer_ApplicationApprover]
ADD CONSTRAINT [fk_Volunteer_ApplicationApprover_Church_Ministry]
    FOREIGN KEY ([Ministry_ID])
    REFERENCES [dbo].[Church_Ministry]
        ([Ministry_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'fk_Volunteer_ApplicationApprover_Church_Ministry'
CREATE INDEX [IX_fk_Volunteer_ApplicationApprover_Church_Ministry]
ON [dbo].[Volunteer_ApplicationApprover]
    ([Ministry_ID]);
GO

-- Creating foreign key on [Region_ID] in table 'Volunteer_ApplicationApprover'
ALTER TABLE [dbo].[Volunteer_ApplicationApprover]
ADD CONSTRAINT [fk_Volunteer_ApplicationApprover_Church_Region]
    FOREIGN KEY ([Region_ID])
    REFERENCES [dbo].[Church_Region]
        ([Region_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'fk_Volunteer_ApplicationApprover_Church_Region'
CREATE INDEX [IX_fk_Volunteer_ApplicationApprover_Church_Region]
ON [dbo].[Volunteer_ApplicationApprover]
    ([Region_ID]);
GO

-- Creating foreign key on [Contact_ID] in table 'Volunteer_ApplicationApprover'
ALTER TABLE [dbo].[Volunteer_ApplicationApprover]
ADD CONSTRAINT [fk_Volunteer_ApplicationApprover_Common_Contact]
    FOREIGN KEY ([Contact_ID])
    REFERENCES [dbo].[Common_Contact]
        ([Contact_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'fk_Volunteer_ApplicationApprover_Common_Contact'
CREATE INDEX [IX_fk_Volunteer_ApplicationApprover_Common_Contact]
ON [dbo].[Volunteer_ApplicationApprover]
    ([Contact_ID]);
GO

-- Creating foreign key on [ApprovalType_ID] in table 'Volunteer_ApplicationApprover'
ALTER TABLE [dbo].[Volunteer_ApplicationApprover]
ADD CONSTRAINT [fk_Volunteer_ApplicationApprover_Volunteer_ApplicationApprovalType]
    FOREIGN KEY ([ApprovalType_ID])
    REFERENCES [dbo].[Volunteer_ApplicationApprovalType]
        ([ApprovalType_ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'fk_Volunteer_ApplicationApprover_Volunteer_ApplicationApprovalType'
CREATE INDEX [IX_fk_Volunteer_ApplicationApprover_Volunteer_ApplicationApprovalType]
ON [dbo].[Volunteer_ApplicationApprover]
    ([ApprovalType_ID]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------