﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Planetshakers.Core.Models;

namespace Planetshakers.Events.Models
{
    public class ConferencePriorityType
    {
        public int ConferencePriorityType_ID { get; set; }
        public int? ConferenceID { get; set; }
        public int PriorityType_ID { get; set; }
        public bool inactive { get; set; }
        public string PriorityType { get; set; }
    }
    public class PriorityTypeManagement
    {
        public int Conference_ID { get; set; }
        public int Selected_PriorityType_ID { get; set; }
        public bool displayPriorityType { get; set; }
        public List<EventModel> _events
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var _events = (from ec in context.Events_Conference
                                   join ev in context.Events_Venue
                                   on ec.Conference_ID equals ev.Conference_ID
                                   orderby ev.ConferenceStartDate descending
                                   select new EventModel
                                   {
                                       ConferenceID = ec.Conference_ID,
                                       ConferenceName = ec.ConferenceName
                                   }).ToList();

                    return _events;
                }
            }
        }
        public IEnumerable<SelectListItem> EventList { get { return new SelectList(_events, "ConferenceID", "ConferenceName"); } }
    }
}