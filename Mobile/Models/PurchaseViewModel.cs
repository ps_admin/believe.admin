﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using Planetshakers.Core.Models;

namespace Planetshakers.Events.Models
{
    public class PurchaseViewModel
    {
        public int selectedID { get; set; }
        public Common_Contact selectedContact
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var contact = (from cc in context.Common_Contact
                                   where cc.Contact_ID == selectedID
                                   select cc).FirstOrDefault();

                    return contact;
                }
            }
        }
        public List<ContactDetails> searchList { get; set; }

        public int selectedEvent { get; set; }
        public List<SelectListItem> event_list
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    DateTime today = DateTime.Today;

                    var list = (from ec in context.Events_Conference
                                join ev in context.Events_Venue
                                    on ec.Conference_ID equals ev.Conference_ID
                                orderby ev.ConferenceStartDate descending
                                select new SelectListItem
                                {
                                    Value = ec.Conference_ID.ToString().Trim(),
                                    Text = ec.ConferenceName
                                }).ToList();

                    return list;
                }
            }                
        }
        
        public List<Events_RegistrationType> rego_type_list
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = (from ert in context.Events_RegistrationType
                                where ert.Conference_ID == selectedEvent
                                orderby ert.SortOrder
                                select ert).ToList();

                    return list;
                }
            }
        }

        public int selectedPaymentType { get; set; }
        public List<SelectListItem> payment_type_list
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = (from pt in context.Common_PaymentType
                                select new SelectListItem
                                {
                                    Value = pt.PaymentType_ID.ToString().Trim(),
                                    Text = pt.PaymentType
                                }).ToList();

                    //For by passing credit card payment use
                    list.Insert(2, new SelectListItem { Value = "3", Text = "Credit Card - STRIPE" });
                    list.Insert(3, new SelectListItem { Value = "10000", Text = "Credit Card - ADMIN" }); //Chosen a big number so there is no chance for payment type to be that id
                    list.Remove(list.Find(x => x.Text == "Credit Card"));

                    return list;
                }
            }
        }
        public List<int> qty { get; set; }

        public IDictionary<int, int> RegoTypes { get; set; }

        [Display(Name = "Comments")]
        public string Comments { get; set; }

        public int Venue_ID
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    int id = (from ev in context.Events_Venue
                              where ev.Conference_ID == selectedEvent
                              select ev.Venue_ID).FirstOrDefault();

                    return id;                    
                }
            }
        }

        [Display(Name = "First Name")]
        public string NewFirstName { get; set; }

        [Display(Name = "Last Name")]
        public string NewLastName { get; set; }

        [Display(Name = "Mobile")]
        public string NewMobile { get; set; }

        [Display(Name = "Email")]
        public string NewEmail { get; set; }

        [Display(Name = "Gender")]
        public string NewGender { get; set; }
        public List<SelectListItem> GenderList
        {
            get
            {
                var selectlist = new List<SelectListItem>
                {
                    new SelectListItem { Text = "Male", Value = "Male" },
                    new SelectListItem { Text = "Female", Value = "Female"}
                };

                return selectlist;
            }
        }

        [Display(Name = "Attended?")]
        public bool NewAttended { get; set; }

        //public CartModel cartModel { get; set; }

        public String currency
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var cur = (from ec in context.Events_Conference
                               join cba in context.Common_BankAccount
                               on ec.BankAccount_ID equals cba.BankAccount_ID
                               where ec.Conference_ID == selectedEvent
                               select cba.Currency).FirstOrDefault();
                    return cur;
                }
            }
        }

        public List<int> previous { get; set; }


    }
}