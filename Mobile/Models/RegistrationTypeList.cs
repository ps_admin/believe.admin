﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Planetshakers.Events.Models
{
    public class RegistrationTypeList
    {
        public int Conference_ID { get; set; }

        public List<Events_RegistrationType> _conference_regotype
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var _regoType = (from et in context.Events_RegistrationType
                                      where et.Conference_ID == Conference_ID
                                      select et).ToList();

                    return _regoType;
                }
            }
        }

        public IEnumerable<SelectListItem> ConferenceRegotypeList { get { return new SelectList(_conference_regotype, "RegistrationType_ID", "RegistrationType"); } }
    }
}
