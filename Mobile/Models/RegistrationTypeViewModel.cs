﻿using System.Collections.Generic;
using Planetshakers.Core.Models;
using System.Linq;
using System.Web.Mvc;

namespace Planetshakers.Events.Models
{
    public class RegistrationTypeViewModel
    {
        public string SelectedConference { get; set; }

        public int SelectedConferenceID { get; set; }

        public SetUpRegistrationType selectedRegoType { get; set; }

        public List<SetUpRegistrationType> RegistrationTypesList { get; set; }

        public List<SelectListItem> QuestionList
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = (from q in context.Volunteer_ApplicationQuestion
                                where q.Active == true
                                orderby q.SortOrder
                                select new SelectListItem
                                {
                                    Value = q.Question_ID.ToString().Trim(),
                                    Text = q.ShortName
                                }).ToList();
                    return list;
                }
            }
        }

        public List<SelectListItem> DocumentTypeList
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = (from q in context.Volunteer_DocumentType
                                where q.Active == true
                                orderby q.SortOrder
                                select new SelectListItem
                                {
                                    Value = q.DocumentType_ID.ToString().Trim(),
                                    Text = q.DocumentType
                                }).ToList();
                    return list;
                }
            }
        }

        public List<SelectListItem> event_list
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {

                    var list = (from ec in context.Events_Conference
                                join ev in context.Events_Venue
                                    on ec.Conference_ID equals ev.Conference_ID
                                orderby ev.ConferenceStartDate descending
                                select new SelectListItem
                                {
                                    Value = ec.Conference_ID.ToString().Trim(),
                                    Text = ec.ConferenceName
                                }).ToList();

                    return list;
                }
            }
        }

        public int minAge
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var age = (from v in context.Events_Conference
                               where v.Conference_ID == SelectedConferenceID
                               select v.PlanetKidsMinAge).FirstOrDefault();
                    return (int)age;
                }
            }
        }

        public int maxAge
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var age = (from v in context.Events_Conference
                               where v.Conference_ID == SelectedConferenceID
                               select v.PlanetKidsMaxAge).FirstOrDefault();
                    return (int)age;
                }
            }
        }

        public void SetRegistrationTypesList(int ConferenceID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {

                var list = (from er in context.Events_RegistrationType
                            join ec in context.Events_Conference
                            on er.Conference_ID equals ec.Conference_ID
                            join ba in context.Common_BankAccount
                            on ec.BankAccount_ID equals ba.BankAccount_ID
                            where er.Conference_ID == ConferenceID
                            orderby er.SortOrder
                            select new SetUpRegistrationType
                            {
                                ConferenceID = er.Conference_ID,
                                ConferenceName = ec.ConferenceName,
                                StartDate = er.StartDate,
                                EndDate = er.EndDate,
                                IsCollegeRegoType = er.IsCollegeRegistrationType,
                                ApplicationRegoType = er.ApplicationRegistrationType,
                                AddOnRegistrationType = (bool)er.AddOnRegistrationType,
                                RegistrationCost = er.RegistrationCost,
                                RegistrationTypeID = er.RegistrationType_ID,
                                RegistrationTypeName = er.RegistrationType,
                                Currency = ba.Currency,
                                Limit = er.Limit,
                                hasFundingGoal = (from fg in context.Events_FundingGoal where fg.RegistrationType_ID == er.RegistrationType_ID select fg).Any(),
                            }).ToList();
                this.RegistrationTypesList = list;
            }
        }

        public void addRegistrationType(Events_RegistrationType newRegoType)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var regoTypeSortOrder = (from egt in context.Events_RegistrationType
                                         where egt.Conference_ID == newRegoType.Conference_ID
                                         orderby egt.SortOrder descending
                                         select egt.SortOrder).FirstOrDefault();

                newRegoType.SortOrder = regoTypeSortOrder;
                context.Events_RegistrationType.Add(newRegoType);
                context.SaveChanges();

                var venue_id = (from v in context.Events_Venue
                                where v.Conference_ID == newRegoType.Conference_ID
                                select v.Venue_ID).FirstOrDefault();
                var rtv = new Events_RegistrationTypeVenue();
                rtv.Venue_ID = venue_id;
                rtv.RegistrationType_ID = newRegoType.RegistrationType_ID;
                context.Events_RegistrationTypeVenue.Add(rtv);

                //Add Events_RegistrationTypeSaleGroup
                //Add Events_RegistrationTypeReportGroup

                context.SaveChanges();
            }
        }

        public void editRegistrationType(Events_RegistrationType editRegoType)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                Events_RegistrationType selectedRegoType = (from ert in context.Events_RegistrationType
                                                            where ert.RegistrationType_ID == editRegoType.RegistrationType_ID
                                                            select ert).FirstOrDefault();

                int sortorder = selectedRegoType.SortOrder;

                selectedRegoType.RegistrationType = editRegoType.RegistrationType;
                selectedRegoType.Limit = editRegoType.Limit;
                selectedRegoType.RegistrationCost = editRegoType.RegistrationCost;
                selectedRegoType.StartDate = editRegoType.StartDate;
                selectedRegoType.EndDate = editRegoType.EndDate;
                selectedRegoType.IsCollegeRegistrationType = editRegoType.IsCollegeRegistrationType;
                selectedRegoType.ApplicationRegistrationType = editRegoType.ApplicationRegistrationType;
                selectedRegoType.AddOnRegistrationType = editRegoType.AddOnRegistrationType;
                context.SaveChanges();
            }
        }

        public bool deleteRegoType(int regoTypeID)
        {
            bool success = false;
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                Events_RegistrationType selectedRegoType = (from erg in context.Events_RegistrationType
                                                            where erg.RegistrationType_ID == regoTypeID
                                                            select erg).FirstOrDefault();


                bool existedSingleRego = (from er in context.Events_Registration
                                          where er.RegistrationType_ID == selectedRegoType.RegistrationType_ID
                                          select er).Any();
                bool existedGroupRego = (from egbr in context.Events_GroupBulkRegistration
                                         where egbr.RegistrationType_ID == selectedRegoType.RegistrationType_ID
                                         select egbr).Any();
                if (!existedSingleRego && !existedGroupRego)
                {
                    var venueType = (from e in context.Events_RegistrationTypeVenue where e.RegistrationType_ID == regoTypeID select e).FirstOrDefault();
                    context.Events_RegistrationTypeVenue.Remove(venueType);
                    context.SaveChanges();
                    success = true;
                    if (selectedRegoType != null) context.Events_RegistrationType.Remove(selectedRegoType);
                    context.SaveChanges();
                }
            }
            return success;
        }

        public bool AddRTquestions(List<int> QuestionList, int RegistrationType_ID)
        {
            if (QuestionList != null)
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    foreach (var id in QuestionList)
                    {
                        var question = new Volunteer_EventQuestion();
                        question.RegistrationType_ID = RegistrationType_ID;
                        question.Question_ID = id;
                        question.Inactive = false;
                        context.Volunteer_EventQuestion.Add(question);
                        context.SaveChanges();
                    }
                }
                return true;
            }
            return false;
        }

        public bool EditRTquestions(List<int> QuestionList, int RegistrationType_ID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                if (QuestionList != null)
                {

                    //add if null, update if inactive
                    foreach (var id in QuestionList)
                    {
                        var question = (from q in context.Volunteer_EventQuestion
                                        where q.Question_ID == id && q.RegistrationType_ID == RegistrationType_ID
                                        select q).FirstOrDefault();
                        if (question != null && question.Inactive)
                        {
                            question.Inactive = false;
                        }
                        else
                        {
                            if (question == null)
                            {
                                var newquestion = new Volunteer_EventQuestion
                                {
                                    RegistrationType_ID = RegistrationType_ID,
                                    Question_ID = id,
                                    Inactive = false
                                };
                                context.Volunteer_EventQuestion.Add(newquestion);
                                context.SaveChanges();
                            }
                        }
                    }
                    //make inactive questions that are not ticked if alr exist
                    var list = (from q in context.Volunteer_EventQuestion
                                where q.RegistrationType_ID == RegistrationType_ID && !QuestionList.Contains(q.Question_ID)
                                select q).ToList();
                    foreach (var item in list)
                    {
                        if (!item.Inactive)
                        {
                            item.Inactive = true;
                        }
                    }
                    context.SaveChanges();


                    return true;
                }
                else
                {
                    var list = (from q in context.Volunteer_EventQuestion
                                where q.RegistrationType_ID == RegistrationType_ID
                                select q).ToList();
                    foreach (var item in list)
                    {
                        if (!item.Inactive)
                        {
                            item.Inactive = true;
                        }
                    }
                    context.SaveChanges();
                }
            }

            return false;
        }

        public bool AddRTdocs(List<int> DocList, int RegistrationType_ID)
        {
            if (DocList != null)
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    foreach (var id in DocList)
                    {
                        var doc = new Volunteer_EventDocument
                        {
                            RegistrationType_ID = RegistrationType_ID,
                            DocumentType_ID = id,
                            Inactive = false
                        };
                        context.Volunteer_EventDocument.Add(doc);
                        context.SaveChanges();
                    }
                }
                return true;
            }
            return false;
        }

        public bool EditRTdocs(List<int> DocList, int RegistrationType_ID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                if (DocList != null)
                {

                    //add if null, update if inactive
                    foreach (var id in DocList)
                    {
                        var doc = (from q in context.Volunteer_EventDocument
                                   where q.DocumentType_ID == id && q.RegistrationType_ID == RegistrationType_ID
                                   select q).FirstOrDefault();
                        if (doc != null && doc.Inactive)
                        {
                            doc.Inactive = false;
                        }
                        else
                        {
                            if (doc == null)
                            {
                                var newdoc = new Volunteer_EventDocument
                                {
                                    RegistrationType_ID = RegistrationType_ID,
                                    DocumentType_ID = id,
                                    Inactive = false
                                };
                                context.Volunteer_EventDocument.Add(newdoc);
                                context.SaveChanges();
                            }
                        }
                    }
                    //make inactive questions that are not ticked if alr exist
                    var list = (from q in context.Volunteer_EventDocument
                                where q.RegistrationType_ID == RegistrationType_ID && !DocList.Contains(q.DocumentType_ID)
                                select q).ToList();
                    foreach (var item in list)
                    {
                        if (!item.Inactive)
                        {
                            item.Inactive = true;
                        }
                    }
                    context.SaveChanges();


                    return true;
                }
                else
                {
                    var list = (from q in context.Volunteer_EventDocument
                                where q.RegistrationType_ID == RegistrationType_ID
                                select q).ToList();
                    foreach (var item in list)
                    {
                        if (!item.Inactive)
                        {
                            item.Inactive = true;
                        }
                    }
                    context.SaveChanges();
                }
            }
            return false;
        }
    }

    public class Event_Question
    {
        public int Question_ID { get; set; }
        public string ShortName { get; set; }
        public bool Inactive { get; set; }
    }
    public class Event_Doc
    {
        public int Doc_ID { get; set; }
        public string Name { get; set; }
        public bool Inactive { get; set; }
    }

}