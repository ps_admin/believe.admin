﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Planetshakers.Core.Models;

namespace Planetshakers.Events.Models
{
    public class RegistrationViewModel
    {        
        public Registration Registration { get; set; }
        public List<Registration> GroupRegistration { get; set; }
        
        //public TicketRegistration Ticket { get; set; }

        public bool IsCoach(Registration rego)
        {
            
            bool iscoach = false;
            using (PlanetshakersEntities entity = new PlanetshakersEntities())
            {
                iscoach = (from ccr in entity.Church_ContactRole
                            join cr in  entity.Church_Role
                            on ccr.Role_ID equals cr.Role_ID
                            where ccr.Contact_ID == rego.ContactID && cr.Deleted == false && cr.Name.Contains("Coach") && ccr.Inactive == false
                           select ccr).Any();
            }
            return iscoach;
            
        }

        public bool hasPlanetBusinessBreakfast(Registration rego)
        {
            bool haspbb = false;
            using (PlanetshakersEntities entity = new PlanetshakersEntities())
            {
                // Change to search by PlanetBusiness Breakfast name and lastest event
                haspbb = (from er in entity.Events_Registration
                          where er.Contact_ID == rego.ContactID && er.Venue_ID == 0
                          select er).Any();

            }
            return haspbb;
        }

        public bool hasLeadershipSummit(Registration rego)
        {
            bool hasls = false;
            using (PlanetshakersEntities entity = new PlanetshakersEntities())
            {
                // Change to search by Leadership Summit name and lastest event
                hasls = (from er in entity.Events_Registration
                          where er.Contact_ID == rego.ContactID && er.Venue_ID == 0
                          select er).Any();

            }
            return hasls;
        }


        public bool IsGroupLeader
        {
            get
            {
                return Registration.ContactID == Registration.GroupLeader_ID;
            }

            set
            {
                this.IsGroupLeader = value;
            }
        }


        public int total_count { get; set; }
        public int unallocated_count { get; set; }
        public int allocated_count { get; set; }

        public int attended_count { get; set; }

        //private int _conference_id;
        //public int Conference_ID
        //{
        //    get
        //    {
                
        //        int id = 0;

        //        using (PlanetshakersEntities entity = new PlanetshakersEntities())
        //        {
        //            id = (from r in entity.vw_Events_RegistrationCheckIn
        //                    where r.Registration_ID == this.Registration.ID
        //                    select r.Conference_ID).FirstOrDefault();

        //            if (id == 0)
        //            {
        //                return _conference_id;
        //            } else
        //            {
        //                return id;
        //            }
        //        }                    
        //    }

        //    set
        //    {
        //        _conference_id = value;
        //    }
        //}

        //public string ConferenceName
        //{
        //    get
        //    {
        //        using (PlanetshakersEntities entity = new PlanetshakersEntities())
        //        {
        //            var name = (from c in entity.Events_Conference
        //                        where c.Conference_ID == this.Conference_ID
        //                        select c.ConferenceName).FirstOrDefault();

        //            return name;
        //        }
        //    }
        //}

        private int _venue_id;

        public int Venue_ID
        {
            get
            {
                using (PlanetshakersEntities entity = new PlanetshakersEntities())
                {
                    var id = (from ev in entity.Events_Registration
                              where ev.Registration_ID == this.Registration.ID
                              select ev.Venue_ID).FirstOrDefault();

                    if (_venue_id == 0)
                        _venue_id = id; 

                    return _venue_id;
                }
            }

            set
            {
                _venue_id = value;
            }
        }

        public List<Registration> searchList { get; set; }
    }

    public class SearchViewModel
    {
        public SearchModel Search { get; set; }
        public ResultsModel Results { get; set; }
        public int Pay_ID { get; set; }
        public int Conference_ID { get; set; }

        #region Select List
        public List<SelectListItem> StateList
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = (from gs in context.Common_GeneralState
                                select new SelectListItem
                                {
                                    Text = gs.State,
                                    Value = gs.State_ID.ToString().Trim()
                                }).ToList();

                    return list;
                }
            }
        }

        public List<SelectListItem> CountryList
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = (from gc in context.Common_GeneralCountry
                                select new SelectListItem
                                {
                                    Text = gc.Country,
                                    Value = gc.Country_ID.ToString().Trim()
                                }).ToList();

                    return list;
                }
            }
        }

        public List<SelectListItem> GenderList
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = new List<SelectListItem>()
                    {
                        new SelectListItem { Text="Male", Value="Male" },
                        new SelectListItem { Text="Female", Value="Female" }
                    };

                    return list;
                }
            }
        }

        public List<SelectListItem> ConferenceList
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = (from ec in context.Events_Conference
                                join ev in context.Events_Venue
                                on ec.Conference_ID equals ev.Conference_ID
                                orderby ev.ConferenceStartDate descending
                                select new SelectListItem
                                {
                                    Text = ec.ConferenceName,
                                    Value = ec.Conference_ID.ToString().Trim()
                                }).ToList();

                    return list;
                }
            }
        }

        public List<SelectListItem> RegistrationTypeList
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = (from ert in context.Events_RegistrationType
                                where ert.Conference_ID == Conference_ID
                                select new SelectListItem {
                                    Text = ert.RegistrationType,
                                    Value = ert.RegistrationType_ID.ToString().Trim()
                                }).ToList();

                    return list;
                }
            }
        }

        public List<SelectListItem> AttendanceOption
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = new List<SelectListItem>
                    {
                        new SelectListItem {Text="Yes", Value="Yes"},
                        new SelectListItem {Text="No", Value="No"}
                    };

                    return list;
                }
            }
        }

        public Payment selectedGroupPayment
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var pay = (from ep in context.Events_GroupPayment
                               join pt in context.Common_PaymentType on ep.PaymentType_ID equals pt.PaymentType_ID
                               join ps in context.Common_PaymentSource on ep.PaymentSource_ID equals ps.PaymentSource_ID
                               where ep.GroupPayment_ID == Pay_ID && (ep.ExpiredIntent != true || ep.ExpiredIntent == null)
                               select new Payment
                               {
                                   ID = ep.GroupPayment_ID,
                                   Category = (ep.PaymentAmount >= 0 ? "Payment" : "Refund"),
                                   PaymentType = pt.PaymentType,
                                   PaymentSource = ps.PaymentSource,
                                   TransactionRef = ep.CCTransactionRef,
                                   Amount = ep.PaymentAmount,
                                   Comment = ep.Comment,
                                   Contact_ID = ep.GroupLeader_ID,
                                   PaymentSource_ID = ep.PaymentSource_ID,
                               }).FirstOrDefault();
                    if (pay.PaymentSource_ID == 2 || pay.PaymentSource_ID == 1)
                    {
                        pay.PaidBy = (from cc in context.Common_Contact where cc.Contact_ID == pay.Contact_ID select cc.FirstName + " " + cc.LastName).FirstOrDefault();
                    }
                    if (pay.PaymentSource_ID == 3)
                    {
                        //var sponsor = (from s in context.Events_Sponsorship
                        //               where s.Sponsorship_ID == pay.Sponsorship_ID
                        //               select new SponsorshipDetails
                        //               {
                        //                   Sponsorship_ID = s.Sponsorship_ID,
                        //                   FirstName = s.FirstName,
                        //                   LastName = s.LastName,
                        //                   Mobile = s.Phone,
                        //                   Email = s.Email,
                        //                   Anonymous = s.Anonymous
                        //               }).FirstOrDefault();
                        //pay.sponsorship = sponsor;
                        pay.PaidBy = "Anonymous";
                    }
                    //Change the credit card type, if pay by credit card but no cctransref means bypassing stripe
                    if (pay.PaymentType == "Credit Card")
                    {
                        if (pay.TransactionRef == "")
                        {
                            pay.PaymentType = "Credit Card - ADMIN";
                        }
                        else
                        {
                            pay.PaymentType = "Credit Card - STRIPE";
                        }
                    }

                    return pay;
                }
            }
        }
        #endregion
    }
}