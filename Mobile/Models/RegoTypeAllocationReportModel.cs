﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web.Mvc;

using Planetshakers.Core.Models;

namespace Planetshakers.Events.Models
{
    public class RegoTypeAllocationReportModel
    {

        public Boolean displayReport = false;

        public int Conference_ID { get; set; }

        public int conferenceSelectionID { get; set; }

        public List<EventModel> _events
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var _events = (from ec in context.Events_Conference
                                   join ev in context.Events_Venue
                                   on ec.Conference_ID equals ev.Conference_ID
                                   orderby ev.ConferenceStartDate descending
                                   select new EventModel
                                   {
                                       ConferenceID = ec.Conference_ID,
                                       ConferenceName = ec.ConferenceName
                                   }).ToList();

                    return _events;
                }
            }
        }
        public IEnumerable<SelectListItem> EventList { get { return new SelectList(_events, "ConferenceID", "ConferenceName"); } }

        public List<RegistrationTypeAllocation> RegoTypeBreakdownList { get; set; }
    }
}