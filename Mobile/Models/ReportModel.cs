﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using Planetshakers.Core.Models;

namespace Planetshakers.Events.Models
{
    public class ReportModel
    {
        // Variable storing data to display summary report for events.  used in Banking Report, Registration and Total Registration report
        public EventSummary eventSummary;
        //Some booleans to help view decide what to display
        public Boolean displayReport = false;
        // Changes from Attendnace, Registration Types, States, Age Groups depending on form submission. 
        public String reportType = null;
        // Idk why this is needed, code in summaryreport.cshtml doesnt work without this.
        [Display(Name = "Event")]
        public int Conference_ID { get; set; }
        public int conferenceSelectionID { get; set; }
        // Variables for banking report
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Boolean IncludeTransactionReport { get; set; }
        public string selectedUser { get; set; }
        public int selectedDateOpt { get; set; }
        public string selectedSingleGroup { get; set; }
        // Holds all possible venue_id that can be selected. 
        public IEnumerable<SelectListItem> Venue_IDs { get; set; }

        // TODO: this needs to be changed to generate the events dropdown in a sustainable manner. 
        public List<EventModel> _events
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var _events = (from ec in context.Events_Conference
                                   join ev in context.Events_Venue
                                   on ec.Conference_ID equals ev.Conference_ID
                                   orderby ev.ConferenceStartDate descending
                                   select new EventModel
                                   {
                                       ConferenceID = ec.Conference_ID,
                                       ConferenceName = ec.ConferenceName
                                   }).ToList();

                    return _events;
                }
            }
        }
        public IEnumerable<SelectListItem> EventList { get { return new SelectList(_events, "ConferenceID", "ConferenceName"); } }

        public List<User> _users
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var _users = (from cc in context.Common_Contact
                                  join ccr in context.Church_ContactRole
                                  on cc.Contact_ID equals ccr.Contact_ID
                                  join cdr in context.Common_ContactDatabaseRole
                                  on cc.Contact_ID equals cdr.Contact_ID into x
                                  from xc in x.DefaultIfEmpty()
                                  where (ccr.Role_ID == 73 || xc.DatabaseRole_ID == 62) && ccr.Inactive == false
                                  select new User
                                  {
                                      ContactID = cc.Contact_ID,
                                      FirstName = cc.FirstName,
                                      LastName = cc.LastName
                                  }).Distinct().OrderBy(xc => xc.FirstName).ToList();

                    return _users;
                }
            }
        }

        public List<TimeSelection> _availableTimeSelection
        {
            get
            {
                List<TimeSelection> newlist = new List<TimeSelection>();
                newlist.Add(new TimeSelection { dateId = 0, startdate = DateTime.Today, enddate = DateTime.Now, selectionname = "-- Select Date --" });
                newlist.Add(new TimeSelection { dateId = 1, startdate = DateTime.Today, enddate = DateTime.Now, selectionname = "Today" });
                newlist.Add(new TimeSelection { dateId = 2, startdate = DateTime.Now.AddDays(-1), enddate = DateTime.Now, selectionname = "Last 24 hours" });
                newlist.Add(new TimeSelection { dateId = 3, startdate = DateTime.Now.AddDays(-7), enddate = DateTime.Now, selectionname = "Last 7 days" });
                newlist.Add(new TimeSelection { dateId = 4, startdate = DateTime.Now.AddDays(-14), enddate = DateTime.Now, selectionname = "Last 14 days" });
                newlist.Add(new TimeSelection { dateId = 5, startdate = DateTime.Now.AddDays(-31), enddate = DateTime.Now, selectionname = "Last 31 days" });
                newlist.Add(new TimeSelection { dateId = 6, startdate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1), enddate = DateTime.Now, selectionname = "Current month" });
                newlist.Add(new TimeSelection { dateId = 7, startdate = new DateTime(DateTime.Today.Year, 1, 1), enddate = DateTime.Now, selectionname = "Current year" });
                newlist.Add(new TimeSelection { dateId = 9, startdate = new DateTime(1753,1,1), enddate = DateTime.Now, selectionname = "All Time" });
                newlist.Add(new TimeSelection { dateId = 8, startdate = DateTime.Today, enddate = DateTime.Now, selectionname = "Custom" });
                

                return newlist;
            }
        }


        public IEnumerable<SelectListItem> singlegroupSelect
        {
            get
            {
                List<SelectListItem> list = new List<SelectListItem>();
                list.Add(new SelectListItem
                {
                    Text = "Single",
                    Value = "single"
                });
                list.Add(new SelectListItem
                {
                    Text = "Group",
                    Value = "group"
                });
                return list;
            }
        }

        public IEnumerable<SelectListItem> UserList { get { return new SelectList(_users, "ContactID", "FullName"); } }
        public IEnumerable<SelectListItem> Dateselection { get { return new SelectList(_availableTimeSelection, "dateId", "selectionname"); } }
        public List<BulkRegistration> bulkrego { get; set; }
        public List<GroupPayment> grouppayment { get; set; }
        public List<SinglePayment> singlepayment { get; set; }
        public decimal paymentSum { get; set; }
        public Common_PaymentType paymentType { get; set; } //used in banking report to find sponsorship payment type
        public int totalRego { get; set; }

        //For summary report rego type allocation use
        public List<RegistrationTypeAllocation> RegoTypeBreakdownList { get; set; }

        public List<string> yearList { get; set; }

        public string ConferenceName { get; set; }

        public EventsTotalRegistration pastConferenceReport = new EventsTotalRegistration();
        public string variant(int curentYear, int previousYear)
        {
            if (curentYear != 0 && previousYear != 0)
            {
                return ((curentYear - previousYear) * 100 / previousYear).ToString("0");
            }
            else
            {
                return "0";
            }

        }

        public string Currency
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var _currency = (from ec in context.Events_Conference
                                     join cba in context.Common_BankAccount
                                     on ec.BankAccount_ID equals cba.BankAccount_ID
                                     where ec.Conference_ID == Conference_ID
                                     select cba.Currency).Single();

                    return _currency;
                }
            }
        }
        public List<string> selectedYears { get; set; }

        /*This following function is the main functionality for total registration report.
         Depends on the section to render, do some string formatting and also display in the right order, 
         by putting them in html string.*/
        #region Total Registration Report
        public string totalRegistrationViewResult(string section, string currentColumn)
        {
            string htmlString = "";

            switch (section)
            {
                case "totalRegoType":

                    DataTable table = pastConferenceReport.totalRegoTypeBreakdown.ds.Tables[0];

                    /*2 case: Total, or just loop through other columns */
                    if (currentColumn == "grandTotal")
                    {
                        var grantTotal = 0;
                        grantTotal = pastConferenceReport.totalRegoTypeBreakdown.grandTotal;
                        htmlString += "<td style = 'text-align:right; padding-left: 30px;'><strong>" + grantTotal + "</strong></td>";
                    }
                    else
                    {
                        DataRow row = table.Rows[0];
                        var currentValue = 0;
                        currentValue = Int32.Parse(pastConferenceReport.totalRegoTypeBreakdown.ds.Tables[0].Rows[0][currentColumn].ToString());
                        htmlString += "<td style = 'text-align:right; padding-left: 30px;'>" + currentValue + "</td>";
                    }

                    break;
                //case "campusBreakdown":

                //    int numTable = pastConferenceReport.totalCampusBreakdown.ds.Tables[0].Rows.Count;
                //    string campusTotal = "";
                //    string campusAllocated = "";
                //    string campusUnallocated = "";
                //    string totalInternal = "";
                //    string totalExternal = "";

                //    /*Go through every single row from stored procedure result and set title for view, also pull the data and put them together, by looping
                //     through every year*/
                //    for (int i = 0; i < numTable; i++)
                //    {
                //        string campus = pastConferenceReport.totalCampusBreakdown.ds.Tables[0].Rows[i]["Campus"].ToString();
                //        switch (campus)
                //        {
                //            //Need to do title string processing for melbourne city campus.
                //            case "Melbourne":
                //                campusTotal = "<tr><td><strong>MELBOURNE TOTAL</strong></td>";
                //                campusAllocated = "<tr><td>Allocated</td>";
                //                campusUnallocated = "<tr><td>Unallocated</td>";
                //                break;

                //            //External record title is different
                //            case "External":
                //                campusTotal = "<tr style= 'background: #FBFCE5;'><td><strong>GRAND TOTAL</strong></td>";
                //                campusAllocated = "<tr><td><strong>EXTERNAL ALLOCATED</strong></td>";
                //                campusUnallocated = "<tr><td><strong>EXTERNAL UNALLOCATED</strong></td>";
                //                break;

                //            //As default, pull from database the current campus name and turn them to big cap, with total at the back as title
                //            default:
                //                campusTotal = "<tr><td><strong>" + campus.ToUpper() + " TOTAL</strong></td>";
                //                campusAllocated = "<tr><td>Allocated</td>";
                //                campusUnallocated = "<tr><td>Unallocated</td>";
                //                break;
                //        }

                //        if (campus != "External")
                //        {

                //            DataTable currentYearTable = pastConferenceReport.totalCampusBreakdown.ds.Tables[0];
                //            DataRow currentYearTableRow = currentYearTable.Rows[i];

                //            int currentTotalValue = Int32.Parse(currentYearTableRow["Total"].ToString());
                //            int currentAllocatedValue = Int32.Parse(currentYearTableRow["Allocated"].ToString());
                //            int currentUnallocatedValue = Int32.Parse(currentYearTableRow["Unallocated"].ToString());

                //            campusTotal += "<td style = 'text-align:right; padding-left: 30px;'><strong>" + currentTotalValue + "</strong></td>";
                //            campusAllocated += "<td style = 'text-align:right; padding-left: 30px;'>" + currentAllocatedValue + "</td>";
                //            campusUnallocated += "<td style = 'text-align:right; padding-left: 30px;'>" + currentUnallocatedValue + "</td>";



                //            campusTotal += "</tr>";
                //            campusAllocated += "</tr>";
                //            campusUnallocated += "</tr>";
                //            htmlString += campusTotal += campusAllocated += campusUnallocated; //putting total, allocated, unallocated together.

                //        }

                //        if (i == numTable - 2) //Before reaching the external record, do a subtotal first. Assuming that the last record will be external record.
                //        {
                //            campusAllocated = "<tr><td><strong>SUBTOTAL ALLOCATED</strong></td>";
                //            campusUnallocated = "<tr><td><strong>SUBTOTAL UNALLOCATED</strong></td>";
                //            totalInternal = "<tr style= 'background: #FBFCE5;'><td><strong>TOTAL INTERNAL</strong></td>";


                //            int currentAllocatedValue = 0;
                //            int currentUnallocatedValue = 0;
                //            currentAllocatedValue = pastConferenceReport.totalCampusBreakdown.subtotalAllocated;
                //            currentUnallocatedValue = pastConferenceReport.totalCampusBreakdown.subtotalUnallocated;

                //            campusAllocated += "<td style = 'text-align:right; padding-left: 30px;'><strong>" + currentAllocatedValue.ToString() + "</strong></td>";
                //            campusUnallocated += "<td style = 'text-align:right; padding-left: 30px;'><strong>" + currentUnallocatedValue.ToString() + "</strong></td>";



                //            int currenttotalinternalvalue = currentUnallocatedValue + currentAllocatedValue;
                //            totalInternal += "<td style = 'text-align:right; padding-left: 30px;'><strong>" + currenttotalinternalvalue + "</strong></td>";


                //            campusAllocated += "</tr>";
                //            campusUnallocated += "</tr>";
                //            totalInternal += "</tr>";
                //            htmlString += campusAllocated += campusUnallocated;
                //        }

                //        if (campus == "External")
                //        {
                //            totalExternal = "<tr style= 'background: #FBFCE5;'><td><strong>TOTAL EXTERNAL</strong></td>";

                //            int currentAllocatedValue = 0;
                //            int currentUnallocatedValue = 0;
                //            int currentGrandTotalValue = 0;
                //            DataTable currentYearTable = pastConferenceReport.totalCampusBreakdown.ds.Tables[0];
                //            DataRow currentYearRow = currentYearTable.Rows[i];

                //            currentAllocatedValue += !String.IsNullOrEmpty(currentYearRow["Allocated"].ToString()) ? Int32.Parse(currentYearRow["Allocated"].ToString()) : 0;
                //            currentUnallocatedValue += !String.IsNullOrEmpty(currentYearRow["Unallocated"].ToString()) ? Int32.Parse(currentYearRow["Unallocated"].ToString()) : 0;
                //            currentGrandTotalValue += pastConferenceReport.totalCampusBreakdown.grandTotal;

                //            int currentTotalExternal = currentAllocatedValue + currentUnallocatedValue;
                //            campusTotal += "<td style = 'text-align:right; padding-left: 30px;'><strong>" + currentGrandTotalValue + "</strong></td>";
                //            campusAllocated += "<td style = 'text-align:right; padding-left: 30px;'>" + currentAllocatedValue + "</td>";
                //            campusUnallocated += "<td style = 'text-align:right; padding-left: 30px;'>" + currentUnallocatedValue + "</td>";
                //            totalExternal += "<td style = 'text-align:right; padding-left: 30px;'><strong>" + currentTotalExternal + "</strong></td>";

                //            campusTotal += "</tr>";
                //            campusAllocated += "</tr>";
                //            campusUnallocated += "</tr>";
                //            totalExternal += "</tr>";
                //            htmlString += totalInternal += campusAllocated += campusUnallocated += totalExternal += campusTotal;
                //        }

                //    }
                //    break;
                //case "ministryBreakdown":
                //    int numTableColumn = pastConferenceReport.totalMinistryBreakdown.ds.Tables[0].Rows.Count;
                //    string ministry = "<tr><td><strong>PlanetKids</strong></td>";
                //    string subTitle = "";
                //    for (int i = 0; i < 4; i++)
                //    {
                //        switch (i)
                //        {
                //            case 1:
                //                ministry = "<tr><td><strong>PlanetBOOM</strong></td>";
                //                break;
                //            case 2:
                //                ministry = "<tr><td><strong>PlanetUni</strong></td>";
                //                break;
                //            case 3:
                //                ministry = "<tr><td><strong>Adults</strong></td>";
                //                break;
                //        }

                //        int currentTotalValue = 0;
                //        DataTable table1 = pastConferenceReport.totalMinistryBreakdown.ds.Tables[0];
                //        DataRow row = table1.Rows[i];

                //        currentTotalValue += Int32.Parse(row["Total"].ToString());
                //        ministry += "<td style = 'text-align:right; padding-left: 30px;'>" + currentTotalValue + "</td>";

                //        ministry += "</tr>";
                //        htmlString += ministry;
                //    }

                //    for (int k = 0; k < 5; k++)
                //    {
                //        DataTable table1 = pastConferenceReport.totalMinistryBreakdown.ds.Tables[0];
                //        int currentTotalValue = 0;
                //        string total;
                //        switch (k)
                //        {
                            
                //            case 0:
                //                subTitle = "<tr><td><strong>NO MINISTRY</strong></td>";

                //                currentTotalValue = 0;
                //                total = table1.Rows[5]["Total"].ToString();
                //                currentTotalValue += (total != "") ? Int32.Parse(total) : 0;
                //                subTitle += "<td style = 'text-align:right; padding-left: 30px;'>" + currentTotalValue + "</td>";

                //                break;
                //            case 1:
                //                subTitle = "<tr><td><strong>EXTERNAL</strong></td>";

                //                currentTotalValue = 0;
                //                total = table1.Rows[4]["Total"].ToString();
                //                currentTotalValue += (total != "")?Int32.Parse(total) : 0;
                                
                //                subTitle += "<td style = 'text-align:right; padding-left: 30px;'>" + currentTotalValue + "</td>";

                //                break;
                //            case 2:
                //                subTitle = "<tr><td><strong>TOTAL ALLOCATED</strong></td>";

                //                currentTotalValue = 0;

                //                total = table1.Rows[6]["Total"].ToString();
                //                currentTotalValue += (total != "") ? Int32.Parse(total) : 0;
                //                subTitle += "<td style = 'text-align:right; padding-left: 30px;'><strong>" + currentTotalValue + "</strong></td>";

                //                break;
                //            case 3:
                //                subTitle = "<tr><td><strong>TOTAL UNALLOCATED</strong></td>";
                //                currentTotalValue = 0;
                //                total = table1.Rows[7]["Total"].ToString();
                //                currentTotalValue += (total != "") ? Int32.Parse(total) : 0;
                //                subTitle += "<td style = 'text-align:right; padding-left: 30px;'>" + currentTotalValue + "</td>";

                //                break;
                //            case 4:
                //                subTitle = "<tr><td><strong>GRAND TOTAL</strong></td>";
                //                currentTotalValue = 0;

                //                total = table1.Rows[8]["Total"].ToString();
                //                currentTotalValue += (total != "") ? Int32.Parse(total) : 0;
                //                subTitle += "<td style = 'text-align:right; padding-left: 30px;'><strong>" + currentTotalValue + "</strong></td>";

                //                break;
                //        }
                //        subTitle += "</tr>";
                //        htmlString += subTitle;
                //    }
                //    break;
            }

            return htmlString;
        }
        #endregion


        /*This follow is the main functionality of Check In Report*/
        public IEnumerable<SelectListItem> attendedSelect
        {
            get
            {
                List<SelectListItem> list = new List<SelectListItem>();
                list.Add(new SelectListItem
                {
                    Text = "No",
                    Value = "False"
                });
                list.Add(new SelectListItem
                {
                    Text = "Yes",
                    Value = "True"
                });
                return list;
            }
        }

        public List<CheckInDetails> checkInList { get; set; }
        public List<SelectListItem> RegistrationTypeList { get; set; }
        public List<SelectListItem> ConferenceList
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = (from c in context.Events_Conference
                                orderby c.Conference_ID descending
                                select new SelectListItem
                                {
                                    Text = c.ConferenceName,
                                    Value = c.Conference_ID.ToString()
                                }).ToList();

                    return list;
                }
            }
        }
        public List<int> SelectedRegoType { get; set; }
    }

    public class CustomReportModel
    {
        public int Conference_ID { get; set; }

        public List<int> PriorityTypes { get; set; }

        public List<int> EmailSent { get; set; }

        public bool LeadershipSummitRegistered { get; set; }

        public bool TagIssued { get; set; }

        public string ChurchName { get; set; }

        public string ChurchRole { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }

    public class RegistrantReportModel
    {
        public IEnumerable<ObjConfList> conferenceList;
    }

    public class VoucherReportModel
    {
        public List<Voucher> VoucherList { get; set; }
        public List<Payment> PaymentList { get; set; }
        public string campus { get; set; }
        public string ministry { get; set; }
        public string inactive { get; set; }
        public int conference_ID { get; set; }
        public bool displayReport { get; set; }
        public bool isFilter { get; set; }
        public decimal GrandTotal { get; set; }
        public decimal Used { get; set; }
        public decimal Remaining { get; set; }
        public List<SelectListItem> ConferenceList
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = (from c in context.Events_Conference
                                orderby c.Conference_ID descending
                                select new SelectListItem
                                {
                                    Text = c.ConferenceName,
                                    Value = c.Conference_ID.ToString()
                                }).ToList();

                    return list;
                }
            }
        }
        public List<SelectListItem> CampusList
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = (from c in context.Church_Campus
                                select new SelectListItem
                                {
                                    Text = c.ShortName,
                                    Value = c.Name
                                }).ToList();

                    return list;
                }
            }
        }
        public List<SelectListItem> MinistryList
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = (from m in context.Church_Ministry
                                select new SelectListItem
                                {
                                    Text = m.Name,
                                    Value = m.Name
                                }).ToList();

                    return list;
                }
            }
        }

        public List<SelectListItem> InOutSelect
        {
            get
            {
                List<SelectListItem> list = new List<SelectListItem>();

                list.Add(new SelectListItem
                {
                    Text = "Active",
                    Value = "Active"
                });
                list.Add(new SelectListItem
                {
                    Text = "Inactive",
                    Value = "Inactive"
                });
                return list;
            }
        }
    }
}
