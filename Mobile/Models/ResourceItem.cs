﻿namespace Planetshakers.Events.Models
{
    public class ResourcesItem
    {
        public string DownloadUrl { get; set; }
        public string Name { get; set; }
        public string Size { get; set; }
        public bool ShowInResources { get; set; }
        public int Index { get; set; }
    }
}