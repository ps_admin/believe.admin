﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

using Planetshakers.Core.Models;

namespace Planetshakers.Events.Models
{
    public class SelfAllocateModel
    {
        public int Contact_ID { get; set; }

        public int Venue_ID { get; set; }

        public int Conference_ID { get; set; }

        public int RegistrationType_ID { get; set; }

        public List<UnallocatedRegistration> ExistedRegistration { get; set; }

        public bool Attended { get; set; }

        public bool isAllocatedSameRego { get
            {
                bool hasSameRego = false;
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var hasrego = (from ev in context.Events_Registration
                                   join rt in context.Events_RegistrationType
                                   on ev.RegistrationType_ID equals rt.RegistrationType_ID
                                   where ev.Contact_ID == Contact_ID && ev.Venue_ID == Venue_ID
                                   select new UnallocatedRegistration
                                   {
                                       RegistrationType = rt.RegistrationType,
                                       RegistrationType_ID = ev.RegistrationType_ID
                                   }).ToList();
                    ExistedRegistration = hasrego;
                    if (AvailableRegistrationTypeList.Count == 0)
                    {
                        hasSameRego = true;
                    }
                }
                return hasSameRego;
            }
        }

        public string RegistrationType
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var name = (from ert in context.Events_RegistrationType
                                where ert.RegistrationType_ID == RegistrationType_ID
                                select ert.RegistrationType).FirstOrDefault();

                    return name;
                }
            }
        }

        //updated for believe
        public List<UnallocatedRegistration> unallocatedList
        {
            get
            {
                var unallocated_regos = new List<UnallocatedRegistration>();

                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = (from egbr in context.Events_GroupBulkRegistration
                                where egbr.GroupLeader_ID == Contact_ID
                                        && egbr.Venue_ID == Venue_ID
                                select egbr).ToList();



                    foreach (var item in list)
                    {
                        var unallocated = new UnallocatedRegistration();

                        unallocated.GroupLeader_ID = item.GroupLeader_ID;
                        unallocated.Venue_ID = item.Venue_ID;

                        unallocated.GroupLeader = (from eg in context.Events_Group
                                                   where eg.GroupLeader_ID == item.GroupLeader_ID
                                                   select eg.GroupName).FirstOrDefault();

                        unallocated.RegistrationType_ID = item.RegistrationType_ID;

                        unallocated.RegistrationType = (from ert in context.Events_RegistrationType
                                                        where ert.RegistrationType_ID == item.RegistrationType_ID
                                                        select ert.RegistrationType).FirstOrDefault();

                        unallocated.Venue_ID = item.Venue_ID;
                        //unallocated.Allocated = (int)item.Allocated;
                        //unallocated.Unallocated = (int)item.UnAllocated;

                        unallocated_regos.Add(unallocated);
                    }

                    return unallocated_regos;
                }
            }
        }

        public List<SelectListItem> AvailableRegistrationTypeList
        {
            get
            {
                var list = unallocatedList.Where(x => x.Unallocated >= 1).ToList();
                var notRepeatedRegoType = new List<SelectListItem>();
                var selectlist = new List<SelectListItem>();

                if (ExistedRegistration.Count != 0)
                {
                    foreach (var rego in ExistedRegistration)
                    {
                        list.Remove(list.Find(x => x.RegistrationType_ID == rego.RegistrationType_ID));
                    }
                }

                foreach (var i in list)
                {
                    notRepeatedRegoType.Add(new SelectListItem { Text = i.RegistrationType, Value = i.RegistrationType_ID.ToString() });
                }
                
                return notRepeatedRegoType;
            }
        }

        


    }
}