﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Planetshakers.Core.Models;

namespace Planetshakers.Events.Models
{
    public class SignUpLink
    {
        public string EventName { get; set; }
        public int Event_ID { get; set; }
        public int Venue_ID { get; set; }
    }
    public class SignUpLinkViewModel
    {
        public List<SignUpLink> SignUpLinks
        {
            get
            {
                using(PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = (from c in context.Events_Conference
                                join v in context.Events_Venue on c.Conference_ID equals v.Conference_ID
                                where c.Visibility == true
                                select new SignUpLink
                                {
                                    EventName = c.ConferenceName,
                                    Event_ID = c.Conference_ID,
                                    Venue_ID = v.Venue_ID
                                }).ToList();
                    return list;
                }
            }
        }

    }
}