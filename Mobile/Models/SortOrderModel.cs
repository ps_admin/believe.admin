﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Planetshakers.Core.Models;

namespace Planetshakers.Events.Models
{
    public class SortOrderModel
    {
        public int Conference_ID { get; set; }
        public int Venue_ID { get
            {
                var id = 0;
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    id = (from ev in context.Events_Venue
                          where ev.Conference_ID == Conference_ID
                          select ev.Venue_ID).FirstOrDefault();
                }
                return id;
            }
        }
        public List<Events_Conference> conf_list
        {
            get
            {
                List<Events_Conference> conflist = new List<Events_Conference>();
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var confs = (from c in context.Events_Conference
                                  join v in context.Events_Venue
                                         on c.Conference_ID equals v.Conference_ID
                                  join rt in context.Events_RegistrationType
                                         on c.Conference_ID equals rt.Conference_ID
                                  where c.ShowConferenceOnWeb == true &&
                                        (c.AllowOnlineRegistrationSingle ||
                                        c.AllowOnlineRegistrationGroup) &&
                                        v.ConferenceEndDate >= DateTime.Now &&
                                        rt.StartDate <= DateTime.Now
                                  orderby c.SortOrder
                                  select c).Distinct().ToList();
                    foreach (var conf in confs)
                    {
                        conflist.Add(conf);
                    }
                    conflist = conflist.OrderBy(x => x.SortOrder).ToList();
                }
                return conflist;
            }
        }

        public void SortConference(List<SortOrderItem> ConferenceList)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                foreach(var item in ConferenceList)
                {
                    Events_Conference confitem = (from ec in context.Events_Conference
                                                  where ec.Conference_ID == item.ItemID
                                                  select ec).FirstOrDefault();
                    if(confitem != null)
                    {
                        confitem.SortOrder = item.ItemSortOrderID;
                        context.SaveChanges();
                    }
                }
            }
        }

        public void SortRegistrationType(List<SortOrderItem> RegistrationTypeList)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                foreach(var item in RegistrationTypeList)
                {
                    Events_RegistrationType regoType = (from ec in context.Events_RegistrationType
                                                  where ec.RegistrationType_ID == item.ItemID
                                                  select ec).FirstOrDefault();
                    if(regoType != null)
                    {
                        regoType.SortOrder = item.ItemSortOrderID;
                        context.SaveChanges();
                    }
                }
            }
        }
    }
}