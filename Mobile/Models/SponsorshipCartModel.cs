﻿using Planetshakers.Core.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Planetshakers.Events.Models
{
    public class SponsorshipCartModel
    {
        public string ErrorMessage { get; set; }

        // Logged in contact
        public int ContactId { get; set; }

        // Items in the cart
        public SponsorshipCartItem Cart { get; set; }

        // Event 
        public Event Event { get; set; }

        // Total cost
        public decimal GrandTotal { get { return Cart.totalSponsoredAmount; } }

        // This is used to prevent multiple transactionsfrom executing
        public bool ProcessingPayment { get; set; }

        // CC Info
        public CCPaymentModel CCPayment { get; set; }

        public int PaymentType_ID { get; set; }
        public string Comment { get; set; }

        public List<SelectListItem> PaymentTypeList
        {
            get
            {
                var list = new List<SelectListItem>();

                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    list = (from pt in context.Common_PaymentType
                            select new SelectListItem
                            {
                                Text = pt.PaymentType,
                                Value = pt.PaymentType_ID.ToString().Trim()
                            }).ToList();

                    //For by passing credit card payment use
                    list.Insert(2, new SelectListItem { Value = "3", Text = "Credit Card - STRIPE" });
                    list.Insert(3, new SelectListItem { Value = "10000", Text = "Credit Card - ADMIN" }); //Chosen a big number so there is no chance for payment type to be that id
                    list.Remove(list.Find(x => x.Text == "Credit Card"));
                }

                return list;
            }
        }

        public List<SelectListItem> ConferenceList
        {
            get
            {
                var list = new List<SelectListItem>();

                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    list = (from ec in context.Events_Conference
                            join ev in context.Events_Venue
                            on ec.Conference_ID equals ev.Conference_ID
                            orderby ev.ConferenceStartDate descending
                            select new SelectListItem
                            {
                                Text = ec.ConferenceName,
                                Value = ec.Conference_ID.ToString().Trim()
                            }).ToList();
                }

                return list;
            }
        }

    }
}