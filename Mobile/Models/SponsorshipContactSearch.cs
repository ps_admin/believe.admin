﻿namespace Planetshakers.Events.Models
{
    public class SponsorshipContactSearch
    {
        public int Contact_ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public int? hasInternal { get; set; }
        public int? hasExternal { get; set; }
    }
}