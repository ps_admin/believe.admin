﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Data.Entity;
using Planetshakers.Core.Models;
using Stripe;

namespace Planetshakers.Events.Models
{
    public class SponsorshipManagementModel
    {
        public SponsorshipItem SelectedSponsorship { get; set; }

        public int Conference_ID { get; set; }

        public List<SponsorshipCampusTotal> CampusBreakdown { get; set; }

        public List<SponsorshipMinistryTotal> MinistryBreakdown { get; set; }

        public List<SponsorshipPaymentTypeTotal> PaymentTypeBreakdown { get; set; }

        public string Currency
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var Currency = (from ec in context.Events_Conference
                                    join ba in context.Common_BankAccount
                                    on ec.BankAccount_ID equals ba.BankAccount_ID
                                    where ec.Conference_ID == Conference_ID
                                    select ba.Currency).FirstOrDefault();

                    return Currency;

                }
            }
        }

        public decimal TotalDonor
        {
            get
            {
                var totalDonorAmt = SponsorshipList.FindAll(x => x.Donor == true).Select(x => x.PaymentAmount).ToList().Sum();

                return totalDonorAmt;
            }
        }

        public decimal TotalRecipient
        {
            get
            {
                var totalRecipientAmt = SponsorshipList.FindAll(x => x.Recipient == true).Select(x => x.PaymentAmount).ToList().Sum();

                return totalRecipientAmt;
            }
        }
        public decimal TotalRemaining
        {
            get
            {
                return TotalDonor - TotalRecipient;
            }

        }

        public List<SponsorshipItem> SponsorshipList
        {
            get
            {
                var sponsorship = new List<SponsorshipItem>();
                sponsorship.AddRange(DonorList);
                sponsorship.AddRange(RecipientList);
                sponsorship.OrderBy(x => x.PaymentDate);

                return sponsorship;
            }
        }

        public List<SponsorshipItem> DonorList
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var donorlist = (from vp in context.Events_VariablePayment
                                     join cc in context.Common_Contact on vp.IncomingContact_ID equals cc.Contact_ID
                                     join ccc in context.Common_Contact on vp.PaymentBy_ID equals ccc.Contact_ID
                                     join ec in context.Events_Conference on vp.Conference_ID equals ec.Conference_ID
                                     join pt in context.Common_PaymentType on vp.PaymentType_ID equals pt.PaymentType_ID
                                     join ba in context.Common_BankAccount on vp.BankAccount_ID equals ba.BankAccount_ID
                                     join cci in context.Common_ContactInternal on cc.Contact_ID equals cci.Contact_ID into subinternal
                                     from i in subinternal.DefaultIfEmpty()
                                     join c in context.Church_Campus on i.Campus_ID equals c.Campus_ID into subcampus
                                     from c in subcampus.DefaultIfEmpty()
                                     join m in context.Church_Ministry on i.Ministry_ID equals m.Ministry_ID into subministry
                                     from m in subministry.DefaultIfEmpty()
                                     where vp.Conference_ID == Conference_ID && vp.IncomingContact_ID != null && vp.PaymentCompleted == true
                                     select new SponsorshipItem
                                     {
                                         VariablePaymentID = vp.VariablePayment_ID,
                                         FullName = cc.FirstName + " " + cc.LastName,
                                         Mobile = cc.Mobile,
                                         Email = cc.Email,
                                         Anonymous = vp.Anonymous,
                                         Donor = true,
                                         Recipient = false,
                                         Currency = ba.Currency,
                                         Conference_ID = ec.Conference_ID,
                                         PaymentType_ID = pt.PaymentType_ID,
                                         PaymentType = pt.PaymentType,
                                         PaymentAmount = (decimal)vp.PaymentAmount,
                                         CCTransactionRef = vp.CCTransactionReference,
                                         Refund = vp.Refund,
                                         Comment = vp.Comment,
                                         PaymentDate = vp.PaymentDate,
                                         PaymentByID = vp.PaymentBy_ID,
                                         PaymentByName = ccc.FirstName + " " + ccc.LastName,
                                         PaymentByEmail = ccc.Email,
                                         PaymentByMobile = ccc.Mobile,
                                         Campus = c.ShortName,
                                         Ministry = m.Name
                                     }).ToList();

                    return donorlist;
                }
            }
        }

        public List<SponsorshipItem> RecipientList
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    //mark any duplicated allocation as incomplete 
                    var sponsorshipAllocations = (from s in context.Events_VariablePayment
                                                  where s.Conference_ID == Conference_ID && s.IncomingContact_ID == null 
                                                  && s.OutgoingContact_ID != null && (s.PaymentCompleted ?? false) && s.Refund == false && s.CCTransactionReference != null
                                                  select s).GroupBy(x => x.CCTransactionReference);
                    foreach (var transaction in sponsorshipAllocations)
                    {
                        if (transaction.Count() > 1)
                        {
                            //duplicated allocation
                            foreach (var item in transaction.Skip(1))
                            {
                                item.PaymentCompleted = false;
                                item.PaymentCompletedDate = null;
                                item.Comment = "Duplicated Allocation | updated: " + DateTime.Now.ToString("dd-MMM-yy HH:mm");
                            }
                        }
                    }
                    context.SaveChanges();


                    var recipientlist = (from vp in context.Events_VariablePayment
                                         join cc in context.Common_Contact on vp.OutgoingContact_ID equals cc.Contact_ID
                                         join ccc in context.Common_Contact on vp.PaymentBy_ID equals ccc.Contact_ID
                                         join ec in context.Events_Conference on vp.Conference_ID equals ec.Conference_ID
                                         join pt in context.Common_PaymentType on vp.PaymentType_ID equals pt.PaymentType_ID
                                         join ba in context.Common_BankAccount on vp.BankAccount_ID equals ba.BankAccount_ID
                                         join cci in context.Common_ContactInternal on cc.Contact_ID equals cci.Contact_ID into subinternal
                                         from i in subinternal.DefaultIfEmpty()
                                         join c in context.Church_Campus on i.Campus_ID equals c.Campus_ID into subcampus
                                         from c in subcampus.DefaultIfEmpty()
                                         join m in context.Church_Ministry on i.Ministry_ID equals m.Ministry_ID into subministry
                                         from m in subministry.DefaultIfEmpty()
                                         where vp.Conference_ID == Conference_ID && vp.OutgoingContact_ID != null && vp.PaymentCompleted == true
                                         select new SponsorshipItem
                                         {
                                             VariablePaymentID = vp.VariablePayment_ID,
                                             FullName = cc.FirstName + " " + cc.LastName,
                                             Mobile = cc.Mobile,
                                             Email = cc.Email,
                                             Anonymous = vp.Anonymous,
                                             Donor = false,
                                             Recipient = true,
                                             Currency = ba.Currency,
                                             Conference_ID = ec.Conference_ID,
                                             PaymentType_ID = pt.PaymentType_ID,
                                             PaymentType = pt.PaymentType,
                                             PaymentAmount = (decimal)vp.PaymentAmount,
                                             CCTransactionRef = vp.CCTransactionReference,
                                             Refund = vp.Refund,
                                             Comment = vp.Comment,
                                             PaymentDate = vp.PaymentDate,
                                             PaymentByID = vp.PaymentBy_ID,
                                             PaymentByName = ccc.FirstName + " " + ccc.LastName,
                                             PaymentByEmail = ccc.Email,
                                             PaymentByMobile = ccc.Mobile,
                                             Campus = c.ShortName,
                                             Ministry = m.Name
                                         }).ToList();
                    return recipientlist;
                }
            }
        }

       
        public bool RemoveRecipientVariablePayment(int VariablePaymentID)
        {
            using(PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var variablePayment = (from v in context.Events_VariablePayment
                                       where v.VariablePayment_ID == VariablePaymentID && v.CCTransactionReference != null
                                       select v).FirstOrDefault();
                if (variablePayment != null)
                {

                    context.Events_VariablePayment.Remove(variablePayment);
                    context.SaveChanges();
                    return true;
                } else
                {
                    return false;
                }
            }
        }
        public void RefundVariablePayment(int VariablePaymentID, int User_ID, bool Donor)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                //get the transaction Ref
                var variablePayment = (from vp in context.Events_VariablePayment
                                       where vp.VariablePayment_ID == VariablePaymentID
                                       select vp).FirstOrDefault();
                var transactionRef = (Donor) ? variablePayment.CCTransactionReference : variablePayment.CCTransactionReference.Substring(5);
                
                if (!String.IsNullOrEmpty(transactionRef))
                {
                    try
                    {
                        var bank_account = new Common_BankAccount();
                        //call stripe to refund value with payment intent id
                        // stripe account based on payment
                        var ba_id = variablePayment.BankAccount_ID;

                        if (ba_id > 0)
                        {
                            bank_account = (from ba in context.Common_BankAccount
                                            where ba.BankAccount_ID == ba_id
                                            select ba).FirstOrDefault();
                        }

                        StripeConfiguration.ApiKey = bank_account.SecretKey;

                        // refund method differs between payment intent and charge type
                        if (transactionRef.Contains("pi_"))
                        {
                            var refundService = new RefundService();
                            var refundOptions = new RefundCreateOptions
                            {
                                Reason = "requested_by_customer",
                                PaymentIntent = transactionRef
                            };

                            Refund refund = refundService.Create(refundOptions);

                        }
                        else if (transactionRef.Contains("ch_"))
                        {
                            var refundService = new RefundService();
                            var refundOptions = new RefundCreateOptions
                            {
                                Reason = "requested_by_customer",
                                Charge = transactionRef
                            };

                            Refund refund = refundService.Create(refundOptions);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                        // TODO: Throw meaningful error to UI
                    }

                    //update database to reflect the refund
                    //1. remove allocation of the sponsorship
                    //2. remove/refund variable payment transaction
                    var refundAmount = variablePayment.PaymentAmount * -1;
                    //find allocation of this variable payment 
                    var allocation = (from vp in context.Events_VariablePayment
                                      where vp.PaymentBy_ID == variablePayment.PaymentBy_ID
                                      && vp.PaymentAmount == variablePayment.PaymentAmount
                                      && vp.CCTransactionReference == "ref: " + transactionRef
                                      && vp.IncomingContact_ID == null
                                      select vp).FirstOrDefault();
                    var removeVP = new Events_VariablePayment()
                    {
                        IncomingContact_ID = allocation.IncomingContact_ID,
                        Conference_ID = allocation.Conference_ID,
                        PaymentType_ID = allocation.PaymentType_ID,
                        PaymentAmount = refundAmount,
                        CCTransactionReference = allocation.CCTransactionReference,
                        Refund = true,
                        OutgoingContact_ID = allocation.OutgoingContact_ID,
                        Comment = "Remove of Variable Payment: " + allocation.VariablePayment_ID,
                        PaymentDate = DateTime.Now,
                        PaymentBy_ID = User_ID,
                        BankAccount_ID = allocation.BankAccount_ID,
                        PaymentCompleted = true,
                        PaymentCompletedDate = DateTime.Now,
                        Anonymous = true
                    };
                    context.Events_VariablePayment.Add(removeVP);

                    //refund sponsorship
                    var VP = (from v in context.Events_VariablePayment
                              where v.CCTransactionReference == transactionRef 
                              && v.OutgoingContact_ID == null
                              && v.PaymentAmount == variablePayment.PaymentAmount
                              select v).FirstOrDefault();

                    var refundVP = new Events_VariablePayment()
                    {
                        IncomingContact_ID = VP.IncomingContact_ID,
                        Conference_ID = VP.Conference_ID,
                        PaymentType_ID = VP.PaymentType_ID,
                        PaymentAmount = refundAmount,
                        CCTransactionReference = "ref: " + transactionRef,
                        Refund = true,
                        OutgoingContact_ID = VP.OutgoingContact_ID,
                        Comment = "Refund of Variable Payment: " + VP.VariablePayment_ID,
                        PaymentDate = DateTime.Now,
                        PaymentBy_ID = User_ID,
                        BankAccount_ID = VP.BankAccount_ID,
                        PaymentCompleted = true,
                        PaymentCompletedDate = DateTime.Now,
                        Anonymous = true
                    };
                    context.Events_VariablePayment.Add(refundVP);
                    context.SaveChanges();
                }
            }
        }

        public bool UpdateVariablePayment(int VariablePayment_ID, int Conference_ID, decimal PaymentAmount, string Comment)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var success = false;
                var variablePayment = (from vp in context.Events_VariablePayment
                                       where vp.VariablePayment_ID == VariablePayment_ID
                                       select vp).FirstOrDefault();


                if (variablePayment != null)
                {
                    variablePayment.Conference_ID = Conference_ID;
                    variablePayment.PaymentAmount = PaymentAmount;
                    variablePayment.Comment = Comment;
                    variablePayment.BankAccount_ID = (from ec in context.Events_Conference
                                                      join vp in context.Events_VariablePayment on ec.Conference_ID equals vp.Conference_ID
                                                      where ec.Conference_ID == Conference_ID
                                                      select ec.BankAccount_ID).FirstOrDefault();

                    context.SaveChanges();
                    success = true;
                }
                return success;

            }
        }

        public List<SelectListItem> InOutSelect
        {
            get
            {
                List<SelectListItem> list = new List<SelectListItem>
                {
                    new SelectListItem
                    {
                        Text = "Donor",
                        Value = "Donor"
                    },
                    new SelectListItem
                    {
                        Text = "Recipient",
                        Value = "Recipient"
                    }
                };
                return list;
            }
        }

        public List<SelectListItem> ConferenceList
        {
            get
            {
                var list = new List<SelectListItem>();

                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    list = (from ec in context.Events_Conference
                            join ev in context.Events_Venue
                            on ec.Conference_ID equals ev.Conference_ID
                            orderby ev.ConferenceStartDate descending
                            select new SelectListItem
                            {
                                Text = ec.ConferenceName,
                                Value = ec.Conference_ID.ToString().Trim()
                            }).ToList();
                }

                return list;
            }
        }

        public List<SelectListItem> PaymentTypeList
        {
            get
            {
                var list = new List<SelectListItem>();

                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    list = (from pt in context.Common_PaymentType
                            select new SelectListItem
                            {
                                Text = pt.PaymentType,
                                Value = pt.PaymentType_ID.ToString().Trim()
                            }).ToList();

                    list.RemoveAll(x => x.Value == "5" || x.Value == "7" || x.Value == "8");
                }

                return list;
            }
        }

        public List<SelectListItem> BreakdownSelect
        {
            get
            {
                List<SelectListItem> list = new List<SelectListItem>();
                list.Add(new SelectListItem
                {
                    Text = "Campus Breakdown",
                    Value = "Campus Breakdown"
                });
                list.Add(new SelectListItem
                {
                    Text = "Ministry Breakdown",
                    Value = "Ministry Breakdown"
                });
                list.Add(new SelectListItem
                {
                    Text = "Payment Type Breakdown",
                    Value = "Payment Type Breakdown"
                });
                return list;
            }
        }
        public List<SelectListItem> CampusList
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = (from c in context.Church_Campus
                                select new SelectListItem
                                {
                                    Text = c.ShortName,
                                    Value = c.ShortName
                                }).ToList();

                    return list;
                }
            }
        }
        public List<SelectListItem> MinistryList
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = (from m in context.Church_Ministry
                                select new SelectListItem
                                {
                                    Text = m.Name,
                                    Value = m.Name
                                }).ToList();

                    return list;
                }
            }
        }
        public bool Refunded { get; set; }
        public bool RemovedAllocation { get; set; }
        public bool intentNotFound { get; set; }
        public bool removeBtn { get; set; }
    }
}