﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Planetshakers.Events.Models
{
    public class TeamRoleModel
    {
        public List<TeamRole> RoleList
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = (from r in context.Events_TeamRole
                                select new TeamRole
                                {
                                    TeamRoleName = r.TeamRole,
                                    TeamRole_ID = r.TeamRole_ID,
                                    AddedDate = r.AddedDate,
                                    Active = r.Active
                                }).ToList();
                    return list;
                }
            }
        }
    }
    public class TeamRole
    {
        public int TeamRole_ID { get; set; } 
        public string TeamRoleName { get; set; }
        public DateTime AddedDate { get; set; }
        public bool Active { get; set; }

        public void getTeamRole(int TeamRole_ID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var role = (from r in context.Events_TeamRole
                            where r.TeamRole_ID == TeamRole_ID
                            select r).FirstOrDefault();
                this.TeamRole_ID = role.TeamRole_ID;
                this.TeamRoleName = role.TeamRole;
                this.AddedDate = role.AddedDate;
                this.Active = role.Active;
            }            
        }
        public void addTeamRole()
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                Events_TeamRole newTeamRole = new Events_TeamRole();
                newTeamRole.TeamRole = this.TeamRoleName;
                newTeamRole.AddedDate = DateTime.Now;
                newTeamRole.Active = this.Active;
                context.Events_TeamRole.Add(newTeamRole);
                context.SaveChanges();
            }
        }
        public void editTeamRole()
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var role = (from r in context.Events_TeamRole
                            where r.TeamRole_ID == this.TeamRole_ID
                            select r).FirstOrDefault();
                if (role != null)
                {
                    role.TeamRole = this.TeamRoleName;
                    role.Active = this.Active;
                    context.SaveChanges();
                }
            }
        }
        public void removeTeamRole(int TeamRole_ID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var role = (from r in context.Events_TeamRole
                            where r.TeamRole_ID == TeamRole_ID
                            select r).FirstOrDefault();
                if (role != null)
                {
                    context.Events_TeamRole.Remove(role);
                    context.SaveChanges();
                }
            }
        }

    }
}