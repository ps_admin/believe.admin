﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Planetshakers.Events.Models
{
    public class TeamSearchViewModel
    {
        public string SelectedConference { get; set; }
        public int? SelectedConferenceID { get; set; }
        public List<TeamModel> SearchResult { get; set; }
        public List<SelectListItem> event_list
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {

                    var list = (from ec in context.Events_Conference
                                join ev in context.Events_Venue
                                    on ec.Conference_ID equals ev.Conference_ID
                                orderby ec.SortOrder
                                select new SelectListItem
                                {
                                    Value = ec.Conference_ID.ToString().Trim(),
                                    Text = ec.ConferenceName
                                }).ToList();

                    return list;
                }
            }
        }
        public void populateResult()
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var list = (from t in context.Events_Team
                            where t.Event_ID == SelectedConferenceID
                            select new TeamModel
                            {
                                Team_ID = t.Team_ID,
                                TeamName = t.TeamName,
                                Inactive = t.Inactive,
                                SortOrder = t.SortOrder
                            }).ToList();
                this.SearchResult = list;
            }
        }
    }
    public class TeamModel
    {
        public int Team_ID { get; set; }
        public int Event_ID { get; set; }
        public string TeamName { get; set; }
        public bool Inactive { get; set; }
        public int SortOrder { get; set; }
        public List<TeamMember> Members { get; set; }
        public List<SelectListItem> PotentialMemberList { get; set; }
        public List<SelectListItem> TeamRoleList
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var list = (from t in context.Events_TeamRole
                                where t.Active == true
                                select new SelectListItem
                                {
                                    Value = t.TeamRole_ID.ToString().Trim(),
                                    Text = t.TeamRole
                                }).ToList();
                    return list;
                }

            }
        }
        public bool isEdit { get; set; }
        public void getTeam()
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var team = (from t in context.Events_Team
                            where t.Team_ID == this.Team_ID
                            select t).FirstOrDefault();
                this.Team_ID = team.Team_ID;
                this.TeamName = team.TeamName;
                this.Inactive = team.Inactive;
                this.SortOrder = team.SortOrder;
                this.isEdit = true;
                this.Event_ID = team.Event_ID;
            }
        }
        public void getMember()
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var members = (from tm in context.Events_TeamContact
                               join cc in context.Common_Contact on tm.Contact_ID equals cc.Contact_ID
                               where tm.Team_ID == this.Team_ID && tm.Inactive == false
                               select new TeamMember
                               {
                                   TeamContact_ID = tm.TeamContact_ID,
                                   Team_ID = tm.Team_ID,
                                   Contact_ID = tm.Contact_ID,
                                   FirstName = cc.FirstName,
                                   LastName = cc.LastName,
                                   Inactive = tm.Inactive,
                                   AddedDate = tm.AddedDate,
                                   //TeamRole_ID = tr.TeamRole_ID
                               }).ToList();
                foreach(var member in members)
                {

                    var teamRoles = (from cr in context.Events_TeamContactRole
                                       join r in context.Events_TeamRole on cr.Role_ID equals r.TeamRole_ID
                                       where cr.Active == true && r.Active == true && cr.TeamContact_ID == member.TeamContact_ID
                                       select r).ToList();
                    member.TeamRole = teamRoles.Select(x => x.TeamRole).ToList();
                    member.TeamRole_IDs = teamRoles.Select(x => x.TeamRole_ID).ToList();

                    if (member.TeamRole.Count != 0)
                    {
                        foreach (var role in member.TeamRole)
                        {
                            member.RoleString += role + ", ";
                        }
                        member.RoleString = member.RoleString.Substring(0, member.RoleString.Length - 2);
                    } else
                    {
                        member.RoleString = "";
                    }
                }
                this.Members = members;
            }
        }
        public void populatePotentialMembers()
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var Event_ID = (from e in context.Events_Team
                                where e.Team_ID == this.Team_ID
                                select e.Event_ID).FirstOrDefault();
                this.Event_ID = Event_ID;
                var regoTypes = (from rt in context.Events_RegistrationType
                                 where rt.Conference_ID == this.Event_ID
                                     && !rt.ApplicationRegistrationType
                                     && !rt.AddOnRegistrationType
                                 select rt.RegistrationType_ID).ToList();
                var memberList = (from tc in context.Events_TeamContact
                                  where tc.Inactive == false
                                  select tc.Contact_ID).ToList();
                var list = (from r in context.Events_Registration
                            join c in context.Common_Contact on r.Contact_ID equals c.Contact_ID
                            where regoTypes.Contains(r.RegistrationType_ID) && !memberList.Contains(r.Contact_ID)
                            select new SelectListItem
                            {
                                Value = c.Contact_ID.ToString().Trim(),
                                Text = c.FirstName + " " + c.LastName
                            }).ToList();
                this.PotentialMemberList = list;
            }
        }
        public bool addMember(int Contact_ID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                //check if exist
                var teamContact = (from c in context.Events_TeamContact
                                   where c.Contact_ID == Contact_ID && c.Inactive == true && c.Team_ID == this.Team_ID
                                   select c).FirstOrDefault();
                if (teamContact != null)
                {
                    teamContact.Inactive = false;
                }
                else
                {
                    Events_TeamContact newTeamContact = new Events_TeamContact();
                    newTeamContact.Contact_ID = Contact_ID;
                    newTeamContact.AddedDate = DateTime.Now;
                    newTeamContact.Team_ID = this.Team_ID;
                    newTeamContact.Inactive = false;
                    context.Events_TeamContact.Add(newTeamContact);
                }
                context.SaveChanges();
                return true;
            }
        }
        public int removeMember(int TeamContact_ID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var teamContact = (from c in context.Events_TeamContact
                                   where c.TeamContact_ID == TeamContact_ID
                                   select c).FirstOrDefault();
                teamContact.Inactive = true;
                context.SaveChanges();

                return teamContact.Team_ID;
            }
        }
        public bool addTeam()
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                Events_Team newTeam = new Events_Team
                {
                    Event_ID = this.Event_ID,
                    TeamName = this.TeamName,
                    Inactive = this.Inactive,
                    SortOrder = 1
                };
                context.Events_Team.Add(newTeam);
                context.SaveChanges();

                return true;
            }
        }
        public bool editTeam(int Team_ID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var team = (from t in context.Events_Team
                            where t.Team_ID == Team_ID
                            select t).FirstOrDefault();

                team.TeamName = this.TeamName;
                team.Inactive = this.Inactive;
                context.SaveChanges();

                return true;
            }
        }
    }

    public class TeamMember
    {
        public int Team_ID { get; set; }
        public int TeamContact_ID { get; set; }
        public int Contact_ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool Inactive { get; set; }
        public DateTime AddedDate { get; set; }
        public List<int> TeamRole_IDs { get; set; }
        public List<string> TeamRole { get; set; }
        public string RoleString { get; set; }
        public string getMemberRole(int TeamContact_ID)
        {
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var existingRole = (from cr in context.Events_TeamContactRole
                                    where cr.TeamContact_ID == TeamContact_ID && cr.Active == true
                                    select cr.Role_ID).ToList();
                var list = (from r in context.Events_TeamRole
                            select new TeamRole()
                            {
                                TeamRoleName = r.TeamRole,
                                TeamRole_ID = r.TeamRole_ID,
                                Active = existingRole.Contains(r.TeamRole_ID),
                            }).ToList();
                var returnString = "<select name='selValue' id='TeamRoleSelected" + "' class='form-control' multiple='multiple' data-live-search='true' data-style='multi-select-btn'>";
                foreach(var item in list)
                {
                    if (item.Active)
                    {

                        returnString += "<option value = '" + item.TeamRole_ID + "' selected>" + item.TeamRoleName + "</option > ";
                    }
                    else
                    {
                        returnString += "<option value = '" + item.TeamRole_ID + "' >" + item.TeamRoleName + "</option > ";
                    }
                }
                returnString += "</select><br />";
                return returnString;
            }
        }
        public int addEditMemberTeamRole(List<int> TeamRole_IDs)
        {
            //add roles that are not already added -> make active if inacitve
            //remove roles that are added but not in list -> make inactive
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                var member = (from c in context.Events_TeamContact
                              where c.TeamContact_ID == this.TeamContact_ID
                              select c).FirstOrDefault();

                var roleList = (from cr in context.Events_TeamContactRole
                                where cr.TeamContact_ID == this.TeamContact_ID && cr.Active == true
                                select cr.Role_ID).ToList();
                //adding new roles 
                if(TeamRole_IDs != null)
                {
                    foreach (var role in TeamRole_IDs)
                    {
                        if (roleList.Contains(role))
                        {
                            var existingRole = (from cr in context.Events_TeamContactRole
                                                where cr.TeamContact_ID == this.TeamContact_ID && cr.Role_ID == role && cr.Active == false
                                                select cr).FirstOrDefault();
                            if (existingRole != null)
                            {
                                existingRole.Active = true;
                            }
                        }
                        else
                        {
                            var newRole = new Events_TeamContactRole()
                            {
                                TeamContact_ID = this.TeamContact_ID,
                                Role_ID = role,
                                AddedDate = DateTime.Now,
                                Active = true
                            };
                            context.Events_TeamContactRole.Add(newRole);
                        }
                    }
                }
                //removing roles 
                if (roleList != null)
                {
                    foreach (var role in roleList)
                    {
                        var delete = (TeamRole_IDs != null) ? ((!TeamRole_IDs.Contains(role)) ? true : false ) : true;
                        if(delete)
                        {
                            var existingRole = (from cr in context.Events_TeamContactRole
                                                where cr.TeamContact_ID == this.TeamContact_ID && cr.Role_ID == role && cr.Active == true
                                                select cr).FirstOrDefault();
                            existingRole.Active = false;
                        }
                    }
                }
                context.SaveChanges();
                return member.Team_ID;
            }
        }

    }
}