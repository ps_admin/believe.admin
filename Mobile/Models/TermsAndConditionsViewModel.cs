﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Planetshakers.Events.Models
{
    public class TermsAndConditionsViewModel
    {

        public int SelectedConferenceID { get; set; }
        public List<SelectListItem> event_list
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {

                    var list = (from ec in context.Events_Conference
                                join ev in context.Events_Venue
                                    on ec.Conference_ID equals ev.Conference_ID
                                orderby ev.ConferenceStartDate descending
                                select new SelectListItem
                                {
                                    Value = ec.Conference_ID.ToString().Trim(),
                                    Text = ec.ConferenceName
                                }).ToList();

                    return list;
                }
            }
        }

    }
}