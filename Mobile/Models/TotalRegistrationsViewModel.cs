﻿using Planetshakers.Events.Models;
using System;
using System.Collections.Generic;
using System.Linq;
public class TotalRegistrationViewModel
{
    public string confName { get; set; }
    public int confSelectedID { get; set; }
    public string searchName { get; set; }
    public List<string> yearList
    {
        get
        {
            List<string> yearList = new List<string> { "2020", "2019", "2018", "2017" };
            return yearList;
        }
    }

    public Dictionary<string, int> yearConf
    {
        get
        {
            Dictionary<string, int> yearConfDict = new Dictionary<string, int>();
            using (PlanetshakersEntities context = new PlanetshakersEntities())
            {
                foreach (string year in yearList)
                {
                    int yearInt = Int32.Parse(year);
                    int conferenceID = (from ec in context.Events_Conference
                                        join ev in context.Events_Venue
                                        on ec.Conference_ID equals ev.Conference_ID
                                        where ev.ConferenceStartDate.Year == yearInt && ec.ConferenceName.Contains(searchName)
                                        select ev.Conference_ID).FirstOrDefault();
                    yearConfDict.Add(year, conferenceID);

                }
            }
            return yearConfDict;
        }
    }
}