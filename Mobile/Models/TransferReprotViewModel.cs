﻿using Planetshakers.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Planetshakers.Events.Models
{
    public class TransferReprotViewModel
    {
        public Boolean displayReport = false;

        public int Conference_ID { get; set; }

        public string Currency { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int selectedDateOpt { get; set; }

        public List<EventModel> _events
        {
            get
            {
                using (PlanetshakersEntities context = new PlanetshakersEntities())
                {
                    var _events = (from ec in context.Events_Conference
                                   join ev in context.Events_Venue
                                   on ec.Conference_ID equals ev.Conference_ID
                                   orderby ev.ConferenceStartDate descending
                                   select new EventModel
                                   {
                                       ConferenceID = ec.Conference_ID,
                                       ConferenceName = ec.ConferenceName
                                   }).ToList();

                    return _events;
                }
            }
        }

        public IEnumerable<SelectListItem> EventList { get { return new SelectList(_events, "ConferenceID", "ConferenceName"); } }

        public List<TimeSelection> _availableTimeSelection
        {
            get
            {
                List<TimeSelection> newlist = new List<TimeSelection>();
                newlist.Add(new TimeSelection { dateId = 0, startdate = DateTime.Today, enddate = DateTime.Now, selectionname = "-- Select Date --" });
                newlist.Add(new TimeSelection { dateId = 1, startdate = DateTime.Today, enddate = DateTime.Now, selectionname = "Today" });
                newlist.Add(new TimeSelection { dateId = 2, startdate = DateTime.Now.AddDays(-1), enddate = DateTime.Now, selectionname = "Last 24 hours" });
                newlist.Add(new TimeSelection { dateId = 3, startdate = DateTime.Now.AddDays(-7), enddate = DateTime.Now, selectionname = "Last 7 days" });
                newlist.Add(new TimeSelection { dateId = 4, startdate = DateTime.Now.AddDays(-14), enddate = DateTime.Now, selectionname = "Last 14 days" });
                newlist.Add(new TimeSelection { dateId = 5, startdate = DateTime.Now.AddDays(-31), enddate = DateTime.Now, selectionname = "Last 31 days" });
                newlist.Add(new TimeSelection { dateId = 6, startdate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1), enddate = DateTime.Today, selectionname = "Current month" });
                newlist.Add(new TimeSelection { dateId = 7, startdate = new DateTime(DateTime.Today.Year, 1, 1), enddate = DateTime.Today, selectionname = "Current year" });
                newlist.Add(new TimeSelection { dateId = 9, startdate = new DateTime(1753, 1, 1), enddate = DateTime.Now, selectionname = "All Time" });
                newlist.Add(new TimeSelection { dateId = 8, startdate = DateTime.Today, enddate = DateTime.Now, selectionname = "Custom" });
                return newlist;
            }
        }

        public IEnumerable<SelectListItem> Dateselection { get { return new SelectList(_availableTimeSelection, "dateId", "selectionname"); } }

        public List<SinglePayment> singlePaymentList { get; set; }

        public List<GroupPayment> groupPaymentList { get; set; }

        public decimal totalSingleTransferAmount { get; set; }

        public decimal totalGroupTransferAmount { get; set; }
    }
}