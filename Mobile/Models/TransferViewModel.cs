﻿using Planetshakers.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Planetshakers.Events.Models
{
    public class TransferViewModel
    {
        //Transfer type possible value: single, group, sponsorship
        public string TransferType { get; set; }

        public int Conference_ID { get; set; }

        public string ConferenceName { get; set; }

        // Selected Contact
        public int ContactId { get; set; }

        public string Comment { get; set; }

        //For transferring single rego use
        public Registration selectedRegistration { get; set; }

        //For transferring group purchase use
        public List<TransferGroupRegistration> TransferGroupRegoOption { get; set; }

        //For transferring sponsorship payment use
        public SponsorshipItem selectedSponsorship { get; set; }

        public bool isSystemTransfer { get; set; }
    }
}