﻿using System.Linq;
using Planetshakers.Core.Models;
using Planetshakers.Events.Security;

namespace Planetshakers.Events.Models
{
    public class UrbanLifeActionResultModel<T>
    {
        public UrbanLifeActionResultModel(int urbanLifeId)
        {
            UrbanLife = CurrentContext.Instance.Groups.Single(x => x.Id == urbanLifeId);
        }

        public UrbanLife UrbanLife { get; private set; }
        public T Model { get; set; }
        
        public string ErrorMessage { get; set; }
        public bool HasError {
            get { return !string.IsNullOrEmpty(ErrorMessage); }
        }
    }
}