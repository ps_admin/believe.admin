﻿namespace Planetshakers.Events.Models
{
    /// <summary>
    /// Represent logged in user into the system
    /// </summary>
    public class User
    {
        public int ContactID { get; set; }

        public string Barcode { get; set; }

        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string FullName { get { return FirstName + " " + LastName; } }

        public bool PasswordResetRequired { get; set; }

        public int PastorOfRegionId { get; set; }

        public bool IsRegionalPastor
        {
            get { return PastorOfRegionId > 0; }
        }
    }
}