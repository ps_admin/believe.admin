﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Planetshakers.Core.Models;

namespace Planetshakers.Events.Models
{
    public class Voucher
    {
        public int Voucher_Id { get; set; }

        public int VoucherType_ID { get; set; }

        public string VoucherType { get; set; }

        public string VoucherCode { get; set; }

        public decimal? VoucherValue { get; set; }

        public decimal? PredecessorValue { get; set; }

        public int Contact_ID { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? ExpiryDate { get; set; }

        public DateTime? UsedDate { get; set; }

        public DateTime? IssuedDate { get; set; }

        public DateTime? ModificationDate { get; set; }

        public string StartDateDisplay { get; set; }

        public string ExpiryDateDisplay { get; set; }

        public int ModifiedByID { get; set; }

        public string MFirstName { get; set; }

        public string MLastName { get; set; }

        public string Campus { get; set; }

        public string Ministry { get; set; }

        public bool Inactive { get; set; }

        public bool Deleted { get; set; }

        public int? CurrentUses { get; set; }

        public int? MaxUses { get; set; }

        public int? Predecessor_ID { get; set; }

        public int? Conference_ID { get; set; }

        public string ConferenceName { get; set; }

        public decimal? PaymentAmount { get; set; }


        public int id { get; set; }

        public string code { get; set; }

        public int contact_id { get; set; }

        public decimal value { get; set; }

        public decimal percentage_value { get; set; }

        public decimal applied_value { get; set; }

        public bool EmailVoucher { get; set; }

        public string Mobile { get; set; }

        public string Email { get; set; }
    }
}
