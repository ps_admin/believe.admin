//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Planetshakers.Events.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class vw_Events_GroupBulkRegistrationView
    {
        public int GroupLeader_ID { get; set; }
        public Nullable<int> State_ID { get; set; }
        public int Conference_ID { get; set; }
        public int Venue_ID { get; set; }
        public int RegistrationType_ID { get; set; }
        public decimal RegistrationCost { get; set; }
        public Nullable<int> Purchased { get; set; }
        public Nullable<int> Allocated { get; set; }
        public Nullable<int> UnAllocated { get; set; }
        public Nullable<decimal> TotalCost { get; set; }
    }
}
