﻿function UpdateAttendanceStatus() {
    var self = $(this);
    var bdtoId = self.attr('bdtoId');
    var pcrContactId = self.attr('id');
    var attendValue = self.val();
    var attending = attendValue === "yes";
    var action = self.data('update-url');
    var attendedCount = $('span#attendedCount');
    var totalAttendedCount = $('span#totalAttendedCount');
    //var attendedPct = $('span#attendedPct');

    $.ajax({
        type: "post",
        url: "/pcr/" + action,
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ 'bdtoId': bdtoId, 'pcrContactId': pcrContactId, 'Attend': attending }),
        success: function (data) {
            if (data) {
                var counter = parseInt(attendedCount.html()) + (attending ? 1 : -1);
                attendedCount.html(counter);

                //var pct = Math.floor((counter / parseInt(totalAttendedCount.html())) * 100);
                //attendedPct.html(pct + '%').fadeOut(300).fadeIn(300);
            }
        },
        error: function (request, status, error) {
            if (request.readyState === 0 || request.status === 0)
                return;  // it's not really an error
            else
            {
                alert('Unfortunately, we are unable to update the attendance.');
            };
        }
    });
}

$(document).ready(function () {
    $('input.attended').bind('click', UpdateAttendanceStatus);
});
