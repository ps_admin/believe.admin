﻿var confirmationMessages = {
    BeforeCommentSubmission: "You leave an empty comment. Are you sure to continue ?",
    CommentOnSubmit: "Comment has been submitted!",
    CommentOnFailedSubmit: "Oops, unable to add comment! Please try again!"
};

function SubmitComment() {
    var self = $(this);
    var form = $('form#formComment');
    var comment = $('form#formComment').find('#Comments');


//    if (comment.val().length == 0) {
//        alert('Comment cannot be empty.');
//    }
//    else {
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: "POST",
            success: function (data) {
                if (data && data.status == "Ok") {
                    //location.reload();
                    alert('Comment has been submitted!');
                    comment.val("");
                }
                else {
                    alert('Oops, unable to add comment! Please try again!');
                }
            },
            error: function (xmlHttpRequest, textStatus, errorThrown) {
                if (xmlHttpRequest.readyState == 0 || xmlHttpRequest.status == 0)
                    return;  // it's not really an error
                else
                    alert('Oops, unable to add comment! Please try again!');
            }
        });
//    }

    return;
}

$(document).ready(function () {
    $('a#submitComment').bind('click', SubmitComment);
});
