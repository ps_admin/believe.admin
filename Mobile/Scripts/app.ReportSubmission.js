﻿var confirmationMessages = {
        BeforeReportSubmission: "This submission cannot be undone! \nMake sure you've completed the report. \nAre you sure to continue?",
        ReportOnSubmit: "Thank you for your submission!",
        ReportOnFailedSubmit: "Unfortunately, there was a problem during the submission. Please try again!",
        ReportOnSave: "Report is saved!",
        ReportOnFailedSave: "Unfortunately, there was a problem saving the report. Please try again!"
    };

function SubmitReport() {
    var self = $(this);
    var form = $('form');

    form.find('#MarkAsCompleted').val('true');

    $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: "POST",
        success: function (data) {
            if (data && data.status == "Ok") {
                location.reload();
            }
            else {
                alert("Unfortunately, there was a problem during the submission. Please try again!");
            }
        },
        error: function (xmlHttpRequest, textStatus, errorThrown) {
            if (xmlHttpRequest.readyState == 0 || xmlHttpRequest.status == 0)
                return;  // it's not really an error
            else
                alert("Unfortunately, there was a problem during the submission. Please try again!");
        }
    });
}

function SaveReport() {
    var self = $(this);
    var form = $('#frmUrbanLifeReport');

    self.attr('disabled', 'disabled');

    form.find('#MarkAsCompleted').val("false");

    $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: "POST",
        success: function (data) {
            if (data && data.status == "Ok") {
                location.reload();
            }
            else {
                alert("Unfortunately, there was a problem saving the report. Please try again!");
            }

            self.removeAttr('disabled');
        },
        error: function (xmlHttpRequest, textStatus, errorThrown) {
            if (xmlHttpRequest.readyState == 0 || xmlHttpRequest.status == 0)
                return;  // it's not really an error
            else
                alert("Unfortunately, there was a problem during the submission. Please try again!");
        }
    });
}

$(document).ready(function () {
    $('a#submitReport').bind('click', SubmitReport);
    $('a#saveReport').bind('click', SaveReport);
});
