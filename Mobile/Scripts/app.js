﻿
window.ps = window.ps || {};

window.ps.helper = function () {

    return {

        /* Get parent with given classname of the underlying element 
        *  It won't stop until it found the parent or hit the root
        */
        getParent: function (el, classname) {
            var parent = el.parent();

            if (parent.length == 0)
                return null;

            if (parent.length == 1 && parent.hasClass(classname))
                return parent;

            return window.ps.helper.getParent(parent, classname);        
        }
        
    };

} ();

var app = app || {};

app = function() {

    function loadAjaxContent($sender, loadingText) {
        
        var dataUrl = $sender.data("src");
        //var refresh = $sender.data("refresh") == 1;
        if (dataUrl) {
            if (dataUrl.indexOf("?") > 0)
                dataUrl += "&_ts=" + new Date().getTime();
            else
                dataUrl += "?_ts=" + new Date().getTime();
        }

        var targetPanel = $sender.data("panel");

        if (!loadingText)
            loadingText = "Loading Content";
        
        if (targetPanel) {

            $.ajax({
                url: dataUrl,

                beforeSend: function () {
                    $.ui.showMask(loadingText + "...");
                },

                success: function (output) {                
                    $.ui.updateContentDiv(targetPanel, output);
                    $.ui.loadContent(targetPanel, false, false, "slide");
                    $.ui.hideMask();

                    // remove the #
                    $.ui.scrollToTop(targetPanel.substring(1));

                    var senderContentReadyCallback = eval($sender.data("content-ready"));
                    if (senderContentReadyCallback)
                        senderContentReadyCallback($(targetPanel), $sender);
                },

                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    if (xmlHttpRequest.readyState == 0 || xmlHttpRequest.status == 0)
                        return;  // it's not really an error
                    else {
                        $.ui.hideMask();
                        $.ui.popup({ suppressTitle: true, message: 'Unable to retrieve details. Try again!', cancelText: 'OK', cancelOnly: true });
                    }
                }

                //error: function () {
                //    $.ui.hideMask();
                //    $.ui.popup({ suppressTitle: true, message: 'Unable to retrieve details. Try again!', cancelText: 'OK', cancelOnly: true });
                //}
            });
        }
        else {            
            $.ui.loadContent(dataUrl, false, false, "slide", $sender.get(0));

            var isMainMenu = $sender.data("index");
            if (isMainMenu)
                $.ui.clearHistory();
        }
    }

    return {
        loadPanel: function(sender, loadingText) {
            loadAjaxContent($(sender), loadingText);
        },
        
        toggleList: function(keySelector, idx) {
            $(keySelector).hide();
            $(".index-" + idx).show();
            $.ui.scrollToTop(location.hash.substring(1));
        },

        updateAttendanceStatus: function (sender) {

            /* This is how jqmobi design the slider (aka checkbox)
            */

            var $self = $(sender);

            // REVISIT: counter box on the page, should be moved to its own class
            var infoBox = window.ps.helper.getParent($self, "list-with-details").siblings(".info");
            var counterSpan = infoBox.find(".counter");

            var slider = $self.siblings(".jq-ui-slider");

            // because it is custom, the label won't have selected class on,
            // until the update is performed.
            var attend = slider.attr("checked") != "true";

            // get contact id
            var contactId = slider.attr("id");

            // get the action
            var action = slider.val();

            $.post("/pcr/" + action,
                { PcrContactId: contactId, Attend: attend },
                function (result) {
                    if (attend)
                        slider.attr("checked", "true");
                    else
                        slider.removeAttr("checked");

                    var counter = parseInt(counterSpan.html()) + (attend ? 1 : -1);
                    counterSpan.html(counter);
                    console.log(result);
                });            
        },
        
        signOut: function() {
            $.get('/Account/LogOff', function () { location.href = "/"; });
        },
        

        report: appReport,
        comment: appComment
    };

}();