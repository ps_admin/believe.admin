﻿

$('.ShowEditModal').click(function (event) {
    event.preventDefault();
    //var href = event.srcElement.parentElement.attributes['data'].nodeValue;
    var href = $(this).attr('data');
    var variables = jQuery.parseJSON(href);
    
    $('#MemberName').text(variables.firstname + " " + variables.lastname);
    $('#MemberID').text(variables.id);
    $('#MemberMobile').text(variables.phone);
    $('#MemberStatus').text(variables.Status);

    $('a.reveal-link').trigger('click');

    var data = new Object();
    data.Contact_id = variables.id;
    data = JSON.stringify(data);
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'Management/GetUnderCarelist',
        data: data,
        dataType: 'json',
        success: function (response) {
            $('#allocated_person tbody').html(response);
            // reveal the modal
        }
    });
    $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: 'Management/GetCarerlist',
        data: data,
        dataType: 'json',
        success: function (response) {
            $('#allocated_carer tbody').html(response);
            // reveal the modal
        }
    });
});

function onClickRemoveContact() {
    $('#unallocate_contact').click(function(event)
    {
        event.preventDefault();
        var data = new Object();
        data.Contact_id =  $(this).attr('id').replace("_RemoveFromCarer", "");
        data.Carer_id = $("#MemberID").val();
        data = JSON.stringify(data)
        $.ajax({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: 'Management/unallocateContact',
            data: data,
            dataType: 'json',
            success: function (response) {
                if (response == true) {
                    $('#' + contact_id + '_AllocatedContactRow').hide("scale", function () {
                        $('#' + contact_id + '_AllocatedContactRow').remove();
                    }
                );
                }
            }
        });
    })
}

$('#editModal close-editmodal').click(function (event) {
    $('#editModal').trigger('reveal:close')
    //    $('a.close-reveal-modal').trigger('click');
    $('#editModal').foundation('reveal', 'close');
});

function clearEditModal() {
    $('#MemberName').text('');
    $('#MemberID').text('');
    $('#MemberMobile').text('');
    $('#MemberStatus').text('');
}