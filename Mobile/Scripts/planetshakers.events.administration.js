﻿$(document).ready(function () {
    if ($("#generatedSponsorshipTable").length > 0) {
        $("#generatedSponsorshipTable").DataTable({
            fixedHeader: {
                header: true,
                footer: true
            },
            language: {
                search: "",
                searchPlaceholder: "Search..."
            },
            paging: true,
            "pageLength": 25,
            "lengthMenu": [25, 50, 100],
            info: false,
            cache: false,
            scrollCollapse: true,
            responsive: true,
            processing: true,
            searching: true,

            order: [6, 'desc'],
            scrollX: "100%",
            scrollY: 400,
            bAutoWidth: true,
        });
    }

});

//========================Quick Add Registration=========================
function submitConferenceRego(e) {
    e.preventDefault();
    
    if ($("#regselectpaymenttype").val() == "") {
        
        $("#alertMessage").show();
    } else {
        document.getElementById("addRegistrationForm").submit();
    }
}

function calculatePrice(RegoType_ID) {
    var price = $('#RegoCost_' + RegoType_ID).text();
    var qty = $('#RegoQTYtxt_' + RegoType_ID).val();
    var total = price * qty;
    $('#RegoTotal_' + RegoType_ID).html(total.toFixed(2));

    var grandtotal = 0.00;
    $(".rowtotal").each(function () {
        var price = $(this).text();
        grandtotal += parseInt(price);
    })

    $('#RegoGrandTotal').html(grandtotal.toFixed(2));
    updateVoucherValue(grandtotal);
}

function updateVoucherValue(grandtotal) {
    var sum = grandtotal;
    var vouchervalue = $("#voucher_value").val();

    $("#voucher_value_applied").val((sum - vouchervalue <= 0) ? sum : vouchervalue);
    if (sum == 0) {
        $("#total_value_applied").val(0);
    } else {
        if (sum < vouchervalue) {
            $("#total_value_applied").val(0);
        } else {
            $("#total_value_applied").val(sum - vouchervalue);
        }
    }
}

//========================End of Quick Add Registration=========================

//=============================Sponsorship Section==============================
var sponsorshipTypingTimer;                //timer identifier
var doneTypingIntervalSponsorship = 300;

$('#SponsorshipSearchContactForm :input').on('keyup', function () {

    clearTimeout(sponsorshipTypingTimer);
    if ($('#SponsorshipSearchContactForm :input').val()) {
        sponsorshipTypingTimer = setTimeout(doneSponsorshipTyping, doneTypingIntervalSponsorship);
    }
    if ($('#SponsorshipSearchContactForm :input').val() === "") {
        sponsorshipTypingTimer = setTimeout(doneSponsorshipTyping, doneTypingIntervalSponsorship);
    }
});

/// Search sponsorship contact
function doneSponsorshipTyping() {
    var firstname = $('#contactFirstName').val();
    var lastname = $('#contactLastName').val();
    var mobile = $('#contactMobile').val();
    var email = $('#contactEmail').val();

    $.ajax({
        cache: false,
        type: "POST",
        url: "/Administration/SearchContacts",
        data: { "FirstName": firstname, "LastName": lastname, "Mobile": mobile, "Email": email },
        success: function (json) {

            var content = "<table class='table card-body'><thead id='sponsorshipContactHeader'><tr>";
            content += "<th>ID</th><th>Name</th><th>Mobile</th><th>Email</th><th>Internal</th><th>External</th><th>Action</th>";
            content += "</tr></thead>";
            content += "<tbody id='sponsorshipContactList'>";

            for (var i = 0; i < json.length; i++) {
                content += "<tr >";
                content += "<td id='id_" + json[i].Contact_ID + "'>" + json[i].Contact_ID + "</td>";
                content += "<td id='name_" + json[i].Contact_ID + "'>" + json[i].FirstName + " " + json[i].LastName + "</td>";
                content += "<td id='mobile_" + json[i].Contact_ID + "'>" + json[i].Mobile + "</td>";
                content += "<td id='email_" + json[i].Contact_ID + "'>" + json[i].Email + "</td>";
                if (json[i].hasExternal == null) {
                    content += "<td>" + "<button id='external_" + json[i].Contact_ID + "' class='btn btn-danger btn-sm' type='button' onclick='addExternalContact(" + json[i].Contact_ID + ")'>Add</button>" + "</td>";
                } else {
                    content += "<td id='external_" + json[i].Contact_ID + "'><i class='fa fa-check'></i></td>";
                }

                if (json[i].hasInternal != null) {
                    content += "<td id='internal_" + json[i].Contact_ID + "'><i class='fa fa-check'></i></td>";
                } else {
                    content += "<td id='internal_" + json[i].Contact_ID + "'></td>"
                }

                content += "<td>" + "<button class='btn btn-primary btn-sm' onclick='selectSponsorshipContact(" + json[i].Contact_ID + ")'>Select</button>" + "</td>";
                content += "</tr >";
            }

            content += "</tbody></table>";
            $('#sponsorshipContactTable').html(content);
        },
        error: function () {
        }
    });
}


function selectSponsorshipContact(id) {

    var selectedName = $("#name_" + id).html();
    var selectedMobile = $("#mobile_" + id).html();
    var selectedEmail = $("#email_" + id).html();

    $("#SelectedContactID").attr("value", id);
    $("#SelectedRecipientContactID").attr("value", id);
    $("#selectedFullName").html(selectedName);
    $("#selectedMobile").html(selectedMobile);
    $("#selectedEmail").html(selectedEmail);
    $("#selectedContactResult").show();
}

function CheckCurrency() {

    var ConferenceID = $("#SelectedEvent").val();

    $.ajax({
        cache: false,
        type: "POST",
        url: "/Administration/CheckCurrency",
        data: { "Conference_ID": ConferenceID },
        success: function (json) {

            $("#currency").html(json.Currency);
            $("#TotalAmount").show();

        },
        error: function () {
        }
    });
}

function SubmitSponsorshipForm() {

    if ($("#SelectedContactID").val() == "" || $("#SelectedContactID").val() == null) {
        $("#ErrorMessage").show();
        animateTo("ErrorMessage");

        return;
    }
    else if ($("#SelectedEvent").val() == 0 || $("#SelectedPaymentID").val() == 0 || $("#Comment").val() == null || $("#Comment").val() == '') {
        $("#Message").html("Please fill in all required fields.")
        $("#ErrorMessage").show();
        animateTo("ErrorMessage");
        return;
    }
    else if ($("#SponsoredAmount").val() < 1) {
        $("#Message").html("Sponsorship amount must be larger than 1.")
        $("#ErrorMessage").show();
        animateTo("ErrorMessage");
        return;
    }
    else {
        $("#SponsorshipForm").submit();
    }
}

function SubmitSponsorshipRecipientForm() {

    if ($("#SelectedRecipientContactID").val() == "" || $("#SelectedRecipientContactID").val() == null) {
        $("#ErrorMessage").show();
        animateTo("ErrorMessage");

        return;
    }
    else if ($("#SelectedRecipientEvent").val() == 0 || $("#RecipientComment").val() == null || $("#RecipientComment").val() == '') {
        $("#Message").html("Please fill in all required fields.")
        $("#ErrorMessage").show();
        animateTo("ErrorMessage");
        return;
    }
    else if ($("#RecipientSponsoredAmount").val() < 1) {
        $("#Message").html("Sponsorship amount must be larger than 1.")
        $("#ErrorMessage").show();
        animateTo("ErrorMessage");
        return;
    }
    else {
        $("#SponsorshipRecipientForm").submit();
    }

}

function SponsorshipSelect() {

    var conferenceID = $("#SponsorshipSelection").val();

    if (conferenceID != null || conferenceID != 0) {
        $.ajax({
            cache: false,
            type: "POST",
            url: "/Administration/SponsorshipManagement",
            data: { "Conference_ID": conferenceID },
            success: function (page) {

                $("#SponsorshipIndexPartial").html(page);
                $("#generatedSponsorshipTable").DataTable({
                    fixedHeader: {
                        header: true,
                        footer: true
                    },
                    language: {
                        search: "",
                        searchPlaceholder: "Search..."
                    },
                    paging: true,
                    "pageLength": 10,
                    "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                    "columnDefs": [{ "targets": 4, "type": "date-euro" }],
                    info: false,
                    cache: false,
                    scrollCollapse: true,
                    responsive: true,
                    processing: true,
                    searching: true,

                    order: [6, 'desc'],
                    bAutoWidth: true,
                });

                $('#generatedSponsorshipTable_wrapper .col-md-7:eq(0)').addClass("d-flex justify-content-center justify-content-md-end");
                $('#generatedSponsorshipTable_paginate').addClass("mt-3 mt-md-2");
            },
            error: function () {
            }
        });
    }
}

function filterSponsorshipTable() {
    var table, type;

    type = $("#filterSponsorship").val();
    table = document.getElementById("generatedSponsorshipTable");
    $(table).DataTable().columns(1).search(type, true).draw();

}

function EditSponsorship(variablePaymentID, conferenceID) {

    $.ajax({
        cache: false,
        type: "POST",
        url: "/Administration/EditSponsorship",
        data: { "VariablePayment_ID": variablePaymentID, "Conference_ID": conferenceID },
        success: function (view) {
            $("#EditModal").html(view);
            $("#Edit_" + variablePaymentID).modal("show");
        },
        error: function () {
        }
    });

}

function UpdateSponsorship(variablePaymentID) {

    var originalAmount = parseFloat($("#amount_" + variablePaymentID).html());
    var inout = $("#InOut_" + variablePaymentID).html();
    var grandtotal = parseFloat($("#GrandTotal").html());
    var total = parseFloat($("#TotalAmount").html());
    //Edited fields
    var confID = $("#SelectedEvent").val();
    var paymentAmount = $("#PaymentAmount").val();
    var comment = $("#Comment").val();

    var amountDiff = originalAmount - paymentAmount;

    $.ajax({
        cache: false,
        type: "POST",
        async: false,
        url: "/Administration/UpdateVariablePayment",
        data: {
            "VariablePayment_ID": variablePaymentID, "Conference_ID": confID,
            "PaymentAmount": paymentAmount, "Comment": comment
        },
        success: function (json) {

            if (json.success) {

                $("#Edit_" + variablePaymentID).modal("hide");
                $("#Message").html("Successfully updated variable payment.");
                $("#SuccessMessage").show();
                $("#errorMessage").hide();

                if (inout == "Donor") {
                    $("#GrandTotal").html((grandtotal - amountDiff).toFixed(2));
                    $("#TotalAmount").html((total - amountDiff).toFixed(2));
                }
                else if (inout == "Recipient") {
                    $("#GrandTotal").html((grandtotal + amountDiff).toFixed(2));
                    $("#TotalAmount").html((total + amountDiff).toFixed(2));
                }

                $("#comment_" + variablePaymentID).html(comment);
                $("#amount_" + variablePaymentID).html(paymentAmount);

                if (confID != $("#SponsorshipSelection").val()) {
                    $("#" + variablePaymentID).hide();
                };

            }
        },
        error: function () {
        }
    });
}

function ShowRemoveConfirmation(variablePaymentID, donor) {

    $("#DeleteConfirmation").attr("style", "background-color:rgba(0, 0, 0, 0.7)")
    var confirmation = "";
    confirmation += '<button class="btn btn-primary" onclick="RefundSponsorship(' + variablePaymentID + ', ' + donor +')">Yes</button>';
    confirmation += '<button data-dismiss="modal" class="btn btn-secondary">No</button>';
    $("#ConfirmationBtn").html(confirmation);
    $("#DeleteConfirmation").modal("show");

}

function RemoveVariablePayment(VariablePaymentID) {
    $.ajax({
        cache: false,
        type: "POST",
        url: "/Administration/RemoveSponsorship",
        data: { "VariablePayment_ID": VariablePaymentID },
        success: function (json) {
            var message = "";
            if (json.message == true) {
                message = "Successfully deleted sponsorship";
            } else {
                message = "This Sponsorship cannot be removed."
            }
            $("#Message").html(message);
            $("#SuccessMessage").show();
            animateTo("SuccessMessage");
        }
    });
}

function RefundSponsorship(variablePaymentID, donor) {

    var inout = $("#InOut_" + variablePaymentID).html();
    var amount = parseFloat($("#amount_" + variablePaymentID).html());
    var grandtotal = parseFloat($("#GrandTotal").html());
    var total = parseFloat($("#TotalAmount").html());
    var URL = "/Administration/RefundSponsorship";
    var isDonor = donor == 1;
    $.ajax({
        cache: false,
        type: "POST",
        url: URL,
        data: { "VariablePayment_ID": variablePaymentID, "Donor": isDonor },
        success: function (json) {

            if (inout == "Donor") {
                $("#GrandTotal").html((grandtotal - amount).toFixed(2));
                $("#TotalAmount").html((total - amount).toFixed(2));
            }
            else if (inout == "Recipient") {
                $("#GrandTotal").html((grandtotal + amount).toFixed(2));
                $("#TotalAmount").html((total + amount).toFixed(2));
            }

            $("#generatedSponsorshipTable").DataTable().row($("#" + variablePaymentID)).remove().draw(false);
            $("#" + variablePaymentID).remove();
            $("#DeleteConfirmation").modal("hide");
            $("#Edit_" + variablePaymentID).modal("hide");
            $("#Message").html("Successfully unallocated and refunded sponsorship");
            $("#SuccessMessage").show();
            animateTo("SuccessMessage");
        },
        error: function () {
        }
    });

}

function ShowSponsorshipBreakdownSelection() {

    var breakdownselect = $("#BreakdownSelect").val();

    if (breakdownselect == "Campus Breakdown") {
        $("#campusBreakdown").show();
        $("#ministryBreakdown").hide();
        $("#paymentTypeBreakdown").hide();
    }
    if (breakdownselect == "Ministry Breakdown") {
        $("#campusBreakdown").hide();
        $("#ministryBreakdown").show();
        $("#paymentTypeBreakdown").hide();
    }
    if (breakdownselect == "Payment Type Breakdown") {
        $("#campusBreakdown").hide();
        $("#ministryBreakdown").hide();
        $("#paymentTypeBreakdown").show();
    }
}

//========================End of Sponsorship Section=========================


///// Search sponsorship contact
//function doneVoucherTyping() {
//    var firstname = $('#contactFirstName').val();
//    var lastname = $('#contactLastName').val();
//    var mobile = $('#contactMobile').val();
//    var email = $('#contactEmail').val();

//    $.ajax({
//        cache: false,
//        type: "POST",
//        url: "/Administration/SearchContacts",
//        data: { "FirstName": firstname, "LastName": lastname, "Mobile": mobile, "Email": email },
//        success: function (json) {

//            var content = "<table class='table card-body'><thead id='voucherContactHeader'><tr>";
//            content += "<th>ID</th><th>Name</th><th>Mobile</th><th>Email</th><th>Action</th>";
//            content += "</tr></thead>";
//            content += "<tbody id='voucherContactList'>";

//            for (var i = 0; i < json.length; i++) {
//                content += "<tr >";
//                content += "<td id='id_" + json[i].Contact_ID + "'>" + json[i].Contact_ID + "</td>";
//                content += "<td id='name_" + json[i].Contact_ID + "'>" + json[i].FirstName + " " + json[i].LastName + "</td>";
//                content += "<td id='mobile_" + json[i].Contact_ID + "'>" + json[i].Mobile + "</td>";
//                content += "<td id='email_" + json[i].Contact_ID + "'>" + json[i].Email + "</td>";
//                content += "<td>" + "<button class='btn btn-primary btn-sm' onclick='selectVoucherContact(" + json[i].Contact_ID + ")'>Select</button>" + "</td>";
//                content += "</tr >";
//            }

//            content += "</tbody></table>";
//            $('#voucherContactTable').html(content);
//        },
//        error: function () {
//        }
//    });
//}

//function selectVoucherContact(id) {

//    var selectedName = $("#name_" + id).html();
//    var selectedMobile = $("#mobile_" + id).html();
//    var selectedEmail = $("#email_" + id).html();

//    $("#SelectedContactID").val(id);
//    $("#selectedFullName").html(selectedName);
//    $("#selectedMobile").html(selectedMobile);
//    $("#selectedEmail").html(selectedEmail);
//    $("#selectedContactResult").show();

//    $("#selectedAssignee").html('<label>Selected Assignee: </label><span>' + selectedName + '</span>');
//    $("#selectedAssigneeMobile").html('<label>Mobile: </label><span>' + selectedMobile + '</span>');
//    $("#selectedAssigneeEmail").html('<label>Email: </label><span>' + selectedEmail + '</span>');
//    $("#selectedAssignee").show();
//    $("#selectedAssigneeMobile").show();
//    $("#selectedAssigneeEmail").show();

//}

function unselectContact() {
    $("#SelectedContactID").val("");
    $("#selectedContactResult").hide();

    $("#selectedAssignee").hide();
    $("#selectedAssigneeMobile").hide();
    $("#selectedAssigneeEmail").hide();
}

function convertToJavaScriptDate(value) {
    var pattern = /Date\(([^)]+)\)/;
    var results = pattern.exec(value);
    var dt = new Date(parseFloat(results[1]));
    return (dt.getMonth() + 1) + "/" + dt.getDate() + "/" + dt.getFullYear();
}

function isValidDate(dateinput) {
    // First check for the pattern
    var regex_date = /^\d{4}\-\d{1,2}\-\d{1,2}$/;

    if (!regex_date.test(dateinput)) {
        return false;
    }

    // Parse the date parts to integers
    var parts = dateinput.split("-");
    var day = parseInt(parts[2], 10);
    var month = parseInt(parts[1], 10);
    var year = parseInt(parts[0], 10);

    // Check the ranges of month and year
    if (year < 1900 || year > 3000 || month == 0 || month > 12) {
        return false;
    }

    var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    // Adjust for leap years
    if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0)) {
        monthLength[1] = 29;
    }

    // Check the range of the day
    return day > 0 && day <= monthLength[month - 1];
}


function CheckRecipientCurrency() {

    var ConferenceID = $("#SelectedRecipientEvent").val();

    $.ajax({
        cache: false,
        type: "POST",
        url: "/Administration/CheckCurrency",
        data: { "Conference_ID": ConferenceID },
        success: function (json) {

            $("#recipientCurrency").html(json.Currency);
            $("#TotalRecipientAmount").show();

        },
        error: function () {
        }
    });
}