﻿$(document).ready(function () {
    if ($("#DataroleTable").length > 0) {
        $("#DataroleTable").DataTable({
            fixedHeader: {
                header: true,
                footer: true
            },
            "bPaginate": false,
            "bLengthChange": false,
            "bFilter": true,
            "bInfo": false,
            "bAutoWidth": false,
            "autoWidth": false,
            scrollCollapse: true,
            responsive: true,
            paging: false,
            order: [],
            language: {
                search: "",
                searchPlaceholder: "Search..."
            },
            scrollY: 600
        });
    }
});

//========================================================Database Role Management Region====================================================================================

function SaveDbRolePermission(DbRoleID) {
    var table = document.getElementById("tbody_" + DbRoleID).rows;
    for (i = 0; i < table.length; i++) {
        var DbRolePermissionID = $(table[i]).attr('id');
        if (DbRolePermissionID != null) {
            var DbObjectName = $("#DbObjectName_" + DbRolePermissionID).val();
            var DbObjectDesc = $("#DbObjectDesc_" + DbRolePermissionID).val();
            var DbRolePermRead = document.getElementById("DbRolePermissionRead_" + DbRolePermissionID).checked;
            var DbRolePermWrite = document.getElementById("DbRolePermissionWrite_" + DbRolePermissionID).checked;

            $.ajax({
                cache: false,
                type: "POST",
                url: "/Management/SaveDbRolePermission",
                data: { "DbRolePermissionID": DbRolePermissionID, "DbObjectName": DbObjectName, "DbObjectDesc": DbObjectDesc, "DbRolePermRead": DbRolePermRead, "DbRolePermWrite": DbRolePermWrite },
                success: function (json) {

                    if (!json.success) {
                        $("#SuccessEditDbRolePermission_" + DbRoleID).html("Cannot Save Changes on Database Role Permission.");
                        $("#SuccessEditDbRolePermission_" + DbRoleID).attr("class", "alert alert-danger");
                    }
                    $("#SuccessEditDbRolePermission_" + DbRoleID).show();
                    animateTo("SuccessEditDbRolePermission_" + DbRoleID);
                    $("#SuccessEditDbRolePermission_" + DbRoleID).fadeOut(3000);
                },
                error: function () {
                    alert("There are some error to delete DatabaseRole Permission.");
                    return false;
                }
            });
            scrollToSuccessMessageDbPerm(DbRoleID);
            
        }

    }

}

function DeleteDbRole(DbRoleID) {
    $.ajax({
        cache: false,
        type: "POST",
        url: "/Management/DeleteDbRole",
        data: { "DbRoleID": DbRoleID},
        success: function (json) {

            if (json.success) {
                $("#DbRole_" + DbRoleID).hide();
                $("#deleteConfirmation_" + DbRoleID).modal("hide");
            }
            else {
                $("#SuccessDeleteRole").attr("class", "alert alert-danger fa-border");
                $("#SuccessDeleteRole").html("<strong>Cannot Delete Database Role. Some Error Occured.</strong>");
            }
            $("#SuccessDeleteRole").show();
            animateTo("SuccessDeleteRole");
            $("#SuccessDeleteRole").fadeOut(3000);
        },
        error: function () {
            alert("There are some error to delete DatabaseRole.");
        }

    });
}

//-------------------------Used to scroll to success message for better UX---------------------------------
function scrollToSuccessMessageDbPerm(DbRoleID) {
    target_offset = $('#SuccessEditDbRolePermission_' + DbRoleID).offset(),
        target_top = target_offset.top;
    $('html, body').animate({
        scrollTop: target_top - 100
    }, 100);
}

//----------------------------Used to close pop up modal---------------------------------------------------
function closeModal() {
    $('a.close-reveal-modal').trigger('click');
}

function AddDbRole() {
    var DbRoleName = $("#DbRoleName").val();
    var everythingSuccess = false;

    if (DbRoleName == null || DbRoleName == '') {
        $("#SuccessAddedRole").attr("class", "alert alert-danger fa-border");
        $("#SuccessAddedRole").html("<strong>Database Role name cannot be empty.</strong>");
        $("#SuccessAddedRole").show();
        animateTo("SuccessAddedRole");
        $("#SuccessAddedRole").fadeOut(3000);
        return false;
    }
    else {

        $.ajax({
            cache: false,
            async: false,
            type: "POST",
            url: "/Management/AddDbRole",
            data: { "DbRoleName": DbRoleName },
            success: function (json) {

                if (json.success) {
                    console.log()
                    everythingSuccess = true;
                    alert("Database Role added!")
                    $("#backButton").click()
                }
                else {
                    everythingSuccess = false;
                    alert("Unable to add database role.")
                }
            },
            error: function () {
                alert("There are some error to add Database Role.");
                return false;
            }
        });

        var table = document.getElementsByTagName("tbody")[0].rows;

        for (i = 0; i < table.length; i++) {
            var DbObjID = $(table[i]).attr('id');
            var DbRolePermRead = document.getElementById("Read_" + DbObjID).checked;
            var DbRolePermWrite = document.getElementById("Write_" + DbObjID).checked;

            if (DbRolePermRead || DbRolePermWrite) {

                $.ajax({
                    cache: false,
                    type: "POST",
                    async: false,
                    url: "/Management/SaveNewDbRolePermission",
                    data: { "DbObjID": DbObjID, "DbRolePermRead": DbRolePermRead, "DbRolePermWrite": DbRolePermWrite },
                    success: function (json) {
                        
                        if (json.success) {
                            everythingSuccess = true;

                        }
                        else {
                            everythingSuccess = false;
                        }
                    },
                    error: function () {
                        alert("There are some error to add DatabaseRole Permission.");
                        return false;
                    }
                });

            }
        }
        if (everythingSuccess) {
            $("#SuccessAddedRole").attr("class", "alert alert-success fa-border");
            $("#SuccessAddedRole").html("<strong>Successfully added role.</strong>");
        }
        else {
            $("#SuccessAddedRole").attr("class", "alert alert-danger fa-border");
            $("#SuccessAddedRole").html("<strong>Cannot Add Database Role. Some Error Occured.</strong>");
        }
        $("#SuccessAddedRole").show();
        animateTo("SuccessAddedRole");
        $("#SuccessAddedRole").fadeOut(3000);
    }
} 

function removeUserDbRole(DbRoleID, ContactID) {
    $.ajax({
        cache: false,
        type: "POST",
        async: false,
        url: "/Management/DeleteContactAccessLevel",
        data: { "DbRoleID": DbRoleID, "ContactID": ContactID},
        success: function (json) {

            if (json.success) {
                $("#UserDbRole_" + ContactID + DbRoleID).hide();
                $("#SuccessRemoveUserRole_" + ContactID).attr("class", "alert alert-success fa-border");
                $("#SuccessRemoveUserRole_" + ContactID).html("<strong>Successfully deleted user database role.</strong>");
                var RoleName = $("#RoleName_" + ContactID + DbRoleID).val();
                select = document.getElementById('AddUserDbRole_' + ContactID);
                var opt = document.createElement('option');
                opt.value = DbRoleID;
                opt.innerHTML = RoleName;
                select.appendChild(opt);
            }
            else {
                $("#SuccessRemoveUserRole_" + ContactID).attr("class", "alert alert-danger fa-border");
                $("#SuccessRemoveUserRole_" + ContactID).html("<strong>Cannot delete user database role. Some Error Occured.</strong>");
            }
            $("#SuccessRemoveUserRole_" + ContactID).show();
            animateTo("SuccessRemoveUserRole_" + ContactID);
            $("#SuccessRemoveUserRole_" + ContactID).fadeOut(3000);
        },
        error: function () {
            alert("There are some error to delete User Db Role.");
            return false;
        }
    });
}

function AddUserDbRole(ContactID) {
    var DbRoleID = $("#AddUserDbRole_" + ContactID).val();
    if (DbRoleID == null) {
        $("#SuccessAddedUserDbRole_" + ContactID).attr("class", "alert alert-danger fa-border");
        $("#SuccessAddedUserDbRole_" + ContactID).html("<strong>No Access has been selected.</strong>");
        $("#SuccessAddedUserDbRole_" + ContactID).show();
        animateTo("SuccessAddedUserDbRole_" + ContactID);
        $("#SuccessAddedUserDbRole_" + ContactID).fadeOut(3000);
        return false;
    }
    $.ajax({
        cache: false,
        type: "POST",
        async: false,
        url: "/Management/AddContactAccessLevel",
        data: { "DbRoleID": DbRoleID, "ContactID": ContactID },
        success: function (json) {

            if (json.success) {
                $("#PendingAddRole_" + DbRoleID).hide();
                $("#SuccessAddedUserDbRole_" + ContactID).attr("class", "alert alert-success fa-border");
                $("#SuccessAddedUserDbRole_" + ContactID).html("<strong>Successfully added user database role.</strong>");
                $("#AddUserDbRole_" + ContactID + " option[value='" + DbRoleID + "']").remove();

                var html = "";
                html += '<tr id="UserDbRole_' + json.contactID + json.dbRoleID + '">';
                html += '<td>' + json.dbRoleName + '</td>';
                html += '<td>' + json.addedDate + '</td>';
                html += '<td><input hidden id="RoleName_' + json.contactID + json.dbRoleID + '" value="' + json.dbRoleName + '" />';
                html += '<button class="btn btn-primary" onclick="removeUserDbRole(' + json.dbRoleID + "," + json.contactID + ')">Remove</button></td>';
                html += '</tr>';
                $("#userDbRoleTable").append(html);
            }
            else {
                $("#SuccessAddedUserDbRole_" + ContactID).attr("class", "alert alert-danger fa-border");
                $("#SuccessAddedUserDbRole_" + ContactID).html("<strong>Cannot add user database role. Some Error Occured.</strong>");
            }
            $("#SuccessAddedUserDbRole_" + ContactID).show();
            animateTo("SuccessAddedUserDbRole_" + ContactID);
            $("#SuccessAddedUserDbRole_" + ContactID).fadeOut(3000);
        },
        error: function () {
            alert("There are some error to add User Db Role.");
            return false;
        }
    });
}

function CustomReadChange(DbObjID, ContactID) {
    var readcheck = document.getElementById("CustomRead_" + DbObjID).checked;
    
    $.ajax({
        cache: false,
        type: "POST",
        async: false,
        url: "/Management/UpdateAccessLevelReadPermission",
        data: { "DbObjID": DbObjID, "ContactID": ContactID, "Read": readcheck},
        success: function (json) {

            if (json.success) {
                $("#SuccessUpdated_" + ContactID).attr("class", "alert alert-success fa-border");
                $("#SuccessUpdated_" + ContactID).html("<strong>Successfully updated read permission.</strong>");
            }
            else {
                $("#SuccessUpdated_" + ContactID).attr("class", "alert alert-danger fa-border");
                $("#SuccessUpdated_" + ContactID).html("<strong>Cannot update role permission. Some Error Occured.</strong>");
            }
            $("#SuccessUpdated_" + ContactID).show();
            animateTo("SuccessUpdated_" + ContactID);
            $("#SuccessUpdated_" + ContactID).fadeOut(3000);
        },
        error: function () {
            alert("There are some error to change User Role Permission.");
            return false;
        }
    });
}

function CustomWriteChange(DbObjID, ContactID) {
    var writecheck = document.getElementById("CustomWrite_" + DbObjID).checked;

    $.ajax({
        cache: false,
        type: "POST",
        async: false,
        url: "/Management/UpdateAccessLevelWritePermission",
        data: { "DbObjID": DbObjID, "ContactID": ContactID, "Write": writecheck },
        success: function (json) {

            if (json.success) {
                $("#SuccessUpdated_"+ContactID).attr("class", "alert alert-success fa-border");
                $("#SuccessUpdated_" + ContactID).html("<strong>Successfully updated write permission.</strong>");
            }
            else {
                $("#SuccessUpdated_" + ContactID).attr("class", "alert alert-danger fa-border");
                $("#SuccessUpdated_" + ContactID).html("<strong>Cannot update role permission. Some Error Occured.</strong>");
            }
            $("#SuccessUpdated_" + ContactID).show();
            animateTo("SuccessUpdated_" + ContactID);
            $("#SuccessUpdated_" + ContactID).fadeOut(3000);
        },
        error: function () {
            alert("There are some error to change User Role Permission.");
            return false;
        }
    });
}

function CustomAdd(DbObjID, ContactID) {
    var readcheck = document.getElementById("AddRead_" + DbObjID).checked;
    var writecheck = document.getElementById("AddWrite_" + DbObjID).checked;

    $.ajax({
        cache: false,
        type: "POST",
        async: false,
        url: "/Management/AddContactAccessLevelReadWritePermission",
        data: { "DbObjID": DbObjID, "ContactID": ContactID, "ReadCheck": readcheck, "WriteCheck": writecheck},
        success: function (json) {

            if (json.success) {
                $("#SuccessAdded_" + ContactID).attr("class", "alert alert-success fa-border");
                $("#SuccessAdded_" + ContactID).html("<strong>Successfully added contact role permission.</strong>");
            }
            else {
                $("#SuccessAdded_" + ContactID).attr("class", "alert alert-danger fa-border");
                $("#SuccessAdded_" + ContactID).html("<strong>Cannot add role permission. Some Error Occured.</strong>");
            }
            $("#SuccessAdded_" + ContactID).show();
            animateTo("SuccessAdded_" + ContactID);
            $("#SuccessAdded_" + ContactID).fadeOut(3000);
        },
        error: function () {
            alert("There are some error to add Contact Role Permission.");
            return false;
        }
    });
}


function RevealUserDataTable(id) {

    if (!$.fn.DataTable.isDataTable("#table_" + id)) {
        setTimeout(function () {
            $("#table_" + id).DataTable({
                fixedHeader: {
                    header: true,
                    footer: true
                },
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": true,
                "bInfo": false,
                "bAutoWidth": false,
                "autoWidth": false,
                scrollCollapse: true,
                responsive: true,
                paging: false,
                language: {
                    search: "",
                    searchPlaceholder: "Search..."
                },
                "columns": [
                    null,
                    { "orderDataType": "dom-checkbox", type: 'bool' },
                    { "orderDataType": "dom-checkbox", type: 'bool' }
                ],
                scrollY: 600
            });
        }, 500);
    }
    
}


function RevealUserObjDataTable(id) {

    if (!$.fn.DataTable.isDataTable("#Objtable_" + id)) {
        setTimeout(function () {
            $("#Objtable_" + id).DataTable({
                fixedHeader: {
                    header: true,
                    footer: true
                },
                "bPaginate": false,
                "bLengthChange": false,
                "bFilter": true,
                "bInfo": false,
                "bAutoWidth": false,
                "autoWidth": false,
                scrollCollapse: true,
                responsive: true,
                paging: false,
                language: {
                    search: "",
                    searchPlaceholder: "Search..."
                },
                "columns": [
                    null,
                    { "orderDataType": "dom-checkbox", type: 'bool' },
                    { "orderDataType": "dom-checkbox", type: 'bool' }
                ],
                scrollY: 600
            });
        }, 500);
    }

}


//==========================================End of Database Role Management Region==============================================================================