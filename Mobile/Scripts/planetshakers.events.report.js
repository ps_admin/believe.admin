﻿$(document).ready(function () {

    $('#confYears').multiselect({
        includeSelectAllOption: true
    });

    $('#ddlRegoType').prop("disabled", true);

    $('#ddlConf').click(function () {
        var id = $("#ddlConf").val();
        $.ajax({
            cache: false,
            type: "GET",
            url: 'GetRegoType',
            data: { "Conference_ID": id },
            success: function (json) {
                $("#ddlRegoType").empty();
                json = json || {};
                for (var i = 0; i < json.length; i++) {
                    $("#ddlRegoType").append('<option value="' + json[i].Value + '">' + json[i].Text + '</option>');
                }
                $("#ddlRegoType").prop("disabled", false);
            },
            error: function () {
                alert("No Registration Type found");
                $("#ddlRegoType").prop("disabled", true);
            }
        });
    });
});

//========================Total Registration Report=========================

function GenerateTotalRegistrationReport() {
    var confSelected_ID = $("#totalRegistrationReportSelection").val();
    //retreat checked years 
    $("#reportLoader").modal('show');

    $.ajax({
        cache: false,
        type: "POST",
        url: "/Report/GenerateTotalRegistrationReports",
        data: { "conferenceSelectionID": confSelected_ID },
        success: function (result) {
            $("#reportLoader").modal('hide');
            $("#TotalRegistrationReportResult").html(result);
        }
    });
}
//========================End of Total Registration Report=========================

//=============================Total Registrant Report=============================
function pullstats(conference_ID) {
    event.preventDefault();

    $.ajax({
        type: 'GET',
        url: 'pullstatistics',
        data: { "conference_ID": conference_ID },
        success: function (json) {
            var stats = json.result;
            var result = "<div class='col-12 mb - 4'>"
            result += "<h3>" + stats.ConferenceName + "</h3> <br/>"


            //Table for each registrationID
            if (stats.StatsList.length != 0) {
                result += "<h5>Registration Types</h5><table class='table table-bordered text-black-100'>";
                result += "<thead><tr><th>Registration Type</th><th>Allocated</th><th>Unallocated</th><th>Total</th><th>Capacity</th><th>Capacity %</th></tr></thead><tbody>";

                stats.StatsList.forEach(item => {

                    result += "<tr><td>" + item.RegistrationTypeName + "</td>";

                    result += "<td>" + item.Allocated + "</td>";
                    if (item.Unallocated != null) {
                        result += "<td>" + item.Unallocated + "</td>";
                    }
                    else {
                        result += "<td>0</td>";
                    }
                    if (item.TotalRegistration) {
                        result += "<td>" + (item.TotalRegistration) + "</td>"
                    }
                    else {
                        result += "<td>0</td>";
                    }
                    if (item.Capacity != null) {
                        result += "<td>" + item.Capacity + "</td>";
                        result += "<td>" + ((item.TotalRegistration / item.Capacity) * 100).toFixed(2) + "%</td>";
                    }
                    else {
                        result += "<td>N/A</td><td>N/A</td></tr>"
                    }
                });

                result += "</table > ";
            }

            //summary table
            if (stats.AdultList != null || stats.BoomList != null || stats.KidsList || null) {
                result += "<h5>Summary</h5><table class='table table-bordered text-black-100'>";
                result += "<thead><tr><th>Registration Type</th><th>Allocated</th><th>Unallocated</th><th>Total Registration</th></tr></thead>";
                result += "<tbody>";

                if (stats.AdultList != null) {
                    result += "<tr><td>" + stats.AdultList.RegistrationTypeName + "</td>";
                    result += "<td>" + stats.AdultList.Allocated + "</td>";
                    if (stats.AdultList.Unallocated != null) {
                        result += "<td>" + stats.AdultList.Unallocated + "</td>";
                    } else {
                        result += "<td>0</td>";
                    }
                    if (stats.AdultList.TotalRegistration) {
                        result += "<td>" + (stats.AdultList.TotalRegistration) + "</td>"
                    } else {
                        result += "<td>0</td></tr>";
                    }
                }

                if (stats.BoomList != null) {
                    result += "<tr><td>" + stats.BoomList.RegistrationTypeName + "</td>";
                    result += "<td>" + stats.BoomList.Allocated + "</td>";
                    if (stats.BoomList.Unallocated != null) {
                        result += "<td>" + stats.BoomList.Unallocated + "</td>";
                    } else {
                        result += "<td>0</td>";
                    }
                    if (stats.BoomList.TotalRegistration) {
                        result += "<td>" + (stats.BoomList.TotalRegistration) + "</td>"
                    } else {
                        result += "<td>0</td></tr>";
                    }
                }


                if (stats.KidsList != null) {
                    result += "<tr><td>" + stats.KidsList.RegistrationTypeName + "</td>";
                    result += "<td>" + stats.KidsList.Allocated + "</td>";
                    if (stats.KidsList.Unallocated != null) {
                        result += "<td>" + stats.KidsList.Unallocated + "</td>";
                    } else {
                        result += "<td>0</td>";
                    }
                    if (stats.KidsList.TotalRegistration) {
                        result += "<td>" + (stats.KidsList.TotalRegistration) + "</td>"
                    } else {
                        result += "<td>0</td></tr>";
                    }
                }
                if (stats.StaffRegistration != 0) {
                    result += "<tr><td>Staff Registration</td><td></td><td></td><td>" + stats.StaffRegistration + "</td></tr>"
                }


                result += "</table></div>";
            }
            $("#conferenceStatsResult").html(result);
            $("#conferenceStats_modal").show();
            $('body').addClass('modal-open');
            $('body').css('overflow', 'hidden');
            $("#conferenceStats_modal").addClass('show');
            $('body').append('<div class="modal-backdrop fade show"></div>');

        }

    })
};
$('#closecsmodal').click(function () {
    $('body').removeClass('modal-open');
    $('body').css('padding-right', '');
    $('body').css('overflow', 'auto');
    $(".modal-backdrop").remove();
    $("#conferenceStats_modal").hide();
    $("#conferenceStats_modal").modal("hide");
    $("#conferenceStats_modal").attr('style', '');
    $("#conferenceStats_modal").removeClass('show');
});
//============================End of Total Registrant Report==============================
