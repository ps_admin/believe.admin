﻿$(document).ready(function () {
    $("#sortConferenceTable tbody").sortable();
    $("#sortRegoType tbody").sortable();
});

function generateRegotype() {
    var conference = $("#selectedConference").val();
    if (conference == "" || conference == null) {
        $("#conferenceCheck").show();
        return false;
    }
    else {
        $("#conferenceCheck").hide();
        $.ajax({
            cache: false,
            async: false,
            type: "POST",
            url: 'GenerateRegistrationType',
            data: {
                "ConferenceID": conference
            },
            success: function (json) {
                $("#regoTypeViewResult").html(json);
                $('#regoTypeDisplayTable').DataTable({
                    fixedHeader: {
                        header: true,
                        footer: true
                    },
                    "bPaginate": false,
                    "bLengthChange": false,
                    "bFilter": true,
                    "bInfo": false,
                    "bAutoWidth": false,
                    "autoWidth": false,
                    "aaSorting": [],
                    scrollCollapse: true,
                    responsive: true,
                    paging: false,
                    language: {
                        search: "",
                        searchPlaceholder: "Search..."
                    },
                    scrollY: 600
                });
            },
            error: function () {
                alert("Finding not successful.");
            }
        });
    }
};

function addRegistrationType() {
    var conferenceID = $("#selectedConference").val();
    var regoType = $("#RegoType").val();
    var limit = $("#RegoLimit").val();
    var cost = $("#RegoCost").val();
    var startDate = $("#StartDate").val();
    var endDate = $("#EndDate").val();
    var startTime = $("#StartTime").val();
    var endTime = $("#EndTime").val();
    //var IsCollegeRego = document.getElementById("IsCollegeRego").checked;
    var IsCollegeRego = false;
    var IsApplicationRego = document.getElementById("IsApplicationRego").checked;
    var IsAddonRego = document.getElementById("IsAddonRego").checked;
    var Questions = $("#AddQuestionSelected").val();
    var DocTypes = $("#AddDocSelected").val();
    var flag = true;


    //data validation
    if (regoType === '') {
        $("#addRegoNameCheck").show();
        flag = false;
    }
    else {
        $("#addRegoNameCheck").hide();
    }
    //check if rego name exist
    $.ajax({
        cache: false,
        async: false,
        type: "POST",
        url: 'CheckRegoTypeName',
        data: { 'Conference_ID': conferenceID, 'regoName': regoType },
        success: function (json) {
            if (json.sameName) {
                $("#sameRegoNameCheck").show();
                flag = false;
            }
        }
    })
    if (cost === '') {
        $("#addRegoCostCheck").show();
        flag = false;
    }
    else {
        $("#addRegoCostCheck").hide();
    }
    if (limit === '') {
        $("#addRegoLimitCheck").show();
        flag = false;
    }
    else {
        $("#addRegoLimitCheck").hide();
    }

    //check empty data
    if (startDate === '' || endDate === '' || startTime === '' || endTime === '') {
        $("#addRegoDateCheck").show();
        flag = false;
    }
    else {
        $("#addRegoDateCheck").hide();
    }
    if (startDate >= endDate) {
        $("#addRegoDateSequenceCheck").show();
        flag = false;
    }
    else {
        $("#addRegoDateSequenceCheck").hide();
    }
    if (flag) {
        var QuestionList = [];
        for (let i = 0; i < Questions.length; i++) {
            QuestionList.push(Questions[i])
        }
        var DocList = [];
        for (let i = 0; i < DocTypes.length; i++) {
            DocList.push(DocTypes[i])
        }
        $.ajax({
            cache: false,
            async: false,
            type: "POST",
            url: 'AddNewRegoType',
            data: {
                "conferenceID": conferenceID,
                "regoTypeName": regoType,
                "regoLimit": limit,
                "regoCost": cost,
                "startDate": startDate,
                "endDate": endDate,
                "startTime": startTime,
                "endTime": endTime,
                "IsCollegeRego": IsCollegeRego,
                "IsApplicationRego": IsApplicationRego,
                "IsAddonRego": IsAddonRego,
                "QuestionList": QuestionList,
                "DocTypeList": DocList,
            },
            success: function () {
                $("#addregistration").modal("hide");
                setTimeout(generateRegotype, 300);
                $("#addRegoTypeSuccess").show();
                $("#addRegoTypeSuccess").fadeOut(3000)
            },
            error: function () {
                alert("Create not successful.");
            }
        });
    }
    else {
        return false;
    }
};

function editRegoType(regoTypeID) {
    var conferenceID = $("#selectedConference").val();
    var regoType = $("#editRegoTypeName_" + regoTypeID).val();
    var limit = $("#editRegoLimit_" + regoTypeID).val();
    var cost = $("#editRegoCost_"+regoTypeID).val();
    var startDate = $("#editStartDate_" + regoTypeID).val();
    var endDate = $("#editEndDate_" + regoTypeID).val();
    var startTime = $("#editStartTime_" + regoTypeID).val();
    var endTime = $("#editEndTime_" + regoTypeID).val();
    //var IsCollegeRego = document.getElementById("editIsCollegeRegoType_" + regoTypeID).checked;
    var IsCollegeRego = false;
    var IsApplicationRego = document.getElementById("editApplicationRegoType_" + regoTypeID).checked;
    var IsAddonRego = document.getElementById("editAddOnRegoType_" + regoTypeID).checked;
    var Questions = $("#QuestionSelected_" + regoTypeID).val();
    var DocTypes = $("#DocSelected_" + regoTypeID).val();

    var flag = true;
    //data validation
    if (regoType === '') {
        $("#editRegoNameCheck").show();
        flag = false;
    }
    else {
        $("#editRegoTypeCheck").hide();
    }
    if (cost === '') {
        $("#editRegoCostCheck").show();
        flag = false;
    }
    else {
        $("#editRegoCostCheck").hide();
    }
    if (limit === '') {
        $("#editRegoLimitCheck").show();
        flag = false;
    }
    else {
        $("#editRegoLimitCheck").hide();
    }

    //check empty data
    if (startDate === '' || endDate === '' || startTime === '' || endTime === '') {
        $("#editRegoDateCheck").show();
        flag = false;
    }
    else {
        $("#editRegoDateCheck").hide();
}
    if (startDate >= endDate) {
        $("#editRegoDateSequenceCheck").show();
        flag = false;
    }
    else {
        $("#editRegoDateSequenceCheck").hide();
    }

    if (flag) {
        var QuestionList = [];
        for (let i = 0; i < Questions.length; i++) {
            QuestionList.push(Questions[i])
        }
        var DocList = [];
        for (let i = 0; i < DocTypes.length; i++) {
            DocList.push(DocTypes[i])
        }
        $.ajax({
            cache: false,
            async: false,
            type: "POST",
            url: 'EditRegoType',
            data: {
                "conferenceID": conferenceID,
                "regoTypeID": regoTypeID,
                "regoTypeName": regoType,
                "regoLimit": limit,
                "regoCost": cost,
                "startDate": startDate,
                "endDate": endDate,
                "startTime": startTime,
                "endTime": endTime,
                "IsCollegeRego": IsCollegeRego,
                "IsApplicationRego": IsApplicationRego,
                "IsAddonRego": IsAddonRego,
                "QuestionList": QuestionList,
                "DocTypeList": DocList,
            },
            success: function () {
                $("#editregistration_" + regoTypeID).modal("hide");
                setTimeout(generateRegotype, 300);
                $("#editRegoSuccess").show();
                $("#editRegoSuccess").fadeOut(3000);
                $("#close_modaledit").trigger('click');
            },
            error: function () {
                alert("Edit not successful.");
            }
        });
    }
    else {
        return false;
    }

};

function deleteRegoType(regoTypeID) {
    $.ajax({
        cache: false,
        async: true,
        type: "POST",
        url: 'DeleteRegoType',
        data: {
            "regoTypeID": regoTypeID
        },
        success: function (json) {
            if (json.success) {
                generateRegotype();
                alert("Registration Type deleted successfully");
            }
            else {
                alert("Please remove all registrations before removing registration type.");
            }
            
        },
        error: function () {
            alert("Delete not successful.");
        }
    });
};

function generateConfDeets() {
    var conference = $("#selectedConference").val();
    console.log(conference)
    if (conference == "" || conference == null) {
        $("#conferenceCheck").show();
        return false;
    }
    else {
        $("#conferenceCheck").hide();
        $.ajax({
            cache: false,
            async: false,
            type: "POST",
            url: 'GenerateConference',
            data: {
                "ConferenceID": conference
            },
            success: function (json) {
                $("#conferenceViewResult").html(json);
            },
            error: function () {
                alert("Finding not successful.");
            }
        });
    }
};

function addConference() {
    $.ajax({
        cache: false,
        async: false,
        type: "POST",
        url: 'AddConference',
        data: {
        },
        success: function (json) {
            $("#conferenceViewResult").html(json);
        },
        error: function () {
            alert("Accessing adding page not successful.");
        }
    });
};

function saveConferenceChanges() {
    var confID = $("#ConferenceID").val();
    var confName = $("#conferenceName").val();
    var confShortName = $("#conferenceShortName").val();
    var confDate = $("#conferenceDate").val();
    var confType = $("#conferenceType").val();
    var confCountry = $("#conferenceCountry").val();
    var confShowOnWeb = document.getElementById("conferenceShowConfOnWeb").checked;
    var confBankAccount = $("#conferenceBankAccount").val();
    var conferenceStartDate = $("#conferenceStartDate").val();
    var conferenceEndDate = $("#conferenceEndDate").val();
    var conferenceStartTime = $("#conferenceStartTime").val();
    var conferenceEndTime = $("#conferenceEndTime").val();
    var venueMaxRegistrants = $("#venueMaxRegistrants").val();
    var conferenceVisibility = document.getElementById("conferenceVisibility").checked;
    var minAge = $("#MinAge").val();
    var maxAge = $("#MaxAge").val();
    var flag = true; //remain true if passed all validation
    //Data validation
    if (confName === '') {
        $("#conferenceNameCheck").show();
        flag = false;
    }
    else {
        $("#conferenceNameCheck").hide();
    }
    if (confDate === '') {
        $("#conferenceDateCheck").show();
        flag = false;
    }
    else {
        $("#conferenceDateCheck").hide();
    }
    if (!conferenceStartDate || !conferenceEndDate || !conferenceStartTime || !conferenceEndTime) {
        $("#venueDateCheck").show();
        flag = false;
    }
    else {
        $("#venueDateCheck").hide();
    }
    if (venueMaxRegistrants === '') {
        $("#venueMaxRegistrantsCheck").show();
        flag = false;
    }
    else {
        $("#venueMaxRegistrantsCheck").hide();
    }
    if (confType === '0') {
        $("#conferenceTypeCheck").show();
        flag = false;
    }
    else {
        $("#conferenceTypeCheck").hide();
    }
    if (confCountry === '0') {
        $("#conferenceCountryCheck").show();
        flag = false;
    }
    else {
        $("#conferenceCountryCheck").hide();
    }
    if (confBankAccount === '0') {
        $("#conferenceBankCheck").show();
        flag = false;
    }
    else {
        $("#conferenceBankCheck").hide();
    }
    if (conferenceStartDate >= conferenceEndDate) {
        $("#venueDateSequenceCheck").show();
        flag = false;
    }
    else {
        $("#venueDateSequenceCheck").hide();
    }
    if (minAge == null || minAge == '') {
        minAge = 16;
    }
    if (maxAge == null || maxAge == '') {
        maxAge = 60;
    }

    if (maxAge < minAge) {
        $("#addMinMaxAgeCheck").show();
        flag = false;
    }

    if (minAge < 16 || maxAge > 99) {
        $("#limitMinMaxAgeCheck").show();
        flag = false;
    }

    if (flag) {
        $.ajax({
            cache: false,
            async: false,
            type: "POST",
            url: 'SaveConference',
            data: {
                "confID": confID,
                "confName": confName,
                "confShortName": confShortName,
                "confDate": confDate,
                "confType": confType,
                "confCountry": confCountry,
                "confShowOnWeb": confShowOnWeb,
                "confBankAccount": confBankAccount,
                "conferenceStartDate": conferenceStartDate,
                "conferenceEndDate": conferenceEndDate,
                "conferenceEndTime": conferenceEndTime,
                "conferenceStartTime": conferenceStartTime,
                "venueMaxRegistrants": venueMaxRegistrants,
                "conferenceVisibility": conferenceVisibility,
                "minAge": minAge,
                "maxAge": maxAge,
            },
            success: function (json) {
                alert(json.success);
                if (json.success == "Conference added successfully.") {
                    window.location.href = "/Setup/Conference";
                }
                else {
                    generateConfDeets();
                }
            },
            error: function () {
                alert("Finding not successful.");
            }
        });
    }
    else {
        return false; 
    }
};

function deleteConference(confID) {
    $.ajax({
        cache: false,
        async: false,
        type: "POST",
        url: 'DeleteConference',
        data: {
            "confID": confID
        },
        success: function (json) {
            console.log(json);
            if (json.success == true) {
                alert("Conference deleted successfully.");
                window.location.href = "/Setup/Conference";
            }
            else {
                alert("Could not delete conference. There are some data linked to current conferences.");
            }
        },
        error: function () {
            alert("Finding not successful.");
        }
    });

};

//==============================================================Sort Order functionality====================================================
function clickSortRego() {
    $('#regoTypeDisplayTable tbody').sortable();
    $("#sortbutton").attr('onClick', 'sortRegistrationTypeOrder()');
    $("#sortbutton").html("Save Sort Order")
}
function sortRegistrationTypeOrder() {
    var table = document.getElementsByTagName("tbody")[0].rows;
    var list = [];

    for (i = 0; i < table.length; i++) {

        var RegoTypeID = $(table[i]).attr('id').replace(/^\D+/g, '')
        if (RegoTypeID != null) {

            var Obj = {
                ItemID: RegoTypeID,
                ItemSortOrderID: i + 1
            };
            list.push(Obj);
        }
    }
    $.ajax({
        cache: false,
        async: false,
        type: "POST",
        url: "/Setup/SortRegistrationType",
        data: { "RegistrationTypeList": list},
        success: function (json) {
            alert(json.resultmessage);
            $("#regoTypeDisplayTable tbody").sortable("destroy"); //call widget-function destroy
            $("#regoTypeDisplayTable tbody li").removeClass('ui-state-default');
            $("#regoTypeDisplayTable tbody li span").remove();
            $("#sortbutton").attr('onClick', 'clickSortRego()');
            $("#sortbutton").html("Sort Order")
        },
        error: function () {
            alert("There are some errors saving changes.");
        }
    });
}
