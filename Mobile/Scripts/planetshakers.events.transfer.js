﻿$(document).ready(function () {

});

//========== For transfer search contact section==========
var transferTypingTimer;                //timer identifier
var doneTypingIntervalTransfer = 300;

$('#TransferSearchContactForm :input').on('keyup', function () {

    clearTimeout(transferTypingTimer);
    if ($('#TransferSearchContactForm :input').val()) {
        transferTypingTimer = setTimeout(doneTransferTyping, doneTypingIntervalTransfer);
    }
    if ($('#TransferSearchContactForm :input').val() === "") {
        transferTypingTimer = setTimeout(doneTransferTyping, doneTypingIntervalTransfer);
    }

});
//=======end of transfer search contact section=============

/// Search transfer contact
function doneTransferTyping() {
    var firstname = $('#contactFirstName').val();
    var lastname = $('#contactLastName').val();
    var mobile = $('#contactMobile').val();
    var email = $('#contactEmail').val();

    $.ajax({
        cache: false,
        type: "POST",
        url: "/Administration/SearchContacts",
        data: { "FirstName": firstname, "LastName": lastname, "Mobile": mobile, "Email": email },
        success: function (json) {

            var content = "<table class='table card-body'><thead id='transferContactHeader'><tr>";
            content += "<th>ID</th><th>Name</th><th>Mobile</th><th>Email</th><th>Action</th>";
            content += "</tr></thead>";
            content += "<tbody id='transferContactList'>";

            for (var i = 0; i < json.length; i++) {
                content += "<tr >";
                content += "<td id='id_" + json[i].Contact_ID + "'>" + json[i].Contact_ID + "</td>";
                content += "<td id='name_" + json[i].Contact_ID + "'>" + json[i].FirstName + " " + json[i].LastName + "</td>";
                content += "<td id='mobile_" + json[i].Contact_ID + "'>" + json[i].Mobile + "</td>";
                content += "<td id='email_" + json[i].Contact_ID + "'>" + json[i].Email + "</td>";
                content += "<td>" + "<button class='btn btn-primary btn-sm' onclick='selectTransferContact(" + json[i].Contact_ID + ")'>Select</button>" + "</td>";
                content += "</tr >";
            }

            content += "</tbody></table>";
            $('#transferContactTable').html(content);
        },
        error: function () {
        }
    });
}

function selectTransferContact(id) {

    var selectedName = $("#name_" + id).html();
    var selectedMobile = $("#mobile_" + id).html();
    var selectedEmail = $("#email_" + id).html();

    $("#transferToID").attr("value", id);
    $("#selectedFullName").html(selectedName);
    $("#selectedMobile").html(selectedMobile);
    $("#selectedEmail").html(selectedEmail);
    $("#selectedContactResult").show();
}

function CheckTransferSingle() {
    var transferToID = $("#transferToID").val();

    if (transferToID == null || transferToID == "" || transferToID == 0) {
        $("#executedUnsuccessfully").show();
        animateTo("executedUnsuccessfully");
        $("#executedUnsuccessfully").fadeOut(5000);
    }
    else {
        $("#TransferSingleForm").submit();
    }
}

function showRefundConfirm() {
    $("#ConfirmRefundModal").modal("show");
}

function CheckTransferGroup() {
    $("#ConfirmRefundModal").modal("hide");
    var transferToID = $("#transferToID").val();
    var transferFromID = $("#transferFromID").val();
    var conferenceID = $("#conferenceID").val();
    var validGroupRegoTransferQty = true;
    var totalTransferQty = 0;
    var comment = $("#Comment").val();
    var transferList = [];

    var regoList = $("[id^='rego_']").toArray();

    regoList.forEach(function (rego){
        var regoTypeID = parseInt($(rego).attr("id").substring(5));
        var regoTransferQty = parseInt($(rego).val());
        
        var regoUnallocatedQty = parseInt($("#unallocated_" + regoTypeID).html());

        totalTransferQty = totalTransferQty + regoTransferQty;
        if (regoTransferQty > regoUnallocatedQty) {
            validGroupRegoTransferQty = false;
        }
        var transferObj = {
            RegistrationType_ID: regoTypeID,
            TransferQty: regoTransferQty,
            UnallocatedQty: regoUnallocatedQty
        };
        transferList.push(transferObj);
    });

    if (transferToID == null || transferToID == "" || transferToID == 0) {
        $("#executedUnsuccessfully").show();
        animateTo("executedUnsuccessfully");
        $("#executedUnsuccessfully").fadeOut(5000);
    }
    else if (totalTransferQty == 0) {
        $("#Message").html("Please fill in transfer option.");
        $("#executedUnsuccessfully").show();
        animateTo("executedUnsuccessfully");
        $("#executedUnsuccessfully").fadeOut(5000);
    }
    else if (!validGroupRegoTransferQty) {
        $("#Message").html("Transfer quantity is more than available quantity.");
        $("#executedUnsuccessfully").show();
        animateTo("executedUnsuccessfully");
        $("#executedUnsuccessfully").fadeOut(5000);
    }
    else
    {
        $.ajax({
            cache: false,
            type: "POST",
            async: false,
            url: "/Administration/TransferGroupRegistration",
            data: { TransferGroupRegoOption: transferList, TransferFromID: transferFromID, TransferToID: transferToID, Conference_ID: conferenceID, Comment: comment },
            success: function (json) {
                window.location.href = json;
            },
            error: function () {
                alert("There are some error to change User Role Permission.");
                return false;
            }
        });
    }
    
}