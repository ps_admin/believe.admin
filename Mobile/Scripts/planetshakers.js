﻿$(document).ready(function () {

    $('#txtRegSearch').change(function () {
        var query = $("#txtRegSearch").val();

        $("#tblRegSearch").empty();
        $("#loading").show();

        $.ajax({
            cache: false,
            type: "GET",
            url: 'GetSearchRegistrant',
            data: { "query": query },
            success: function (json) {
                $("#loading").hide();

                json = json || {};
                for (var i = 0; i < json.length; i++) {
                    $("#tblRegSearch").append('<tr>'
                        + '<td>' + json[i].FirstName + '</td>'
                        + '<td>' + json[i].LastName + '</td>'
                        + '<td>' + json[i].Mobile + '</td>'
                        + '<td>' + json[i].Email + '</td>'
                        + '<td><a href="#" class="btn btn-primary" onclick="">Add</a></td>'
                        + '</tr >');
                }
            },
            error: function () {
                alert("No results returned");
            }
        });
    });

    $('.open-allocation-btn').on('click', function (e) {
        e.preventDefault();

        // Hide tabs
        $('#allocate_previous').hide();
        $('#search_registrant').hide();
        $('#new_registrant').hide();
        $('#manual_allocation').hide();
    });

    $('#toggle_allocate_previous').on('click', function (e) {
        e.preventDefault();

        $('#allocate_previous').show('blind');
        $('#search_registrant').hide();
        $('#new_registrant').hide();
        $('#manual_allocation').hide();

    });

    $('#toggle_search_registrant').on('click', function (e) {
        e.preventDefault();

        $('#allocate_previous').hide();
        $('#search_registrant').show('blind');
        $('#new_registrant').hide();
        $('#manual_allocation').hide();

    });

    $('#toggle_new_registrant').on('click', function (e) {
        e.preventDefault();

        $('#allocate_previous').hide();
        $('#search_registrant').hide();
        $('#new_registrant').show('blind');
        $('#manual_allocation').hide();

    });

    $('#toggle_manual_allocation').on('click', function (e) {
        e.preventDefault();

        $('#allocate_previous').hide();
        $('#search_registrant').hide();
        $('#manual_allocation').show('blind');
        $('#new_registrant').hide();

    });

    if ($("#bankingTable").length > 0) {
        $("table").DataTable({
            fixedHeader: {
                header: false,
                footer: false
            },
            "bInfo": false,
            paging: false,
            responsive: true,
            "order": [],
            language: {
                search: "",
                searchPlaceholder: "Search..."
            }
        });
    }
    if ($("#checkInTable").length > 0) {
        $("table").DataTable({
            fixedHeader: {
                header: false,
                footer: false
            },
            "bInfo": false,
            paging: false,
            "order": [],
            language: {
                search: "",
                searchPlaceholder: "Search..."
            },
            scrollX: true
        });
    }


    if ($("#newRegistrantForm").length > 0) {
        clearForm($("#newRegistrantForm"));
    }

    if ($("#newGroupForm").length > 0) {
        clearForm($("#newGroupForm"));
    }

    if ($("#generatedChildEnrolmentFormTable").length > 0) {
        $("#generatedChildEnrolmentFormTable").DataTable({
            fixedHeader: {
                header: false,
                footer: false
            },
            "bInfo": false,
            paging: false,
            "order": [],
            language: {
                search: "",
                searchPlaceholder: "Search..."
            },
            scrollY: 600,
            scrollX: true
        });
    }

    //if ($("#singleRegoTable").length > 0) {
    //    $("table").DataTable({
    //        fixedHeader: {
    //            header: true,
    //            footer: true
    //        },
    //        dom: 'Blfrtip',
    //        buttons: [
    //            { extend: 'excel', className: 'btn btn-primary' },
    //            { extend: 'csv', className: 'btn btn-secondary' }
    //        ],
    //        paging: true,
    //        "pageLength": 5,
    //        "lengthMenu": [5, 10, 25, 50],
    //        processing: true,
    //        searching: true,
    //        info: false,
    //        scrollCollapse: true,
    //        autoWidth: true,
    //        "order": [],
    //        responsive: true,
    //        language: {
    //            search: "",
    //            searchPlaceholder: "Search..."
    //        },
    //        scrollY: 600,
    //        scrollX: true
    //    });
    //    $('#singleRegoTable_wrapper .col-md-7:eq(0)').addClass("d-flex justify-content-center justify-content-md-end");
    //    $('#singleRegoTable_paginate').addClass("mt-3 mt-md-2");
    //}

    //if ($("#groupRegoTable").length > 0) {
    //    $("table").DataTable({
    //        fixedHeader: {
    //            header: true,
    //            footer: true
    //        },
    //        dom: 'Blfrtip',
    //        buttons: [
    //            { extend: 'excel', className: 'btn btn-primary' },
    //            { extend: 'csv', className: 'btn btn-secondary' }
    //        ],
    //        paging: true,
    //        "pageLength": 5,
    //        "lengthMenu": [5, 10, 25, 50],
    //        processing: true,
    //        searching: true,
    //        info: false,
    //        scrollCollapse: true,
    //        autoWidth: true,
    //        "order": [],
    //        responsive: true,
    //        language: {
    //            search: "",
    //            searchPlaceholder: "Search..."
    //        },
    //        scrollY: 600,
    //        scrollX: true
    //    });
    //    $('#groupRegoTable_wrapper .col-md-7:eq(0)').addClass("d-flex justify-content-center justify-content-md-end");
    //    $('#groupRegoTable_paginate').addClass("mt-3 mt-md-2");
    //}

    //if ($("#groupSummaryTable").length > 0) {
    //    $("table").DataTable({
    //        fixedHeader: {
    //            header: true,
    //            footer: true
    //        },
    //        dom: 'Blfrtip',
    //        buttons: [
    //            { extend: 'excel', className: 'btn btn-primary' },
    //            { extend: 'csv', className: 'btn btn-secondary' }
    //        ],
    //        paging: true,
    //        "pageLength": 5,
    //        "lengthMenu": [5, 10, 25, 50],
    //        processing: true,
    //        searching: true,
    //        info: false,
    //        scrollCollapse: true,
    //        autoWidth: true,
    //        "order": [],
    //        language: {
    //            search: "",
    //            searchPlaceholder: "Search..."
    //        },
    //        scrollY: 600,
    //        scrollX: true
    //    });
    //    $('#groupSummaryTable_wrapper .col-md-7:eq(0)').addClass("d-flex justify-content-center justify-content-md-end");
    //    $('#groupSummaryTable_paginate').addClass("mt-3 mt-md-2");
    //}

    if ($("#regoTypeAllocationTable").length > 0) {
        $("table").DataTable({
            fixedHeader: {
                header: false,
                footer: false
            },
            "bInfo": false,
            paging: false,
            responsive: true,
            "order": [],
            language: {
                search: "",
                searchPlaceholder: "Search..."
            }
        });
    }

    if ($("#contactMailingListTable").length > 0) {
        $("table").DataTable({
            fixedHeader: {
                header: true,
                footer: true
            },
            "order": [],
            "bLengthChange": false,
            "bFilter": true,
            "bInfo": true,
            "bAutoWidth": false,
            scrollCollapse: true,
            responsive: true,
            paging: true,
            language: {
                search: "",
                searchPlaceholder: "Search..."
            },
            scrollY: 600,
            scrollX: true,
            dom: 'Bfrtip',
            buttons: [
                { extend: 'excel', className: 'btn btn-primary' },
                { extend: 'csv', className: 'btn btn-secondary' }
            ]
        });
    }

    if ($('#purchase_table_id').length > 0) {
        $('#purchase_table_id').DataTable({
            fixedHeader: {
                header: false,
                footer: false
            },
            "bInfo": false,
            paging: false,
            "order": [],
            language: {
                search: "",
                searchPlaceholder: "Search..."
            },
            scrollY: 400,
            scrollX: true
        });
    }

    if ($('#purchase_table_id2').length > 0) {
        $('#purchase_table_id2').DataTable({
            fixedHeader: {
                header: false,
                footer: false
            },
            "bInfo": false,
            paging: false,
            "order": [],
            language: {
                search: "",
                searchPlaceholder: "Search..."
            },
            scrollY: 400,
            scrollX: true
        });
    }
    if ($('#purchase_table_id3').length > 0) {
        $('#purchase_table_id3').DataTable({
            fixedHeader: {
                header: false,
                footer: false
            },
            "bInfo": false,
            paging: false,
            "order": [],
            language: {
                search: "",
                searchPlaceholder: "Search..."
            },
            scrollX: true
        });
    }

    if (('#purchase_table_id4').length > 0) {
        $('#purchase_table_id4').DataTable({
            fixedHeader: {
                header: false,
                footer: false
            },
            "bInfo": false,
            paging: false,
            "order": [],
            language: {
                search: "",
                searchPlaceholder: "Search..."
            },
            scrollX: true
        });
    }

    if ($("#selectedDateOpt").length > 0) {
        if ($("#selectedDateOpt").val() == 8) {
            $("#customDate").show();
        }
    }

});

function addRegistrant(id) {

    $.ajax({
        cache: false,
        async: false,
        type: "POST",
        url: 'AddRegistrant',
        data: {
            "Contact_ID": id
        },
        success: function (json) {
            alert("Allocation successful.");
        },
        error: function () {
            alert("Allocation not successful.");
        }
    });
}

//Giving backdrop for overlapping modal
$(document).on('show.bs.modal', '.modal', function () {
    var zIndex = 1040 + (10 * $('.modal:visible').length);
    $(this).css('z-index', zIndex);
    setTimeout(function () {
        $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
    }, 0);
});

function viewGroupPayment(id) {

    $.ajax({
        cache: false,
        async: false,
        type: "POST",
        url: 'GetGroupPayment',
        data: {
            "Payment_ID": id
        },
        success: function (json) {            
            $("#viewPayment").html(json);
            $("#viewpaymentmodal").modal('show');
        },
        error: function () {
            alert("Action not successful");
        }
    });
}

function refundConfirm(id) {
    var str = "Are you sure to proceed to refund? \n \n"
    console.log('refund')
    $.ajax({
        cache: false,
        async: false,
        type: "POST",
        url: 'GetPaymentType',
        data: {
            "Payment_ID": id
        },
        success: function (json) {
            json = json || {};
            str += "NOTE: This person has a " + json.paymenttype + " payment type. Please ensure all records are accurate."
            if (confirm(str)) {

                var comment = $("#payment_Comment").val()
                var transRef = $("#payment_TransactionRef").val()
                $.ajax({
                    cache: false,
                    type: "POST",
                    url: "/Registration/RefundGroupPayment",
                    data: { "Payment_ID": id, "TransactionRef": transRef, "Comment": comment },
                    success: function (json) {
                        $("#successMessage").show();
                        window.location.reload();
                    },
                    error: function () {
                        alert("You are not allowed here.");
                    }
                });
            }
        }
    });
}

//single = true, contact payment, single = false, group payment
function UpdatePayment(id, single) {

    var transRef = $("#transactionRef").val();
    var comment = $("#Comment").val();
    var urllink = "UpdateGroupPayment";

    $.ajax({
        cache: false,
        type: "POST",
        url: urllink,
        data: { "Payment_ID": id, "TransactionRef": transRef, "Comment": comment },
        success: function (json) {
            $("#successMessage").show();
            $("#successMessage").delay(1500).fadeOut(400);
        },
        error: function () {
            alert("You are not allowed here.");
        }
    });
}

function SendReceipt(id) {
    $.ajax({
        cache: false,
        type: "POST",
        url: 'SendReceipt',
        data: { "Payment_ID": id },
        success: function (json) {
            $("#successMessage").html("<div class='col-lg-12'><div class='alert alert-success'>Receipt has been sent successfully.</div></div>");
            $("#successMessage").show();
            $("#successMessage").delay(1500).fadeOut(400);
        },
        error: function () {
            $("#successMessage").html("<div class='col-lg-12'><div class='alert alert-danger'>Receipt has not been sent due to an error occured.</div></div>");
            $("#successMessage").show();
            $("#successMessage").delay(1500).fadeOut(400);
        }
    });
}


function removeBulkRego(id, group_id, conf_id) {
    $.ajax({
        cache: false,
        type: "POST",
        url: 'confirmAllocation',
        data: { "BulkRegistration_ID": id },
        success: function (json) {
            var len = Object.entries(json).length;

            if (len !== 0) {
                var string = "<table style='width:100%' class='table table-bordered'>";
                var regtype_id = 0;

                string += "<tr><th>Registration ID</th><th>Name</th><th></th></tr>";

                for (var i = 0; i < len; i++) {
                    string += "<tr>";

                    string += "<td>";
                    string += json[i].ID;
                    string += "</td>";

                    string += "<td>";
                    string += json[i].FirstName + " " + json[i].LastName;
                    string += "</td>";

                    string += "<td>";
                    string += "<input class='remove_check' type='checkbox' value='" + json[i].ID + "' />";
                    string += "</td>"

                    string += "</tr>";

                    if (i == len - 1) {
                        regtype_id = json[i].RegoType_ID;
                    }
                }

                string += "</table>";

                string += "<input class='btn btn-primary' type='submit' onclick='remove(" + id + "," + group_id + "," + conf_id + "," + regtype_id + ")' value='Remove'/>";
                $('#remove_allocation #contentModal').append(string);

                $('#remove_allocation').modal('show');
            } else {
                remove(id, group_id, conf_id, null);
            }
        },
        error: function () {
            alert("You are not allowed here.");
        }
    });
}

function remove(id, group_id, conf_id, regtype_id) {
    var remove_reg = [];

    $(".remove_check:checkbox:checked").each(function () {
        remove_reg.push(this.value);
    });


    if (typeof remove_reg !== 'undefined' && remove_reg.length > 0) {
        $.ajax({
            cache: false,
            type: "POST",
            url: 'RemoveComplimentaryRego',
            data: { "Registration_IDs": remove_reg, "Group_ID": group_id, "RegType_ID": regtype_id },
            error: function () {
                alert("Error trying to remove an allocation");
            }
        });
    }

    $.ajax({
        cache: false,
        type: "POST",
        url: 'RemoveBulkRego',
        data: { "BulkRegistration_ID": id, "Conference_ID": conf_id },
        success: function (json) {
            $("#removeBulkRegoSuccess").show();
            location.reload(true);
        },
        error: function () {
            alert("No results returned");
        }
    });
}

// Need to test confirmation email
function sendEmail(rid) {

    $.ajax({
        cache: false,
        async: true,
        type: "POST",
        url: "SendConfirmationEmail",
        data: { "Registration_ID": rid },
        success: function (json) {
            alert("Email sent!");
        },
        error: function () {
            alert("An error occurred while trying to send an email.");
        }
    });
}

function sendTaxInvoice(groupLeaderID, ConferenceID) {

    $.ajax({
        cache: false,
        async: true,
        type: "POST",
        url: "SendTaxInvoice",
        data: { "GroupLeader_ID": groupLeaderID, "Conference_ID": ConferenceID },
        success: function (json) {
            $("#sendTaxInvoiceCheck").show();
        },
        error: function () {
            alert("An error occurred while trying to send an email.");
        }
    });
}


function changeDetails() {
    var id = $('#txtContact_ID').val();
    var firstname = $('#txt_firstname').val();
    var lastname = $('#txt_lastname').val();
    var email = $('#txt_email').val();
    var mobile = $('#txt_mobile').val();
    var pw = $('#txt_password').val();

    if (pw.length < 8 || pw.length > 20) {
        alert("Password must be between 8 to 20 characters long.");
        return;
    }

    $.ajax({
        cache: false,
        async: true,
        type: "POST",
        url: "changeDetails",
        data: { "cid": id, "newEmail": email, "newMobile": mobile, "newFirstName": firstname, "newLastName": lastname, "newPassword": pw },
        success: function (json) {
            alert(json);
        },
        error: function () {
            alert("Error changing details");
        }
    });
}

function changeGroupDetails() {
    var id = $('#txtGroup_ID').val();
    var groupname = $('#txt_groupname').val();
    var email = $('#txt_groupemail').val();
    var mobile = $('#txt_groupmobile').val();
    var pw = $('#txt_password').val();

    if (pw.length < 8 || pw.length > 20) {
        alert("Password must be between 8 to 20 characters long.");
        return;
    }

    $.ajax({
        cache: false,
        async: true,
        type: "POST",
        url: "changeGroupDetails",
        data: { "gid": id, "newEmail": email, "newMobile": mobile, "newGroupName": groupname, "newPassword": pw },
        success: function (json) {
            alert(json);
        },
        error: function () {
            alert("Error changing details");
        }
    });
}

function updatePurchase(rid) {
    var id = "#bulkCount_" + rid;
    var qty = $(id).val();

    $.ajax({
        cache: false,
        async: true,
        type: "POST",
        url: "updatePurchase",
        data: { "reg_id": rid, "newQty": qty },
        success: function (json) {
            alert(json.message);
            window.location.href = json.link;
        },
        error: function () {
            alert("An error occurred while trying to update record.");
        }
    });

}


// Select All for check in
function selectAll(source) {
    var count = 0;

    checkboxes = document.getElementsByName('attended');

    for (var i = 0, n = checkboxes.length; i < n; i++) {
        checkboxes[i].checked = source.checked;
        count++;
    }

    document.getElementById('count').innerHTML = "(" + count + ")";

    var cid = $("#Contact_ID").val();
    var vid = $("#Venue_ID").val();

    if (source.checked) {

        $.ajax({
            cache: false,
            async: false,
            type: "POST",
            url: "GetRegoRegistrationTypes",
            data: { "contact_id": cid, "venue_id": vid, "count": count },
            success: function (json) {

                if (json != null) {
                    $("#selected_collect").empty();
                    $.each(json, function (key, val) {
                        $("#selected_collect").append("<tr><td><strong>" + key + "</strong></td><td><strong>" + val + "</strong></td></tr>");
                    });
                } else {
                    alert("null");
                }
            },
            error: function () {
                alert("error");
            }

        });

        $('#count').show();

    } else {
        $('#count').hide();
        $('#selected_collect').empty();
    }

}

function goBack() {
    window.history.back();
}

/*
function allocateprevious() {
    $.ajax({
        cache: false,
        type: "POST",
        url: "/Registration/AllocatePrevious",
        data: { },
        success: function (json) {
            alert("Allocated successfully.");
        },
        error: function () {
            alert("Something went wrong during allocation process.");
        }
    });
}*/
function manualallocate() {
    var firstname = $("#manualAllocateFirstName").val();
    var lastname = $("#manualAllocateLastName").val();
    var email = $("#manualAllocateEmail").val();
    var mobile = $("#manualAllocateMobile").val();

    $.ajax({
        cache: false,
        type: "POST",
        url: "/Registration/ManualAllocationSearch",
        data: { "firstname": firstname, "lastname": lastname, "email": email, "mobile": mobile },
        success: function (json) {
            var content = "<table class='table table-bordered' id='searchResultTable'><tr id='tabletitle'><th>ContactID</th><th>FirstName</th><th>LastName</th><th>Mobile</th>\
                    <th>Email</th>\
                    <th>External</th>\
                    <th>Internal</th>\
                    <th id='actionheader'>Action</th>";
            content += "</tr>";

            for (var i = 0; i < json.length; i++) {
                content += "<tr id='table_" + json[i].Id + "'>";
                content += "<td id='id_" + json[i].Id + "'>" + json[i].Id + "</td>";
                content += "<td id='firstname_" + json[i].FirstName + "'>" + json[i].FirstName + "</td>";
                content += "<td id='lastname_" + json[i].LastName + "'>" + json[i].LastName + "</td>";
                content += "<td id='mobile_" + json[i].ContactNumber + "'>" + json[i].ContactNumber + "</td>";
                content += "<td id='email_" + json[i].Email + "'>" + json[i].Email + "</td>";
                if (json[i].External == false) {
                    content += "<td>" + "<button id='external_" + json[i].Id + "' class='btn btn-danger btn-block' type='button' onclick='addExternalContact(" + json[i].Id + ")'>Add</button>" + "</td>";;
                }
                if (json[i].External == true) {
                    content += "<td id='external_" + json[i].Id + "'><i class='fa fa-check'></i></td>";
                }

                if (json[i].Internal) {
                    content += "<td id='internal_" + json[i].Id + "'><i class='fa fa-check'></i></td>";
                } else {
                    content += "<td id='internal_" + json[i].Id + "'></td>"
                }

                if (json[i].External == false) {
                    content += "<td>" + "<button id='allocatebtn_" + json[i].Id + "'class='btn btn-primary btn-block' type='button' disabled='' onclick='allocateCurrent(" + json[i].Id + ")'>Select</button>" + "</td>";
                }
                else {
                    content += "<td id='allocatebtn_cell_" + json[i].Id + "'>" + "<button id='allocatebtn_" + json[i].Id + "'class='btn btn-primary btn-block' type='button' onclick='allocateCurrent(" + json[i].Id + ")'>Select</button>" + "</td>";
                };
                content += "</tr>";
            }
            $("#partialviewresult").html(content);
        },
        error: function () {
        }
    });
}
function allocateCurrent(id) {
    var content = "<input value='" + id + "' data-val='true' id='NewRegistration_Contact_ID' name='NewRegistration.ContactID' type='hidden'>";
    $("#allocate_select_rego_type").append(content);
    clearForm($('#manualsearchform'));
    $("#searchResultTable tr").hide();
    $("#searchResultTable").show();
    $("#tabletitle").show();
    $("#actionheader").hide();
    $("#table_" + id).show();
    $("#allocatebtn_cell_" + id).hide();
    $("#regotype").show();
}

function addExternalContact(id) {

    $.ajax({
        cache: false,
        type: "POST",
        url: "/Registration/AddExternalContact",
        data: { "Id": id },
        success: function (json) {
            alert("External Contact has been added successfully.");
            $("#allocatebtn_" + id).removeAttr('disabled');
            $("#external_" + id).replaceWith("<i class='fa fa-check'></i>");

        },
        error: function () {
            alert("Error adding external contact.");
        }
    });

}

$("#selectedDateOpt").change(function () {
    var selectedId = $("#selectedDateOpt").val();
    if (selectedId == 8) {
        $("#customDate").show();
    }
    else {
        $("#customDate").hide();
    }
});


$("#reportTypeSelected").change(function () {
    var selectedOption = $("#reportTypeSelected").val();
    if (selectedOption == "Registration Types" || selectedOption == "Payment Types") {
        $("#dateSelectionOption").show();
    }
    else {
        $("#dateSelectionOption").hide();
    }
});
$('#summaryForm').ready(function () {
    var selectedOption = $("#reportTypeSelected").val();
    if (selectedOption == "Registration Types" || selectedOption == "Payment Types") {
        $("#dateSelectionOption").show();
    }
    else {
        $("#dateSelectionOption").hide();
    }

});

var typingTimerPurchase;                //timer identifier
var doneTypingIntervalPurchase = 300;

/// Search guest
function doneTypingPurchase() {
    $("#actionCol").show();
    var firstname = $('#PurchaseFirstName').val();
    var lastname = $('#PurchaseLastName').val();

    $.ajax({
        cache: false,
        type: "POST",
        url: "/Report/searchContact",
        data: { "FirstName": firstname, "LastName": lastname },
        success: function (json) {

            var content = "";

            for (var i = 0; i < json.length; i++) {
                content += "<tr id='table_" + json[i].Contact_ID + "'>";
                content += "<td id='id_" + json[i].Contact_ID + "'>" + json[i].Contact_ID + "</td>";
                content += "<td id='name_" + json[i].Contact_ID + "'>" + json[i].FirstName + " " + json[i].LastName + "</td>";
                content += "<td" + " id = 'action_" + json[i].Contact_ID + "'>" + "<button class='btn btn-primary'" + "onclick='selectPurchaseContact(" + json[i].Contact_ID + ")'>Select</button>" + "</td>";
                content += "</tr>";
            }

            content += "</tbody>";
            $('#purchaseContactList').html(content);
        },
        error: function () {
        }
    });
}
$('#purchase_form :input').on('keyup', function () {

    clearTimeout(typingTimerPurchase);
    if ($('#purchase_form :input').val()) {
        typingTimerPurchase = setTimeout(doneTypingPurchase, doneTypingIntervalPurchase);
    }
    if ($('#purchase_form :input').val() == "") {
        typingTimerPurchase = setTimeout(doneTypingPurchase, doneTypingIntervalPurchase);
    }

});

function selectPurchaseContact(id) {
    var content = "<input value='" + id + "' data-val='true' id='PurchaseContact_Contact_ID' name='Contact_ID' type='hidden'>";
    $("#setPurchaseContactId").html(content);
    clearForm($('#purchase_form'));
    $("#purchaseContactTable tr").hide();
    $("#actionCol").hide();
    $("#action_" + id).hide();
    $("#purchaseContactHeader").show();
    $("#table_" + id).show();
}

$("#singleGroupSelection").change(function () {
    var selection = $("#singleGroupSelection").val();
    var table = document.getElementById("PaymentTable").rows;
    if (selection !== "") {

        for (i = 0; i < table.length; i++) {
            if ($(table[i]).attr('data-val') != selection) {
                $(table[i]).hide();
            }
            else {
                $(table[i]).show();
            }
        }
    }
    if (selection === "") {
        for (i = 0; i < table.length; i++) {
            $(table[i]).show();
        }
    }
});

function clearForm(form) {
    // iterate over all of the inputs for the form
    // element that was passed in
    $(':input', form).forEach(function () {
        var type = this.type;
        var tag = this.tagName.toLowerCase(); // normalize case
        // it's ok to reset the value attr of text inputs,
        // password inputs, and textareas
        if (type === 'text' || type === 'password' || tag === 'textarea')
            this.value = "";
        // checkboxes and radios need to have their checked state cleared
        // but should *not* have their 'value' changed
        else if (type === 'checkbox' || type === 'radio')
            this.checked = false;
        // select elements need to have their 'selectedIndex' property set to -1
        // (this works for both single and multiple select elements)
        else if (tag === 'select')
            this.selectedIndex = -1;
        else if (type === 'date')
            this.value = "";
    });
}

function checkInReportFilter() {
    var count = 0;
    var CheckedInID = $("#CheckedIn").val();
    var regoType = $("#RegistrationTypeFilter").val();
    var table = document.getElementsByTagName("tbody")[0].rows;
    if (regoType !== "" && CheckedInID !== "") {
        for (i = 0; i < table.length; i++) {
            if ($(table[i]).attr('data-val').includes(regoType) && $(table[i]).attr('data-val').includes(CheckedInID)) {
                $(table[i]).show();
                count++
            }
            else {
                $(table[i]).hide();
            }
        }
    } else if (regoType !== "" && CheckedInID === "") {
        for (i = 0; i < table.length; i++) {
            if (!$(table[i]).attr('data-val').includes(regoType)) {
                $(table[i]).hide();
            }
            else {
                count += 1;
                $(table[i]).show();
            }
        }
    } else if (regoType === "" && CheckedInID !== "") {
        for (i = 0; i < table.length; i++) {
            if (!$(table[i]).attr('data-val').includes(CheckedInID)) {
                $(table[i]).hide();
            }
            else {
                count += 1;
                $(table[i]).show();
            }
        }
    } else {
        for (i = 0; i < table.length; i++) {
            count = table.length;
            $(table[i]).show();
        }
    }

    $("#totalCheckInCount").html(count);

}


//Quick add registration, disabling submit button if no payment is selected
$("#selectedPaymentType").change(function () {
    var paymentoption = $("#selectedPaymentType").val();
    if (paymentoption > 0) {
        document.getElementById("submitRegistration").disabled = false;
    }
    else {
        document.getElementById("submitRegistration").disabled = true;
    }
});

//Quick add allocation, changing the display of details according to dropdown selection of Conference
$("#Venue_ID").change(function () {
    var venue_ID = $("#Venue_ID").val();
    var contactID = $("#Contact_ID").val();
    console.log("listening to change")
    $.ajax({
        cache: false,
        type: "GET",
        url: "/Administration/GetUnallocatedList",
        data: { "Venue_ID": venue_ID, "Contact_ID": contactID },
        success: function (json) {
            html1 = "";
            html2 = "";
            html3 = "<select class='form-control' data-val='true' data-val-number='The field RegistrationType_ID must be a number.'" +
                "data-val-required='The RegistrationType_ID field is required.'" +
                "id='RegistrationType_ID' name ='RegistrationType_ID'><option value=''>Select Registration Type</option>";

            for (i = 0; i < json.unallocatedList.length; i++) {
                var unallocated = json.unallocatedList[i];
                html1 += "<tr><td>" + unallocated.RegistrationType + "</td><td>" + unallocated.Allocated + "</td><td>" + unallocated.Unallocated + "</td></tr>";
            }

            for (j = 0; j < json.allocatedList.length; j++) {
                var allocated = json.allocatedList[j];
                html2 += "<tr><td>" + allocated.ID + "</td><td>" + allocated.FirstName + " " + allocated.LastName + "</td><td>" +
                    allocated.Mobile + "</td><td>" + allocated.Email + "</td></tr>";
            }

            for (k = 0; k < json.availableRegistrationList.length; k++) {
                var availableType = json.availableRegistrationList[k];
                html3 += "<option value='" + availableType.RegistrationType_ID + "'>" + availableType.RegistrationType + " </option>";
            }

            html3 += "</select>";

            //This section to change registration datails table
            $("#unallocatedRegistrationType").html("");
            $("#unallocatedRegistrationType").append(html1);

            //This section to change allocated details table
            $("#allocatedRegistration").html("");
            $("#allocatedRegistration").append(html2);

            //This section to change dropdown ajax
            $("#registrationTypeSelect").html("");
            $("#registrationTypeSelect").append(html3);
        },
        error: function () {
        }
    });
});


function closeModal() {
    $('a.close-reveal-modal').trigger('click');
}

function submitForm(allocatedToId) {
    document.getElementById("form_" + allocatedToId).submit();
}

function checkRegistered(allocatedToId, venue_ID) {

    $.ajax({
        cache: false,
        type: "GET",
        url: "/Administration/Registered",
        data: { "Allocation_ID": allocatedToId, "Venue_ID": venue_ID },
        success: function (json) {
            console.log(json);
            if (json) {
                $('#addExistedAllocation_' + allocatedToId).modal('show');
            }
            else {
                document.getElementById("form_" + allocatedToId).submit();
            }
        },
        error: function () {
            alert("There are some error to allocate to person.");
        }

    });

}

$("#selfAllocateChose").change(function () {
    checkboxticked = document.getElementById("selfAllocateChose").checked == true;

    if (checkboxticked) {
        $("#selfAllcoteSection").show();
        $("#continueRegistrationSuccess").hide();
    }
    else {
        $("#selfAllcoteSection").hide();
        $("#continueRegistrationSuccess").show();
    }
});

$("#selfAllocateRegoType").change(function () {

    if ($("#selfAllocateRegoType").val() != "") {
        $("#selfAllocatedAttended").show();
    }
    else {
        $("#selfAllocatedAttended").hide();

    }
});
//=====================RESET password============================================================
$('#confirmedpassword').keyup(function () {
    var newpassword = $('#newpassword').val();
    var confirmedpassword = $('#confirmedpassword').val();

    if (newpassword !== confirmedpassword) {
        $('#passwordmatchcheck').show();
        $('#password-submit').prop('disabled', true);
    }
    if (newpassword === confirmedpassword) {
        $('#passwordmatchcheck').hide();
    }
    if (newpassword.length >= 8 && newpassword == confirmedpassword) {
        $('#passwordmatchcheck').hide();
        $('#password-submit').prop('disabled', false);
    }
});

$('#newpassword').keyup(function () {
    var newpassword = $('#newpassword').val();
    var confirmedpassword = $('#confirmedpassword').val();

    if ($("#newpassword").val().length < 8) {
        $("#passwordlencheck").show();
        $('#password-submit').prop('disabled', true);
    }
    else {
        $("#passwordlencheck").hide();
    }
    if (newpassword !== confirmedpassword) {
        $('#passwordmatchcheck').show();
        $('#password-submit').prop('disabled', true);
    }
    if (newpassword === confirmedpassword) {
        $('#passwordmatchcheck').hide();
    }
    if (newpassword.length >= 8 && newpassword == confirmedpassword) {
        $('#passwordmatchcheck').hide();
        $('#password-submit').prop('disabled', false);
    }

});

function animateTo(id) {
    $("html, body").animate({
        scrollTop: $("#" + id).offset().top - 20
    }, 1000);
}

//Animate function
function animateModal(id) {
    $("#"+ id).animate({
        scrollTop: 0
    }, 1000);
}

//Filter internal/external/both contact with inputting in datatable search based on column
function filterContact() {

    var selection = $("#filterContact").val();
    if (selection == "internal" || selection == "external") {
        selection += "|both"; //pulling internal/external only and if they are both internal/external
    }

    $('table').DataTable().columns(3).search(selection, true).draw();
}

//stripe payment on registration allocation
function loadStripePayment() {

    var data = new Object();

    data = JSON.stringify(data);

    $.ajax({
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        url: "/Payment/loadStripePayment",
        data: data,
        success: function (result) {
            console.log('stripe success');
            $('#stripe-payment').html(result);
        },
        error: function () {
            console.log("failed");
        }
    });
}


