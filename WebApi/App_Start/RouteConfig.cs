﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace Planetshakers.WebApi
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "v1/{controller}/"
            );
        }
    }
}