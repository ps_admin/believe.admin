﻿using Newtonsoft.Json;
using Planetshakers.WebApi.Configurations.WebApi.MessageHandlers;
using System;
using System.Net.Http.Formatting;
using System.Runtime.Serialization.Formatters;
using System.Web.Http;

namespace Planetshakers.WebApi.App_Start
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            ConfigureFormatter(config.Formatters);

            config.MessageHandlers.Add(new CompressionHandler());            
        }

        private static void ConfigureFormatter(MediaTypeFormatterCollection formatters)
        {            
            formatters.JsonFormatter.SerializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented,
                NullValueHandling = NullValueHandling.Ignore,
                PreserveReferencesHandling = PreserveReferencesHandling.None,
                TypeNameHandling = TypeNameHandling.Auto,
                TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Simple,
                Error = (sender, args) =>
                {
                    throw new InvalidOperationException(args.ErrorContext.Error.Message);
                }
            };
        }
    }
}
