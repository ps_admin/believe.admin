﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using Planetshakers.Core.DataInterfaces;
using Planetshakers.WebApi.Areas.Authentication.Models;

namespace Planetshakers.WebApi.Areas.Authentication.Controllers
{
    public class AuthController : ApiController
    {
        private readonly IAccountRepository _accountRepository;

        public AuthController(IAccountRepository accountRepository)
        {
            _accountRepository = accountRepository;
        }

        public HttpResponseMessage Post([FromBody] AuthInputModel authInputModel)
        {
            var account = _accountRepository.Login(authInputModel.Email, authInputModel.Password);
            if (account == null)
                return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Username and password combination is rejected.");

            return Request.CreateResponse(HttpStatusCode.OK, new
                { 
                    Id = account.ContactId, // Should expose external ID instead?
                    account.FirstName,
                    account.LastName
                });
        }
    }
}