﻿namespace Planetshakers.WebApi.Areas.Authentication.Models
{
    public class AuthInputModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}