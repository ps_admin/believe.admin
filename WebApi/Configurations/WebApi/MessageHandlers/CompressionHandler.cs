﻿using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Planetshakers.WebApi.Configurations.WebApi.MessageHandlers
{
    internal class CompressionHandler : DelegatingHandler
    {
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            return base.SendAsync(request, cancellationToken).ContinueWith(
                responseToCompleteTask =>
                {
                    HttpResponseMessage response = responseToCompleteTask.Result;

                    if (response.RequestMessage.Headers.AcceptEncoding != null)
                    {
                        string encodingType = response.RequestMessage.Headers.AcceptEncoding.First().Value;

                        if (response.Content == null || encodingType == null)
                            response.Content = response.Content;
                        else
                            response.Content = new CompressedContent(response.Content, encodingType);
                    }

                    return response;
                },
                TaskContinuationOptions.OnlyOnRanToCompletion);
        }
    }
}