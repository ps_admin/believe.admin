﻿using Ninject.Modules;
using Planetshakers.Core.DataInterfaces;
using Planetshakers.Data.SqlServer.Repositories;
using System.Configuration;

namespace Planetshakers.WebApi.Ioc
{
    public class BindingModules : NinjectModule
    {
        public override void Load()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["PlanetshakersDB"].ConnectionString;

            Bind<IAccountRepository>().To<AccountRepository>().WithConstructorArgument("connectionString", connectionString);
            Bind<IUserRepository>().To<UserRepository>().WithConstructorArgument("connectionString", connectionString);
            Bind<ICheckInRepository>().To<CheckInRepository>().WithConstructorArgument("connectionString", connectionString);
            Bind<IAdministrationRepository>().To<AdministrationRepository>().WithConstructorArgument("connectionString", connectionString);
            Bind<IRegistrationRepository>().To<RegistrationRepository>().WithConstructorArgument("connectionString", connectionString);
            Bind<IReportRepository>().To<ReportRepository>().WithConstructorArgument("connectionString", connectionString);
            Bind<IManagementRepository>().To<ManagementRepository>().WithConstructorArgument("connectionString", connectionString);
        }
    }
}