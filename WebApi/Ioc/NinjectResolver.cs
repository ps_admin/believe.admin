﻿using System.Collections.Generic;
using System.Web.Http.Dependencies;
using Ninject;

namespace Planetshakers.WebApi.Ioc
{
    public class NinjectResolver : IDependencyResolver
    {
        private readonly IKernel _kernel;

        public NinjectResolver(IKernel kernel)
        {
            _kernel = kernel;
        }

        public IDependencyScope BeginScope()
        {
            return this;
        }

        public object GetService(System.Type serviceType)
        {
            return _kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(System.Type serviceType)
        {
            try
            {
                return _kernel.GetAll(serviceType);
            }
            catch 
            {
                return new List<object>();
            }
            
        }

        public void Dispose()
        {
            // When BeginScope returns 'this', the Dispose method must be a no-op.
        }
    }
}