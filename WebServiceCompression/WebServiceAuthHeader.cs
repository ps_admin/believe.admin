﻿using System.Web.Services.Protocols;

namespace WebServiceCompression
{
    public class WebServiceAuthHeader: SoapHeader
	{
		public string UserName;
		public string Password;
	}
}